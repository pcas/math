// Module defines the interface for a module.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package module

import (
	"bitbucket.org/pcas/math/abeliangroup"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
)

// Scalable defines the ScalarMultiply and ScalarRing methods.
type Scalable interface {
	ScalarMultiply(a object.Element, x object.Element) (object.Element, error) // ScalarMultiply returns the scalar product a * x, where a is an element of the ring, and x is an element of the module.
	ScalarRing() ring.Interface                                                // ScalarRing returns the scalar ring.
}

// Interface defines the module interface that all modules must satisfy.
type Interface interface {
	abeliangroup.Interface
	Scalable
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum return the sum x1 + ... + xn. The sum of the empty sequence is zero. If M satisfies the interface:
//		type Sumer interface {
//			Sum(S ...object.Element) (object.Element, error) // Sum returns
//			   the sum of the elements in the slice S. The sum of the empty
//			   slice is the zero element.
//		}
// then M's Sum method will be called.
func Sum(M Interface, xs ...object.Element) (object.Element, error) {
	return abeliangroup.Sum(M, xs...)
}
