// Properties defines functions for checking properties of elements in a polynomial ring.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package polynomial

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
)

// variabler defines the interface satisfied by the Variable method.
type variabler interface {
	Variable(i int) (object.Element, error) // Variable returns the i-th variable x_i of the polynomial ring. The variables are indexed from 0 up to rank - 1 (inclusive).
}

// isConstanter defines the interface satisfied by the IsConstant method.
type isConstanter interface {
	IsConstant(f object.Element) (bool, object.Element, error) // IsConstant returns true iff f is a constant polynomial. If true, also returns the constant.
}

// isMonomialer defines the interface satisfied by the IsMonomial method.
type isMonomialer interface {
	IsMonomial(f object.Element) (bool, error) // IsMonomial returns true iff the polynomial f is a monomial (that is, if f is of the form x^m, for some element m in the monoid of exponents).
}

// isTermer defines the interface satisfied by the IsTerm method.
type isTermer interface {
	IsTerm(f object.Element) (bool, error) // IsTerm returns true iff f is a term (that is, if f is of the form c * x^m for some element c in the coefficient ring, and some element m in the monoid of exponents.).
}

// isMonicer defines the interface satisfied by the IsMonic method.
type isMonicer interface {
	IsMonic(f object.Element) (bool, error) // IsMonic returns true iff f is a monic polynomial (that is, if the leading coefficient of f is 1).
}

// leadingMonomialer defines the interface satisfied by the LeadingMonomial method.
type leadingMonomialer interface {
	LeadingMonomial(f object.Element) (object.Element, error) // LeadingMonomial returns the leading monomial of the non-zero polynomial f.
}

// leadingTermer defines the interface satisfied by the LeadingTerm method.
type leadingTermer interface {
	LeadingTerm(f object.Element) (object.Element, error) // LeadingTerm returns the leading term of the polynomial f.
}

// leadingCoefficienter defines the interface satisfied by the LeadingCoefficient method.
type leadingCoefficienter interface {
	LeadingCoefficient(f object.Element) (object.Element, error) // LeadingCoefficient returns the leading coefficient of the polynomial f. The returned coefficient is an element of the coefficient ring.
}

// constantCoefficienter defines the interface satisfied by the ConstantCoefficient method.
type constantCoefficienter interface {
	ConstantCoefficient(f object.Element) (object.Element, error) // ConstantCoefficient returns the constant coefficient of the polynomial f. The returned coefficient is an element of the coefficient ring.
}

// coefficientser defines the interface satisfied by the Coefficients method.
type coefficientser interface {
	Coefficients(f object.Element) ([]object.Element, error) // Coefficients returns the slice of coefficients of the polynomial f. Only the non-zero coefficients will be returned, hence the zero polynomial returns an empty slices. The returned coefficients are elements of the coefficient ring.
}

// coefficientsAndExponentser defines the interface satisfied by the CoefficientsAndExponents method.
type coefficientsAndExponentser interface {
	CoefficientsAndExponents(f object.Element) (cs []object.Element, es []object.Element, err error) // CoefficientsAndExponents returns two parallel slices of coefficients cs and exponents es of the polynomial f. Only the non-zero coefficients will be returned, hence the zero polynomial returns two empty slices. The returned coefficients are elements of the coefficient ring, and the returned exponents are elements of the monoid of exponents. The exponents are sorted in increasing order.
}

// termser define the interface satisfied by the Terms method.
type termser interface {
	Terms(f object.Element) ([]object.Element, error) // Terms returns the non-zero terms of the polynomial f, sorted in increasing exponent order.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Rank returns the rank of the polynomial ring R (that is, the rank of the exponent monoid, or equivalently the number of variables of R).
func Rank(R Interface) int {
	return R.ExponentMonoid().Rank()
}

// MonomialOrder returns the monomial order on the exponent monoid of the polynomial ring R.
func MonomialOrder(R Interface) monomialorder.Type {
	return R.ExponentMonoid().MonomialOrder()
}

// Variable returns the i-th variable x_i of the polynomial ring R, if supported. Variables are indexed from 0 up to rank - 1 (inclusive). This is supported if and only if R satisfies the interface:
//		type Variabler interface {
//			Variable(i int) (object.Element, error) // Variable returns the
//			   i-th variable x_i of the polynomial ring. The variables are
//			   indexed from 0 up to rank - 1 (inclusive).
//		}
func Variable(R Interface, i int) (object.Element, error) {
	RR, ok := R.(variabler)
	if !ok {
		return nil, errors.PolynomialRingNoVariables.New()
	}
	return RR.Variable(i)
}

// Variables returns the variables of the polynomial ring R, if supported. This is supported if and only if R satisfies the Variabler interface as described in the Variable function.
func Variables(R Interface) ([]object.Element, error) {
	// Does R have a Variable method?
	RR, ok := R.(variabler)
	if !ok {
		return nil, errors.PolynomialRingNoVariables.New()
	}
	// Extract the variables into a slice
	n := Rank(R)
	vars := make([]object.Element, 0, n)
	for i := 0; i < n; i++ {
		x, err := RR.Variable(i)
		if err != nil {
			return nil, err
		}
		vars = append(vars, x)
	}
	return vars, nil
}

// VariableNames returns the names to be used in printing of the variables of the polynomial ring R.
func VariableNames(R Interface) ([]string, error) {
	n := Rank(R)
	names := make([]string, 0, n)
	for i := 0; i < n; i++ {
		name, err := R.VariableName(i)
		if err != nil {
			return nil, err
		}
		names = append(names, name)
	}
	return names, nil
}

// AssignVariableNames assigns names to the variables of the polynomial ring R to be used in printing. The variables are indexed from 0 up to rank - 1 (inclusive).
func AssignVariableNames(R Interface, names []string) error {
	n := Rank(R)
	if len(names) != n {
		return errors.SliceLengthNotEqualRank.New(len(names), n)
	}
	for i, name := range names {
		if err := R.AssignVariableName(name, i); err != nil {
			return err
		}
	}
	return nil
}

// IsConstant returns true iff f is a constant polynomial in the polynomial ring R. If true, also returns the constant. The returned constant is an element of the coefficient ring of R. If R satisfies the interface:
//		type IsConstanter interface {
//			IsConstant(f object.Element) (bool, object.Element, error)
//				// IsConstant returns true iff f is a constant polynomial. If
//				   true, also returns the constant.
//		}
// then R's IsConstant method will be called.
func IsConstant(R Interface, f object.Element) (bool, object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(isConstanter); ok {
		return RR.IsConstant(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return false, nil, err
	} else if ok {
		return true, R.CoefficientRing().Zero(), nil
	}
	// Extract the exponents of f
	if es, err := R.Exponents(f); err != nil {
		return false, nil, err
	} else if len(es) == 1 {
		if isZero, err := R.ExponentMonoid().IsZero(es[0]); err != nil {
			return false, nil, err
		} else if isZero {
			// The exponent is zero, so this is a constant polynomial
			c, err := R.Coefficient(f, es[0])
			if err != nil {
				return false, nil, err
			}
			return true, c, nil
		}
	}
	return false, nil, nil
}

// IsMonomial returns true iff the polynomial f is a monomial (that is, if f is of the form x^m for exponent m) in the polynomial ring R. If R satisfies the interface:
//		type IsMonomialer interface {
//			IsMonomial(f object.Element) (bool, error) // IsMonomial returns
//			   true iff the polynomial f is a monomial (that is, if f is of the
//			   form x^m, for some element m in the monoid of exponents).
//		}
// then R's IsMonomial method will be called.
func IsMonomial(R Interface, f object.Element) (bool, error) {
	// Does R support this method directly?
	if RR, ok := R.(isMonomialer); ok {
		return RR.IsMonomial(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return false, err
	} else if ok {
		return false, nil
	}
	// Extract the exponents of f
	if es, err := R.Exponents(f); err != nil {
		return false, err
	} else if len(es) == 1 {
		// f consists of a single term, so check that the coefficient is 1
		c, err := R.Coefficient(f, es[0])
		if err != nil {
			return false, err
		}
		return R.CoefficientRing().IsOne(c)
	}
	return false, nil
}

// IsTerm returns true iff f is a term (that is, if f is of the form c * x^m for some coefficient c and exponent m) in the polynomial ring R. If R satisfies the interface:
//		type IsTermer interface {
//			IsTerm(f object.Element) (bool, error) // IsTerm returns true iff f
//			   is a term (that is, if f is of the form c * x^m for some element
//			   c in the coefficient ring, and some element m in the monoid of
//			   exponents.).
//		}
// then R's IsTerm method will be called.
func IsTerm(R Interface, f object.Element) (bool, error) {
	// Does R support this method directly?
	if RR, ok := R.(isTermer); ok {
		return RR.IsTerm(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return false, err
	} else if ok {
		return true, nil
	}
	// If there is only one exponent, this is a term
	es, err := R.Exponents(f)
	if err != nil {
		return false, err
	}
	return len(es) == 1, nil
}

// IsMonic returns true iff f is a monic polynomial (that is, if the leading coefficient of f is 1) in the polynomial ring R. If R satisfies the interface:
//		type IsMonicer interface {
//			IsMonic(f object.Element) (bool, error) // IsMonic returns true iff
//			   f is a monic polynomial (that is, if the leading coefficient of
//			   f is 1).
//		}
// then R's IsMonic method will be called.
func IsMonic(R Interface, f object.Element) (bool, error) {
	// Does R support this method directly?
	if RR, ok := R.(isMonicer); ok {
		return RR.IsMonic(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); ok || err != nil {
		return false, err
	}
	// Recover the degree
	m, err := R.Degree(f)
	if err != nil {
		return false, err
	}
	// Recover the corresponding coefficient
	var c object.Element
	if c, err = R.Coefficient(f, m); err != nil {
		return false, err
	}
	// Is this coefficient equal to 1?
	return R.CoefficientRing().IsOne(c)
}

// LeadingMonomial returns the leading monomial of the non-zero polynomial f in the polynomial ring R. If R satisfies the interface:
//		type LeadingMonomialer interface {
//			LeadingMonomial(f object.Element) (object.Element, error)
//				// LeadingMonomial returns the leading monomial of the non-zero
//				   polynomial f.
//		}
// then R's LeadingMonomial method will be called.
func LeadingMonomial(R Interface, f object.Element) (object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(leadingMonomialer); ok {
		return RR.LeadingMonomial(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return nil, err
	} else if ok {
		return nil, errors.IllegalZeroArg.New()
	}
	// Recover the degree
	m, err := R.Degree(f)
	if err != nil {
		return nil, err
	}
	// Return the monomial
	return R.ToMonomial(m)
}

// LeadingTerm returns the leading term of the polynomial f in the polynomial ring R. If R satisfies the interface:
//		type LeadingTermer interface {
//			LeadingTerm(f object.Element) (object.Element, error)
//				// LeadingTerm returns the leading term of the polynomial f.
//		}
// then R's LeadingTerm method will be called.
func LeadingTerm(R Interface, f object.Element) (object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(leadingTermer); ok {
		return RR.LeadingTerm(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return nil, err
	} else if ok {
		return R.Zero(), nil
	}
	// Recover the degree
	m, err := R.Degree(f)
	if err != nil {
		return nil, err
	}
	// Recover the corresponding coefficient
	var c object.Element
	if c, err = R.Coefficient(f, m); err != nil {
		return nil, err
	}
	// Build and return the term
	return R.ToTerm(c, m)
}

// LeadingCoefficient returns the leading coefficient of the polynomial f in the polynomial ring R. The returned coefficient is an element of the coefficient ring of R. If R satisfies the interface:
//		type LeadingCoefficienter interface {
//			LeadingCoefficient(f object.Element) (object.Element, error)
//				// LeadingCoefficient returns the leading coefficient of the
//				   polynomial f. The returned coefficient is an element of the
//				   coefficient ring.
//		}
// then R's LeadingCoefficient method will be called.
func LeadingCoefficient(R Interface, f object.Element) (object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(leadingCoefficienter); ok {
		return RR.LeadingCoefficient(f)
	}
	// Get the zero polynomial out of the way
	if ok, err := R.IsZero(f); err != nil {
		return nil, err
	} else if ok {
		return R.CoefficientRing().Zero(), nil
	}
	// Recover the degree
	m, err := R.Degree(f)
	if err != nil {
		return nil, err
	}
	// Recover the corresponding coefficient
	return R.Coefficient(f, m)
}

// ConstantCoefficient returns the constant coefficient of the polynomial f in the polynomial ring R. The returned coefficient is an element of the coefficient ring of R. If R satisfies the interface:
//		type ConstantCoefficienter interface {
//			ConstantCoefficient(f object.Element) (object.Element, error)
//				// ConstantCoefficient returns the constant coefficient of the
//				   polynomial f. The returned coefficient is an element of the
//				   coefficient ring.
//		}
// then R's ConstantCoefficient method will be called.
func ConstantCoefficient(R Interface, f object.Element) (object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(constantCoefficienter); ok {
		return RR.ConstantCoefficient(f)
	}
	// Return the constant coefficient
	return R.Coefficient(f, R.ExponentMonoid().Zero())
}

// Coefficients returns the slice of coefficients of the polynomial f in the polynomial ring R. Only the non-zero coefficients will be returned, hence the zero polynomial returns an empty slices. The returned coefficients are elements of the coefficient ring of R. If R satisfies the interface:
//		type Coefficientser interface {
//			Coefficients(f object.Element) ([]object.Element, error)
//				// Coefficients returns the slice of coefficients of the
//				   polynomial f. Only the non-zero coefficients will be
//				   returned, hence the zero polynomial returns an empty slices.
//				   The returned coefficients are elements of the coefficient
//				   ring.
//		}
// then R's Coefficients method will be called.
func Coefficients(R Interface, f object.Element) ([]object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(coefficientser); ok {
		return RR.Coefficients(f)
	}
	// Does R support the CoefficientsAndExponents method?
	if RR, ok := R.(coefficientsAndExponentser); ok {
		cs, _, err := RR.CoefficientsAndExponents(f)
		if err != nil {
			return nil, err
		}
		return cs, nil
	}
	// Recover the exponents
	es, err := R.Exponents(f)
	if err != nil {
		return nil, err
	}
	// For each exponent, recover the corresponding coefficient
	cs := make([]object.Element, 0, len(es))
	for _, m := range es {
		var c object.Element
		if c, err = R.Coefficient(f, m); err != nil {
			return nil, err
		}
		cs = append(cs, c)
	}
	return cs, nil
}

// CoefficientsAndExponents returns two parallel slices of coefficients cs and exponents es of the polynomial f in the polynomial ring R. Only the non-zero coefficients will be returned, hence the zero polynomial returns two empty slices.  The returned coefficients are elements of the coefficient ring of R, and the returned exponents are elements of the monoid of exponents of R. The exponents are sorted in increasing order. If R satisfies the interface:
//		type CoefficientsAndExponentser interface {
//			CoefficientsAndExponents(f object.Element) (cs []object.Element,
//				es []object.Element, err error) // CoefficientsAndExponents
//				   returns two parallel slices of coefficients cs and exponents
//				   es of the polynomial f. Only the non-zero coefficients will
//				   be returned, hence the zero polynomial returns two empty
//				   slices. The returned coefficients are elements of the
//				   coefficient ring, and the returned exponents are elements of
//			  	   the monoid of exponents. The exponents are sorted in
//				   increasing order.
//		}
// then R's CoefficientsAndExponents method will be called.
func CoefficientsAndExponents(R Interface, f object.Element) (cs []object.Element, es []object.Element, err error) {
	// Does R support this method directly?
	if RR, ok := R.(coefficientsAndExponentser); ok {
		return RR.CoefficientsAndExponents(f)
	}
	// Recover the exponents
	es, err = R.Exponents(f)
	if err != nil {
		return nil, nil, err
	}
	// Does R support the Coefficients method?
	if RR, ok := R.(coefficientser); ok {
		cs, err = RR.Coefficients(f)
		if err != nil {
			return nil, nil, err
		}
		return
	}
	// For each exponent, recover the corresponding coefficient
	cs = make([]object.Element, 0, len(es))
	for _, m := range es {
		var c object.Element
		if c, err = R.Coefficient(f, m); err != nil {
			return nil, nil, err
		}
		cs = append(cs, c)
	}
	return
}

// Terms returns the non-zero terms of the polynomial f in the polynomial ring R, sorted in increasing exponent order. The returned terms are elements of R. If R satisfies the interface:
//		type Termser interface {
//			Terms(f object.Element) ([]object.Element, error) // Terms returns
//			   the non-zero terms of the polynomial f, sorted in increasing
//			   exponent order.
// 		}
// then R's Terms method will be called.
func Terms(R Interface, f object.Element) ([]object.Element, error) {
	// Does R support this method directly?
	if RR, ok := R.(termser); ok {
		return RR.Terms(f)
	}
	// Get the easy cases out of the way
	if isZero, err := R.IsZero(f); err != nil {
		return nil, err
	} else if isZero {
		return []object.Element{}, nil
	} else if isTerm, err := IsTerm(R, f); err != nil {
		return nil, err
	} else if isTerm {
		return []object.Element{f}, nil
	}
	// Extract the parallel slices of coefficients and exponents of f
	cs, es, err := CoefficientsAndExponents(R, f)
	if err != nil {
		return nil, err
	}
	// Create the terms
	T := make([]object.Element, 0, len(cs))
	for i, c := range cs {
		t, err := R.ToTerm(c, es[i])
		if err != nil {
			return nil, err
		}
		T = append(T, t)
	}
	return T, nil
}
