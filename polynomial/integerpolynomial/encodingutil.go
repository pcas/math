//go:generate ../internal/template/template.py ../internal/template/encodingutil.txt encodingutil.go "package=integerpolynomial" "polynomial=polynomial"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../internal/template/encodingutil.txt".
To modify this file, edit "../internal/template/encodingutil.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Encodingutil defines helpful functions for use when encoding/decoding polynomials.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/polynomial/integerpolynomial
/////////////////////////////////////////////////////////////////////////
// encodingutil.go
/////////////////////////////////////////////////////////////////////////

package integerpolynomial

import (
	"encoding/gob"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// GobDecode reads the next value from the given gob.Decoder and decodes it as a polynomial.
func GobDecode(dec *gob.Decoder) (*Element, error) {
	// Decode into a element
	f := &Element{}
	if err := dec.Decode(f); err != nil {
		return nil, err
	}
	// Check whether the result is 0 and 1
	if f.IsZero() {
		return Zero(parentOfElement(f)), nil
	} else if f.IsOne() {
		return One(parentOfElement(f)), nil
	}
	// Return the element
	return f, nil
}

// GobDecodeSlice reads the next value from the given gob.Decoder and decodes it as a slice of polynomials.
func GobDecodeSlice(dec *gob.Decoder) ([]*Element, error) {
	// Decode into a slice
	var S []*Element
	if err := dec.Decode(&S); err != nil {
		return nil, err
	}
	// Check whether any of the resulting polynomials are 0 or 1
	for i, f := range S {
		if f.IsZero() {
			S[i] = Zero(parentOfElement(f))
		} else if f.IsOne() {
			S[i] = One(parentOfElement(f))
		}
	}
	// Return the slice
	return S, nil
}
