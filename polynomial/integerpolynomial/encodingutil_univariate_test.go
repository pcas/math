// Tests of univariate polynomial gob encoding/decoding.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/testing/assert"
	"bytes"
	"encoding/gob"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobSliceEncoding tests gob encoding/decoding of slices.
func TestGobSliceEncoding(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The slices of coefficients to test
	coeffs := [][]int{
		// 0
		{0},
		// 1
		{1},
		// 2
		{2},
		// x
		{0, 1},
		// 10x^2
		{0, 0, 10},
		// x^3+x^4
		{0, 0, 0, 1, 1},
		// 1 + x^3 - 10x^4 + 3x^5
		{1, 0, 0, 1, -10, 3},
		// -5 + 2x + 3x^7 + x^10
		{-5, 2, 0, 0, 0, 0, 0, 3, 0, 0, 1},
	}
	// Create the slice of polynomials
	S := make([]*Element, 0, len(coeffs))
	for _, cs := range coeffs {
		S = append(S, fromIntSlice(R, cs))
	}
	// Create the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	// Encode the slice of polynomials
	if assert.NoError(enc.Encode(S)) {
		// Decode the slice of polynomials
		if SS, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Len(SS, len(S)) {
			// Check for equality
			for i, f := range S {
				assert.AreEqual(R, f, SS[i])
			}
		}
	}
}
