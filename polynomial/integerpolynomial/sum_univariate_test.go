// Tests taking sums/differences of univariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"math/rand"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// randomIntSlice returns a random slice of ints of length n with entries in the range [-k,...,k].
func randomIntSlice(n int, k int) []int {
	k++
	S := make([]int, 0, n)
	for i := 0; i < n; i++ {
		x := rand.Int() % k
		if (rand.Int() % 2) == 0 {
			x = -x
		}
		S = append(S, x)
	}
	return S
}

// sumIntSlices returns the entry-wise sum of given slice of []ints. Assumes that all the slices are of the same length, and that there exists at least one slice to sum.
func sumIntSlices(S [][]int) []int {
	T := make([]int, len(S[0]))
	for _, s := range S {
		for i, x := range s {
			T[i] += x
		}
	}
	return T
}

// negateSlice returns the slice with each entry negated.
func negateSlice(S []int) []int {
	T := make([]int, 0, len(S))
	for _, c := range S {
		T = append(T, -c)
	}
	return T
}

// toObjectSlice returns the slice of *Elements as a slice of object.Elements.
func toObjectSlice(S []*Element) []object.Element {
	T, err := object.SliceToElementSlice(S)
	if err != nil {
		panic(err)
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestSum tests Sum.
func TestSum(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Summing no polynomials should return the zero element
	if g, err := Sum(R); assert.NoError(err) {
		assert.IsZero(g)
	}
	// Summing a single polynomial should return that polynomial
	tests := [][]int{
		{0},
		{1},
		{0, 1, 2},
		{1, 0, -2, 0, -1},
	}
	for _, test := range tests {
		f := fromIntSlice(R, test)
		if g, err := Sum(R, f); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
		if g, err := R.Sum(f); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
	}
	// Summing two polynomials is special-cased to call Add. We will do
	// tests[i] + tests[i+1], 0 <= i < len(tests) - 1. First create the expected
	// results.
	res := [][]int{
		{1},
		{1, 1, 2},
		{1, 1, 0, 0, -1},
	}
	for i := 0; i < len(tests)-1; i++ {
		f := fromIntSlice(R, tests[i])
		g := fromIntSlice(R, tests[i+1])
		expected := fromIntSlice(R, res[i])
		if h, err := Sum(R, f, g); assert.NoError(err) {
			assert.AreEqual(R, h, expected)
		}
		if h, err := R.Sum(f, g); assert.NoError(err) {
			assert.AreEqual(R, h, expected)
		}
	}
	// Sum a slice of 10 zeros and 10 ones
	fs := make([]*Element, 0, 20)
	for i := 0; i < 20; i++ {
		if (i % 2) == 0 {
			fs = append(fs, Zero(R))
		} else {
			fs = append(fs, One(R))
		}
	}
	if g, err := Sum(R, fs...); assert.NoError(err) {
		assert.AreEqual(R, g, fromIntSlice(R, []int{10}))
	}
	if g, err := R.Sum(toObjectSlice(fs)...); assert.NoError(err) {
		assert.AreEqual(R, g, fromIntSlice(R, []int{10}))
	}
	// Sum slices of polynomials of various lengths
	for _, size := range []int{3, 10, 100, 500, 1000, 2000, 5000} {
		// Create some random slices to sum
		tests = make([][]int, 0, size)
		for i := 0; i < size; i++ {
			tests = append(tests, randomIntSlice(20, 8))
		}
		// Convert them to polynomials
		fs := make([]*Element, 0, len(tests))
		for _, test := range tests {
			fs = append(fs, fromIntSlice(R, test))
		}
		// Perform the sum
		if g, err := Sum(R, fs...); assert.NoError(err) {
			assert.AreEqual(R, g, fromIntSlice(R, sumIntSlices(tests)))
		}
		if g, err := R.Sum(toObjectSlice(fs)...); assert.NoError(err) {
			assert.AreEqual(R, g, fromIntSlice(R, sumIntSlices(tests)))
		}
	}
	// If the slice contains elements of the wrong parent, this should error
	_, err := Sum(R, Zero(NewUnivariateRing()))
	assert.Error(err)
	_, err = R.Sum(Zero(NewUnivariateRing()))
	assert.Error(err)
}

// TestNegate tests Negate.
func TestNegae(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Negate polynomials of various lengths
	for _, size := range []int{3, 10, 100, 500, 1000, 2000, 5000} {
		// Create a random slice to negate
		test := randomIntSlice(size, 10000)
		// Convert it to a polynomial
		f := fromIntSlice(R, test)
		// Perform the negation
		assert.AreEqual(R, f.Negate(), fromIntSlice(R, negateSlice(test)))
	}
}
