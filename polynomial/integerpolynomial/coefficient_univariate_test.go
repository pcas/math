// Tests of coefficientutil.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcas/math/vector/integervector"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestElementCoefficientsAndDegrees tests CoefficientsAndDegrees and CoefficientsAndExponents.
func TestElementCoefficientsAndDegrees(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{},
		{1},
		{0, 1},
		{-13},
		{0, -11, 0, 3, 0, -1},
		{5, 0, 0, 0, 17},
	}
	// Run the tests
	for _, test := range tests {
		// Build the polynomial
		f := fromIntSlice(R, test)
		// Recover the coefficients and exponents, and check them
		cs, es := f.CoefficientsAndExponents()
		if assert.Equal(len(cs), len(es)) {
			idx := 0
			for i, c := range test {
				if c != 0 {
					assert.IsEqualToInt64(cs[idx], int64(c))
					if m := es[idx]; assert.Equal(m.Dimension(), 1) {
						assert.IsEqualToInt64(m.EntryOrPanic(0), int64(i))
					}
					idx++
				}
			}
			assert.True(idx == len(cs))
		}
	}
}

// TestLeadingAndConstantCoefficient tests LeadingCoefficient and ConstantCoefficient.
func TestLeadingAndConstantCoefficient(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{},
		{1},
		{0, 1},
		{78},
		{0, -7, 0, 0, 1, 9},
		{5, 0, 0, 0, 2},
	}
	// Run the tests
	for _, test := range tests {
		// Build the polynomial
		f := fromIntSlice(R, test)
		// Check the leading coefficient
		c := f.LeadingCoefficient()
		if len(test) == 0 {
			assert.IsZero(c)
		} else {
			assert.IsEqualToInt64(c, int64(test[len(test)-1]))
		}
		// Check the constant coefficient
		c = f.ConstantCoefficient()
		if len(test) == 0 {
			assert.IsZero(c)
		} else {
			assert.IsEqualToInt64(c, int64(test[0]))
		}
	}
}

// TestCoefficientOfDegree tests CoefficientOfDegree,  CoefficientOfDegreeInt64, and Coefficient.
func TestCoefficientOfDegree(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{},
		{1},
		{0, 1},
		{-9},
		{0, -2, 5, 0, -1, 12},
		{2, 0, 0, 7},
		{0, 0, 0, 1, -1},
	}
	// Run the tests
	for _, test := range tests {
		// Build the polynomial
		f := fromIntSlice(R, test)
		// Walk through a range of degrees
		for i := -10; i <= 10; i++ {
			// Recover the coefficient
			if c, err := f.Coefficient(toExponentInt64(R, int64(i))); assert.NoError(err) {
				// Check the result
				if i >= 0 && i < len(test) {
					assert.IsEqualToInt64(c, int64(test[i]))
				} else {
					assert.IsZero(c)
				}
			}
		}
	}
	// Asking for the coefficient for a huge exponent should return 0
	f := fromIntSlice(R, []int{1, 2, 3, 4})
	if pow2, err := integer.FromInt(2).PowerInt64(70); assert.NoError(err) {
		assert.IsZero(f.Coefficient(toExponent(R, pow2)))
	}
	// Asking for the coefficient using a non-1-dimensional vector should error
	if L, err := integervector.DefaultLattice(3); assert.NoError(err) {
		_, err = f.Coefficient(integervector.Zero(L))
		assert.Error(err)
	}
}

// TestElementIsConstant tests IsConstant.
func TestElementIsConstant(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test and the expected results
	tests := [][]int{
		{0},
		{1},
		{17},
		{0, 1},
		{0, 0, 7},
		{0, 1, -1},
	}
	expected := []bool{true, true, true, false, false, false}
	// Run the tests
	for i, test := range tests {
		ok, val := fromIntSlice(R, test).IsConstant()
		if expected[i] {
			if assert.True(ok) {
				assert.AreEqual(integer.Ring(), val, integer.FromInt(test[0]))
			}
		} else {
			assert.False(ok)
		}
	}
}
