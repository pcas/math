// Tests of creation.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestFromIntegerSlice tests FromIntegerSlice, FromInt32Slice, FromInt64Slice, and FromUint64Slice.
func TestFromIntegerSlice(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The test slices
	tests := [][]int{
		{},
		{0},
		{1},
		{0, 1},
		{1, 0, 2, 3, 10},
	}
	// Run the tests
	for _, testInt := range tests {
		// Rebuild the slice
		testInt64 := make([]int64, 0, len(testInt))
		testInteger := make([]*integer.Element, 0, len(testInt))
		for _, c := range testInt {
			testInt64 = append(testInt64, int64(c))
			testInteger = append(testInteger, integer.FromInt(c))
		}
		// Build the polynomial using each function
		fs := make([]*Element, 0, 3)
		if g, err := FromIntSlice(R, 0, 1, testInt); assert.NoError(err) {
			fs = append(fs, g)
		}
		if g, err := FromInt64Slice(R, 0, 1, testInt64); assert.NoError(err) {
			fs = append(fs, g)
		}
		if g, err := FromIntegerSlice(R, 0, 1, testInteger); assert.NoError(err) {
			fs = append(fs, g)
		}
		// Assert equality (we assume nothing!)
		for _, f := range fs {
			for _, g := range fs {
				assert.AreEqual(R, f, g)
			}
		}
	}
}

// TestDuplicateExponents tests that duplicate exponents are handled correctly.
func TestDuplicateExponents(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The test slice of coefficients
	tests := [][]int64{
		{0},
		{1},
		{0, 1, 2},
		{1, -1, -4, 7, 11},
	}
	// We will create a polynomial where all coefficients are doubled
	for _, test := range tests {
		// Create the first test polynomial
		es := make([]int64, 0, 2*len(test))
		cs := make([]int64, 0, 2*len(test))
		for i, c := range test {
			es = append(es, int64(i))
			es = append(es, int64(i))
			cs = append(cs, c)
			cs = append(cs, c)
		}
		f, err1 := FromParallelInt64Slices(R, cs, es)
		// Create the second test polynomial
		es = make([]int64, 2*len(test))
		cs = make([]int64, 2*len(test))
		for i, c := range test {
			es[i] = int64(i)
			es[len(test)+i] = int64(i)
			cs[i] = c
			cs[len(test)+i] = c
		}
		g, err2 := FromParallelInt64Slices(R, cs, es)
		// Create the expected polynomial
		es = make([]int64, 0, len(test))
		cs = make([]int64, 0, len(test))
		for i, c := range test {
			es = append(es, int64(i))
			cs = append(cs, 2*c)
		}
		h, err3 := FromParallelInt64Slices(R, cs, es)
		// Check that they agree
		if assert.NoError(err1) && assert.NoError(err2) && assert.NoError(err3) {
			assert.AreEqual(R, f, h)
			assert.AreEqual(R, g, h)
		}
	}
}

// TestFromParallelSlices tests FromParallelIntegerSlices and FromParallelInt64Slices.
func TestFromParallelSlices(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The test slices
	tests := [][]int{
		{},
		{0},
		{1},
		{0, 1},
		{1, 0, 2, 0, 0, 0, 0, 0, 10},
		{0, 0, 0, 0, -7, 8, 1},
	}
	// Run the tests
	for _, test := range tests {
		// Build the parallel slices of coefficients and exponents
		csInt64 := make([]int64, 0, len(test))
		esInt64 := make([]int64, 0, len(test))
		csInteger := make([]*integer.Element, 0, len(test))
		esInteger := make([]*integer.Element, 0, len(test))
		for i, c := range test {
			if c != 0 {
				csInt64 = append(csInt64, int64(c))
				esInt64 = append(esInt64, int64(i))
				csInteger = append(csInteger, integer.FromInt(c))
				esInteger = append(esInteger, integer.FromInt(i))
			}
		}
		// Build and check the polynomials
		f := fromIntSlice(R, test)
		if g, err := FromParallelInt64Slices(R, csInt64, esInt64); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
		if g, err := FromParallelIntegerSlices(R, csInteger, esInteger); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
		// The order we list the elements shouldn't matter
		if len(csInt64) > 1 {
			// Swap the first two entries in the slices
			csInt64[0], csInt64[1] = csInt64[1], csInt64[0]
			esInt64[0], esInt64[1] = esInt64[1], esInt64[0]
			csInteger[0], csInteger[1] = csInteger[1], csInteger[0]
			esInteger[0], esInteger[1] = esInteger[1], esInteger[0]
			// Build and check the polynomials
			if g, err := FromParallelInt64Slices(R, csInt64, esInt64); assert.NoError(err) {
				assert.AreEqual(R, f, g)
			}
			if g, err := FromParallelIntegerSlices(R, csInteger, esInteger); assert.NoError(err) {
				assert.AreEqual(R, f, g)
			}
		}
	}
	// Building with negative exponents should return an error
	// THINK
	//	_, err := FromParallelInt64Slices(R, []int64{1}, []int64{-10})
	//	assert.Error(err)
	//	_, err = FromParallelIntegerSlices(R, []*integer.Element{integer.One()}, []*integer.Element{integer.FromInt(-10)})
	//	assert.Error(err)
	// Building with unequal slices should return an error
	_, err := FromParallelInt64Slices(R, []int64{1}, []int64{})
	assert.Error(err)
	_, err = FromParallelIntegerSlices(R, []*integer.Element{integer.One()}, []*integer.Element{})
	assert.Error(err)
	// Building with an exponent that's too large should return an error
	if powTwo, err := integer.FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = FromParallelIntegerSlices(R, []*integer.Element{integer.One()}, []*integer.Element{powTwo})
		assert.Error(err)
	}
}
