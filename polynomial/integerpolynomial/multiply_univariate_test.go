// Tests taking products of univariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestMultiply tests Multiply.
func TestMultiply(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Taking the product of no polynomials should return the identity element
	//	if g, err := Product(R); assert.NoError(err) {
	//		assert.IsOne(g)
	//	}
	// Run through some basic tests
	tests := [][]int{
		{0},
		{1},
		{0, 1, 2},
		{1, 0, -2, 0, -1},
	}
	for _, test := range tests {
		f := fromIntSlice(R, test)
		// Taking the product of a single polynomial should return that
		// polynomial
		//			if g, err := Product(R, f); assert.NoError(err) {
		//				assert.AreEqual(R, f, g)
		//			}
		// Multiplying a polynomial by 1 should return that polynomial
		if g, err := Multiply(f, One(R)); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
		if g, err := Multiply(One(R), f); assert.NoError(err) {
			assert.AreEqual(R, f, g)
		}
		// Multiplying a polynomial by 0 should return the zero polynomial
		if g, err := Multiply(f, Zero(R)); assert.NoError(err) {
			assert.IsZero(g)
		}
		if g, err := Multiply(Zero(R), f); assert.NoError(err) {
			assert.IsZero(g)
		}
	}
	// Test multiplication of two polynomials. Test data is in the format:
	//		{f,g,f*g}
	multTests := [][][]int{
		// (x+2)*(x^3-3*x+1) = x^4 + 2*x^3 - 3*x^2 - 5*x + 2
		{{2, 1}, {1, -3, 0, 1}, {2, -5, -3, 2, 1}},
		// (x^3-2*x)*(x^3+2*x) = x^6 - 4*x^2
		{{0, -2, 0, 1}, {0, 2, 0, 1}, {0, 0, -4, 0, 0, 0, 1}},
		// (2*x)*(-7*x^3+x^2-x+2) = -14*x^4 + 2*x^3 - 2*x^2 + 4*x
		{{0, 2}, {2, -1, 1, -7}, {0, 4, -2, 2, -14}},
	}
	for _, test := range multTests {
		f := fromIntSlice(R, test[0])
		g := fromIntSlice(R, test[1])
		fg := fromIntSlice(R, test[2])
		// We do the multiplication both ways round
		if h, err := Multiply(f, g); assert.NoError(err) {
			assert.AreEqual(R, h, fg)
		}
		if h, err := Multiply(g, f); assert.NoError(err) {
			assert.AreEqual(R, h, fg)
		}
	}
	// Calculate (7*x^2-x+7)^100 successively via multiplication and check it
	// against the power
	f := fromIntSlice(R, []int{7, -1, 7})
	g := f
	var err error
	for i := 2; i <= 100 && assert.NoError(err); i++ {
		// We vary the order of multiplication
		if i%2 == 0 {
			g, err = Multiply(g, f)
		} else {
			g, err = Multiply(f, g)
		}
	}
	// Now compute the power directly
	if h, err := f.PowerInt64(100); assert.NoError(err) {
		assert.AreEqual(R, h, g)
	}
	// Calculate 2*x^3-1 to various powers and check that the products agree
	// with the powers (the aim here is to check that multiplying polynomials
	// with large numbers of coefficients works).
	f = fromIntSlice(R, []int{-1, 0, 0, 2})
	if h, err := f.PowerInt64(100); assert.NoError(err) {
		for _, i := range []int64{37, 43, 52, 61} {
			// Compute f^i * f^(100-i) and check this equals f^100
			if f1, err := f.PowerInt64(i); assert.NoError(err) {
				if f2, err := f.PowerInt64(100 - i); assert.NoError(err) {
					if g, err := Multiply(f1, f2); assert.NoError(err) {
						assert.AreEqual(R, h, g)
					}
				}
			}
		}
	}
	// If the elements have different parents, these calls should error
	_, err = Multiply(Zero(R), Zero(NewUnivariateRing()))
	assert.Error(err)
	//	_, err = Product(R, Zero(NewUnivariateRing()))
	//	assert.Error(err)
}
