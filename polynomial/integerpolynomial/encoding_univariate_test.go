// Tests of univariate polynomial gob encoding/decoding.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/testing/assert"
	"bytes"
	"encoding/gob"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobEncoding tests gob encoding/decoding.
func TestGobEncoding(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The slices of coefficients to test
	coeffs := [][]int{
		// 0
		{0},
		// 1
		{1},
		// 2
		{2},
		// x
		{0, 1},
		// 10x^2
		{0, 0, 10},
		// x^3+x^4
		{0, 0, 0, 1, 1},
		// 1 + x^3 - 10x^4 + 3x^5
		{1, 0, 0, 1, -10, 3},
		// -5 + 2x + 3x^7 + x^10
		{-5, 2, 0, 0, 0, 0, 0, 3, 0, 0, 1},
	}
	// Work through the tests
	for _, cs := range coeffs {
		f := fromIntSlice(R, cs)
		// Create the encoder and decoder
		var b bytes.Buffer
		enc := gob.NewEncoder(&b)
		dec := gob.NewDecoder(&b)
		// Encode the polynomial
		if assert.NoError(enc.Encode(f)) {
			// Decode the polynomial
			if g, err := GobDecode(dec); assert.NoError(err) {
				assert.AreEqual(R, f, g)
			}
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var n *Element
	if b, err := n.GobEncode(); assert.NoError(err) {
		// Decoding should give zero
		m := &Element{}
		if assert.NoError(m.GobDecode(b)) {
			assert.IsZero(m)
		}
	}
}

// TestGobDecodeEmpty tests gob decoding an empty slice of bytes
func TestGobDecodeEmpty(t *testing.T) {
	assert := assert.New(t)
	// This should give an error
	m := &Element{}
	assert.Error(m.GobDecode(nil))
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	assert := assert.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode the an integer
	if assert.NoError(enc.Encode(One(DefaultUnivariateRing()))) {
		// Try to decode directly into a nil object -- this should error
		var m *Element
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	assert := assert.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5}
	dec := gob.NewDecoder(bytes.NewReader(b))
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(dec)
	assert.Error(err)
	m := &Element{}
	assert.Error(m.GobDecode(b))
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	assert := assert.New(t)
	// Encode a version number from the future
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	if assert.NoError(enc.Encode(byte(encodingVersion + 1))) {
		// Try decoding -- this should error
		m := &Element{}
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeExisting tests gob decoding into an existing polynomial.
func TestGobDecodeExisting(t *testing.T) {
	require := require.New(t)
	R := DefaultUnivariateRing()
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode a polynomial
	require.NoError(enc.Encode(One(R)))
	// Try to decode directly into an existing polynomial -- this should error
	require.Error(Zero(R).GobDecode(b.Bytes()))
}
