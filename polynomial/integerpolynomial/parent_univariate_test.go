// Basic tests of polynomial ring.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"github.com/stretchr/testify/require"
	"testing"
)

// half is equal to the rational 1/2, and should be treated as a constant.
var half *rational.Element

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the value half.
func init() {
	var err error
	half, err = rational.FromIntPair(1, 2)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestParent tests Parent.
func TestParent(t *testing.T) {
	assert := assert.New(t)
	// The nil polynomial has the nil parent
	var f *Element
	var R *Parent
	assert.Contains(R, f)
	assert.Contains(R, Zero(R))
	assert.AreEqual(R, f, Zero(R))
	assert.True(f.Parent() == R)
	// A nil parent is rank 0, so asking for the 0th variable should error
	_, err := R.NthVariable(0)
	assert.Error(err)
	// A non-nil parent and a nil parent are different, the following should
	// return errors
	g := Zero(NewUnivariateRing())
	_, err = Add(f, g)
	assert.Error(err)
	_, err = Add(g, f)
	assert.Error(err)
	_, err = Subtract(f, g)
	assert.Error(err)
	_, err = Subtract(g, f)
	assert.Error(err)
	_, err = Multiply(f, g)
	assert.Error(err)
	_, err = Multiply(g, f)
	assert.Error(err)
	assert.False(g.Parent() == R)
	// Polynomials with different parents can never be equal
	assert.False(g.IsEqualTo(f))
	assert.False(AreEqual(g, f))
	assert.False(g.IsEqualTo(Zero(NewUnivariateRing())))
	assert.False(AreEqual(g, Zero(NewUnivariateRing())))
}

// TestBasicRingOperations tests the basic properties of a univariate polynomial ring.
func TestBasicRingProperties(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// This had better implement the polynomial interface
	assert.Implements((*polynomial.Interface)(nil), R)
	// The rank had better be 1
	assert.EqualValues(polynomial.Rank(R), 1)
	// CoefficientRing should return the (unique) ring of integers
	assert.Equal(R.CoefficientRing(), integer.Ring())
	// Create a second ring and fetch the variable from each ring
	S := NewUnivariateRing()
	xR, err := R.NthVariable(0)
	require.NoError(t, err)
	xS, err := S.NthVariable(0)
	require.NoError(t, err)
	// Variables should returns a slice of length 1 containing the variable
	if vars := R.Variables(); assert.Len(vars, 1) {
		assert.Equal(vars[0], xR)
	}
	// These should live in their own ring, but not the other
	assert.Contains(R, xR)
	assert.NotContains(R, xS)
	assert.NotContains(S, xR)
	assert.Contains(S, xS)
	// They should be equal to themselves
	assert.AreEqual(R, xR, xR)
	// Asking if they are equal to each other should generate an error
	_, err = R.AreEqual(xR, xS)
	assert.Error(err)
	// Using the wrong ring to ask for equality should generate an error
	_, err = S.AreEqual(xR, xR)
	assert.Error(err)
	// Add, Subtract, Multiply, Negate, Power, ScalarMultiplyByCoefficient,
	// ScalarMultiplyByInteger, Derivative, and NthDerivative should all
	// generate an error if we use the wrong parent
	_, err = R.Add(xR, xS)
	assert.Error(err)
	_, err = R.Add(xS, xR)
	assert.Error(err)
	_, err = R.Subtract(xR, xS)
	assert.Error(err)
	_, err = R.Subtract(xS, xR)
	assert.Error(err)
	_, err = R.Multiply(xR, xS)
	assert.Error(err)
	_, err = R.Multiply(xS, xR)
	assert.Error(err)
	_, err = R.Negate(xS)
	assert.Error(err)
	_, err = R.Power(xS, integer.FromInt(10))
	assert.Error(err)
	_, err = R.ScalarMultiplyByInteger(integer.FromInt(10), xS)
	assert.Error(err)
	_, err = R.ScalarMultiplyByCoefficient(integer.FromInt(10), xS)
	assert.Error(err)
	_, err = R.Derivative(xS, 0)
	assert.Error(err)
	_, err = R.NthDerivative(xS, 0, 10)
	assert.Error(err)
	// String on nil should be like "ZZ[]"
	var T *Parent
	assert.Equal(T.String(), T.CoefficientRing().String()+"[]")
	// Assign names to S
	S.AssignName("S")
	assert.NoError(S.AssignVariableName("s", 0))
	// Attempting to assign an out-of-range name should generate an error
	assert.Error(S.AssignVariableName("t", 1))
	// Recover the name
	if vars, err := polynomial.VariableNames(S); assert.NoError(err) && assert.Len(vars, 1) {
		assert.Equal(vars[0], "s")
	}
	assert.Equal(S.String(), "S[s]")
	// Attempting to assign a variable name to a nil Parent should generate an
	// error
	assert.Error(T.AssignVariableName("t", 0))
}

// TestToElement tests the ToElement method.
func TestToElement(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// We should be able to cast elements of the coefficient ring into R
	_, err := R.ToElement(R.CoefficientRing().One())
	assert.NoError(err)
	// Attempting to cast elements from other rings should fail
	_, err = R.ToElement(half)
	assert.Error(err)
	// Polynomials should be elements in their own ring
	_, err = R.ToElement(R.One())
	assert.NoError(err)
	// But polynomials from other rings should not be
	_, err = R.ToElement(NewUnivariateRing().One())
	assert.Error(err)
}

// TestToMonomial tests the ToMonomial method.
func TestToMonomial(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Fetch the default lattice of exponents
	if L, err := integervector.DefaultLatticeWithOrder(polynomial.Rank(R), ExponentMonoid(R).MonomialOrder()); assert.NoError(err) {
		// Create the unit element 1
		if val, err := R.ToMonomial(integervector.Zero(L)); assert.NoError(err) {
			assert.IsOneIn(R, val)
		}
		// Create the variable element x
		if m, err := integervector.FromIntSlice(L, []int{1}); assert.NoError(err) {
			if f, err := R.ToMonomial(m); assert.NoError(err) {
				if ok, err := R.IsNthVariable(f, 0); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// Fetch a lattice of the wrong dimension
	if L, err := integervector.DefaultLattice(polynomial.Rank(R) + 1); assert.NoError(err) {
		// This should return an error
		_, err := R.ToMonomial(integervector.Zero(L))
		assert.Error(err)
	}
}

// TestToTerm tests the ToTerm method.
func TestToTerm(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Fetch the default lattice of exponents
	if L, err := integervector.DefaultLatticeWithOrder(polynomial.Rank(R), ExponentMonoid(R).MonomialOrder()); assert.NoError(err) {
		// Create the zero element 0
		if val, err := R.ToTerm(integer.Zero(), integervector.Zero(L)); assert.NoError(err) {
			assert.IsZeroIn(R, val)
		}
		// Create the unit element 1
		if val, err := R.ToTerm(integer.One(), integervector.Zero(L)); assert.NoError(err) {
			assert.IsOneIn(R, val)
		}
		// Create the variable element x
		if m, err := integervector.FromIntSlice(L, []int{1}); assert.NoError(err) {
			if val, err := R.ToTerm(integer.One(), m); assert.NoError(err) {
				if ok, err := R.IsNthVariable(val, 0); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
		// Using a non-integer coefficient should return an error
		_, err = R.ToTerm(half, integervector.Zero(L))
		assert.Error(err)
	}
	// Fetch a lattice of the wrong dimension
	if L, err := integervector.DefaultLattice(polynomial.Rank(R) + 1); assert.NoError(err) {
		// This should return an error
		_, err = R.ToTerm(integer.Zero(), integervector.Zero(L))
		assert.Error(err)
	}
}

// TestIsConstant tests IsConstant.
func TestIsConstant(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	zZ := integer.Ring()
	// Constant polynomials should return true
	for i := -3; i <= 3; i++ {
		c := integer.FromInt(i)
		if x, err := R.ToTerm(c, integer.Zero()); assert.NoError(err) {
			if ok, val, err := R.IsConstant(x); assert.NoError(err) && assert.True(ok) {
				assert.AreEqual(zZ, val, c)
			}
		}
	}
	// Non-constant polynomials should return false
	for i := 1; i <= 3; i++ {
		c := integer.FromInt(i)
		if x, err := R.ToTerm(c, integer.One()); assert.NoError(err) {
			if ok, _, err := R.IsConstant(x); assert.NoError(err) {
				assert.False(ok)
			}
		}
	}
	// Non-polynomial values should return an error
	_, _, err := R.IsConstant(half)
	assert.Error(err)
}

// TestIsMonomial tests IsMonomial.
func TestIsMonomial(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Monomials should return true: x^i should return true
	for i := int64(0); i <= 3; i++ {
		if x, err := R.ToMonomial(integer.FromInt64(i)); assert.NoError(err) {
			if ok, err := R.IsTerm(x); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Non-monomials should return false: 2x^3 should return false
	if x, err := R.ToTerm(integer.FromInt(2), integer.FromInt(3)); assert.NoError(err) {
		if ok, err := R.IsMonomial(x); assert.NoError(err) {
			assert.False(ok)
		}
	}
	// x^2+x^4 should return false
	x := []int{0, 0, 1, 0, 1}
	if ok, err := R.IsMonomial(fromIntSlice(R, x)); assert.NoError(err) {
		assert.False(ok)
	}
	// Non-polynomial values should return an error
	_, err := R.IsMonomial(half)
	assert.Error(err)
}

// TestIsTerm tests IsTerm.
func TestIsTerm(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Terms should return true: i * x^i should return true
	for i := 0; i <= 3; i++ {
		c := integer.FromInt(i)
		if x, err := R.ToTerm(c, c); assert.NoError(err) {
			if ok, err := R.IsTerm(x); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Non-terms should return false: x+x^2 should return false
	x := []int{0, 1, 1}
	if ok, err := R.IsTerm(fromIntSlice(R, x)); assert.NoError(err) {
		assert.False(ok)
	}
	// Non-polynomial values should return an error
	_, err := R.IsTerm(half)
	assert.Error(err)
}

// TestIsVariable tests IsVariable.
func TestIsVariable(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The variable should return true
	if x, err := R.ToMonomial(integer.One()); assert.NoError(err) {
		if ok, err := R.IsNthVariable(x, 0); assert.NoError(err) {
			assert.True(ok)
		}
	}
	if x, err := R.NthVariable(0); assert.NoError(err) {
		if ok, err := R.IsNthVariable(x, 0); assert.NoError(err) {
			assert.True(ok)
		}
	}
	if xs := R.Variables(); assert.Len(xs, 1) {
		if ok, err := R.IsNthVariable(xs[0], 0); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// Non-variables should return false: x^2 should return false
	x := []int{0, 0, 1}
	if ok, err := R.IsNthVariable(fromIntSlice(R, x), 0); assert.NoError(err) {
		assert.False(ok)
	}
	// Non-polynomial values should return an error
	_, err := R.IsNthVariable(half, 0)
	assert.Error(err)
}

// TestIsMonic tests IsMonic.
func TestIsMonic(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The monic polynomials to test
	tests := [][]int{
		{1},
		{0, 2, 0, 0, 3, 1},
		{4, -2, 0, 1},
	}
	// Run the tests
	for _, test := range tests {
		if ok, err := R.IsMonic(fromIntSlice(R, test)); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// The non-monic polynomials to test
	tests = [][]int{
		{},
		{7},
		{0, 1, 0, 0, 4, 4},
		{1, -2, 0, 2},
	}
	// Run the tests
	for _, test := range tests {
		if ok, err := R.IsMonic(fromIntSlice(R, test)); assert.NoError(err) {
			assert.False(ok)
		}
	}
	// Non-polynomial values should return an error
	_, err := R.IsMonic(half)
	assert.Error(err)
}

// TestValuation tests Valuation.
func TestValuation(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{1},
		{3},
		{0, 2, 0, 0, 3, 1},
		{4, -2, 0, 1},
		{0, 0, 0, 1, -1},
	}
	valuations := []int64{0, 0, 1, 0, 3}
	// Run the tests
	for i, test := range tests {
		if m, err := R.Valuation(fromIntSlice(R, test)); assert.NoError(err) {
			if mm, ok := m.(*integervector.Element); assert.True(ok) && assert.Equal(mm.Dimension(), 1) {
				assert.IsEqualToInt64(mm.EntryOrPanic(0), valuations[i])
			}
		}
	}
	// The zero polynomial should return an error
	_, err := R.Valuation(R.Zero())
	assert.Error(err)
	// Non-polynomial values should return an error
	_, err = R.Valuation(half)
	assert.Error(err)
}

// TestDegree tests Degree
func TestDegree(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{1},
		{-3},
		{0, -1, 2, 0, 3, 8},
		{4, 0, 0, 1},
		{0, 0, 0, 1, -1},
	}
	degrees := []int64{0, 0, 5, 3, 4}
	// Run the tests
	for i, test := range tests {
		if d, err := R.Degree(fromIntSlice(R, test)); assert.NoError(err) {
			if dd, ok := d.(*integervector.Element); assert.True(ok) && assert.Equal(dd.Dimension(), 1) {
				assert.IsEqualToInt64(dd.EntryOrPanic(0), degrees[i])
			}
		}
	}
	// The zero polynomial should return an error
	_, err := R.Degree(R.Zero())
	assert.Error(err)
	// Non-polynomial values should return an error
	_, err = R.Degree(half)
	assert.Error(err)
}

// TestLeadingTerm tests LeadingMonomial, LeadingTerm, LeadingCoefficient, and ConstantCoefficient.
func TestLeadingTerm(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The polynomials to test
	tests := [][]int{
		{1},
		{-3},
		{0, -1, 2, 0, 3, 8},
		{4, 0, 0, 1},
		{0, 0, 0, 1, -1},
	}
	coeffs := []int{1, -3, 8, 1, -1}
	degrees := []int{0, 0, 5, 3, 4}
	consts := []int{1, -3, 0, 4, 0}
	// Run the tests
	for i, test := range tests {
		f := fromIntSlice(R, test)
		// Test LeadingMonomial
		if m, err := R.LeadingMonomial(f); assert.NoError(err) {
			if g, err := R.ToMonomial(integer.FromInt(degrees[i])); assert.NoError(err) {
				assert.AreEqual(R, m, g)
			}
		}
		// Test LeadingTerm
		if t, err := R.LeadingTerm(f); assert.NoError(err) {
			if g, err := R.ToTerm(integer.FromInt(coeffs[i]), integer.FromInt(degrees[i])); assert.NoError(err) {
				assert.AreEqual(R, t, g)
			}
		}
		// Test LeadingCoefficient
		if c, err := R.LeadingCoefficient(f); assert.NoError(err) {
			assert.AreEqual(integer.Ring(), c, integer.FromInt(coeffs[i]))
		}
		// Test ConstantCoefficient
		if c, err := R.ConstantCoefficient(f); assert.NoError(err) {
			assert.AreEqual(integer.Ring(), c, integer.FromInt(consts[i]))
		}
	}
	// Non-polynomial values should return an error
	_, err := R.LeadingMonomial(half)
	assert.Error(err)
	_, err = R.LeadingTerm(half)
	assert.Error(err)
	_, err = R.LeadingCoefficient(half)
	assert.Error(err)
	_, err = R.ConstantCoefficient(half)
	assert.Error(err)
}

// TestCoefficientsAndExponents tests Exponents, CoefficientsAndExponents, and Coefficient.
func TestCoefficientsAndExponents(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Fetch the default lattice
	L, err := integervector.DefaultLatticeWithOrder(polynomial.Rank(R), ExponentMonoid(R).MonomialOrder())
	require.NoError(t, err)
	// The polynomials to test
	tests := [][]int{
		{0},
		{1},
		{-3},
		{0, -1, 2, 0, 3, 8},
		{4, 0, 0, 1},
		{0, 0, 0, 1, -1},
	}
	// Run the tests
	for _, test := range tests {
		// Build the parallel slices of coefficients and exponents that we
		// expect
		actualCs := make([]*integer.Element, 0, len(test))
		actualEs := make([]*integervector.Element, 0, len(test))
		for i, c := range test {
			if c != 0 {
				actualCs = append(actualCs, integer.FromInt(c))
				m, err := integervector.FromIntSlice(L, []int{i})
				require.NoError(t, err)
				actualEs = append(actualEs, m)
			}
		}
		// Build the polynomial
		f := fromIntSlice(R, test)
		// Test Exponents
		if es, err := R.Exponents(f); assert.NoError(err) {
			assert.AreEqualSlices(L, es, actualEs)
		}
		// Test CoefficientsAndExponents
		if cs, es, err := R.CoefficientsAndExponents(f); assert.NoError(err) {
			assert.AreEqualSlices(L, es, actualEs)
			assert.AreEqualSlices(R.CoefficientRing(), cs, actualCs)
		}
		// Test Coefficient
		for i := -10; i <= 10; i++ {
			m, err := integervector.FromIntSlice(L, []int{i})
			require.NoError(t, err)
			if c, err := R.Coefficient(f, m); assert.NoError(err) {
				if i >= 0 && i < len(test) {
					assert.AreEqual(R.CoefficientRing(), c, integer.FromInt(test[i]))
				} else {
					assert.IsZeroIn(R.CoefficientRing(), c)
				}
			}
		}
	}
	// Non-polynomial values should return an error
	_, err = R.Exponents(half)
	assert.Error(err)
	_, _, err = R.CoefficientsAndExponents(half)
	assert.Error(err)
	_, err = R.Coefficient(half, integervector.Zero(L))
	assert.Error(err)
	// Coefficient for an exponent of the wrong dimension should return an error
	L, err = integervector.DefaultLattice(polynomial.Rank(R) + 1)
	require.NoError(t, err)
	_, err = R.Coefficient(One(R), integervector.Zero(L))
	assert.Error(err)
}

// TestZero tests the basic properties of the zero polynomial.
func TestZero(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	zero := R.Zero()
	// The zero element of R should be contained in R
	assert.Contains(R, zero)
	// The zero element should be zero
	assert.IsZeroIn(R, zero)
	// The zero element should be equal to itself
	assert.AreEqual(R, zero, R.Zero())
	// The zero element should not be one
	if ok, err := R.IsOne(zero); assert.NoError(err) {
		assert.False(ok)
	}
	assert.False(Zero(R).IsEqualTo(One(R)))
	assert.False(One(R).IsEqualTo(Zero(R)))
	assert.False(AreEqual(Zero(R), One(R)))
	assert.False(AreEqual(One(R), Zero(R)))
	// Non-integer values should return an error
	_, err := R.IsZero(half)
	assert.Error(err)
	// The zero element should be a constant, equal to zero
	if ok, val, err := R.IsConstant(zero); assert.NoError(err) && assert.True(ok) {
		assert.IsZeroIn(R.CoefficientRing(), val)
	}
	// The zero element should be a term
	if ok, err := R.IsTerm(zero); assert.NoError(err) {
		assert.True(ok)
	}
	// The zero element should not monic
	if ok, err := R.IsMonic(zero); assert.NoError(err) {
		assert.False(ok)
	}
	// Valuation should return an error
	_, err = R.Valuation(zero)
	assert.Error(err)
	// Degree should return an error
	_, err = R.Degree(zero)
	assert.Error(err)
	// IsMonomial should return false
	if ok, err := R.IsMonomial(zero); assert.NoError(err) {
		assert.False(ok)
	}
	// LeadingMonomial should return an error
	_, err = R.LeadingMonomial(zero)
	assert.Error(err)
	// LeadingTerm should be zero
	if val, err := R.LeadingTerm(zero); assert.NoError(err) {
		assert.AreEqual(R, val, zero)
	}
	// LeadingCoefficient should be 0
	if val, err := R.LeadingCoefficient(zero); assert.NoError(err) {
		assert.IsZeroIn(R.CoefficientRing(), val)
	}
	// ConstantCoefficient should be 0
	if val, err := R.ConstantCoefficient(zero); assert.NoError(err) {
		assert.IsZeroIn(R.CoefficientRing(), val)
	}
	// Exponents should return an empty slice
	if es, err := R.Exponents(zero); assert.NoError(err) {
		assert.Empty(es)
	}
	// Test Coefficient
	if L, err := integervector.DefaultLatticeWithOrder(polynomial.Rank(R), ExponentMonoid(R).MonomialOrder()); assert.NoError(err) {
		// Coefficient should return 0
		exps := []int{-4, -3, -2, -1, 0, 1, 2, 3, 4}
		for _, k := range exps {
			if m, err := integervector.FromIntSlice(L, []int{k}); assert.NoError(err) {
				if val, err := R.Coefficient(zero, m); assert.NoError(err) {
					assert.IsZeroIn(R.CoefficientRing(), val)
				}
			}
		}
	}
	// CoefficientsAndExponents should return two empty slices
	if cs, es, err := R.CoefficientsAndExponents(zero); assert.NoError(err) {
		assert.Empty(cs)
		assert.Empty(es)
	}
	// Add two copies of zero should be zero
	assert.AddIsEqualTo(R, zero, zero, zero)
	// Subtract two copies of zero should be zero
	assert.SubtractIsEqualTo(R, zero, zero, zero)
	// Negate zero should be zero
	assert.NegateIsEqualTo(R, zero, zero)
	// Multiply two copies of zero should be zero
	assert.MultiplyIsEqualTo(R, zero, zero, zero)
	// Scalar multiply by any non-zero value should return zero
	for k := -4; k <= 4; k++ {
		assert.ScalarMultiplyByIntegerIsEqualTo(R, integer.FromInt(k), zero, zero)
	}
	// Power by any positive integer value should return zero
	for k := 1; k <= 4; k++ {
		assert.PowerIsEqualTo(R, zero, integer.FromInt(k), zero)
	}
	// Power by zero should return one
	assert.PowerIsEqualTo(R, zero, integer.Zero(), R.One())
	// Power by a negative value should return an error
	_, err = R.Power(zero, integer.FromInt(-1))
	assert.Error(err)
	// ScalarMultiplyByCoefficient by a non-integer should return an error
	_, err = R.ScalarMultiplyByCoefficient(half, zero)
	assert.Error(err)
	// Derivative should be zero
	if val, err := R.Derivative(zero, 0); assert.NoError(err) {
		assert.IsZeroIn(R, val)
	}
	// NthDerivative should be zero for any non-negative value of n
	for n := 0; n <= 4; n++ {
		if val, err := R.NthDerivative(zero, 0, n); assert.NoError(err) {
			assert.IsZeroIn(R, val)
		}
	}
	// NthDerivative should return an error if n if negative
	_, err = R.NthDerivative(zero, 0, -1)
	assert.Error(err)
}

// TestOne tests the basic properties of the unit polynomial.
func TestOne(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	R := DefaultUnivariateRing()
	one := R.One()
	// The identity element of R should be contained in R
	assert.Contains(R, one)
	// The identity element should be one
	assert.IsOneIn(R, one)
	// The identity element should be equal to itself
	assert.AreEqual(R, one, R.One())
	// The identity element should not be zero
	if ok, err := R.IsZero(one); assert.NoError(err) {
		assert.False(ok)
	}
	// Non-integer values should return an error
	_, err := R.IsOne(half)
	assert.Error(err)
	// The identity element should be a constant, equal to one
	if ok, val, err := R.IsConstant(one); assert.NoError(err) && assert.True(ok) {
		assert.IsOneIn(R.CoefficientRing(), val)
	}
	// The identity element should be a monomial
	if ok, err := R.IsMonomial(one); assert.NoError(err) {
		assert.True(ok)
	}
	// The identity element should be a term
	if ok, err := R.IsTerm(one); assert.NoError(err) {
		assert.True(ok)
	}
	// The identity element should be monic
	if ok, err := R.IsMonic(one); assert.NoError(err) {
		assert.True(ok)
	}
	// Valuation should return the 1-dimensional zero vector
	if val, err := R.Valuation(one); assert.NoError(err) {
		if v, ok := val.(*integervector.Element); assert.True(ok) && assert.Equal(v.Dimension(), 1) {
			assert.IsZero(v.EntryOrPanic(0))
		}
	}
	// Degree should return 0
	if val, err := R.Degree(one); assert.NoError(err) {
		if v, ok := val.(*integervector.Element); assert.True(ok) {
			assert.IsZero(v)
		}
	}
	// LeadingMonomial should be one
	if val, err := R.LeadingMonomial(one); assert.NoError(err) {
		assert.IsOneIn(R, val)
	}
	// LeadingTerm should be one
	if val, err := R.LeadingTerm(one); assert.NoError(err) {
		assert.IsOneIn(R, val)
	}
	// LeadingCoefficient should be 1
	if val, err := R.LeadingCoefficient(one); assert.NoError(err) {
		assert.IsOneIn(R.CoefficientRing(), val)
	}
	// ConstantCoefficient should be 1
	if val, err := R.ConstantCoefficient(one); assert.NoError(err) {
		assert.IsOneIn(R.CoefficientRing(), val)
	}
	// Exponents should return a slice of length 1 containing the 1-dimensional
	// zero vector
	if es, err := R.Exponents(one); assert.NoError(err) && assert.Len(es, 1) {
		if m, ok := es[0].(*integervector.Element); assert.True(ok) && assert.Equal(m.Dimension(), 1) {
			assert.IsZero(m.EntryOrPanic(0))
		}
	}
	// Test Coefficient
	if L, err := integervector.DefaultLatticeWithOrder(polynomial.Rank(R), ExponentMonoid(R).MonomialOrder()); assert.NoError(err) {
		// Coefficient should return 0 for any non-zero degree
		for _, k := range []int{-4, -3, -2, -1, 1, 2, 3, 4} {
			if m, err := integervector.FromIntSlice(L, []int{k}); assert.NoError(err) {
				if val, err := R.Coefficient(one, m); assert.NoError(err) {
					assert.IsZeroIn(R.CoefficientRing(), val)
				}
			}
		}
		// Coefficient should return 1 for the degree-zero term
		if val, err := R.Coefficient(one, integervector.Zero(L)); assert.NoError(err) {
			assert.IsOneIn(R.CoefficientRing(), val)
		}
	}
	// CoefficientsAndExponents should return two slices of length 1, the first
	// equal to [1], and the second equal to [(0)].
	if cs, es, err := R.CoefficientsAndExponents(one); assert.NoError(err) && assert.Len(cs, 1) && assert.Len(es, 1) {
		if m, ok := es[0].(*integervector.Element); assert.True(ok) && assert.Equal(m.Dimension(), 1) {
			// Check that the coefficient is 1
			assert.IsOneIn(R.CoefficientRing(), cs[0])
			// Check that the exponent is the zero-vector
			assert.IsZero(m.EntryOrPanic(0))
		}
	}
	// Add one to zero should be one
	assert.AddIsEqualTo(R, one, R.Zero(), one)
	assert.AddIsEqualTo(R, R.Zero(), one, one)
	// Create two
	two, err := R.ToTerm(integer.FromInt(2), integer.Zero())
	require.NoError(err)
	// Add two copies of one should be two
	assert.AddIsEqualTo(R, one, one, two)
	// Subtract two copies of one should be zero
	assert.SubtractIsEqualTo(R, one, one, R.Zero())
	// Subtract one from zero should be -one
	assert.SubtractIsEqualTo(R, R.Zero(), one, One(R).Negate())
	// Subtract zero from one should be one
	assert.SubtractIsEqualTo(R, one, R.Zero(), one)
	// Subtract one from two should be one
	assert.SubtractIsEqualTo(R, two, one, one)
	// Multiply two copies of one should be one
	assert.MultiplyIsEqualTo(R, one, one, one)
	// Multiply one and zero should be zero
	assert.MultiplyIsEqualTo(R, one, R.Zero(), R.Zero())
	assert.MultiplyIsEqualTo(R, R.Zero(), one, R.Zero())
	// Multiply one and two should be two
	assert.MultiplyIsEqualTo(R, one, two, two)
	assert.MultiplyIsEqualTo(R, two, one, two)
	// Scalar multiply by zero should return zero
	assert.ScalarMultiplyByIntegerIsEqualTo(R, integer.Zero(), one, R.Zero())
	// Scalar multiply by one should return one
	assert.ScalarMultiplyByIntegerIsEqualTo(R, integer.One(), one, one)
	// Scalar multiply by two should return two
	assert.ScalarMultiplyByIntegerIsEqualTo(R, integer.FromInt(2), one, two)
	// Power by any non-negative integer value should return one
	for k := 0; k <= 4; k++ {
		assert.PowerIsEqualTo(R, one, integer.FromInt(k), one)
	}
	// Derivative should be zero
	if val, err := R.Derivative(one, 0); assert.NoError(err) {
		assert.IsZeroIn(R, val)
	}
	// NthDerivative should be zero for any positive value of n
	for n := 1; n <= 4; n++ {
		if val, err := R.NthDerivative(one, 0, n); assert.NoError(err) {
			assert.IsZeroIn(R, val)
		}
	}
	// NthDerivative should be one for n = 0
	if val, err := R.NthDerivative(one, 0, 0); assert.NoError(err) {
		assert.IsOneIn(R, val)
	}
}

// TestX3 tests the basic properties of the monomial x^3.
func TestX3(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	R := DefaultUnivariateRing()
	// Create the monomial x^3
	x3, err := R.ToMonomial(integer.FromInt(3))
	require.NoError(err)
	// This should be non-zero
	if ok, err := R.IsZero(x3); assert.NoError(err) {
		assert.False(ok)
	}
	assert.AreNotEqual(R, x3, Zero(R))
	assert.AreNotEqual(R, Zero(R), x3)
	// This should be non-one
	if ok, err := R.IsOne(x3); assert.NoError(err) {
		assert.False(ok)
	}
	assert.AreNotEqual(R, x3, One(R))
	assert.AreNotEqual(R, One(R), x3)
	// This should be different from -x^3
	if f, err := R.Negate(x3); assert.NoError(err) {
		assert.AreNotEqual(R, f, x3)
	}
	// This should be different from 2x^3
	if f, err := R.ScalarMultiplyByInteger(integer.FromInt(2), x3); assert.NoError(err) {
		assert.AreNotEqual(R, f, x3)
	}
	// This should be different from 1+x^3
	if f, err := R.Add(R.One(), x3); assert.NoError(err) {
		assert.AreNotEqual(R, f, x3)
	}
	if f, err := R.Add(x3, R.One()); assert.NoError(err) {
		assert.AreNotEqual(R, f, x3)
	}
	// This should be non-constant
	if ok, _, err := R.IsConstant(x3); assert.NoError(err) {
		assert.False(ok)
	}
	// This should be a monomial
	if ok, err := R.IsMonomial(x3); assert.NoError(err) {
		assert.True(ok)
	}
	// This should be a term
	if ok, err := R.IsTerm(x3); assert.NoError(err) {
		assert.True(ok)
	}
	// This should be monic
	if ok, err := R.IsMonic(x3); assert.NoError(err) {
		assert.True(ok)
	}
	// Valuation should be the 1-dimensional vector (3)
	if m, err := R.Valuation(x3); assert.NoError(err) {
		if mm, ok := m.(*integervector.Element); assert.True(ok) && assert.EqualValues(mm.Dimension(), 1) {
			assert.IsEqualToInt64(mm.EntryOrPanic(0), 3)
		}
	}
	// Degree should be 3
	if k, err := R.Degree(x3); assert.NoError(err) {
		if kk, ok := k.(*integervector.Element); assert.True(ok) && assert.Equal(kk.Dimension(), 1) {
			assert.IsEqualToInt64(kk.EntryOrPanic(0), 3)
		}
	}
	// LeadingMonomial should be x^3
	if val, err := R.LeadingMonomial(x3); assert.NoError(err) {
		assert.AreEqual(R, val, x3)
	}
	// LeadingTerm should be x^3
	if val, err := R.LeadingTerm(x3); assert.NoError(err) {
		assert.AreEqual(R, val, x3)
	}
	// LeadingCoefficient should be 1
	if val, err := R.LeadingCoefficient(x3); assert.NoError(err) {
		assert.IsOneIn(R.CoefficientRing(), val)
	}
	// ConstantCoefficient should be 0
	if val, err := R.ConstantCoefficient(x3); assert.NoError(err) {
		if ok, err := R.CoefficientRing().IsZero(val); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// The 0-th power of x^3 should be 1
	assert.PowerIsEqualTo(R, x3, integer.Zero(), R.One())
	// The k-th power of x^3 should be x^(3k)
	for k := 0; k <= 4; k++ {
		if val, err := R.Power(x3, integer.FromInt(k)); assert.NoError(err) {
			// Check the result is a monomial
			if ok, err := R.IsMonomial(val); assert.NoError(err) {
				assert.True(ok)
			}
			// Check the degree
			if d, err := R.Degree(val); assert.NoError(err) {
				if m, ok := d.(*integervector.Element); assert.True(ok) && assert.Equal(m.Dimension(), 1) {
					assert.IsEqualToInt64(m.EntryOrPanic(0), 3*int64(k))
				}
			}
			// Check equality directly
			if x3k, err := R.ToMonomial(integer.FromInt64(3 * int64(k))); assert.NoError(err) {
				assert.AreEqual(R, val, x3k)
			}
		}
	}
	// Derivative should be 3*x^2
	if val, err := R.Derivative(x3, 0); assert.NoError(err) {
		if dx3, err := R.ToTerm(integer.FromInt(3), integer.FromInt(2)); assert.NoError(err) {
			assert.AreEqual(R, val, dx3)
		}
	}
	// NthDerivative should be 6*x for n = 2
	if val, err := R.NthDerivative(x3, 0, 2); assert.NoError(err) {
		if d2x3, err := R.ToTerm(integer.FromInt(6), integer.FromInt(1)); assert.NoError(err) {
			assert.AreEqual(R, val, d2x3)
		}
	}
	// NthDerivative should be 6 for n = 3
	if val, err := R.NthDerivative(x3, 0, 3); assert.NoError(err) {
		if d3x3, err := R.ToTerm(integer.FromInt(6), integer.Zero()); assert.NoError(err) {
			assert.AreEqual(R, val, d3x3)
		}
	}
	// NthDerivative should be zero for any n > 3
	for n := 4; n <= 6; n++ {
		if val, err := R.NthDerivative(x3, 0, n); assert.NoError(err) {
			assert.IsZeroIn(R, val)
		}
	}
	// Check printing of x^3
	if str, err := R.VariableName(0); assert.NoError(err) {
		assert.Equal(x3.String(), str+"^3")
	}
}
