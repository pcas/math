// Tests taking powers of univariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"math"
	"testing"
)

// testPowerArgs provides a way of encoding test input/output for Power.
type testPowerArgs struct {
	arg    []int64 // A slice of coefficients describing the input polynomial
	k      int64   // The power
	result []int64 // A slice of coefficients describing the output polynomial

}

/////////////////////////////////////////////////////////////////////////
// testPowerArgs functions
/////////////////////////////////////////////////////////////////////////

// Arg returns the input polynomial in the polynomial ring R.
func (t *testPowerArgs) Arg(R *Parent) *Element {
	f, err := FromInt64Slice(R, 0, 1, t.arg)
	if err != nil {
		panic(err)
	}
	return f
}

// Result returns the expected output polynomial in the polynomial ring R.
func (t *testPowerArgs) Result(R *Parent) *Element {
	f, err := FromInt64Slice(R, 0, 1, t.result)
	if err != nil {
		panic(err)
	}
	return f
}

// Test returns true iff the output polynomial agrees with the expected result. This uses the Power method to perform the calculation.
func (t *testPowerArgs) Test(a *assert.Assertions) bool {
	R := DefaultUnivariateRing()
	return a.PowerIsEqualTo(R, t.Arg(R), integer.FromInt64(t.k), t.Result(R))
}

// TestInt64 returns true iff the output polynomial agrees with the expected result. This uses the PowerInt64 method to perform the calculation.
func (t *testPowerArgs) TestInt64(a *assert.Assertions) bool {
	R := DefaultUnivariateRing()
	in := t.Arg(R)
	f, err := in.PowerInt64(t.k)
	if !a.NoError(err, "Test: PowerInt64(%s,%d)\n", in.String(), t.k) {
		return false
	}
	out := t.Result(R)
	return a.AreEqual(R, out, f)
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestPower tests taking powers of polynomials.
func TestPower(t *testing.T) {
	assert := assert.New(t)
	// Create the table of inputs and outputs
	table := []*testPowerArgs{
		&testPowerArgs{
			// (5)^5 = 3125
			arg:    []int64{5},
			k:      5,
			result: []int64{3125},
		},
		&testPowerArgs{
			// (1 + x)^5 = 1 + 5x + 10x^2 + 10x^3 + 5x^4 + x^5
			arg:    []int64{1, 1},
			k:      5,
			result: []int64{1, 5, 10, 10, 5, 1},
		},
		&testPowerArgs{
			// (x + 2x^4)^3 = x^3 + 6x^6 + 12x^9 + 8x^12
			arg:    []int64{0, 1, 0, 0, 2},
			k:      3,
			result: []int64{0, 0, 0, 1, 0, 0, 6, 0, 0, 12, 0, 0, 8},
		},
		&testPowerArgs{
			// (1 + x + x^2)^4 = 1 + 4x + 10x^2 + 16x^3 + 19x^4 + 16x^5 + 10x^6 + 4x^7 + x^8
			arg:    []int64{1, 1, 1},
			k:      4,
			result: []int64{1, 4, 10, 16, 19, 16, 10, 4, 1},
		},
		&testPowerArgs{
			// (x + 2x^2 + 3x^3)^3 = x^3 + 6x^4 + 21x^5 + 44x^6 + 63x^7 + 54x^8 + 27x^9
			arg:    []int64{0, 1, 2, 3},
			k:      3,
			result: []int64{0, 0, 0, 1, 6, 21, 44, 63, 54, 27},
		},
		&testPowerArgs{
			// (1 - x)^9 = 1 - 9x + 36x^2 - 84x^3 + 126x^4 - 126x^5 + 84x^6 - 36x^7 + 9x^8 - x^9
			arg:    []int64{1, -1},
			k:      9,
			result: []int64{1, -9, 36, -84, 126, -126, 84, -36, 9, -1},
		},
		&testPowerArgs{
			// (1 + 2x - x^2 + x^3 - 2x^4 + x^5)^2 = 1 + 4x + 2x^2 - 2x^3 + x^4 - 8x^5 + 9x^6 - 6x^7 + 6x^8 - 4x^9 + x^10
			arg:    []int64{1, 2, -1, 1, -2, 1},
			k:      2,
			result: []int64{1, 4, 2, -2, 1, -8, 9, -6, 6, -4, 1},
		},
		&testPowerArgs{
			// (2x^2)^5 = 32x^10
			arg:    []int64{0, 0, 2},
			k:      5,
			result: []int64{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32},
		},
		&testPowerArgs{
			// (1 + 2x - 2x^2)^2 = 1 + 4x - 8x^3 + 4x^4
			arg:    []int64{1, 2, -2},
			k:      2,
			result: []int64{1, 4, 0, -8, 4},
		},
	}
	// Run the tests
	for _, test := range table {
		test.Test(assert)
		test.TestInt64(assert)
	}
}

// TestPowerEdgeCases tests some edge cases when taking powers of polynomials.
func TestPowerEdgeCases(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Create the polynomial 1 + x^10
	f := fromIntSlice(R, []int{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1})
	// Taking a negative power should error
	_, err := f.PowerInt64(-1)
	assert.Error(err)
	_, err = f.Power(integer.FromInt(-1))
	assert.Error(err)
	// Taking a power of 0 should return 1
	if g, err := f.PowerInt64(0); assert.NoError(err) {
		assert.IsOne(g)
	}
	if g, err := f.Power(integer.Zero()); assert.NoError(err) {
		assert.IsOne(g)
	}
	// Taking a power of 0 for the nil object should return 1
	var h *Element
	if g, err := h.PowerInt64(0); assert.NoError(err) {
		assert.IsOne(g)
	}
	if g, err := h.Power(integer.Zero()); assert.NoError(err) {
		assert.IsOne(g)
	}
	// Taking a power of 1 should return f
	if g, err := f.PowerInt64(1); assert.NoError(err) {
		assert.AreEqual(R, g, f)
	}
	if g, err := f.Power(integer.One()); assert.NoError(err) {
		assert.AreEqual(R, g, f)
	}
	// Zero to any positive power should be zero
	if g, err := Zero(R).PowerInt64(17); assert.NoError(err) {
		assert.IsZero(g)
	}
	if g, err := Zero(R).Power(integer.FromInt(17)); assert.NoError(err) {
		assert.IsZero(g)
	}
	// Taking too-large powers should error
	if powTwo, err := integer.FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = f.Power(powTwo)
		assert.Error(err)
	}
	// If the degree grows too large this should error
	_, err = f.PowerInt64(math.MaxInt64/10 + 1)
	assert.Error(err)
	_, err = f.Power(integer.FromInt64(math.MaxInt64/10 + 1))
	assert.Error(err)
}
