// Tests of element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcas/math/vector/integervector"
	"github.com/stretchr/testify/require"
	"math/rand"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// fromIntSlice is a wrapper around FromIntSlice which panics on error.
func fromIntSlice(R *Parent, cs []int) *Element {
	f, err := FromIntSlice(R, 0, 1, cs)
	if err != nil {
		panic(err)
	}
	return f
}

// randIntSlice returns a slice of length d of randomly chosen ints in the range [-n, ..., n].
func randIntSlice(d int, n int) []int {
	if d < 0 {
		panic("Length must be non-negative.")
	} else if n < 0 {
		n = -n
	}
	S := make([]int, 0, d+1)
	for i := 0; i <= d; i++ {
		s := rand.Intn(n)
		if rand.Int()%2 == 0 {
			s = -s
		}
		S = append(S, s)
	}
	return S
}

// toExponentInt64 converts the given degree d to an exponent (d) in the exponent monoid of R. If R is not of rank 1, this will panic.
func toExponentInt64(R *Parent, d int64) *integervector.Element {
	return toExponent(R, integer.FromInt64(d))
}

// toExponent converts the given degree d to an exponent (d) in the exponent monoid of R. If R is not of rank 1, this will panic.
func toExponent(R *Parent, d *integer.Element) *integervector.Element {
	m, err := integervector.ChangeParent(ExponentMonoid(R), d)
	if err != nil {
		panic(err)
	}
	return m
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestElementToMonomial tests ToMonomial and ToMonomialInt64.
func TestElementToMonomial(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// We test a few edge cases
	// x^0 = 1
	if m, err := ToMonomial(R, toExponentInt64(R, 0)); assert.NoError(err) {
		assert.IsOne(m)
	}
	// x^1 = x
	if m, err := ToMonomial(R, toExponentInt64(R, 1)); assert.NoError(err) {
		if x, err := R.NthVariable(0); assert.NoError(err) {
			assert.AreEqual(R, m, x)
		}
	}
	// Test a more meaty example
	f := fromIntSlice(R, []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 1})
	if m, err := ToMonomial(R, toExponentInt64(R, 9)); assert.NoError(err) {
		assert.AreEqual(R, m, f)
	}
	// Taking a negative exponent should return an error
	// THINK
	//	_, err := ToMonomial(R, toExponentInt64(R, -7))
	//	assert.Error(err)
	// Taking a too-large exponent should return an error
	if powTwo, err := integer.FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = ToMonomial(R, toExponent(R, powTwo))
		assert.Error(err)
	}
}

// TestElementToTerm tests ToTerm and ToTermInt64.
func TestElementToTerm(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// We test a few edge cases
	// 0 * x^0 = 0
	if m, err := ToTerm(R, integer.Zero(), toExponentInt64(R, 0)); assert.NoError(err) {
		assert.IsZero(m)
	}
	// 0 * x^1 = 0
	if m, err := ToTerm(R, integer.Zero(), toExponentInt64(R, 1)); assert.NoError(err) {
		assert.IsZero(m)
	}
	// 1 * x^0 = 1
	if m, err := ToTerm(R, integer.One(), toExponentInt64(R, 0)); assert.NoError(err) {
		assert.IsOne(m)
	}
	// 1 * x^1 = x
	if m, err := ToTerm(R, integer.One(), toExponentInt64(R, 1)); assert.NoError(err) {
		if x, err := R.NthVariable(0); assert.NoError(err) {
			assert.AreEqual(R, m, x)
		}
	}
	// Test a more meaty example
	f := fromIntSlice(R, []int{0, 0, 0, 0, 0, 0, -10})
	if m, err := ToTerm(R, integer.FromInt(-10), toExponentInt64(R, 6)); assert.NoError(err) {
		assert.AreEqual(R, m, f)
	}
	// Taking a negative exponent should return an error
	// THINK
	//	_, err := ToTerm(R, integer.One(), toExponentInt64(R, -10))
	//	assert.Error(err)
	// Taking a too-large exponent should return an error
	if powTwo, err := integer.FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = ToTerm(R, integer.One(), toExponent(R, powTwo))
		assert.Error(err)
	}
}

// TestRank tests Rank.
func TestRank(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(polynomial.Rank(DefaultUnivariateRing()), 1)
}

// TestScalarMultiplyByInteger tests ScalarMultiplyByInteger, ScalarMultiplyByInt64, and ScalarMultiplyByUint64.
func TestScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Scalar multiples of zero are always zero
	f := Zero(R)
	for a := -10; a <= 10; a++ {
		assert.IsZero(f.ScalarMultiplyByInteger(integer.FromInt(a)))
		assert.IsZero(f.ScalarMultiplyByInt64(int64(a)))
	}
	// Create a test polynomial
	cs := []int{1, 0, 0, -3, 8}
	f = fromIntSlice(R, cs)
	// Check the scalar multiples are as expected
	for a := -10; a <= 10; a++ {
		// Create the expected polynomial
		expectedCs := make([]int, 0, len(cs))
		for _, c := range cs {
			expectedCs = append(expectedCs, a*c)
		}
		g := fromIntSlice(R, expectedCs)
		// Check the result
		assert.AreEqual(R, f.ScalarMultiplyByInteger(integer.FromInt(a)), g)
		assert.AreEqual(R, f.ScalarMultiplyByInt64(int64(a)), g)
	}
	// Create a random polynomial of high degree and compute its scalar multiple
	cs = randIntSlice(50000, 100)
	f = fromIntSlice(R, cs).ScalarMultiplyByInt64(17)
	for i, c := range cs {
		cs[i] = 17 * c
	}
	assert.AreEqual(R, f, fromIntSlice(R, cs))
}

// TestScalarMultiplyByCoefficient tests ScalarMultiplyByCoefficient.
func TestScalarMultiplyByCoefficient(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Scalar multiples of the zero polynomial are always zero
	f := Zero(R)
	var h *Element
	for a := -10; a <= 10; a++ {
		if g, err := f.ScalarMultiplyByCoefficient(integer.FromInt(a)); assert.NoError(err) {
			assert.IsZero(g)
		}
		if g, err := h.ScalarMultiplyByCoefficient(integer.FromInt(a)); assert.NoError(err) {
			assert.IsZero(g)
			assert.AreEqual(h.Parent(), h, g)
		}
	}
	// Create a test polynomial
	cs := []int{3, 4, 5, 6}
	f = fromIntSlice(R, cs)
	// Multiplying f by zero is the zero polynomial
	if g, err := f.ScalarMultiplyByCoefficient(integer.Zero()); assert.NoError(err) {
		assert.IsZero(g)
	}
	// Multiplying f by one is f
	if g, err := f.ScalarMultiplyByCoefficient(integer.One()); assert.NoError(err) {
		assert.AreEqual(R, f, g)
	}
	// Check the scalar multiples are as expected
	for a := -10; a <= 10; a++ {
		// Create the expected polynomial
		expectedCs := make([]int, 0, len(cs))
		for _, c := range cs {
			expectedCs = append(expectedCs, a*c)
		}
		h = fromIntSlice(R, expectedCs)
		// Check the result
		if g, err := f.ScalarMultiplyByCoefficient(integer.FromInt(a)); assert.NoError(err) {
			assert.AreEqual(R, h, g)
		}
	}
	// Create a random polynomial of high degree and compute its scalar multiple
	cs = randIntSlice(50000, 100)
	if f, err := fromIntSlice(R, cs).ScalarMultiplyByCoefficient(integer.FromInt(17)); assert.NoError(err) {
		for i, c := range cs {
			cs[i] = 17 * c
		}
		assert.AreEqual(R, f, fromIntSlice(R, cs))
	}
}

// TestDerivative tests Derivative and NthDerivative.
func TestDerivative(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// Create a table of derivatives
	derivatives := [][]int{
		{1, 0, 0, -2, -2, 0, 1}, // x^6 - 2*x^4 - 2*x^3 + 1
		{0, 0, -6, -8, 0, 6},    // 6*x^5 - 8*x^3 - 6*x^2
		{0, -12, -24, 0, 30},    // 30*x^4 - 24*x^2 - 12*x
		{-12, -48, 0, 120},      // 120*x^3 - 48*x - 12
		{-48, 0, 360},           // 360*x^2 - 48
		{0, 720},                // 720*x
		{720},                   // 720
		{0},                     // 0
		{0},                     // 0
	}
	// Convert these to polynomials
	fs := make([]*Element, 0, len(derivatives))
	for _, cs := range derivatives {
		fs = append(fs, fromIntSlice(R, cs))
	}
	// Run the tests
	for i := 0; i < len(fs)-1; i++ {
		// Test Derivative
		if df, err := fs[i].Derivative(0); assert.NoError(err) {
			assert.AreEqual(R, df, fs[i+1])
		}
		// Test NthDerivative
		for j := i; j < len(fs); j++ {
			if g, err := fs[i].NthDerivative(0, j-i); assert.NoError(err) {
				assert.AreEqual(R, g, fs[j])
			}
		}
	}
}

// TestHash tests Hash.
func TestHash(t *testing.T) {
	assert := assert.New(t)
	R := DefaultUnivariateRing()
	// The nil polynomial should return the same hash as the zero polynomial
	var f *Element
	assert.Equal(f.Hash(), Zero(R).Hash())
	// The hashes of equal objects should be equal
	x, err := NthVariable(R, 0)
	require.NoError(t, err)
	for i := 1; i < 10; i++ {
		f = fromIntSlice(R, []int{0, i})
		if cx := x.ScalarMultiplyByInt64(int64(i)); assert.AreEqual(R, f, cx) {
			assert.Equal(f.Hash(), cx.Hash())
		}
	}
}

// TestString tests String.
func TestString(t *testing.T) {
	assert := assert.New(t)
	R := NewUnivariateRing()
	require.NoError(t, R.AssignVariableName("x", 0))
	// The test cases and expected results
	tests := [][]int{
		{0},
		{1},
		{-1},
		{0, 1},
		{0, -1},
		{1, 0, 2},
		{1, 0, -2},
		{-1, 0, 2},
		{-1, 0, -2},
	}
	strings := []string{
		"0",
		"1",
		"-1",
		"x",
		"-x",
		"2*x^2 + 1",
		"-2*x^2 + 1",
		"2*x^2 - 1",
		"-2*x^2 - 1",
	}
	// Run the tests
	for i, test := range tests {
		assert.Equal(fromIntSlice(R, test).String(), strings[i])
	}
	// The nil polynomial should return zero
	var f *Element
	assert.Equal(f.String(), "0")
}
