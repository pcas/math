// Encoding contains functions for encoding/decoding a polynomial with integer coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerpolynomial

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/integercoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents"
	"bitbucket.org/pcastools/gobutil"
)

// encodingVersion is the internal version number. This will permit backwards-compatible changes to the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (f *Element) GobEncode() ([]byte, error) {
	// Deal with the nil case
	if f == nil {
		return Zero(nil).GobEncode()
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the slice of exponents
	if err := enc.Encode(f.es); err != nil {
		return nil, err
	}
	// Add the slice of coefficients
	if err := enc.Encode(f.cs); err != nil {
		return nil, err
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface. Important: Take great care that you are decoding into a new *Element; the safe way to do this is to use the GobDecode(dec *gob.Decode) function. The decoded polynomial will lie in the default parent.
func (f *Element) GobDecode(buf []byte) error {
	// Sanity check
	if f == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if f.parent != nil {
		return gobutil.DecodingIntoExistingObject.New()
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the slice of exponents
	es := &integerexponents.Slice{}
	if err := dec.Decode(es); err != nil {
		return err
	}
	// Read in the slice of coefficients
	cs, err := integer.GobDecodeSlice(dec)
	if err != nil {
		return err
	}
	// Sanity check on the lengths of the slices
	if es.Len() != len(cs) {
		return errors.SliceLengthNotEqual.New()
	}
	// Sanity check on the exponents
	if !exponents.IsSorted(es) {
		return errors.CorruptEncoding.New()
	}
	// Create the parent
	L := es.Universe()
	R, err := DefaultRing(L.Rank(), L.MonomialOrder())
	if err != nil {
		return err
	} else if R.ExponentMonoid() != L {
		return errors.ParentsDoNotAgree.New()
	}
	// Set the values
	f.parent = R
	f.es = es
	f.cs = integercoefficients.Slice(cs)
	return nil
}
