// Polynomial defines the interface satisfied by a polynomial ring.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package polynomial

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/product"
	"bitbucket.org/pcas/math/ring"
)

// ExponentMonoid defines the interface satisfied by the abelian monoid containing the exponents.
type ExponentMonoid interface {
	monomialorder.Interface
	monomialorder.MonomialOrderer
	product.Interface
}

// Interface defines the interface that all polynomial rings must satisfy.
type Interface interface {
	ring.Interface
	CoefficientRing() ring.Interface                                                        // CoefficientRing returns the ring containing the coefficients.
	ExponentMonoid() ExponentMonoid                                                         // ExponentMonoid returns the abelian monoid containing the exponents.
	CmpLeadingMonomial(f object.Element, g object.Element) (int, error)                     // CmpLeadingMonomial returns -1 if LM(f) < LM(g), 0 if LM(f) = LM(g), and +1 if LM(f) > LM(g), were LM denotes the leading monomial, and f and g are non-zero polynomials.
	ScalarMultiplyByCoefficient(c object.Element, f object.Element) (object.Element, error) // ScalarMultiplyByCoefficient returns the product c * f of the polynomial f with the element c in the coefficient ring.
	Degree(f object.Element) (object.Element, error)                                        // Degree returns the degree of the non-zero polynomial f. The returned exponent is an element in the exponent monoid.
	Valuation(f object.Element) (object.Element, error)                                     // Valuation returns the exponent of the smallest non-zero term of the non-zero polynomial f. The returned exponent is an element in the exponent monoid.
	Exponents(f object.Element) ([]object.Element, error)                                   // Exponents returns a slice of exponents of the polynomial f. Only the exponents corresponding to terms with non-zero coefficient will be returned, hence the zero polynomial returns an empty slice. The returned exponents will live in the exponent monoid. The exponents are sorted in increasing order.
	Coefficient(f object.Element, n object.Element) (object.Element, error)                 // Coefficient returns the coefficient of the multi-degree n term of the polynomial f.
	ToMonomial(m object.Element) (object.Element, error)                                    // ToMonomial returns the monomial x^m, where m is an element in the monoid of exponents.
	ToTerm(c object.Element, m object.Element) (object.Element, error)                      // ToTerm returns the term c * x^m, where c is an element in the coefficient ring, and m is an element in the monoid of exponents.
	AssignName(name string)                                                                 // AssignName assigns a name the ring to be used in printing.
	AssignVariableName(name string, i int) error                                            // AssignVariableName assigns a name to the i-th variable of the ring to be used in printing. The variables are indexed from 0 up to rank - 1 (inclusive).
	VariableName(i int) (string, error)                                                     // VariableName returns the name to be used in printing of the i-th variable of the ring. The variables are indexed from 0 up to rank - 1 (inclusive).
}
