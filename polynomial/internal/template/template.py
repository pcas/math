#!/usr/bin/python

### This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see http://creativecommons.org/publicdomain/zero/1.0/

import sys
import os
from tempfile import mkstemp
from shutil import move

# Output the usage message to stderr, then exits with a non-zero exit code.
def helpmessage():
    name = sys.argv[0]
    sys.stderr.write("""Usage: """ + name + """ template_path dest_path var1=value1 [... varN=valueN]
""")
    sys.exit(1)

# Outputs the given message to stderr, then exits with a non-zero exit code.
def fatalmessage(s):
    name = sys.argv[0]
    sys.stderr.write(name + ': ' + s + '\n')
    sys.exit(1)

# Validate the argument
args = sys.argv
if len(args) < 4:
    helpmessage()
templatepath = args[1].strip()
if len(templatepath) == 0 or templatepath[0] == '-':
    helpmessage()
destpath = args[2].strip()
if len(destpath) == 0 or destpath[0] == '-':
    helpmessage()

# Extract the patterns to find and replace
subs = {}
for i in range(3,len(args)):
    S=args[i].split('=')
    if len(S) != 2:
        fatalmessage('Invalid key=value pair: '+args[i])
    S[0]=S[0].strip()
    S[1]=S[1].strip()
    if len(S[0]) == 0 or len(S[1]) == 0:
        fatalmessage('Invalid key=value pair: '+args[i])
    subs['{{'+S[0]+'}}']=S[1]

# Create a temporary file
fh, tmppath = mkstemp()

# Iterates over the lines in the file templatepath and apply the substitutions
with open(tmppath,'w') as newfile:
    with open(templatepath) as templatefile:
        for line in templatefile:
            for key,value in subs.iteritems():
                line = line.replace(key,value)
            # Write out the result to the temporary file
            newfile.write(line)

# Close the temporary file (this is necessary to force a flush)
os.close(fh)

# Move the temporary file to the destination
move(tmppath, destpath)

# Run go fmt on the destination
os.system('go fmt "' + destpath + '" &>/dev/null' )
