// Encoding contains functions for encoding/decoding a slice of coefficients in a finite field.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package finitefieldcoefficients

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield"
	"bitbucket.org/pcastools/gobutil"
)

// The internal version number.This will permit backwards-compatible changes to
// the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (s *Slice) GobEncode() ([]byte, error) {
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the order of the coefficient ring
	if err := enc.Encode(s.K.OrderInt64()); err != nil {
		return nil, err
	}
	// Add the slice of coefficients
	if err := enc.Encode(s.Cs); err != nil {
		return nil, err
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (s *Slice) GobDecode(buf []byte) error {
	// Sanity check
	if s == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if s.K != nil {
		return gobutil.DecodingIntoExistingObject.New()
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read the order of the coefficient ring
	var n int64
	if err := dec.Decode(&n); err != nil {
		return err
	}
	// Read in the slice of coefficients
	cs, err := finitefield.GobDecodeSlice(dec)
	if err != nil {
		return err
	}
	// Fetch the coefficient ring
	K, err := finitefield.GF(n)
	if err != nil {
		return err
	}
	// Sanity check on the parent of the elements in the coefficients slice
	for _, x := range cs {
		if !K.Contains(x) {
			return errors.ParentsDoNotAgree.New()
		}
	}
	// Set the values
	s.K = K
	s.Cs = cs
	return nil
}
