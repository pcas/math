// Finitefieldcoefficients describes a slice of coefficients taken from a common finite field.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package finitefieldcoefficients

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/integercoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/rationalcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
)

// Slice describes a slice of *finitefield.Elements with common parent.
type Slice struct {
	k  *finitefield.Parent    // The common parent
	cs []*finitefield.Element // The slice of elements
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// newSlice returns a new slice with common parent k and given capacity.
func newSlice(k *finitefield.Parent, capacity int) {
	if capacity < 0 {
		capacity = 0
	}
	return &Slice{
		k:  k,
		cs: make([]*finitefield.Element, 0, capacity),
	}
}

// appendSlice appends the element of S to the end of the Slice s. Returns the updated Slice on success.
func appendSlice(s *Slice, S slice.Interface) (*Slice, error) {
	k := s.k
	cs := s.cs
	switch T := S.(type) {
	case *Slice:
		if T.k == k {
			cs = append(cs, T.cs...)
		} else {
			return nil, errors.ParentsDoNotAgree.New()
		}
	case finitefield.Slice:
		for _, x := range T {
			if x.Parent() != k {
				return nil, errors.ParentsDoNotAgree.New()
			}
			cs = append(cs, x)
		}
	case integercoefficients.Slice:
		for _, x := range T {
			cs = append(cs, finitefield.FromInteger(k, x))
		}
	case integer.Slice:
		for _, x := range T {
			cs = append(cs, finitefield.FromInteger(k, x))
		}
	case rationalcoefficients.Slice:
		for _, x := range T {
			y, err := finitefield.FromRational(k, x)
			if err != nil {
				return nil, err
			}
			cs = append(cs, y)
		}
	case rational.Slice:
		for _, x := range T {
			y, err := finitefield.FromRational(k, x)
			if err != nil {
				return nil, err
			}
			cs = append(cs, y)
		}
	case slice.ElementSlice:
		for _, x := range T {
			y, err := finitefield.ToElement(k, x)
			if err != nil {
				return nil, err
			}
			cs = append(cs, y)
		}
	default:
		n := T.Len()
		for i := 0; i < n; i++ {
			y, err := finitefield.ToElement(k, T.Entry(i))
			if err != nil {
				return nil, err
			}
			cs = append(cs, y)
		}
	}
	// Return the new slice
	return &Slice{
		k:  k,
		cs: cs,
	}, nil
}

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent ring common to the elements in the slice.
func (s *Slice) Universe() ring.Interface {
	return s.k
}

// Len returns the length of the slice of coefficients.
func (s *Slice) Len() int {
	return len(s.cs)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (s *Slice) Entry(i int) object.Element {
	return s.cs[i]
}

// Append appends the element x to the end of the coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s *Slice) Append(x object.Element) unwrappedcoefficients.Interface {
	y, err := finitefield.ToElement(s.k, x)
	if err != nil {
		panic(err)
	}
	s.cs = append(s.cs, y)
	return s
}

// AppendSlice appends the element of S to the end of the coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic.
func (s *Slice) AppendSlice(S slice.Interface) unwrappedcoefficients.Interface {
	T, err := appendSlice(s, S)
	if err != nil {
		panic(err)
	}
	return T
}

// Set sets the entry at index idx to x. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, or if the index is invalid, this will panic.
func (s *Slice) Set(x object.Element, idx int) unwrappedcoefficients.Interface {
	y, err := finitefield.ToElement(s.k, x)
	if err != nil {
		panic(err)
	}
	s.cs[idx] = y
	return s
}

// Insert inserts the element x at index idx. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic.
func (s *Slice) Insert(x object.Element, idx int) unwrappedcoefficients.Interface {
	n := s.Len()
	if idx == n {
		return s.Append(x)
	}
	y, err := finitefield.ToElement(s.k, x)
	if err != nil {
		panic(err)
	}
	cs := make([]*finitefield.Element, 0, n+1)
	s.cs = append(append(append(cs, s.cs[:idx]...), y), s.cs[idx:]...)
	return s
}

// Remove removes the element at index idx. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid.
func (s *Slice) Remove(idx int) unwrappedcoefficients.Interface {
	n := s.Len()
	if idx == 0 {
		return s.Slice(1, n).(unwrappedcoefficients.Interface)
	} else if idx == n-1 {
		return s.Slice(0, n-1).(unwrappedcoefficients.Interface)
	}
	cs := make([]*finitefield.Element, 0, n-1)
	s.cs = append(append(es, s.cs[:idx]...), s.cs[idx+1:]...)
	return s
}

// Swap exchanges the i-th and j-th elements. This will panic if i or j is out of range.
func (s *Slice) Swap(i, j int) {
	s.cs[i], s.cs[j] = s.cs[j], s.cs[i]
}

// Slice returns a subslice of the coefficients, starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s *Slice) Slice(k int, m int) slice.Interface {
	return &Slice{
		k:  s.k,
		cs: s.cs[k:m],
	}
}

// ToElementSlice returns a copy of the slice as an []object.Element.
func (s *Slice) ToElementSlice() []object.Element {
	n := s.Len()
	T := make([]object.Element, 0, n)
	if n == 0 {
		return T
	}
	for _, x := range s.cs {
		T = append(T, x)
	}
	return T
}

// Unwrap returns s as an unwrappedcoefficients.Interface object.
func (s *Slice) Unwrap() unwrappedcoefficients.Interface {
	return s
}

// Copy returns a copy of this slice (which will be of the same underlying type).
func (s *Slice) Copy() unwrappedcoefficients.Interface {
	return &Slice{
		k:  s.k,
		cs: finitefield.CopySlice(s.cs),
	}
}

// Hash returns a hash value for this slice.
func (s *Slice) Hash() hash.Value {
	if s.Len() == 0 {
		return 0
	}
	v := hash.NewSequenceHasher()
	defer hash.ReuseSequenceHasher(v)
	for _, x := range s.cs {
		v.Add(x)
	}
	return v.Hash()
}

// PrettyPrint returns a string str and int sgn for the i-th element in this slice. Here str is the string representation of the i-th element in the slice, and sgn is 0 if the element is 0, and 1 otherwise. This will panic if i is out of range.
func (s *Slice) PrettyPrint(i int) (string, int) {
	x := s.cs[i]
	if x.IsZero() {
		return x.String(), 0
	}
	return x.String(), 1
}
