// Helper simplifies working with a slice of coefficients taken from a finite field.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package finitefieldcoefficients

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
)

// helper is the helper for integer coefficients.
type helper struct {
	k *finitefield.Parent // The finite field
}

/////////////////////////////////////////////////////////////////////////
// Local function
/////////////////////////////////////////////////////////////////////////

// commonParent returns the common parent.
func commonParent(h *helper) *finitefield.Parent {
	if h == nil {
		k, err := finitefield.GF(1)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return k
	}
	return h.k
}

/////////////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the user-facing parent of the coefficients.
func (h *helper) Universe() ring.Interface {
	return commonParent(h)
}

// New returns a new slice of unwrapped coefficients with given capacity.
func (h *helper) New(capacity int) unwrappedcoefficients.Interface {
	return newSlice(commonParent(h), capacity)
}

// Wrap wraps the given slice of unwrapped coefficients.
func (h *helper) Wrap(S unwrappedcoefficients.Interface) (coefficients.Interface, error) {
	SS, ok := S.(*Slice)
	if !ok {
		return nil, errors.UnknownSliceType.New()
	}
	return SS, nil
}

// ToUnwrappedElement attempts to map the element x from the user-facing universe to the universe of the unwrapped coefficients.
func (h *helper) ToUnwrappedElement(x object.Element) (object.Element, error) {
	return finitefield.ToElement(commonParent(h), x)
}

// SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped coefficients.
func (h *helper) SliceToUnwrappedSlice(S slice.Interface) (unwrappedcoefficients.Interface, error) {
	return appendSlice(newSlice(commonParent(h), S.Len()), S)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewHelper returns a new helper for coefficients in the given finite field.
func NewHelper(K *finitefield.Parent) coefficients.Helper {
	return defaultHelper
}
