// Coefficients provides a way of converting between what the user sees as the ring of exponents, and how this is implemented internally.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package coefficients

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/internal/converter"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"github.com/pkg/errors"
	"runtime/debug"
)

// Interface is the interface satisfied by a slice of coefficients.
type Interface interface {
	slice.Interface
	Universe() ring.Interface                // Universe returns the user-facing parent of the coefficients.
	Unwrap() unwrappedcoefficients.Interface // Unwrap returns the underlying internal slice of coefficients.
}

// Helper defines the common interface for a coefficient slice helper.
type Helper interface {
	Universe() ring.Interface                                    // Universe returns the user-facing parent of the coefficients.
	New(capacity int) unwrappedcoefficients.Interface            // New returns a new slice of unwrapped coefficients with given capacity.
	Wrap(S unwrappedcoefficients.Interface) (Interface, error)   // Wrap wraps the given slice of unwrapped coefficients.
	ToUnwrappedElement(x object.Element) (object.Element, error) // ToUnwrappedElement attempts to map the element x from the user-facing universe to the universe of the unwrapped coefficients.
}

// sliceToUnwrappedSlicer is the interface satisfied by the SliceToUnwrappedSlice method.
type sliceToUnwrappedSlicer interface {
	SliceToUnwrappedSlice(S slice.Interface) (unwrappedcoefficients.Interface, error) // SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped coefficients.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// EmptySlice returns an empty slice.
func EmptySlice(h Helper) Interface {
	S, err := h.Wrap(h.New(0))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return S
}

// SliceContainingOne returns a slice with the single entry 1.
func SliceContainingOne(h Helper) Interface {
	unwrappedS := h.New(1)
	S, err := h.Wrap(unwrappedS.Append(unwrappedS.Universe().One()))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return S
}

// SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped coefficients using the given helper. If S satisfies the interface:
//		type SliceToUnwrappedSlicer interface {
//			SliceToUnwrappedSlice(S slice.Interface)
//				(unwrappedcoefficients.Interface, error)
//				// SliceToUnwrappedSlice attempts to convert the entries in the
//				   slice S to a slice of unwrapped coefficients.
//		}
// then S's SliceToUnwrappedSlice method will be used.
func SliceToUnwrappedSlice(h Helper, S slice.Interface) (T unwrappedcoefficients.Interface, err error) {
	// If the helper supports this method directly, call it
	if hh, ok := h.(sliceToUnwrappedSlicer); ok {
		T, err = hh.SliceToUnwrappedSlice(S)
		return
	}
	// Appending the elements to the slices might panic, so allow us to recover
	defer func() {
		if r := recover(); r != nil {
			err = errors.Errorf("Error converting coefficients: %s\n%s", r, debug.Stack())
		}
	}()
	// Append the converted entries to a new unwrapped slice
	T = unwrappedcoefficients.AppendSlice(h.New(S.Len()), converter.New(h, S))
	return
}
