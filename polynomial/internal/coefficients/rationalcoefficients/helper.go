// Helper simplifies working with a slice of rational coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rationalcoefficients

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
)

// helper is the helper for rational coefficients.
type helper struct{}

// defaultHelper is the default helper instance.
var defaultHelper = helper{}

/////////////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the user-facing parent of the coefficients.
func (_ helper) Universe() ring.Interface {
	return rational.Field()
}

// New returns a new slice of unwrapped coefficients with given capacity.
func (_ helper) New(capacity int) unwrappedcoefficients.Interface {
	if capacity < 0 {
		capacity = 0
	}
	return make(Slice, 0, capacity)
}

// Wrap wraps the given slice of unwrapped coefficients.
func (_ helper) Wrap(S unwrappedcoefficients.Interface) (coefficients.Interface, error) {
	SS, ok := S.(Slice)
	if !ok {
		return nil, errors.UnknownSliceType.New()
	}
	return SS, nil
}

// ToUnwrappedElement attempts to map the element x from the user-facing universe to the universe of the unwrapped coefficients.
func (_ helper) ToUnwrappedElement(x object.Element) (object.Element, error) {
	return rational.ToElement(x)
}

// SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped coefficients.
func (_ helper) SliceToUnwrappedSlice(S slice.Interface) (unwrappedcoefficients.Interface, error) {
	return appendSlice(make(Slice, 0, S.Len()), S)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewHelper returns a new helper for rational coefficients.
func NewHelper() coefficients.Helper {
	return defaultHelper
}
