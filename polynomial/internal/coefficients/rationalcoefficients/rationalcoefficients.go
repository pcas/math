// Rationalcoefficients defines a slice of rational coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rationalcoefficients

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/integercoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
)

// Slice wraps a slice of *rational.Elements.
type Slice []*rational.Element

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// appendSlice appends the element of S to the end of the Slice s. Returns the updated Slice on success.
func appendSlice(s Slice, S slice.Interface) (Slice, error) {
	switch T := S.(type) {
	case Slice:
		s = append(s, T...)
	case rational.Slice:
		s = append(s, T...)
	case integercoefficients.Slice:
		for _, x := range T {
			s = append(s, rational.FromInteger(x))
		}
	case integer.Slice:
		for _, x := range T {
			s = append(s, rational.FromInteger(x))
		}
	case slice.ElementSlice:
		for _, x := range T {
			y, err := rational.ToElement(x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
	default:
		n := T.Len()
		for i := 0; i < n; i++ {
			y, err := rational.ToElement(T.Entry(i))
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
	}
	return s, nil
}

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent ring common to the elements in the slice.
func (s Slice) Universe() ring.Interface {
	return rational.Field()
}

// Len returns the length of the slice of coefficients.
func (s Slice) Len() int {
	return len(s)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (s Slice) Entry(i int) object.Element {
	return s[i]
}

// Append appends the element x to the end of the coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s Slice) Append(x object.Element) unwrappedcoefficients.Interface {
	y, err := rational.ToElement(x)
	if err != nil {
		panic(err)
	}
	return append(s, y)
}

// AppendSlice appends the element of S to the end of the coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic.
func (s Slice) AppendSlice(S slice.Interface) unwrappedcoefficients.Interface {
	T, err := appendSlice(s, S)
	if err != nil {
		panic(err)
	}
	return T
}

// Set sets the entry at index idx to x. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, or if the index is invalid, this will panic.
func (s Slice) Set(x object.Element, idx int) unwrappedcoefficients.Interface {
	y, err := rational.ToElement(x)
	if err != nil {
		panic(err)
	}
	s[idx] = y
	return s
}

// Insert inserts the element x at index idx. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic.
func (s Slice) Insert(x object.Element, idx int) unwrappedcoefficients.Interface {
	y, err := rational.ToElement(x)
	if err != nil {
		panic(err)
	} else if idx == len(s) {
		return append(s, y)
	}
	t := make(Slice, 0, len(s)+1)
	return append(append(append(t, s[:idx]...), y), s[idx:]...)
}

// Remove removes the element at index idx. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid.
func (s Slice) Remove(idx int) unwrappedcoefficients.Interface {
	if idx == 0 {
		return s[1:]
	} else if idx == len(s)-1 {
		return s[:idx]
	}
	t := make(Slice, 0, len(s)-1)
	return append(append(t, s[:idx]...), s[idx+1:]...)
}

// Swap exchanges the i-th and j-th elements. This will panic if i or j is out of range.
func (s Slice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Slice returns a subslice of the coefficients, starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s Slice) Slice(k int, m int) slice.Interface {
	return s[k:m]
}

// ToElementSlice returns a copy of the slice as an []object.Element.
func (s Slice) ToElementSlice() []object.Element {
	T := make([]object.Element, 0, len(s))
	for _, x := range s {
		T = append(T, x)
	}
	return T
}

// Unwrap returns s as an unwrappedcoefficients.Interface object.
func (s Slice) Unwrap() unwrappedcoefficients.Interface {
	return s
}

// Copy returns a copy of this slice (which will be of the same underlying type).
func (s Slice) Copy() unwrappedcoefficients.Interface {
	return Slice(rational.CopySlice(s))
}

// Hash returns a hash value for this slice.
func (s Slice) Hash() hash.Value {
	if len(s) == 0 {
		return 0
	}
	v := hash.NewSequenceHasher()
	defer hash.ReuseSequenceHasher(v)
	for _, x := range s {
		v.Add(x)
	}
	return v.Hash()
}

// PrettyPrint returns a string str and int sgn for the i-th element in this slice. Here str represents the "absolute value" of the i-th entry, and sgn represents the "sign" of the i-th entry. For example,
//		-7 would return "7", 1
//		13 would return "13", -1
//		0 would return "0", 0.
// This will panic if i is out of range.
func (s Slice) PrettyPrint(i int) (string, int) {
	x := s[i]
	return x.Abs().String(), x.Sign()
}
