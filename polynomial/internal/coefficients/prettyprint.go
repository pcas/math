// Prettyprint contains tools to help with converting coefficients to strings.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package coefficients

import (
	"bitbucket.org/pcas/math/compare"
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcastools/fatal"
	"strings"
)

// PrettyPrintFunc returns a string str and int sgn for the i-th element in the slice. Here str represents the "absolute value" of the i-th entry (whatever that means), and sgn represents the "sign" of the i-th entry. For example, over the integers:
//		the element -7 would return "7", 1;
//		the element 13 would return "13", -1;
//  and the element 0 would return "0", 0.
// This will panic if i is out of range.
type PrettyPrintFunc func(i int) (string, int)

// prettyPrinter is an optional interface that a slice of coefficients may choose to satisfy to assist with printing polynomials.
type prettyPrinter interface {
	PrettyPrint(i int) (string, int) // PrettyPrint implements the PrettyPrintFunc for the slice of coefficients.
}

// signFunc returns  -1 if x < 0, 0 if x == 0, and +1 if x > 0.
type signFunc func(x object.Element) int

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createSignFunc returns a sign function for the given ring using the given comparison functions.
func createSignFunc(R ring.Interface, cmp compare.CmpFunc) signFunc {
	zero := R.Zero()
	return func(x object.Element) int {
		sgn, err := cmp(x, zero)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return sgn
	}
}

// polynomialPrettyPrint handles pretty printing when the coefficient ring is a polynomial ring.
func polynomialPrettyPrint(R polynomial.Interface, x object.Element) (string, int) {
	// Get the zero case out of the way
	if isZero, err := R.IsZero(x); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	} else if isZero {
		return x.String(), 0
	}
	// Convert the element to a string and inspect whether it begins with a
	// negative sign
	str := x.String()
	sgn := 1
	if strings.HasPrefix(str, "-") {
		// It begins with a negative, so negate the element to flip the sign
		var err error
		if x, err = R.Negate(x); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		str = x.String()
		sgn = -1
	}
	// Check to see if this is a term -- if not, wrap it in parenthesis
	if isTerm, err := polynomial.IsTerm(R, x); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	} else if !isTerm {
		str = "(" + str + ")"
	}
	// Return the string and the sign
	return str, sgn
}

// cmperPrettyPrint handles pretty printing when the coefficient ring has a Cmp method.
func cmperPrettyPrint(R ring.Interface, signOf signFunc, x object.Element) (string, int) {
	// Get the sign of x and adjust as appropriate
	sgn := signOf(x)
	if sgn == 0 {
		return x.String(), 0
	} else if sgn < 0 {
		var err error
		if x, err = R.Negate(x); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		sgn = -1
	}
	// Build the string
	str := x.String()
	// Our best guess is that if the string contains + or - then we wrap it in
	// parenthesis
	if strings.ContainsAny(str, "+-") {
		str = "(" + str + ")"
	}
	// Return the string and the sign
	return str, sgn
}

// genericPrettyPrint is the most general pretty print function.
func genericPrettyPrint(R ring.Interface, x object.Element) (string, int) {
	// Get the zero case out of the way
	if isZero, err := R.IsZero(x); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	} else if isZero {
		return x.String(), 0
	}
	// Convert the element to a string and inspect whether it begins with a
	// negative sign
	str := x.String()
	sgn := 1
	if strings.HasPrefix(str, "-") {
		// It begins with a negative, so negate the element to flip the sign
		var err error
		if x, err = R.Negate(x); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		str = x.String()
		sgn = -1
	}
	// Our best guess is that if the string contains + or - then we wrap it in
	// parenthesis
	if strings.ContainsAny(str, "+-") {
		str = "(" + str + ")"
	}
	// Return the string and the sign
	return str, sgn
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// PrettyPrint returns a PrettyPrintFunc for the given coefficient slice S. If S satisfies the interface:
//		type PrettyPrinter interface {
//			PrettyPrint(i int) (string, int) // PrettyPrint implements the
//			   PrettyPrintFunc for the slice of coefficients.
// 		}
// then S's PrettyPrint method will be used by the returned PrettyPrintFunc. Otherwise, if the underlying slice of wrapped coefficients satisfies the PrettyPrinter interface, then its PrettyPrint method will be used by the returned PrettyPrintFunc. Failing that, a generic PrettyPrintFunc will be returned.
func PrettyPrint(S Interface) PrettyPrintFunc {
	// Does the slice implement the prettyPrinter interface?
	if pp, ok := S.(prettyPrinter); ok {
		return pp.PrettyPrint
	} else if pp, ok := S.Unwrap().(prettyPrinter); ok {
		// Otherwise if the underlying coefficients slice has a PrettyPrint
		// method, use that
		return pp.PrettyPrint
	}
	// No such luck -- note the common parent
	R := S.Universe()
	// How we proceed depends on properties of the parent
	switch K := R.(type) {
	case integer.Parent:
		// R is the ring of integers ZZ
		return func(i int) (string, int) {
			x := S.Entry(i).(*integer.Element)
			return x.Abs().String(), x.Sign()
		}
	case rational.Parent:
		// R is the field of rationals QQ
		return func(i int) (string, int) {
			x := S.Entry(i).(*rational.Element)
			return x.Abs().String(), x.Sign()
		}
	case *gfp.Parent:
		// R is a finite field
		return func(i int) (string, int) {
			x := S.Entry(i).(*gfp.Element)
			if x.IsZero() {
				return x.String(), 0
			}
			return x.String(), 1
		}
	case polynomial.Interface:
		// R is a polynomial ring
		return func(i int) (string, int) {
			return polynomialPrettyPrint(K, S.Entry(i))
		}
	case compare.Cmper:
		// R has a Cmp method
		signOf := createSignFunc(R, K.Cmp)
		return func(i int) (string, int) {
			return cmperPrettyPrint(R, signOf, S.Entry(i))
		}
	default:
		// We know nothing helpful about R
		return func(i int) (string, int) {
			return genericPrettyPrint(R, S.Entry(i))
		}
	}
}
