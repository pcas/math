// Unwrappedcoefficients defines the common interface satisfied by a slice of unwrapped coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package unwrappedcoefficients

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/sort"
)

// appendSlicer is an optional interface that a slice of unwrapped coefficients may choose to satisfy to assist with appending a slice.
type appendSlicer interface {
	AppendSlice(S slice.Interface) Interface // AppendSlice appends the element of the slice S to the end of the coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic.
}

// setter is an optional interface that an unwrapped slice of coefficients may choose to satisfy to assist with setting an element.
type setter interface {
	Set(x object.Element, idx int) Interface // Set sets the entry at index idx to x. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, or if the index is invalid, this will panic.
}

// inserter is an optional interface that an unwrapped slice of coefficients may choose to satisfy to assist with inserting an element.
type inserter interface {
	Insert(x object.Element, idx int) Interface // Insert inserts the element x at index idx. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic.
}

// remover is an optional interface that an unwrapped slice of coefficients may choose to satisfy to assist with removing an element.
type remover interface {
	Remove(idx int) Interface // Remove removes the element at index idx. Returns the updated coefficients on success (which will be of the same underlying type). This will panic if the index is invalid.
}

// isEqualToer is an optional interface that an unwrapped slice of coefficients may choose to satisfy to answer whether two slices are equal.
type isEqualToer interface {
	IsEqualTo(S slice.Interface) (bool, error) // IsEqualTo returns true iff the entries in the slice are equal to the given slice S.
}

// NewFunc returns a new coefficient slice of given capacity.
type NewFunc func(capacity int) Interface

// Interface is the interface that a slice of coefficients needs to satisfy.
type Interface interface {
	sort.Swapper
	slice.Interface
	Universe() ring.Interface           // Universe returns the parent ring common to the elements in the slice.
	Append(x object.Element) Interface  // Append appends the element x to the end of the unwrapped coefficients. Returns the updated coefficients on success (which will be of the same underlying type). If x is not of the required type, this will panic.
	Slice(k int, m int) slice.Interface // Slice returns a subslice of the unwrapped coefficients, starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
	Copy() Interface                    // Copy returns a copy of this slice (which will be of the same underlying type).
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AppendSlice appends the element of the slice S to the end of the unwrapped coefficient slice T. Returns the updated coefficients on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic. If T satisfies the interface:
//		type AppendSlicer interface {
//			AppendSlice(S slice.Interface) Interface // AppendSlice appends the
//			   element of the slice S to the end of the coefficients. Returns
//			   the updated coefficients on success (which will be of the same
//			   underlying type). If the elements of S are not of the required
//			   type, this will panic.
//		}
// then T's AppendSlice method will be called.
func AppendSlice(T Interface, S slice.Interface) Interface {
	// Does the slice implement the appendSlicer interface?
	if TT, ok := T.(appendSlicer); ok {
		return TT.AppendSlice(S)
	}
	// No such luck -- append the elements one at a time
	n := S.Len()
	for i := 0; i < n; i++ {
		T = T.Append(S.Entry(i))
	}
	return T
}

// Set sets the entry at index idx of the unwrapped coefficients slice S to x. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, or if the index is invalid, this will panic. If S satisfies the interface:
//		type Setter interface {
//			Set(x object.Element, idx int) Interface // Set sets the entry at
//		      index idx to x. Returns the updated coefficients on success
//			  (which will be of the same underlying type). If the element x is
//			  not of the required type, or if the index is invalid, this will
//			  panic.
//		}
// then S's Set method will be called.
func Set(S Interface, x object.Element, idx int) Interface {
	// Does the slice implement the setter interface?
	if SS, ok := S.(setter); ok {
		return SS.Set(x, idx)
	}
	// Sanity check on the index
	size := S.Len()
	if idx < 0 || idx >= size {
		panic(errors.InvalidIndexRange.New(idx, size-1))
	}
	// Return S[0:idx] cat [x] cat S[idx+1:size]
	return AppendSlice(S.Slice(0, idx).(Interface).Copy().Append(x), S.Slice(idx+1, size))
}

// Insert inserts the element x into the unwrapped coefficients slice S at index idx. Returns the updated coefficients on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic. If S satisfies the interface:
//		type Inserter interface {
//			Insert(x object.Element, idx int) Interface // Insert inserts the
//			  element x at index idx. Returns the updated coefficients on
//			  success (which will be of the same underlying type). If the
//			  element x is not of the required type, this will panic. The index
//			  must be in the range 0..S.Len() (inclusive), or this will panic.
//		}
// then S's Insert method will be called.
func Insert(S Interface, x object.Element, idx int) Interface {
	// Does the slice implement the inserter interface?
	if SS, ok := S.(inserter); ok {
		return SS.Insert(x, idx)
	}
	// Sanity check on the index
	size := S.Len()
	if idx < 0 || idx > size {
		panic(errors.InvalidIndexRange.New(idx, size))
	}
	// Can we simply append x?
	if idx == size {
		return S.Append(x)
	}
	// Return S[0:idx] cat [x] cat S[idx:size]
	return AppendSlice(S.Slice(0, idx).(Interface).Copy().Append(x), S.Slice(idx, size))
}

// Remove removes the element at index idx from the unwrapped coefficients slice S. Returns the updated coefficients on success (which will be of the same underlying type). This will panic if the index is invalid. If S satisfies the interface:
//		type Remover interface {
//			Remove(idx int) Interface // Remove removes the element at index
// 			  idx. Returns the updated coefficients on success (which will be of
// 			  the same underlying type). This will panic if the index is
//			  invalid.
//		}
// then S's Remove method will be called.
func Remove(S Interface, idx int) Interface {
	// Does the slice implement the remover interface?
	if SS, ok := S.(remover); ok {
		return SS.Remove(idx)
	}
	// Sanity check on the index
	size := S.Len()
	if idx < 0 || idx >= size {
		panic(errors.InvalidIndexRange.New(idx, size-1))
	}
	// Can we simply slice?
	if idx == 0 {
		return S.Slice(1, size).(Interface)
	} else if idx == size-1 {
		return S.Slice(0, size-1).(Interface)
	}
	// Return S[0:idx] cat S[idx+1:size]
	return AppendSlice(S.Slice(0, idx).(Interface).Copy(), S.Slice(idx+1, size))
}

// AreEqual returns true iff the slice of unwrapped coefficients S is equal to the slice T. If S satisfies the interface:
//		type IsEqualToer interface {
//			IsEqualTo(S slice.Interface) (bool, error) // IsEqualTo returns
//			   true iff the entries in the slice are equal to the given slice S.
//		}
// then S's IsEqualTo method will be called.
func AreEqual(S Interface, T slice.Interface) (bool, error) {
	// Does S implement the isEqualToer interface?
	if SS, ok := S.(isEqualToer); ok {
		return SS.IsEqualTo(T)
	}
	// Check that the lengths agree
	size := S.Len()
	if T.Len() != size {
		return false, nil
	}
	// Start walking through the entries checking for equality
	K := S.Universe()
	for i := 0; i < size; i++ {
		if ok, err := K.AreEqual(S.Entry(i), T.Entry(i)); err != nil {
			return false, err
		} else if !ok {
			return false, nil
		}
	}
	// If we're here then the slices are equal
	return true, nil
}
