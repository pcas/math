// Common defines useful functions for use when working with coefficients and exponents.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package common

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/creation"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/operation"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/property"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
)

// BinaryOperation defines a binary operation on polynomials f and g, which returns exponents and coefficients defining a new polynomial. Here f is represented by parallel slices of exponents esf and coefficients csf, and similarly for g.
type BinaryOperation func(
	esf exponents.Interface, csf coefficients.Interface,
	esg exponents.Interface, csg coefficients.Interface,
	eh exponents.Helper, ch coefficients.Helper,
) (exponents.Interface, coefficients.Interface, error)

// binaryOperation defines a binary operation on polynomials f and g, which returns exponents and coefficients defining a new polynomial. Here f is represented by parallel slices of exponents esf and coefficients csf, and similarly for g.
type binaryOperation func(
	esf unwrappedexponents.Interface, csf unwrappedcoefficients.Interface,
	esg unwrappedexponents.Interface, csg unwrappedcoefficients.Interface,
	newEs unwrappedexponents.NewFunc, newCs unwrappedcoefficients.NewFunc,
) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrapPair returns the wrapped the slices of unwrapped exponents and coefficients.
func wrapPair(es unwrappedexponents.Interface, cs unwrappedcoefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	wrappedEs, err := eh.Wrap(es)
	if err != nil {
		return nil, nil, err
	}
	wrappedCs, err := ch.Wrap(cs)
	if err != nil {
		return nil, nil, err
	}
	return wrappedEs, wrappedCs, nil
}

// applyBinaryOperation applies the given binaryOperation to the given parallel slices of exponents and coefficients representing polynomials f and g.
func applyBinaryOperation(op binaryOperation, esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	es, cs, err := op(
		esf.Unwrap(), csf.Unwrap(),
		esg.Unwrap(), csg.Unwrap(),
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(es, cs, eh, ch)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns the sum f + g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The sum is returned as two parallel slices of exponents and coefficients.
func Add(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	return applyBinaryOperation(operation.Add, esf, csf, esg, csg, eh, ch)
}

// Subtract returns the difference f - g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The difference is returned as two parallel slices of exponents and coefficients.
func Subtract(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	return applyBinaryOperation(operation.Subtract, esf, csf, esg, csg, eh, ch)
}

// Negate returns the negation -f, where f is the polynomial defined by the slice cs of coefficients. The resulting coefficients will be returned.
func Negate(cs coefficients.Interface, ch coefficients.Helper) (coefficients.Interface, error) {
	newcs, err := operation.Negate(cs.Unwrap(), ch.New)
	if err != nil {
		return nil, err
	}
	return ch.Wrap(newcs)
}

// Multiply returns the product f * g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The product is returned as two parallel slices of exponents and coefficients.
func Multiply(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	return applyBinaryOperation(operation.Multiply, esf, csf, esg, csg, eh, ch)
}

// ScalarMultiplyByInteger returns n * f, where this is defined to be f + ... + f (n times) if n is positive, -f - ... - f (|n| times) if n is negative, and 0 if n is zero. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The result is returned as two parallel slices of exponents and coefficients.
func ScalarMultiplyByInteger(n *integer.Element, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	newes, newcs, err := operation.ScalarMultiplyByInteger(
		n,
		es.Unwrap(), cs.Unwrap(),
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(newes, newcs, eh, ch)
}

// ScalarMultiplyByCoefficient returns a * f, where a is an element in the coefficient ring and f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The result is returned as two parallel slices of exponents and coefficients.
func ScalarMultiplyByCoefficient(a object.Element, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	newes, newcs, err := operation.ScalarMultiplyByCoefficient(
		a,
		es.Unwrap(), cs.Unwrap(),
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(newes, newcs, eh, ch)
}

// PowerInt64 returns f^k. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The power is returned as two parallel slices of exponents and coefficients.
func PowerInt64(es exponents.Interface, cs coefficients.Interface, k int64, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	newes, newcs, err := operation.PowerInt64(
		es.Unwrap(), cs.Unwrap(),
		k,
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(newes, newcs, eh, ch)
}

// Power returns f^k. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The power is returned as two parallel slices of exponents and coefficients.
func Power(es exponents.Interface, cs coefficients.Interface, k *integer.Element, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	newes, newcs, err := operation.Power(
		es.Unwrap(), cs.Unwrap(),
		k,
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(newes, newcs, eh, ch)
}

// Sum returns the sum of the polynomials described by the parallel slices Es and Cs, where the i-th entry Es[i] and Cs[i] in each slice are parallel slices describing the exponents and coefficients, respectively, of a polynomial. The sum is returned as two parallel slices of exponents and coefficients. The slices Es and Cs are assumed to be of the same length.
func Sum(Es []exponents.Interface, Cs []coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// Unwrap the exponents and coefficients
	unwrappedEs := make([]unwrappedexponents.Interface, 0, len(Es))
	for _, es := range Es {
		unwrappedEs = append(unwrappedEs, es.Unwrap())
	}
	unwrappedCs := make([]unwrappedcoefficients.Interface, 0, len(Cs))
	for _, cs := range Cs {
		unwrappedCs = append(unwrappedCs, cs.Unwrap())
	}
	// Perform the computation
	newes, newcs, err := operation.Sum(
		unwrappedEs, unwrappedCs,
		eh.New, ch.New,
	)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(newes, newcs, eh, ch)
}

// IsOne returns true iff the parallel slices of exponents es and coefficients cs correspond to the polynomial 1.
func IsOne(es exponents.Interface, cs coefficients.Interface) (bool, error) {
	return property.IsOne(es.Unwrap(), cs.Unwrap())
}

// IsMonomial returns true iff the slice of coefficients cs correspond to a monomial (that is, x^m for some m in the exponent monoid).
func IsMonomial(cs coefficients.Interface) (bool, error) {
	return property.IsMonomial(cs.Unwrap())
}

// IsMonic returns true iff the slice of coefficients cs describe a monic polynomial f (that is, if the leading coefficient of f is 1). Assumes that the slice is sorted in increasing exponent order.
func IsMonic(cs coefficients.Interface) (bool, error) {
	return property.IsMonic(cs.Unwrap())
}

// IsConstant returns true iff the parallel slices of exponents es and coefficients cs describe a constant polynomial. If true, also returns the constant.
func IsConstant(es exponents.Interface, cs coefficients.Interface) (bool, object.Element, error) {
	if size := cs.Len(); size == 0 {
		return true, cs.Universe().Zero(), nil
	} else if size != 1 {
		return false, nil, nil
	}
	unwrappedEs := es.Unwrap()
	if isZero, err := unwrappedEs.Universe().IsZero(unwrappedEs.Entry(0)); err != nil {
		return false, nil, err
	} else if !isZero {
		return false, nil, nil
	}
	return true, cs.Entry(0), nil
}

// AreEqual returns true iff the polynomials f and g are equal, where f is described by the parallel slices of exponents esf and coefficients csf, and g is described by the parallel slices of exponents esg and coefficients csg. Assumes that the slices are sorted in increasing exponent order.
func AreEqual(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface) (bool, error) {
	return property.AreEqual(
		esf.Unwrap(), csf.Unwrap(),
		esg.Unwrap(), csg.Unwrap(),
	)
}

// CmpLeadingMonomial returns -1 if LM(f) < LM(g), 0 if LM(f) = LM(g), and +1 if LM(f) > LM(g), were LM denotes the leading monomial. The exponents of the  polynomial f (and, respectively, g) are given by the slice of exponents esf (resp. esg), which are assumed to be sorted in increasing exponent order. It returns an error if f = 0 or g = 0, in which case LM(f) or LM(g) is not well-defined, or if the parents of the monomials do not agree.
func CmpLeadingMonomial(esf exponents.Interface, esg exponents.Interface) (int, error) {
	return property.CmpLeadingMonomial(esf.Unwrap(), esg.Unwrap())
}

// NthMonomial returns parallel slices of exponents and coefficients describing the N-th monomial x^k in the polynomial f, where the exponents of f are described by the given slice of exponents es.
func NthMonomial(N int, es exponents.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	unwrappedEs, unwrappedCs, err := creation.NthMonomial(N, es.Unwrap(), eh.New, ch.New)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// NthTerm returns parallel slices of exponents and coefficients describing the N-th term c * x^k in the polynomial f, where f is described by the given parallel slices of exponents es and coefficients cs.
func NthTerm(N int, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	unwrappedEs, unwrappedCs, err := creation.NthTerm(N, es.Unwrap(), cs.Unwrap(), eh.New, ch.New)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// ToMonomial returns parallel slices of exponents and coefficients describing the monomial x^k.
func ToMonomial(k object.Element, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// Attempt to cast the exponent into the unwrapped universe
	kk, err := eh.ToUnwrappedElement(k)
	if err != nil {
		return nil, nil, err
	}
	// Create and return the monomial
	unwrappedEs, unwrappedCs, err := creation.ToMonomial(kk, eh.New, ch.New)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// ToTerm returns parallel slices of exponents and coefficients describing the term c * x^k, where c is an element in the coefficient ring.
func ToTerm(k object.Element, c object.Element, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// Attempt to cast the exponent into the unwrapped universe
	kk, err := eh.ToUnwrappedElement(k)
	if err != nil {
		return nil, nil, err
	}
	// Attempt to cast the coefficient into the unwrapped universe
	cc, err := ch.ToUnwrappedElement(c)
	if err != nil {
		return nil, nil, err
	}
	// Create and return the term
	unwrappedEs, unwrappedCs, err := creation.ToTerm(kk, cc, eh.New, ch.New)
	if err != nil {
		return nil, nil, err
	}
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// FromParallelSlices returns parallel slices of exponents and coefficients, where the exponents are from the slice es, and the coefficients from the slice cs. The returned slices of exponents and coefficients will be parallel sorted.
func FromParallelSlices(es slice.Interface, cs slice.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// Check that the length of the slices agree
	if es.Len() != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Attempt to convert the exponents
	unwrappedEs, err := exponents.SliceToUnwrappedSlice(eh, es)
	if err != nil {
		return nil, nil, err
	}
	// Attempt to convert the coefficients
	unwrappedCs, err := coefficients.SliceToUnwrappedSlice(ch, cs)
	if err != nil {
		return nil, nil, err
	}
	// Prune any zero terms
	if unwrappedEs, unwrappedCs, err = creation.PruneZeroCoefficients(unwrappedEs, unwrappedCs); err != nil {
		return nil, nil, err
	}
	// Sort and merge the terms
	if unwrappedEs, unwrappedCs, err = creation.SortAndMergeExponentsAndCoefficients(unwrappedEs, unwrappedCs, eh.New, ch.New); err != nil {
		return nil, nil, err
	}
	// Return the wrapped parallel slices of exponents and coefficients
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// Hash returns a hash value for the given parallel slices of exponents and coefficients.
func Hash(es exponents.Interface, cs coefficients.Interface) hash.Value {
	return property.Hash(es.Unwrap(), cs.Unwrap())
}
