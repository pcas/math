// Property defines functions for inspecting properties of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package property

import (
	"bitbucket.org/pcas/math/errors"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
)

// boolOrError contains a boolean value and optional error.
type boolOrError struct {
	b   bool  // The boolean
	err error // The error (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// sliceHashToChannel feeds the hash of the given slice down the given channel.
func sliceHashToChannel(S slice.Interface, c chan<- hash.Value) {
	c <- slice.Hash(S)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsOne returns true iff the parallel slices of exponents es and coefficients cs correspond to the polynomial 1.
func IsOne(es exponents.Interface, cs coefficients.Interface) (bool, error) {
	if es.Len() != 1 {
		return false, nil
	} else if isZero, err := es.Universe().IsZero(es.Entry(0)); err != nil {
		return false, err
	} else if !isZero {
		return false, nil
	}
	return cs.Universe().IsOne(cs.Entry(0))
}

// IsMonomial returns true iff the slice of coefficients cs correspond to a monomial (that is, x^m for some m in the exponent monoid).
func IsMonomial(cs coefficients.Interface) (bool, error) {
	if size := cs.Len(); size != 1 {
		return false, nil
	}
	return cs.Universe().IsOne(cs.Entry(0))
}

// IsMonic returns true iff the slice of coefficients cs describe a monic polynomial f (that is, if the leading coefficient of f is 1). Assumes that the slice is sorted in increasing exponent order.
func IsMonic(cs coefficients.Interface) (bool, error) {
	size := cs.Len()
	if size == 0 {
		return false, nil
	}
	return cs.Universe().IsOne(cs.Entry(size - 1))
}

// AreEqual returns true iff the polynomials f and g are equal, where f is described by the parallel slices of exponents esf and coefficients csf, and g is described by the parallel slices of exponents esg and coefficients csg. Assumes that the slices are sorted in increasing exponent order.
func AreEqual(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface) (bool, error) {
	// Sanity check
	if esf.Len() != csf.Len() || esg.Len() != csg.Len() {
		return false, errors.SliceLengthNotEqual.New()
	}
	// Check for equality
	c := make(chan *boolOrError, 1)
	go func() {
		ok, err := exponents.AreEqual(esf, esg)
		c <- &boolOrError{b: ok, err: err}
	}()
	go func() {
		ok, err := coefficients.AreEqual(csf, csg)
		c <- &boolOrError{b: ok, err: err}
	}()
	// Extract the results
	res := <-c
	if res.err != nil || !res.b {
		return false, res.err
	}
	res = <-c
	return res.b, res.err
}

// CmpLeadingMonomial returns -1 if LM(f) < LM(g), 0 if LM(f) = LM(g), and +1 if LM(f) > LM(g), were LM denotes the leading monomial. The exponents of the  polynomial f (and, respectively, g) are given by the slice of exponents esf (resp. esg), which are assumed to be sorted in increasing exponent order. It returns an error if f = 0 or g = 0, in which case LM(f) or LM(g) is not well-defined, or if the parents of the monomials do not agree.
func CmpLeadingMonomial(esf exponents.Interface, esg exponents.Interface) (int, error) {
	lenf, leng := esf.Len(), esg.Len()
	if lenf == 0 || leng == 0 {
		return 0, errors.IllegalZeroArg.New()
	}
	return esf.Universe().Cmp(esf.Entry(lenf-1), esg.Entry(leng-1))
}

// Hash returns a hash value for the given parallel slices of exponents and coefficients.
func Hash(es exponents.Interface, cs coefficients.Interface) hash.Value {
	ces, ccs := make(chan hash.Value), make(chan hash.Value)
	go sliceHashToChannel(es, ces)
	go sliceHashToChannel(cs, ccs)
	return hash.Value((int64(<-ces) + (int64(<-ccs) << 1)) % hash.Max)
}
