// Recursive provides a standard way of recursively performing an operation on the terms of a polynomial via binary split and merge.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package recursive

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/creation"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"runtime"
)

// cacheSize is the cache size to use when collecting terms into parallel slices of exponents and coefficients.
const cacheSize = 100

// workerResult
type workerResult struct {
	es  exponents.Interface
	cs  coefficients.Interface
	err error
}

// Func is the base function of the recursion. It should iterate over the range start <= i < finish, and return parallel slices of exponents and coefficients.
type Func func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// baseCase runs the function f for the given range. The returned parallel slices of exponents and coefficients are sorted in strictly increasing exponent order, with all coefficients non-zero.
func baseCase(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc, f Func) (exponents.Interface, coefficients.Interface, error) {
	// Call the function
	es, cs, err := f(start, finish, newEs, newCs)
	if err != nil {
		return nil, nil, err
	}
	// Sort the exponents and coefficients
	return creation.SortExponentsAndCoefficients(es, cs)
}

func baseCaseToChannel(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc, f Func, c chan<- *workerResult) {
	es, cs, err := baseCase(start, finish, newEs, newCs, f)
	c <- &workerResult{
		es:  es,
		cs:  cs,
		err: err,
	}
}

// temp
func temp(size int, f Func, newEs exponents.NewFunc, newCs coefficients.NewFunc) (*MergeAndSumIterator, error) {
	// Decide on the number of workers
	numWorkers := runtime.GOMAXPROCS(0)
	if numWorkers > size/2 {
		if numWorkers = size / 2; numWorkers == 0 {
			numWorkers = 1
		}
	}
	// If there's only one worker, do it in place
	if numWorkers == 1 {
		es, cs, err := baseCase(0, size, newEs, newCs, f)
		if err != nil {
			return nil, err
		}
		return NewMergeAndSumIterator([]exponents.Interface{es}, []coefficients.Interface{cs}, es.Universe(), cs.Universe())
	}
	// Create the communication channel
	c := make(chan *workerResult)
	// Set the workers working
	stepSize := size / numWorkers
	for i := 0; i < numWorkers-1; i++ {
		go baseCaseToChannel(i*stepSize, (i+1)*stepSize, newEs, newCs, f, c)
	}
	go baseCaseToChannel((numWorkers-1)*stepSize, size, newEs, newCs, f, c)
	// Extract the results
	Es := make([]exponents.Interface, 0, numWorkers)
	Cs := make([]coefficients.Interface, 0, numWorkers)
	var L monomialorder.Interface
	var R ring.Interface
	for i := 0; i < numWorkers; i++ {
		res := <-c
		// THINK: FIX
		if res.err != nil {
			return nil, res.err
		}
		Es = append(Es, res.es)
		Cs = append(Cs, res.cs)
		if i == 0 {
			L = res.es.Universe()
			R = res.cs.Universe()
		}
	}
	// Return the iterator
	return NewMergeAndSumIterator(Es, Cs, L, R)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run performs a recursive binary split on the range 0..length. When the resulting sub-range is of length at most minsize the function f will be called. The results from f are merged into the returned parallel slices of exponents and coefficients. The resulting slices are sorted in strictly-increasing exponent order (so all monomials appearing amongst the terms are distinct) and with all coefficients non-zero.
func Run(length int, _ int, newEs exponents.NewFunc, newCs coefficients.NewFunc, f Func) (exponents.Interface, coefficients.Interface, error) {
	// Create the iterator
	k, err := temp(length, f, newEs, newCs)
	if err != nil {
		return nil, nil, err
	}
	// Create the destination slices of exponents and coefficients
	destEs, destCs := newEs(length), newCs(length)
	// Create the cache of exponents and coefficients
	cacheEs := make([]object.Element, cacheSize)
	cacheCs := make([]object.Element, cacheSize)
	size := 0
	// Start populating the caches with the terms
	for k.HasNext() {
		// Extract the next exponent and coefficient pair into the cache
		cacheEs[size], cacheCs[size] = k.Next()
		size++
		// If necessary, flush the cache
		if size == cacheSize {
			destEs = exponents.AppendSlice(destEs, slice.ElementSlice(cacheEs))
			destCs = coefficients.AppendSlice(destCs, slice.ElementSlice(cacheCs))
			size = 0
		}
	}
	// Check for an error
	if err := k.Close(); err != nil {
		return nil, nil, err
	}
	// If necessary, flush the cache
	if size != 0 {
		destEs = exponents.AppendSlice(destEs, slice.ElementSlice(cacheEs[:size]))
		destCs = coefficients.AppendSlice(destCs, slice.ElementSlice(cacheCs[:size]))
	}
	return destEs, destCs, nil
}
