// Iterator provides a way of iterating over multiply slices of coefficients and exponents, in increasing exponent order.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package recursive

import (
	"bitbucket.org/pcas/math/compare"
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/ring"
	"sync"
)

// defaultBufSize is the default buffer size used by an Iterator.
const defaultBufSize = 512

// iterator provides a way of iterating over multiple slices of exponents and coefficients, sorted in exponent order.
type iterator struct {
	t   *termList                // The term list used to sort the data
	ess []exponents.Interface    // Parallel slices of parallel slices of exponents...
	css []coefficients.Interface // ...and coefficients
	e   object.Element           // The next exponent
	c   object.Element           // The next coefficient
	err error                    // The last error encountered (if any)
}

// circularList provides a circular list for storing pairs of exponents and coefficients.
type circularList struct {
	es       []object.Element // Parallel slices of exponents...
	cs       []object.Element // ...and coefficients
	size     int              // The current size of the list
	readPos  int              // The index of the next read
	writePos int              // The index of the next write
}

// termList provides a way to return data sorted by exponents.
type termList struct {
	cmp    compare.CmpFunc // The exponent comparison function
	offset int             // The free capacity in the list
	l      []*termListData // The underlying list storage
}

// termListData describes the data contained in an entry of the termList.
type termListData struct {
	e        object.Element // The exponent...
	c        object.Element // ...and coefficient
	sliceIdx int            // The index of the source slice
	entryIdx int            // The entry index in the source slice
}

// Iterator provides a way of iterating over multiple slices of exponents and coefficients, sorted in exponent order.
type Iterator struct {
	m        sync.Mutex     // Mutex controlling data access
	wc       *sync.Cond     // Wake up the worker
	rc       *sync.Cond     // Wake up the user
	cl       *circularList  // The buffer of results
	exit     bool           // Have we been asked to finish iterating?
	isClosed bool           // Has iteration finished?
	e        object.Element // The next exponent...
	c        object.Element // ...and coefficient
	err      error          // The last error encountered (if any)
}

type MergeAndSumIterator struct {
	k       *Iterator
	cmp     compare.CmpFunc
	esK     monomialorder.Interface
	csK     ring.Interface
	hasNext bool
	nextE   object.Element
	nextC   object.Element
	curE    object.Element
	curC    object.Element
}

/////////////////////////////////////////////////////////////////////////
// circularList functions
/////////////////////////////////////////////////////////////////////////

// newCircularList returns a new circular list of given capacity.
func newCircularList(capacity int) *circularList {
	if capacity < 0 {
		capacity = 0
	}
	return &circularList{
		es: make([]object.Element, capacity),
		cs: make([]object.Element, capacity),
	}
}

// Len returns the number of entries in the circular list.
func (cl *circularList) Len() int {
	if cl == nil {
		return 0
	}
	return cl.size
}

// IsEmpty returns true iff the circular list is empty.
func (cl *circularList) IsEmpty() bool {
	return cl == nil || cl.size == 0
}

// IsFull returns true iff the circular list is full.
func (cl *circularList) IsFull() bool {
	return cl == nil || cl.size == len(cl.es)-1
}

// Append attempts to append the given exponent e and coefficient c to the end of the circular list. Returns true on success, otherwise -- if the list is full -- returns false.
func (cl *circularList) Append(e object.Element, c object.Element) bool {
	if cl.IsFull() {
		return false
	}
	cl.es[cl.writePos], cl.cs[cl.writePos] = e, c
	cl.writePos = (cl.writePos + 1) % len(cl.es)
	cl.size++
	return true
}

// Pop pops the next exponent e and coefficient c from the front of the circular list. If the list is empty, ok will be false.
func (cl *circularList) Pop() (e object.Element, c object.Element, ok bool) {
	if cl.IsEmpty() {
		return
	}
	e, c = cl.es[cl.readPos], cl.cs[cl.readPos]
	cl.readPos = (cl.readPos + 1) % len(cl.es)
	cl.size--
	ok = true
	return
}

/////////////////////////////////////////////////////////////////////////
// termList functions
/////////////////////////////////////////////////////////////////////////

// newTermList returns a new term list with given exponent comparison function and initial capacity.
func newTermList(cmp compare.CmpFunc, capacity int) *termList {
	// Sanity check
	if cmp == nil {
		panic("Illegal nil comparator function.")
	} else if capacity < 0 {
		capacity = 0
	}
	// Return the list
	return &termList{
		cmp:    cmp,
		offset: capacity,
		l:      make([]*termListData, capacity),
	}
}

// IsEmpty returns true iff the term list contains no more data.
func (t *termList) IsEmpty() bool {
	return t == nil || len(t.l) == t.offset
}

// Add adds the given data to the term list.
func (t *termList) Add(d *termListData) error {
	// Ensure that there's space in the list
	offset := t.offset
	if offset == 0 {
		l := make([]*termListData, len(t.l)+1)
		copy(l[1:], t.l)
		t.l = l
		t.offset = 1
		offset = 1
	}
	// Insert the data at the correct location in the list
	cmp := t.cmp
	for i, d2 := range t.l[offset:] {
		sgn, err := cmp(d.e, d2.e)
		if err != nil {
			return err
		} else if sgn <= 0 {
			copy(t.l[offset-1:], t.l[offset:offset+i])
			t.l[offset+i-1] = d
			t.offset--
			return nil
		}
	}
	// If we're here then the data needs to be placed at the end of the list
	copy(t.l[offset-1:], t.l[offset:])
	t.l[len(t.l)-1] = d
	t.offset--
	return nil
}

// Pop removes and returns the smallest data in the term list. If the list is empty, returns nil.
func (t *termList) Pop() *termListData {
	// Is there anything to do?
	if t.IsEmpty() {
		return nil
	}
	// Return the next piece of data
	d := t.l[t.offset]
	t.offset++
	return d
}

/////////////////////////////////////////////////////////////////////////
// iterator functions
/////////////////////////////////////////////////////////////////////////

// newIterator returns a new iterator for the given parallel slices of exponents and coefficients, using the given exponent comparator. The pairs of exponents and coefficients will be iterated over in increasing exponent order. Note that no attempt is made to exclude zero coefficients, or to sum repeated instances of the same exponent. Assumes that the input slices are sorted in increasing exponent order.
func newIterator(Es []exponents.Interface, Cs []coefficients.Interface, cmp compare.CmpFunc) (*iterator, error) {
	// Sanity check
	if len(Es) != len(Cs) {
		return nil, errors.SliceLengthNotEqual.New()
	}
	for i, es := range Es {
		if es.Len() != Cs[i].Len() {
			return nil, errors.SliceLengthNotEqual.New()
		}
	}
	// Create the term list
	t := newTermList(cmp, len(Es))
	// Place the initial data onto the term list
	for i, es := range Es {
		if es.Len() != 0 {
			if err := t.Add(&termListData{
				sliceIdx: i,
				e:        es.Entry(0),
				c:        Cs[i].Entry(0),
			}); err != nil {
				return nil, err
			}
		}
	}
	// Create and return the iterator
	return &iterator{
		t:   t,
		ess: Es,
		css: Cs,
	}, nil
}

// Err returns the last error encountered during iteration (if any).
func (r *iterator) Err() error {
	if r == nil {
		return nil
	}
	return r.err
}

// HasNext returns true iff there is a next element in the iterator.
func (r *iterator) HasNext() bool {
	// Is there anything to do?
	if r == nil || r.t.IsEmpty() {
		return false
	}
	// Pop the next piece of data off the list and note the exponent and coeff
	d := r.t.Pop()
	r.e, r.c = d.e, d.c
	// Add the next piece of data to the list
	d.entryIdx++
	if i := d.sliceIdx; d.entryIdx < r.ess[i].Len() {
		d.e, d.c = r.ess[i].Entry(d.entryIdx), r.css[i].Entry(d.entryIdx)
		if err := r.t.Add(d); err != nil {
			r.err = err
			return false
		}
	}
	// Return success
	return true
}

// Next returns the next pair of exponent e and coefficient c in the iterator. You must call HasNext before calling Next.
func (r *iterator) Next() (e object.Element, c object.Element) {
	if r != nil {
		e, c = r.e, r.c
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Iterator functions
/////////////////////////////////////////////////////////////////////////

// NewIterator returns a new iterator for the given parallel slices of exponents and coefficients, using the given exponent comparator cmp. The pairs of exponents and coefficients will be iterated over in increasing exponent order. Be sure to call Close on the iterator when finished to ensure that there are no memory leaks. Note that no attempt is made to exclude zero coefficients, or to sum repeated instances of the same exponent. Assumes that the input slices are sorted in increasing exponent order.
func NewIterator(Es []exponents.Interface, Cs []coefficients.Interface, cmp compare.CmpFunc) (*Iterator, error) {
	// Create the underlying iterator
	r, err := newIterator(Es, Cs, cmp)
	if err != nil {
		return nil, err
	}
	// Create the main iterator
	k := &Iterator{
		cl: newCircularList(defaultBufSize),
	}
	k.wc = sync.NewCond(&k.m)
	k.rc = sync.NewCond(&k.m)
	// Set the background worker running and return
	go k.run(r, cmp)
	return k, nil
}

// markAsClosed records the given error (if any) and marks the iterator as closed.
func (k *Iterator) markAsClosed(err error) {
	k.m.Lock()
	k.err = err
	k.isClosed = true
	k.rc.Broadcast()
	k.m.Unlock()
}

// run is the background worker for the iterator.
func (k *Iterator) run(r *iterator, cmp compare.CmpFunc) {
	// Iterate over the underlying iterator
	ok := true
	for ok && r.HasNext() {
		// Fetch the next pair of exponent and coefficient
		e, c := r.Next()
		// Acquire a lock on the mutex
		k.m.Lock()
		// Wait for there to be space (or to be asked to exit)
		for !k.exit && k.cl.IsFull() {
			k.wc.Wait()
		}
		if k.exit {
			// We've been asked to exit
			ok = false
		} else {
			// Add the data and wake up any waiting go-routines
			k.cl.Append(e, c)
			k.rc.Broadcast()
		}
		// Release the lock on the mutex
		k.m.Unlock()
	}
	// Close the underlying iterator
	k.markAsClosed(r.Err())
}

// Close closes the iterator and returns the last error encountered during iteration (if any).
func (k *Iterator) Close() error {
	// Is there anything to do?
	if k == nil {
		return nil
	}
	// Acquire a lock on the mutex
	k.m.Lock()
	// Ask the worker to exit
	k.exit = true
	k.wc.Broadcast()
	for !k.isClosed {
		k.rc.Wait()
	}
	// Release the lock and return any error
	err := k.err
	k.m.Unlock()
	return err
}

// HasNext returns true iff there is a next element in the iterator.
func (k *Iterator) HasNext() bool {
	// Is there anything to do?
	if k == nil {
		return false
	}
	// Acquire a lock on the mutex
	k.m.Lock()
	// Wait for new data to become available, or for the iterator to close
	for !k.isClosed && k.cl.IsEmpty() {
		k.rc.Wait()
	}
	// Pop the next pair of exponent and coefficient from the list and
	// wake-up the worker
	var hasNext bool
	k.e, k.c, hasNext = k.cl.Pop()
	k.wc.Broadcast()
	// Release the lock and return the result
	k.m.Unlock()
	return hasNext
}

// Next returns the next pair of exponent e and coefficient c in the iterator. You must call HasNext before calling Next.
func (k *Iterator) Next() (e object.Element, c object.Element) {
	if k != nil {
		e, c = k.e, k.c
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// MergeAndSumIterator functions
/////////////////////////////////////////////////////////////////////////

// NewMergeAndSumIterator returns a new iterator for the given parallel slices of exponents and coefficients. The pairs of exponents and coefficients will be iterated over in increasing exponent order. Duplicate exponents will have their coefficients summed, and any exponent with zero coefficient will be skipped. Be sure to call Close on the iterator when finished to ensure that there are no memory leaks. Assumes that the input slices are sorted in increasing exponent order. Here L is the parent of the exponents in Es, and K is the parent of the coefficients in Cs.
func NewMergeAndSumIterator(Es []exponents.Interface, Cs []coefficients.Interface, L monomialorder.Interface, K ring.Interface) (*MergeAndSumIterator, error) {
	// Create the underlying iterator and peek ahead
	k, err := NewIterator(Es, Cs, L.Cmp)
	if err != nil {
		return nil, err
	}
	hasNext := k.HasNext()
	nextE, nextC := k.Next()
	// Create and return the iterator
	return &MergeAndSumIterator{
		k:       k,
		esK:     L,
		csK:     K,
		hasNext: hasNext,
		nextE:   nextE,
		nextC:   nextC,
	}, nil
}

// Close closes the iterator and returns the last error encountered during iteration (if any).
func (m *MergeAndSumIterator) Close() error {
	if m == nil {
		return nil
	}
	return m.k.Close()
}

// HasNext returns true iff there is a next element in the iterator.
func (m *MergeAndSumIterator) HasNext() bool {
	// Is there anything to do?
	if m == nil || !m.hasNext {
		return false
	}
	// Zero the current exponent and coefficient
	m.curE, m.curC = nil, nil
	// Loop until we have a non-zero coefficient, or iteration is over
	isZero := true
	for isZero && m.hasNext {
		// Start merging the coefficients
		areEqual := true
		curE, curC := m.nextE, m.nextC
		for areEqual {
			if m.k.HasNext() {
				e, c := m.k.Next()
				if eq, err := m.esK.AreEqual(curE, e); err != nil {
					// THINK
					panic(err)
				} else if !eq {
					m.nextE, m.nextC = e, c
					areEqual = false
				} else if curC, err = m.csK.Add(curC, c); err != nil {
					// THINK
					panic(err)
				}
			} else {
				m.hasNext = false
				areEqual = false
			}
		}
		// Is the coefficient zero?
		var err error
		if isZero, err = m.csK.IsZero(curC); err != nil {
			// THINK
			panic(err)
		} else if !isZero {
			m.curE, m.curC = curE, curC
		}
	}
	return !isZero
}

// Next returns the next pair of exponent e and coefficient c in the iterator. You must call HasNext before calling Next.
func (m *MergeAndSumIterator) Next() (e object.Element, c object.Element) {
	if m != nil {
		e, c = m.curE, m.curC
	}
	return
}
