// Subtract defines internal functions for use when subtracting two polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/recursive"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

// maxAddLen is the maximum number of coefficients before addition and subtraction is done by splitting.
const maxAddLen = 250

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// subtractInRange returns the difference f - g, where f is the polynomial defined by the parallel slices esf[start:finish] and csf[start:finish] of exponents and coefficients, and g is defined by esg and csg. The difference is returned as two parallel slices of exponents and coefficients.
func subtractInRange(esf exponents.Interface, csf coefficients.Interface, start int, finish int, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Guess the capacity of the new slice.
	lenf, leng := finish-start, esg.Len()
	capacity := lenf + leng
	if capacity < lenf {
		capacity = leng // Looks ominous: we overflowed an int
	}
	// Create the destination exponent and coefficient slices
	destEs, destCs := newEs(capacity), newCs(capacity)
	// Note the common parent for the exponents and for the coefficients
	L := destEs.Universe()
	K := destCs.Universe()
	// Initialize our data
	idxf, idxg := start, 0
	var ef, eg object.Element
	if lenf > 0 {
		ef = esf.Entry(idxf)
	}
	if leng > 0 {
		eg = esg.Entry(idxg)
	}
	// Subtract termwise in increasing order
	for idxf < finish && idxg < leng {
		// Compare the two exponents
		sgn, err := L.Cmp(ef, eg)
		if err != nil {
			return nil, nil, err
		}
		// How we proceed depends on the sign
		if sgn < 0 {
			// ef is the smaller of the two exponents
			destEs = destEs.Append(ef)
			destCs = destCs.Append(csf.Entry(idxf))
			// Move on
			if idxf++; idxf < finish {
				ef = esf.Entry(idxf)
			}
		} else if sgn > 0 {
			// eg is the smaller of the two exponents
			destEs = destEs.Append(eg)
			x, err := K.Negate(csg.Entry(idxg))
			if err != nil {
				return nil, nil, err
			}
			destCs = destCs.Append(x)
			// Move on
			if idxg++; idxg < leng {
				eg = esg.Entry(idxg)
			}
		} else {
			// The exponents are equal, so subtract the coefficients
			if c, err := K.Subtract(csf.Entry(idxf), csg.Entry(idxg)); err != nil {
				return nil, nil, err
			} else if isZero, err := K.IsZero(c); err != nil {
				return nil, nil, err
			} else if !isZero {
				destEs = destEs.Append(eg)
				destCs = destCs.Append(c)
			}
			// Move on
			if idxf++; idxf < finish {
				ef = esf.Entry(idxf)
			}
			if idxg++; idxg < leng {
				eg = esg.Entry(idxg)
			}
		}
	}
	// Append any remaining terms in degrees larger than that of the smaller
	// degree polynomial
	if idxf < finish {
		destEs = exponents.AppendSlice(destEs, esf.Slice(idxf, finish))
		destCs = coefficients.AppendSlice(destCs, csf.Slice(idxf, finish))
	} else if idxg < leng {
		destEs = exponents.AppendSlice(destEs, esg.Slice(idxg, leng))
		for ; idxg < leng; idxg++ {
			x, err := K.Negate(csg.Entry(idxg))
			if err != nil {
				return nil, nil, err
			}
			destCs = destCs.Append(x)
		}
	}
	// Return the exponents and coefficients
	return destEs, destCs, nil
}

// createSubtractFunc returns the function that calculates the difference f - g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg.
func createSubtractFunc(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface) recursive.Func {
	return func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
		return subtractInRange(esf, csf, start, finish, esg, csg, newEs, newCs)
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Subtract returns the difference f - g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The difference is returned as two parallel slices of exponents and coefficients.
func Subtract(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	lenf, leng := esf.Len(), esg.Len()
	if lenf != csf.Len() || leng != csg.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Perform the calculation
	return recursive.Run(lenf, maxAddLen, newEs, newCs, createSubtractFunc(esf, csf, esg, csg))
}
