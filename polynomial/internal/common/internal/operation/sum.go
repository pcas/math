// Sum calculates the sum of a slice of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	"bitbucket.org/pcas/math/errors"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/recursive"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createSumFunc returns the function that calculates the sum of the polynomials defined by the parallel slices Es and Cs, where Es[i] and Cs[i] are parallel slices of exponents and coefficients for the i-th polynomial.
func createSumFunc(Es []exponents.Interface, Cs []coefficients.Interface) recursive.Func {
	return func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
		// How we proceed depends on the number of terms to consider
		switch finish - start {
		case 0:
			// Return the zero polynomial
			return newEs(0), newCs(0), nil
		case 1:
			// There is only one polynomial
			return Es[start].Copy(), Cs[start].Copy(), nil
		case 2:
			// Add two polynomials
			return Add(Es[start], Cs[start], Es[start+1], Cs[start+1], newEs, newCs)
		}
		// We add the polynomials in sequence
		fes, fcs, err := Add(Es[start], Cs[start], Es[start+1], Cs[start+1], newEs, newCs)
		if err != nil {
			return nil, nil, err
		}
		for i, ges := range Es[start+2 : finish] {
			if fes, fcs, err = Add(fes, fcs, ges, Cs[start+2+i], newEs, newCs); err != nil {
				return nil, nil, err
			}
		}
		return fes, fcs, nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum returns the sum of the polynomials described by the parallel slices Es and Cs, where the i-th entry Es[i] and Cs[i] in each slice are parallel slices describing the exponents and coefficients, respectively, of a polynomial. The sum is returned as two parallel slices of exponents and coefficients. The slices Es and Cs are assumed to be of the same length.
func Sum(Es []exponents.Interface, Cs []coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	n := len(Es)
	if n != len(Cs) {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Check that the lengths of the slices agree
	for i, es := range Es {
		if es.Len() != Cs[i].Len() {
			return nil, nil, errors.SliceLengthNotEqual.New()
		}
	}
	// Do the length 0, 1, and 2 cases
	if n == 0 {
		return newEs(0), newCs(0), nil
	} else if n == 1 {
		return Es[0].Copy(), Cs[0].Copy(), nil
	} else if n == 2 {
		return Add(Es[0], Cs[0], Es[1], Cs[1], newEs, newCs)
	}
	// Perform the calculation
	return recursive.Run(n, 2, newEs, newCs, createSumFunc(Es, Cs))
}
