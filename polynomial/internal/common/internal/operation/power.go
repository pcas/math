// Power calculates the power of a multivariate polynomial.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcastools/sort"
)

// parallelSlices represents parallel slices of exponents and coefficients describing a polynomial f.
type parallelSlices struct {
	es  exponents.Interface    // The exponents
	cs  coefficients.Interface // The coefficients
	err error                  // A possible error
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// extractSlicesFromParallelSlicesChannel extracts and returns the parallel slices from the channel c. Note: We read num+1 results from c before returning.
func extractSlicesFromParallelSlicesChannel(num int64, c <-chan *parallelSlices) ([]exponents.Interface, []coefficients.Interface, error) {
	// Note the capacity
	capacity := int(num + 1)
	if capacity < 0 {
		capacity = 10 // This is a bad sign -- we overflowed
	}
	// Create the destination slices
	Es := make([]exponents.Interface, 0, capacity)
	Cs := make([]coefficients.Interface, 0, capacity)
	// Start extracting the data
	var err error
	for i := int64(0); i <= num; i++ {
		result := <-c
		if result.err != nil {
			// Set a background go-routine draining the channel and return the
			// error
			go func() {
				for j := i + 1; j <= num; j++ {
					_ = <-c
				}
			}()
			return nil, nil, err
		}
		// Append the results
		Es, Cs = append(Es, result.es), append(Cs, result.cs)
	}
	return Es, Cs, nil
}

// powerTerm returns the power c^k * x^(mk). Returns parallel slices of exponents and coefficients on success.
func powerTerm(m object.Element, c object.Element, k *integer.Element, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Create the destination slice of exponents and coefficients
	destEs, destCs := newEs(1), newCs(1)
	// Calculate c^k
	K := destCs.Universe()
	d, err := K.Power(c, k)
	if err != nil {
		return nil, nil, err
	} else if isZero, err := K.IsZero(d); err != nil {
		return nil, nil, err
	} else if isZero {
		// This result is zero
		return destEs, destCs, nil
	}
	// Calculate x^m
	n, err := destEs.Universe().ScalarMultiplyByInteger(k, m)
	if err != nil {
		return nil, nil, err
	}
	return destEs.Append(n), destCs.Append(d), nil
}

// powerTwoTerms returns the power (cx^m+dx^n)^k. Returns parallel slices of exponents and coefficients on success. Assumes that k is a non-negative integer.
func powerTwoTerms(m object.Element, c object.Element, n object.Element, d object.Element, k int64, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	if k < 0 {
		return nil, nil, errors.IllegalNegativeExponent.New()
	}
	// Guess the capacity
	capacity := int(k + 1)
	if capacity < 1 || capacity > 1000 {
		capacity = 500 // Doesn't look promising
	}
	// Create the destination exponent and coefficient slices
	destEs, destCs := newEs(capacity), newCs(capacity)
	// Note the common parents for the exponents and coefficients
	L := destEs.Universe()
	K := destCs.Universe()
	// Populate the slices
	for i := int64(0); i <= k; i++ {
		// Convert i and k - i to integer elements
		ii := integer.FromInt64(i)
		ki := integer.FromInt64(k - i)
		// Compute the coefficient (k \choose i) * c^i * d^(k - i)
		if cc, err := K.Power(c, ii); err != nil {
			return nil, nil, err
		} else if dd, err := K.Power(d, ki); err != nil {
			return nil, nil, err
		} else if cc, err = K.Multiply(cc, dd); err != nil {
			return nil, nil, err
		} else if cc, err = K.ScalarMultiplyByInteger(integer.BinomialInt64(k, i), cc); err != nil {
			return nil, nil, err
		} else if isZero, err := K.IsZero(cc); err != nil {
			return nil, nil, err
		} else if !isZero {
			// Compute the exponent i * m + (k - i) * n
			var e object.Element
			if s1, err := L.ScalarMultiplyByInteger(ii, m); err != nil {
				return nil, nil, err
			} else if s2, err := L.ScalarMultiplyByInteger(ki, n); err != nil {
				return nil, nil, err
			} else if e, err = L.Add(s1, s2); err != nil {
				return nil, nil, err
			}
			// Append the exponent and coefficient to the slices
			destEs, destCs = destEs.Append(e), destCs.Append(cc)
		}
	}
	// Parallel sort the slices in increasing exponent order
	sort.ParallelSort(destEs, destCs)
	// Return the slices
	return destEs, destCs, nil
}

// powerGeneral recursively computes the power h^k, where h is the polynomial described by the parallel slices of exponents es and coefficients cs. Returns parallel slices of exponents and coefficients on success. Assumes that k is a non-negative integer.
func powerGeneral(es exponents.Interface, cs coefficients.Interface, k int64, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// The approach is that we consider (f(x) + g(x))^k and calculate the
	// binomial expansion of this, recursing on the terms. Begin by splitting
	// in the middle to form our new polynomials, f and g.
	size := es.Len()
	n := size / 2
	fes := es.Slice(0, n).(exponents.Interface)
	fcs := cs.Slice(0, n).(coefficients.Interface)
	ges := es.Slice(n, size).(exponents.Interface)
	gcs := cs.Slice(n, size).(coefficients.Interface)
	// Create the communication channel
	resC := make(chan *parallelSlices)
	// Start computing (k \choose i) * f^i * g^(k-i) in separate go-routines
	for i := int64(0); i <= k; i++ {
		go powerPolynomialIntermediateTermToChannel(fes, fcs, ges, gcs, k, i, newEs, newCs, resC)
	}
	// Extract the data from the channels
	Es, Cs, err := extractSlicesFromParallelSlicesChannel(k, resC)
	// Handle any errors
	if err != nil {
		return nil, nil, err
	}
	// Finally we need to sum together the intermediate results for our answer
	return Sum(Es, Cs, newEs, newCs)
}

// powerPolynomialIntermediateTermToChannel wraps powerPolynomialIntermediateTerm. If feeds the results down the channel resC. This does NOT close the channel on return.
func powerPolynomialIntermediateTermToChannel(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, k int64, i int64, newEs exponents.NewFunc, newCs coefficients.NewFunc, resC chan<- *parallelSlices) {
	destEs, destCs, err := powerPolynomialIntermediateTerm(esf, csf, esg, csg, k, i, newEs, newCs)
	resC <- &parallelSlices{es: destEs, cs: destCs, err: err}
}

// powerPolynomialIntermediateTerm calculates (k \choose i) * f^i * g^(k-i), as required by powerPolynomial.
func powerPolynomialIntermediateTerm(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, k int64, i int64, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Create the communication channels
	leftRes, rightRes := make(chan *parallelSlices), make(chan *parallelSlices)
	// Recursively start calculating f^i and g^(k-i)
	go powerPolynomialToChannel(esf, csf, i, newEs, newCs, leftRes)
	go powerPolynomialToChannel(esg, csg, k-i, newEs, newCs, rightRes)
	// Calculate the binomial coefficient
	bin := integer.BinomialInt64(k, i)
	// Extract the results
	l, r := <-leftRes, <-rightRes
	// Check for any errors
	if l.err != nil {
		return nil, nil, l.err
	} else if r.err != nil {
		return nil, nil, r.err
	}
	// Multiply the results...
	destEs, destCs, err := Multiply(l.es, l.cs, r.es, r.cs, newEs, newCs)
	if err != nil {
		return nil, nil, err
	}
	// ...and multiply this by the binomial coefficient
	return ScalarMultiplyByInteger(bin, destEs, destCs, newEs, newCs)
}

// powerPolynomialToChannel wraps PowerInt64. It feeds the result down the  channel resC, then closes the channel.
func powerPolynomialToChannel(es exponents.Interface, cs coefficients.Interface, k int64, newEs exponents.NewFunc, newCs coefficients.NewFunc, resC chan<- *parallelSlices) {
	defer close(resC)
	destEs, destCs, err := PowerInt64(es, cs, k, newEs, newCs)
	resC <- &parallelSlices{es: destEs, cs: destCs, err: err}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// PowerInt64 returns f^k. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The power is returned as two parallel slices of exponents and coefficients.
func PowerInt64(es exponents.Interface, cs coefficients.Interface, k int64, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Fast-track the easy cases
	if k == 0 {
		// Create the zero exponent with coefficient one
		destEs := newEs(1).Append(es.Universe().Zero())
		destCs := newCs(1).Append(cs.Universe().One())
		return destEs, destCs, nil
	} else if k == 1 {
		// Return the slices es and cs
		return es.Copy(), cs.Copy(), nil
	} else if size == 0 {
		if k < 0 {
			return nil, nil, errors.DivisionByZero.New()
		}
		// Return the zero term
		return newEs(0), newCs(0), nil
	} else if size == 1 {
		// Return (cx^m)^k
		return powerTerm(es.Entry(0), cs.Entry(0), integer.FromInt64(k), newEs, newCs)
	} else if k < 0 {
		// We can't do f^-k in general
		return nil, nil, errors.IllegalNegativeExponent.New()
	} else if size == 2 {
		// Return (cx^m+dx^n)^k
		return powerTwoTerms(es.Entry(0), cs.Entry(0), es.Entry(1), cs.Entry(1), k, newEs, newCs)
	}
	// Compute the power
	return powerGeneral(es, cs, k, newEs, newCs)
}

// Power returns f^k. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The power is returned as two parallel slices of exponents and coefficients.
func Power(es exponents.Interface, cs coefficients.Interface, k *integer.Element, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Fast-track the easy cases
	if k.IsZero() {
		// Create the zero exponent with coefficient one
		destEs := newEs(1).Append(es.Universe().Zero())
		destCs := newCs(1).Append(cs.Universe().One())
		return destEs, destCs, nil
	} else if k.IsOne() {
		// Return the slices es and cs
		return es.Copy(), cs.Copy(), nil
	} else if size == 0 {
		if k.IsNegative() {
			return nil, nil, errors.DivisionByZero.New()
		}
		// Return the zero term
		return newEs(0), newCs(0), nil
	} else if size == 1 {
		// Return (cx^m)^k
		return powerTerm(es.Entry(0), cs.Entry(0), k, newEs, newCs)
	} else if k.IsNegative() {
		// We can't do f^-k in general
		return nil, nil, errors.IllegalNegativeExponent.New()
	}
	// Convert the exponent to an int64
	kk, err := k.Int64()
	if err != nil {
		return nil, nil, errors.ExponentTooLarge.New()
	}
	if size == 2 {
		// Return (cx^m+dx^n)^k
		return powerTwoTerms(es.Entry(0), cs.Entry(0), es.Entry(1), cs.Entry(1), kk, newEs, newCs)
	}
	// Compute the power
	return powerGeneral(es, cs, kk, newEs, newCs)
}
