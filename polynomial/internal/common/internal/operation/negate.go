// Negate defines internal functions for use when negating a polynomial.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Negate returns the negation -f, where f is the polynomial defined by the slice cs of coefficients. The coefficients of -f will be returned.
func Negate(cs coefficients.Interface, newCs coefficients.NewFunc) (coefficients.Interface, error) {
	// Create the destination coefficient slice and note the common parent
	size := cs.Len()
	destCs := newCs(size)
	K := destCs.Universe()
	// Negate the coefficients
	for i := 0; i < size; i++ {
		c, err := K.Negate(cs.Entry(i))
		if err != nil {
			return nil, err
		}
		destCs = destCs.Append(c)
	}
	return destCs, nil
}
