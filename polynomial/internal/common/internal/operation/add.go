// Add defines internal functions for use when adding two polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"runtime"
	"sync"
)

// maxAddSubtractTerms is the maximum number of terms before we consider doing the addition/subtraction in parallel.
const maxAddSubtractTerms = 350

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// chooseNumWorkers picks the number of workers to use to process the data of size "size", where the hope is that the job can be broken up into pieces of size "target".
func chooseNumWorkers(size int, target int) int {
	if size <= target {
		return 1
	}
	numWorkers := size / target
	if size%target != 0 {
		numWorkers++
	}
	if maxProcs := runtime.NumCPU(); numWorkers > maxProcs {
		numWorkers = maxProcs
	}
	return numWorkers
}

// indicesForCommonRange returns the start and finish indices of es such that
//		es[i] < m1, 		for 0 <= i < start;
//		m1 <= es[i] <= m2, 	for start <= i < finish;
//		es[i] > m2, 		for finish <= i < len(es).
func indicesForCommonRange(es exponents.Interface, m1 object.Element, m2 object.Element) (start int, finish int, err error) {
	// Find the index of m1 and m2 in es
	if start, err = exponents.Search(es, m1); err != nil {
		return
	} else if finish, err = exponents.Search(es, m2); err != nil {
		return
	}
	// If es[finish] is equal to m2 we need to increment finish by one
	if finish < es.Len() {
		var eq bool
		if eq, err = es.Universe().AreEqual(es.Entry(finish), m2); err != nil {
			return
		} else if eq {
			finish++
		}
	}
	return
}

// indicesForg returns the start and finish indices of esg that contain esf[start:finish].
func indicesForg(esf exponents.Interface, esg exponents.Interface, start int, finish int) (startg int, finishg int, err error) {
	if start == 0 {
		if _, finishg, err = indicesForCommonRange(esg, esf.Entry(0), esf.Entry(finish-1)); err != nil {
			return
		}
	} else {
		m := esf.Entry(start - 1)
		if startg, finishg, err = indicesForCommonRange(esg, m, esf.Entry(finish-1)); err != nil {
			return
		} else {
			var areEqual bool
			areEqual, err = esg.Universe().AreEqual(esg.Entry(startg), m)
			if err != nil {
				return
			} else if areEqual {
				startg++
			}
		}
	}
	if finish == esf.Len() {
		finishg = esg.Len()
	}
	return
}

// addTerm returns the sum f + c * x^m, where f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The sum is returns as two parallel slices of exponents and coefficients.
func addTerm(es exponents.Interface, cs coefficients.Interface, m object.Element, c object.Element, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// How we proceed depends on the location in f in which the term should be
	// placed
	idx, err := exponents.Search(es, m)
	if err != nil {
		return nil, nil, err
	}
	// Does the exponent already exist in f?
	if size := es.Len(); idx < size {
		if eq, err := es.Universe().AreEqual(es.Entry(idx), m); err != nil {
			return nil, nil, err
		} else if eq {
			// Yes. We need to sum the coefficients -- how we proceed depends on
			// whether the sum is zero or not.
			K := cs.Universe()
			if c, err = K.Add(c, cs.Entry(idx)); err != nil {
				return nil, nil, err
			} else if isZero, err := K.IsZero(c); err != nil {
				return nil, nil, err
			} else if isZero {
				// The sum is zero, so exclude the term
				return exponents.Remove(es.Copy(), idx),
					coefficients.Remove(cs.Copy(), idx), nil
			}
			// If we're here then the sum is non-zero
			return es.Copy(), coefficients.Set(cs.Copy(), c, idx), nil
		}
	}
	// The exponent is not already in f, so we need to insert the term in f
	return exponents.Insert(es.Copy(), m, idx),
		coefficients.Insert(cs.Copy(), c, idx), nil
}

// addPolynomials returns the sum f + g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and g is similarly defined by esg and csg. The sum is returned as two parallel slices of exponents and coefficients.
func addPolynomials(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Guess the capacity of the destination slice
	lenf, leng := esf.Len(), esg.Len()
	capacity := lenf + leng
	if capacity < lenf {
		capacity = lenf // Looks ominous: we overflowed an int
	}
	// Create the destination slices
	destEs, destCs := newEs(capacity), newCs(capacity)
	// Note the common parent for the exponents and for the coefficients
	L, K := destEs.Universe(), destCs.Universe()
	// Initialize our data
	idxf, idxg := 0, 0
	ef, eg := esf.Entry(0), esg.Entry(0)
	// Add termwise in increasing order
	for idxf < lenf && idxg < leng {
		// Compare the two exponents
		sgn, err := L.Cmp(ef, eg)
		if err != nil {
			return nil, nil, err
		}
		// How we proceed depends on the sign
		if sgn < 0 {
			// ef is the smaller of the two exponents
			destEs = destEs.Append(ef)
			destCs = destCs.Append(csf.Entry(idxf))
			// Move on
			if idxf++; idxf < lenf {
				ef = esf.Entry(idxf)
			}
		} else if sgn > 0 {
			// eg is the smaller of the two exponents
			destEs = destEs.Append(eg)
			destCs = destCs.Append(csg.Entry(idxg))
			// Move on
			if idxg++; idxg < leng {
				eg = esg.Entry(idxg)
			}
		} else {
			// The exponents are equal, so add the coefficients
			if c, err := K.Add(csf.Entry(idxf), csg.Entry(idxg)); err != nil {
				return nil, nil, err
			} else if isZero, err := K.IsZero(c); err != nil {
				return nil, nil, err
			} else if !isZero {
				destEs = destEs.Append(eg)
				destCs = destCs.Append(c)
			}
			// Move on
			if idxf++; idxf < lenf {
				ef = esf.Entry(idxf)
			}
			if idxg++; idxg < leng {
				eg = esg.Entry(idxg)
			}
		}
	}
	// Append any remaining terms in degrees larger than that of the smaller
	// degree polynomial and return
	if idxf < lenf {
		destEs = exponents.AppendSlice(destEs, esf.Slice(idxf, lenf))
		destCs = coefficients.AppendSlice(destCs, csf.Slice(idxf, lenf))
	} else if idxg < leng {
		destEs = exponents.AppendSlice(destEs, esg.Slice(idxg, leng))
		destCs = coefficients.AppendSlice(destCs, csg.Slice(idxg, leng))
	}
	return destEs, destCs, nil
}

// addInRange returns the partial sum f[start:finish] + g[start':finish'], where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and g is defined by esg and csg. The difference is returned as two parallel slices of exponents and coefficients.
func addInRange(start int, finish int, esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// First we need to figure out what slice of g to consider
	startg, finishg, err := indicesForg(esf, esg, start, finish)
	if err != nil {
		return nil, nil, err
	}
	// Restrict f and g to the correct subslices
	esf = esf.Slice(start, finish).(exponents.Interface)
	csf = csf.Slice(start, finish).(coefficients.Interface)
	esg = esg.Slice(startg, finishg).(exponents.Interface)
	csg = csg.Slice(startg, finishg).(coefficients.Interface)
	// Perform the addition
	return addPolynomials(esf, csf, esg, csg, newEs, newCs)
}

// addInRangeAndRecord saves the partial sum f[start:finish] + g[start':finish'] in Es[n] and Cs[n] (as parallel slices of exponents and coefficients), with access controlled by the given mutex m. Any error will be recorded in err. The wait group wg will be notified on completion.
func addInRangeAndRecord(n int, start int, finish int, esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc, m *sync.Mutex, Es []exponents.Interface, Cs []coefficients.Interface, err *error, wg *sync.WaitGroup) {
	es, cs, myerr := addInRange(start, finish, esf, csf, esg, csg, newEs, newCs)
	m.Lock()
	if myerr != nil {
		*err = myerr
	} else {
		Es[n] = es
		Cs[n] = cs
	}
	m.Unlock()
	wg.Done()
}

// add returns the sum f + g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and g is defined by esg and csg. The sum is returned as two parallel slices of exponents and coefficients.
func add(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Make sure that lenf <= leng
	lenf, leng := esf.Len(), esg.Len()
	if lenf > leng {
		esf, esg = esg, esf
		csf, csg = csg, csf
		lenf, leng = leng, lenf
	}
	// Get the easy cases out of the way
	if lenf == 0 {
		// f == 0 so return g
		return esg.Copy(), csg.Copy(), nil
	} else if lenf == 1 {
		// f is a term, so insert it into g in the correct place
		return addTerm(esg, csg, esf.Entry(0), csf.Entry(0), newEs, newCs)
	}
	// Decide on the number of workers
	numWorkers := chooseNumWorkers(lenf+leng, maxAddSubtractTerms)
	// If there's only one worker, do it in place
	if numWorkers == 1 {
		return addInRange(0, lenf, esf, csf, esg, csg, newEs, newCs)
	}
	// Allocate the data used by the workers
	var err error
	var m sync.Mutex
	Es := make([]exponents.Interface, numWorkers)
	Cs := make([]coefficients.Interface, numWorkers)
	// Set the workers working and wait for them to finish
	var wg sync.WaitGroup
	wg.Add(numWorkers)
	stepSize := lenf / numWorkers
	for i := 0; i < numWorkers-1; i++ {
		go addInRangeAndRecord(i, i*stepSize, (i+1)*stepSize, esf, csf, esg, csg, newEs, newCs, &m, Es, Cs, &err, &wg)
	}
	go addInRangeAndRecord(numWorkers-1, (numWorkers-1)*stepSize, lenf, esf, csf, esg, csg, newEs, newCs, &m, Es, Cs, &err, &wg)
	wg.Wait()
	// Handle any errors
	if err != nil {
		return nil, nil, err
	}
	// Figure out the capacity we need
	size := 0
	for _, es := range Es {
		size += es.Len()
	}
	// We can simply cat the results
	destEs, destCs := newEs(size), newCs(size)
	for i, es := range Es {
		destEs = exponents.AppendSlice(destEs, es)
		destCs = coefficients.AppendSlice(destCs, Cs[i])
	}
	return destEs, destCs, nil
}

/*
func temp(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {

	// Extract the common range
	startf, finishf, err := indicesForCommonRange(esf, esg.Entry(0), esg.Entry(leng-1))
	if err != nil {
		return nil, nil, err
	}
	startg, finishg, err := indicesForCommonRange(esg, esf.Entry(0), esf.Entry(lenf-1))
	if err != nil {
		return nil, nil, err
	}
	// Partition into the appropriate ranges
	var startes, startcs, finishes, finishcs slice.Interface
	if startf != 0 {
		if startg != 0 {
			panic("Impossible!")
		}
		startes = esf.Slice(0, startf)
		startcs = csf.Slice(0, startf)
	} else if startg != 0 {
		if startf != 0 {
			panic("Impossible!")
		}
		startes = esg.Slice(0, startg)
		startcs = csg.Slice(0, startg)
	}
	if finishf != lenf {
		if finishg != leng {
			panic("Impossible!")
		}
		finishes = esf.Slice(finishf, lenf)
		finishcs = csf.Slice(finishf, lenf)
	} else if finishg != leng {
		if finishf != lenf {
			panic("Impossible!")
		}
		finishes = esg.Slice(finishg, leng)
		finishcs = csg.Slice(finishg, leng)
	}
	esf = esf.Slice(startf, finishf).(exponents.Interface)
	csf = csf.Slice(startf, finishf).(coefficients.Interface)
	esg = esg.Slice(startg, finishg).(exponents.Interface)
	csg = csg.Slice(startg, finishg).(coefficients.Interface)
	// Perform the addition on the common range
	var es, cs slice.Interface
	if es, cs, err = add(esf, csf, esg, csg, newEs, newCs); err != nil {
		return nil, nil, err
	}
	// Collate the results
	size := startf + startg + finishf + finishg + es.Len()
	destEs, destCs := newEs(size), newCs(size)
	if startes != nil {
		destEs = exponents.AppendSlice(destEs, startes)
		destCs = coefficients.AppendSlice(destCs, startcs)
	}
	destEs = exponents.AppendSlice(destEs, es)
	destCs = coefficients.AppendSlice(destCs, cs)
	if finishes != nil {
		destEs = exponents.AppendSlice(destEs, finishes)
		destCs = coefficients.AppendSlice(destCs, finishcs)
	}
	// Return the sum
	return destEs, destCs, nil
}*/

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns the sum f + g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The sum is returned as two parallel slices of exponents and coefficients.
func Add(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	lenf, leng := esf.Len(), esg.Len()
	if lenf != csf.Len() || leng != csg.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Make sure that lenf <= leng
	if lenf > leng {
		esf, esg = esg, esf
		csf, csg = csg, csf
		lenf, leng = leng, lenf
	}
	// Get the easy cases out of the way
	if lenf == 0 {
		// f == 0 so return g
		return esg.Copy(), csg.Copy(), nil
	} else if lenf == 1 {
		// f is a term, so insert it into g in the correct place
		return addTerm(esg, csg, esf.Entry(0), csf.Entry(0), newEs, newCs)
	}
	// Partition f and g into a head, common overlap, and tail
	// FIXME: ...
	// Perform the addition on the common overlap
	es, cs, err := add(esf, csf, esg, csg, newEs, newCs)
	if err != nil {
		return nil, nil, err
	}
	// Combine the results with the head and tail of the partiton
	// FIXME: ...
	return es, cs, nil
}
