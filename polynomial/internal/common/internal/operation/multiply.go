// Multiply defines functions for use when multiplying two multivariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package operation

import (
	"bitbucket.org/pcas/math/abelianmonoid"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common/internal/recursive"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/ring"
)

// maxScalarMultiplyLen is the maximum number of coefficients before scalar multiplication is done by splitting.
const maxScalarMultiplyLen = 750

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isZeroElement returns true iff the element x is the additive identity element of its parent.
func isZeroElement(x object.Element) bool {
	K, ok := x.Parent().(abelianmonoid.Interface)
	if ok {
		isZero, err := K.IsZero(x)
		return err == nil && isZero
	}
	return false
}

// isOneElement returns true iff the element x is the multiplicative identity element of its parent.
func isOneElement(x object.Element) bool {
	R, ok := x.Parent().(ring.Interface)
	if ok {
		isOne, err := R.IsOne(x)
		return err == nil && isOne
	}
	return false
}

// createMultiplyFunc returns the function that calculates the product f * g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg.
func createMultiplyFunc(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface) recursive.Func {
	return func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
		// Create the destination slices of exponents and coefficients
		size := esg.Len()
		destEs := newEs(size * (finish - start))
		destCs := newCs(size * (finish - start))
		// Note the common parents for the exponents and coefficients
		L := destEs.Universe()
		K := destCs.Universe()
		// Work through the terms of f in the given range
		for i := start; i < finish; i++ {
			// Extract the i-th term c*x^m of f
			m, c := esf.Entry(i), csf.Entry(i)
			// Multiply every term in g by c*x^m
			for j := 0; j < size; j++ {
				// First multiply by the coefficient c...
				if d, err := K.Multiply(c, csg.Entry(j)); err != nil {
					return nil, nil, err
				} else if isZero, err := K.IsZero(d); err != nil {
					return nil, nil, err
				} else if !isZero {
					// ...and now multiply by the monomial x^m
					n, err := L.Add(m, esg.Entry(j))
					if err != nil {
						return nil, nil, err
					}
					// Append the resulting exponent and coefficient
					destEs, destCs = destEs.Append(n), destCs.Append(d)
				}
			}
		}
		// Return the slices
		return destEs, destCs, nil
	}
}

// createScalarMultiplyByIntegerFunc returns the function that calculates the scalar multiply a * f, where f is the polynomial defined by the parallel slices es and cs of exponents and coefficients, and a is an integer.
func createScalarMultiplyByIntegerFunc(a *integer.Element, es exponents.Interface, cs coefficients.Interface) recursive.Func {
	return func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
		// Create the destination slice of coefficients
		destCs := newCs(finish - start)
		// Note the common parent for the coefficients
		K := destCs.Universe()
		// Work through the terms of f in the given range
		for i := start; i < finish; i++ {
			// Multiply the coefficient by a and append it to the slice
			c, err := K.ScalarMultiplyByInteger(a, cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			destCs = destCs.Append(c)
		}
		// Return the slices
		destEs := es.Slice(start, finish).(exponents.Interface).Copy()
		return destEs, destCs, nil
	}
}

// createScalarMultiplyByCoefficientFunc returns the function that calculates the scalar multiply a * f, where f is the polynomial defined by the parallel slices es and cs of exponents and coefficients, and a is a coefficient.
func createScalarMultiplyByCoefficientFunc(a object.Element, es exponents.Interface, cs coefficients.Interface) recursive.Func {
	return func(start int, finish int, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
		// Create the destination slice of coefficients
		destCs := newCs(finish - start)
		// Note the common parent for the coefficients
		K := destCs.Universe()
		// Work through the terms of f in the given range
		for i := start; i < finish; i++ {
			// Multiply the coefficient by a and append it to the slice
			c, err := K.Multiply(a, cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			destCs = destCs.Append(c)
		}
		// Return the slices
		destEs := es.Slice(start, finish).(exponents.Interface).Copy()
		return destEs, destCs, nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Multiply returns the product f * g, where f is the polynomial defined by the parallel slices esf and csf of exponents and coefficients, and similarly g is defined by esg and csg. The product is returned as two parallel slices of exponents and coefficients.
func Multiply(esf exponents.Interface, csf coefficients.Interface, esg exponents.Interface, csg coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	lenf, leng := esf.Len(), esg.Len()
	if lenf != csf.Len() || leng != csg.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Get the zero case out of the way
	if lenf == 0 || leng == 0 {
		return newEs(0), newCs(0), nil
	}
	// Perform the calculation
	return recursive.Run(lenf, 1, newEs, newCs, createMultiplyFunc(esf, csf, esg, csg))
}

// ScalarMultiplyByInteger returns n * f, where this is defined to be f + ... + f (n times) if n is positive, -f - ... - f (|n| times) if n is negative, and 0 if n is zero. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The result is returned as two parallel slices of exponents and coefficients.
func ScalarMultiplyByInteger(n *integer.Element, es exponents.Interface, cs coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Get the zero and one cases out of the way
	if size == 0 || n.IsZero() {
		return newEs(0), newCs(0), nil
	} else if n.IsOne() {
		return es.Copy(), cs.Copy(), nil
	}
	// Perform the calculation
	return recursive.Run(size, maxScalarMultiplyLen, newEs, newCs, createScalarMultiplyByIntegerFunc(n, es, cs))
}

// ScalarMultiplyByCoefficient returns a * f, where a is an element in the coefficient ring and f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The result is returned as two parallel slices of exponents and coefficients.
func ScalarMultiplyByCoefficient(a object.Element, es exponents.Interface, cs coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Handle the case where either f = 0 or a = 0.
	if size == 0 || isZeroElement(a) {
		if !cs.Universe().Contains(a) {
			return nil, nil, errors.ArgNotContainedInCoefficientRing.New()
		}
		return newEs(0), newCs(0), nil
	}
	// Handle the case when a = 1.
	if isOneElement(a) {
		if !cs.Universe().Contains(a) {
			return nil, nil, errors.ArgNotContainedInCoefficientRing.New()
		}
		return es.Copy(), cs.Copy(), nil
	}
	// Perform the calculation
	return recursive.Run(size, maxScalarMultiplyLen, newEs, newCs, createScalarMultiplyByCoefficientFunc(a, es, cs))
}
