// Creation defines functions to help with the creation of multivariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package creation

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NthMonomial returns parallel slices of exponents and coefficients describing the N-th monomial x^k in the polynomial f, where the exponents of f are described by the given slice of exponents es.
func NthMonomial(N int, es exponents.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	if N < 0 || N >= es.Len() {
		return nil, nil, errors.InvalidIndexRange.New(N, es.Len()-1)
	}
	// Return the slices
	cs := newCs(1)
	return newEs(1).Append(es.Entry(N)), cs.Append(cs.Universe().One()), nil
}

// NthTerm returns parallel slices of exponents and coefficients describing the N-th term c * x^k in the polynomial f, where f is described by the given parallel slices of exponents es and coefficients cs.
func NthTerm(N int, es exponents.Interface, cs coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	} else if N < 0 || N >= size {
		return nil, nil, errors.InvalidIndexRange.New(N, size-1)
	}
	// Return the slices
	return newEs(1).Append(es.Entry(N)), newCs(1).Append(cs.Entry(N)), nil
}

// ToMonomial returns parallel slices of exponents and coefficients describing the monomial x^k.
func ToMonomial(k object.Element, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	destEs := newEs(1)
	m, err := destEs.Universe().ToElement(k)
	if err != nil {
		return nil, nil, err
	}
	destCs := newCs(1)
	return destEs.Append(m), destCs.Append(destCs.Universe().One()), nil
}

// ToTerm returns parallel slices of exponents and coefficients describing the term c * x^k, where c is an element in the coefficient ring.
func ToTerm(k object.Element, c object.Element, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Create the destination slices of exponents and coefficients
	destEs, destCs := newEs(1), newCs(1)
	// Convert the coefficient and check that it's non-zero
	K := destCs.Universe()
	d, err := K.ToElement(c)
	if err != nil {
		return nil, nil, err
	} else if isZero, err := K.IsZero(d); err != nil {
		return nil, nil, err
	} else if isZero {
		return destEs, destCs, nil
	}
	// Convert the exponent
	m, err := destEs.Universe().ToElement(k)
	if err != nil {
		return nil, nil, err
	}
	// Append the exponent and coefficient to the slice and return
	return destEs.Append(m), destCs.Append(d), nil
}
