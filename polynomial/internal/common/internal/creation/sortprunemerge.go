// Sortprunemerge defines functions to help with the creation of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package creation

import (
	"bitbucket.org/pcas/math/abelianmonoid"
	"bitbucket.org/pcas/math/errors"
	coefficients "bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	exponents "bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/sort"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// PruneZeroCoefficients takes parallel slices of exponents es and coefficients cs, and removes any entries with zero coefficient. The slices es and cs are assumed to be of the same length. Important: Note that this will MODIFY the arguments.
func PruneZeroCoefficients(es exponents.Interface, cs coefficients.Interface) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// Note the common parent for the coefficients
	K := cs.Universe()
	// Prune any entries with zero coefficient from the slices
	offset := 0
	for i := 0; i < size; i++ {
		if isZero, err := K.IsZero(cs.Entry(i)); err != nil {
			return nil, nil, err
		} else if isZero {
			offset++
		} else if offset != 0 {
			es.Swap(i-offset, i)
			cs.Swap(i-offset, i)
		}
	}
	// Truncate the slices (if necessary)
	if offset != 0 {
		es = es.Slice(0, size-offset).(exponents.Interface)
		cs = cs.Slice(0, size-offset).(coefficients.Interface)
	}
	return es, cs, nil
}

// SortExponentsAndCoefficients parallel sorts the given slices of exponents es and coefficients cs in strictly increasing exponent order. Note that this will NOT merge duplicate exponents. The slices es and cs are assumed to be of the same length. Important: Note that this will MODIFY the arguments.
func SortExponentsAndCoefficients(es exponents.Interface, cs coefficients.Interface) (exponents.Interface, coefficients.Interface, error) {
	// Sanity check
	if es.Len() != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	}
	// If necessary, parallel sort the slices in increasing exponent order
	if !exponents.IsSorted(es) {
		sort.ParallelSort(es, cs)
	}
	return es, cs, nil
}

// SortAndMergeExponentsAndCoefficients parallel sorts the given slices of exponents es and coefficients cs in strictly increasing exponent order. Any duplicate exponents are merged (and their coefficients summed). The slices es and cs are assumed to be of the same length. Important: Note that this will MODIFY the arguments.
func SortAndMergeExponentsAndCoefficients(es exponents.Interface, cs coefficients.Interface, newEs exponents.NewFunc, newCs coefficients.NewFunc) (exponents.Interface, coefficients.Interface, error) {
	// Parallel sort the slices in increasing exponent order
	var err error
	if es, cs, err = SortExponentsAndCoefficients(es, cs); err != nil {
		return nil, nil, err
	}
	// Find the first range of duplicate coefficient
	start, finish, err := exponents.RangeOfDuplicateExponent(es)
	if err != nil {
		return nil, nil, err
	}
	// Is there anything to do?
	if start < 0 {
		return es, cs, nil
	}
	// Create destination slices
	size := es.Len()
	destEs := newEs(size - finish + start + 1)
	destCs := newCs(size - finish + start + 1)
	// Note the common parent for the coefficients
	K := cs.Universe()
	// Loop until there are no more duplicates
	for start >= 0 {
		// Append up to the first duplicate
		if start > 0 {
			destEs = exponents.AppendSlice(destEs, es.Slice(0, start))
			destCs = coefficients.AppendSlice(destCs, cs.Slice(0, start))
		}
		// Sum the range of duplicates
		if c, err := abelianmonoid.Sum(K, slice.ToElementSlice(cs.Slice(start, finish))...); err != nil {
			return nil, nil, err
		} else if isZero, err := K.IsZero(c); err != nil {
			return nil, nil, err
		} else if !isZero {
			// The resulting coefficient is non-zero, so append it to the slices
			destEs, destCs = destEs.Append(es.Entry(start)), destCs.Append(c)
		}
		// Are we done?
		if finish == size {
			return destEs, destCs, nil
		}
		// Trim es and cs to start after the first duplicate and update the size
		es = es.Slice(finish, size).(exponents.Interface)
		cs = cs.Slice(finish, size).(coefficients.Interface)
		size = es.Len()
		// Move on
		start, finish, err = exponents.RangeOfDuplicateExponent(es)
		if err != nil {
			return nil, nil, err
		}
	}
	return destEs, destCs, nil
}
