// Derivative provides functions for computing partial derivatives of polynomials (when defined).

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package common

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

// derivativer is the interface satisfied by the Derivative method.
type derivativer interface {
	Derivative(k int, es exponents.Interface, cs coefficients.Interface, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) // Derivative returns the formal partial derivative df/dx_k of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
}

// nthDerivativer is the interface satisfied by the NthDerivative method.
type nthDerivativer interface {
	NthDerivative(n int, k int, es exponents.Interface, cs coefficients.Interface, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) // NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal partial derivative df/dx_k of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients. The exponent helper eh must satisfy one of the following two interfaces:
//		type Derivativer interface {
//			Derivative(k int, es exponents.Interface,
//				cs coefficients.Interface, ch coefficients.Helper)
//				(unwrappedexponents.Interface, unwrappedcoefficients.Interface,
//				 error) // Derivative returns the formal partial derivative
//				   df/dx_k of the polynomial f, where the variables x_i are
//				   indexed from 0. Here f is the polynomial defined by the
//				   parallel slices es and cs of exponents and coefficients. The
//				   partial derivative is returned as two parallel slices of
//				   exponents and coefficients.
//		}
//
//		type NthDerivativer interface {
//			NthDerivative(n int, k int, es exponents.Interface,
//				cs coefficients.Interface, ch coefficients.Helper)
//				(unwrappedexponents.Interface, unwrappedcoefficients.Interface,
//				 error) // NthDerivative returns the n-th formal partial
//				   derivative d^nf/dx_k^n of the polynomial f, where the
//				   variables x_i are indexed from 0. Here f is the polynomial
//				   defined by the parallel slices es and cs of exponents and
//				   coefficients. The partial derivative is returned as two
//				   parallel slices of exponents and coefficients.
//		}
func Derivative(k int, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	var err error
	var unwrappedEs unwrappedexponents.Interface
	var unwrappedCs unwrappedcoefficients.Interface
	// Compute the derivative
	if eeh, ok := eh.(derivativer); ok {
		unwrappedEs, unwrappedCs, err = eeh.Derivative(k, es, cs, ch)
	} else if eeh, ok := eh.(nthDerivativer); ok {
		unwrappedEs, unwrappedCs, err = eeh.NthDerivative(1, k, es, cs, ch)
	} else {
		err = errors.ParentDoesNotHaveDerivative.New()
	}
	// Handle any errors
	if err != nil {
		return nil, nil, err
	}
	// Wrap and return the results
	return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
}

// NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients. The exponent helper eh must satisfy either the Derivativer or NthDerivativer interfaces, as described in Derivative.
func NthDerivative(n int, k int, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// Compute the n-th derivative
	if eeh, ok := eh.(nthDerivativer); ok {
		// Compute the n-th derivative directly
		unwrappedEs, unwrappedCs, err := eeh.NthDerivative(n, k, es, cs, ch)
		if err != nil {
			return nil, nil, err
		}
		return wrapPair(unwrappedEs, unwrappedCs, eh, ch)
	} else if eeh, ok := eh.(derivativer); ok {
		// Compute the n-th derivative by calling Derivative n times
		for i := 0; i < n; i++ {
			unwrappedEs, unwrappedCs, err := eeh.Derivative(k, es, cs, ch)
			if err != nil {
				return nil, nil, err
			}
			es, cs, err = wrapPair(unwrappedEs, unwrappedCs, eh, ch)
			if err != nil {
				return nil, nil, err
			}
		}
		return es, cs, nil
	}
	// If we're here then we don't know what to do
	return nil, nil, errors.ParentDoesNotHaveDerivative.New()
}
