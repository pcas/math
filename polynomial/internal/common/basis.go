// Basis defines functions for working with the variable x_0,...,x_k in a polynomial ring (when defined).

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package common

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

// basiser is the interface satisfied by the Basis method.
type basiser interface {
	Basis() []unwrappedexponents.Interface // Basis returns a slice of slices of unwrapped exponents corresponding to the variables x_0,...,x_{rk-1} in the polynomial ring, where rk is the rank of the exponent universe.
}

// nthBasisElementer is the interface satisfied by the NthBasisElement method.
type nthBasisElementer interface {
	NthBasisElement(n int) (unwrappedexponents.Interface, error) // NthBasisElement returns a slice of unwrapped exponents corresponding to the n-th variable x_n in the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank of the exponent universe.
}

// isNthBasisElementer is the interface satisfied by the IsNthBasisElement method.
type isNthBasisElementer interface {
	IsNthBasisElement(n int, S unwrappedexponents.Interface) (bool, error) // IsNthBasisElement returns true iff the slice of unwrapped exponents S corresponds to the n-th variable x_n in the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank of the exponent universe.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Basis returns two parallel slices of exponents and coefficients corresponding to the variables x_0,...,x_k of the polynomial ring. The exponent helper eh must satisfy one of the following interfaces:
// 		type Basiser interface {
//			Basis() []unwrappedexponents.Interface // Basis returns a slice of
//			   slices of unwrapped exponents corresponding to the variables
//			   x_0,...,x_{rk-1} in the polynomial ring, where rk is the rank of
//			   the exponent universe.
//		}
//
//		type NthBasisElementer interface {
//			NthBasisElement(n int) (unwrappedexponents.Interface, error)
//				// NthBasisElement returns a slice of unwrapped exponents
//				   corresponding to the n-th variable x_n in the polynomial
//				   ring. Here variables are indexed from 0 to rk - 1
//				   (inclusive), where rk is the rank of the exponent universe.
//		}
func Basis(eh exponents.Helper, ch coefficients.Helper) ([]exponents.Interface, []coefficients.Interface, error) {
	// If eh has a Basis method, call that
	var unwrappedEs []unwrappedexponents.Interface
	if eeh, ok := eh.(basiser); ok {
		// Create the unwrapped exponents
		unwrappedEs = eeh.Basis()
	} else if eeh, ok := eh.(nthBasisElementer); ok {
		// Otherwise if eh has an NthBasisElement method, call that
		rk := eh.Universe().Rank()
		unwrappedEs = make([]unwrappedexponents.Interface, 0, rk)
		for i := 0; i < rk; i++ {
			es, err := eeh.NthBasisElement(i)
			if err != nil {
				return nil, nil, err
			}
			unwrappedEs = append(unwrappedEs, es)
		}
	} else {
		// Otherwise we're stuck
		return nil, nil, errors.BasisUndefined.New()
	}
	// Wrap the exponents
	size := len(unwrappedEs)
	ees := make([]exponents.Interface, 0, size)
	for _, es := range unwrappedEs {
		wrappedEs, err := eh.Wrap(es)
		if err != nil {
			return nil, nil, err
		}
		ees = append(ees, wrappedEs)
	}
	// Create the coefficients
	cs := coefficients.SliceContainingOne(ch)
	ccs := make([]coefficients.Interface, 0, size)
	for i := 0; i < size; i++ {
		ccs = append(ccs, cs)
	}
	// Return the slices
	return ees, ccs, nil
}

// NthBasisElement returns the parallel slices of exponents and coefficients corresponding to the n-th variable x_n of the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank. The exponent helper eh must satisfy either the Basiser or NthBasisElementer interfaces, as described in Basis.
func NthBasisElement(n int, eh exponents.Helper, ch coefficients.Helper) (exponents.Interface, coefficients.Interface, error) {
	// If eh has an NthBasisElement method, call that
	var unwrappedEs unwrappedexponents.Interface
	if eeh, ok := eh.(nthBasisElementer); ok {
		// Create the unwrapped exponents
		var err error
		unwrappedEs, err = eeh.NthBasisElement(n)
		if err != nil {
			return nil, nil, err
		}
	} else if eeh, ok := eh.(basiser); ok {
		// Otherwise if eh has a Basis method, call that
		basis := eeh.Basis()
		if rk := len(basis); n < 0 || n >= rk {
			return nil, nil, errors.InvalidIndexRange.New(n, rk-1)
		}
		unwrappedEs = basis[n]
	} else {
		// Otherwise we're stuck
		return nil, nil, errors.BasisUndefined.New()
	}
	// Wrap the exponents
	es, err := eh.Wrap(unwrappedEs)
	if err != nil {
		return nil, nil, err
	}
	// Return the slices
	return es, coefficients.SliceContainingOne(ch), nil
}

// IsNthBasisElement returns true iff the parallel slices of exponents es and coefficients cs correspond to the n-th variable x_n of the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank. If the exponent helper eh satisfies the interface:
//		type IsNthBasisElementer interface {
//			IsNthBasisElement(n int, S unwrappedexponents.Interface) (bool,
//				 error) // IsNthBasisElement returns true iff the slice of
//					 unwrapped exponents S corresponds to the n-th variable x_n
//					 in the polynomial ring. Here variables are indexed from 0
//					 to rk - 1 (inclusive), where rk is the rank of the
//					 exponent universe.
//		}
// then eh's IsNthBasisElement method will be called. Otherwise, the exponent helper eh must satisfy either the Basiser or NthBasisElementer interfaces, as described in Basis.
func IsNthBasisElement(n int, es exponents.Interface, cs coefficients.Interface, eh exponents.Helper, ch coefficients.Helper) (bool, error) {
	// Sanity check
	if es.Len() != cs.Len() {
		return false, errors.SliceLengthNotEqual.New()
	} else if rk := eh.Universe().Rank(); n < 0 || n >= rk {
		return false, errors.InvalidIndexRange.New(n, rk-1)
	} else if isMonomial, err := IsMonomial(cs); err != nil || !isMonomial {
		return isMonomial, err
	}
	// Does eh support this function directly?
	if eeh, ok := eh.(isNthBasisElementer); ok {
		return eeh.IsNthBasisElement(n, es.Unwrap())
	}
	// Recover the n-th basis element
	esb, csb, err := NthBasisElement(n, eh, ch)
	if err != nil {
		return false, err
	}
	// Perform the comparison
	return AreEqual(es, cs, esb, csb)
}
