// Converter provides a way of converting elements of slices using a ToWrappedElement method.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package converter

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
)

// ToUnwrappedElementer is the interface satisfied by the ToUnwrappedElement method.
type ToUnwrappedElementer interface {
	ToUnwrappedElement(x object.Element) (object.Element, error) // ToUnwrappedElement attempts to map the element x from the user-facing universe to the universe of the unwrapped coefficients.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new converter for the given slice S.
func New(c ToUnwrappedElementer, S slice.Interface) slice.Interface {
	// Create a generator whose generator function is the conversion function
	T, err := slice.NewGenerator(S.Len(), func(i int) object.Element {
		// First fetch the i-th entry from S
		x := S.Entry(i)
		// Now attempt to convert it to the unwrapped universe
		y, err := c.ToUnwrappedElement(x)
		if err != nil {
			panic(err)
		}
		return y
	})
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return T
}
