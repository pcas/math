// Exponents provides a way of converting between what the user sees as the monoid of exponents, and how this is implemented internally.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package exponents

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/polynomial/internal/internal/converter"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"github.com/pkg/errors"
	"runtime/debug"
)

// Interface is the interface satisfied by a slice of exponents.
type Interface interface {
	slice.Interface
	Universe() polynomial.ExponentMonoid  // Universe returns the user-facing parent of the exponents.
	Unwrap() unwrappedexponents.Interface // Unwrap returns the underlying internal slice of exponents.
}

// Helper defines the common interface for an exponent slice helper.
type Helper interface {
	Universe() polynomial.ExponentMonoid                         // Universe returns the user-facing parent of the exponents.
	New(capacity int) unwrappedexponents.Interface               // New returns a new slice of unwrapped exponents with given capacity.
	Wrap(S unwrappedexponents.Interface) (Interface, error)      // Wrap wraps the given slice of unwrapped exponents.
	ToUnwrappedElement(x object.Element) (object.Element, error) // ToUnwrappedElement attempts to map the element x from the user-facing universe to the universe of the unwrapped exponents.
}

// sliceToUnwrappedSlicer is the interface satisfied by the SliceToUnwrappedSlice method.
type sliceToUnwrappedSlicer interface {
	SliceToUnwrappedSlice(S slice.Interface) (unwrappedexponents.Interface, error) // SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped exponents.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// EmptySlice returns an empty slice.
func EmptySlice(h Helper) Interface {
	S, err := h.Wrap(h.New(0))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return S
}

// SliceContainingZero returns a slice with the single entry 0.
func SliceContainingZero(h Helper) Interface {
	unwrappedS := h.New(1)
	S, err := h.Wrap(unwrappedS.Append(unwrappedS.Universe().Zero()))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return S
}

// SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped exponents using the given helper. If S satisfies the interface:
//		type SliceToUnwrappedSlicer interface {
//			SliceToUnwrappedSlice(S slice.Interface)
//				(unwrappedexponents.Interface, error) // SliceToUnwrappedSlice
//				  attempts to convert the entries in the slice S to a slice of
//				  unwrapped exponents.
//		}
// then S's SliceToUnwrappedSlice method will be used.
func SliceToUnwrappedSlice(h Helper, S slice.Interface) (T unwrappedexponents.Interface, err error) {
	// If the helper supports this method directly, call it
	if hh, ok := h.(sliceToUnwrappedSlicer); ok {
		T, err = hh.SliceToUnwrappedSlice(S)
		return
	}
	// Appending the elements to the slices might panic, so allow us to recover
	defer func() {
		if r := recover(); r != nil {
			err = errors.Errorf("Error converting exponents: %s\n%s", r, debug.Stack())
		}
	}()
	// Append the converted entries to a new unwrapped slice
	T = unwrappedexponents.AppendSlice(h.New(S.Len()), converter.New(h, S))
	return
}

// IsSorted returns true iff the entries in the slice S are in strictly increasing order.
func IsSorted(S Interface) bool {
	return unwrappedexponents.IsSorted(S.Unwrap())
}

// IndexOfZero returns the index of 0 in the slice, or -1 if not present. Assumes that the slice is sorted in increasing order.
func IndexOfZero(S Interface) int {
	T := S.Unwrap()
	idx, err := unwrappedexponents.Index(T, T.Universe().Zero())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return idx
}

// Index searches for x in a sorted slice S and returns the index, or -1 if not present. Assumes that the slice is sorted in increasing order.
func Index(h Helper, S Interface, x object.Element) (int, error) {
	// Ensure that x is an element of S
	xx, err := S.Universe().ToElement(x)
	if err != nil {
		return -1, err
	}
	// Now attempt to map x into the unwrapped universe
	y, err := h.ToUnwrappedElement(xx)
	if err != nil {
		return -1, err
	}
	// Perform the search on the unwrapped slice
	return unwrappedexponents.Index(S.Unwrap(), y)
}
