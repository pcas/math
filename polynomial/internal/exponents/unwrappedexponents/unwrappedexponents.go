// Unwrappedexponents describes the internal "unwrapped" slice of exponents.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package unwrappedexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/sort"
)

// NewFunc returns a new unwrapped slice of exponents with given capacity.
type NewFunc func(capacity int) Interface

// appendSlicer is an optional interface that an unwrapped slice of exponents may choose to satisfy to assist with appending a slice.
type appendSlicer interface {
	AppendSlice(S slice.Interface) Interface // AppendSlice appends the elements of the slice S to the end of the unwrapped slice of exponents. Returns the updated exponents on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic.
}

// inserter is an optional interface that an unwrapped slice of exponents may choose to satisfy to assist with inserting an element.
type inserter interface {
	Insert(x object.Element, idx int) Interface // Insert inserts the element x at index idx. Returns the updated exponents on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic.
}

// remover is an optional interface that an unwrapped slice of exponents may choose to satisfy to assist with removing an element.
type remover interface {
	Remove(idx int) Interface // Remove removes the element at index idx. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid.
}

// isEqualToer is an optional interface that an unwrapped slice of exponents may choose to satisfy to answer whether two slices are equal.
type isEqualToer interface {
	IsEqualTo(S slice.Interface) (bool, error) // IsEqualTo returns true iff the entries in the slice are equal to the given slice S.
}

// isSorteder is an optional interface that an unwrapped slice of exponents may choose to satisfy to answer whether the slice is sorted.
type isSorteder interface {
	IsSorted() bool // IsSorted returns true iff the entries in the slice are in strictly increasing order.
}

// searcher is an optional interface that an unwrapped slice of exponents may choose to satisfy to search for an element.
type searcher interface {
	Search(x object.Element) (int, error) // Search searches for x in a slice S, and return the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be S.Len()). Assumes that the slice is sorted in increasing order.
}

// indexer is an optional interface that an unwrapped slice of exponents may choose to satisfy to find the index of an element.
type indexer interface {
	Index(x object.Element) (int, error) // Index searches for x in the slice, returning the index of x if present, or -1 if not present. Assumes that the slice is sorted in increasing order.
}

// rangeOfDuplicateExponenter is an optional interface that an unwrapped slice of exponents may choose to satisfy to answer whether the slice has duplicate elements.
type rangeOfDuplicateExponenter interface {
	RangeOfDuplicateExponent() (int, int, error) // RangeOfDuplicateExponent returns the range of the first (consecutive) duplicate in the slice S. That is, returns indices 'start' and 'finish' such that S[i] == S[start] for all start <= i < finish. Assumes that the slice is sorted in increasing order. Returns -1, 0 if no duplicates are found.
}

// Universe is the interface satisfied by the common parent of elements in an unwrapped slice of exponents.
type Universe interface {
	monomialorder.Interface
}

// Interface is the interface satisfied by an unwrapped slice of exponents.
type Interface interface {
	slice.Interface
	sort.Lesser
	sort.Swapper
	Universe() Universe                 // Universe returns the parent common to the elements in the unwrapped slice.
	Append(x object.Element) Interface  // Append appends the element x to the end of the unwrapped slice of exponents. Returns the updated exponents on success (which will be of the same underlying type). If x is not of the required type, this will panic.
	Slice(k int, m int) slice.Interface // Slice returns a subslice of the unwrapped slice of exponents, starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
	Copy() Interface                    // Copy returns a copy of this slice (which will be of the same underlying type).
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AppendSlice appends the elements of the slice S to the end of the unwrapped exponents slice T. Returns the updated exponents on success (which will be of the same underlying type). If the elements of S are not of the required type, this will panic. If T satisfies the interface:
//		type AppendSlicer interface {
//			AppendSlice(S slice.Interface) Interface // AppendSlice appends the
//			   elements of the slice S to the end of the unwrapped slice of
//			   exponents. Returns the updated exponents on success (which will
//			   be of the same underlying type). If the elements of S are not of
//			   the required type, this will panic.
//		}
// then T's AppendSlice method will be called.
func AppendSlice(T Interface, S slice.Interface) Interface {
	// Does the slice implement the appendSlicer interface?
	if TT, ok := T.(appendSlicer); ok {
		return TT.AppendSlice(S)
	}
	// No such luck -- append the elements one at a time
	n := S.Len()
	for i := 0; i < n; i++ {
		T = T.Append(S.Entry(i))
	}
	return T
}

// Insert inserts the element x into the unwrapped exponents slice S at index idx. Returns the updated exponents on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..S.Len() (inclusive), or this will panic. If S satisfies the interface:
//		type Inserter interface {
//			Insert(x object.Element, idx int) Interface // Insert inserts the
//	 		  element x at index idx. Returns the updated exponents on success
//			  (which will be of the same underlying type). If the element x is
//			  not of the required type, this will panic. The index must be in
//			  the range 0..S.Len() (inclusive), or this will panic.
//		}
// then S's Insert method will be called.
func Insert(S Interface, x object.Element, idx int) Interface {
	// Does the slice implement the inserter interface?
	if SS, ok := S.(inserter); ok {
		return SS.Insert(x, idx)
	}
	// Sanity check on the index
	size := S.Len()
	if idx < 0 || idx > size {
		panic(errors.InvalidIndexRange.New(idx, size))
	}
	// Can we simply append x?
	if idx == size {
		return S.Append(x)
	}
	// Return S[0:idx] cat [x] cat S[idx:size]
	return AppendSlice(S.Slice(0, idx).(Interface).Copy().Append(x), S.Slice(idx, size))
}

// Remove removes the element at index idx from the unwrapped exponents slice S. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid. If S satisfies the interface:
//		type Remover interface {
//			Remove(idx int) Interface // Remove removes the element at index
// 			  idx. Returns the updated exponents on success (which will be of
// 			  the same underlying type). This will panic if the index is
//			  invalid.
//		}
// then S's Remove method will be called.
func Remove(S Interface, idx int) Interface {
	// Does the slice implement the remover interface?
	if SS, ok := S.(remover); ok {
		return SS.Remove(idx)
	}
	// Sanity check on the index
	size := S.Len()
	if idx < 0 || idx >= size {
		panic(errors.InvalidIndexRange.New(idx, size-1))
	}
	// Can we simply slice?
	if idx == 0 {
		return S.Slice(1, size).(Interface)
	} else if idx == size-1 {
		return S.Slice(0, size-1).(Interface)
	}
	// Return S[0:idx] cat S[idx+1:size]
	return AppendSlice(S.Slice(0, idx).(Interface).Copy(), S.Slice(idx+1, size))
}

// AreEqual returns true iff the slice of unwrapped exponents S is equal to the slice T. If S satisfies the interface:
//		type IsEqualToer interface {
//			IsEqualTo(S slice.Interface) (bool, error) // IsEqualTo returns
//			   true iff the entries in the slice are equal to the given slice S.
//		}
// then S's IsEqualTo method will be called.
func AreEqual(S Interface, T slice.Interface) (bool, error) {
	// Does S implement the isEqualToer interface?
	if SS, ok := S.(isEqualToer); ok {
		return SS.IsEqualTo(T)
	}
	// Check that the lengths agree
	size := S.Len()
	if T.Len() != size {
		return false, nil
	}
	// Start walking through the entries checking for equality
	K := S.Universe()
	for i := 0; i < size; i++ {
		if ok, err := K.AreEqual(S.Entry(i), T.Entry(i)); err != nil {
			return false, err
		} else if !ok {
			return false, nil
		}
	}
	// If we're here then the slices are equal
	return true, nil
}

// IsSorted returns true iff the entries in the slice S are in strictly increasing order. If S satisfies the interface:
//		type IsSorteder interface {
//			IsSorted() bool // IsSorted returns true iff the entries in the
//			   slice are in strictly increasing order.
//		}
// then S's IsSorted method will be called.
func IsSorted(S Interface) bool {
	// Does the slice implement the isSorteder interface?
	if SS, ok := S.(isSorteder); ok {
		return SS.IsSorted()
	}
	// Perform the check ourselves
	k := S.Len()
	for i := 1; i < k; i++ {
		if !S.Less(i-1, i) {
			return false
		}
	}
	return true
}

// Search searches for x in a slice S, and return the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be S.Len()). Assumes that the slice is sorted in increasing order. If S satisfies the interface:
//		type Searcher interface {
//			Search(x object.Element) (int, error) // Search searches for x in a
//		 		slice S, and return the index as specified by sort.Search. The
//				return value is the index to insert x if x is not present (it
// 				could be S.Len()). Assumes that the slice is sorted in
// 				increasing order.
//		}
// then S's Search method will be called.
func Search(S Interface, x object.Element) (int, error) {
	// Does the slice implement the searcher interface?
	if SS, ok := S.(searcher); ok {
		return SS.Search(x)
	}
	// Ensure that x is an element of S
	L := S.Universe()
	y, err := L.ToElement(x)
	if err != nil {
		return 0, err
	}
	// Perform the search
	return slice.Search(L.Cmp, S, y)
}

// Index searches for x in a sorted slice S and returns the index, or -1 if not present. Assumes that the slice is sorted in increasing order. If S satisfies the interface:
//		type Indexer interface {
//			Index(x object.Element) (int, error) // Index searches for x in the
//				slice, returning the index of x if present, or -1 if not
//				present. Assumes that the slice is sorted in increasing order.
//		}
// then S's Index method will be called.
func Index(S Interface, x object.Element) (int, error) {
	// Does the slice implement the indexer interface?
	if SS, ok := S.(indexer); ok {
		return SS.Index(x)
	}
	// Ensure that x is an element of S
	L := S.Universe()
	y, err := L.ToElement(x)
	if err != nil {
		return -1, err
	}
	// Perform the search
	idx, err := Search(S, y)
	if err != nil {
		return -1, err
	}
	// Does the entry at the given index equal x?
	if idx != S.Len() {
		if sgn, err := L.Cmp(S.Entry(idx), y); err != nil {
			return -1, err
		} else if sgn == 0 {
			return idx, nil
		}
	}
	return -1, nil
}

// RangeOfDuplicateExponent returns the range of the first (consecutive) duplicate in the slice S. That is, returns indices 'start' and 'finish' such that S[i] == S[start] for all start <= i < finish. Assumes that the slice is sorted in increasing order. Returns -1, 0 if no duplicates are found. If S satisfies the interface:
//		type RangeOfDuplicateExponenter interface {
//			RangeOfDuplicateExponent() (int, int, error)
//				// RangeOfDuplicateExponent returns the range of the first
//				  (consecutive) duplicate in the slice S. That is, returns
//				  indices 'start' and 'finish' such that S[i] == S[start] for
//				  all start <= i < finish. Assumes that the slice is sorted in
//				  increasing order. Returns -1, 0 if no duplicates are found.
//		}
// then S's RangeOfDuplicateExponent method will be called.
func RangeOfDuplicateExponent(S Interface) (int, int, error) {
	// Does the slice implement this method?
	if SS, ok := S.(rangeOfDuplicateExponenter); ok {
		return SS.RangeOfDuplicateExponent()
	}
	// Is there anything to do?
	size := S.Len()
	if size <= 1 {
		return -1, 0, nil
	}
	// Note the common parent of the exponents
	L := S.Universe()
	// Get started
	lastk := S.Entry(0)
	start := -1
	for i := 1; i < size; i++ {
		k := S.Entry(i)
		if areEqual, err := L.AreEqual(k, lastk); err != nil {
			return -1, 0, err
		} else if areEqual {
			if start == -1 {
				start = i - 1
			}
		} else if start != -1 {
			return start, i, nil
		}
		lastk = k
	}
	// We got to the end of the slice
	if start != -1 {
		return start, size, nil
	}
	return -1, 0, nil
}
