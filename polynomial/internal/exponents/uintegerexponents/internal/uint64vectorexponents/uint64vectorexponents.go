// Uint64vectorexponents defines uint64-valued vector exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64vectorexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/internal/uint64vector"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
	"strconv"
)

// Slice defines a slice of multinomial exponents.
type Slice struct {
	k       *uint64vector.Parent    // The universe
	entries []*uint64vector.Element // The underlying slice
}

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent common to the elements in the slice.
func (s *Slice) Universe() unwrappedexponents.Universe {
	if s == nil {
		k, err := uint64vector.DefaultLatticeWithOrder(0, monomialorder.Grevlex)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return k
	}
	return s.k
}

// Len returns the length of this slice of exponents.
func (s *Slice) Len() int {
	if s == nil {
		return 0
	}
	return len(s.entries)
}

// Entry returns the i-th entry of the slice. This will panic if out of range.
func (s *Slice) Entry(i int) object.Element {
	return s.entries[i]
}

// Append appends the element x to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s *Slice) Append(x object.Element) unwrappedexponents.Interface {
	y, err := uint64vector.ToElement(s.k, x)
	if err != nil {
		panic(err)
	}
	return &Slice{
		k:       s.k,
		entries: append(s.entries, y),
	}
}

// AppendSlice appends the elements in S to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If the elements in S are not of the required type, this will panic.
func (s *Slice) AppendSlice(S slice.Interface) unwrappedexponents.Interface {
	k := s.k
	entries := s.entries
	switch T := S.(type) {
	case *Slice:
		if T.k == k {
			entries = append(entries, T.entries...)
		} else {
			panic("Universes do not agree.")
		}
	case slice.ElementSlice:
		for _, x := range T {
			y, err := uint64vector.ToElement(k, x)
			if err != nil {
				panic(err)
			}
			entries = append(entries, y)
		}
	default:
		n := T.Len()
		for i := 0; i < n; i++ {
			y, err := uint64vector.ToElement(k, T.Entry(i))
			if err != nil {
				panic(err)
			}
			entries = append(entries, y)
		}
	}
	// Return the new slice
	return &Slice{
		k:       k,
		entries: entries,
	}
}

// Less reports whether the element with index i should sort before the element with index j. This panics if i or j are aout of range.
func (s *Slice) Less(i int, j int) bool {
	sgn, err := uint64vector.Cmp(s.entries[i], s.entries[j])
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return sgn < 0
}

// Swap swaps the elements with indexes i and j. This panics if i or j are out of range.
func (s *Slice) Swap(i int, j int) {
	s.entries[i], s.entries[j] = s.entries[j], s.entries[i]
}

// Slice returns a subslice of the slice of exponents starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s *Slice) Slice(k int, m int) slice.Interface {
	if s == nil && k == 0 && m == 0 {
		return s
	}
	return &Slice{
		k:       s.k,
		entries: s.entries[k:m],
	}
}

// Hash returns a hash value for this slice.
func (s *Slice) Hash() hash.Value {
	if s.Len() == 0 {
		return 0
	}
	v := hash.NewSequenceHasher()
	defer hash.ReuseSequenceHasher(v)
	for _, x := range s.entries {
		v.Add(x)
	}
	return v.Hash()
}

// PrettyPrint returns a string representation of the i-th exponent using the given slice of names. This will panic if the index is out of range, or if there are not enough names for the dimension.
func (s *Slice) PrettyPrint(i int, names []string) string {
	x := s.entries[i]
	if x.IsZero() {
		return "1"
	}
	val := x.ToUint64Slice()
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	first := true
	for i, c := range val {
		if c == 1 {
			if !first {
				b.WriteByte('*')
			}
			b.WriteString(names[i])
			first = false
		} else if c != 0 {
			if !first {
				b.WriteByte('*')
			}
			b.WriteString(names[i])
			b.WriteByte('^')
			b.WriteString(strconv.FormatUint(c, 10))
			first = false
		}
	}
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new exponent slice using the given universe and capacity.
func New(k *uint64vector.Parent, capacity int) *Slice {
	if capacity < 0 {
		capacity = 0
	}
	return &Slice{
		k:       k,
		entries: make([]*uint64vector.Element, 0, capacity),
	}
}
