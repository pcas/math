// Uint64numberexponents defines uint64-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64numberexponents

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/internal/uint64number"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
	"strconv"
)

// Slice defines a slice of univariate exponents.
type Slice []uint64

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent common to the elements in the slice.
func (s Slice) Universe() unwrappedexponents.Universe {
	return uint64number.Set()
}

// Len returns the length of this slice of exponents.
func (s Slice) Len() int {
	return len(s)
}

// Entry returns the i-th entry of the slice. This will panic if out of range.
func (s Slice) Entry(i int) object.Element {
	return uint64number.FromUint64(s[i])
}

// Append appends the element x to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s Slice) Append(x object.Element) unwrappedexponents.Interface {
	switch y := x.(type) {
	case *uint64number.Element:
		s = append(s, y.Uint64())
	case *integer.Element:
		n, err := y.Uint64()
		if err != nil {
			panic(err)
		}
		s = append(s, n)
	default:
		n, err := uint64number.ToElement(x)
		if err != nil {
			panic(err)
		}
		s = append(s, n.Uint64())
	}
	return Slice(s)
}

// AppendSlice appends the elements in S to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If the elements in S are not of the required type, this will panic.
func (s Slice) AppendSlice(S slice.Interface) unwrappedexponents.Interface {
	switch T := S.(type) {
	case Slice:
		s = append(s, T...)
	case integer.Slice:
		for _, x := range T {
			y, err := x.Uint64()
			if err != nil {
				panic(err)
			}
			s = append(s, y)
		}
	case slice.ElementSlice:
		for _, x := range T {
			y, err := uint64number.ToElement(x)
			if err != nil {
				panic(err)
			}
			s = append(s, y.Uint64())
		}
	default:
		n := T.Len()
		for i := 0; i < n; i++ {
			y, err := uint64number.ToElement(T.Entry(i))
			if err != nil {
				panic(err)
			}
			s = append(s, y.Uint64())
		}
	}
	// Return the new slice
	return Slice(s)
}

// Less reports whether the element with index i should sort before the element with index j. This panics if i or j are aout of range.
func (s Slice) Less(i int, j int) bool {
	return s[i] < s[j]
}

// Swap swaps the elements with indexes i and j. This panics if i or j are out of range.
func (s Slice) Swap(i int, j int) {
	s[i], s[j] = s[j], s[i]
}

// Slice returns a subslice of the slice of exponents starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s Slice) Slice(k int, m int) slice.Interface {
	return Slice(s[k:m])
}

// Hash returns a hash value for this slice.
func (s Slice) Hash() hash.Value {
	return hash.Uint64Slice(s)
}

// PrettyPrint returns a string representation of the i-th exponent using the given slice of names. This will panic if the index is out of range, or if there are not enough names for the dimension.
func (s Slice) PrettyPrint(i int, names []string) string {
	c := s[i]
	if c == 0 {
		return "1"
	} else if c == 1 {
		return names[0]
	}
	return names[0] + "^" + strconv.FormatUint(c, 10)
}
