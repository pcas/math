// Derivative provides partial derivative methods for integer-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerexponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64numberexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64vectorexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
)

/////////////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal partial derivative df/dx_k of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
func (h *helper) Derivative(k int, es exponents.Interface, cs coefficients.Interface, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) {
	// How we proceed depends on the unwrapped type of es
	switch unwrappedEs := es.Unwrap().(type) {
	case int64numberexponents.Slice:
		if k != 0 {
			return nil, nil, errors.InvalidIndexRange.New(k, 0)
		}
		return int64numberexponents.Derivative(unwrappedEs, cs.Unwrap(), ch)
	case *int64vectorexponents.Slice:
		return int64vectorexponents.Derivative(k, unwrappedEs, cs.Unwrap(), h, ch)
	}
	// The underlying type isn't supported
	return nil, nil, errors.UnknownSliceType.New()
}

// NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
func (h *helper) NthDerivative(n int, k int, es exponents.Interface, cs coefficients.Interface, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) {
	// How we proceed depends on the unwrapped type of es
	switch unwrappedEs := es.Unwrap().(type) {
	case int64numberexponents.Slice:
		if k != 0 {
			return nil, nil, errors.InvalidIndexRange.New(k, 0)
		}
		return int64numberexponents.NthDerivative(n, unwrappedEs, cs.Unwrap(), ch)
	case *int64vectorexponents.Slice:
		return int64vectorexponents.NthDerivative(n, k, unwrappedEs, cs.Unwrap(), h, ch)
	}
	// The underlying type isn't supported
	return nil, nil, errors.UnknownSliceType.New()
}
