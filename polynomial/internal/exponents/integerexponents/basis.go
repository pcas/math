// Basis provides basis methods for integer-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerexponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/internal/int64vector"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////////////

// Basis returns a slice of slices of unwrapped exponents corresponding to the variables x_0,...,x_{rk-1} in the polynomial ring, where rk is the rank of the exponent universe.
func (h *helper) Basis() []unwrappedexponents.Interface {
	// How we build the basis depends on the unwrapped universe
	switch m := unwrappedUniverse(h).(type) {
	case int64number.Parent:
		return []unwrappedexponents.Interface{
			h.New(1).Append(int64number.One()),
		}
	case *int64vector.Parent:
		basis := make([]unwrappedexponents.Interface, 0, m.Rank())
		for _, x := range int64vector.Basis(m) {
			basis = append(basis, h.New(1).Append(x))
		}
		return basis
	}
	panic(fatal.ImpossibleError(errors.ParentsDoNotAgree.New())) // This should never happen
}

// NthBasisElement returns a slice of unwrapped exponents corresponding to the n-th variable x_n in the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank of the exponent universe.
func (h *helper) NthBasisElement(n int) (unwrappedexponents.Interface, error) {
	// Sanity check
	if rk := h.Universe().Rank(); n < 0 || n >= rk {
		return nil, errors.InvalidIndexRange.New(n, rk-1)
	}
	// How we build the basis depends on the unwrapped universe
	switch m := unwrappedUniverse(h).(type) {
	case int64number.Parent:
		return h.New(1).Append(int64number.One()), nil
	case *int64vector.Parent:
		x, err := m.NthBasisElement(n)
		if err != nil {
			return nil, err
		}
		return h.New(1).Append(x), nil
	}
	return nil, errors.ParentsDoNotAgree.New() // This should never happen
}

// IsNthBasisElement returns true iff the slice of unwrapped exponents S corresponds to the n-th variable x_n in the polynomial ring. Here variables are indexed from 0 to rk - 1 (inclusive), where rk is the rank of the exponent universe.
func (h *helper) IsNthBasisElement(n int, S unwrappedexponents.Interface) (bool, error) {
	// Sanity check
	if rk := h.Universe().Rank(); n < 0 || n >= rk {
		return false, errors.InvalidIndexRange.New(n, rk-1)
	} else if S.Universe() != unwrappedUniverse(h) {
		return false, errors.ParentsDoNotAgree.New()
	} else if S.Len() != 1 {
		return false, nil
	}
	// How we build the basis depends on the unwrapped universe
	switch m := S.Universe().(type) {
	case int64number.Parent:
		return m.IsOne(S.Entry(0))
	case *int64vector.Parent:
		x := S.Entry(0).(*int64vector.Element)
		rk := x.Dimension()
		for i := 0; i < rk; i++ {
			if i == n {
				if x.EntryAsInt64OrPanic(i) != 1 {
					return false, nil
				}
			} else if x.EntryAsInt64OrPanic(i) != 0 {
				return false, nil
			}
		}
		return true, nil
	}
	return false, errors.ParentsDoNotAgree.New() // This should never happen
}
