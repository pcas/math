// Integerexponents implements integer-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerexponents

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
)

// Slice defines the helper wrapper for an integer-valued exponent slice.
type Slice struct {
	k       *integervector.Parent        // The user-facing parent
	entries unwrappedexponents.Interface // The underlying slice
}

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent common to the elements in the slice.
func (s *Slice) Universe() polynomial.ExponentMonoid {
	if s == nil {
		k, err := integervector.DefaultLattice(0)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return k
	}
	return s.k
}

// Len returns the length of this slice of exponents.
func (s *Slice) Len() int {
	if s == nil {
		return 0
	}
	return s.entries.Len()
}

// Entry returns the i-th entry of the slice. This will panic if out of range.
func (s *Slice) Entry(i int) object.Element {
	x, err := integervector.ChangeParent(s.k, s.entries.Entry(i))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return x
}

// Unwrap returns the underlying internal slice of exponents.
func (s *Slice) Unwrap() unwrappedexponents.Interface {
	if s == nil {
		return nil
	}
	return s.entries
}
