// Int64numberexponents defines int64-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64numberexponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/internal/int64vector"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/hash"
	"sort"
)

// Slice defines a slice of univariate exponents.
type Slice []*int64number.Element

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent common to the elements in the slice.
func (s Slice) Universe() unwrappedexponents.Universe {
	return int64number.Set()
}

// Len returns the length of this slice of exponents.
func (s Slice) Len() int {
	return len(s)
}

// Entry returns the i-th entry of the slice. This will panic if out of range.
func (s Slice) Entry(i int) object.Element {
	return s[i]
}

// Append appends the element x to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s Slice) Append(x object.Element) unwrappedexponents.Interface {
	y, err := ToInt64Number(x)
	if err != nil {
		panic(err)
	}
	return append(s, y)
}

// AppendSlice appends the elements in S to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If the elements in S are not of the required type, this will panic.
func (s Slice) AppendSlice(S slice.Interface) unwrappedexponents.Interface {
	T, err := ToSlice(S)
	if err != nil {
		panic(err)
	}
	return append(s, T...)
}

// Insert inserts the element x at index idx. Returns the updated exponents on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..s.Len() (inclusive), or this will panic.
func (s Slice) Insert(x object.Element, idx int) unwrappedexponents.Interface {
	y, err := ToInt64Number(x)
	if err != nil {
		panic(err)
	} else if idx == len(s) {
		return append(s, y)
	}
	t := make(Slice, 0, len(s)+1)
	return append(append(append(t, s[:idx]...), y), s[idx:]...)
}

// Remove removes the element at index idx. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid.
func (s Slice) Remove(idx int) unwrappedexponents.Interface {
	if idx == 0 {
		return s[1:]
	} else if idx == len(s)-1 {
		return s[:idx]
	}
	t := make(Slice, 0, len(s)-1)
	return append(append(t, s[:idx]...), s[idx+1:]...)
}

// Less reports whether the element with index i should sort before the element with index j. This panics if i or j are aout of range.
func (s Slice) Less(i int, j int) bool {
	return s[i].IsLessThan(s[j])
}

// Swap swaps the elements with indexes i and j. This panics if i or j are out of range.
func (s Slice) Swap(i int, j int) {
	s[i], s[j] = s[j], s[i]
}

// Slice returns a subslice of the slice of exponents starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s Slice) Slice(k int, m int) slice.Interface {
	return Slice(s[k:m])
}

// IsEqualTo returns true iff the entries in the slice are equal to the given slice S.
func (s Slice) IsEqualTo(S slice.Interface) (bool, error) {
	// Attempt to convert S to a slice of correct type
	T, err := ToSlice(S)
	if err != nil {
		return false, err
	}
	// Check that the lengths agree
	if len(T) != len(s) {
		return false, nil
	}
	// Check the entries
	for i, x := range s {
		if !x.IsEqualTo(T[i]) {
			return false, nil
		}
	}
	return true, nil
}

// IsSorted returns true iff the entries in the slice are in strictly increasing order.
func (s Slice) IsSorted() bool {
	if len(s) <= 1 {
		return true
	}
	x := s[0]
	for _, y := range s[1:] {
		if y.IsLessThanOrEqualTo(x) {
			return false
		}
		x = y
	}
	return true
}

// Search searches for x in a slice s, and return the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be s.Len()). Assumes that the slice is sorted in increasing order.
func (s Slice) Search(x object.Element) (int, error) {
	// Ensure that x lies in the universe
	xx, err := int64number.ToElement(x)
	if err != nil {
		return 0, err
	}
	// Perform the search
	return sort.Search(len(s), func(i int) bool {
		return s[i].IsGreaterThanOrEqualTo(xx)
	}), nil
}

// Index searches for x in the slice s, returning the index of x if present, or -1 if not present. Assumes that s is sorted in increasing order.
func (s Slice) Index(x object.Element) (int, error) {
	// Ensure that x lies in the universe
	xx, err := int64number.ToElement(x)
	if err != nil {
		return -1, err
	}
	// Perform the search
	if idx := sort.Search(len(s), func(i int) bool {
		return s[i].IsGreaterThanOrEqualTo(xx)
	}); idx != len(s) && s[idx].IsEqualTo(xx) {
		return idx, nil
	}
	return -1, nil
}

// RangeOfDuplicateExponent returns the range of the first (consecutive) duplicate in the slice s. That is, returns indices 'start' and 'finish' such that s[i] == s[start] for all start <= i < finish. Assumes that the slice is sorted in increasing order. Returns -1, 0 if no duplicates are found.
func (s Slice) RangeOfDuplicateExponent() (int, int, error) {
	// Is there anything to do?
	if len(s) <= 1 {
		return -1, 0, nil
	}
	// Get started
	lastk := s[0]
	start := -1
	for i, k := range s[1:] {
		if lastk.IsEqualTo(k) {
			if start == -1 {
				start = i
			}
		} else if start != -1 {
			return start, i + 1, nil
		}
		lastk = k
	}
	// We got to the end of the slice
	if start != -1 {
		return start, len(s), nil
	}
	return -1, 0, nil
}

// Copy returns a copy of this slice (which will be of the same underlying type).
func (s Slice) Copy() unwrappedexponents.Interface {
	return Slice(int64number.CopySlice(s))
}

// Hash returns a hash value for this slice.
func (s Slice) Hash() hash.Value {
	return int64number.Slice(s).Hash()
}

// PrettyPrint returns a string representation of the i-th exponent using the given slice of names. This will panic if the index is out of range, or if there are not enough names for the dimension.
func (s Slice) PrettyPrint(i int, names []string) string {
	x := s[i]
	if x.IsZero() {
		return "1"
	} else if x.IsOne() {
		return names[0]
	}
	return names[0] + "^" + x.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToInt64Number attempts to convert the element x to an int64number element.
func ToInt64Number(x object.Element) (*int64number.Element, error) {
	switch y := x.(type) {
	case *int64number.Element:
		return y, nil
	case *integer.Element:
		return int64number.FromInteger(y)
	case *int64vector.Element:
		if y.Dimension() != 1 {
			return nil, errors.DimensionAndRankDoNotAgree.New()
		}
		return int64number.FromInt64(y.EntryAsInt64OrPanic(0)), nil
	case *integervector.Element:
		if y.Dimension() != 1 {
			return nil, errors.DimensionAndRankDoNotAgree.New()
		}
		return int64number.FromInteger(y.EntryOrPanic(0))
	}
	return int64number.ToElement(x)
}

// ToSlice attempts to convert the slice S to a Slice. Note that this will attempt to avoid copying the underlying slice S, so future modifications to S MAY also modify the returned slice.
func ToSlice(S slice.Interface) (Slice, error) {
	switch T := S.(type) {
	case Slice:
		return T, nil
	case int64number.Slice:
		return Slice(T), nil
	case integer.IntSlice:
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			s = append(s, int64number.FromInt(x))
		}
		return s, nil
	case integer.Int32Slice:
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			s = append(s, int64number.FromInt32(x))
		}
		return s, nil
	case integer.Int64Slice:
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			s = append(s, int64number.FromInt64(x))
		}
		return s, nil
	case integer.Slice:
		// A slice of integers is easy to convert
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			y, err := int64number.FromInteger(x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	case int64vector.Slice:
		// A slice of int64-valued vectors will need their dimensions checking
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			if x.Dimension() != 1 {
				return nil, errors.DimensionAndRankDoNotAgree.New()
			}
			s = append(s, int64number.FromInt64(x.EntryAsInt64OrPanic(0)))
		}
		return s, nil
	case integervector.Slice:
		// A slice of vectors will need their dimensions checking
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			if x.Dimension() != 1 {
				return nil, errors.DimensionAndRankDoNotAgree.New()
			}
			y, err := int64number.FromInteger(x.EntryOrPanic(0))
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	case slice.ElementSlice:
		// This is an ElementSlice, so at least we can range over the entries
		s := make(Slice, 0, T.Len())
		for _, x := range T {
			y, err := ToInt64Number(x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	}
	// Fall back to the default method for a generic slice
	size := S.Len()
	s := make(Slice, 0, size)
	for i := 0; i < size; i++ {
		x, err := ToInt64Number(S.Entry(i))
		if err != nil {
			return nil, err
		}
		s = append(s, x)
	}
	return s, nil
}
