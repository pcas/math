// Derivative defines functions for computing the formal derivative of univariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64numberexponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/mathutil"
	"math"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// nthDerivativeMultiple returns the "partial factorial" d(d-1)...(d-n+1). This is the quantity the term c * x^d is multiplied by when differentiating n times.
func nthDerivativeMultiple(d int64, n int64) *integer.Element {
	if a, ok := mathutil.SubtractInt64(d, n); ok && a != math.MaxInt64 {
		return integer.ProductOfRangeInt64(a+1, d)
	}
	dd := integer.FromInt64(d)
	return integer.ProductOfRange(integer.AddInt64(n, dd).Increment(), dd)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal derivative df/dx of the polynomial f. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The derivative is returned as two parallel slices of exponents and coefficients.
func Derivative(es Slice, cs slice.Interface, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	} else if size == 0 {
		return make(Slice, 0), ch.New(0), nil
	}
	// Create the destination slices of exponents and coefficients
	destEs, destCs := make(Slice, 0, size), ch.New(size)
	// Note the common parent of the coefficients
	R := destCs.Universe()
	// Calculate the derivative
	for i, d := range es {
		if !d.IsZero() {
			c, err := R.ScalarMultiplyByInteger(d.ToInteger(), cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			dd, err := d.Decrement()
			if err != nil {
				return nil, nil, err
			}
			destEs, destCs = append(destEs, dd), destCs.Append(c)
		}
	}
	// Return the data
	return destEs, destCs, nil
}

// NthDerivative returns the n-th formal derivative d^nf/dx^n of the polynomial f. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The derivative is returned as two parallel slices of exponents and coefficients.
func NthDerivative(n int, es Slice, cs slice.Interface, ch coefficients.Helper) (Slice, unwrappedcoefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	} else if n < 0 {
		return nil, nil, errors.IllegalNegativeArg.New()
	} else if size == 0 {
		return make(Slice, 0), ch.New(0), nil
	}
	// Get the 0-th derivative out of the way
	if n == 0 {
		destEs, destCs := make(Slice, size), ch.New(size)
		copy(destEs, es)
		return destEs, unwrappedcoefficients.AppendSlice(destCs, cs), nil
	}
	// Create the destination slices of exponents and coefficients
	destEs, destCs := make(Slice, 0, size), ch.New(size)
	// Note the common parent of the coefficients
	R := destCs.Universe()
	// Calculate the n-th derivative
	nn := int64(n)
	for i, d := range es {
		if d.IsNegative() || d.IsGreaterThanOrEqualToInt64(nn) {
			b := nthDerivativeMultiple(d.Int64(), nn)
			c, err := R.ScalarMultiplyByInteger(b, cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			dd, err := int64number.SubtractInt64(d, nn)
			if err != nil {
				return nil, nil, err
			}
			destEs, destCs = append(destEs, dd), destCs.Append(c)
		}
	}
	// Return the data
	return destEs, destCs, nil
}
