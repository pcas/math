// Derivative defines functions for computing the formal derivative of multivariate polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64vectorexponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/unwrappedcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/mathutil"
	"math"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// nthDerivativeMultiple returns the "partial factorial" d(d-1)...(d-n+1). This is the quantity the term c * x^d is multiplied by when differentiating n times.
func nthDerivativeMultiple(d int64, n int64) *integer.Element {
	if a, ok := mathutil.SubtractInt64(d, n); ok && a != math.MaxInt64 {
		return integer.ProductOfRangeInt64(a+1, d)
	}
	dd := integer.FromInt64(d)
	return integer.ProductOfRange(integer.AddInt64(n, dd).Increment(), dd)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal partial derivative df/dx_k of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
func Derivative(k int, es *Slice, cs slice.Interface, eh exponents.Helper, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	} else if k < 0 || k >= es.Rank() {
		return nil, nil, errors.InvalidIndexRange.New(k, es.Rank()-1)
	} else if size == 0 {
		return eh.New(0), ch.New(0), nil
	}
	// Create the destination slices of exponents and coefficients
	destEs, destCs := eh.New(size), ch.New(size)
	// Note the common parent of the coefficients
	R := destCs.Universe()
	// Calculate the derivative
	for i, d := range es.entries {
		if m := d.EntryAsInt64OrPanic(k); m != 0 {
			c, err := R.ScalarMultiplyByInteger(integer.FromInt64(m), cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			dd, err := d.DecrementEntry(k)
			if err != nil {
				return nil, nil, err
			}
			destEs, destCs = destEs.Append(dd), destCs.Append(c)
		}
	}
	// Return the data
	return destEs, destCs, nil
}

// NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where the variables x_i are indexed from 0. Here f is the polynomial defined by the parallel slices es and cs of exponents and coefficients. The partial derivative is returned as two parallel slices of exponents and coefficients.
func NthDerivative(n int, k int, es *Slice, cs slice.Interface, eh exponents.Helper, ch coefficients.Helper) (unwrappedexponents.Interface, unwrappedcoefficients.Interface, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return nil, nil, errors.SliceLengthNotEqual.New()
	} else if n < 0 {
		return nil, nil, errors.IllegalNegativeArg.New()
	} else if k < 0 || k >= es.Rank() {
		return nil, nil, errors.InvalidIndexRange.New(k, es.Rank()-1)
	} else if size == 0 {
		return eh.New(0), ch.New(0), nil
	}
	// Get the 0-th derivative out of the way
	if n == 0 {
		return unwrappedexponents.AppendSlice(eh.New(size), es), unwrappedcoefficients.AppendSlice(ch.New(size), cs), nil
	}
	// Create the destination slices of exponents and coefficients
	destEs, destCs := eh.New(size), ch.New(size)
	// Note the common parent of the coefficients
	R := destCs.Universe()
	// Calculate the n-th derivative
	for i, d := range es.entries {
		if m := d.EntryAsInt64OrPanic(k); m < 0 || m >= int64(n) {
			b := nthDerivativeMultiple(m, int64(n))
			c, err := R.ScalarMultiplyByInteger(b, cs.Entry(i))
			if err != nil {
				return nil, nil, err
			}
			dd, err := d.SubtractInt64FromEntry(k, int64(n))
			if err != nil {
				return nil, nil, err
			}
			destEs, destCs = destEs.Append(dd), destCs.Append(c)
		}
	}
	// Return the data
	return destEs, destCs, nil
}
