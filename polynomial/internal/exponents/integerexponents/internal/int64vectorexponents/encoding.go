// Encoding contains functions for encoding/decoding a slice of int64-valued vector exponents.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64vectorexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/internal/int64vector"
	"bitbucket.org/pcastools/gobutil"
)

// The internal version number. This will permit backwards-compatible changes to
// the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (s *Slice) GobEncode() ([]byte, error) {
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the rank
	if err := enc.Encode(s.Rank()); err != nil {
		return nil, err
	}
	// Add the monomial order
	if err := enc.Encode(s.MonomialOrder()); err != nil {
		return nil, err
	}
	// Add the slice of entries
	if s == nil {
		if err := enc.Encode([]*int64vector.Element{}); err != nil {
			return nil, err
		}
	} else {
		if err := enc.Encode(s.entries); err != nil {
			return nil, err
		}
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (s *Slice) GobDecode(buf []byte) error {
	// Sanity check
	if s == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if s.k != nil {
		return gobutil.DecodingIntoExistingObject.New()
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the rank
	var rk int
	if err := dec.Decode(&rk); err != nil {
		return err
	}
	// Read in the monomial order
	var order monomialorder.Type
	if err := dec.Decode(&order); err != nil {
		return err
	}
	// Read in the slice of entries
	entries, err := int64vector.GobDecodeSlice(dec)
	if err != nil {
		return err
	}
	// Fetch the lattice of exponents
	k, err := int64vector.DefaultLatticeWithOrder(rk, order)
	if err != nil {
		return err
	}
	// Sanity check on the parent of the elements in the slice
	for _, x := range entries {
		if !k.Contains(x) {
			return errors.ParentsDoNotAgree.New()
		}
	}
	// Set the values
	s.k = k
	s.entries = entries
	return nil
}
