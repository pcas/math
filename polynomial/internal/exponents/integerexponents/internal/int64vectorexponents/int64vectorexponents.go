// Int64vectorexponents defines int64-valued vector exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64vectorexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/internal/int64vector"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
	"sort"
	"strconv"
)

// Slice defines a slice of multinomial exponents.
type Slice struct {
	k       *int64vector.Parent    // The universe
	entries []*int64vector.Element // The underlying slice
}

// DefaultOrder is the default monomial order.
const DefaultOrder = monomialorder.Grevlex

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the parent common to the elements in the slice.
func (s *Slice) Universe() unwrappedexponents.Universe {
	if s == nil {
		k, err := int64vector.DefaultLatticeWithOrder(0, DefaultOrder)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return k
	}
	return s.k
}

// Rank returns the rank of the parent common to the elements in the slice.
func (s *Slice) Rank() int {
	if s == nil {
		return 0
	}
	return s.k.Rank()
}

// MonomialOrder returns the monomial order of the parent common to the elements in the slice.
func (s *Slice) MonomialOrder() monomialorder.Type {
	if s == nil {
		return DefaultOrder
	}
	return s.k.MonomialOrder()
}

// Len returns the length of this slice of exponents.
func (s *Slice) Len() int {
	if s == nil {
		return 0
	}
	return len(s.entries)
}

// Entry returns the i-th entry of the slice. This will panic if out of range.
func (s *Slice) Entry(i int) object.Element {
	return s.entries[i]
}

// Append appends the element x to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If x is not of the required type, this will panic.
func (s *Slice) Append(x object.Element) unwrappedexponents.Interface {
	y, err := ToInt64Vector(s.k, x)
	if err != nil {
		panic(err)
	}
	s.entries = append(s.entries, y)
	return s
}

// AppendSlice appends the elements in S to the end of the exponents. Returns the updated exponents on success (which will be of the same underlying type). If the elements in S are not of the required type, this will panic.
func (s *Slice) AppendSlice(S slice.Interface) unwrappedexponents.Interface {
	T, err := ToInt64VectorSlice(s.k, S)
	if err != nil {
		panic(err)
	}
	s.entries = append(s.entries, T...)
	return s
}

// Insert inserts the element x at index idx. Returns the updated exponents on success (which will be of the same underlying type). If the element x is not of the required type, this will panic. The index must be in the range 0..s.Len() (inclusive), or this will panic.
func (s *Slice) Insert(x object.Element, idx int) unwrappedexponents.Interface {
	n := s.Len()
	if idx == n {
		return s.Append(x)
	}
	y, err := ToInt64Vector(s.k, x)
	if err != nil {
		panic(err)
	}
	es := make([]*int64vector.Element, 0, n+1)
	s.entries = append(append(append(es, s.entries[:idx]...), y), s.entries[idx:]...)
	return s
}

// Remove removes the element at index idx. Returns the updated exponents on success (which will be of the same underlying type). This will panic if the index is invalid.
func (s *Slice) Remove(idx int) unwrappedexponents.Interface {
	n := s.Len()
	if idx == 0 {
		return s.Slice(1, n).(unwrappedexponents.Interface)
	} else if idx == n-1 {
		return s.Slice(0, n-1).(unwrappedexponents.Interface)
	}
	es := make([]*int64vector.Element, 0, n-1)
	s.entries = append(append(es, s.entries[:idx]...), s.entries[idx+1:]...)
	return s
}

// Less reports whether the element with index i should sort before the element with index j. This panics if i or j are aout of range.
func (s *Slice) Less(i int, j int) bool {
	return s.entries[i].IsLessThan(s.entries[j])
}

// Swap swaps the elements with indexes i and j. This panics if i or j are out of range.
func (s *Slice) Swap(i int, j int) {
	s.entries[i], s.entries[j] = s.entries[j], s.entries[i]
}

// Slice returns a subslice of the slice of exponents starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s *Slice) Slice(k int, m int) slice.Interface {
	if s == nil && k == 0 && m == 0 {
		return s
	}
	return &Slice{
		k:       s.k,
		entries: s.entries[k:m],
	}
}

// IsEqualTo returns true iff the entries in the slice are equal to the given slice S.
func (s *Slice) IsEqualTo(S slice.Interface) (bool, error) {
	// Check that the lengths agree
	if size := s.Len(); size != S.Len() {
		return false, nil
	} else if size == 0 {
		return true, nil
	}
	// Attempt to convert S to a slice of correct type
	T, err := ToInt64VectorSlice(s.k, S)
	if err != nil {
		return false, err
	}
	// Check the entries
	for i, x := range s.entries {
		if !x.IsEqualTo(T[i]) {
			return false, nil
		}
	}
	return true, nil
}

// IsSorted returns true iff the entries in the slice are in strictly increasing order.
func (s *Slice) IsSorted() bool {
	if s.Len() <= 1 {
		return true
	}
	x := s.entries[0]
	for _, y := range s.entries[1:] {
		if y.IsLessThanOrEqualTo(x) {
			return false
		}
		x = y
	}
	return true
}

// Search searches for x in a slice s, and return the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be s.Len()). Assumes that the slice is sorted in increasing order.
func (s *Slice) Search(x object.Element) (int, error) {
	// Is there anything to do?
	size := s.Len()
	if size == 0 {
		return 0, nil
	}
	// Ensure that x has the correct parent
	y, err := ToInt64Vector(s.k, x)
	if err != nil {
		return 0, err
	}
	// Perform the search
	return sort.Search(size, func(i int) bool {
		return s.entries[i].IsGreaterThanOrEqualTo(y)
	}), nil
}

// Index searches for x in the slice s, returning the index of x if present, or -1 if not present. Assumes that s is sorted in increasing order.
func (s *Slice) Index(x object.Element) (int, error) {
	// Is there anything to do?
	size := s.Len()
	if size == 0 {
		return -1, nil
	}
	// Ensure that x has the correct parent
	y, err := ToInt64Vector(s.k, x)
	if err != nil {
		return -1, err
	}
	// Perform the search
	idx := sort.Search(size, func(i int) bool {
		return s.entries[i].IsGreaterThanOrEqualTo(y)
	})
	if idx != size && s.entries[idx].IsEqualTo(y) {
		return idx, nil
	}
	return -1, nil
}

// RangeOfDuplicateExponent returns the range of the first (consecutive) duplicate in the slice s. That is, returns indices 'start' and 'finish' such that s[i] == s[start] for all start <= i < finish. Assumes that the slice is sorted in increasing order. Returns -1, 0 if no duplicates are found.
func (s *Slice) RangeOfDuplicateExponent() (int, int, error) {
	// Is there anything to do?
	if s.Len() <= 1 {
		return -1, 0, nil
	}
	// Get started
	lastk := s.entries[0]
	start := -1
	for i, k := range s.entries[1:] {
		if k.IsEqualTo(lastk) {
			if start == -1 {
				start = i
			}
		} else if start != -1 {
			return start, i + 1, nil
		}
		lastk = k
	}
	// We got to the end of the slice
	if start != -1 {
		return start, s.Len(), nil
	}
	return -1, 0, nil
}

// Copy returns a copy of this slice (which will be of the same underlying type).
func (s *Slice) Copy() unwrappedexponents.Interface {
	return &Slice{
		k:       s.k,
		entries: int64vector.CopySlice(s.entries),
	}
}

// Hash returns a hash value for this slice.
func (s *Slice) Hash() hash.Value {
	if s.Len() == 0 {
		return 0
	}
	return int64vector.Slice(s.entries).Hash()
}

// PrettyPrint returns a string representation of the i-th exponent using the given slice of names. This will panic if the index is out of range, or if there are not enough names for the dimension.
func (s *Slice) PrettyPrint(i int, names []string) string {
	x := s.entries[i]
	if x.IsZero() {
		return "1"
	}
	val, err := x.ToInt64Slice()
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	first := true
	for i, c := range val {
		if c == 1 {
			if !first {
				b.WriteByte('*')
			}
			b.WriteString(names[i])
			first = false
		} else if c != 0 {
			if !first {
				b.WriteByte('*')
			}
			b.WriteString(names[i])
			b.WriteByte('^')
			b.WriteString(strconv.FormatInt(c, 10))
			first = false
		}
	}
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new exponent slice using the given universe and capacity.
func New(k *int64vector.Parent, capacity int) *Slice {
	if capacity < 0 {
		capacity = 0
	}
	return &Slice{
		k:       k,
		entries: make([]*int64vector.Element, 0, capacity),
	}
}

// ToInt64Vector attempts to convert the element x to an int64vector element in k.
func ToInt64Vector(k *int64vector.Parent, x object.Element) (*int64vector.Element, error) {
	if y, ok := x.(*int64vector.Element); ok {
		return int64vector.ToElement(k, y)
	}
	return int64vector.ChangeParent(k, x)
}

// ToInt64VectorExponentsSlice attempts to convert the slice S to a slice of int64-valued exponents in k. Note that this will attempt to avoid copying the underlying slice S, so future modifications to S MAY also modify the returned slice.
func ToInt64VectorExponentsSlice(k *int64vector.Parent, S slice.Interface) (*Slice, error) {
	if T, ok := S.(*Slice); ok && T != nil {
		if T.k != k {
			return nil, errors.ParentsDoNotAgree.New()
		}
		return T, nil
	}
	T, err := ToInt64VectorSlice(k, S)
	if err != nil {
		return nil, err
	}
	return &Slice{
		k:       k,
		entries: T,
	}, nil
}

// ToInt64VectorSlice attempts to convert the slice S to a slice of int64-valued vector elements in k. Note that this will attempt to avoid copying the underlying slice S, so future modifications to S MAY also modify the returned slice.
func ToInt64VectorSlice(k *int64vector.Parent, S slice.Interface) (int64vector.Slice, error) {
	switch T := S.(type) {
	case *Slice:
		// Get the nil case out of the way
		if T == nil {
			return nil, nil
		}
		// All we need to do is check the parents agree
		if T.k != k {
			return nil, errors.ParentsDoNotAgree.New()
		}
		return int64vector.Slice(T.entries), nil
	case int64vector.Slice:
		// We need to check the parents agree
		for _, x := range T {
			if x.Parent() != k {
				return nil, errors.ParentsDoNotAgree.New()
			}
		}
		return T, nil
	case integervector.Slice:
		// Convert the vectors to int64-valued vectors
		s := make(int64vector.Slice, 0, T.Len())
		for _, x := range T {
			y, err := int64vector.ChangeParent(k, x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	case int64number.Slice:
		// If k isn't of rank 1, we can't succeed
		if k.Rank() != 1 {
			return nil, errors.DimensionAndRankDoNotAgree.New()
		}
		// Convert the integers to 1-dimensional int64-valued vectors
		s := make(int64vector.Slice, 0, T.Len())
		for _, x := range T {
			y, err := int64vector.ChangeParent(k, x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	case integer.Slice:
		// If k isn't of rank 1, we can't succeed
		if k.Rank() != 1 {
			return nil, errors.DimensionAndRankDoNotAgree.New()
		}
		// Convert the integers to 1-dimensional int64-valued vectors
		s := make(int64vector.Slice, 0, T.Len())
		for _, x := range T {
			y, err := int64vector.ChangeParent(k, x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	case slice.ElementSlice:
		// Since this is an ElementSlice we can range over it
		s := make(int64vector.Slice, 0, T.Len())
		for _, x := range T {
			y, err := ToInt64Vector(k, x)
			if err != nil {
				return nil, err
			}
			s = append(s, y)
		}
		return s, nil
	}
	// Fall back to the most generic case
	size := S.Len()
	s := make(int64vector.Slice, 0, size)
	for i := 0; i < size; i++ {
		x, err := ToInt64Vector(k, S.Entry(i))
		if err != nil {
			return nil, err
		}
		s = append(s, x)
	}
	return s, nil
}
