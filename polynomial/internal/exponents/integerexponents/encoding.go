// Encoding contains functions for encoding/decoding a slice of integer-valued exponents.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64numberexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64vectorexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/gobutil"
)

// The internal version number. This will permit backwards-compatible changes to
// the encoding.
const encodingVersion byte = 1

// Constants used to define the underlying slice implementation.
const (
	int64numberexponentsType byte = iota
	int64vectorexponentsType
)

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (s *Slice) GobEncode() ([]byte, error) {
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Fetch the parent
	k := s.Universe()
	// Add the rank
	if err := enc.Encode(k.Rank()); err != nil {
		return nil, err
	}
	// Add the monomial order
	if err := enc.Encode(k.MonomialOrder()); err != nil {
		return nil, err
	}
	// How we proceed now depends on the underlying type
	switch T := s.Unwrap().(type) {
	case int64numberexponents.Slice:
		// Add the constant for this type and encode the slice
		if err := enc.Encode(int64numberexponentsType); err != nil {
			return nil, err
		} else if err := enc.Encode(T); err != nil {
			return nil, err
		}
	case *int64vectorexponents.Slice:
		// Add the constant for this type and encode the slice
		if err := enc.Encode(int64vectorexponentsType); err != nil {
			return nil, err
		} else if err := enc.Encode(T); err != nil {
			return nil, err
		}
	default:
		// The underlying type isn't supported
		return nil, errors.UnknownSliceType.New()
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (s *Slice) GobDecode(buf []byte) error {
	// Sanity check
	if s == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if s.k != nil {
		return gobutil.DecodingIntoExistingObject.New()
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the rank
	var rk int
	if err := dec.Decode(&rk); err != nil {
		return err
	}
	// Read in the monomial order
	var order monomialorder.Type
	if err := dec.Decode(&order); err != nil {
		return err
	}
	// Read in the underlying type
	var t byte
	if err := dec.Decode(&t); err != nil {
		return err
	}
	// How we decode the entries depends on the underlying type
	var entries unwrappedexponents.Interface
	switch t {
	case int64numberexponentsType:
		// Sanity check on the rank
		if rk != 1 {
			return errors.DimensionAndRankDoNotAgree.New()
		}
		// Decode the slice
		T, err := int64number.GobDecodeSlice(dec)
		if err != nil {
			return err
		}
		entries = int64numberexponents.Slice(T)
	case int64vectorexponentsType:
		// Decode the slice
		T := &int64vectorexponents.Slice{}
		if err := dec.Decode(T); err != nil {
			return err
		}
		// Sanity check on the rank and monomial order
		if T.Rank() != rk {
			return errors.DimensionAndRankDoNotAgree.New()
		} else if T.MonomialOrder() != order {
			return errors.IncompatibleMonomialOrders.New()
		}
		entries = T
	default:
		// The underlying type isn't supported
		return errors.UnknownSliceType.New()
	}
	// Fetch the lattice of exponents
	k, err := integervector.DefaultLatticeWithOrder(rk, order)
	if err != nil {
		return err
	}
	// Set the values
	s.k = k
	s.entries = entries
	return nil
}
