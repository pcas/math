// Helper implements the helper for integer-valued exponents of polynomials.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerexponents

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/internal/int64number"
	"bitbucket.org/pcas/math/internal/int64vector"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64numberexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents/internal/int64vectorexponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/unwrappedexponents"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// helper implements the helper for integer-valued exponents.
type helper struct {
	k *integervector.Parent // The user-facing "wrapped" parent
	m *int64vector.Parent   // The "unwrapped" parent in the rank != 1 case
}

// The cached helpers and a mutex controlling access.
var (
	cache map[monomialorder.Type]map[int]*helper
	mutex *sync.RWMutex
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of helpers and controlling mutex
	mutex = new(sync.RWMutex)
	cache = make(map[monomialorder.Type]map[int]*helper)
}

// userfacingUniverse returns the "wrapped" user-facing parent.
func userfacingUniverse(h *helper) *integervector.Parent {
	if h == nil {
		k, err := integervector.DefaultLatticeWithOrder(0, int64vectorexponents.DefaultOrder)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return k
	}
	return h.k
}

// unwrappedUniverse returns the "unwrapped" internal parent.
func unwrappedUniverse(h *helper) object.Parent {
	if h == nil {
		m, err := int64vector.DefaultLatticeWithOrder(0, int64vectorexponents.DefaultOrder)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return m
	}
	if h.m == nil {
		return int64number.Set()
	}
	return h.m
}

// assertUserfacingSliceEntries checks that, if any of the entries of the slice S are integervector.Elements, then they lie in the user-facing universe.
func assertUserfacingSliceEntries(h *helper, S slice.Interface) error {
	// We're slightly lazy and validate this only in the case when S is an
	// integervector.Slice
	if T, ok := S.(integervector.Slice); ok {
		k := userfacingUniverse(h)
		for _, x := range T {
			if x.Parent() != k {
				return errors.ParentsDoNotAgree.New()
			}
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// helper functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the user-facing parent of the exponents.
func (h *helper) Universe() polynomial.ExponentMonoid {
	return userfacingUniverse(h)
}

// New returns a new slice of unwrapped exponents with given capacity.
func (h *helper) New(capacity int) (s unwrappedexponents.Interface) {
	// Sanity check on the capacity
	if capacity < 0 {
		capacity = 0
	}
	// How we proceed depends on the underlying type of the "unwrapped" universe
	switch m := unwrappedUniverse(h).(type) {
	case int64number.Parent:
		s = make(int64numberexponents.Slice, 0, capacity)
	case *int64vector.Parent:
		s = int64vectorexponents.New(m, capacity)
	default:
		panic(fatal.ImpossibleError(errors.ParentsDoNotAgree.New())) // This should never happen
	}
	return
}

// Wrap wraps the given slice of unwrapped exponents.
func (h *helper) Wrap(S unwrappedexponents.Interface) (exponents.Interface, error) {
	// Sanity check
	if S.Universe() != unwrappedUniverse(h) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Return the wrapped slice
	return &Slice{
		k:       userfacingUniverse(h),
		entries: S,
	}, nil
}

// ToUnwrappedElement attempts to map the element x to the universe of the unwrapped exponents.
func (h *helper) ToUnwrappedElement(x object.Element) (object.Element, error) {
	// If x is an integervector.Element, check that it lies in the
	// public-facing universe
	if v, ok := x.(*integervector.Element); ok && v.Parent() != userfacingUniverse(h) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// How we proceed depends on the "unwrapped" universe
	switch m := unwrappedUniverse(h).(type) {
	case int64number.Parent:
		return int64numberexponents.ToInt64Number(x)
	case *int64vector.Parent:
		return int64vectorexponents.ToInt64Vector(m, x)
	}
	return nil, errors.ParentsDoNotAgree.New() // This should never happen
}

// SliceToUnwrappedSlice attempts to convert the entries in the slice S to a slice of unwrapped exponents.
func (h *helper) SliceToUnwrappedSlice(S slice.Interface) (unwrappedexponents.Interface, error) {
	// If any of the entries of S are integervector.Elements, we need to assert
	// that they lie in the user-facing universe
	if err := assertUserfacingSliceEntries(h, S); err != nil {
		return nil, err
	}
	// Convert S to a slice T of the appropriate "unwrapped" elements
	var T slice.Interface
	switch m := unwrappedUniverse(h).(type) {
	case int64number.Parent:
		var err error
		if T, err = int64numberexponents.ToSlice(S); err != nil {
			return nil, err
		}
	case *int64vector.Parent:
		var err error
		if T, err = int64vectorexponents.ToInt64VectorSlice(m, S); err != nil {
			return nil, err
		}
	default:
		return nil, errors.ParentsDoNotAgree.New() // This should never happen
	}
	// Append the slice T to the final destination slice
	return unwrappedexponents.AppendSlice(h.New(T.Len()), T), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewHelper returns a new helper of given rank and monomial order.
func NewHelper(rk int, order monomialorder.Type) (exponents.Helper, error) {
	// Sanity check
	if rk < 0 {
		return nil, errors.RankMustBeNonNegative.New()
	} else if !order.IsValid() {
		return nil, errors.UnknownMonomialOrder.New(order)
	}
	// Aquire a read lock on the mutex and check the cache
	mutex.RLock()
	if hs, ok := cache[order]; ok {
		if h, ok := hs[rk]; ok {
			// The helper is in the cache so return it
			mutex.RUnlock()
			return h, nil
		}
	}
	// No luck -- release the read lock and create the new helper
	mutex.RUnlock()
	k, err := integervector.DefaultLatticeWithOrder(rk, order)
	if err != nil {
		return nil, err
	}
	h := &helper{k: k}
	// If this isn't the rank 1 case, create the int64-vectorspace
	if rk != 1 {
		m, err := int64vector.DefaultLatticeWithOrder(rk, order)
		if err != nil {
			return nil, err
		}
		h.m = m
	}
	// Acquire a write lock and check the cache once again before returning
	mutex.Lock()
	hs, ok := cache[order]
	if !ok {
		hs = make(map[int]*helper)
		cache[order] = hs
	}
	if hh, ok := hs[rk]; ok {
		h = hh
	} else {
		hs[rk] = h
	}
	// Release the write lock and return the helper
	mutex.Unlock()
	return h, nil
}
