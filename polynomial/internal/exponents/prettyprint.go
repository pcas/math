// Prettyprint contains tools to help with converting exponents to strings.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package exponents

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/product"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/stringsbuilder"
)

// PrettyPrintFunc returns a string str for the i-th exponent in the slice. This will panic if i is out of range.
type PrettyPrintFunc func(i int) string

// prettyPrinter is an optional interface that a slice of exponents may choose to satisfy to assist with printing polynomials.
type prettyPrinter interface {
	PrettyPrint(i int, names []string) string // PrettyPrint returns a string representation of the i-th exponent using the given slice of names. This will panic if the index is out of range, or if there are not enough names for the dimension.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// variableAndExponentToString returns a string description of x^m from the given variable x (as a string) and exponent m.
func variableAndExponentToString(x string, m object.Element) string {
	str := m.String()
	if str == "0" {
		return "1"
	} else if str == "1" {
		return x
	}
	return x + "^" + str
}

// rank1PrettyPrintFunc returns the pretty print function in the rank 1 case.
func rank1PrettyPrintFunc(S Interface, x string) (PrettyPrintFunc, error) {
	// Note the common parent of the exponents and extract the projection map
	L := S.Universe()
	proj, err := L.ProjectionMap(0)
	if err != nil {
		return nil, err
	}
	// Return the pretty print function
	return func(i int) string {
		// Note the i-th entry
		m := S.Entry(i)
		// Handle the zero case
		if isZero, err := L.IsZero(m); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if isZero {
			return "1"
		}
		// Apply the projection map
		n, err := proj.Evaluate(m)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		// Return the string
		return variableAndExponentToString(x, n)
	}, nil
}

// genericPrettyPrintFunc returns the generic pretty print function.
func genericPrettyPrintFunc(S Interface, names []string) (PrettyPrintFunc, error) {
	// Note the common parent of the exponents and the projection maps
	L := S.Universe()
	projs, err := product.ProjectionMaps(L)
	if err != nil {
		return nil, err
	}
	// Return the pretty print function in the general rank case
	return func(i int) string {
		// Note the i-th entry
		m := S.Entry(i)
		// Handle the zero case
		if isZero, err := L.IsZero(m); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if isZero {
			return "1"
		}
		// Create a strings builder in which to assemble the string
		b := stringsbuilder.New()
		defer stringsbuilder.Reuse(b)
		// Start creating the string
		idx := 0
		for i, proj := range projs {
			if mi, err := proj.Evaluate(m); err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			} else if str := variableAndExponentToString(names[i], mi); str != "1" {
				if idx != 0 {
					b.WriteByte('*')
				}
				b.WriteString(str)
				idx++
			}
		}
		// Combine the strings and return
		if idx == 0 {
			return "1"
		}
		return b.String()
	}, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// PrettyPrint returns a PrettyPrintFunc for the given exponents slice S and variable print names. If S satisfies the interface:
//		type PrettyPrinter interface {
//			PrettyPrint(i int, names []string) string // PrettyPrint returns a
//			   string representation of the i-th exponent using the given slice
//			   of names. This will panic if the index is out of range, or if
//			   there are not enough names for the dimension.
//		}
// then S's PrettyPrint method will be used by the returned PrettyPrintFunc. Otherwise, if the underlying slice of wrapped exponents satisfies the PrettyPrinter interface, then its PrettyPrint method will be used by the returned PrettyPrintFunc. Failing that, a generic PrettyPrintFunc will be returned.
func PrettyPrint(S Interface, names []string) (PrettyPrintFunc, error) {
	// Check that the rank and the number of names agree
	if rk := S.Universe().Rank(); rk != len(names) {
		return nil, errors.SliceLengthNotEqualRank.New(len(names), rk)
	} else if pp, ok := S.(prettyPrinter); ok {
		// If S has a PrettyPrint method, use that
		return func(i int) string { return pp.PrettyPrint(i, names) }, nil
	} else if pp, ok := S.Unwrap().(prettyPrinter); ok {
		// Otherwise if the underlying exponents slice has a PrettyPrint
		// method, use that
		return func(i int) string { return pp.PrettyPrint(i, names) }, nil
	} else if rk == 1 {
		// Otherwise if this is of rank 1, return the rank 1 pretty print
		// function
		return rank1PrettyPrintFunc(S, names[0])
	}
	// Return the generic pretty print function
	return genericPrettyPrintFunc(S, names)
}
