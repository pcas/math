// Strings defines functions for representing a polynomial as a string.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package strings

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcastools/stringsbuilder"
	"strconv"
	"strings"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultVariableNames returns a slice of default variable names for a polynomial ring of rank d.
func DefaultVariableNames(d int) []string {
	// Get the special cases out of the way
	if d <= 0 {
		return []string{}
	} else if d == 1 {
		return []string{"x"}
	}
	// Create the slice of names in the form x_i
	names := make([]string, 0, d)
	for i := 1; i <= d; i++ {
		names = append(names, "x_"+strconv.Itoa(i))
	}
	return names
}

// ToString returns a string representation of the polynomial represented by the parallel slices of exponents es and coefficients cs, using the given variable names. Assumes that es and cs are of the same length, and that the dimension of the exponents agrees with the number of names.
func ToString(es exponents.Interface, cs coefficients.Interface, names []string) (string, error) {
	// Sanity check
	size := es.Len()
	if size != cs.Len() {
		return "", errors.SliceLengthNotEqual.New()
	}
	// Get the zero case out of the way
	if size == 0 {
		return cs.Universe().Zero().String(), nil
	}
	// Build a pretty print function for the coefficient slice
	csToString := coefficients.PrettyPrint(cs)
	// Build a pretty print function for the exponents slice
	esToString, err := exponents.PrettyPrint(es, names)
	if err != nil {
		return "", err
	}
	// Create a strings builder in which to assemble the string
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Build the string term-by-term, with the largest degree term first
	for i := size - 1; i >= 0; i-- {
		// Build the coefficient string
		cstr, sgn := csToString(i)
		// Write the correct sign for the term
		if i == size-1 {
			if sgn < 0 {
				b.WriteByte('-')
			}
		} else if sgn < 0 {
			b.WriteString(" - ")
		} else {
			b.WriteString(" + ")
		}
		// Build the monomial string
		x := esToString(i)
		// Write the term string
		if x == "1" {
			b.WriteString(cstr)
		} else if cstr == "1" {
			b.WriteString(x)
		} else {
			b.WriteString(cstr)
			b.WriteByte('*')
			b.WriteString(x)
		}
		// Do we need to write some new-lines?
		if strings.Index(cstr, "\n") != -1 {
			b.WriteString("\n\n")
		}
	}
	// Return the string
	return b.String(), nil
}
