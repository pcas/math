//go:generate ../internal/template/template.py ../internal/template/element.txt element.go "package=rationalpolynomial" "coefficientPackage=bitbucket.org/pcas/math/rational" "coefficientParent=*rational.Parent" "coefficientElement=*rational.Element" "exponentPackage=bitbucket.org/pcas/math/vector/integervector" "exponentParent=*integervector.Parent" "exponentElement=*integervector.Element" "polynomial=polynomial" "description=rational coefficients"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../internal/template/element.txt".
To modify this file, edit "../internal/template/element.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Element defines a polynomial with rational coefficients.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/polynomial/rationalpolynomial
/////////////////////////////////////////////////////////////////////////
// element.go
/////////////////////////////////////////////////////////////////////////

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/common"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/strings"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
)

// Element describes a polynomial with rational coefficients.
type Element struct {
	parent *Parent                // The parent of this element
	es     exponents.Interface    // The parallel slices of exponents...
	cs     coefficients.Interface // ...and of coefficients
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// parentOfElement returns the parent of the polynomial f.
func parentOfElement(f *Element) *Parent {
	if f == nil {
		return nil
	}
	return f.parent
}

// isParent returns true iff R is the parent of the polynomial f.
func isParent(R *Parent, f *Element) bool {
	return parentOfElement(f) == R
}

// createZero creates a new zero polynomial for the given parent R. It does not assign zero to the parent.
func createZero(R *Parent) *Element {
	return &Element{
		parent: R,
		es:     exponents.EmptySlice(R.exponentsHelper()),
		cs:     coefficients.EmptySlice(R.coefficientsHelper()),
	}
}

// createOne creates a new constant polynomial 1 for the given parent R. It does not assign it to the parent.
func createOne(R *Parent) *Element {
	return &Element{
		parent: R,
		es:     exponents.SliceContainingZero(R.exponentsHelper()),
		cs:     coefficients.SliceContainingOne(R.coefficientsHelper()),
	}
}

// toMonomial returns the monomial x^m in the polynomial ring R, where m lies in the exponent monoid of R.
func toMonomial(R *Parent, m object.Element) (*Element, error) {
	es, cs, err := common.ToMonomial(
		m,
		R.exponentsHelper(),
		R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, es, cs), nil
}

// toTerm returns the term c * x^m  in the polynomial ring R, where c lies in the coefficient ring of R and m lies in the exponent monoid of R.
func toTerm(R *Parent, c object.Element, m object.Element) (*Element, error) {
	es, cs, err := common.ToTerm(
		m, c,
		R.exponentsHelper(),
		R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, es, cs), nil
}

// fromParallelSlices returns a polynomial in the polynomial ring R, with parallel slices of coefficients cs and exponents es. The coefficients should lie in the coefficient ring of R, and the exponents should lie in the exponents ring of R.
func fromParallelSlices(R *Parent, cs slice.Interface, es slice.Interface) (*Element, error) {
	fes, fcs, err := common.FromParallelSlices(es, cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, fes, fcs), nil
}

// createPolynomial returns a polynomial in R with the given parallel slices of exponents es and coefficients cs. Assumes that:
//	* the slices are of the same length;
//	* the exponents are sorted in increasing order;
//	* the exponents are distinct;
//	* the exponents are valid;
//  * the coefficients are non-zero.
func createPolynomial(R *Parent, es exponents.Interface, cs coefficients.Interface) *Element {
	// Assert that the slices are of the same length
	size := es.Len()
	if cs.Len() != size {
		panic(errors.SliceLengthNotEqual.New())
	} else if size == 0 {
		// Check whether the result is 0 or 1
		return Zero(R)
	} else if size == 1 {
		if isOne, err := common.IsOne(es, cs); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if isOne {
			return One(R)
		}
	} else if !exponents.IsSorted(es) {
		// Assert that the exponents are in increasing order
		panic(errors.SliceNotSorted.New())
	}
	// Return the new element
	return &Element{
		parent: R,
		es:     es,
		cs:     cs,
	}
}

// coefficientOfDegree returns the coefficient of the degree m term of the polynomial f, where m is an element of the exponent monoid. The returned coefficient will live in the coefficient ring of the parent polynomial ring.
func coefficientOfDegree(f *Element, m object.Element) (*rational.Element, error) {
	// Ensure that m lies in the exponent monomial
	R := parentOfElement(f)
	mm, err := R.ExponentMonoid().ToElement(m)
	if err != nil {
		return nil, err
	}
	// Extract the coefficient
	var c object.Element
	if f.IsZero() {
		c = R.CoefficientRing().Zero()
	} else if idx, err := exponents.Index(R.exponentsHelper(), f.es, mm); err != nil {
		return nil, err
	} else if idx == -1 {
		c = R.CoefficientRing().Zero()
	} else {
		c = f.cs.Entry(idx)
	}
	return c.(*rational.Element), nil
}

// scalarMultiplyByCoefficient returns the product a * f of the polynomial f with a, where a is an element in the coefficient ring of the parent.
func scalarMultiplyByCoefficient(a object.Element, f *Element) (*Element, error) {
	// Ensure that a is an element of the coefficient ring
	R := parentOfElement(f)
	K := R.CoefficientRing()
	aa, err := K.ToElement(a)
	if err != nil {
		return nil, err
	}
	// Fast-track the easy cases
	if f.IsZero() {
		return Zero(R), nil
	} else if isZero, err := K.IsZero(aa); err != nil {
		return nil, err
	} else if isZero {
		return Zero(R), nil
	} else if isOne, err := K.IsOne(aa); err != nil {
		return nil, err
	} else if isOne {
		return f, nil
	}
	// Calculate the multiple
	es, cs, err := common.ScalarMultiplyByCoefficient(
		aa, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the corresponding polynomial
	return createPolynomial(R, es, cs), nil
}

// applyBinaryOperation returns a polynomial in R as given by applying the given BinaryOperation to the polynomials f and g. Assumes that f and g are non-nil, and that R is the common parent.
func applyBinaryOperation(op common.BinaryOperation, R *Parent, f *Element, g *Element) (*Element, error) {
	// Perform the operation
	es, cs, err := op(
		f.es, f.cs, g.es, g.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the polynomial
	return createPolynomial(R, es, cs), nil
}

/////////////////////////////////////////////////////////////////////////
// Creation functions
/////////////////////////////////////////////////////////////////////////

// Zero returns the zero polynomial of R.
func Zero(R *Parent) *Element {
	if R == nil {
		return createZero(R)
	}
	return R.zero
}

// One returns the constant polynomial 1 of R.
func One(R *Parent) *Element {
	if R == nil {
		return createOne(R)
	}
	return R.one
}

// ToMonomial returns the monomial x^m in the polynomial ring R, where m is an element in the exponent monoid of R.
func ToMonomial(R *Parent, m *integervector.Element) (*Element, error) {
	return toMonomial(R, m)
}

// ToTerm returns the term c * x^m in the polynomial ring R, where c is an element in the coefficient ring of R and m is an element in the exponent monoid of R.
func ToTerm(R *Parent, c *rational.Element, m *integervector.Element) (*Element, error) {
	return toTerm(R, c, m)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ExponentMonoid returns the exponent monoid of the polynomial ring R.
func ExponentMonoid(R *Parent) *integervector.Parent {
	return R.ExponentMonoid().(*integervector.Parent)
}

// CoefficientRing returns the coefficient ring of the polynomial ring R.
func CoefficientRing(R *Parent) *rational.Parent {
	return R.CoefficientRing().(*rational.Parent)
}

// AreEqual returns true iff the polynomials f and g have the same parent, and f = g.
func AreEqual(f *Element, g *Element) bool {
	// Get the easy checks out of the way
	if f == g {
		return true
	} else if parentOfElement(f) != parentOfElement(g) {
		return false
	} else if f.IsZero() {
		return g.IsZero()
	} else if g.IsZero() {
		return false
	}
	// Perform the check
	areEqual, err := common.AreEqual(f.es, f.cs, g.es, g.cs)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return areEqual
}

// CmpLeadingMonomial returns -1 if LM(f) < LM(g), 0 if LM(f) = LM(g), and +1 if LM(f) > LM(g), were LM denotes the leading monomial. Here f and g are non-zero polynomials with the same parent.
func CmpLeadingMonomial(f *Element, g *Element) (int, error) {
	// Sanity check
	R := parentOfElement(f)
	if !isParent(R, g) {
		return 0, errors.ParentsDoNotAgree.New()
	} else if f.IsZero() {
		return 0, errors.IllegalZeroArg.New()
	} else if g.IsZero() {
		return 0, errors.IllegalZeroArg.New()
	}
	// Fast-track the obvious case of equality
	if f == g {
		return 0, nil
	}
	// Perform the comparison
	return common.CmpLeadingMonomial(f.es, g.es)
}

// Add returns the sum f + g of two polynomials f and g. This will return an error if f and g do not have the same parent.
func Add(f *Element, g *Element) (*Element, error) {
	// Check the parents agree
	R := parentOfElement(f)
	if !isParent(R, g) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the zero case
	if f.IsZero() {
		return g, nil
	} else if g.IsZero() {
		return f, nil
	}
	// Perform the sum
	return applyBinaryOperation(common.Add, R, f, g)
}

// Subtract returns the difference f - g of two polynomials f and g. This will return an error if f and g do not have the same parent.
func Subtract(f *Element, g *Element) (*Element, error) {
	// Check that the parents agree
	R := parentOfElement(f)
	if !isParent(R, g) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the easy cases
	if f.IsZero() {
		return Negate(g), nil
	} else if g.IsZero() {
		return f, nil
	} else if f == g {
		return Zero(R), nil
	}
	// Perform the subtraction
	return applyBinaryOperation(common.Subtract, R, f, g)
}

// Sum returns the sum of the polynomials in the slice S. All polynomials must have the same parent R. If S is empty, the zero element in R is returned.
func Sum(R *Parent, S ...*Element) (*Element, error) {
	// Fast-track the length 0, 1, and 2 cases
	n := len(S)
	if n == 0 {
		return Zero(R), nil
	} else if n == 1 {
		if !isParent(R, S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return S[0], nil
	} else if n == 2 {
		if !isParent(R, S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return Add(S[0], S[1])
	}
	// Create the slices of exponents and coefficients
	Es := make([]exponents.Interface, 0, n)
	Cs := make([]coefficients.Interface, 0, n)
	for _, f := range S {
		if !isParent(R, f) {
			return nil, errors.ArgNotContainedInParent.New()
		} else if !f.IsZero() {
			Es, Cs = append(Es, f.es), append(Cs, f.cs)
		}
	}
	// Is there anything to do?
	if len(Es) == 0 {
		return Zero(R), nil
	}
	// Perform the sum
	es, cs, err := common.Sum(Es, Cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the polynomial
	return createPolynomial(R, es, cs), nil
}

// Multiply returns the product f * g of two polynomials f and g. This will return an error if f and g do not have the same parent.
func Multiply(f *Element, g *Element) (*Element, error) {
	// Sanity check
	R := parentOfElement(f)
	if !isParent(R, g) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the zero and one cases
	if f.IsZero() {
		return Zero(R), nil
	} else if g.IsZero() {
		return Zero(R), nil
	} else if f.IsOne() {
		return g, nil
	} else if g.IsOne() {
		return f, nil
	}
	// Do the multiplication
	return applyBinaryOperation(common.Multiply, R, f, g)
}

// Negate returns the negation -f of the polynomial f.
func Negate(f *Element) *Element {
	// Fast-track the zero case
	R := parentOfElement(f)
	if f.IsZero() {
		return Zero(R)
	}
	// Negate the coefficients
	cs, err := common.Negate(f.cs, R.coefficientsHelper())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Return the polynomial
	return createPolynomial(R, f.es, cs)
}

// PowerInt64 returns the power f^k of the polynomial f.
func PowerInt64(f *Element, k int64) (*Element, error) {
	// Fast-track the easy cases
	R := parentOfElement(f)
	if k == 0 {
		return One(R), nil
	} else if k == 1 {
		return f, nil
	} else if k > 0 && f.IsZero() {
		return Zero(R), nil
	}
	// Compute the power
	es, cs, err := common.PowerInt64(f.es, f.cs, k, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the polynomial
	return createPolynomial(R, es, cs), nil
}

// Power returns the power f^k of the polynomial f.
func Power(f *Element, k *integer.Element) (*Element, error) {
	// Fast-track the easy cases
	R := parentOfElement(f)
	if k.IsZero() {
		return One(R), nil
	} else if k.IsOne() {
		return f, nil
	} else if k.IsPositive() && f.IsZero() {
		return Zero(R), nil
	}
	// Compute the power
	es, cs, err := common.Power(f.es, f.cs, k, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the polynomial
	return createPolynomial(R, es, cs), nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff f is the zero polynomial.
func (f *Element) IsZero() bool {
	return f == nil || f.es.Len() == 0
}

// IsOne returns true iff f is the constant polynomial 1.
func (f *Element) IsOne() bool {
	if f == nil || f.es.Len() != 1 {
		return false
	}
	isOne, err := common.IsOne(f.es, f.cs)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return isOne
}

// IsConstant returns true iff the polynomial f is a constant polynomial. If true, also returns the constant (as an element in the coefficient ring of the parent polynomial ring).
func (f *Element) IsConstant() (bool, *rational.Element) {
	if f.IsZero() {
		return true, parentOfElement(f).CoefficientRing().Zero().(*rational.Element)
	}
	isConstant, c, err := common.IsConstant(f.es, f.cs)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	} else if !isConstant {
		return false, nil
	}
	return true, c.(*rational.Element)
}

// IsMonomial returns true iff the polynomial f is a monomial (that is, if f is of the form x^m for some m in the monoid of exponents).
func (f *Element) IsMonomial() bool {
	if f.IsZero() {
		return false
	}
	isMonomial, err := common.IsMonomial(f.cs)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return isMonomial
}

// IsTerm returns true iff the polynomial f is a term (that is, if f is of the form c * x^m for some c in the coefficient ring, and m in the monoid of exponents).
func (f *Element) IsTerm() bool {
	return f == nil || f.es.Len() <= 1
}

// IsMonic returns true iff the polynomial f is monic (that is, if the leading coefficient of f is 1).
func (f *Element) IsMonic() bool {
	if f.IsZero() {
		return false
	}
	isMonic, err := common.IsMonic(f.cs)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return isMonic
}

// IsEqualTo returns true iff the polynomials f and g have the same parent, and f = g.
func (f *Element) IsEqualTo(g *Element) bool {
	return AreEqual(f, g)
}

// Negate returns the negation -f of the polynomial f.
func (f *Element) Negate() *Element {
	return Negate(f)
}

// PowerInt64 returns the power f^k of the polynomial f.
func (f *Element) PowerInt64(k int64) (*Element, error) {
	return PowerInt64(f, k)
}

// Power returns the power f^k of the polynomial f.
func (f *Element) Power(k *integer.Element) (*Element, error) {
	return Power(f, k)
}

// ScalarMultiplyByInt64 returns the scalar multiple n * f of the polynomial f with the integer n, where this is defined to be f + ... + f (n times) if n is positive, -f - ... - f (|n| times) if n is negative, and 0 if n is zero.
func (f *Element) ScalarMultiplyByInt64(n int64) *Element {
	// Get the easy cases out of the way
	if f.IsZero() || n == 1 {
		return f
	} else if n == 0 {
		return Zero(parentOfElement(f))
	} else if n == -1 {
		return Negate(f)
	}
	// Return the scalar multiple
	return f.ScalarMultiplyByInteger(integer.FromInt64(n))
}

// ScalarMultiplyByInteger returns the scalar multiple n * f of the polynomial f with the integer n, where this is defined to be f + ... + f (n times) if n is positive, -f - ... - f (|n| times) if n is negative, and 0 if n is zero.
func (f *Element) ScalarMultiplyByInteger(n *integer.Element) *Element {
	// Get the easy cases out of the way
	R := parentOfElement(f)
	if f.IsZero() || n.IsOne() {
		return f
	} else if n.IsZero() {
		return Zero(R)
	}
	// Calculate the scalar multiple
	es, cs, err := common.ScalarMultiplyByInteger(n, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Return the polynomial
	return createPolynomial(R, es, cs)
}

// ScalarMultiplyByCoefficient returns the product a * f of the polynomial f with a, where a is an element in the coefficient ring of the parent of f.
func (f *Element) ScalarMultiplyByCoefficient(a *rational.Element) (*Element, error) {
	return scalarMultiplyByCoefficient(a, f)
}

// Degree returns the degree of the non-zero polynomial f. The degree is an element of the exponent monoid of the parent polynomial ring.
func (f *Element) Degree() (*integervector.Element, error) {
	if f.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	}
	return f.es.Entry(f.es.Len() - 1).(*integervector.Element), nil
}

// LeadingMonomial returns the leading monomial of the non-zero polynomial f as an element of the polynomial ring.
func (f *Element) LeadingMonomial() (*Element, error) {
	// Sanity check
	if f.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	} else if f.IsMonomial() {
		return f, nil
	}
	// Create the exponent and coefficient slices for the leading monomial
	R := parentOfElement(f)
	es, cs, err := common.NthMonomial(f.es.Len()-1, f.es, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	// Return the corresponding polynomial
	return createPolynomial(R, es, cs), nil
}

// LeadingTerm returns the leading term of the polynomial f.
func (f *Element) LeadingTerm() *Element {
	// Sanity check
	if f.IsTerm() {
		return f
	}
	// Create the exponent and coefficient slices for the leading term
	R := parentOfElement(f)
	es, cs, err := common.NthTerm(f.es.Len()-1, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Return the corresponding polynomial
	return createPolynomial(R, es, cs)
}

// LeadingCoefficient returns the leading coefficient of the polynomial f. The returned coefficient will live in the coefficient ring of the parent polynomial ring.
func (f *Element) LeadingCoefficient() *rational.Element {
	var c object.Element
	if f.IsZero() {
		c = parentOfElement(f).CoefficientRing().Zero()
	} else {
		c = f.cs.Entry(f.cs.Len() - 1)
	}
	return c.(*rational.Element)
}

// Valuation returns the exponent of the smallest non-zero term of the non-zero polynomial f. This is returned as an element of the exponent monoid of the parent polynomial ring.
func (f *Element) Valuation() (*integervector.Element, error) {
	if f.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	}
	return f.es.Entry(0).(*integervector.Element), nil
}

// ConstantCoefficient returns the constant coefficient of the polynomial f. The returned coefficient will live in the coefficient ring of the parent polynomial ring.
func (f *Element) ConstantCoefficient() *rational.Element {
	var c object.Element
	if f.IsZero() {
		c = parentOfElement(f).CoefficientRing().Zero()
	} else if idx := exponents.IndexOfZero(f.es); idx == -1 {
		c = parentOfElement(f).CoefficientRing().Zero()
	} else {
		c = f.cs.Entry(idx)
	}
	return c.(*rational.Element)
}

// Coefficient returns the coefficient of the degree m term of the polynomial f, where m is an element of the exponent monoid. The returned coefficient will live in the coefficient ring of the parent polynomial ring.
func (f *Element) Coefficient(m *integervector.Element) (*rational.Element, error) {
	return coefficientOfDegree(f, m)
}

// Exponents returns a slice of exponents of the polynomial f. Only the exponents corresponding to terms with non-zero coefficient will be returned, hence the zero polynomial returns an empty slice. The returned exponents will live in the exponent monoid of the parent polynomial ring, and are sorted in increasing order.
func (f *Element) Exponents() []*integervector.Element {
	if f.IsZero() {
		return []*integervector.Element{}
	}
	size := f.es.Len()
	es := make([]*integervector.Element, 0, size)
	for i := 0; i < size; i++ {
		es = append(es, f.es.Entry(i).(*integervector.Element))
	}
	return es
}

// Coefficients returns a slice of coefficient of the polynomial f. Only the non-zero coefficient will be returned, hence the zero polynomial returns an empty slice. The returned coefficients will live in the coefficient ring of the parent polynomial ring.
func (f *Element) Coefficients() []*rational.Element {
	if f.IsZero() {
		return []*rational.Element{}
	}
	size := f.cs.Len()
	cs := make([]*rational.Element, 0, size)
	for i := 0; i < size; i++ {
		cs = append(cs, f.cs.Entry(i).(*rational.Element))
	}
	return cs
}

// CoefficientsAndExponents returns parallel slices of coefficient cs and exponents es of the polynomial f. Only the terms with non-zero coefficient will be returned, hence the zero polynomial returns a pair of empty slices. The returned coefficients will live in the coefficient ring of the parent polynomial ring. The returned exponents will live in the exponent monoid of the parent polynomial ring, and are sorted in increasing order.
func (f *Element) CoefficientsAndExponents() (cs []*rational.Element, es []*integervector.Element) {
	return f.Coefficients(), f.Exponents()
}

// Terms returns the non-zero terms of the polynomial f, sorted in increasing exponent order.
func (f *Element) Terms() []*Element {
	if f.IsZero() {
		return []*Element{}
	} else if f.IsTerm() {
		return []*Element{f}
	}
	R := parentOfElement(f)
	size := f.es.Len()
	T := make([]*Element, 0, size)
	for i := 0; i < size; i++ {
		es, cs, err := common.NthTerm(i, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		T = append(T, createPolynomial(R, es, cs))
	}
	return T
}

// Hash returns a hash value for the polynomial f.
func (f *Element) Hash() hash.Value {
	if f.IsZero() {
		return 0
	}
	return common.Hash(f.es, f.cs)
}

// Parent returns the parent of the polynomial f.
func (f *Element) Parent() object.Parent {
	return parentOfElement(f)
}

// String returns a string representation of the polynomial f.
func (f *Element) String() string {
	if f.IsZero() {
		return "0"
	}
	str, err := strings.ToString(f.es, f.cs, parentOfElement(f).varNames)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return str
}
