//go:generate ../internal/template/template.py ../internal/template/variable.txt variable.go "package=rationalpolynomial" "polynomial=polynomial"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../internal/template/variable.txt".
To modify this file, edit "../internal/template/variable.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Variable defines helpful functions for use when working with the variables of a polynomial ring.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/polynomial/rationalpolynomial
/////////////////////////////////////////////////////////////////////////
// variable.go
/////////////////////////////////////////////////////////////////////////

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/common"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Variables returns the variables x_0,...,x_k of the polynomial ring R, where k+1 is equal to the rank of R.
func Variables(R *Parent) []*Element {
	// Recover the parallel slices of exponents and coefficients
	ees, ccs, err := common.Basis(R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Convert the slices to elements
	vars := make([]*Element, 0, len(ees))
	for i, es := range ees {
		vars = append(vars, createPolynomial(R, es, ccs[i]))
	}
	return vars
}

// NthVariable returns the n-th variable x_n in the polynomial ring R. Here variables are indexed from 0 to rk-1 (inclusive), where rk is the rank of R.
func NthVariable(R *Parent, n int) (*Element, error) {
	es, cs, err := common.NthBasisElement(n, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, es, cs), nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Variables returns the variables x_0,...,x_k of the polynomial ring R, where k+1 is equal to the rank of R.
func (R *Parent) Variables() []object.Element {
	// Recover the parallel slices of exponents and coefficients
	ees, ccs, err := common.Basis(R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Convert the slices to elements
	vars := make([]object.Element, 0, len(ees))
	for i, es := range ees {
		vars = append(vars, createPolynomial(R, es, ccs[i]))
	}
	return vars
}

// NthVariable returns the n-th variable x_n in the polynomial ring R. Here variables are indexed from 0 to rk-1 (inclusive), where rk is the rank of R.
func (R *Parent) NthVariable(n int) (*Element, error) {
	return NthVariable(R, n)
}

// IsNthVariable returns true iff the polynomial f is the n-th variable x_n in the polynomial ring R. Here variables are indexed from 0 to rk-1 (inclusive), where rk is the rank of R.
func (R *Parent) IsNthVariable(f object.Element, n int) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsNthVariable(n)
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsNthVariable returns true iff the polynomial f is the n-th variable x_n in the parent polynomial ring. Here variables are indexed from 0 to rk-1 (inclusive), where rk is the rank of the parent.
func (f *Element) IsNthVariable(n int) (bool, error) {
	// Get the zero case out of the way
	R := parentOfElement(f)
	if f.IsZero() {
		if rk := R.ExponentMonoid().Rank(); n < 0 || n >= rk {
			return false, errors.InvalidIndexRange.New(n, rk-1)
		}
		return false, nil
	}
	// Perform the check
	return common.IsNthBasisElement(n, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
}
