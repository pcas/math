// Rank 1 provides creation functions for the univariate polynomial ring with rational coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// fromSlice returns a univariate polynomial in R with coefficients from the slice cs. The exponents, which must all be non-negative, begin at degree "initial" and increase in degree by "step" (which can be positive or negative).
func fromSlice(R *Parent, initial int64, step int64, cs slice.Interface) (*Element, error) {
	// The polynomial ring must be rank 1
	if R.ExponentMonoid().Rank() != 1 {
		return nil, errors.RankMustBeOne.New()
	}
	// Convert the initial degree and step size to integers
	n, k := integer.FromInt64(initial), integer.FromInt64(step)
	// Create the slice of exponents
	es, err := slice.NewGenerator(cs.Len(), func(i int) object.Element {
		return integer.MultiplyByInt64ThenAdd(int64(i), k, n)
	})
	if err != nil {
		return nil, err
	}
	// Return the polynomial
	return fromParallelSlices(R, cs, es)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultUnivariateRing returns the default univariate polynomial ring over QQ.
func DefaultUnivariateRing() *Parent {
	R, err := DefaultRing(1, monomialorder.Lex)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return R
}

// NewUnivariateRing returns a new univariate polynomial ring over QQ.
func NewUnivariateRing() *Parent {
	R, err := NewRing(1, monomialorder.Lex)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return R
}

// FromParallelRationalSlices returns a polynomial in the univariate polynomial ring R, with parallel slices of coefficients cs and exponents es.
func FromParallelRationalSlices(R *Parent, cs []*rational.Element, es []*integer.Element) (*Element, error) {
	if R.ExponentMonoid().Rank() != 1 {
		return nil, errors.RankMustBeOne.New()
	}
	return fromParallelSlices(R, rational.Slice(cs), integer.Slice(es))
}

// FromParallelIntegerSlices returns a polynomial in the univariate polynomial ring R, with parallel slices of coefficients cs and exponents es.
func FromParallelIntegerSlices(R *Parent, cs []*integer.Element, es []*integer.Element) (*Element, error) {
	if R.ExponentMonoid().Rank() != 1 {
		return nil, errors.RankMustBeOne.New()
	}
	return fromParallelSlices(R, integer.Slice(cs), integer.Slice(es))
}

// FromParallelIntSlices returns a polynomial in the univariate polynomial ring R, with parallel slices of coefficients cs and exponents es.
func FromParallelIntSlices(R *Parent, cs []int, es []int) (*Element, error) {
	if R.ExponentMonoid().Rank() != 1 {
		return nil, errors.RankMustBeOne.New()
	}
	return fromParallelSlices(R, integer.IntSlice(cs), integer.IntSlice(es))
}

// FromParallelInt64Slices returns a polynomial in the univariate polynomial ring R, with parallel slices of coefficients cs and exponents es.
func FromParallelInt64Slices(R *Parent, cs []int64, es []int64) (*Element, error) {
	if R.ExponentMonoid().Rank() != 1 {
		return nil, errors.RankMustBeOne.New()
	}
	return fromParallelSlices(R, integer.Int64Slice(cs), integer.Int64Slice(es))
}

// FromRationalSlice returns a univariate polynomial in R with coefficients from the slice cs. The exponents, which must all be non-negative, begin at degree "initial" and increase in degree by "step" (which can be positive or negative).
func FromRationalSlice(R *Parent, initial int64, step int64, cs []*rational.Element) (*Element, error) {
	return fromSlice(R, initial, step, rational.Slice(cs))
}

// FromIntegerSlice returns a univariate polynomial in R with coefficients from the slice cs. The exponents, which must all be non-negative, begin at degree "initial" and increase in degree by "step" (which can be positive or negative).
func FromIntegerSlice(R *Parent, initial int64, step int64, cs []*integer.Element) (*Element, error) {
	return fromSlice(R, initial, step, integer.Slice(cs))
}

// FromIntSlice returns a univariate polynomial in R with coefficients from the slice cs. The exponents, which must all be non-negative, begin at degree "initial" and increase in degree by "step" (which can be positive or negative).
func FromIntSlice(R *Parent, initial int64, step int64, cs []int) (*Element, error) {
	return fromSlice(R, initial, step, integer.IntSlice(cs))
}

// FromInt64Slice returns a univariate polynomial in R with coefficients from the slice cs. The exponents, which must all be non-negative, begin at degree "initial" and increase in degree by "step" (which can be positive or negative).
func FromInt64Slice(R *Parent, initial int64, step int64, cs []int64) (*Element, error) {
	return fromSlice(R, initial, step, integer.Int64Slice(cs))
}
