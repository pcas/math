//go:generate ../internal/template/template.py ../internal/template/parent.txt parent.go "package=rationalpolynomial" "polynomial=polynomial" "description=rational coefficients"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../internal/template/parent.txt".
To modify this file, edit "../internal/template/parent.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Parent defines a polynomial ring with rational coefficients.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/polynomial/rationalpolynomial
/////////////////////////////////////////////////////////////////////////
// parent.go
/////////////////////////////////////////////////////////////////////////
/*
Requires the functions:

// defaultExponentsHelper returns the default exponents helper, as required by the nil parent.
func defaultExponentsHelper() exponents.Helper {
	...
}

// defaultCoefficientsHelper returns the default coefficients helper, as required by the nil parent.
func defaultCoefficientsHelper() coefficients.Helper {
	...
}
*/

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/strings"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	stdstrings "strings"
)

// Parent describes a polynomial ring with rational coefficients.
type Parent struct {
	eh       exponents.Helper    // The exponents helper
	ch       coefficients.Helper // The coefficients helper
	name     string              // The assigned name
	varNames []string            // The names of the variables
	zero     *Element            // The additive identity element
	one      *Element            // The multiplicative identity element
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// objectsToElements returns the arguments as an element in the given parent, if that's where they belong.
func objectsToElements(R *Parent, f object.Element, g object.Element) (*Element, *Element, error) {
	ff, err1 := ToElement(R, f)
	if err1 != nil {
		return nil, nil, errors.Arg1NotContainedInParent.New()
	}
	gg, err2 := ToElement(R, g)
	if err2 != nil {
		return nil, nil, errors.Arg2NotContainedInParent.New()
	}
	return ff, gg, nil
}

// createRing returns a new polynomial ring with given exponents helper eh and coefficients helper ch.
func createRing(eh exponents.Helper, ch coefficients.Helper) *Parent {
	R := &Parent{
		eh:       eh,
		ch:       ch,
		varNames: strings.DefaultVariableNames(eh.Universe().Rank()),
	}
	R.zero = createZero(R)
	R.one = createOne(R)
	return R
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// exponentsHelper return the exponents helper for the polynomial ring.
func (R *Parent) exponentsHelper() exponents.Helper {
	if R == nil {
		return defaultExponentsHelper()
	}
	return R.eh
}

// coefficientsHelper return the coefficients helper for the polynomial ring.
func (R *Parent) coefficientsHelper() coefficients.Helper {
	if R == nil {
		return defaultCoefficientsHelper()
	}
	return R.ch
}

// CoefficientRing returns the ring of coefficients of the polynomial ring R.
func (R *Parent) CoefficientRing() ring.Interface {
	return R.coefficientsHelper().Universe()
}

// ExponentMonoid returns the monoid of exponents of the polynomial ring R.
func (R *Parent) ExponentMonoid() polynomial.ExponentMonoid {
	return R.exponentsHelper().Universe()
}

// ToMonomial returns the monomial x^m in the polynomial ring R, where m lies in the exponent monoid of R.
func (R *Parent) ToMonomial(m object.Element) (object.Element, error) {
	return toMonomial(R, m)
}

// ToTerm returns the term c * x^m  in the polynomial ring R, where c lies in the coefficient ring of R and m lies in the exponent monoid of R.
func (R *Parent) ToTerm(c object.Element, m object.Element) (object.Element, error) {
	return toTerm(R, c, m)
}

// FromParallelSlices returns a polynomial in the polynomial ring R, with parallel slices of coefficients cs and exponents es. The coefficients should lie in the coefficient ring of R, and the exponents should lie in the exponents ring of R.
func (R *Parent) FromParallelSlices(cs []object.Element, es []object.Element) (*Element, error) {
	return fromParallelSlices(R, slice.ElementSlice(es), slice.ElementSlice(cs))
}

// Zero returns the zero element of the polynomial ring R.
func (R *Parent) Zero() object.Element {
	return Zero(R)
}

// IsZero returns true iff f is the zero element in the polynomial ring R.
func (R *Parent) IsZero(f object.Element) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsZero(), nil
}

// One returns the constant polynomial 1 of the polynomial ring R.
func (R *Parent) One() object.Element {
	return One(R)
}

// IsOne returns true iff f is the constant polynomial 1 in the polynomial ring R.
func (R *Parent) IsOne(f object.Element) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsOne(), nil
}

// IsConstant returns true iff f is a constant polynomial in the polynomial ring R. If true, also returns the constant (as an element in the coefficient ring of R).
func (R *Parent) IsConstant(f object.Element) (bool, object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, nil, err
	}
	ok, c := ff.IsConstant()
	return ok, c, nil
}

// IsMonomial returns true iff the polynomial f is a monomial (that is, if f is of the form x^m for some m in the exponent monoid of R).
func (R *Parent) IsMonomial(f object.Element) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsMonomial(), nil
}

// IsTerm returns true iff the polynomial f is a term (that is, if f is of the form c * x^n for some c in the coefficient ring of R and m in the exponent monoid of R).
func (R *Parent) IsTerm(f object.Element) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsTerm(), nil
}

// IsMonic returns true iff f is a monic polynomial (that is, if the leading coefficient of f is 1).
func (R *Parent) IsMonic(f object.Element) (bool, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return false, err
	}
	return ff.IsMonic(), nil
}

// Degree returns the degree of the non-zero polynomial f. The returned exponent will live in the exponent monoid of R.
func (R *Parent) Degree(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.Degree()
}

// LeadingMonomial returns the leading monomial of the non-zero polynomial f.
func (R *Parent) LeadingMonomial(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.LeadingMonomial()
}

// LeadingTerm returns the leading term of the polynomial f.
func (R *Parent) LeadingTerm(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.LeadingTerm(), nil
}

// LeadingCoefficient returns the leading coefficient of the polynomial f. The returned coefficient will live in the coefficient ring of R.
func (R *Parent) LeadingCoefficient(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.LeadingCoefficient(), nil
}

// Valuation returns the exponent of the smallest non-zero term of the non-zero polynomial f. This is returned as an element of the exponent monoid of R.
func (R *Parent) Valuation(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.Valuation()
}

// ConstantCoefficient returns the constant coefficient of the polynomial f. The returned coefficient will live in the coefficient ring of R.
func (R *Parent) ConstantCoefficient(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.ConstantCoefficient(), nil
}

// Coefficient returns the coefficient of the degree m term of the polynomial f, where m is an element of the exponent monoid of R. The returned coefficient will live in the coefficient ring of R.
func (R *Parent) Coefficient(f object.Element, m object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return coefficientOfDegree(ff, m)
}

// Exponents returns a slice of exponents of the polynomial f. Only the exponents corresponding to terms with non-zero coefficient will be returned, hence the zero polynomial returns an empty slice. The returned exponents will live in the exponent monoid of R, and are sorted in increasing order.
func (R *Parent) Exponents(f object.Element) ([]object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	} else if ff.IsZero() {
		return []object.Element{}, nil
	}
	return slice.ToElementSlice(ff.es), nil
}

// Coefficients returns a slice of coefficient of the polynomial f. Only the non-zero coefficient will be returned, hence the zero polynomial returns an empty slice. The returned coefficients will live in the coefficient ring of R.
func (R *Parent) Coefficients(f object.Element) ([]object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	} else if ff.IsZero() {
		return []object.Element{}, nil
	}
	return slice.ToElementSlice(ff.cs), nil
}

// CoefficientsAndExponents returns two parallel slices of coefficients cs and exponents es of the polynomial f. Only the non-zero coefficient will be returned, hence the zero polynomial returns as pair of empty slice. The returned coefficients cs will live in the coefficient ring of R. The returned exponents es will live in the exponent monoid of R, and are sorted in increasing order.
func (R *Parent) CoefficientsAndExponents(f object.Element) (cs []object.Element, es []object.Element, err error) {
	var ff *Element
	if ff, err = ToElement(R, f); err == nil && !ff.IsZero() {
		cs, es = slice.ToElementSlice(ff.cs), slice.ToElementSlice(ff.es)
	}
	return
}

// Terms returns the non-zero terms of the polynomial f, sorted in increasing exponent order.
func (R *Parent) Terms(f object.Element) ([]object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	} else if ff.IsZero() {
		return []object.Element{}, nil
	}
	T := ff.Terms()
	TT := make([]object.Element, 0, len(T))
	for _, t := range T {
		TT = append(TT, t)
	}
	return TT, nil
}

// Add returns the sum f + g of the polynomials f and g in the ring R.
func (R *Parent) Add(f object.Element, g object.Element) (object.Element, error) {
	ff, gg, err := objectsToElements(R, f, g)
	if err != nil {
		return nil, err
	}
	return Add(ff, gg)
}

// Subtract returns the difference f - g of the polynomials f and g in the ring R.
func (R *Parent) Subtract(f object.Element, g object.Element) (object.Element, error) {
	ff, gg, err := objectsToElements(R, f, g)
	if err != nil {
		return nil, err
	}
	return Subtract(ff, gg)
}

// Negate returns -f, where f is a polynomial in the ring R.
func (R *Parent) Negate(f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.Negate(), nil
}

// Sum returns the sum of the polynomials in the slice S. All polynomials must have the same parent R. If S is empty, the zero element in the polynomial ring R is returned.
func (R *Parent) Sum(S ...object.Element) (object.Element, error) {
	// Convert the elements in S
	SS := make([]*Element, 0, len(S))
	for _, f := range S {
		ff, err := ToElement(R, f)
		if err != nil {
			return nil, err
		}
		SS = append(SS, ff)
	}
	// Return the sum
	return Sum(R, SS...)
}

// Multiply returns the product f * g of the polynomials f and g in the polynomial ring R.
func (R *Parent) Multiply(f object.Element, g object.Element) (object.Element, error) {
	ff, gg, err := objectsToElements(R, f, g)
	if err != nil {
		return nil, err
	}
	return Multiply(ff, gg)
}

// Power returns f^k, where k is a non-negative integer and f is a polynomial in the ring R.
func (R *Parent) Power(f object.Element, k *integer.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, errors.Arg1NotContainedInParent.New()
	}
	return ff.Power(k)
}

// ScalarMultiplyByInteger returns c * f, where this is defined to be f + ... + f (c times) if c is positive, -f - ... - f (|c| times) if c is negative, and 0 if c is zero. Here f is an element of the polynomial ring R.
func (R *Parent) ScalarMultiplyByInteger(c *integer.Element, f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, errors.Arg2NotContainedInParent.New()
	}
	return ff.ScalarMultiplyByInteger(c), nil
}

// ScalarMultiplyByCoefficient returns the product c * f, where c is an element in the coefficient ring of R, and f is an element of the polynomial ring R.
func (R *Parent) ScalarMultiplyByCoefficient(c object.Element, f object.Element) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, errors.Arg2NotContainedInParent.New()
	}
	return scalarMultiplyByCoefficient(c, ff)
}

// ToElement returns f as an element in the polynomial ring R, or an error if f cannot naturally be regarded as an element of R.
func (R *Parent) ToElement(f object.Element) (object.Element, error) {
	return ToElement(R, f)
}

// Contains returns true iff f is a polynomial in the ring R, or can naturally be regarded as such. Note that we do not allow a natural isomorphism between two different polynomial rings.
func (R *Parent) Contains(f object.Element) bool {
	_, err := ToElement(R, f)
	return err == nil
}

// AreEqual returns true iff f and g are both contained in the polynomial ring R, and f = g.
func (R *Parent) AreEqual(f object.Element, g object.Element) (bool, error) {
	ff, gg, err := objectsToElements(R, f, g)
	if err != nil {
		return false, err
	}
	return AreEqual(ff, gg), nil
}

// CmpLeadingMonomial returns -1 if LM(f) < LM(g), 0 if LM(f) = LM(g), and +1 if LM(f) > LM(g), were LM denotes the leading monomial, and f and g are non-zero {{polynomials}}s in the ring R.
func (R *Parent) CmpLeadingMonomial(f object.Element, g object.Element) (int, error) {
	ff, gg, err := objectsToElements(R, f, g)
	if err != nil {
		return 0, err
	}
	return CmpLeadingMonomial(ff, gg)
}

// AssignName assigns a name to the polynomial ring R to be used in printing.
func (R *Parent) AssignName(name string) {
	if R != nil {
		R.name = name
	}
}

// AssignVariableName assigns a name to the i-th variable of the polynomial ring R to be used in printing. The variables are indexed from 0 up to rank - 1 (inclusive).
func (R *Parent) AssignVariableName(name string, i int) error {
	if R == nil {
		return errors.InvalidIndexRange.New(i, -1)
	} else if i < 0 || i >= len(R.varNames) {
		return errors.InvalidIndexRange.New(i, len(R.varNames)-1)
	}
	R.varNames[i] = name
	return nil
}

// VariableName returns the name to be used in printing of the i-th variable of the polynomial ring R. The variables are indexed from 0 up to rank - 1 (inclusive).
func (R *Parent) VariableName(i int) (string, error) {
	if R == nil {
		return "", errors.InvalidIndexRange.New(i, -1)
	} else if i < 0 || i >= len(R.varNames) {
		return "", errors.InvalidIndexRange.New(i, len(R.varNames)-1)
	}
	return R.varNames[i], nil
}

// String returns a string representation of the polynomial ring R.
func (R *Parent) String() string {
	if R == nil {
		return R.CoefficientRing().String() + "[]"
	}
	name := R.name
	if len(name) == 0 {
		name = R.CoefficientRing().String()
	}
	return name + "[" + stdstrings.Join(R.varNames, ",") + "]"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToElement returns f as an element in the polynomial ring R with rational coefficients, if f can naturally be regarded as belonging there.
func ToElement(R *Parent, f object.Element) (*Element, error) {
	if ff, ok := f.(*Element); ok {
		// Is f a *Element? Is R the parent?
		if isParent(R, ff) {
			return ff, nil
		}
	} else if c, err := R.CoefficientRing().ToElement(f); err == nil {
		// Is f an element of the coefficient ring?
		if ff, err := toTerm(R, c, R.ExponentMonoid().Zero()); err == nil {
			return ff, nil
		}
	}
	// f is not naturally in R
	return nil, errors.ArgNotContainedInParent.New()
}
