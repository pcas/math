//go:generate ../internal/template/template.py ../internal/template/derivative.txt derivative.go "package=rationalpolynomial" "polynomial=polynomial"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../internal/template/derivative.txt".
To modify this file, edit "../internal/template/derivative.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Derivative provides functions for computing formal derivatives of polynomials.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/polynomial/rationalpolynomial
/////////////////////////////////////////////////////////////////////////
// derivative.go
/////////////////////////////////////////////////////////////////////////

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/polynomial/internal/common"
)

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal partial derivative df/dx_k of the polynomial f. Here the variables x_i are indexed from 0 to rk-1 (inclusive), where rk is the rank of the exponent monoid of R.
func (R *Parent) Derivative(f object.Element, k int) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.Derivative(k)
}

// NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where n is a non-negative integer. Here the variables x_i are indexed from 0 to rk-1 (inclusive), where rk is the rank of the exponent monoid of R.
func (R *Parent) NthDerivative(f object.Element, k int, n int) (object.Element, error) {
	ff, err := ToElement(R, f)
	if err != nil {
		return nil, err
	}
	return ff.NthDerivative(k, n)
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Derivative returns the formal partial derivative df/dx_k of the polynomial f. Here the variables x_i are indexed from 0 to rk-1 (inclusive), where rk is the rank of the parent.
func (f *Element) Derivative(k int) (*Element, error) {
	// Sanity check
	R := parentOfElement(f)
	if f.IsZero() {
		if rk := R.ExponentMonoid().Rank(); k < 0 || k >= rk {
			return nil, errors.InvalidIndexRange.New(k, rk-1)
		}
		return Zero(R), nil
	}
	// Compute the derivative
	es, cs, err := common.Derivative(k, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, es, cs), nil
}

// NthDerivative returns the n-th formal partial derivative d^nf/dx_k^n of the polynomial f, where n is a non-negative integer. Here the variables x_i are indexed from 0 to rk-1 (inclusive), where rk is the rank of the parent.
func (f *Element) NthDerivative(k int, n int) (*Element, error) {
	// Sanity check
	if n < 0 {
		return nil, errors.Arg1MustBeNonNegative.New()
	}
	R := parentOfElement(f)
	if f.IsZero() {
		if rk := R.ExponentMonoid().Rank(); k < 0 || k >= rk {
			return nil, errors.InvalidIndexRange.New(k, rk-1)
		}
		return Zero(R), nil
	}
	// Compute the derivative
	es, cs, err := common.NthDerivative(n, k, f.es, f.cs, R.exponentsHelper(), R.coefficientsHelper())
	if err != nil {
		return nil, err
	}
	return createPolynomial(R, es, cs), nil
}
