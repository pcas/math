// Creation provides creation functions for the polynomial ring with rational coefficients.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rationalpolynomial

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients"
	"bitbucket.org/pcas/math/polynomial/internal/coefficients/rationalcoefficients"
	"bitbucket.org/pcas/math/polynomial/internal/exponents"
	"bitbucket.org/pcas/math/polynomial/internal/exponents/integerexponents"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/vector/integervector"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// The cached Parents and a mutex controlling access.
var (
	cache map[monomialorder.Type]map[int]*Parent
	mutex sync.RWMutex
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of parents
	cache = make(map[monomialorder.Type]map[int]*Parent)
}

// defaultExponentsHelper returns the default exponents helper, as required by the nil parent.
func defaultExponentsHelper() exponents.Helper {
	eh, err := integerexponents.NewHelper(0, monomialorder.Grevlex)
	if err != nil {
		panic(fatal.ImpossibleError(err))
	}
	return eh
}

// defaultCoefficientsHelper returns the default coefficients helper, as required by the nil parent.
func defaultCoefficientsHelper() coefficients.Helper {
	return rationalcoefficients.NewHelper()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultRing returns the default polynomial ring over QQ of given rank and monomial order.
func DefaultRing(rk int, order monomialorder.Type) (*Parent, error) {
	// Sanity check
	if rk < 0 {
		return nil, errors.RankMustBeNonNegative.New()
	} else if !order.IsValid() {
		return nil, errors.UnknownMonomialOrder.New(order)
	}
	// Aquire a read lock on the mutex and check the cache
	mutex.RLock()
	if Rs, ok := cache[order]; ok {
		if R, ok := Rs[rk]; ok {
			// The Parent is in the cache so return it
			mutex.RUnlock()
			return R, nil
		}
	}
	// No luck -- release the read lock and create the new Parent
	mutex.RUnlock()
	R, err := NewRing(rk, order)
	if err != nil {
		return nil, err
	}
	// Acquire a write lock and check the cache once again before returning
	mutex.Lock()
	Rs, ok := cache[order]
	if !ok {
		Rs = make(map[int]*Parent)
		cache[order] = Rs
	}
	if RR, ok := Rs[rk]; ok {
		R = RR
	} else {
		Rs[rk] = R
	}
	// Release the write lock and return the Parent
	mutex.Unlock()
	return R, nil
}

// NewRing returns a new polynomial ring over QQ of given rank and monomial order.
func NewRing(rk int, order monomialorder.Type) (*Parent, error) {
	// Create the helpers
	eh, err := integerexponents.NewHelper(rk, order)
	if err != nil {
		return nil, err
	}
	ch := rationalcoefficients.NewHelper()
	// Return the ring
	return createRing(eh, ch), nil
}

// FromParallelSlices returns a polynomial in the polynomial ring R, with parallel slices of coefficients cs and exponents es. The exponents should lie in the monoid of exponents of R.
func FromParallelSlices(R *Parent, cs []*rational.Element, es []*integervector.Element) (*Element, error) {
	return fromParallelSlices(R, rational.Slice(cs), integervector.Slice(es))
}
