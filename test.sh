#!/bin/bash

####################################################################
# Works through the packages in a logical order running go-test.
# Add new packages to test to the PACKAGES array.
####################################################################

# The array of packages to test, in the order given.
PACKAGES=(
    'errors'
    'object'
    'integer'
    'testing/assert'
    'rational'
    'finitefield/gfp'
    'boolean'
    'compare'
    'compare/monomialorder'
    'abelianmonoid'
    'abeliangroup'
    'ring'
    'slice'
    'module'
    'field'
    'objectmap'
    'product'
    'internal/int64number'
    'internal/uint64number'
    'internal/int64vector'
    'internal/uint64vector'
    'vector/integervector'
    'matrix/integermatrix/integermatrixalgebra'
    'matrix/integermatrix/integermatrixspace'
    'matrix/fpmatrix/fpmatrixspace'
    'polynomial'
    'polynomial/integerpolynomial'
    'polynomial/rationalpolynomial'
)

####################################################################

# Get the sctip name
PROG=$(basename ${0})

# Function to output the help message and exit.
function helpmessage {
    echo "Usage: ${PROG}"
    exit 1
} >&2

# Displays an error message and exits.
function error_msg {
    printf "${PROG}: ${1}\n"
    exit 1
} >&2

# There had better be no arguments
if [ $# -ne 0 ]; then
    helpmessage
fi

# Compute the formatting data
LENGTH=0
for PACKAGE in "${PACKAGES[@]}"; do
    if [ ${#PACKAGE} -gt ${LENGTH} ]; then
        LENGTH=${#PACKAGE}
    fi
done
if [ ${LENGTH} -gt 54 ]; then
    LENGTH=54
fi

# The colours
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# Work through the packages
EXITCODE=0
for PACKAGE in "${PACKAGES[@]}"; do
    # Create the padding we'll need
    STR=" "
    for ((i = 0; i < $((${LENGTH} - ${#PACKAGE})); i++)); do
        STR="${STR} "
    done
    # Output the package name (with padding)
    printf "Testing ${PACKAGE}...${STR}"
    # Ensure that the package directory exists
    if [ ! -d "${PACKAGE}" ]; then
        echo -e "${RED}[Failed]${NC}"
        echo " -> Directory does not exist."
        EXITCODE=1
    else
        # Are there any test files for this package?
        pushd "${PACKAGE}" &> /dev/null
        COUNT=`ls -1 *_test.go 2> /dev/null | wc -l`
        if [ ${COUNT} == 0 ]; then
            # Warn that there are no test files for this package
            echo -e "${ORANGE}[No tests]${NC}"
        else
            # Run the tests
            OUTPUT=`go test -cover 2>&1`
            if [ $? -eq 0 ]; then
                # The test fan successfully - extract the coverage
                COVER=`echo "${OUTPUT}" | grep "coverage: "`
                if [[ "${COVER}" =~ [0-9][.0-9]*% ]]; then
                    PERCENT="${BASH_REMATCH[0]}"
                    if [ "x${PERCENT}" = "x0.0%" ]; then
                        echo -e "${GREEN}[OK]${NC}"
                    else
                        echo -e "${GREEN}[OK]${NC} ${BLUE}${BASH_REMATCH[0]}${NC}"
                    fi
                else
                    echo -e "${GREEN}[OK]${NC}"
                fi
            elif [ `echo "${OUTPUT}" | tail -n1 | grep -c "\[build failed\]"` != 0 ]; then
                # The build failed
                echo -e "${RED}[Build failed]${NC}"
                EXITCODE=1
            else
                # The test ran and failed
                echo -e "${RED}[Failed]${NC}"
                EXITCODE=1
            fi
        fi
        popd &> /dev/null
    fi
done

# Exit with a non-zero exit code if a test failed
exit ${EXITCODE}
