// Parent defines the ZZ-module whose elements are n-dimensional integer-valued vectors. That is, it defined ZZ^n.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/objectmap"
	"strconv"
	"sync"
)

// Parent is an n-dimensional ZZ-module.
type Parent struct {
	n     int                // The dimension
	name  string             // The assigned name
	order monomialorder.Type // The monomial order to use
	zero  *Element           // The zero element
}

// The default lattices and a mutex controlling access.
var (
	defaultLattice      map[monomialorder.Type]map[int]*Parent
	defaultLatticeMutex *sync.RWMutex
)

// toIntegerSlicer is the interface for converting an object to a slice of integer elements, without an error value.
type toIntegerSlicer interface {
	ToIntegerSlice() []*integer.Element // ToIntegerSlice returns the object as a slice of integers.
}

// toIntegerSlicerWithError is the interface for converting an object to a slice of integer elements, with an error value.
type toIntegerSlicerWithError interface {
	ToIntegerSlice() ([]*integer.Element, error) // ToIntegerSlice returns the object as a slice of integers.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of lattices and controlling mutex
	defaultLatticeMutex = new(sync.RWMutex)
	defaultLattice = make(map[monomialorder.Type]map[int]*Parent)
}

// objectsToElements returns the arguments as an element in the given parent, if that's where they belong.
func objectsToElements(M *Parent, x1 object.Element, x2 object.Element) (*Element, *Element, error) {
	y1, err1 := ToElement(M, x1)
	if err1 != nil {
		return nil, nil, errors.Arg1NotContainedInParent.New()
	}
	y2, err2 := ToElement(M, x2)
	if err2 != nil {
		return nil, nil, errors.Arg2NotContainedInParent.New()
	}
	return y1, y2, nil
}

// sumElementsForIndex sums the i-th entries (indexed from 0) of the vectors in S. The result is returned down the resc channel. If the given slice of object.Elements do not all lie in M then the nil value with index -1 will be passed down the results channel.
func sumElementsForIndex(M *Parent, S []object.Element, i int, resc chan<- *verticalSum) {
	xs := make([]*integer.Element, 0, len(S))
	for _, v := range S {
		if vv, err := ToElement(M, v); err != nil {
			resc <- &verticalSum{i: -1, value: nil}
			return
		} else if !vv.IsZero() {
			xs = append(xs, vv.value[i])
		}
	}
	resc <- &verticalSum{i: i, value: integer.Sum(xs...)}
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Dimension returns the dimension of M.
func (M *Parent) Dimension() int {
	if M == nil {
		return 0
	}
	return M.n
}

// Rank returns the dimension of M.
func (M *Parent) Rank() int {
	return M.Dimension()
}

// Basis returns the standard basis of M.
func (M *Parent) Basis() []object.Element {
	if M == nil {
		return []object.Element{}
	}
	basis := make([]object.Element, 0, M.Dimension())
	for _, x := range Basis(M) {
		basis = append(basis, x)
	}
	return basis
}

// NthBasisElement returns the n-th standard basis element of M. Here the basis elements are indexed from 0 to d-1 (inclusive), where d is the dimension of M.
func (M *Parent) NthBasisElement(n int) (object.Element, error) {
	return NthBasisElement(M, n)
}

// Zero returns the n-dimensional zero vector in M.
func (M *Parent) Zero() object.Element {
	return Zero(M)
}

// IsZero returns true iff x is the n-dimensional zero vector in M.
func (M *Parent) IsZero(x object.Element) (bool, error) {
	xx, err := ToElement(M, x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// Add returns the sum x + y of the n-dimensional vectors x and y in M.
func (M *Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(M, x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy)
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element of M.
func (M *Parent) Sum(S ...object.Element) (object.Element, error) {
	// Handle the empty case
	if len(S) == 0 {
		return Zero(M), nil
	}
	// Do the length 1 and 2 cases
	if len(S) == 1 {
		return ToElement(M, S[0])
	} else if len(S) == 2 {
		return M.Add(S[0], S[1])
	}
	// Calculate the sum
	n := M.Dimension()
	// treat the dimension-zero case separately
	if n == 0 {
		// we check that all elements of S are actually in M
		for _, v := range S {
			if _, err := ToElement(M, v); err != nil {
				return nil, err
			}
		}
		return Zero(M), nil
	}
	// otherwise there are actually entries, so sum each entry separately
	resc := make(chan *verticalSum)
	for i := 0; i < n; i++ {
		go sumElementsForIndex(M, S, i, resc)
	}
	// Collect the results, checking for errors
	ok := true
	value := make([]*integer.Element, n)
	for i := 0; i < n; i++ {
		res := <-resc
		if res.i >= 0 {
			value[res.i] = res.value
		} else {
			ok = false
		}
	}
	// Handle any errors and return
	if !ok {
		return nil, errors.ArgNotContainedInParent.New()
	}
	return fromIntegerSlice(M, value)
}

// Subtract returns the difference x - y of the n-dimensional vectors x and y in M.
func (M *Parent) Subtract(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(M, x, y)
	if err != nil {
		return nil, err
	}
	return Subtract(xx, yy)
}

// Negate returns the negation -x of the n-dimensional vector x in M.
func (M *Parent) Negate(x object.Element) (object.Element, error) {
	xx, err := ToElement(M, x)
	if err != nil {
		return nil, err
	}
	return xx.Negate(), nil
}

// ScalarMultiplyByInteger returns nx, where this is defined to be x + ... + x (n times) if n is positive, and -x - ... - x (|n| times) if n is negative.
func (M *Parent) ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(M, x)
	if err != nil {
		return nil, errors.Arg2NotContainedInParent.New()
	}
	return xx.ScalarMultiplyByInteger(n), nil
}

// Contains returns true iff x is an n-dimension vector in M. Note that we do not allow a natural isomorphism between two different parent ZZ-modules, hence it is not sufficient that the dimensions agree.
func (M *Parent) Contains(x object.Element) bool {
	_, err := ToElement(M, x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (M *Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(M, x)
}

// AreEqual returns true iff x and y are both contained in M, and x = y.
func (M *Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := objectsToElements(M, x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func (M *Parent) Cmp(x object.Element, y object.Element) (int, error) {
	xx, yy, err := objectsToElements(M, x, y)
	if err != nil {
		return 0, err
	}
	return Cmp(xx, yy)
}

// MonomialOrder returns the monomial order being used to order the elements of M.
func (M *Parent) MonomialOrder() monomialorder.Type {
	if M == nil {
		return monomialorder.Lex
	}
	return M.order
}

// ProjectionMap returns the projection M -> ZZ onto the (i+1)-th factor of M. That is, projection maps are indexed from 0.
func (M *Parent) ProjectionMap(i int) (objectmap.Interface, error) {
	if i < 0 || i >= M.Rank() {
		return nil, errors.InvalidIndexRange.New(i, M.Rank()-1)
	}
	return objectmap.New(M, integer.Ring(), func(x object.Element) (object.Element, error) {
		xx, err := ToElement(M, x)
		if err != nil {
			return nil, err
		}
		return xx.EntryOrPanic(i), nil
	}), nil
}

// InclusionMap returns the inclusion ZZ -> M into the (i+1)-th factor of the of M. This is, the inclusion maps are indexed from 0.
func (M *Parent) InclusionMap(i int) (objectmap.Interface, error) {
	rk := M.Rank()
	if i < 0 || i >= rk {
		return nil, errors.InvalidIndexRange.New(i, M.Rank()-1)
	}
	return objectmap.New(integer.Ring(), M, func(x object.Element) (object.Element, error) {
		xx, err := integer.ToElement(x)
		if err != nil {
			return nil, err
		}
		S := integer.ZeroSlice(rk)
		S[i] = xx
		return fromIntegerSlice(M, S)
	}), nil
}

// AssignName assigns a name to M to be used by String.
func (M *Parent) AssignName(name string) {
	if M != nil {
		M.name = name
	}
}

// String returns a string representation of M.
func (M *Parent) String() string {
	if M != nil && len(M.name) != 0 {
		return M.name
	}
	return integer.Ring().String() + "^" + strconv.Itoa(M.Dimension())
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DefaultLattice returns the default space of n-dimensional integer-valued vectors with lex ordering. This is provided for use when the particular space the vector lives in is either currently undetermined, or doesn't matter. Calls to DefaultLattice (with the same value of n) always return the same n-dimensional space. This will return an error if n is negative.
func DefaultLattice(n int) (*Parent, error) {
	return DefaultLatticeWithOrder(n, monomialorder.Lex)
}

// DefaultLatticeWithOrder returns the default space of n-dimensional integer-valued vectors with given ordering. This is provided for use when the particular space the vector lives in is either currently undetermined, or doesn't matter. Calls to DefaultLattice (with the same value of n) always return the same n-dimensional space. This will return an error if n is negative.
func DefaultLatticeWithOrder(n int, c monomialorder.Type) (*Parent, error) {
	// Sanity check
	if n < 0 {
		return nil, errors.DimensionMustBeNonNegative.New()
	} else if !c.IsValid() {
		return nil, errors.UnknownMonomialOrder.New(c)
	}
	// Aquire a read lock on the defaultLatticeMutex and check the cache
	defaultLatticeMutex.RLock()
	if Ms, ok := defaultLattice[c]; ok {
		if M, ok := Ms[n]; ok {
			// The lattice is in the cache so return it
			defaultLatticeMutex.RUnlock()
			return M, nil
		}
	}
	// No luck -- release the read lock and create the new lattice
	defaultLatticeMutex.RUnlock()
	M, err := NewLatticeWithOrder(n, c)
	if err != nil {
		return nil, err
	}
	// Acquire a write lock and check the cache once again before returning
	defaultLatticeMutex.Lock()
	Ms, ok := defaultLattice[c]
	if !ok {
		Ms = make(map[int]*Parent)
		defaultLattice[c] = Ms
	}
	if N, ok := Ms[n]; ok {
		M = N
	} else {
		Ms[n] = M
	}
	// Release the write lock and return the lattice
	defaultLatticeMutex.Unlock()
	return M, nil
}

// NewLattice returns a new space of n-dimensional integer-valued vectors with lex ordering. That is, it returns a copy of ZZ^n. Note that two different copies of ZZ^n will be regarded as being distinct. This will return an error if n is negative.
func NewLattice(n int) (*Parent, error) {
	return NewLatticeWithOrder(n, monomialorder.Lex)
}

// NewLatticeWithOrder returns a new space of n-dimensional integer-valued vectors with given ordering. That is, it returns a copy of ZZ^n. Note that two different copies of ZZ^n will be regarded as being distinct. This will return an error if n is negative.
func NewLatticeWithOrder(n int, c monomialorder.Type) (M *Parent, err error) {
	// Sanity check
	if n < 0 {
		err = errors.DimensionMustBeNonNegative.New()
	} else if !c.IsValid() {
		err = errors.UnknownMonomialOrder.New(c)
	} else {
		// Create the lattice
		M = &Parent{n: n, order: c}
		M.zero = createZero(M)
	}
	return
}

// ToElement returns the argument as an n-dimensional integer-valued vector in the given parent, if that's where it belongs. If necessary, if M is 1-dimensional then an attempt will be made to convert x to an integer, which will then be returned as an element in k.
func ToElement(M *Parent, x object.Element) (*Element, error) {
	if y, ok := x.(*Element); ok {
		if parentOfElement(y) == M {
			return y, nil
		}
	} else if M.Dimension() == 1 {
		// This is a 1-dimensional parent, so we'll try converting x to an
		// integer
		if c, err := integer.ToElement(x); err == nil {
			return fromIntegerSlice(M, append(make([]*integer.Element, 0, 1), c))
		}
	}
	return nil, errors.ArgNotContainedInParent.New()
}

// ChangeParent returns a copy of x with the parent set to the space M of n-dimensional integer-valued vectors. If x has a ToIntegerSlice method, this will be called to allow conversion. If x cannot be cast into M -- either because the dimensions or length of the slices disagree, or because there is no known way of automatically performing the conversion -- then an error will be returned.
func ChangeParent(M *Parent, x object.Element) (*Element, error) {
	n := M.Dimension()
	if y, ok := x.(*Element); ok {
		// If this is a *Element then this is easy
		if parentOfElement(y) == M {
			return y, nil
		} else if y.Dimension() == n {
			if y.IsZero() {
				return Zero(M), nil
			}
			return fromIntegerSlice(M, y.value)
		}
		return nil, errors.SliceLengthNotEqualDimension.New(y.Dimension(), n)
	} else if yy, ok := x.(toIntegerSlicerWithError); ok {
		// If this has a ToIntegerSlice method with an error return value, try
		// that.
		S, err := yy.ToIntegerSlice()
		if err != nil {
			return nil, err
		}
		return fromIntegerSlice(M, S)
	} else if yy, ok := x.(toIntegerSlicer); ok {
		// If this has a ToIntegerSlice method without an error return value,
		// try that.
		return fromIntegerSlice(M, yy.ToIntegerSlice())
	} else if n == 1 {
		// This is a 1-dimensional parent, so we'll try converting x to an
		// integer
		if c, err := integer.ToElement(x); err == nil {
			return fromIntegerSlice(M, []*integer.Element{c})
		}
	}
	// No use -- we don't know how to automatically convert the object
	return nil, errors.ArgNotAnIntegralVector.New()
}
