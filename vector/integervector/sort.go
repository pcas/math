// Sort allows sorting of slices of integer-valued vectors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"sort"
)

// SortableSlice implements the sort.Interface interface.
type SortableSlice []*Element

// The maximum length of the slice before using the standard sort package.
const maxSortLen = 3000

// The channel buffer size.
const sortBufSize = maxSortLen / 10

/////////////////////////////////////////////////////////////////////////
// SortableSlice functions
/////////////////////////////////////////////////////////////////////////

// Len is the number of elements in the slice.
func (S SortableSlice) Len() int {
	return len(S)
}

// Less reports whether the element with index i should sort before the element with index j.
func (S SortableSlice) Less(i int, j int) bool {
	return S[i].IsLessThan(S[j])
}

// Swap swaps the elements with indexes i and j.
func (S SortableSlice) Swap(i int, j int) {
	S[i], S[j] = S[j], S[i]
}

// Sort sorts the slice in place.
func (S SortableSlice) Sort() {
	sort.Sort(S)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// pipeResults pipes the contents of the src channel down the dst channel. The
// dst channel will NOT be closed upon return.
func pipeResults(src <-chan *Element, dst chan<- *Element) {
	for x := range src {
		dst <- x
	}
}

// sortSlice sorts the slice S. The sorted elements are returned down the resc channel, which is closed on finish.
func sortSlice(S []*Element, resc chan<- *Element) {
	// If the slice is small enough we use the standard sort package
	if len(S) <= maxSortLen {
		sortedS := make([]*Element, len(S))
		copy(sortedS, S)
		SortableSlice(sortedS).Sort()
		for _, x := range sortedS {
			resc <- x
		}
		close(resc)
		return
	}
	// Create the communication channels
	rescLeft := make(chan *Element, sortBufSize)
	rescRight := make(chan *Element, sortBufSize)
	// Partition the task
	div := len(S) / 2
	go sortSlice(S[:div], rescLeft)
	go sortSlice(S[div:], rescRight)
	// Start merging the results
	xL, okLeft := <-rescLeft
	xR, okRight := <-rescRight
	for okLeft && okRight {
		if xL.IsLessThan(xR) {
			resc <- xL
			xL, okLeft = <-rescLeft
		} else {
			resc <- xR
			xR, okRight = <-rescRight
		}
	}
	// Forward any remaining results down the results channel
	if okLeft {
		resc <- xL
		pipeResults(rescLeft, resc)
	} else if okRight {
		resc <- xR
		pipeResults(rescRight, resc)
	}
	close(resc)
}

/////////////////////////////////////////////////////////////////////////
// Sort functions
/////////////////////////////////////////////////////////////////////////

// Sort non-destructively sorts the given slice of integer-valued vectors. The sorted slice is returned. All vectors must have the same parent, otherwise an error will be returned.
func Sort(S []*Element) ([]*Element, error) {
	// Do the easy cases
	if len(S) == 0 {
		return S, nil
	} else if len(S) == 1 {
		return []*Element{S[0]}, nil
	}
	// Check that the parents are equal
	var M *Parent
	if S[0] != nil {
		M = parentOfElement(S[0])
	}
	for _, v := range S {
		if parentOfElement(v) != M {
			return nil, errors.ParentsDoNotAgree.New()
		}
	}
	// Handle the case where S is of length 2
	if len(S) == 2 {
		if S[0].IsGreaterThan(S[1]) {
			return []*Element{S[1], S[0]}, nil
		}
		return []*Element{S[0], S[1]}, nil
	}
	// If the slice is short enough we use the sort package...
	if len(S) <= maxSortLen {
		sortedS := make([]*Element, len(S), cap(S))
		copy(sortedS, S)
		SortableSlice(sortedS).Sort()
		return sortedS, nil
	}
	// ...otherwise we split the slice in two and recurse. First create the
	// communication channels
	resc := make(chan *Element, sortBufSize)
	// Set a background go routine sorting
	go sortSlice(S, resc)
	// Extract the results into a slice
	sortedS := make([]*Element, 0, cap(S))
	for x := range resc {
		sortedS = append(sortedS, x)
	}
	return sortedS, nil
}

// Sort non-destructively sorts the given slice of integer-valued vectors. The sorted slice is returned.
func (M *Parent) Sort(S []object.Element) ([]object.Element, error) {
	// Get the easy cases out of the way
	if len(S) == 0 {
		return S, nil
	} else if len(S) == 1 {
		xx, err := ToElement(M, S[0])
		if err != nil {
			return nil, err
		}
		return []object.Element{xx}, nil
	}
	// Convert the slice type
	sS := make([]*Element, 0, len(S))
	for _, x := range S {
		xx, err := ToElement(M, x)
		if err != nil {
			return nil, err
		}
		sS = append(sS, xx)
	}
	// If the slice is short enough we use the sort package...
	if len(sS) <= maxSortLen {
		SortableSlice(sS).Sort()
		sortedS := make([]object.Element, 0, cap(S))
		for _, x := range sS {
			sortedS = append(sortedS, x)
		}
		return sortedS, nil
	}
	// ...otherwise we split the slice in two and recurse. First create the
	// communication channels
	resc := make(chan *Element, sortBufSize)
	// Set a background go routine sorting
	go sortSlice(sS, resc)
	// Extract the results into a slice and return
	sortedS := make([]object.Element, 0, cap(S))
	for x := range resc {
		sortedS = append(sortedS, x)
	}
	return sortedS, nil
}
