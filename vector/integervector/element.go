// Element defines an n-dimensional integer-valued vector in the ZZ-module ZZ^n.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
)

// Element describes an integer-valued vector.
type Element struct {
	parent *Parent            // The parent of this element
	value  []*integer.Element // The underlying slice of integers
}

// verticalSum gives the result of summing the i-th entry (indexed from 0) over all vectors.
type verticalSum struct {
	i     int              // The entry (indexed from 0)
	value *integer.Element // The sum
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createZero creates a new zero element for the given parent M. It does not assign zero to the parent.
func createZero(M *Parent) *Element {
	return &Element{parent: M, value: integer.ZeroSlice(M.Dimension())}
}

// parentOfElement returns the parent for x.
func parentOfElement(x *Element) *Parent {
	if x == nil {
		return nil
	}
	return x.parent
}

// doParentsAgree returns true iff x and y have the same parents.
func doParentsAgree(x *Element, y *Element) bool {
	return parentOfElement(x) == parentOfElement(y)
}

// isZeroSlice returns true iff all the entries in the given slice are zero.
func isZeroSlice(S []*integer.Element) bool {
	for _, v := range S {
		if !v.IsZero() {
			return false
		}
	}
	return true
}

// fromIntegerSlice returns a vector element with the given parent set to the value S. The slice S is NOT copied, and so must NOT be modified after calling this function. If the length of the slice does not agree with the dimension of the parent then the function will return an error.
func fromIntegerSlice(M *Parent, S []*integer.Element) (*Element, error) {
	// Sanity check on the dimension/length
	if M.Dimension() != len(S) {
		return nil, errors.SliceLengthNotEqualDimension.New(len(S), M.Dimension())
	}
	// Handle the zero vector
	if isZeroSlice(S) {
		if M == nil {
			return nil, nil
		}
		return M.zero, nil
	}
	// Create a new element using the slice
	return &Element{parent: M, value: S}, nil
}

/////////////////////////////////////////////////////////////////////////
// Sum functions
/////////////////////////////////////////////////////////////////////////

// sumForIndex sums the i-th entries (indexed from 0) of the vectors in S. The result is returned down the resc channel.
func sumForIndex(S []*Element, i int, resc chan<- *verticalSum) {
	xs := make([]*integer.Element, 0, len(S))
	for _, v := range S {
		if !v.IsZero() {
			xs = append(xs, v.value[i])
		}
	}
	resc <- &verticalSum{i: i, value: integer.Sum(xs...)}
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element of M. This will return an error if the elements of S do not all have M as their parent.
func Sum(M *Parent, S ...*Element) (*Element, error) {
	// Handle the empty case
	if len(S) == 0 {
		return Zero(M), nil
	}
	// Sanity check on the parents
	for _, v := range S {
		if parentOfElement(v) != M {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	// Do the length 1 and 2 cases
	if len(S) == 1 {
		return S[0], nil
	} else if len(S) == 2 {
		return Add(S[0], S[1])
	}
	// Calculate the sum
	resc := make(chan *verticalSum)
	n := M.Dimension()
	for i := 0; i < n; i++ {
		go sumForIndex(S, i, resc)
	}
	// Collect the results and return
	value := make([]*integer.Element, n)
	for i := 0; i < n; i++ {
		res := <-resc
		value[res.i] = res.value
	}
	return fromIntegerSlice(M, value)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromIntegerSlice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromIntegerSlice(M *Parent, S []*integer.Element) (*Element, error) {
	return fromIntegerSlice(M, integer.CopySlice(S))
}

// FromIntSlice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromIntSlice(M *Parent, S []int) (*Element, error) {
	return fromIntegerSlice(M, integer.SliceFromIntSlice(S))
}

// FromInt32Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromInt32Slice(M *Parent, S []int32) (*Element, error) {
	return fromIntegerSlice(M, integer.SliceFromInt32Slice(S))
}

// FromInt64Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromInt64Slice(M *Parent, S []int64) (*Element, error) {
	return fromIntegerSlice(M, integer.SliceFromInt64Slice(S))
}

// FromUint64Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromUint64Slice(M *Parent, S []uint64) (*Element, error) {
	return fromIntegerSlice(M, integer.SliceFromUint64Slice(S))
}

// FromString returns the n-dimensional vector in M represented by the string s.
func FromString(M *Parent, s string) (*Element, error) {
	// Begin by converting the string into a slice of integers
	value, err := integer.SliceFromString(s)
	if err != nil {
		return nil, err
	}
	// Return the slice as a vector
	return fromIntegerSlice(M, value)
}

// ConstantVector returns an n-dimensional vector in M all of whose entries are c.
func ConstantVector(M *Parent, c *integer.Element) *Element {
	// Handle the nil case and the case when c is zero
	n := M.Dimension()
	if n == 0 || c.IsZero() {
		return Zero(M)
	}
	// Create the slice
	value := make([]*integer.Element, 0, n)
	for i := 0; i < n; i++ {
		value = append(value, c)
	}
	// Return the vector
	v, err := fromIntegerSlice(M, value)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return v
}

// Basis returns the n-dimensional standard basis of M.
func Basis(M *Parent) []*Element {
	// Note the dimension
	n := M.Dimension()
	// Create a slice of zeros of length 2n - 1, and set the (n-1)-th entry to 1
	value := integer.ZeroSlice(2*n - 1)
	value[n-1] = integer.One()
	// Create the basis elements
	basis := make([]*Element, 0, n)
	for i := 0; i < n; i++ {
		v, err := fromIntegerSlice(M, value[n-i-1:2*n-i-1])
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		basis = append(basis, v)
	}
	return basis
}

// NthBasisElement returns the n-th standard basis element of M. Here the basis elements are indexed from 0 to d-1 (inclusive), where d is the dimension of M.
func NthBasisElement(M *Parent, n int) (*Element, error) {
	// Sanity check
	rk := M.Dimension()
	if n < 0 || n >= rk {
		return nil, errors.InvalidIndexRange.New(n, rk-1)
	}
	// Create the slice describing the n-th basis element
	value := integer.ZeroSlice(rk)
	value[n] = integer.One()
	// Return the vector
	return fromIntegerSlice(M, value)
}

// Zero returns the n-dimensional zero vector in M.
func Zero(M *Parent) *Element {
	if M == nil {
		return createZero(M)
	}
	return M.zero
}

// AreEqual returns true iff x and y have the same parent, and x = y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y. This will return an error if x and y do not have the same parent.
func Cmp(x *Element, y *Element) (int, error) {
	// Check that the parents agree and get the easy case out of the way
	if !doParentsAgree(x, y) {
		return 0, errors.ParentsDoNotAgree.New()
	} else if x == y || x == nil || y == nil {
		return 0, nil
	}
	// Perform the comparison
	return parentOfElement(x).MonomialOrder().CmpInteger(x.value, y.value)
}

// DotProduct returns the dot-product of the n-dimensional integer vectors x and y. Here there is no condition on the parents of x and y. This will return an error if the dimensions differ.
func DotProduct(x *Element, y *Element) (*integer.Element, error) {
	// Sanity check on the dimension
	if x.Dimension() != y.Dimension() {
		return nil, errors.DimensionsDoNotAgree.New()
	}
	// Get the zero cases out of the way
	if x.IsZero() || y.IsZero() {
		return integer.Zero(), nil
	}
	// Return the dot product
	return integer.DotProduct(x.value, y.value), nil
}

// Negate returns -x.
func Negate(x *Element) *Element {
	return x.Negate()
}

// Add returns the vector sum x + y. This will return an error if x and y do not have the same parent.
func Add(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the case where one of the arguments is known to be zero
	if x.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x, nil
	}
	// Calculate the sum
	value := make([]*integer.Element, 0, x.Dimension())
	for i, v := range x.value {
		value = append(value, integer.Add(v, y.value[i]))
	}
	return fromIntegerSlice(parentOfElement(x), value)
}

// Subtract returns the vector different x - y. This will return an error if x and y do not have the same parent.
func Subtract(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the case where one of the arguments is known to be zero
	if x.IsZero() {
		return y.Negate(), nil
	} else if y.IsZero() {
		return x, nil
	} else if x == y {
		return Zero(parentOfElement(x)), nil
	}
	// Calculate the difference
	value := make([]*integer.Element, 0, x.Dimension())
	for i, v := range x.value {
		value = append(value, integer.Subtract(v, y.value[i]))
	}
	return fromIntegerSlice(parentOfElement(x), value)
}

// ScalarMultiplyByIntegerThenAdd returns a * x + y, where a is an integer, and x and y are vectors. This will return an error if x and y do not have the same parent.
func ScalarMultiplyByIntegerThenAdd(a *integer.Element, x *Element, y *Element) (*Element, error) {
	// Check that the parents agree and get the easy cases out of the way
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if x.IsZero() || a.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x.ScalarMultiplyByInteger(a), nil
	}
	// Do the calculation
	value := make([]*integer.Element, 0, x.Dimension())
	for i, xi := range x.value {
		value = append(value, integer.MultiplyThenAdd(a, xi, y.value[i]))
	}
	return fromIntegerSlice(parentOfElement(x), value)
}

// ScalarMultiplyByIntegerThenSubtract returns a * x - y, where a is an integer, and x and y are vectors. This will return an error if x and y do not have the same parent.
func ScalarMultiplyByIntegerThenSubtract(a *integer.Element, x *Element, y *Element) (*Element, error) {
	// Check that the parents agree and get the easy cases out of the way
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if x.IsZero() || a.IsZero() {
		return y.Negate(), nil
	} else if y.IsZero() {
		return x.ScalarMultiplyByInteger(a), nil
	}
	// Do the calculation
	value := make([]*integer.Element, 0, x.Dimension())
	for i, xi := range x.value {
		value = append(value, integer.MultiplyThenSubtract(a, xi, y.value[i]))
	}
	return fromIntegerSlice(parentOfElement(x), value)
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Dimension returns the dimension of the parent of the vector x.
func (x *Element) Dimension() int {
	if x == nil {
		return 0
	}
	return len(x.value)
}

// EntryOrPanic returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will panic if i is out of range.
func (x *Element) EntryOrPanic(i int) *integer.Element {
	return x.value[i]
}

// Entry returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range.
func (x *Element) Entry(i int) (*integer.Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	}
	return x.value[i], nil
}

// IncrementEntryOrPanic returns a new n-dimensional vector equal to x, but with the i-th entry incremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will panic if i is out of range.
func (x *Element) IncrementEntryOrPanic(i int) *Element {
	value := integer.CopySlice(x.value)
	value[i] = value[i].Increment()
	y, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return y
}

// IncrementEntry returns a new n-dimensional vector equal to x, but with the i-th entry incremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range.
func (x *Element) IncrementEntry(i int) (*Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	}
	value := integer.CopySlice(x.value)
	value[i] = value[i].Increment()
	y, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		return nil, err
	}
	return y, nil
}

// DecrementEntryOrPanic returns a new n-dimensional vector equal to x, but with the i-th entry decremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will panic if i is out of range.
func (x *Element) DecrementEntryOrPanic(i int) *Element {
	value := integer.CopySlice(x.value)
	value[i] = value[i].Decrement()
	y, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return y
}

// DecrementEntry returns a new n-dimensional vector equal to x, but with the i-th entry decremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range.
func (x *Element) DecrementEntry(i int) (*Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	}
	value := integer.CopySlice(x.value)
	value[i] = value[i].Decrement()
	y, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		return nil, err
	}
	return y, nil
}

// ToIntegerSlice returns the vector as a slice of integers. Note that you can modify the returned slice without affecting the vector.
func (x *Element) ToIntegerSlice() []*integer.Element {
	if x == nil {
		return make([]*integer.Element, 0)
	}
	return integer.CopySlice(x.value)
}

// ToInt64Slice returns the vector as a slice of int64s. It returns an error if any of the entries is too large to fit in an int64.
func (x *Element) ToInt64Slice() ([]int64, error) {
	if x == nil {
		return make([]int64, 0), nil
	}
	return integer.SliceToInt64Slice(x.value)
}

// IsZero returns true iff x is the zero vector (0,0,...,0).
func (x *Element) IsZero() bool {
	if M := parentOfElement(x); M == nil || M.zero == x {
		return true
	}
	return isZeroSlice(x.value)
}

// Sign returns -1 if x < 0, 0 if x == 0, and +1 if x > 0.
func (x *Element) Sign() int {
	if x.IsZero() {
		return 0
	}
	sgn, err := Cmp(x, Zero(parentOfElement(x)))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return sgn
}

// IsNegative returns true iff x < 0.
func (x *Element) IsNegative() bool {
	return x.Sign() < 0
}

// IsPositive returns true iff x > 0.
func (x *Element) IsPositive() bool {
	return x.Sign() > 0
}

// IsEqualTo returns true iff x and y have the same parent, and x = y.
func (x *Element) IsEqualTo(y *Element) bool {
	if !doParentsAgree(x, y) {
		return false
	}
	if x != y && x != nil {
		for i, v := range x.value {
			if !v.IsEqualTo(y.value[i]) {
				return false
			}
		}
	}
	return true
}

// IsGreaterThan returns true iff x and y have the same parent, and x > y.
func (x *Element) IsGreaterThan(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val > 0
}

// IsGreaterThanOrEqualTo returns true iff x and y have the same parent, and x >= y.
func (x *Element) IsGreaterThanOrEqualTo(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val >= 0
}

// IsLessThan returns true iff x and y have the same parent, and x < y.
func (x *Element) IsLessThan(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val < 0
}

// IsLessThanOrEqualTo returns true iff x and y have the same parent, and x <= y.
func (x *Element) IsLessThanOrEqualTo(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val <= 0
}

// Negate returns the negation -x.
func (x *Element) Negate() *Element {
	if x.IsZero() {
		return x
	}
	value := make([]*integer.Element, 0, x.Dimension())
	for _, v := range x.value {
		value = append(value, v.Negate())
	}
	y, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return y
}

// ScalarMultiplyByInt64 returns the scalar product a * x, where a is an integer.
func (x *Element) ScalarMultiplyByInt64(a int64) *Element {
	if x.IsZero() || a == 1 {
		return x
	} else if a == 0 {
		return Zero(parentOfElement(x))
	} else if a == -1 {
		return x.Negate()
	}
	return x.ScalarMultiplyByInteger(integer.FromInt64(a))
}

// ScalarMultiplyByUint64 returns the scalar product a * x, where a is an integer.
func (x *Element) ScalarMultiplyByUint64(a uint64) *Element {
	if x.IsZero() || a == 1 {
		return x
	} else if a == 0 {
		return Zero(parentOfElement(x))
	}
	return x.ScalarMultiplyByInteger(integer.FromUint64(a))
}

// ScalarMultiplyByInteger returns the scalar product a * x, where a is an integer.
func (x *Element) ScalarMultiplyByInteger(a *integer.Element) *Element {
	if x.IsZero() {
		return x
	} else if a.IsZero() {
		return Zero(parentOfElement(x))
	} else if a.IsOne() {
		return x
	}
	value := make([]*integer.Element, 0, x.Dimension())
	for _, v := range x.value {
		value = append(value, integer.Multiply(a, v))
	}
	v, err := fromIntegerSlice(parentOfElement(x), value)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return v
}

// TotalDegree returns the total degree of the vector x. That is, it returns the sum of the entries in x.
func (x *Element) TotalDegree() *integer.Element {
	if x.IsZero() {
		return integer.Zero()
	}
	return integer.Sum(x.value...)
}

// Hash returns a hash value for this vector.
func (x *Element) Hash() hash.Value {
	n := x.Dimension()
	if n == 0 {
		return 0
	}
	hashes := make([]hash.Value, 0, n)
	for _, v := range x.value {
		hashes = append(hashes, v.Hash())
	}
	return hash.Slice(hashes)
}

// Parent returns the parent of this vector.
func (x *Element) Parent() object.Parent {
	return parentOfElement(x)
}

// String returns a string representation of the vector.
func (x *Element) String() string {
	if x.Dimension() == 0 {
		return "()"
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('(')
	for i, v := range x.value {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteString(v.String())
	}
	b.WriteByte(')')
	return b.String()
}
