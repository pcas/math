// Tests encoding.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/testing/assert"
	"bytes"
	"encoding/gob"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobEncode tests gob encoding/decoding.
func TestGobEncode(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ2other, err := NewLattice(2)
	assert.NoError(err)
	ZZ2Grevlex, err := DefaultLatticeWithOrder(2, monomialorder.Grevlex)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	vm11, err := FromIntSlice(ZZ2, []int{-1, 1})
	assert.NoError(err)
	v25, err := FromIntSlice(ZZ2, []int{2, 5})
	assert.NoError(err)
	v25other, err := ChangeParent(ZZ2other, v25)
	assert.NoError(err)
	v25Grevlex, err := ChangeParent(ZZ2Grevlex, v25)
	assert.NoError(err)
	nilZero := Zero(nilLattice)
	// Create the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if assert.NoError(enc.Encode(vm11)) {
		// check the decoded vector is the same as the original
		if v, err := GobDecode(dec); assert.NoError(err) {
			assert.True(vm11.IsEqualTo(v))
		}
	}
	// the result should always end up in the default lattice of that dimension with the correct monomial order
	if assert.NoError(enc.Encode(v25other)) {
		if v, err := GobDecode(dec); assert.NoError(err) {
			assert.False(v25other.IsEqualTo(v))
			assert.True(v25.IsEqualTo(v))
		}
	}
	if assert.NoError(enc.Encode(v25Grevlex)) {
		if v, err := GobDecode(dec); assert.NoError(err) {
			assert.True(v25Grevlex.IsEqualTo(v))
			assert.False(v25.IsEqualTo(v))
		}
	}
	// elements of the nil lattice should encode and decode correctly
	if assert.NoError(enc.Encode(nilZero)) {
		if v, err := GobDecode(dec); assert.NoError(err) {
			assert.True(v.Dimension() == 0)
			assert.True(v.IsZero())
			// note that this does *not* come back as an element of nilLattice
			assert.False(v.Parent() == nilLattice)
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var nilElement *Element
	if b, err := nilElement.GobEncode(); assert.NoError(err) {
		// Decoding should give the zero element of the default zero-dimensional lattice
		m := &Element{}
		if assert.NoError(m.GobDecode(b)) {
			assert.True(m.IsZero())
			assert.True(m.Dimension() == 0)
			L, err := DefaultLattice(0)
			assert.NoError(err)
			assert.True(m.Parent() == L)
		}
	}
}

// TestGobDecodeEmpty tests gob decoding an empty slice of bytes
func TestGobDecodeEmpty(t *testing.T) {
	assert := assert.New(t)
	// This should give the zero element of the default zero-dimensional lattice
	m := &Element{}
	if assert.NoError(m.GobDecode(nil)) {
		assert.True(m.IsZero())
		assert.True(m.Dimension() == 0)
		L, err := DefaultLattice(0)
		assert.NoError(err)
		assert.True(m.Parent() == L)
	}
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	assert := assert.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// a lattice and a vector
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	v := Zero(ZZ2)
	if assert.NoError(enc.Encode(v)) {
		// Try to decode directly into a nil object -- this should error
		var m *Element
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	assert := assert.New(t)
	// Create the byte slice: the first byte is the version number, which we
	// set to "future" version.
	var b bytes.Buffer
	var illegalEncodingVersion byte = encodingVersion + 1
	enc := gob.NewEncoder(&b)
	err := enc.Encode(illegalEncodingVersion)
	assert.NoError(err)
	// Try to decode as a *Element -- this should error
	m := &Element{}
	assert.Error(m.GobDecode(b.Bytes()))
}

// TestGobDecodeExisting tests gob decoding into an existing *Element.
func TestGobDecodeExisting(t *testing.T) {
	assert := assert.New(t)
	// a lattice and two elements
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	v := Zero(ZZ2)
	w, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode the integer
	if assert.NoError(enc.Encode(v)) {
		// Try to decode directly into m -- this should error
		assert.Error(w.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	assert := assert.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
	// Try to decode as a []*Element -- this should error
	_, err = GobDecodeSlice(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
}

// TestGobSliceEncoding tests gob encoding/decoding of slices of vectors.
func TestGobSliceEncoding(t *testing.T) {
	assert := assert.New(t)
	// two lattices
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	// Create the slice of vectors
	T := [][]int{
		[]int{-5, 2},
		[]int{0, 1},
		[]int{0, 0},
		[]int{-1, 1},
	}
	S := make([]*Element, 0, len(T))
	for _, t := range T {
		if v, err := FromIntSlice(ZZ2, t); assert.NoError(err) {
			S = append(S, v)
		}
	}
	// stick a vector from a different lattice on the end
	if v, err := FromIntSlice(ZZ3, []int{5, 6, 7}); assert.NoError(err) {
		S = append(S, v)
	}
	// Create the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	// Encode the slice of vectors
	if assert.NoError(enc.Encode(S)) {
		// Decode the slice of vectors
		if SS, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Len(SS, len(S)) {
			// Check for equality
			for i, s := range S {
				assert.AreEqual(s.Parent(), s, SS[i])
			}
		}
	}
}
