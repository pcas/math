// Tests sort.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// reverseSlice reverses a slice of *Elements
func reverseSlice(S []*Element) []*Element {
	SS := make([]*Element, 0, len(S))
	for i := 0; i < len(S); i++ {
		SS = append(SS, S[len(S)-1-i])
	}
	return SS
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestSort tests Sort
func TestSort(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	lattices := []*Parent{ZZ2, nilLattice}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for _, L := range lattices {
		for _, n := range []int{0, 1, 2, 300, 5000} {
			for _, reverse := range []bool{true, false} {
				// we build and populate a slice of length n
				S := make([]*Element, 0, n)
				for i := 0; i < n; i++ {
					s := make([]int, L.Rank())
					for j := 0; j < len(s); j++ {
						s[j] = (j+1)*i*i*(-1) ^ i%127
					}
					if v, err := FromIntSlice(L, s); assert.NoError(err) {
						S = append(S, v)
					}
				}
				if reverse {
					S = reverseSlice(S)
				}
				T, err := Sort(S)
				assert.NoError(err)

				// the sorted list should be sorted
				for i := 0; i < n-1; i++ {
					assert.True(T[i].IsLessThanOrEqualTo(T[i+1]))
				}

				// same test again but for the parent's sort method
				SS := make([]object.Element, 0, len(S))
				for _, v := range S {
					SS = append(SS, v)
				}
				TT, err := L.Sort(SS)
				assert.NoError(err)
				// the sorted list should be sorted
				for i := 0; i < n-1; i++ {
					v, w := TT[i], TT[i+1]
					assert.True(v.(*Element).IsLessThanOrEqualTo(w.(*Element)))
				}

			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// cases with mismatched parents
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	wrongZero := Zero(ZZ3)
	for _, L := range lattices {
		for _, n := range []int{2, 8, 300, 5000} {
			// we build and populate a slice of length n
			S := make([]*Element, 0, n)
			for i := 0; i < n; i++ {
				s := make([]int, L.Rank())
				for j := 0; j < len(s); j++ {
					s[j] = (j + 1) * i % 127
				}
				if v, err := FromIntSlice(L, s); assert.NoError(err) {
					S = append(S, v)
				}
			}
			// make sure one of the elements has the wrong parent
			S[1] = wrongZero
			_, err := Sort(S)
			assert.Error(err)
			// same test again but for the parent sort method
			SS := make([]object.Element, len(S))
			for _, v := range S {
				SS = append(SS, v)
			}
			_, err = L.Sort(SS)
			assert.Error(err)
		}
	}
	// also we should check a length-1 slice in the wrong parent
	_, err = ZZ2.Sort([]object.Element{wrongZero})
	assert.Error(err)
}
