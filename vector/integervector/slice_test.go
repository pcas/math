// Tests slice.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestCopySlice tests CopySlice
func TestCopySlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some slices of vectors
	//////////////////////////////////////////////////////////////////////
	var nilSlice []*Element
	emptySlice := make([]*Element, 0)
	S := make([]*Element, 0, 10)
	for i := -2; i < 3; i++ {
		if v, err := FromIntSlice(ZZ2, []int{i, i + 1}); assert.NoError(err) {
			S = append(S, v)
		}
	}
	sliceOfZeroes := make([]*Element, 0, 10)
	for i := 0; i < 8; i++ {
		sliceOfZeroes = append(sliceOfZeroes, Zero(nilLattice))
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	T := CopySlice(S)
	for i, v := range T {
		assert.AreEqual(ZZ2, v, S[i])
	}
	assert.Equal(len(S), len(T))
	assert.Equal(cap(S), cap(T))
	// now again, with a slice of elements of the nil *Parent
	T = CopySlice(sliceOfZeroes)
	for i, v := range T {
		assert.AreEqual(nilLattice, v, sliceOfZeroes[i])
	}
	assert.Equal(len(sliceOfZeroes), len(T))
	assert.Equal(cap(sliceOfZeroes), cap(T))
	//////////////////////////////////////////////////////////////////////
	// nil and empty cases
	//////////////////////////////////////////////////////////////////////
	T = CopySlice(nilSlice)
	assert.Zero(len(T))
	assert.Zero(cap(T))
	T = CopySlice(emptySlice)
	assert.Zero(len(T))
	assert.Zero(cap(T))
}
