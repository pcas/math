// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integervector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcastools/hash"
	"math"
	"testing"
)

//////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////

// TestFromIntegerSlice tests FromIntegerSlice
func TestFromIntegerSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	integerSlice := make([]*integer.Element, 0, 3)
	for i := 0; i < 3; i++ {
		integerSlice = append(integerSlice, integer.FromInt(100+i))
	}
	if v, err := FromIntegerSlice(ZZ3, integerSlice); assert.NoError(err) {
		if vs, err := v.ToInt64Slice(); assert.NoError(err) {
			assert.Equal(vs, []int64{100, 101, 102})
		}
	}
	//////////////////////////////////////////////////////////////////////
	// this should fail, because the dimension doesn't match the
	// length of the slice
	//////////////////////////////////////////////////////////////////////
	_, err = FromIntegerSlice(ZZ2, integerSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test zero input
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		integerSlice[i] = integer.Zero()
	}
	zeroZeroZero, err := FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	assert.Equal(zeroZeroZero, Zero(ZZ3))
	//////////////////////////////////////////////////////////////////////
	// test the nil case
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []*integer.Element
	_, err = FromIntegerSlice(nilLattice, integerSlice)
	assert.Error(err)
	_, err = FromIntegerSlice(ZZ3, nilSlice)
	assert.Error(err)
	v, err := FromIntegerSlice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v)
	assert.IsZero(v)
}

// TestFromString tests FromString
func TestFromString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// a slice to test against
	//////////////////////////////////////////////////////////////////////
	integerSlice := make([]*integer.Element, 0, 3)
	for i := 100; i < 103; i++ {
		integerSlice = append(integerSlice, integer.FromInt(i))
	}
	v1, err := FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed and give v1
	//////////////////////////////////////////////////////////////////////
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should also succeed and give v1
	//////////////////////////////////////////////////////////////////////
	v3, err := FromString(ZZ3, "[100,\t         101,102\n\n\n             ]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v3)
	//////////////////////////////////////////////////////////////////////
	// this should fail for dimensional reasons
	//////////////////////////////////////////////////////////////////////
	_, err = FromString(ZZ2, "[100, 101, 102]")
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail, as the input is garbage
	//////////////////////////////////////////////////////////////////////
	_, err = FromString(ZZ2, "[100, 101, a little turnip of my own 102]")
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilString string
	_, err = FromString(nilLattice, "[100, 101, 102]")
	assert.Error(err)
	_, err = FromString(ZZ2, nilString)
	assert.Error(err)
	v4, err := FromString(nilLattice, nilString)
	assert.NoError(err)
	assert.Nil(v4)
	v5, err := FromString(nilLattice, "\t\t      \t      \n\n      \n         ")
	assert.NoError(err)
	assert.Nil(v5)
	_, err = FromString(ZZ3, "\t\t      \t      \n\n      \n         ")
	assert.Error(err)
}

// TestFromIntSlice tests FromIntSlice
func TestFromIntSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int, 0, 3)
	for i := 100; i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromIntSlice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromIntSlice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromIntSlice(ZZ2, make([]int, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int
	_, err = FromIntSlice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromIntSlice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromIntSlice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromIntSlice(nilLattice, make([]int, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromInt32Slice tests FromInt32Slice
func TestFromInt32Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int32, 0, 3)
	for i := int32(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromInt32Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt32Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromInt32Slice(ZZ2, make([]int32, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int32
	_, err = FromInt32Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromInt32Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromInt32Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromInt32Slice(nilLattice, make([]int32, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromInt64Slice tests FromInt64Slice
func TestFromInt64Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int64, 0, 3)
	for i := int64(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromInt64Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt64Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromInt64Slice(ZZ2, make([]int64, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int64
	_, err = FromInt64Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromInt64Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromInt64Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromInt64Slice(nilLattice, make([]int64, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromUint64Slice tests FromUint64Slice
func TestFromUint64Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]uint64, 0, 3)
	for i := uint64(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromUint64Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromUint64Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromUint64Slice(ZZ2, make([]uint64, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []uint64
	_, err = FromUint64Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromUint64Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromUint64Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromUint64Slice(nilLattice, make([]uint64, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestConstantVector tests ConstantVector
func TestConstantVector(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// the constant zero vector should be zero
	//////////////////////////////////////////////////////////////////////
	zeroZeroZero := ConstantVector(ZZ3, integer.Zero())
	assert.Equal(zeroZeroZero, Zero(ZZ3))
	assert.IsZero(ConstantVector(nilLattice, integer.FromInt(7)))
	//////////////////////////////////////////////////////////////////////
	// a non-zero constant vector
	//////////////////////////////////////////////////////////////////////
	v1 := ConstantVector(ZZ3, integer.FromInt(-7))
	v2, err := FromString(ZZ3, "(-7,-7,-7)")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
}

// TestSumElement tests Sum in element.go
func TestSumParent(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := Zero(ZZ2)
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	vm1m2, err := FromIntSlice(ZZ2, []int{-1, -2})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	// empty sum
	if v, err := Sum(ZZ2); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
	}
	// one-element sum
	if v, err := Sum(ZZ2, vm1m2); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, vm1m2)
	}
	// two-element sum
	if v, err := Sum(ZZ2, v12, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	// bigger sum
	if v, err := Sum(ZZ2, vm1m2, vm1m2, v24, zero, vm1m2); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, vm1m2)
	}
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = Sum(ZZ2, v111)
	assert.Error(err)
	// two-element sum
	_, err = Sum(ZZ2, v111, v24)
	assert.Error(err)
	_, err = Sum(ZZ2, v24, v111)
	assert.Error(err)
	// bigger sum
	_, err = Sum(ZZ2, vm1m2, v111, v24, zero, vm1m2)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the nil case
	//////////////////////////////////////////////////////////////////////
	nilZero := nilLattice.Zero().(*Element)
	// empty sum
	if v, err := Sum(nilLattice); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// one-element sum
	if v, err := Sum(nilLattice, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// two-element sum
	if v, err := Sum(nilLattice, nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// bigger sum
	if v, err := Sum(nilLattice, nilZero, nilZero, nilZero, nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = Sum(ZZ2, v111)
	assert.Error(err)
	_, err = Sum(nilLattice, v111)
	assert.Error(err)
	// two-element sum
	_, err = Sum(ZZ2, v111, v24)
	assert.Error(err)
	_, err = Sum(ZZ2, v24, v111)
	assert.Error(err)
	_, err = Sum(nilLattice, v111, nilZero)
	assert.Error(err)
	_, err = Sum(nilLattice, nilZero, v111)
	assert.Error(err)
	// bigger sum
	_, err = Sum(ZZ2, vm1m2, v111, v24, zero, vm1m2)
	assert.Error(err)
	_, err = Sum(nilLattice, nilZero, nilZero, nilZero, zero, nilZero)
	assert.Error(err)
}

// TestEquality tests AreEqual
func TestEquality(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(AreEqual(v24, v24))
	assert.False(AreEqual(v111, v24))
	assert.False(AreEqual(v24, v111))
	assert.False(AreEqual(nilVector, v24))
	assert.False(AreEqual(v24, nilVector))
	assert.False(AreEqual(nilZero, v24))
	assert.False(AreEqual(v24, nilZero))
	assert.False(nilZero == nilVector)
	assert.True(AreEqual(nilZero, nilVector))
	assert.True(AreEqual(nilVector, nilZero))
}

// TestDotProduct tests DotProduct
func TestDotProduct(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	L3, err := NewLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	vm12, err := FromIntSlice(ZZ2, []int{-1, 2})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	v51m1, err := FromIntSlice(L3, []int{5, 1, -1})
	assert.NoError(err)
	vbig := ConstantVector(L3, integer.FromInt64(math.MaxInt64))
	vmbig := ConstantVector(ZZ3, integer.FromInt64(math.MinInt64))
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// basic tests
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v24, Zero(ZZ2)); assert.NoError(err) {
		assert.IsZero(x)
	}
	if x, err := DotProduct(v24, vm12); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt(6)))
	}
	//////////////////////////////////////////////////////////////////////
	// dot products of elements of different lattices are fine, provided
	// that they have the same dimension
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v111, v51m1); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt(5)))
	}
	_, err = DotProduct(v111, v24)
	assert.Error(err)
	_, err = DotProduct(v24, v111)
	assert.Error(err)
	_, err = DotProduct(nilVector, v24)
	assert.Error(err)
	_, err = DotProduct(v24, nilVector)
	assert.Error(err)
	_, err = DotProduct(nilZero, v24)
	assert.Error(err)
	_, err = DotProduct(v24, nilZero)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// large values for dot products are fine
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v111, vbig); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.Multiply(integer.FromInt(3), integer.FromInt64(math.MaxInt64))))
	}
	if x, err := DotProduct(v111, vmbig); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.Multiply(integer.FromInt(3), integer.FromInt64(math.MinInt64))))
	}

}

// TestNegate Tests Negate
func TestNegate(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := NewLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v1m27, err := FromIntSlice(ZZ3, []int{1, -2, 7})
	assert.NoError(err)
	vm12m7, err := FromIntSlice(ZZ3, []int{-1, 2, -7})
	assert.NoError(err)
	zero := Zero(ZZ3)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// basic tests
	//////////////////////////////////////////////////////////////////////
	assert.AreEqual(ZZ3, Negate(v1m27), vm12m7)
	assert.AreEqual(ZZ3, Negate(zero), zero)
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	assert.AreEqual(nilLattice, Negate(nilVector), nilVector)
	assert.AreEqual(nilLattice, Negate(nilVector), nilZero)
	assert.AreEqual(nilLattice, Negate(nilZero), nilVector)
	assert.AreEqual(nilLattice, Negate(nilZero), nilZero)
}

// TestScalarMultiplyByThen tests ScalarMultiplyByIntegerThenAdd and ScalarMultiplyByIntegerThenSubtract
func TestScalarMultiplyByThen(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	vm16, err := FromIntSlice(ZZ2, []int{-1, 6})
	assert.NoError(err)
	v25, err := FromIntSlice(ZZ2, []int{2, 5})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for ScalarMultiplyByIntegerThenAdd
	//////////////////////////////////////////////////////////////////////
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), vm16, Zero(ZZ2)); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, vm16)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.FromInt(-4), vm16, v25); assert.NoError(err) {
		if vs, err := v.ToInt64Slice(); assert.NoError(err) {
			assert.Equal(vs, []int64{6, -19})
		}
	}
	// test mismatched parents
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v25, v111)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v111, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), nilVector, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), vm16, nilVector)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), nilZero, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), vm16, nilZero)
	assert.Error(err)
	// test nil cases
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilVector, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilZero, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilVector, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	//////////////////////////////////////////////////////////////////////
	// tests for ScalarMultiplyByIntegerThenSubtract
	//////////////////////////////////////////////////////////////////////
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), vm16, Zero(ZZ2)); assert.NoError(err) {
		assert.AreEqual(v.Parent(), v, vm16)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.FromInt(-4), vm16, v25); assert.NoError(err) {
		if vs, err := v.ToInt64Slice(); assert.NoError(err) {
			assert.Equal(vs, []int64{2, -29})
		}
	}
	// test mismatched parents
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v25, v111)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v111, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), nilVector, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), vm16, nilVector)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), nilZero, vm16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), vm16, nilZero)
	assert.Error(err)
	// test nil cases
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilVector, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilZero, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilVector, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
}

// TestEntry tests Entry and EntryOrPanic
func TestEntry(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests that succeed
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < v123.Dimension(); i++ {
		if v, err := v123.Entry(i); assert.NoError(err) {
			assert.True(v.IsEqualTo(integer.FromInt(i + 1)))
		}
		assert.True(v123.EntryOrPanic(i).IsEqualTo(integer.FromInt(i + 1)))
	}
	//////////////////////////////////////////////////////////////////////
	// tests that fail or panic
	//////////////////////////////////////////////////////////////////////
	_, err = v123.Entry(3)
	assert.Error(err)
	assert.Panics(func() { v123.EntryOrPanic(3) })
	_, err = v123.Entry(-1)
	assert.Error(err)
	assert.Panics(func() { v123.EntryOrPanic(-1) })
	_, err = nilVector.Entry(0)
	assert.Error(err)
	assert.Panics(func() { nilVector.EntryOrPanic(0) })
	_, err = nilZero.Entry(0)
	assert.Error(err)
	assert.Panics(func() { nilZero.EntryOrPanic(0) })
}

// TestIncrementDecrement tests IncrementEntry, IncrementEntryOrPanic, DecrementEntry, and DecrementEntryOrPanic
func TestIncrementDecrement(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v555, err := FromIntSlice(ZZ3, []int{5, 5, 5})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests that should pass
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		if v, err := v555.IncrementEntry(i); assert.NoError(err) {
			for j := 0; j < 3; j++ {
				if i == j {
					assert.True(v.EntryOrPanic(j).IsEqualToInt64(6))
				} else {
					assert.True(v.EntryOrPanic(j).IsEqualToInt64(5))
				}
			}
			w := v555.IncrementEntryOrPanic(i)
			assert.True(v.IsEqualTo(w))
		}
		if v, err := v555.DecrementEntry(i); assert.NoError(err) {
			for j := 0; j < 3; j++ {
				if i == j {
					assert.True(v.EntryOrPanic(j).IsEqualToInt64(4))
				} else {
					assert.True(v.EntryOrPanic(j).IsEqualToInt64(5))
				}
			}
			w := v555.DecrementEntryOrPanic(i)
			assert.True(v.IsEqualTo(w))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests that should fail or panic
	//////////////////////////////////////////////////////////////////////
	_, err = v555.IncrementEntry(-1)
	assert.Error(err)
	assert.Panics(func() { v555.IncrementEntryOrPanic(-1) })
	_, err = v555.IncrementEntry(3)
	assert.Error(err)
	assert.Panics(func() { v555.IncrementEntryOrPanic(3) })
	_, err = v555.DecrementEntry(-1)
	assert.Error(err)
	assert.Panics(func() { v555.DecrementEntryOrPanic(-1) })
	_, err = v555.DecrementEntry(3)
	assert.Error(err)
	assert.Panics(func() { v555.DecrementEntryOrPanic(3) })
	_, err = nilVector.IncrementEntry(0)
	assert.Error(err)
	assert.Panics(func() { nilVector.IncrementEntryOrPanic(0) })
	_, err = nilVector.DecrementEntry(0)
	assert.Error(err)
	assert.Panics(func() { nilVector.DecrementEntryOrPanic(0) })
	_, err = nilZero.IncrementEntry(0)
	assert.Error(err)
	assert.Panics(func() { nilZero.IncrementEntryOrPanic(0) })
	_, err = nilZero.DecrementEntry(0)
	assert.Error(err)
	assert.Panics(func() { nilZero.DecrementEntryOrPanic(0) })
}

// TestToSlice tests ToIntegerSlice and ToInt64Slice
func TestToSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// the code paths for the dimension gt 0 case seem to have been
	// tested already
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilElement *Element
	nilZero := Zero(nilLattice)
	assert.Len(nilElement.ToIntegerSlice(), 0)
	if v, err := nilElement.ToInt64Slice(); assert.NoError(err) {
		assert.Len(v, 0)
	}
	assert.Len(nilZero.ToIntegerSlice(), 0)
	if v, err := nilZero.ToInt64Slice(); assert.NoError(err) {
		assert.Len(v, 0)
	}
}

// TestSign tests Sign, IsNegative, and IsPositive
func TestSign(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v1m1, err := FromIntSlice(ZZ2, []int{1, -1})
	assert.NoError(err)
	vm11, err := FromIntSlice(ZZ2, []int{-1, 1})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(v1m1.IsPositive())
	assert.False(v1m1.IsNegative())
	assert.True(vm11.IsNegative())
	assert.False(vm11.IsPositive())
	assert.False(nilElement.IsPositive())
	assert.False(nilElement.IsNegative())
	assert.False(nilZero.IsPositive())
	assert.False(nilZero.IsNegative())
	assert.Zero(nilElement.Sign())
	assert.Zero(nilZero.Sign())
}

// TestComparison tests IsEqualTo, IsGreaterThan, IsGreaterThanOrEqualTo, IsLessThan, IsLessThanOrEqualTo, and TotalDegree
func TestComparison(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	vm11, err := FromIntSlice(ZZ2, []int{-1, 1})
	assert.NoError(err)
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	v231, err := FromIntSlice(ZZ3, []int{2, 3, 1})
	assert.NoError(err)
	v125, err := FromIntSlice(ZZ3, []int{1, 2, 5})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for IsEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsEqualTo(v123))
	assert.False(v123.IsEqualTo(v231))
	assert.False(v123.IsEqualTo(vm11))
	assert.False(v123.IsEqualTo(nilElement))
	assert.False(v123.IsEqualTo(nilZero))
	assert.True(nilZero.IsEqualTo(nilElement))
	assert.True(nilZero.IsEqualTo(nilZero))
	assert.False(nilZero.IsEqualTo(vm11))
	assert.True(nilElement.IsEqualTo(nilElement))
	assert.True(nilElement.IsEqualTo(nilZero))
	assert.False(nilElement.IsEqualTo(vm11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThan
	//////////////////////////////////////////////////////////////////////
	assert.False(v123.IsGreaterThan(v123))
	assert.False(v123.IsGreaterThan(v231))
	assert.False(v123.IsGreaterThan(v125))
	assert.False(v123.IsGreaterThan(vm11))
	assert.False(v123.IsGreaterThan(nilElement))
	assert.False(v123.IsGreaterThan(nilZero))
	assert.False(nilZero.IsGreaterThan(nilElement))
	assert.False(nilZero.IsGreaterThan(nilZero))
	assert.False(nilZero.IsGreaterThan(vm11))
	assert.False(nilElement.IsGreaterThan(nilElement))
	assert.False(nilElement.IsGreaterThan(nilZero))
	assert.False(nilElement.IsGreaterThan(vm11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsGreaterThanOrEqualTo(v123))
	assert.False(v123.IsGreaterThanOrEqualTo(v231))
	assert.False(v123.IsGreaterThanOrEqualTo(v125))
	assert.False(v123.IsGreaterThanOrEqualTo(vm11))
	assert.False(v123.IsGreaterThanOrEqualTo(nilElement))
	assert.False(v123.IsGreaterThanOrEqualTo(nilZero))
	assert.True(nilZero.IsGreaterThanOrEqualTo(nilElement))
	assert.True(nilZero.IsGreaterThanOrEqualTo(nilZero))
	assert.False(nilZero.IsGreaterThanOrEqualTo(vm11))
	assert.True(nilElement.IsGreaterThanOrEqualTo(nilElement))
	assert.True(nilElement.IsGreaterThanOrEqualTo(nilZero))
	assert.False(nilElement.IsGreaterThanOrEqualTo(vm11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThan
	//////////////////////////////////////////////////////////////////////
	assert.False(v123.IsLessThan(v123))
	assert.True(v123.IsLessThan(v231))
	assert.True(v123.IsLessThan(v125))
	assert.False(v123.IsLessThan(vm11))
	assert.False(v123.IsLessThan(nilElement))
	assert.False(v123.IsLessThan(nilZero))
	assert.False(nilZero.IsLessThan(nilElement))
	assert.False(nilZero.IsLessThan(nilZero))
	assert.False(nilZero.IsLessThan(vm11))
	assert.False(nilElement.IsLessThan(nilElement))
	assert.False(nilElement.IsLessThan(nilZero))
	assert.False(nilElement.IsLessThan(vm11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsLessThanOrEqualTo(v123))
	assert.True(v123.IsLessThanOrEqualTo(v231))
	assert.True(v123.IsLessThanOrEqualTo(v125))
	assert.False(v123.IsLessThanOrEqualTo(vm11))
	assert.False(v123.IsLessThanOrEqualTo(nilElement))
	assert.False(v123.IsLessThanOrEqualTo(nilZero))
	assert.True(nilZero.IsLessThanOrEqualTo(nilElement))
	assert.True(nilZero.IsLessThanOrEqualTo(nilZero))
	assert.False(nilZero.IsLessThanOrEqualTo(vm11))
	assert.True(nilElement.IsLessThanOrEqualTo(nilElement))
	assert.True(nilElement.IsLessThanOrEqualTo(nilZero))
	assert.False(nilElement.IsLessThanOrEqualTo(vm11))
	//////////////////////////////////////////////////////////////////////
	// tests for TotalDegree
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.TotalDegree().IsEqualTo(integer.FromInt(6)))
	assert.True(v231.TotalDegree().IsEqualTo(integer.FromInt(6)))
	assert.True(v125.TotalDegree().IsEqualTo(integer.FromInt(8)))
	assert.True(vm11.TotalDegree().IsZero())
	assert.True(nilZero.TotalDegree().IsZero())
	assert.True(nilElement.TotalDegree().IsZero())
}

// TestScalarMultiplyBy tests ScalarMultiplyByInteger, ScalarMultiplyByInt64, and ScalarMultiplyByUint64
func TestScalarMultiplyBy(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByInteger
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.ScalarMultiplyByInteger(integer.Zero()).IsZero())
	if vs, err := v123.ScalarMultiplyByInteger(integer.FromInt(-6)).ToInt64Slice(); assert.NoError(err) {
		assert.Equal(vs, []int64{-6, -12, -18})
	}
	assert.True(nilElement.ScalarMultiplyByInteger(integer.Zero()).IsZero())
	assert.True(nilElement.ScalarMultiplyByInteger(integer.FromInt(6)).IsZero())
	assert.True(nilZero.ScalarMultiplyByInteger(integer.Zero()).IsZero())
	assert.True(nilZero.ScalarMultiplyByInteger(integer.FromInt(6)).IsZero())
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByInt64
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.ScalarMultiplyByInt64(0).IsZero())
	if vs, err := v123.ScalarMultiplyByInt64(-1).ToInt64Slice(); assert.NoError(err) {
		assert.Equal(vs, []int64{-1, -2, -3})
	}
	if vs, err := v123.ScalarMultiplyByInt64(7).ToInt64Slice(); assert.NoError(err) {
		assert.Equal(vs, []int64{7, 14, 21})
	}
	assert.True(nilElement.ScalarMultiplyByInt64(0).IsZero())
	assert.True(nilElement.ScalarMultiplyByInt64(3).IsZero())
	assert.True(nilZero.ScalarMultiplyByInt64(0).IsZero())
	assert.True(nilZero.ScalarMultiplyByInt64(3).IsZero())
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByUint64
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.ScalarMultiplyByUint64(0).IsZero())
	assert.AreEqual(ZZ3, v123.ScalarMultiplyByUint64(1), v123)
	if vs, err := v123.ScalarMultiplyByUint64(7).ToInt64Slice(); assert.NoError(err) {
		assert.Equal(vs, []int64{7, 14, 21})
	}
	assert.True(nilElement.ScalarMultiplyByUint64(0).IsZero())
	assert.True(nilElement.ScalarMultiplyByUint64(3).IsZero())
	assert.True(nilZero.ScalarMultiplyByUint64(0).IsZero())
	assert.True(nilZero.ScalarMultiplyByUint64(3).IsZero())
}

// TestHash tests Hash
func TestHash(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	vm11, err := FromIntSlice(ZZ2, []int{-1, 1})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// equal elements should hash to the same thing
	//////////////////////////////////////////////////////////////////////
	if v, err := FromString(ZZ2, "(-1,1)"); assert.NoError(err) {
		assert.Equal(vm11.Hash(), v.Hash())
	}
	assert.Equal(nilElement.Hash(), hash.Value(0))
	assert.Equal(nilZero.Hash(), hash.Value(0))
}

// TestString tests String in element.go
func TestElementString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	vm11, err := FromIntSlice(ZZ2, []int{-1, 1})
	assert.NoError(err)
	zero := Zero(ZZ2)
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.Equal(vm11.String(), "(-1,1)")
	assert.Equal(zero.String(), "(0,0)")
	assert.Equal(nilElement.String(), "()")
	assert.Equal(nilZero.String(), "()")
}

// TestBadParents tests that errors occur in various functions when they are passed arguments that lie in incompatible or wrong parents
func TestBadParents(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// the tests for Cmp
	//////////////////////////////////////////////////////////////////////
	_, err = Cmp(v24, v111)
	assert.Error(err)
	_, err = Cmp(v111, v24)
	assert.Error(err)
	_, err = Cmp(v24, nilVector)
	assert.Error(err)
	_, err = Cmp(nilVector, v24)
	assert.Error(err)
	_, err = Cmp(v24, nilZero)
	assert.Error(err)
	_, err = Cmp(nilZero, v24)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the tests for Add
	//////////////////////////////////////////////////////////////////////
	_, err = Add(v24, v111)
	assert.Error(err)
	_, err = Add(v111, v24)
	assert.Error(err)
	_, err = Add(v24, nilVector)
	assert.Error(err)
	_, err = Add(nilVector, v24)
	assert.Error(err)
	_, err = Add(v24, nilZero)
	assert.Error(err)
	_, err = Add(nilZero, v24)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the tests for Subtract
	//////////////////////////////////////////////////////////////////////
	_, err = Subtract(v24, v111)
	assert.Error(err)
	_, err = Subtract(v111, v24)
	assert.Error(err)
	_, err = Subtract(v24, nilVector)
	assert.Error(err)
	_, err = Subtract(nilVector, v24)
	assert.Error(err)
	_, err = Subtract(v24, nilZero)
	assert.Error(err)
	_, err = Subtract(nilZero, v24)
	assert.Error(err)
}
