// Tests assert.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package assert

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestContains tests Contains and NotContains.
func TestContains(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	// Run the tests
	if !a.Contains(R, zero) {
		t.Error("Contains should return true")
	}
	if a.Contains(R, nil) {
		t.Error("Contains should return false")
	}
	if a.NotContains(R, zero) {
		t.Error("NotContains should return false")
	}
	if !a.NotContains(R, nil) {
		t.Error("NotContains should return true")
	}
}

// TestIs tests IsZero, IsZeroIn, IsOne, and IsOneIn.
func TestIsZero(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	// Run the tests
	if !a.IsZero(zero) {
		t.Error("IsZero should return true")
	}
	if a.IsZero(one) {
		t.Error("IsZero should return false")
	}
	if !a.IsZeroIn(R, zero) {
		t.Error("IsZeroIn should return true")
	}
	if a.IsZeroIn(R, one) {
		t.Error("IsZeroIn should return false")
	}
	if a.IsOne(zero) {
		t.Error("IsOne should return false")
	}
	if !a.IsOne(one) {
		t.Error("IsOne should return true")
	}
	if a.IsOneIn(R, zero) {
		t.Error("IsOneIn should return false")
	}
	if !a.IsOneIn(R, one) {
		t.Error("IsOneIn should return true")
	}
	// If an element comes from a different parent, this should fail
	if a.IsZeroIn(R, nil) {
		t.Error("IsZeroIn should return false")
	}
	if a.IsOneIn(R, nil) {
		t.Error("IsOneIn should return false")
	}
}

// TestAddSubtractIsEqualTo tests AddIsEqualTo and SubtractIsEqualTo.
func TestAddIsEqualTo(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	// Run the tests
	if !a.AddIsEqualTo(R, zero, one, one) {
		t.Error("AddIsEqualTo should return true")
	}
	if a.AddIsEqualTo(R, zero, one, zero) {
		t.Error("AddIsEqualTo should return false")
	}
	if !a.SubtractIsEqualTo(R, one, zero, one) {
		t.Error("SubtractIsEqualTo should return true")
	}
	if a.SubtractIsEqualTo(R, one, zero, zero) {
		t.Error("SubtractIsEqualTo should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.AddIsEqualTo(R, zero, nil, zero) {
		t.Error("AddIsEqualTo should return false")
	}
	if a.AddIsEqualTo(R, zero, zero, nil) {
		t.Error("AddIsEqualTo should return false")
	}
	if a.SubtractIsEqualTo(R, zero, nil, zero) {
		t.Error("SubtractIsEqualTo should return true")
	}
	if a.SubtractIsEqualTo(R, zero, zero, nil) {
		t.Error("SubtractIsEqualTo should return true")
	}
}

// TestNegateIsEqualTo tests NegateIsEqualTo.
func TestNegateIsEqualTo(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	// Run the tests
	if !a.NegateIsEqualTo(R, zero, zero) {
		t.Error("NegateIsEqualTo should return true")
	}
	if a.NegateIsEqualTo(R, zero, one) {
		t.Error("NegateIsEqualTo should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.NegateIsEqualTo(R, nil, zero) {
		t.Error("NegateIsEqualTo should return false")
	}
	if a.NegateIsEqualTo(R, zero, nil) {
		t.Error("NegateIsEqualTo should return false")
	}
}

// TestMultiplyIsEqualTo tests MultiplyIsEqualTo.
func TestMultiplyIsEqualTo(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	// Run the tests
	if !a.MultiplyIsEqualTo(R, one, one, one) {
		t.Error("MultiplyIsEqualTo should return true")
	}
	if a.MultiplyIsEqualTo(R, one, one, zero) {
		t.Error("MultiplyIsEqualTo should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.MultiplyIsEqualTo(R, one, nil, zero) {
		t.Error("MultiplyIsEqualTo should return false")
	}
	if a.MultiplyIsEqualTo(R, one, zero, nil) {
		t.Error("MultiplyIsEqualTo should return false")
	}
}

// TestPowerIsEqualTo tests PowerIsEqualTo.
func TestPowerIsEqualTo(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	two := integer.FromInt(2)
	four := integer.FromInt(4)
	// Run the tests
	if !a.PowerIsEqualTo(R, two, two, four) {
		t.Error("PowerIsEqualTo should return true")
	}
	if a.PowerIsEqualTo(R, two, two, two) {
		t.Error("PowerIsEqualTo should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.PowerIsEqualTo(R, nil, two, integer.Zero()) {
		t.Error("PowerIsEqualTo should return false")
	}
	if a.PowerIsEqualTo(R, integer.Zero(), two, nil) {
		t.Error("PowerIsEqualTo should return false")
	}
}

// TestScalarMultiplyByIntegerIsEqualTo tests ScalarMultiplyByIntegerIsEqualTo.
func TestScalarMultiplyByIntegerIsEqualTo(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	two := integer.FromInt(2)
	four := integer.FromInt(4)
	// Run the tests
	if !a.ScalarMultiplyByIntegerIsEqualTo(R, two, two, four) {
		t.Error("ScalarMultiplyByIntegerIsEqualTo should return true")
	}
	if a.ScalarMultiplyByIntegerIsEqualTo(R, two, two, two) {
		t.Error("ScalarMultiplyByIntegerIsEqualTo should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.ScalarMultiplyByIntegerIsEqualTo(R, two, nil, integer.Zero()) {
		t.Error("ScalarMultiplyByIntegerIsEqualTo should return false")
	}
	if a.ScalarMultiplyByIntegerIsEqualTo(R, two, integer.Zero(), nil) {
		t.Error("ScalarMultiplyByIntegerIsEqualTo should return false")
	}
}

// TestAreEqual tests AreEqual and AreNotEqual.
func TestAreEqual(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	// Run the tests
	if !a.AreEqual(R, zero, zero) {
		t.Error("AreEqual should return true")
	}
	if a.AreEqual(R, zero, one) {
		t.Error("AreEqual should return false")
	}
	if a.AreNotEqual(R, zero, zero) {
		t.Error("AreNotEqual should return false")
	}
	if !a.AreNotEqual(R, zero, one) {
		t.Error("AreNotEqual should return true")
	}
	// If an element comes from a different parent, this should fail
	if a.AreEqual(R, zero, nil) {
		t.Error("AreEqual should return false")
	}
	if a.AreNotEqual(R, zero, nil) {
		t.Error("AreNotEqual should return false")
	}
}

// TestAreEqualSlices tests AreEqualSlices.
func TestAreEqualSlices(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and some slices of elements
	R := integer.Ring()
	zero := integer.Zero()
	one := integer.One()
	R1 := []*integer.Element{zero, one}
	R2 := []*integer.Element{zero, one, one}
	R3 := []*integer.Element{zero, zero}
	S := []object.Element{nil, nil}
	// Run the tests
	if !a.AreEqualSlices(R, R1, R1) {
		t.Error("AreEqualSlices should return true")
	}
	if a.AreEqualSlices(R, R1, R2) {
		t.Error("AreEqualSlices should return false")
	}
	if a.AreEqualSlices(R, R1, R3) {
		t.Error("AreEqualSlices should return false")
	}
	if a.AreEqualSlices(R, R3, S) {
		t.Error("AreEqualSlices should return false")
	}
	// Passing an object that isn't a slice should fail
	if a.AreEqualSlices(R, zero, R1) {
		t.Error("AreEqualSlices should return false")
	}
	if a.AreEqualSlices(R, R1, zero) {
		t.Error("AreEqualSlices should return false")
	}
	// Passing a slice that contains a non-object.Element should fail
	U := []interface{}{zero, 0}
	if a.AreEqualSlices(R, U, R1) {
		t.Error("AreEqualSlices should return false")
	}
	if a.AreEqualSlices(R, R1, U) {
		t.Error("AreEqualSlices should return false")
	}
}

// TestIsEqualToInt tests IsEqualToInt and IsEqualToInt64.
func TestIsEqualToInt(t *testing.T) {
	a := New(new(testing.T))
	if !a.IsEqualToInt(integer.Zero(), 0) {
		t.Error("IsEqualToInt should return true")
	}
	if a.IsEqualToInt(integer.Zero(), 1) {
		t.Error("IsEqualToInt should return false")
	}
	if !a.IsEqualToInt64(integer.Zero(), 0) {
		t.Error("IsEqualToInt64 should return true")
	}
	if a.IsEqualToInt64(integer.Zero(), 1) {
		t.Error("IsEqualToInt64 should return false")
	}
}

// TestCmp tests Cmp.
func TestCmp(t *testing.T) {
	a := New(new(testing.T))
	// Create a new parent R and the values to check
	R := integer.Ring()
	zero := integer.Zero()
	// Run the tests
	if !a.Cmp(R, zero, zero, 0) {
		t.Error("Cmp should return true")
	}
	if a.Cmp(R, zero, zero, 1) {
		t.Error("Cmp should return false")
	}
	// If an element comes from a different parent, this should fail
	if a.Cmp(R, zero, nil, 0) {
		t.Error("Cmp should return false")
	}
}
