// Assert extends the Assertions object of github.com/stretchr/testify/assert to provide convenient access to common assertions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package assert

import (
	"bitbucket.org/pcas/math/abeliangroup"
	"bitbucket.org/pcas/math/compare"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"fmt"
	"github.com/stretchr/testify/assert"
	"reflect"
)

// Assertions embeds the Assertions struct from github.com/stretchr/testify/assert.
type Assertions struct {
	*assert.Assertions
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getLen returns the length of the given object. Returns false,0 if impossible.
func getLen(x interface{}) (ok bool, length int) {
	v := reflect.ValueOf(x)
	defer func() {
		if e := recover(); e != nil {
			ok = false
		}
	}()
	return true, v.Len()
}

// getEntryAsElement returns the i-th element as an object.Element. Returns false,nil if impossible.
func getEntryAsElement(x interface{}, i int) (bool, object.Element) {
	v := reflect.ValueOf(x)
	val, ok := v.Index(i).Interface().(object.Element)
	return ok, val
}

/////////////////////////////////////////////////////////////////////////
// Assertions functions
/////////////////////////////////////////////////////////////////////////

// New returns a new *Assertions for the given testing context t.
func New(t assert.TestingT) (a *Assertions) {
	a = &Assertions{}
	a.Assertions = assert.New(t)
	return
}

// Contains asserts that R.Contains(x) returns true.
func (a *Assertions) Contains(R object.Parent, x object.Element, msgAndArgs ...interface{}) bool {
	if !R.Contains(x) {
		return a.Fail(fmt.Sprintf("object.Element not contained in object.Parent:\n"+
			"           %s (Arg)\n"+
			"           %s (Parent)", x, R), msgAndArgs...)
	}
	return true
}

// NotContains asserts that R.Contains(x) returns false.
func (a *Assertions) NotContains(R object.Parent, x object.Element, msgAndArgs ...interface{}) bool {
	if R.Contains(x) {
		return a.Fail(fmt.Sprintf("object.Element contained in object.Parent:\n"+
			"           %s (Arg)\n"+
			"           %s (Parent)", x, R), msgAndArgs...)
	}
	return true
}

// IsZero asserts that x.IsZero() returns true.
func (a *Assertions) IsZero(x IsZeroer, msgAndArgs ...interface{}) bool {
	if !x.IsZero() {
		return a.Fail(fmt.Sprintf("object.Element not zero: %s\n", x), msgAndArgs...)
	}
	return true
}

// IsZeroIn asserts that R.IsZero(x) returns true.
func (a *Assertions) IsZeroIn(R abeliangroup.Interface, x object.Element, msgAndArgs ...interface{}) bool {
	ok, err := R.IsZero(x)
	if err != nil {
		return a.Fail(fmt.Sprintf("IsZero for object.Element returned an error:\n"+
			"      Arg: %s\n"+
			"      Err: %s", x, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Element non-zero:\n"+
			"           %s (Arg)", x), msgAndArgs...)
	}
	return true
}

// AddIsEqualTo asserts that R.AreEqual(R.Add(x,y),z) returns true.
func (a *Assertions) AddIsEqualTo(R abeliangroup.Interface, x object.Element, y object.Element, z object.Element, msgAndArgs ...interface{}) bool {
	sum, err := R.Add(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("Add for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(sum, z)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", sum, z, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", sum, z), msgAndArgs...)
	}
	return true
}

// SubtractIsEqualTo asserts that R.AreEqual(R.Subtract(x,y),z) returns true.
func (a *Assertions) SubtractIsEqualTo(R abeliangroup.Interface, x object.Element, y object.Element, z object.Element, msgAndArgs ...interface{}) bool {
	dif, err := R.Subtract(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("Subtract for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(dif, z)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", dif, z, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", dif, z), msgAndArgs...)
	}
	return true
}

// NegateIsEqualTo asserts that R.AreEqual(R.Negate(x),y) returns true.
func (a *Assertions) NegateIsEqualTo(R abeliangroup.Interface, x object.Element, y object.Element, msgAndArgs ...interface{}) bool {
	neg, err := R.Negate(x)
	if err != nil {
		return a.Fail(fmt.Sprintf("Negate for object.Element returned an error:\n"+
			"      Arg: %s\n"+
			"      Err: %s", x, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(neg, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", neg, y, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", neg, y), msgAndArgs...)
	}
	return true
}

// IsOne asserts that x.IsOne() returns true.
func (a *Assertions) IsOne(x IsOner, msgAndArgs ...interface{}) bool {
	if !x.IsOne() {
		return a.Fail(fmt.Sprintf("object.Element not one: %s\n", x), msgAndArgs...)
	}
	return true
}

// IsOneIn asserts that R.IsOne(x) returns true.
func (a *Assertions) IsOneIn(R ring.Interface, x object.Element, msgAndArgs ...interface{}) bool {
	ok, err := R.IsOne(x)
	if err != nil {
		return a.Fail(fmt.Sprintf("IsOne for object.Element returned an error:\n"+
			"      Arg: %s\n"+
			"      Err: %s", x, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Element non-one:\n"+
			"           %s (Arg)", x), msgAndArgs...)
	}
	return true
}

// MultiplyIsEqualTo asserts that R.AreEqual(R.Multiply(x,y),z) returns true.
func (a *Assertions) MultiplyIsEqualTo(R ring.Interface, x object.Element, y object.Element, z object.Element, msgAndArgs ...interface{}) bool {
	prod, err := R.Multiply(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("Multiply for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(prod, z)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", prod, z, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", prod, z), msgAndArgs...)
	}
	return true
}

// PowerIsEqualTo asserts that R.AreEqual(R.Power(x,k),y) returns true.
func (a *Assertions) PowerIsEqualTo(R ring.Interface, x object.Element, k *integer.Element, y object.Element, msgAndArgs ...interface{}) bool {
	pow, err := R.Power(x, k)
	if err != nil {
		return a.Fail(fmt.Sprintf("Power for object.Elements returned an error:\n"+
			"      Arg: %s\n"+
			"      Pow: %s\n"+
			"      Err: %s", x, k, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(pow, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", pow, y, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", pow, y), msgAndArgs...)
	}
	return true
}

// ScalarMultiplyByIntegerIsEqualTo asserts that R.AreEqual(R.ScalarMultiplyByInteger(c,x),y) returns true.
func (a *Assertions) ScalarMultiplyByIntegerIsEqualTo(R abeliangroup.Interface, c *integer.Element, x object.Element, y object.Element, msgAndArgs ...interface{}) bool {
	cx, err := R.ScalarMultiplyByInteger(c, x)
	if err != nil {
		return a.Fail(fmt.Sprintf("ScalarMultiplyByInteger for object.Elements returned an error:\n"+
			"   Scalar: %s\n"+
			"      Arg: %s\n"+
			"      Err: %s", c, x, err), msgAndArgs...)
	}
	ok, err := R.AreEqual(cx, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", cx, y, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", cx, y), msgAndArgs...)
	}
	return true
}

// AreEqual asserts that R.AreEqual(x,y) returns true.
func (a *Assertions) AreEqual(R object.Parent, x object.Element, y object.Element, msgAndArgs ...interface{}) bool {
	ok, err := R.AreEqual(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	if !ok {
		return a.Fail(fmt.Sprintf("object.Elements not equal:\n"+
			"           %s (Arg1)\n"+
			"        != %s (Arg2)", x, y), msgAndArgs...)
	}
	return true
}

// AreNotEqual asserts that R.AreEqual(x,y) returns false (but not an error).
func (a *Assertions) AreNotEqual(R object.Parent, x object.Element, y object.Element, msgAndArgs ...interface{}) bool {
	ok, err := R.AreEqual(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("AreEqual for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	if ok {
		return a.Fail(fmt.Sprintf("object.Elements equal:\n"+
			"           %s (Arg1)\n"+
			"        == %s (Arg2)", x, y), msgAndArgs...)
	}
	return true
}

// AreEqualSlices asserts that len(S1) = len(S2) and that R.AreEqual(S1[i],S2[i]) return true for all i.
func (a *Assertions) AreEqualSlices(R object.Parent, S1 interface{}, S2 interface{}, msgAndArgs ...interface{}) bool {
	// Check that S1 and S2 are slices of the same length
	var l1 int
	var ok bool
	if ok, l1 = getLen(S1); !ok {
		return a.Fail(fmt.Sprintf("Unable to apply built-in len() to argument 1:\n"+
			"     Arg1: %s\n", S1), msgAndArgs...)
	} else if ok, l2 := getLen(S2); !ok {
		return a.Fail(fmt.Sprintf("Unable to apply built-in len() to argument 2:\n"+
			"     Arg2: %s\n", S2), msgAndArgs...)
	} else if l1 != l2 {
		return a.Fail(fmt.Sprintf("Lengths disagree:\n"+
			"     Arg1: %s\n"+
			"  Length1: %d\n"+
			"     Arg2: %s\n"+
			"  Length2: %d\n", S1, l1, S2, l2), msgAndArgs...)
	}
	// Start working through the slices
	for i := 0; i < l1; i++ {
		if ok, x1 := getEntryAsElement(S1, i); !ok {
			return a.Fail(fmt.Sprintf("Unable to coerce entry %d of argument 1 to an object.Element:\n"+
				"     Arg1: %s\n", i, S1), msgAndArgs...)
		} else if ok, x2 := getEntryAsElement(S2, i); !ok {
			return a.Fail(fmt.Sprintf("Unable to coerce entry %d of argument 2 to an object.Element:\n"+
				"     Arg2: %s\n", i, S2), msgAndArgs...)
		} else if ok, err := R.AreEqual(x1, x2); err != nil {
			return a.Fail(fmt.Sprintf("AreEqual for entry %d returned an error:\n"+
				"       x1: %s\n"+
				"       x2: %s\n"+
				"      Err: %s", i, x1, x2, err), msgAndArgs...)
		} else if !ok {
			return a.Fail(fmt.Sprintf("Entry %d elements not equal:\n"+
				"           %s (x1)\n"+
				"        != %s (x2)", i, x1, x2), msgAndArgs...)
		}
	}
	return true
}

// IsEqualToInt asserts that the given *integer.Element is equal to the given int.
func (a *Assertions) IsEqualToInt(x *integer.Element, y int, msgAndArgs ...interface{}) bool {
	if !x.IsEqualToInt(y) {
		return a.Fail(fmt.Sprintf("*integer.Element and int not equal:\n"+
			"           %s (Arg1)\n"+
			"        == %d (Arg2)", x, y), msgAndArgs...)
	}
	return true
}

// IsEqualToInt64 asserts that the given *integer.Element is equal to the given int64.
func (a *Assertions) IsEqualToInt64(x *integer.Element, y int64, msgAndArgs ...interface{}) bool {
	if !x.IsEqualToInt64(y) {
		return a.Fail(fmt.Sprintf("*integer.Element and int64 not equal:\n"+
			"           %s (Arg1)\n"+
			"        == %d (Arg2)", x, y), msgAndArgs...)
	}
	return true
}

// Cmp asserts that R.Cmp(x,y) == sgn.
func (a *Assertions) Cmp(R compare.Cmper, x object.Element, y object.Element, sgn int, msgAndArgs ...interface{}) bool {
	cmpsgn, err := R.Cmp(x, y)
	if err != nil {
		return a.Fail(fmt.Sprintf("Cmp for object.Elements returned an error:\n"+
			"     Arg1: %s\n"+
			"     Arg2: %s\n"+
			"      Err: %s", x, y, err), msgAndArgs...)
	}
	if cmpsgn != sgn {
		return a.Fail(fmt.Sprintf("Cmp for object.Elements not of expected sign:\n"+
			"           %s (Arg1)\n"+
			"           %s (Arg2)\n"+
			"		    %d (Expected)\n"+
			"			%d (Actual)\n", x, y, sgn, cmpsgn), msgAndArgs...)
	}
	return true
}

// Printf is a copy of fmt.Printf, for easier debug-printing
func (a *Assertions) Printf(format string, x ...interface{}) (n int, err error) {
	return fmt.Printf(format, x...)
}
