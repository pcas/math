// Interfaces defines interfaces used by the assert package.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package assert

import (
	"bitbucket.org/pcas/math/object"
)

// IsZeroer is the interface satisfied by an object.Element with the IsZero method.
type IsZeroer interface {
	object.Element
	IsZero() bool // IsZero returns true iff the object is the zero element 0.
}

// IsOner is the interface satisfied by an object.Element with the IsOne method.
type IsOner interface {
	object.Element
	IsOne() bool // IsOne returns true iff the object is the unit element 1.
}
