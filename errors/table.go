//go:generate ./table.py table.txt
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "table.txt".
To modify this file, edit "table.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/errors
/////////////////////////////////////////////////////////////////////////
// table.go
/////////////////////////////////////////////////////////////////////////

package errors

import (
	"bitbucket.org/pcastools/errors"
)

// The error templates.
const (
	Arg1MustBeNonNegative               = errors.Template("Argument 1 must be a non-negative integer.")
	Arg1NotABoolean                     = errors.Template("Argument 1 is not a boolean.")
	Arg1NotARational                    = errors.Template("Argument 1 is not a rational number.")
	Arg1NotASliceOfElements             = errors.Template("Argument 1 is not a slice of elements.")
	Arg1NotAnInteger                    = errors.Template("Argument 1 is not an integer.")
	Arg1NotContainedInParent            = errors.Template("Argument 1 is not contained in the given parent.")
	Arg1OutOfRange                      = errors.Template("Argument 1 out of range.")
	Arg1TooLarge                        = errors.Template("Argument 1 is too large.")
	Arg2MustBeNonNegative               = errors.Template("Argument 2 must be a non-negative integer.")
	Arg2NotABoolean                     = errors.Template("Argument 2 is not a boolean.")
	Arg2NotARational                    = errors.Template("Argument 2 is not a rational number.")
	Arg2NotASliceOfElements             = errors.Template("Argument 2 is not a slice of elements.")
	Arg2NotAnInteger                    = errors.Template("Argument 2 is not an integer.")
	Arg2NotContainedInParent            = errors.Template("Argument 2 is not contained in the given parent.")
	Arg2OutOfRange                      = errors.Template("Argument 2 out of range.")
	Arg2TooLarge                        = errors.Template("Argument 2 is too large.")
	ArgNotABoolean                      = errors.Template("Argument is not a boolean.")
	ArgNotAPrimePower                   = errors.Template("Argument is not a prime power.")
	ArgNotARational                     = errors.Template("Argument is not a rational number.")
	ArgNotASliceOfElements              = errors.Template("Argument is not a slice of elements.")
	ArgNotASliceOfPrimeFieldElements    = errors.Template("Argument is not a slice of elements of fields of prime characteristic.")
	ArgNotASliceOfPrimes                = errors.Template("Argument is not a slice of primes.")
	ArgNotAnInteger                     = errors.Template("Argument is not an integer.")
	ArgNotAnIntegerGreaterThan1         = errors.Template("Argument is not an integer >= 2.")
	ArgNotAnIntegralVector              = errors.Template("Argument is not an integral vector.")
	ArgNotContainedInCoefficientRing    = errors.Template("Argument is not contained in the coefficient ring.")
	ArgNotContainedInParent             = errors.Template("Argument is not contained in the given parent.")
	ArgNotPrime                         = errors.Template("Argument is not prime.")
	ArgOutOfRange                       = errors.Template("Argument out of range.")
	ArgsNotCoprime                      = errors.Template("The arguments must be coprime.")
	BasisUndefined                      = errors.Template("Undefined basis.")
	CorruptEncoding                     = errors.Template("The encoded data is corrupted.")
	DimensionAndRankDoNotAgree          = errors.Template("The dimension must equal the rank.")
	DimensionMustBeNonNegative          = errors.Template("Dimension must be a non-negative integer.")
	DimensionsDoNotAgree                = errors.Template("Arguments must have the same dimension.")
	DivisionByZero                      = errors.Template("Division by zero.")
	DomainNotEqualCodomainInComposition = errors.Template("Illegal composition: the codomain (%s) does not equal the domain (%s).")
	ElementNotAUnit                     = errors.Template("Element is not a unit.")
	ElementNotOne                       = errors.Template("Element is not one.")
	EmptySlice                          = errors.Template("Illegal empty slice.")
	ExponentTooLarge                    = errors.Template("Exponent is too large.")
	GroundRingsDoNotAgree               = errors.Template("Arguments must be defined over the same ground ring.")
	IllegalMatrixSize                   = errors.Template("Illegal matrix size (%d x %d).")
	IllegalNegativeArg                  = errors.Template("Argument must be non-negative.")
	IllegalNegativeExponent             = errors.Template("Exponent must be non-negative.")
	IllegalNext                         = errors.Template("Illegal call to Next without first calling HasNext.")
	IllegalNilParent                    = errors.Template("The parent must be non-nil.")
	IllegalNonPositiveArg               = errors.Template("Argument must be positive.")
	IllegalZeroArg                      = errors.Template("Argument must be non-zero.")
	IncompatibleDimensions              = errors.Template("The arguments have incompatible dimensions.")
	IncompatibleMonomialOrders          = errors.Template("The arguments have incompatible monomial orders.")
	InvalidArgRange                     = errors.Template("Argument (%d) should be in range %d..%d.")
	InvalidBiIndexRange                 = errors.Template("Index (%d, %d) should be in range (0..%d, 0..%d).")
	InvalidIndexInequality              = errors.Template("Invalid index: %d > %d.")
	InvalidIndexNonNegative             = errors.Template("Index (%d) should be non-negative.")
	InvalidIndexRange                   = errors.Template("Index (%d) should be in range 0..%d.")
	IteratorIsClosed                    = errors.Template("Iterator is closed.")
	MatricesOfIncompatibleSizes         = errors.Template("The matrices are of incompatible sizes (%d x %d and %d x %d).")
	MatrixNotSquare                     = errors.Template("The matrix must be square.")
	MatrixOfIncompatibleSize            = errors.Template("The matrix (%d x %d) is of incompatible size (expected %d x %d).")
	MatrixSizesMustAgree                = errors.Template("The sizes of the matrices must agree (%d x %d and %d x %d).")
	MatrixSizesMustAgreeNoDetails       = errors.Template("The sizes of the matrices must agree.")
	MatrixTooLarge                      = errors.Template("The matrix has too many entries.")
	NilObject                           = errors.Template("Illegal nil object.")
	NotImplemented                      = errors.Template("Not implemented.")
	NotImplementedYet                   = errors.Template("Not implemented yet.")
	NumberOfColumnsDoesNotAgree         = errors.Template("The number of columns in the parent must equal the length of each row slice.")
	NumberOfColumnsOfMatricesDoNotAgree = errors.Template("The number of columns of the matrices (%d and %d) must be equal.")
	NumberOfRowsDoesNotAgree            = errors.Template("The number of rows in the parent must equal the length of the slice of rows.")
	NumbersOfRowsAndColumnsDoNotAgree   = errors.Template("The number of rows must equal the number of columns.")
	OddLengthSlice                      = errors.Template("The slice should be of even length.")
	OutOfRange                          = errors.Template("Result of calculation out of range.")
	ParentDoesNotHaveDerivative         = errors.Template("Parent must provide a formal derivative method.")
	ParentIsNotACmper                   = errors.Template("Parent must have a comparison method.")
	ParentIsNotAnAbelianMonoid          = errors.Template("Parent must be an abelian monoid.")
	ParentMustHaveRankAndMonomialOrder  = errors.Template("Parent must have a rank and monomial order.")
	ParentsDoNotAgree                   = errors.Template("Arguments must have the same parent.")
	ParentsOfEntriesDoNotAgree          = errors.Template("The entries must have the same parent.")
	PolynomialRingNoVariables           = errors.Template("The polynomial ring does not support variables.")
	ProductDoesNotHaveInclusionMaps     = errors.Template("Product does not have inclusion maps.")
	RankMustBeNonNegative               = errors.Template("Rank must be a non-negative integer.")
	RankMustBeOne                       = errors.Template("Rank must be equal to 1.")
	RationalNotAnInteger                = errors.Template("The rational number cannot be converted to an integer.")
	SliceLengthMustBeNonNegative        = errors.Template("The slice length (%d) must be non-negative.")
	SliceLengthNotEqual                 = errors.Template("The slices must be of the same length.")
	SliceLengthNotEqualDimension        = errors.Template("The length of the slice (%d) does not match the dimension of the parent (%d).")
	SliceLengthNotEqualRank             = errors.Template("The length of the slice (%d) does not match the rank of the parent (%d).")
	SliceLengthNotEqualTo               = errors.Template("The length of the slice (%d) must be %d.")
	SliceNotCoprime                     = errors.Template("The elements of the slice must be coprime.")
	SliceNotPositive                    = errors.Template("The elements of the slice must be positive.")
	SliceNotSorted                      = errors.Template("The elements of the slice must be sorted in increasing order.")
	SliceWrongLength                    = errors.Template("The slice has the wrong length.")
	StringNotARational                  = errors.Template("The string cannot be converted to a rational number.")
	StringNotAnInteger                  = errors.Template("The string cannot be converted to an integer.")
	TooManyTerms                        = errors.Template("Too many terms.")
	UnableToConvertToIntegerSlice       = errors.Template("Unable to convert element to an integer slice.")
	UnknownMonomialOrder                = errors.Template("Unknown monomial order (%s).")
	UnknownSliceType                    = errors.Template("Unknown slice type.")
)
