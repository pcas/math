// Tests for table.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package errors

import (
	"bitbucket.org/pcastools/errors"
	"github.com/stretchr/testify/require"
	"testing"
)

// iser is the interface we expect our templates to conform to.
type iser interface {
	Is(e error) bool // Is returns true iff the given error e was created from the template p.
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestTemplate tests that the imported errors.Template satisfies the iser interface, and creates errors (which is the behaviour that we expect).
func TestTemplate(t *testing.T) {
	require := require.New(t)
	template := errors.Template("This is an error.")
	require.Implements((*iser)(nil), template)
	e := template.New()
	require.Error(e)
	require.True(template.Is(e))
	secondTemplate := errors.Template("This is another error.")
	require.False(secondTemplate.Is(e))
}
