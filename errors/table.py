#!/usr/bin/python

### This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see http://creativecommons.org/publicdomain/zero/1.0/

import string
import sys

# Output the usage message to stderr, then exits with a non-zero exit code.
def helpmessage():
    name = sys.argv[0]
    sys.stderr.write("""Usage: """ + name + """ src.txt
Output will be written to \"src.go\".
""")
    sys.exit(1)

# Outputs the given message to stderr, then exits with a non-zero exit code.
def fatalmessage(s):
    name = sys.argv[0]
    sys.stderr.write(name + ': ' + s + '\n')
    sys.exit(1)

# Validate the argument
args = sys.argv
if len(args) != 2:
    helpmessage()
src = string.strip(args[1])
if len(src) == 0 or src[0] == '-':
    helpmessage()
if len(src) <= 4 or src[-4:] != '.txt':
    fatalmessage('Argument must have the suffix \".txt\".')

# Open the input file
inf = open(src)

# Outputs the given message to stderr, prefixed with the given line number, then exits with a non-zero exit code.
def fatalline(linenum,s):
    name = sys.argv[0]
    sys.stderr.write(name + ': line ' + str(linenum) + ': ' + s + '\n')
    sys.exit(1)

# Returns true iff the string s contains white space.
def hasSpace(s):
    for x in s:
        if x in string.whitespace:
            return True
    return False

# Returns true iff the string s contains only the characters a-zA-Z0-9.
def areLetters(s):
    for x in s:
        if not (x in string.letters or x in string.digits):
            return False
    return True

# Returns the first index of a row in S with name n, or -1.
def nameIndex(S,n):
    for i in range(len(S)):
        if S[i][0] == n:
            return i
    return -1

# Iterating through the lines of the file building the data we need to output.
linenum = 0
data = []
for line in inf.readlines():
    # Increment the line number
    linenum = linenum + 1
    # Trim any space from the left of the line
    line = string.lstrip(line)
    # Skip over comment lines and empty lines
    if len(line) == 0 or line[0] == '#':
        continue
    # Split the line into [name, message] strings
    row = [string.strip(s) for s in string.split(line, sep=':', maxsplit=1)]
    if len(row) == 1:
        fatalline(linenum,'Not of the form \"name: message\".')
    # The name should be non-empty
    if len(row[0]) == 0:
        fatalline(linenum,'The name must be non-empty.')
    # The name should contain no white space
    if hasSpace(row[0]):
        fatalline(linenum,'The name (' + row[0] + ') must not contain space.')
    # The name should only contain the characters a-zA-Z0-9
    if not areLetters(row[0]):
        fatalline(linenum,'The name (' + row[0] + ') must contain only the characters a-zA-Z0-9.')
    # The first character of name should be in A-Z
    if not (row[0][0] in string.uppercase):
        fatalline(linenum,'The first character of name (' + row[0] + ') must be in A-Z.')
    # Check that the name is unique
    idx = nameIndex(data,row[0])
    if idx != -1:
        fatalline(linenum,row[0] + ' already defined on line ' + str(data[idx][2]) + '.')
    # Add this to the data
    row.append(linenum)
    data.append(row)

# Drop the line numbers and sort the data
data = [row[:-1] for row in data]
data.sort()

# Calculate the longest variable name
maxLen = max([len(row[0]) for row in data])

# Returns the padding needed to make the string s of length l
def padding(s,l):
    diff = l - len(s)
    if diff > 0:
        return string.join([' ' for i in range(diff)],sep='')
    return ''

# Open the output file
dst = src[:-3] + 'go'
outf = open(dst,'w')

# Output the header
outf.write('//go:generate ./table.py ' + src + """
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from \"""" + src + """\".
To modify this file, edit \"""" + src + """\" and run \"go generate\".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/errors
/////////////////////////////////////////////////////////////////////////
// """ + dst + """
/////////////////////////////////////////////////////////////////////////

package errors

import (
\t\"bitbucket.org/pcastools/errors\"
)

// The error templates.
const (
""")

# Write out the data
for row in data:
    outf.write('\t' + row[0] + padding(row[0],maxLen) + ' = errors.Template(\"' + row[1] + '\")\n')

# Output the footer
outf.write(')\n')