//go:generate ../../internal/template/template.py ../../internal/template/elementbasic.txt elementbasic.go "package=integermatrixspace" "dir=integermatrix" "description=integer entries" "entryPackage=bitbucket.org/pcas/math/integer" "entryElement=*integer.Element" "entryParent=*integer.Parent"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/elementbasic.txt".
To modify this file, edit "../../internal/template/elementbasic.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Elementbasic defines basic functions for working with matrices with integer entries.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/integermatrix/integermatrixspace
/////////////////////////////////////////////////////////////////////////
// elementbasic.go
/////////////////////////////////////////////////////////////////////////
/*
Requires the functions:

// defaultParent returns the parent of the nil element.
func defaultParent() *Parent {
    ...
}

// inverseMatrix returns the inverse of the matrix x, or an error if x is not invertible. Assumes that x is a non-nill square matrix. This does NOT cache the inverse on x.
func inverseMatrix(x *Element) (*Element, error) {
    ...
}

// computeDeterminant computes and returns the determinant of the matrix x. Assumes that x is a non-nill square matrix. This does NOT cache the determinant on x.
func computeDeterminant(x *Element) (*integer.Element, error) {
	...
}
*/

package integermatrixspace

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"math"
	"sync"
)

// Element is a matrix with integer entries.
type Element struct {
	parent *Parent           // The parent for this element
	es     entries.Interface // The entries describing this element
	m      sync.Mutex        // Mutex controlling access to the following data
	det    object.Element    // The determinant (if known)
	trans  *Element          // The transpose (if set)
	inv    *Element          // The inverse (if known)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// parentOfElement returns the parent of the matrix x.
func parentOfElement(x *Element) *Parent {
	if x == nil {
		return defaultParent()
	}
	return x.parent
}

// isParent returns true iff R is the parent of the matrix x.
func isParent(R *Parent, x *Element) bool {
	if R == nil {
		R = defaultParent()
	}
	return parentOfElement(x) == R
}

// createZero creates a new zero matrix for the given parent R. It does not assign zero to the parent.
func createZero(R *Parent) *Element {
	if R == nil {
		R = defaultParent()
	}
	S, err := entries.ZeroSlice(R.entriesHelper(), R.NumberOfRows(), R.NumberOfColumns())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	x := &Element{
		parent: R,
		es:     S,
	}
	if x.IsSquare() {
		x.det = R.Ring().Zero()
		x.trans = x
	}
	return x
}

// createOne creates a new identity matrix for the given parent R. It does not assign one to the parent. This will panic if R is not a space of n x n (i.e. square) matrices.
func createOne(R *Parent) *Element {
	if R == nil {
		R = defaultParent()
	}
	n := R.NumberOfRows()
	if R.NumberOfColumns() != n {
		panic(errors.MatrixNotSquare.New())
	}
	S, err := entries.IdentitySlice(R.entriesHelper(), n)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	x := &Element{
		parent: R,
		es:     S,
		det:    R.Ring().One(),
	}
	x.trans = x
	x.inv = x
	return x
}

// fromSlice returns a matrix in R with slice of entries determined by S. The entries should lie in the ring of entries of R.
func fromSlice(R *Parent, S slice.Interface) (*Element, error) {
	if R == nil {
		R = defaultParent()
	}
	nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
	if size := nrows * ncols; S.Len() != size {
		return nil, errors.SliceLengthNotEqualTo.New(S.Len(), size)
	}
	T, err := R.entriesHelper().FromSlice(S, nrows, ncols)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, T), nil
}

// quickIsZero will only returns true if x is the zero matrix, but may incorrectly return false when x is the zero matrix. If you need an iff, call x.IsZero() instead.
func quickIsZero(x *Element) bool {
	return x == nil || x == x.parent.zero
}

// quickIsOne will only returns true if x is the identity matrix, but may incorrectly return false when x is the identity matrix. If you need an iff, call x.IsOne() instead.
func quickIsOne(x *Element) bool {
	return x == nil || x == x.parent.one
}

// scalarMultiplyByCoefficient returns the product c * x of the matrix x with c, where c is an element in the ring containing the entries of the matrix.
func scalarMultiplyByCoefficient(c object.Element, x *Element) (*Element, error) {
	// Ensure that c is an element of the ring
	R := parentOfElement(x)
	K := R.Ring()
	cc, err := K.ToElement(c)
	if err != nil {
		return nil, err
	}
	// Fast-track the easy cases
	if quickIsZero(x) {
		return Zero(R), nil
	} else if isZero, err := K.IsZero(cc); err != nil {
		return nil, err
	} else if isZero {
		return Zero(R), nil
	} else if isOne, err := K.IsOne(cc); err != nil {
		return nil, err
	} else if isOne {
		return x, nil
	}
	// Calculate the multiple
	S, err := entries.ScalarMultiplyByCoefficient(R.entriesHelper(), cc, x.es)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, S), nil
}

// getDeterminant returns the determinant of the matrix x (if x is square).
func getDeterminant(x *Element) (*integer.Element, error) {
	// Is the matrix square?
	if !x.IsSquare() {
		return nil, errors.MatrixNotSquare.New()
	}
	// Get the zero case out of the way
	if quickIsZero(x) {
		return parentOfElement(x).Ring().Zero().(*integer.Element), nil
	}
	// Do we know the answer?
	var d *integer.Element
	x.m.Lock()
	if x.det != nil {
		d = x.det.(*integer.Element)
	}
	x.m.Unlock()
	// If we don't know the answer, we need to compute it
	if d == nil {
		// Compute the determinant
		var err error
		if d, err = computeDeterminant(x); err != nil {
			return nil, err
		}
		setDeterminant(x, d)
	}
	return d, nil
}

// setDeterminant sets the determinant of x (if not already set), and propagates this data out to the cached matrices.
func setDeterminant(x *Element, d object.Element) {
	// Get the nil case out of the way
	if x == nil {
		return
	}
	// Set the determinant and note if we know the transpose or inverse
	var trans, inv *Element
	x.m.Lock()
	if x.det == nil {
		x.det = d
		if x.trans != nil {
			trans = x.trans
		}
		if x.inv != nil {
			inv = x.inv
		}
	}
	x.m.Unlock()
	// If we know the transpose, set the determinant
	if trans != nil && trans != x {
		setDeterminant(trans, d)
	}
	// If we know the inverse, set the determinant
	if inv != nil && inv != x && inv != trans {
		R := x.es.Universe()
		if RR, ok := R.(ring.IsUniter); ok {
			if hasInverse, dinv, err := RR.IsUnit(d); err == nil && hasInverse {
				setDeterminant(inv, dinv)
			}
		} else if isOne, err := R.IsOne(d); err == nil && isOne {
			setDeterminant(inv, d)
		} else if minusd, err := R.Negate(d); err == nil {
			if isMinusOne, err := R.IsOne(minusd); err == nil && isMinusOne {
				setDeterminant(inv, d)
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Ring returns the ring containing the entries of matrices in R.
func Ring(R *Parent) *integer.Parent {
	return R.Ring().(*integer.Parent)
}

// Zero returns the zero matrix of R.
func Zero(R *Parent) *Element {
	if R == nil {
		R = defaultParent()
	}
	return R.zero
}

// AreEqual returns true iff the matrices x and y have the same parent, and x = y.
func AreEqual(x *Element, y *Element) bool {
	// Get the easy checks out of the way
	if x == y {
		return true
	}
	R := parentOfElement(x)
	if parentOfElement(y) != R {
		return false
	} else if quickIsZero(x) {
		return y.IsZero()
	} else if quickIsZero(y) {
		return x.IsZero()
	}
	// Perform the check
	areEqual, err := entries.AreEqual(R.entriesHelper(), x.es, y.es)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return areEqual
}

// Add returns the sum x + y of two matrices x and y. This will return an error if x and y do not have the same parent.
func Add(x *Element, y *Element) (*Element, error) {
	// Check the parents agree
	R := parentOfElement(x)
	if !isParent(R, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the easy cases
	if x == y {
		return x.ScalarMultiplyByInt64(2), nil
	} else if quickIsZero(x) {
		return y, nil
	} else if quickIsZero(y) {
		return x, nil
	}
	// Perform the sum
	S, err := entries.Add(R.entriesHelper(), x.es, y.es)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, S), nil
}

// Subtract returns the difference x - y of two matrices x and y. This will return an error if x and y do not have the same parent.
func Subtract(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	R := parentOfElement(x)
	if !isParent(R, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the easy cases
	if x == y {
		return Zero(R), nil
	} else if quickIsZero(x) {
		return Negate(y), nil
	} else if quickIsZero(y) {
		return x, nil
	}
	// Perform the subtraction
	S, err := entries.Subtract(R.entriesHelper(), x.es, y.es)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, S), nil
}

// Sum returns the sum of the matrices in the slice S. All matrices must have the same parent R. If S is empty, the zero element in R is returned.
func Sum(R *Parent, S ...*Element) (*Element, error) {
	// If we've been passed the nil parent, move to the default parent
	if R == nil {
		R = defaultParent()
	}
	// Fast-track the length 0, 1, and 2 cases
	n := len(S)
	if n == 0 {
		return Zero(R), nil
	} else if n == 1 {
		if !isParent(R, S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return S[0], nil
	} else if n == 2 {
		if !isParent(R, S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return Add(S[0], S[1])
	}
	// Create the slices of entries
	Es := make([]entries.Interface, 0, n)
	for _, x := range S {
		if !isParent(R, x) {
			return nil, errors.ArgNotContainedInParent.New()
		} else if !quickIsZero(x) {
			Es = append(Es, x.es)
		}
	}
	// Is there anything to do?
	if len(Es) == 0 {
		return Zero(R), nil
	}
	// Perform the sum
	es, err := entries.Sum(R.entriesHelper(), Es)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, es), nil
}

// Negate returns the negation -x of the matrix x.
func Negate(x *Element) *Element {
	// Fast-track the zero case
	R := parentOfElement(x)
	if quickIsZero(x) {
		return Zero(R)
	}
	// Negate the entries
	S, err := entries.Negate(R.entriesHelper(), x.es)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return createMatrix(R, S)
}

// Power returns x^k.
func Power(x *Element, k *integer.Element) (*Element, error) {
	// Sanity check
	if !x.IsSquare() {
		return nil, errors.MatrixNotSquare.New()
	}
	// Handle the easy cases
	if k.IsZero() {
		return oneOrPanic(parentOfElement(x)), nil
	} else if k.IsOne() || x.IsOne() {
		return x, nil
	} else if x.IsZero() {
		if k.IsPositive() {
			return Zero(parentOfElement(x)), nil
		}
		return nil, errors.DivisionByZero.New()
	}
	// Convert k to an int64 and calculate the power
	kk, err := k.Int64()
	if err != nil {
		return nil, errors.ExponentTooLarge.New()
	}
	return PowerInt64(x, kk)
}

// PowerInt64 returns x^k.
func PowerInt64(x *Element, k int64) (*Element, error) {
	// Sanity check
	if !x.IsSquare() {
		return nil, errors.MatrixNotSquare.New()
	}
	// Handle the easy cases
	R := parentOfElement(x)
	if k == 0 {
		return oneOrPanic(R), nil
	} else if k == 1 || x.IsOne() {
		return x, nil
	}
	// If k is negative we need to compute the inverse of x
	if k < 0 {
		// Check for division by zero
		if x.IsZero() {
			return nil, errors.DivisionByZero.New()
		}
		// Replace x with its inverse (if it exists)
		var err error
		if x, err = x.Inverse(); err != nil {
			return nil, err
		}
		// Replace k with the absolute value
		if k == math.MinInt64 {
			return nil, errors.ExponentTooLarge.New()
		}
		k = -k
	}
	// From here onwards k is (strictly) positive -- handle the zero case
	if x.IsZero() {
		return Zero(R), nil
	}
	// Compute the power via successive multiplication
	h := R.entriesHelper()
	S, T := x.es, x.Transpose().es
	for i := int64(1); i < k; i++ {
		var err error
		if S, err = entries.RowRowMultiply(h, S, T); err != nil {
			return nil, err
		}
	}
	return createMatrix(R, S), nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Len returns the total number of entries in the matrix x (i.e. the product of the number of rows and the number of columns).
func (x *Element) Len() int {
	if x == nil {
		return 0
	}
	return x.es.Len()
}

// IsZero returns true iff x is the zero matrix.
func (x *Element) IsZero() bool {
	return quickIsZero(x) || entries.IsZero(x.es)
}

// IsOne returns true iff x is the (square) identity matrix.
func (x *Element) IsOne() bool {
	return quickIsOne(x) || entries.IsIdentity(x.es)
}

// IsEqualTo returns true iff the matrices x and y have the same parent, and x = y.
func (x *Element) IsEqualTo(y *Element) bool {
	return AreEqual(x, y)
}

// Negate returns the negation -x of the matrix x.
func (x *Element) Negate() *Element {
	return Negate(x)
}

// Power returns x^k.
func (x *Element) Power(k *integer.Element) (*Element, error) {
	return Power(x, k)
}

// PowerInt64 returns x^k.
func (x *Element) PowerInt64(k int64) (*Element, error) {
	return PowerInt64(x, k)
}

// Inverse returns the inverse of the matrix x, if x is invertible.
func (x *Element) Inverse() (*Element, error) {
	// Is the matrix square?
	if !x.IsSquare() {
		return nil, errors.MatrixNotSquare.New()
	}
	// Get the zero and one case out of the way
	if quickIsZero(x) {
		return nil, errors.DivisionByZero.New()
	} else if quickIsOne(x) {
		return oneOrPanic(parentOfElement(x)), nil
	}
	// Do we know the answer?
	var y *Element
	x.m.Lock()
	if x.inv != nil {
		y = x.inv
	}
	x.m.Unlock()
	if y != nil {
		return y, nil
	}
	// We need to compute the inverse
	var err error
	if y, err = inverseMatrix(x); err != nil {
		return nil, err
	}
	// Cache the inverse on x and return
	x.m.Lock()
	if x.inv == nil {
		x.inv = y
	} else {
		y = x.inv
	}
	x.m.Unlock()
	return y, nil
}

// ScalarMultiplyByInt64 returns the scalar multiple n * x of the matrix x with the integer n, where this is defined to be x + ... + x (n times) if n is positive, -x - ... - x (|n| times) if n is negative, and 0 if n is zero.
func (x *Element) ScalarMultiplyByInt64(n int64) *Element {
	// Get the easy cases out of the way
	if n == 1 {
		return x
	} else if n == 0 || quickIsZero(x) {
		return Zero(parentOfElement(x))
	} else if n == -1 {
		return Negate(x)
	}
	// Calculate the scalar multiple
	R := parentOfElement(x)
	S, err := entries.ScalarMultiplyByInteger(R.entriesHelper(), integer.FromInt64(n), x.es)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return createMatrix(R, S)
}

// ScalarMultiplyByInteger returns the scalar multiple n * x of the matrix x with the integer n, where this is defined to be x + ... + x (n times) if n is positive, -x - ... - x (|n| times) if n is negative, and 0 if n is zero.
func (x *Element) ScalarMultiplyByInteger(n *integer.Element) *Element {
	// Get the easy cases out of the way
	if n.IsOne() {
		return x
	} else if n.IsZero() || quickIsZero(x) {
		return Zero(parentOfElement(x))
	} else if n.IsEqualToInt64(-1) {
		return Negate(x)
	}
	// Calculate the scalar multiple
	R := parentOfElement(x)
	S, err := entries.ScalarMultiplyByInteger(R.entriesHelper(), n, x.es)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return createMatrix(R, S)
}

// ScalarMultiplyByCoefficient returns c * x, where c is an element in the ring containing the entries of the matrices in R.
func (x *Element) ScalarMultiplyByCoefficient(c *integer.Element) (*Element, error) {
	return scalarMultiplyByCoefficient(c, x)
}

// Entries returns the entries of the matrix x, row by row.
func (x *Element) Entries() []*integer.Element {
	// Get the zero case out of the way
	if quickIsZero(x) {
		size := x.Len()
		zero := parentOfElement(x).Ring().Zero().(*integer.Element)
		entries := make([]*integer.Element, 0, size)
		for i := 0; i < size; i++ {
			entries = append(entries, zero)
		}
		return entries
	}
	// Recover the entries
	entries := slice.ToElementSlice(x.es)
	// Convert the element types
	entriescpy := make([]*integer.Element, 0, len(entries))
	for _, c := range entries {
		entriescpy = append(entriescpy, c.(*integer.Element))
	}
	return entriescpy
}

// Row returns the i-th row of the matrix x. Rows are indexed from 0. It returns an error if the index i is out of range.
func (x *Element) Row(i int) ([]*integer.Element, error) {
	// Get the zero case out of the way
	if quickIsZero(x) {
		// Sanity check
		if i < 0 || i >= x.NumberOfRows() {
			return nil, errors.InvalidIndexRange.New(i, x.NumberOfRows()-1)
		}
		// Create the zero row
		ncols := x.NumberOfColumns()
		zero := parentOfElement(x).Ring().Zero().(*integer.Element)
		row := make([]*integer.Element, 0, ncols)
		for i := 0; i < ncols; i++ {
			row = append(row, zero)
		}
		return row, nil
	}
	// Recover the row
	row, err := entries.Row(i, x.es)
	if err != nil {
		return nil, err
	}
	// Convert the element types
	rowcpy := make([]*integer.Element, 0, len(row))
	for _, c := range row {
		rowcpy = append(rowcpy, c.(*integer.Element))
	}
	return rowcpy, nil
}

// Rows returns the rows of the matrix x.
func (x *Element) Rows() [][]*integer.Element {
	nrows := x.NumberOfRows()
	T := make([][]*integer.Element, 0, nrows)
	for i := 0; i < nrows; i++ {
		S, err := x.Row(i)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		T = append(T, S)
	}
	return T
}

// Column returns the i-th column of the matrix x. Columns are indexed from 0. It returns an error if the index i is out of range.
func (x *Element) Column(i int) ([]*integer.Element, error) {
	// Get the zero case out of the way
	if quickIsZero(x) {
		// Sanity check
		if i < 0 || i >= x.NumberOfColumns() {
			return nil, errors.InvalidIndexRange.New(i, x.NumberOfColumns()-1)
		}
		// Create the zero column
		nrows := x.NumberOfRows()
		zero := parentOfElement(x).Ring().Zero().(*integer.Element)
		col := make([]*integer.Element, 0, nrows)
		for i := 0; i < nrows; i++ {
			col = append(col, zero)
		}
		return col, nil
	}
	// If we know the transpose, then we'll return the transpose's row
	var trans *Element
	x.m.Lock()
	trans = x.trans
	x.m.Unlock()
	if trans != nil {
		return trans.Row(i)
	}
	// Recover the column
	col, err := entries.Column(i, x.es)
	if err != nil {
		return nil, err
	}
	// Convert the element types
	colcpy := make([]*integer.Element, 0, len(col))
	for _, c := range col {
		colcpy = append(colcpy, c.(*integer.Element))
	}
	return colcpy, nil
}

// Columns returns the columns of the matrix x.
func (x *Element) Columns() [][]*integer.Element {
	ncols := x.NumberOfColumns()
	T := make([][]*integer.Element, 0, ncols)
	for i := 0; i < ncols; i++ {
		S, err := x.Column(i)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		T = append(T, S)
	}
	return T
}

// FlatEntry returns the i-th entry of the matrix x, read row by row. Flat entries are indexed from zero up to (but not including) x.Len(). It returns an error if the index i is out of range.
func (x *Element) FlatEntry(i int) (*integer.Element, error) {
	if size := x.Len(); i < 0 || i >= size {
		return nil, errors.InvalidIndexRange.New(i, size-1)
	}
	return x.FlatEntryOrPanic(i), nil
}

// FlatEntryOrPanic returns the i-th entry of the matrix x, read row by row. Flat entries are indexed from zero up to (but not including) x.Len(). It panics if the index is out of range.
func (x *Element) FlatEntryOrPanic(i int) *integer.Element {
	return x.es.Entry(i).(*integer.Element)
}

// Entry returns the (i,j)-th entry of the matrix x. Rows and columns are indexed from zero. It returns an error if either i or j is negative or too large (i.e. if i and j refer to a non-existent entry of x).
func (x *Element) Entry(i int, j int) (*integer.Element, error) {
	nrows, ncols := x.NumberOfRows(), x.NumberOfColumns()
	if i < 0 || j < 0 || i >= nrows || j >= ncols {
		return nil, errors.InvalidBiIndexRange.New(i, j, nrows-1, ncols-1)
	}
	return x.FlatEntryOrPanic(i*ncols + j), nil
}

// EntryOrPanic returns the (i,j)-th entry of the matrix x. Rows and columns are indexed from zero. It panics error if either i or j is negative or too large (i.e. if i and j refer to a non-existent entry of x).
func (x *Element) EntryOrPanic(i int, j int) *integer.Element {
	c, err := x.Entry(i, j)
	if err != nil {
		panic(err)
	}
	return c
}

// NumberOfRows returns the number of rows of the matrix x.
func (x *Element) NumberOfRows() int {
	if x == nil {
		return parentOfElement(x).NumberOfRows()
	}
	return x.es.NumberOfRows()
}

// NumberOfColumns returns the number of columns of the matrix x.
func (x *Element) NumberOfColumns() int {
	if x == nil {
		return parentOfElement(x).NumberOfColumns()
	}
	return x.es.NumberOfColumns()
}

// Hash returns a hash value for the matrix x.
func (x *Element) Hash() hash.Value {
	if x.IsZero() {
		return 0
	}
	return slice.Hash(x.es)
}

// Parent returns the parent of the matrix x.
func (x *Element) Parent() object.Parent {
	return parentOfElement(x)
}

// String returns a string representation of the matrix x.
func (x *Element) String() string {
	if x == nil {
		x = Zero(parentOfElement(x))
	}
	return entries.ToString(x.es)
}
