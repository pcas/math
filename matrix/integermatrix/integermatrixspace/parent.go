//go:generate ../../internal/template/template.py ../../internal/template/matrixspace/parent.txt parent.go "package=integermatrixspace" "dir=integermatrix" "description=integer entries"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/matrixspace/parent.txt".
To modify this file, edit "../../internal/template/matrixspace/parent.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Parent defines the parent matrix space for matrices with integer entries.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/integermatrix/integermatrixspace
/////////////////////////////////////////////////////////////////////////
// parent.go
/////////////////////////////////////////////////////////////////////////

package integermatrixspace

import (
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"strconv"
)

// Parent is the parent matrix space for matrices with integer entries.
type Parent struct {
	h     entries.Helper // The helper for the entries
	nrows int            // The number of rows
	ncols int            // The number of columns
	zero  *Element       // The additive identity element
	one   *Element       // The multiplicative identity element (if square)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createParent returns a new matrix space for nrows x ncols matrices, using the given entries helper.
func createParent(h entries.Helper, nrows int, ncols int) (*Parent, error) {
	// Sanity check
	if err := entries.ValidateSize(nrows, ncols); err != nil {
		return nil, err
	}
	// Create and return the parent
	R := &Parent{
		h:     h,
		nrows: nrows,
		ncols: ncols,
	}
	R.zero = createZero(R)
	if nrows == ncols {
		R.one = createOne(R)
	}
	return R, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Determinant returns the determinant of the matrix x (if x is square). This is given as an element in the ring of entries.
func (R *Parent) Determinant(x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Determinant()
}

// NumberOfRows returns the number of rows of a matrix in R.
func (R *Parent) NumberOfRows() int {
	if R == nil {
		R = defaultParent()
	}
	return R.nrows
}

// NumberOfColumns returns the number of columns of a matrix in R.
func (R *Parent) NumberOfColumns() int {
	if R == nil {
		R = defaultParent()
	}
	return R.ncols
}

// String returns a string description of this parent.
func (R *Parent) String() string {
	return "MatrixSpace(" + R.Ring().String() + ", " + strconv.Itoa(R.NumberOfRows()) + ", " + strconv.Itoa(R.NumberOfColumns()) + ")"
}
