// Creation creates the space of n x m integer-valued matrices.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixspace

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/integermatrix/internal/integerentries"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// The cached Parents and a mutex controlling access.
var (
	cache map[[2]int]*Parent
	mutex sync.RWMutex
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of parents
	cache = make(map[[2]int]*Parent)
}

// defaultParent returns the parent of the nil element.
func defaultParent() *Parent {
	R, err := NewSpace(0, 0)
	if err != nil {
		panic(fatal.ImpossibleError(err))
	}
	return R
}

// parentOfTranspose returns the parent that contains the transposes of matrices in R (i.e. the parent of Transpose(x), where x is a matrix in R).
func parentOfTranspose(R *Parent) *Parent {
	// Is there anything to do?
	nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
	if nrows == ncols {
		return R
	}
	// Create and return the space
	T, err := NewSpace(ncols, nrows)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return T
}

// parentOfProduct returns the parent that contains the product of matrices in R1 and R2 (i.e. the parent of x1 * x2, where x1 is a matrix in R1, and x2 is a matrix in R2). This will panic if R1 and R2 are incompatible.
func parentOfProduct(R1 *Parent, R2 *Parent) *Parent {
	// Sanity check
	nrows1, ncols1 := R1.NumberOfRows(), R1.NumberOfColumns()
	nrows2, ncols2 := R2.NumberOfRows(), R2.NumberOfColumns()
	if ncols1 != nrows2 {
		panic(errors.MatricesOfIncompatibleSizes.New(nrows1, ncols1, nrows2, ncols2))
	}
	// Is there anything to do?
	if ncols1 == ncols2 {
		return R1
	} else if nrows1 == nrows2 {
		return R2
	}
	// Create and return the space
	T, err := NewSpace(nrows1, ncols2)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewSpace returns the space of n x m matrices with entries in ZZ.
func NewSpace(n int, m int) (*Parent, error) {
	// Aquire a read lock on the mutex and check the cache
	k := [2]int{n, m}
	mutex.RLock()
	if R, ok := cache[k]; ok {
		// The Parent is in the cache so return it
		mutex.RUnlock()
		return R, nil
	}
	// No luck -- release the read lock and create the new Parent
	mutex.RUnlock()
	R, err := createParent(defaultEntriesHelper(), n, m)
	if err != nil {
		return nil, err
	}
	// Acquire a write lock and check the cache once again before returning
	mutex.Lock()
	if RR, ok := cache[k]; ok {
		R = RR
	} else {
		cache[k] = R
	}
	// Release the write lock and return the Parent
	mutex.Unlock()
	return R, nil
}

// FromSlice returns a matrix in the matrix space R, with entries S. The entries are read row-by-row.
func FromSlice(R *Parent, S []*integer.Element) (*Element, error) {
	return fromSlice(R, integer.Slice(S))
}
