// Int64slice defines the slice of int64 entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
)

// int64slice is a slice of int64 entries of a matrix.
type int64slice struct {
	nrows int     // The number of rows
	ncols int     // The number of columns
	es    []int64 // The underlying entries of int64s
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createInt64Slice returns a new slice of entries from the given slice of int64s, with the given number of rows and columns.
func createInt64Slice(S []int64, nrows int, ncols int) entries.Interface {
	// Sanity check
	if nrows < 0 || ncols < 0 {
		panic(errors.IllegalMatrixSize.New(nrows, ncols))
	} else if len(S) != nrows*ncols {
		panic(errors.SliceLengthNotEqualTo.New(len(S), nrows*ncols))
	}
	// Return the slice
	return &int64slice{
		nrows: ncols,
		ncols: nrows,
		es:    S,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the common parent of the entries in the slice.
func (S *int64slice) Universe() ring.Interface {
	return integer.Ring()
}

// Len returns the length of the slice.
func (S *int64slice) Len() int {
	if S == nil {
		return 0
	}
	return len(S.es)
}

// NumberOfRows returns the number of rows of the matrix represented by the  entries in the slice.
func (S *int64slice) NumberOfRows() int {
	if S == nil {
		return 0
	}
	return S.nrows
}

// NumberOfColumns returns the number of columns of the matrix represented by the entries in the slice.
func (S *int64slice) NumberOfColumns() int {
	if S == nil {
		return 0
	}
	return S.ncols
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *int64slice) Entry(i int) object.Element {
	return integer.FromInt64(S.es[i])
}

// Row returns (a copy of) the i-th row. Rows are indexed from 0. This will panic if the index is out of range.
func (S *int64slice) Row(i int) []object.Element {
	rawrow := S.rawRow(i)
	row := make([]object.Element, 0, len(rawrow))
	for _, c := range rawrow {
		row = append(row, integer.FromInt64(c))
	}
	return row
}

// rawRow returns the raw underlying data for the i-th row. This will panic if i is out of range.
func (S *int64slice) rawRow(i int) []int64 {
	ncols := S.NumberOfColumns()
	offset := ncols * i
	return S.es[offset : offset+ncols]
}

// Column returns (a copy of) the i-th column. Columns are indexed from 0. This will panic if the index is out of range.
func (S *int64slice) Column(i int) []object.Element {
	nrows, ncols := S.NumberOfRows(), S.NumberOfColumns()
	col := make([]object.Element, 0, nrows)
	for j := 0; j < nrows; j++ {
		col = append(col, integer.FromInt64(S.es[ncols*j+i]))
	}
	return col
}

// IsZero returns true iff the matrix defined by the slice is the zero matrix.
func (S *int64slice) IsZero() bool {
	if S == nil {
		return true
	}
	for _, c := range S.es {
		if c != 0 {
			return false
		}
	}
	return true
}

// IsIdentity returns true iff the matrix defined by the slice is the zero matrix.
func (S *int64slice) IsIdentity() bool {
	if S == nil {
		return true
	}
	ncols := S.NumberOfColumns()
	if ncols != S.NumberOfRows() {
		return false
	}
	idx := 0
	for i, c := range S.es {
		if i == idx {
			if c != 1 {
				return false
			}
			idx += ncols + 1
		} else if c != 0 {
			return false
		}
	}
	return true
}

// Transpose returns the transpose of the matrix described by the slice of entries.
func (S *int64slice) Transpose() entries.Interface {
	// Is there anything to do?
	if S == nil {
		return S
	}
	// Create the storage for the transpose
	nrows, ncols := S.NumberOfRows(), S.NumberOfColumns()
	es := make([]int64, 0, nrows*ncols)
	// Start working through the columns of S
	col := make([]int64, nrows)
	for i := 0; i < ncols; i++ {
		// Extract the column
		for j := 0; j < nrows; j++ {
			col[j] = S.es[j*ncols+i]
		}
		// Append the column as a row
		es = append(es, col...)
	}
	// Return the slice
	return &int64slice{
		nrows: ncols,
		ncols: nrows,
		es:    es,
	}
}
