// Helper defines the helper for matrices with integer entries.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/worker"
)

// helper is the helper for matrices with integer entries.
type helper struct{}

// defaultHelper is the default helper instance.
var defaultHelper = helper{}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the common parent of the entries.
func (_ helper) Universe() ring.Interface {
	return integer.Ring()
}

// DotProduct returns a dot-product function to compute S1[i] \dot S2[j] of the i-th row of S1 with the j-th row of S2, where S1 and S2 are slices of entries. S1 and S2 must have the same number of columns.
func (_ helper) DotProduct(S1 entries.Interface, S2 entries.Interface) (entries.DotProductFunc, error) {
	// Sanity check
	ncols := S1.NumberOfColumns()
	if ncols != S2.NumberOfColumns() {
		return nil, errors.NumberOfColumnsOfMatricesDoNotAgree.New(ncols, S2.NumberOfColumns())
	}
	// Convert the arguments to the expected underlying types -- this will
	// cause a runtime panic if something is wrong
	s1, s2 := S1.(*integerslice), S2.(*integerslice)
	// Return the dot-product function
	return func(i int, j int) (object.Element, error) {
		// Sanity check
		if i < 0 || i >= ncols {
			return nil, errors.InvalidIndexRange.New(i, ncols-1)
		} else if j < 0 || j >= ncols {
			return nil, errors.InvalidIndexRange.New(j, ncols-1)
		}
		// Return the dot-product
		return integer.DotProduct(s1.RawRow(i), s2.RawRow(j)), nil
	}, nil
}

// RowRowMultiply returns the slice of entries of pair-wise dot-products of the rows of S1 and S2. That is, the returned slice has (i,j)-th entry equal to the dot-product of the i-th row of S1 with the j-th row of S2. This is equivalent to the matrix multiplication represented by S1 * Transpose(S2). Requires that S1 and S2 have the same number of columns. The returned slice will represent an nrows1 x nrow2 matrix, where nrows1 is the number of rows of S1, and nrows2 is the number of rows of S2.
func (_ helper) RowRowMultiply(S1 entries.Interface, S2 entries.Interface) (entries.Interface, error) {
	// Sanity check
	ncols := S1.NumberOfColumns()
	if ncols != S2.NumberOfColumns() {
		return nil, errors.NumberOfColumnsOfMatricesDoNotAgree.New(ncols, S2.NumberOfColumns())
	}
	// Convert the arguments to the expected underlying types -- this will
	// cause a runtime panic if something is wrong
	s1, s2 := S1.(*integerslice), S2.(*integerslice)
	// Allocate storage for the resulting entries
	nrows1, nrows2 := S1.NumberOfRows(), S2.NumberOfRows()
	es := make([]*integer.Element, nrows1*nrows2)
	// Perform the row-row multiplication
	if err := worker.Range(func(i int) error {
		row1 := s1.RawRow(i)
		offset := ncols * i
		for j := 0; j < ncols; j++ {
			es[offset+j] = integer.DotProduct(row1, s2.RawRow(j))
		}
		return nil
	}, 0, nrows1, 1); err != nil {
		return nil, err
	}
	// Return the slice
	return &integerslice{
		nrows: nrows1,
		ncols: nrows2,
		es:    es,
	}, nil
}

// Helper returns the default helper for matrices with integer entries.
func Helper() entries.Helper {
	return defaultHelper
}
