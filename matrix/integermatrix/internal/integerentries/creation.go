// Creation contains local functions used to convert slices of elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integerentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"math"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// integerSliceToInt64Slice attempts to convert the slice S to a slice of int64s. The second return value indicates success or not.
func integerSliceToInt64Slice(S integer.Slice) ([]int64, bool) {
	T := make([]int64, 0, len(S))
	for _, c := range S {
		if !c.IsInt64() {
			return nil, false
		}
		d, err := c.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		T = append(T, d)
	}
	return T, true
}

// fromIntegerSlice returns a slice of entries from the given slice S with the given number of rows and columns.
func fromIntegerSlice(S integer.Slice, nrows int, ncols int) entries.Interface {
	if T, ok := integerSliceToInt64Slice(S); ok {
		return createInt64Slice(T, nrows, ncols)
	}
	return createIntegerSlice(integer.CopySlice(S), nrows, ncols)
}

// fromIntSlice returns a slice of entries from the given slice S with the given number of rows and columns.
func fromIntSlice(S integer.IntSlice, nrows int, ncols int) entries.Interface {
	T := make([]int64, 0, len(S))
	for _, c := range S {
		T = append(T, int64(c))
	}
	return createInt64Slice(T, nrows, ncols)
}

// fromInt32Slice returns a slice of entries from the given slice S with the given number of rows and columns.
func fromInt32Slice(S integer.Int32Slice, nrows int, ncols int) entries.Interface {
	T := make([]int64, 0, len(S))
	for _, c := range S {
		T = append(T, int64(c))
	}
	return createInt64Slice(T, nrows, ncols)
}

// fromInt64Slice returns a slice of entries from the given slice S with the given number of rows and columns.
func fromInt64Slice(S integer.Int64Slice, nrows int, ncols int) entries.Interface {
	T := make([]int64, len(S))
	copy(T, S)
	return createInt64Slice(T, nrows, ncols)
}

// uint64SliceToInt64Slice attempts to convert a slice S of uint64s to a slice of int64s. The second return value indicates success or not.
func uint64SliceToInt64Slice(S integer.Uint64Slice) ([]int64, bool) {
	T := make([]int64, 0, len(S))
	for _, c := range S {
		if c > math.MaxInt64 {
			return nil, false
		}
		T = append(T, int64(c))
	}
	return T, true
}

// fromUint64Slice returns a slice of entries from the given slice S with the given number of rows and columns.
func fromUint64Slice(S integer.Uint64Slice, nrows int, ncols int) entries.Interface {
	if T, ok := uint64SliceToInt64Slice(S); ok {
		return createInt64Slice(T, nrows, ncols)
	}
	T := make([]*integer.Element, 0, len(S))
	for _, c := range S {
		T = append(T, integer.FromUint64(c))
	}
	return createIntegerSlice(T, nrows, ncols)
}

// rationalSliceToInt64Slice attempts to convert the slice S to a slice of int64s. The second return value indicates success or not. If the conversion failed because of an error, this is also returned.
func rationalSliceToInt64Slice(S rational.Slice) ([]int64, bool, error) {
	T := make([]int64, 0, len(S))
	for _, c := range S {
		if !c.IsIntegral() {
			return nil, false, errors.RationalNotAnInteger.New()
		} else if !c.IsInt64() {
			return nil, false, nil
		}
		d, err := c.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		T = append(T, d)
	}
	return T, true, nil
}

// fromRationalSlice returns a slice of entries from the given slice S with the given number of rows and columns. If the elements of S are not all integral, returns an error.
func fromRationalSlice(S rational.Slice, nrows int, ncols int) (entries.Interface, error) {
	if T, ok, err := rationalSliceToInt64Slice(S); ok {
		return createInt64Slice(T, nrows, ncols), nil
	} else if err != nil {
		return nil, err
	}
	T := make([]*integer.Element, 0, len(S))
	for _, c := range S {
		d, err := c.ToInteger()
		if err != nil {
			return nil, err
		}
		T = append(T, d)
	}
	return createIntegerSlice(T, nrows, ncols), nil
}

// genericSliceToInt64Slice attempts to convert the slice S to a slice of int64s. The second return value indicates success or not. If the conversion failed because of an error, this is also returned.
func genericSliceToInt64Slice(S slice.Interface) ([]int64, bool, error) {
	size := S.Len()
	T := make([]int64, 0, size)
	for i := 0; i < size; i++ {
		// How we proceed depends on the type of this element
		switch d := S.Entry(i).(type) {
		case *integer.Element:
			if !d.IsInt64() {
				return nil, false, nil
			}
			n, err := d.Int64()
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			T = append(T, n)
		case *rational.Element:
			if !d.IsIntegral() {
				return nil, false, errors.RationalNotAnInteger.New()
			} else if !d.IsInt64() {
				return nil, false, nil
			}
			n, err := d.Int64()
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			T = append(T, n)
		default:
			k, err := integer.ToElement(d)
			if err != nil {
				return nil, false, err
			} else if !k.IsInt64() {
				return nil, false, nil
			}
			n, err := k.Int64()
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			T = append(T, n)
		}
	}
	return T, true, nil
}

// fromGenericSlice returns a slice of entries from the given slice S with the given number of rows and columns. If the elements of S are not all integral, returns an error.
func fromGenericSlice(S slice.Interface, nrows int, ncols int) (entries.Interface, error) {
	if T, ok, err := genericSliceToInt64Slice(S); ok {
		return createInt64Slice(T, nrows, ncols), nil
	} else if err != nil {
		return nil, err
	}
	size := S.Len()
	T := make([]*integer.Element, 0, size)
	for i := 0; i < size; i++ {
		n, err := integer.ToElement(S.Entry(i))
		if err != nil {
			return nil, err
		}
		T = append(T, n)
	}
	return createIntegerSlice(T, nrows, ncols), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromSlice attempts to create a slice of entries for a matrix of size nrows x ncols, with entries given by the slice S.
func (_ helper) FromSlice(S slice.Interface, nrows int, ncols int) (entries.Interface, error) {
	// Sanity check
	if nrows < 0 || ncols < 0 {
		return nil, errors.IllegalMatrixSize.New(nrows, ncols)
	} else if S.Len() != nrows*ncols {
		return nil, errors.SliceLengthNotEqualTo.New(S.Len(), nrows*ncols)
	}
	// How we create the slice of entries depends on the underlying type of S
	switch T := S.(type) {
	case *integerslice:
		return createIntegerSlice(integer.CopySlice(T.es), nrows, ncols), nil
	case integer.Slice:
		return fromIntegerSlice(T, nrows, ncols), nil
	case integer.IntSlice:
		return fromIntSlice(T, nrows, ncols), nil
	case integer.Int32Slice:
		return fromInt32Slice(T, nrows, ncols), nil
	case *int64slice:
		return fromInt64Slice(integer.Int64Slice(T.es), nrows, ncols), nil
	case integer.Int64Slice:
		return fromInt64Slice(T, nrows, ncols), nil
	case integer.Uint64Slice:
		return fromUint64Slice(T, nrows, ncols), nil
	case rational.Slice:
		return fromRationalSlice(T, nrows, ncols)
	}
	return fromGenericSlice(S, nrows, ncols)
}
