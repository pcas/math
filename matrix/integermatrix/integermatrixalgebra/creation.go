// Creation creates the algebra of n x n integer-valued matrices.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/integermatrix/internal/integerentries"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"sync"
)

// The cached Parents and a mutex controlling access.
var (
	cache map[int]*Parent
	mutex sync.RWMutex
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of parents
	cache = make(map[int]*Parent)
}

// defaultParent returns the parent of the nil element.
func defaultParent() *Parent {
	R, err := NewAlgebra(0)
	if err != nil {
		panic(fatal.ImpossibleError(err))
	}
	return R
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewAlgebra returns the algebra of n x n matrices with entries in ZZ.
func NewAlgebra(n int) (*Parent, error) {
	// Aquire a read lock on the mutex and check the cache
	mutex.RLock()
	if R, ok := cache[n]; ok {
		// The Parent is in the cache so return it
		mutex.RUnlock()
		return R, nil
	}
	// No luck -- release the read lock and create the new Parent
	mutex.RUnlock()
	R, err := createParent(defaultEntriesHelper(), n)
	if err != nil {
		return nil, err
	}
	// Acquire a write lock and check the cache once again before returning
	mutex.Lock()
	if RR, ok := cache[n]; ok {
		R = RR
	} else {
		cache[n] = R
	}
	// Release the write lock and return the Parent
	mutex.Unlock()
	return R, nil
}

// FromSlice returns a matrix in the matrix algebra R, with entries S. The entries are read row-by-row.
func FromSlice(R *Parent, S []*integer.Element) (*Element, error) {
	return fromSlice(R, integer.Slice(S))
}
