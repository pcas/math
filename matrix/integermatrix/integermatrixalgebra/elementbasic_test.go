// Tests for elementbasic.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcastools/hash"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestRing tests Ring.
func TestRing(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 0; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// The coefficient ring should be the integers
		assert.True(Ring(R) == integer.Ring())
	}
	// The coefficient ring of the nil parent should be the integers
	var R *Parent
	assert.True(Ring(R) == integer.Ring())
}

// TestAreEqual tests AreEqual.
func TestAreEqual(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a random matrix
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		x := fromIntSliceOrPanic(R, S)
		// Create a copy of this matrix
		y := fromIntSliceOrPanic(R, S)
		// Create an element different from x (and y)
		S[0]--
		z := fromIntSliceOrPanic(R, S)
		// Assert that x is equal to x and y, but not equal to z
		assert.True(AreEqual(x, x))
		assert.True(AreEqual(x, y))
		assert.True(AreEqual(y, x))
		assert.False(AreEqual(x, z))
		assert.False(AreEqual(z, x))
		// Assert the x is not equal to the nil element
		z = nil
		assert.False(AreEqual(x, z))
		assert.False(AreEqual(z, x))
	}
	// The nil elements should be equal
	var x *Element
	assert.True(AreEqual(x, x))
}

// TestAdd tests Add.
func TestAdd(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(5)
	require.NoError(t, err)
	// Create two random matrices in R, and the zero element
	zero := Zero(R)
	S1 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x1 := fromIntSliceOrPanic(R, S1)
	S2 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x2 := fromIntSliceOrPanic(R, S2)
	// Create the expected sum x1 + x2
	z := fromIntSliceOrPanic(R, addIntSlices(S1, S2))
	// Do some sums
	if y, err := Add(zero, zero); err != nil {
		assert.IsZeroIn(R, y)
	}
	if y, err := Add(x1, zero); err != nil {
		assert.AreEqual(R, x1, y)
	}
	if y, err := Add(zero, x1); err != nil {
		assert.AreEqual(R, x1, y)
	}
	if y, err := Add(x1, x2); err != nil {
		assert.AreEqual(R, y, z)
	}
	// Attempting to add a matrix from a different parent should error
	R2, err := NewAlgebra(4)
	require.NoError(t, err)
	_, err = Add(Zero(R2), x1)
	assert.Error(err)
	_, err = Add(x1, Zero(R2))
	assert.Error(err)
}

// TestSubtract tests Subtract.
func TestSubtract(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(5)
	require.NoError(t, err)
	// Create two random matrices in R, and the zero element
	zero := Zero(R)
	S1 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x1 := fromIntSliceOrPanic(R, S1)
	S2 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x2 := fromIntSliceOrPanic(R, S2)
	// Create the expected difference x1 - x2
	z := fromIntSliceOrPanic(R, subtractIntSlices(S1, S2))
	// Calculate some differences
	if y, err := Subtract(zero, zero); err != nil {
		assert.IsZeroIn(R, y)
	}
	if y, err := Subtract(x1, zero); err != nil {
		assert.AreEqual(R, x1, y)
	}
	if y, err := Subtract(zero, x1); err != nil {
		assert.AreEqual(R, Negate(x1), y)
	}
	if y, err := Subtract(x1, x2); err != nil {
		assert.AreEqual(R, y, z)
	}
	// Attempting to subtract a matrix from a different parent should error
	R2, err := NewAlgebra(4)
	require.NoError(t, err)
	_, err = Subtract(Zero(R2), x1)
	assert.Error(err)
	_, err = Subtract(x1, Zero(R2))
	assert.Error(err)
}

// TestElementSum tests Sum.
func TestElementSum(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(4)
	require.NoError(t, err)
	// The sum of the zero slice should be the zero element
	if x, err := Sum(R); assert.NoError(err) {
		assert.IsZeroIn(R, x)
	}
	// Create random slices of entries and the corresponding matrices
	Ss := make([][]int, 0, 3000)
	xs := make([]*Element, 0, cap(Ss))
	for i := 0; i < cap(Ss); i++ {
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		Ss = append(Ss, S)
		xs = append(xs, fromIntSliceOrPanic(R, S))
	}
	// The sum of a single matrix should be that matrix
	if x, err := Sum(R, xs[0]); assert.NoError(err) {
		assert.AreEqual(R, x, xs[0])
	}
	// The sum of two matrices should be x1 + x2
	if x, err := Sum(R, xs[0], xs[1]); assert.NoError(err) {
		y := fromIntSliceOrPanic(R, addIntSlices(Ss[0], Ss[1]))
		assert.AreEqual(R, x, y)
	}
	// Calculate the sum of the slices
	if x, err := Sum(R, xs...); assert.NoError(err) {
		y := fromIntSliceOrPanic(R, addSliceOfIntSlices(Ss))
		assert.AreEqual(R, x, y)
	}
	// If any of the elements are not in R, this should error
	RR, err := NewAlgebra(5)
	require.NoError(t, err)
	xs[0] = Zero(RR)
	_, err = Sum(R, xs[0])
	assert.Error(err)
	_, err = Sum(R, xs[0], xs[1])
	assert.Error(err)
	_, err = Sum(R, xs...)
	assert.Error(err)
	// A slice of zeros should sum to zero
	zero := Zero(R)
	for i := 0; i < len(xs); i++ {
		xs[i] = zero
	}
	if x, err := Sum(R, xs...); assert.NoError(err) {
		assert.IsZero(x)
	}
}

// TestPowerInt64 tests PowerInt64.
func TestPowerInt64(t *testing.T) {
	assert := assert.New(t)
	// Raising any matrix to the power of 0 should return the identity matrix
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a random matrix x and compute x^0
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		if y, err := PowerInt64(fromIntSliceOrPanic(R, S), 0); assert.NoError(err) {
			assert.IsOneIn(R, y)
		}
	}
	// Raising the identity matrix any power should return the identity matrix
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create the identity matrix
		one := One(R)
		// Raise the identity to various powers
		for k := int64(-100); k < 100; k++ {
			if y, err := PowerInt64(one, k*100); assert.NoError(err) {
				assert.IsOneIn(R, y)
			}
		}
	}
	// Raising the zero matrix any positive power should return zero
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create the zero matrix
		zero := Zero(R)
		// Raise the zero matrix to various powers
		for k := int64(1); k < 100; k++ {
			if y, err := PowerInt64(zero, k*100); assert.NoError(err) {
				assert.IsZeroIn(R, y)
			}
		}
		// Raising the zero matrix to a negative power should error
		if n == 0 {
			if y, err := PowerInt64(zero, -1); assert.NoError(err) {
				assert.IsZeroIn(R, y)
			}
		} else {
			_, err = PowerInt64(zero, -1)
			assert.Error(err)
		}
	}
	// The tests to perform
	tests := []struct {
		n    int   // This will be an n x n matrix
		vals []int // The entries of the matrix x
		k    int64 // The power to calculate
		res  []int // The entries of the matrix x^k
	}{
		{
			n:    1,
			vals: []int{5},
			k:    3,
			res:  []int{125},
		},
		{
			n:    2,
			vals: []int{1, 3, -2, 0},
			k:    7,
			res:  []int{-539, -87, 58, -510},
		},
		{
			n:    3,
			vals: []int{0, -9, 9, -4, -4, 4, -10, 8, 0},
			k:    5,
			res:  []int{11232, -30564, 17892, -912, -18192, 14288, -7208, 64, 11936},
		},
		{
			n:    4,
			vals: []int{-1, -4, -3, 4, 3, 5, 9, -2, 9, -4, -6, 7, -7, 7, 4, 0},
			k:    6,
			res:  []int{-326246, 365767, 263927, -352940, -3944, -489349, -480154, 570069, -775172, 511670, 291626, -442737, 900483, -112272, 123377, 102524},
		},
		{
			n:    5,
			vals: []int{3, 8, 4, -5, 5, 5, 8, -10, -10, -4, -6, -8, -2, 9, -7, 5, 4, 3, 4, 10, 4, 5, 2, -3, 4},
			k:    3,
			res:  []int{81, 545, -800, -1590, -817, -92, 851, -1663, -2920, -2147, 57, -590, 1291, 2114, 1526, 777, 1612, -864, -1883, -361, 140, 588, -742, -1341, -725},
		},
	}
	// Run the tests
	for _, test := range tests {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// Create the test matrix x and expected result z
		x := fromIntSliceOrPanic(R, test.vals)
		z := fromIntSliceOrPanic(R, test.res)
		// Compute the power
		if y, err := PowerInt64(x, test.k); assert.NoError(err) {
			assert.AreEqual(R, y, z)
		}
		if y, err := x.PowerInt64(test.k); assert.NoError(err) {
			assert.AreEqual(R, y, z)
		}
		if y, err := x.Power(integer.FromInt64(test.k)); assert.NoError(err) {
			assert.AreEqual(R, y, z)
		}
	}
}

// TestLen tests Len
func TestLen(t *testing.T) {
	assert := assert.New(t)
	// Len should be equal to the product of the number of rows and number of
	// columns
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Calculate the expected length
		size := R.NumberOfRows() * R.NumberOfColumns()
		require.Equal(t, size, n*n)
		// Check the length of zero and one
		assert.Equal(Zero(R).Len(), size)
		assert.Equal(One(R).Len(), size)
		// Create a random matrix x and check the length
		x := fromIntSliceOrPanic(R, randIntSlice(size, 5000))
		assert.Equal(x.Len(), size)
	}
	// The length of a nil matrix must be 0
	var x *Element
	assert.Equal(x.Len(), 0)
}

// TestScalarMultiplyByInt64 tests ScalarMultiplyByInt64.
func TestScalarMultiplyByInt64(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(4)
	require.NoError(t, err)
	// Create a random matrix
	S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x := fromIntSliceOrPanic(R, S)
	// Work through a range of scalars
	for c := int64(-10); c <= 10; c++ {
		// Create the expected result...
		y := fromIntSliceOrPanic(R, scalarMultipleIntSlice(int(c), S))
		// ...and check it against the scalar multiple
		z := x.ScalarMultiplyByInt64(c)
		assert.AreEqual(R, z, y)
	}
	// Scalar multiples of the zero matrix should always be the zero matrix
	zero := Zero(R)
	for c := int64(-10); c <= 10; c++ {
		assert.IsZero(zero.ScalarMultiplyByInt64(c))
	}
	// Scalar multiples of the nil matrix should be the nil matrix
	x = nil
	for c := int64(-10); c <= 10; c++ {
		assert.IsZero(x.ScalarMultiplyByInt64(c))
	}
}

// TestScalarMultiplyCoefficient tests ScalarMultiplyByCoefficient.
func TestScalarMultiplyCoefficient(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(4)
	require.NoError(t, err)
	// Create a random matrix
	S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
	x := fromIntSliceOrPanic(R, S)
	// Work through a range of scalars
	for c := int64(-10); c <= 10; c++ {
		// Create the expected result...
		y := fromIntSliceOrPanic(R, scalarMultipleIntSlice(int(c), S))
		// ...and check it against the scalar multiple
		if z, err := x.ScalarMultiplyByCoefficient(integer.FromInt64(c)); assert.NoError(err) {
			assert.AreEqual(R, z, y)
		}
	}
	// Scalar multiples of the zero matrix should always be the zero matrix
	zero := Zero(R)
	for c := int64(-10); c <= 10; c++ {
		if z, err := zero.ScalarMultiplyByCoefficient(integer.FromInt64(c)); assert.NoError(err) {
			assert.IsZero(z)
		}
	}
	// Scalar multiples of the nil matrix should be the nil matrix
	x = nil
	for c := int64(-10); c <= 10; c++ {
		if z, err := x.ScalarMultiplyByCoefficient(integer.FromInt64(c)); assert.NoError(err) {
			assert.IsZero(z)
		}
	}
}

// TestElementEntries tests the Entries method for an Element.
func TestElementEntries(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create the zero matrix and check the entries are all zero
		entries := Zero(R).Entries()
		assert.True(len(entries) == R.NumberOfRows()*R.NumberOfColumns())
		for _, c := range entries {
			assert.IsZero(c)
		}
		// Create a random matrix, and check the entries are as expected
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		entries = fromIntSliceOrPanic(R, S).Entries()
		assert.True(len(entries) == R.NumberOfRows()*R.NumberOfColumns())
		for i, c := range entries {
			assert.IsEqualToInt(c, S[i])
		}
	}
	// Check that we can recover the entries of a nil matrix
	var x *Element
	assert.Equal(len(x.Entries()), 0)
}

// TestElementRowRows tests the Row and Rows methods for an Element.
func TestElementRowRows(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		// Create the zero matrix
		x := Zero(R)
		// Check that each row consists of the zero element
		for i := 0; i < nrows; i++ {
			if row, err := x.Row(i); assert.NoError(err) && assert.Equal(len(row), ncols) {
				for _, c := range row {
					assert.IsZero(c)
				}
			}
		}
		// Check that all the rows consist of the zero element
		if rows := x.Rows(); assert.Equal(len(rows), nrows) {
			for _, row := range rows {
				if assert.Equal(len(row), ncols) {
					for _, c := range row {
						assert.IsZero(c)
					}
				}
			}
		}
		// Asking for an out-or-range row of the zero matrix should error
		_, err = x.Row(nrows + 1)
		assert.Error(err)
		// Create a random matrix
		S := randIntSlice(nrows*ncols, 5000)
		x = fromIntSliceOrPanic(R, S)
		// Check that each row consists of the expected element
		for i := 0; i < nrows; i++ {
			offset := i * ncols
			if row, err := x.Row(i); assert.NoError(err) && assert.Equal(len(row), ncols) {
				for j, c := range row {
					assert.IsEqualToInt(c, S[offset+j])
				}
			}
		}
		// Check that all the rows consist of the expected elements
		if rows := x.Rows(); assert.Equal(len(rows), nrows) {
			for i, row := range rows {
				if assert.Equal(len(row), ncols) {
					offset := i * ncols
					for j, c := range row {
						assert.IsEqualToInt(c, S[offset+j])
					}
				}
			}
		}
		// Asking for an out-or-range row of the matrix should error
		_, err = x.Row(nrows + 1)
		assert.Error(err)
	}
	// A nil matrix should not have any rows
	var x *Element
	_, err := x.Row(0)
	assert.Error(err)
	rows := x.Rows()
	assert.Equal(len(rows), 0)
}

// TestElementColumnColumns tests the Column and Columns methods for an Element.
func TestElementColumnColumns(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		// Create the zero matrix
		x := Zero(R)
		// Check that each column consists of the zero element
		for i := 0; i < ncols; i++ {
			if col, err := x.Column(i); assert.NoError(err) && assert.Equal(len(col), nrows) {
				for _, c := range col {
					assert.IsZero(c)
				}
			}
		}
		// Check that all the columns consist of the zero element
		if cols := x.Columns(); assert.Equal(len(cols), ncols) {
			for _, col := range cols {
				if assert.Equal(len(col), nrows) {
					for _, c := range col {
						assert.IsZero(c)
					}
				}
			}
		}
		// Force the creation of the transpose, and then recheck the columns
		_ = x.Transpose()
		for i := 0; i < ncols; i++ {
			if col, err := x.Column(i); assert.NoError(err) && assert.Equal(len(col), nrows) {
				for _, c := range col {
					assert.IsZero(c)
				}
			}
		}
		// Asking for an out-or-range column of the zero matrix should error
		_, err = x.Column(ncols + 1)
		assert.Error(err)
		// Create a random matrix
		S := randIntSlice(nrows*ncols, 5000)
		x = fromIntSliceOrPanic(R, S)
		// Check that each column consists of the expected element
		for i := 0; i < ncols; i++ {
			if col, err := x.Column(i); assert.NoError(err) && assert.Equal(len(col), nrows) {
				for j, c := range col {
					assert.IsEqualToInt(c, S[j*ncols+i])
				}
			}
		}
		// Check that all the rows consist of the expected elements
		if cols := x.Columns(); assert.Equal(len(cols), ncols) {
			for i, col := range cols {
				if assert.Equal(len(col), nrows) {
					for j, c := range col {
						assert.IsEqualToInt(c, S[j*ncols+i])
					}
				}
			}
		}
		// Force the creation of the transpose, and then recheck the columns
		_ = x.Transpose()
		for i := 0; i < ncols; i++ {
			if col, err := x.Column(i); assert.NoError(err) && assert.Equal(len(col), nrows) {
				for j, c := range col {
					assert.IsEqualToInt(c, S[j*ncols+i])
				}
			}
		}
		// Asking for an out-or-range column of the matrix should error
		_, err = x.Column(ncols + 1)
		assert.Error(err)
	}
	// A nil matrix should not have any columns
	var x *Element
	_, err := x.Column(0)
	assert.Error(err)
	cols := x.Columns()
	assert.Equal(len(cols), 0)
}

// TestElementFlatEntry tests the FlatEntry method for an Element.
func TestElementFlatEntry(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		size := R.NumberOfRows() * R.NumberOfColumns()
		// Create the zero matrix and check the entries are all zero
		x := Zero(R)
		for i := 0; i < size; i++ {
			if c, err := x.FlatEntry(i); assert.NoError(err) {
				assert.IsZero(c)
			}
		}
		// Asking for an out-of range entry should error
		_, err = x.FlatEntry(-1)
		assert.Error(err)
		_, err = x.FlatEntry(size)
		assert.Error(err)
		// Create a random matrix, and check the entries are as expected
		S := randIntSlice(size, 5000)
		x = fromIntSliceOrPanic(R, S)
		for i, d := range S {
			if c, err := x.FlatEntry(i); assert.NoError(err) {
				assert.IsEqualToInt(c, d)
			}
		}
		// Asking for an out-of range entry should error
		_, err = x.FlatEntry(-1)
		assert.Error(err)
		_, err = x.FlatEntry(size)
		assert.Error(err)
	}
	// Check that a nil matrix has no entries
	var x *Element
	_, err := x.FlatEntry(0)
	assert.Error(err)
}

// TestElementEntryOrPanic tests the EntryOrPanic method for an Element.
func TestElementEntryOrPanic(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		// Create the zero matrix and check the entries are all zero
		x := Zero(R)
		for i := 0; i < nrows; i++ {
			for j := 0; j < ncols; j++ {
				assert.IsZero(x.EntryOrPanic(i, j))
			}
		}
		// Asking for an out-of range entry should panic
		assert.Panics(func() { x.EntryOrPanic(-1, 0) })
		assert.Panics(func() { x.EntryOrPanic(nrows, 0) })
		assert.Panics(func() { x.EntryOrPanic(0, -1) })
		assert.Panics(func() { x.EntryOrPanic(0, ncols) })
		// Create a random matrix, and check the entries are as expected
		S := randIntSlice(nrows*ncols, 5000)
		x = fromIntSliceOrPanic(R, S)
		for i := 0; i < nrows; i++ {
			for j := 0; j < ncols; j++ {
				assert.IsEqualToInt(x.EntryOrPanic(i, j), S[i*ncols+j])
			}
		}
		// Asking for an out-of range entry should panic
		assert.Panics(func() { x.EntryOrPanic(-1, 0) })
		assert.Panics(func() { x.EntryOrPanic(nrows, 0) })
		assert.Panics(func() { x.EntryOrPanic(0, -1) })
		assert.Panics(func() { x.EntryOrPanic(0, ncols) })
	}
	// Check that a nil matrix has no entries
	var x *Element
	assert.Panics(func() { x.EntryOrPanic(0, 0) })
}

// TestHash tests the Hash method.
func TestHash(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create the zero matrix and check the hash is 0
		x := Zero(R)
		assert.Equal(x.Hash(), hash.Value(0))
		// Create the same random matrix twice, and check the hashes agree
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		x = fromIntSliceOrPanic(R, S)
		y := fromIntSliceOrPanic(R, S)
		assert.Equal(x.Hash(), y.Hash())
	}
	// Check that a nil matrix has a hash of zero
	var x *Element
	assert.Equal(x.Hash(), hash.Value(0))
}

// TestParent tests the Parent method.
func TestParent(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of sizes
	for n := 0; n <= 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create the zero matrix and check the parent is R
		x := Zero(R)
		assert.Equal(x.Parent(), R)
		// Create a random matrix and check that the parent is R
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		x = fromIntSliceOrPanic(R, S)
		assert.Equal(x.Parent(), R)
	}
	// Check that the parent of a nil matrix is the nil parent
	var R *Parent
	var x *Element
	assert.Equal(x.Parent(), R)
}

// TestElementString tests the String method for an Element.
func TestElementString(t *testing.T) {
	assert := assert.New(t)
	// Create the parent
	R, err := NewAlgebra(3)
	require.NoError(t, err)
	// The tests and expected results
	tests := []struct {
		vals []int  // The entries of t
		s    string // The expected string
	}{
		{
			vals: []int{0, 0, 0, 0, 0, 0, 0, 0, 0},
			s:    "⎛0 0 0⎞\n⎜0 0 0⎟\n⎝0 0 0⎠",
		},
		{
			vals: []int{1, 0, 0, 0, 1, 0, 0, 0, 1},
			s:    "⎛1 0 0⎞\n⎜0 1 0⎟\n⎝0 0 1⎠",
		},
		{
			vals: []int{-1, 2, 3, 10, -20, 7, 9, 0, -100},
			s:    "⎛-1   2    3⎞\n⎜10 -20    7⎟\n⎝ 9   0 -100⎠",
		},
	}
	// Perform the tests
	for _, test := range tests {
		assert.Equal(test.s, fromIntSliceOrPanic(R, test.vals).String())
	}
	// Check that the nil matrix gives the expected string
	var x *Element
	assert.Equal("()", x.String())
}
