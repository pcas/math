// Tests for creation.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcastools/mathutil"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestNewAlgebra tests NewAlgebra.
func TestNewAlgebra(t *testing.T) {
	assert := assert.New(t)
	// Create some new matrix algebras and check that we get the same ones back
	Rs := make([]*Parent, 10)
	for i := 0; i < len(Rs); i++ {
		if R, err := NewAlgebra(i); assert.NoError(err) {
			Rs[i] = R
		}
	}
	for i, R := range Rs {
		if RR, err := NewAlgebra(i); assert.NoError(err) {
			assert.True(RR == R)
		}
	}
	// The following should fail
	_, err := NewAlgebra(-1)
	assert.Error(err)
	_, err = NewAlgebra(int(mathutil.MaxInt / 2))
	assert.Error(err)
}
