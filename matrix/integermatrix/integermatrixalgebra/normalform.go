// Normalform defines Echelon form-related functions for working with n x n integer-valued matrices.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// inverseMatrix returns the inverse of the matrix x, or an error if x is not invertible. Assumes that x is a non-nill square matrix. This does NOT cache the inverse on x.
func inverseMatrix(x *Element) (*Element, error) {
	panic(fatal.NotImplemented()) // FIXME: Needs implementing
}

// computeDeterminant computes and returns the determinant of the matrix x. Assumes that x is a non-nill square matrix. This does NOT cache the determinant on x.
func computeDeterminant(x *Element) (*integer.Element, error) {
	// FIXME: This is a stupid way to compute the determinant
	d, err := entries.DeterminantViaLaplace(x.es)
	if err != nil {
		return nil, err
	}
	return d.(*integer.Element), nil
}
