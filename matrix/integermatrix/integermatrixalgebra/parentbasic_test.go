// Tests for parentbasic.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/boolean"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"github.com/stretchr/testify/require"
	"math/rand"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// randIntSlice returns a random slice of ints in {-r,...,r} of the given length.
func randIntSlice(n int, r int) []int {
	if n < 0 {
		panic("Slice must be of a non-negative length")
	}
	if r < 0 {
		r = -r
	}
	S := make([]int, 0, n)
	for i := 0; i < n; i++ {
		c := rand.Int() % (r + 1)
		if rand.Int()%2 == 0 {
			c = -c
		}
		S = append(S, c)
	}
	return S
}

// addIntSlices returns the sum S1 + S2 of the two integer slices. This will panic if S1 and S2 are not of the same length.
func addIntSlices(S1 []int, S2 []int) []int {
	n := len(S1)
	if len(S2) != n {
		panic("Slices must be of the same length")
	}
	T := make([]int, 0, n)
	for i, c := range S1 {
		T = append(T, c+S2[i])
	}
	return T
}

// addSliceOfIntSlices returns the sum of the S[i]. This will panic if the S[i] are not of the same length. Assumes that Ss is non-empty.
func addSliceOfIntSlices(Ss [][]int) []int {
	n := len(Ss[0])
	T := make([]int, n)
	for _, S := range Ss {
		if len(S) != n {
			panic("Slices must be of the same length")
		}
		for i, c := range S {
			T[i] += c
		}
	}
	return T
}

// subtractIntSlices returns the difference S1 - S2 of the two integer slices. This will panic if S1 and S2 are not of the same length.
func subtractIntSlices(S1 []int, S2 []int) []int {
	n := len(S1)
	if len(S2) != n {
		panic("Slices must be of the same length")
	}
	T := make([]int, 0, n)
	for i, c := range S1 {
		T = append(T, c-S2[i])
	}
	return T
}

// negateIntSlice returns the negation -S of the integer slice.
func negateIntSlice(S []int) []int {
	T := make([]int, 0, len(S))
	for _, c := range S {
		T = append(T, -c)
	}
	return T
}

// scalarMultipleIntSlice returns the slice given by n * S[i].
func scalarMultipleIntSlice(n int, S []int) []int {
	T := make([]int, 0, len(S))
	for _, c := range S {
		T = append(T, n*c)
	}
	return T
}

// transposeIntSlice returns the transpose of the slice S representing an n x n matrix.
func transposeIntSlice(S []int, n int) []int {
	T := make([]int, 0, len(S))
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			T = append(T, S[n*j+i])
		}
	}
	return T
}

// fromIntSliceOrPanic returns a matrix in the matrix algebra R, with entries S. The entries are read row-by-row.
func fromIntSliceOrPanic(R *Parent, S []int) *Element {
	x, err := FromSlice(R, integer.SliceFromIntSlice(S))
	if err != nil {
		panic(err)
	}
	return x
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestBasic tests Ring,
func TestBasic(t *testing.T) {
	assert := assert.New(t)
	// We test a range of dimensions
	for n := 0; n < 10; n++ {
		// Create a test ring
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Check that it returns the integers as the coefficient ring
		assert.True(R.Ring() == integer.Ring())
		// Check that the number of rows and columns is correct
		assert.True(R.NumberOfRows() == n)
		assert.True(R.NumberOfColumns() == n)
		// This should satisfy the matrix.Algebra interface
		var RR object.Parent
		RR = R
		_, ok := RR.(matrix.Algebra)
		assert.True(ok)
		// An element of R should be contained in R and be an element in R
		x := fromIntSliceOrPanic(R, randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000))
		assert.True(R.Contains(x))
		if y, err := R.ToElement(x); assert.NoError(err) {
			assert.AreEqual(R, y, x)
		}
		// A matrix not in R should not be contained in R
		RR, err = NewAlgebra(n + 1)
		require.NoError(t, err)
		assert.False(RR.Contains(x))
		_, err = RR.ToElement(x)
		assert.Error(err)
		// And a non-matrix should not be contained in R
		assert.False(RR.Contains(integer.Zero()))
		_, err = RR.ToElement(integer.Zero())
		assert.Error(err)
		// A non-matrix should not be equal to zero
		_, err = R.IsZero(integer.Zero())
		assert.Error(err)
		// A non-matrix should not be equal to a matrix
		_, err = R.AreEqual(integer.Zero(), R.Zero())
		assert.Error(err)
		_, err = R.AreEqual(R.Zero(), integer.Zero())
		assert.Error(err)
	}
	// Checks of a nil algebra
	var R *Parent
	assert.True(R.Ring() == integer.Ring())
	assert.True(R.NumberOfRows() == 0)
	assert.True(R.NumberOfColumns() == 0)
}

// TestZero tests Zero and IsZero.
func TestZero(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent and its zero element
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		zero := R.Zero()
		assert.True(R.Contains(zero))
		assert.IsZeroIn(R, zero)
		// Create the zero element by hand
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		myzero := fromIntSliceOrPanic(R, make([]int, nrows*ncols))
		assert.True(R.Contains(myzero))
		assert.IsZeroIn(R, zero)
		// Check that my zero and the original zero are equal
		assert.AreEqual(R, zero, myzero)
		// The sum of two zero elements should be zero
		if res, err := R.Add(zero, myzero); assert.NoError(err) {
			assert.AreEqual(R, zero, res)
		}
		// The difference between two zero elements should be zero
		if res, err := R.Subtract(zero, myzero); assert.NoError(err) {
			assert.AreEqual(R, zero, res)
		}
		// The product of two zero elements should be zero
		if res, err := R.Multiply(zero, myzero); assert.NoError(err) {
			assert.AreEqual(R, zero, res)
		}
		// The negation of zero should be zero
		if res, err := R.Negate(myzero); assert.NoError(err) {
			assert.AreEqual(R, zero, res)
		}
		// Positive powers of zero should be zero
		for k := 1; k < 10; k++ {
			if res, err := R.Power(myzero, integer.FromInt(1000*k)); assert.NoError(err) {
				assert.AreEqual(R, zero, res)
			}
		}
		// A zero power of zero should be the identity matrix
		if res, err := R.Power(myzero, integer.Zero()); assert.NoError(err) {
			assert.IsOneIn(R, res)
		}
		// Negative powers of zero should error
		_, err = R.Power(myzero, integer.FromInt(-100))
		assert.Error(err)
		// The inverse of zero should error
		_, err = R.Inverse(myzero)
		assert.Error(err)
		// Scalar multiples of zero should be zero
		for k := -10; k <= 10; k++ {
			kk := integer.FromInt(k)
			if res, err := R.ScalarMultiplyByInteger(kk, myzero); assert.NoError(err) {
				assert.AreEqual(R, res, zero)
			}
			if res, err := R.ScalarMultiplyByCoefficient(kk, myzero); assert.NoError(err) {
				assert.AreEqual(R, res, zero)
			}
		}
		// The transpose of zero should be zero
		if res, err := R.Transpose(myzero); assert.NoError(err) {
			assert.AreEqual(R, res, zero)
		}
		// The rows of zero should all contain zero
		for i := 0; i < nrows; i++ {
			if row, err := R.Row(myzero, i); assert.NoError(err) {
				for _, c := range row {
					assert.IsZeroIn(integer.Ring(), c)
				}
			}
		}
		// The columns of zero should all contain zero
		for i := 0; i < ncols; i++ {
			if row, err := R.Column(myzero, i); assert.NoError(err) {
				for _, c := range row {
					assert.IsZeroIn(integer.Ring(), c)
				}
			}
		}
		// The entries of zero should all be zero
		if entries, err := R.Entries(myzero); assert.NoError(err) {
			for _, c := range entries {
				assert.IsZeroIn(integer.Ring(), c)
			}
		}
		// The determinant of the zero matrix is zero
		if det, err := R.Determinant(myzero); assert.NoError(err) {
			assert.IsZeroIn(integer.Ring(), det)
		}
		// Test multiplying a random matrix with the zero matrix
		x := fromIntSliceOrPanic(R, randIntSlice(nrows*ncols, 5000))
		if y, err := R.Multiply(zero, x); assert.NoError(err) {
			assert.IsZeroIn(R, y)
		}
		if y, err := R.Multiply(x, zero); assert.NoError(err) {
			assert.IsZeroIn(R, y)
		}
		// Create a non-zero element -- this should not be equal to zero
		S := make([]int, nrows*ncols)
		S[R.NumberOfRows()-1] = 1
		x = fromIntSliceOrPanic(R, S)
		assert.True(R.Contains(x))
		if ok, err := R.IsZero(x); assert.NoError(err) {
			assert.False(ok)
		}
		if ok, err := R.AreEqual(zero, x); assert.NoError(err) {
			assert.False(ok)
		}
		if ok, err := R.AreEqual(x, zero); assert.NoError(err) {
			assert.False(ok)
		}
	}
	// The zero matrix in the nil case
	var R *Parent
	var x *Element
	assert.IsZeroIn(R, R.Zero())
	assert.IsZeroIn(R, x)
	assert.AreEqual(R, R.Zero(), x)
}

// TestAddSubtractNegate tests Add, Subtract, and Negate.
func TestAddSubtractNegate(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// We test a few randomly generated matrices
		for i := 0; i < 10; i++ {
			// Create two random slices
			S1 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
			S2 := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
			// Calculate S1 + S2, S1 - S2, and -S1.
			sum := addIntSlices(S1, S2)
			difference := subtractIntSlices(S1, S2)
			negation := negateIntSlice(S1)
			// Convert the slices S1 and S2 into matrices
			x1, x2 := fromIntSliceOrPanic(R, S1), fromIntSliceOrPanic(R, S2)
			// Assert that the matices are elements of R
			assert.True(R.Contains(x1))
			assert.True(R.Contains(x2))
			// Check the results are as expected
			if y, err := R.Add(x1, x2); assert.NoError(err) {
				assert.AreEqual(R, y, fromIntSliceOrPanic(R, sum))
			}
			if y, err := R.Subtract(x1, x2); assert.NoError(err) {
				assert.AreEqual(R, y, fromIntSliceOrPanic(R, difference))
			}
			if y, err := R.Negate(x1); assert.NoError(err) {
				assert.AreEqual(R, y, fromIntSliceOrPanic(R, negation))
			}
		}
	}
	// Create a test ring
	R, err := NewAlgebra(2)
	require.NoError(t, err)
	// Adding, subtracting, and negating elements not in R should error
	_, err = R.Add(integer.Zero(), R.Zero())
	assert.Error(err)
	_, err = R.Add(R.Zero(), integer.Zero())
	assert.Error(err)
	_, err = R.Subtract(integer.Zero(), R.One())
	assert.Error(err)
	_, err = R.Subtract(R.Zero(), integer.One())
	assert.Error(err)
	_, err = R.Negate(integer.Zero())
	assert.Error(err)
}

// TestSum tests Sum.
func TestSum(t *testing.T) {
	assert := assert.New(t)
	// Create a parent
	R, err := NewAlgebra(3)
	require.NoError(t, err)
	// The sum of the zero slice should be the zero element
	if x, err := R.Sum(); assert.NoError(err) {
		assert.IsZeroIn(R, x)
	}
	// Create random slices of entries and the corresponding matrices
	Ss := make([][]int, 0, 1000)
	xs := make([]object.Element, 0, cap(Ss))
	for i := 0; i < cap(Ss); i++ {
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		Ss = append(Ss, S)
		xs = append(xs, fromIntSliceOrPanic(R, S))
	}
	// The sum of a single matrix should be that matrix
	if x, err := R.Sum(xs[0]); assert.NoError(err) {
		assert.AreEqual(R, x, xs[0])
	}
	// The sum of two matrices should be x1 + x2
	if x, err := R.Sum(xs[0], xs[1]); assert.NoError(err) {
		y := fromIntSliceOrPanic(R, addIntSlices(Ss[0], Ss[1]))
		assert.AreEqual(R, x, y)
	}
	// Calculate the sum of the slices
	if x, err := R.Sum(xs...); assert.NoError(err) {
		y := fromIntSliceOrPanic(R, addSliceOfIntSlices(Ss))
		assert.AreEqual(R, x, y)
	}
	// If any of the elements are not in R, this should error
	xs[0] = integer.Zero()
	_, err = R.Sum(xs...)
	assert.Error(err)
}

// TestPower tests Power.
func TestPower(t *testing.T) {
	assert := assert.New(t)
	// Raising any matrix to the power of 0 should return the identity matrix
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a random matrix x and compute x^0
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		if y, err := R.Power(fromIntSliceOrPanic(R, S), integer.Zero()); assert.NoError(err) {
			assert.IsOneIn(R, y)
		}
	}
	// The tests to perform
	tests := []struct {
		n    int   // This will be an n x n matrix
		vals []int // The entries of the matrix x
		k    int   // The power to calculate
		res  []int // The entries of the matrix x^k
	}{
		{
			n:    1,
			vals: []int{3},
			k:    4,
			res:  []int{81},
		},
		{
			n:    2,
			vals: []int{-1, 5, 0, 9},
			k:    10,
			res:  []int{1, 1743392200, 0, 3486784401},
		},
		{
			n:    3,
			vals: []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
			k:    7,
			res:  []int{31644432, 38881944, 46119456, 71662158, 88052265, 104442372, 111679884, 137222586, 162765288},
		},
		{
			n:    4,
			vals: []int{-5, 4, -3, 2, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
			k:    9,
			res:  []int{-1953125, 1302084, -976563, 651042, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
		},
		{
			n:    5,
			vals: []int{1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1},
			k:    10000,
			res:  []int{1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1},
		},
	}
	// Run the tests
	for _, test := range tests {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// Compute the power
		if y, err := R.Power(fromIntSliceOrPanic(R, test.vals),
			integer.FromInt(test.k)); assert.NoError(err) {
			assert.AreEqual(R, y, fromIntSliceOrPanic(R, test.res))
		}
	}
	// Create a test ring
	R, err := NewAlgebra(2)
	require.NoError(t, err)
	// Taking the power of an element not in R should error
	_, err = R.Power(integer.Zero(), integer.One())
	assert.Error(err)
}

// TestInverse tests Inverse.
func TestInverse(t *testing.T) {
	assert := assert.New(t)
	// The tests to perform
	tests := []struct {
		n    int   // This will be an n x n matrix
		vals []int // The entries of the matrix x
		res  []int // The entries of the matrix x^-1
	}{
		{
			n:    2,
			vals: []int{1, 0, 0, 1},
			res:  []int{1, 0, 0, 1},
		},
		{
			n:    3,
			vals: []int{1, 0, 0, 0, 1, 0, 0, 0, 1},
			res:  []int{1, 0, 0, 0, 1, 0, 0, 0, 1},
		},
		{
			n:    4,
			vals: []int{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
			res:  []int{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
		},
		/* FIXME: Uncomment these test cases
		{
			n:    2,
			vals: []int{1, 2, 3, 5},
			res:  []int{-5, 2, 3, -1},
		},
		{
			n:    3,
			vals: []int{0, 1, 1, 0, -2, -3, -1, 1, 0},
			res:  []int{3, 1, -1, 3, 1, 0, -2, -1, 0},
		},
		{
			n:    4,
			vals: []int{0, 2, 0, -1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, -1},
			res:  []int{0, 0, 1, 0, 1, 0, 0, -1, -1, 1, 0, 2, 1, 0, 0, -2},
		},
		{
			n:    5,
			vals: []int{-1, 0, 0, -1, -1, 2, 1, 1, 2, 0, -1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, -1, 0, 1},
			res:  []int{0, 0, -1, 0, 0, 1, 1, 3, -1, 1, -1, 0, -1, -1, -1, 0, 0, 0, 1, 0, -1, 0, 1, -1, 0},
		},
		*/
	}
	// Run the tests
	for _, test := range tests {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// Create x and x^-1
		x, y := fromIntSliceOrPanic(R, test.vals), fromIntSliceOrPanic(R, test.res)
		// Compute the inverse in both directions
		if z, err := R.Inverse(x); assert.NoError(err) {
			assert.AreEqual(R, z, y)
		}
		if z, err := R.Inverse(y); assert.NoError(err) {
			assert.AreEqual(R, z, x)
		}
	}
	// Test some matrices that are not invertible over ZZ, so should error
	tests2 := []struct {
		n    int   // This will be an n x n matrix
		vals []int // The entries of the matrix x
	}{
	/* FIXME: Uncomment these test cases
	{
		n:    2,
		vals: []int{1, 2, 3, 4},
	},
	{
		n:    3,
		vals: []int{1, 2, -6, 3, 4, -7, 1, 1, 1},
	},
	{
		n:    3,
		vals: []int{5, 3, 6, -1, -2, -4, 0, 1, 2},
	},
	{
		n:    3,
		vals: []int{7, -1, 2, 4, -5, -5, -14, 2, -4},
	},
	{
		n:    4,
		vals: []int{7, -1, 2, 1, 1, 1, 1, 1, 4, -5, -5, 2, -14, 2, -4, 3},
	},
	*/
	}
	// Run the tests
	for _, test := range tests2 {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// The inverse should error
		_, err = R.Inverse(fromIntSliceOrPanic(R, test.vals))
		assert.Error(err)
	}
	// Create a test ring
	R, err := NewAlgebra(2)
	require.NoError(t, err)
	// Taking the inverse of an element not in R should error
	_, err = R.Inverse(integer.Zero())
	assert.Error(err)
}

// TestScalarMultiplyBy tests ScalarMultiplyByInteger and ScalarMultiplyByCoefficient.
func TestScalarMultiplyBy(t *testing.T) {
	assert := assert.New(t)
	// Work through a range of dimensions
	for n := 0; n < 10; n++ {
		// Create the parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a random matrix x
		S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
		x := fromIntSliceOrPanic(R, S)
		// Scalar multiply by a range of values
		for k := -10; k <= 10; k++ {
			// Create the expected result
			z := fromIntSliceOrPanic(R, scalarMultipleIntSlice(k, S))
			// Compute the scalar multiple and check the result
			kk := integer.FromInt(k)
			if y, err := R.ScalarMultiplyByInteger(kk, x); assert.NoError(err) {
				assert.AreEqual(R, y, z)
			}
			if y, err := R.ScalarMultiplyByCoefficient(kk, x); assert.NoError(err) {
				assert.AreEqual(R, y, z)
			}
		}
	}
	// Create a test ring
	R, err := NewAlgebra(2)
	require.NoError(t, err)
	// Taking the scalar multiple of an element not in R should error
	_, err = R.ScalarMultiplyByInteger(integer.One(), integer.Zero())
	assert.Error(err)
	_, err = R.ScalarMultiplyByCoefficient(integer.One(), integer.Zero())
	assert.Error(err)
	_, err = R.ScalarMultiplyByCoefficient(R.Zero(), R.Zero())
	assert.Error(err)
}

// TestTranspose tests Transpose.
func TestTranspose(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// We test a few randomly generated matrices
		for i := 0; i < 10; i++ {
			// Create a random slice and its transpose
			S := randIntSlice(R.NumberOfRows()*R.NumberOfColumns(), 5000)
			T := transposeIntSlice(S, R.NumberOfRows())
			// Create matrices from the slices
			x, y := fromIntSliceOrPanic(R, S), fromIntSliceOrPanic(R, T)
			// Check the transposes are as expected
			if z, err := R.Transpose(x); assert.NoError(err) {
				assert.AreEqual(R, z, y)
			}
			if z, err := R.Transpose(y); assert.NoError(err) {
				assert.AreEqual(R, z, x)
			}
		}
		// Asking for the transpose of a non-matrix should error
		_, err = R.Transpose(integer.Zero())
		assert.Error(err)
	}
}

// TestRowsAndColumns tests Row, Rows, Column, and Columns.
func TestRowsAndColumns(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a test matrix
		ncols, nrows := R.NumberOfRows(), R.NumberOfColumns()
		S := randIntSlice(nrows*ncols, 5000)
		x := fromIntSliceOrPanic(R, S)
		// Work through the rows, checking that they're as expected
		for i := 0; i < nrows; i++ {
			if row, err := R.Row(x, i); assert.NoError(err) {
				for j, c := range row {
					assert.IsEqualToInt(c.(*integer.Element), S[i*ncols+j])
				}
			}
		}
		// Recover all the rows and check that they're as expected
		if rows, err := R.Rows(x); assert.NoError(err) {
			for i, row := range rows {
				for j, c := range row {
					assert.IsEqualToInt(c.(*integer.Element), S[i*ncols+j])
				}
			}
		}
		// Work through the columns, checking that they're as expected
		for i := 0; i < ncols; i++ {
			if col, err := R.Column(x, i); assert.NoError(err) {
				for j, c := range col {
					assert.IsEqualToInt(c.(*integer.Element), S[j*ncols+i])
				}
			}
		}
		// Recover all the columns and check that they're as expected
		if cols, err := R.Columns(x); assert.NoError(err) {
			for i, col := range cols {
				for j, c := range col {
					assert.IsEqualToInt(c.(*integer.Element), S[j*ncols+i])
				}
			}
		}
		// Force the creation of the transpose, and then recheck the columns
		_ = x.Transpose()
		for i := 0; i < ncols; i++ {
			if col, err := R.Column(x, i); assert.NoError(err) {
				for j, c := range col {
					assert.IsEqualToInt(c.(*integer.Element), S[j*ncols+i])
				}
			}
		}
		// Asking for out-of-range indices should error
		_, err = R.Row(x, -1)
		assert.Error(err)
		_, err = R.Row(x, nrows)
		assert.Error(err)
		_, err = R.Column(x, -1)
		assert.Error(err)
		_, err = R.Column(x, ncols)
		assert.Error(err)
		// Asking for row,column etc. of a non-matrix should error
		_, err = R.Row(integer.Zero(), 0)
		assert.Error(err)
		_, err = R.Column(integer.Zero(), 0)
		assert.Error(err)
		_, err = R.Rows(integer.Zero())
		assert.Error(err)
		_, err = R.Columns(integer.Zero())
		assert.Error(err)
	}
}

// TestEntries tests Entry and Entries.
func TestEntries(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a test matrix
		ncols, nrows := R.NumberOfRows(), R.NumberOfColumns()
		S := randIntSlice(nrows*ncols, 5000)
		x := fromIntSliceOrPanic(R, S)
		// Work through the individual entries
		for i := 0; i < nrows; i++ {
			for j := 0; j < ncols; j++ {
				if c, err := R.Entry(x, i, j); assert.NoError(err) {
					assert.IsEqualToInt(c.(*integer.Element), S[i*ncols+j])
				}
			}
		}
		// Recover all the entries in one go and check equality
		if entries, err := R.Entries(x); assert.NoError(err) {
			for i, c := range entries {
				assert.IsEqualToInt(c.(*integer.Element), S[i])
			}
		}
		// Asking for an out-of-range entry should error
		_, err = R.Entry(x, -1, 0)
		assert.Error(err)
		_, err = R.Entry(x, 0, -1)
		assert.Error(err)
		_, err = R.Entry(x, nrows, 0)
		assert.Error(err)
		_, err = R.Entry(x, 0, ncols)
		assert.Error(err)
		_, err = R.Column(integer.Zero(), 0)
		assert.Error(err)
		// Asking for entries for of a non-matrix should error
		_, err = R.Entry(integer.Zero(), 0, 0)
		assert.Error(err)
		_, err = R.Entries(integer.Zero())
		assert.Error(err)
	}
}

// TestFromSlice tests FromSlice.
func TestFromSlice(t *testing.T) {
	assert := assert.New(t)
	// We work through a range of sizes
	for n := 1; n < 10; n++ {
		// Create a new parent
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Create a test slices
		ncols, nrows := R.NumberOfRows(), R.NumberOfColumns()
		S := randIntSlice(nrows*ncols, 5000)
		T := make([]object.Element, 0, len(S))
		for _, c := range S {
			T = append(T, integer.FromInt(c))
		}
		// Create the corresponding matrices and check for equality
		x := fromIntSliceOrPanic(R, S)
		if y, err := R.FromSlice(T); assert.NoError(err) {
			assert.AreEqual(R, x, y)
		}
		// A slice of incorrect length should error
		T = append(T, integer.Zero())
		_, err = R.FromSlice(T)
		assert.Error(err)
		// A slice of incorrect objects should error
		U := make([]object.Element, 0, len(S))
		for i := 0; i < len(S); i++ {
			U = append(U, boolean.Element(true))
		}
		_, err = R.FromSlice(U)
		assert.Error(err)
	}
}
