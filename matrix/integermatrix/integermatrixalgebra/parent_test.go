// Tests for parent.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestString tests String.
func TestString(t *testing.T) {
	assert := assert.New(t)
	// We test a range of dimensions
	for n := 0; n < 10; n++ {
		// Create a test ring
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Is the string what we expect?
		str := R.String()
		expected := fmt.Sprintf("MatrixAlgebra(%s, %d)", integer.Ring().String(), n)
		assert.True(str == expected, "Expected string: %s\nGot string: %s", expected, str)
	}
}

// TestOne tests One and IsOne.
func TestOne(t *testing.T) {
	assert := assert.New(t)
	// We test a range of dimensions
	for n := 0; n < 10; n++ {
		// Create a test ring
		R, err := NewAlgebra(n)
		require.NoError(t, err)
		// Recover the identity matrix
		one := R.One()
		assert.IsOneIn(R, one)
		// Check that the entries are as expected
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		for i := 0; i < nrows; i++ {
			if row, err := R.Row(one, i); assert.NoError(err) {
				for j, c := range row {
					if j == i {
						assert.IsOneIn(integer.Ring(), c)
					} else {
						assert.IsZeroIn(integer.Ring(), c)
					}
				}
			}
		}
		// The determinant of the identity matrix is one
		if det, err := R.Determinant(one); assert.NoError(err) {
			assert.IsOneIn(integer.Ring(), det)
		}
		// Any power of the identity matrix should be the identity matrix
		for k := -100; k <= 100; k++ {
			if x, err := R.Power(one, integer.FromInt(k)); assert.NoError(err) {
				assert.AreEqual(R, x, one)
			}
		}
		// Multiplying the identity matrix with itself should be the identity
		if x, err := R.Multiply(one, one); assert.NoError(err) {
			assert.AreEqual(R, x, one)
		}
		// Test multiplying a random matrix with the identity matrix
		x := fromIntSliceOrPanic(R, randIntSlice(nrows*ncols, 5000))
		if y, err := R.Multiply(one, x); assert.NoError(err) {
			assert.AreEqual(R, y, x)
		}
		if y, err := R.Multiply(x, one); assert.NoError(err) {
			assert.AreEqual(R, y, x)
		}
		// Create the identity matrix by hand and test equality
		S := make([]object.Element, 0, nrows*ncols)
		for i := 0; i < nrows; i++ {
			for j := 0; j < ncols; j++ {
				if i == j {
					S = append(S, integer.One())
				} else {
					S = append(S, integer.Zero())
				}
			}
		}
		if myone, err := R.FromSlice(S); assert.NoError(err) {
			assert.IsOneIn(R, myone)
			assert.AreEqual(R, myone, one)
		}
		// The zero matrix should equal the identity matrix iff n = 0
		if nrows == 0 {
			assert.IsZeroIn(R, one)
		} else {
			if ok, err := R.IsZero(one); assert.NoError(err) {
				assert.False(ok)
			}
		}
		// Asking if a non-matrix is one should error
		_, err = R.IsOne(integer.One())
		assert.Error(err)
	}
	// The identity matrix in the nil case
	var R *Parent
	var x *Element
	assert.IsOneIn(R, R.One())
	assert.IsZeroIn(R, R.One())
	assert.AreEqual(R, R.One(), R.Zero())
	assert.IsOneIn(R, R.Zero())
	assert.AreEqual(R, R.One(), x)
	assert.IsOneIn(R, x)
}

// TestMultiply tests Multiply.
func TestMultiply(t *testing.T) {
	assert := assert.New(t)
	// The tests to perform
	tests := []struct {
		n     int   // This will be an n x n matrix
		vals1 []int // The entries of the matrix x1
		vals2 []int // The entries of the matrix x2
		res   []int // The entries of the product x1 * x2
	}{
		{
			n:     1,
			vals1: []int{7},
			vals2: []int{-2},
			res:   []int{-14},
		},
		{
			n:     2,
			vals1: []int{1, 2, 3, 4},
			vals2: []int{4, -7, 1, 0},
			res:   []int{6, -7, 16, -21},
		},
		{
			n:     3,
			vals1: []int{-2, -7, -40, -55, -51, 96, 99, -30, 75},
			vals2: []int{36, 84, -7, -31, -26, -86, -36, 8, -91},
			res:   []int{1585, -306, 4256, -3855, -2526, -3965, 1794, 9696, -4938},
		},
		{
			n:     4,
			vals1: []int{53, 52, 9, -64, -67, -72, -96, 72, -69, 84, 6, 1, -43, 46, 68, -21},
			vals2: []int{-22, 74, -68, 29, -42, 69, 57, -28, -5, -87, 57, 18, 7, 47, -62, -100},
			res:   []int{-3843, 3719, 3841, 6643, 5482, 1810, -9484, -8855, -2033, 215, 9760, -4345, -1473, -6911, 10724, 789},
		},
		{
			n:     5,
			vals1: []int{-35, 7, 44, -30, 91, -40, -39, -100, 23, -4, -94, -15, -55, -90, -5, -97, 83, 86, -14, -11, -57, -59, -73, -14, 25},
			vals2: []int{-24, 41, 89, -60, 79, -55, 44, -11, -69, -19, 16, 98, -71, 23, 46, -25, 79, -96, -40, -70, -68, -41, -6, 50, 45},
			res:   []int{-4279, -2916, -3982, 8379, 5321, 1202, -11175, 1785, 1671, -8809, 4791, -16809, 4374, 8760, -3596, 237, 7448, -14242, 2081, -4799, 2095, -14218, 1953, 7622, -4635},
		},
		{
			n:     6,
			vals1: []int{50, 44, -84, 52, 37, 93, -5, 85, 97, -67, 29, -71, -99, -79, 31, -95, 62, 24, 69, 94, -66, 70, -66, -84, -36, 84, 49, 38, -6, -38, 40, -26, -13, 11, 43, 79},
			vals2: []int{-18, -66, 60, 32, 73, -44, 81, -84, 85, -70, -31, -33, 79, 24, 61, -56, -33, -16, -57, 99, -45, 75, 76, 97, 87, 38, 37, -74, 40, 91, -6, -91, 45, 12, -98, -33},
			res:   []int{-4275, -10921, 4830, 5502, 1376, 3034, 21406, -3552, 13735, -19565, -3175, -5654, 8497, 4681, -3115, -10799, -12893, 2102, -8070, -1968, -1268, 8450, 15213, -1526, 8863, 3488, 4327, -6938, -477, 2422, -1213, -5234, 4048, 2419, -1031, 1679},
		},
	}
	// Run the tests
	for _, test := range tests {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// Compute the product
		x1 := fromIntSliceOrPanic(R, test.vals1)
		if z, err := R.Multiply(x1, fromIntSliceOrPanic(R, test.vals2)); assert.NoError(err) {
			assert.AreEqual(R, z, fromIntSliceOrPanic(R, test.res))
		}
		// The product with a matrix not in R should fail
		R2, err := NewAlgebra(test.n + 1)
		require.NoError(t, err)
		_, err = R.Multiply(x1, R2.Zero())
		assert.Error(err)
		_, err = R.Multiply(R2.Zero(), x1)
		assert.Error(err)
		// The product with a non-matrix should fail
		_, err = R.Multiply(x1, integer.Zero())
		assert.Error(err)
		_, err = R.Multiply(integer.Zero(), x1)
		assert.Error(err)
	}
}

// TestDeterminant tests Determinant.
func TestDeterminant(t *testing.T) {
	assert := assert.New(t)
	// The tests to perform
	tests := []struct {
		n    int   // This will be an n x n matrix
		vals []int // The entries of the matrix
		d    int   // The determinant
	}{
		{
			n:    1,
			vals: []int{7},
			d:    7,
		},
		{
			n:    2,
			vals: []int{27, 80, 94, 17},
			d:    -7061,
		},
		{
			n:    2,
			vals: []int{48, 7, -30, 20},
			d:    1170,
		},
		{
			n:    3,
			vals: []int{-47, 25, 55, 18, 34, -51, -90, 38, -16},
			d:    262352,
		},
		{
			n:    3,
			vals: []int{-60, 10, 4, 37, -54, -49, 14, 22, 79},
			d:    161470,
		},
		{
			n:    4,
			vals: []int{2, -10, -3, -3, 8, 5, 4, 4, 1, 8, 9, 4, -1, -5, -4, 4},
			d:    3357,
		},
		{
			n:    4,
			vals: []int{-9, -4, -6, 1, 7, 1, 10, 10, 2, -5, 8, -6, -7, 0, -3, 8},
			d:    2883,
		},
		{
			n:    5,
			vals: []int{7, 5, 1, 1, 7, 7, 4, 8, -5, 2, 0, -2, -4, -3, 7, -4, -3, 0, 4, 5, 6, -10, 7, -7, 5},
			d:    -48246,
		},
		{
			n:    5,
			vals: []int{0, -6, 5, 10, 4, -8, -1, 7, 10, -4, -7, 5, 9, -4, 0, 9, 10, -9, -8, -3, 4, -6, 3, 10, -9},
			d:    111792,
		},
		{
			n:    6,
			vals: []int{-4, -9, -4, 4, -1, -3, 7, 10, 0, -7, 0, 0, -5, -4, -10, -9, -3, 6, 9, -5, -7, -3, -10, 6, -2, 5, -3, -9, 2, -3, -1, 5, 1, -10, 8, -5},
			d:    171780,
		},
		{
			n:    6,
			vals: []int{-8, 10, -3, -3, 4, -8, 8, -9, -5, 0, 2, -1, 9, 7, 7, 4, 2, -5, -5, 0, 3, 9, 10, -6, -7, -10, -10, -9, -2, -6, -1, -3, -1, 1, -6, -2},
			d:    1324238,
		},
	}
	// Run the tests
	for _, test := range tests {
		// Create the parent
		R, err := NewAlgebra(test.n)
		require.NoError(t, err)
		// Compute the determinant
		x := fromIntSliceOrPanic(R, test.vals)
		if d, err := R.Determinant(x); assert.NoError(err) {
			assert.IsEqualToInt(d.(*integer.Element), test.d)
		}
		// Asking for the determinant of a matrix not in R should fail
		R2, err := NewAlgebra(test.n + 1)
		require.NoError(t, err)
		_, err = R.Determinant(R2.Zero())
		assert.Error(err)
		// Asking for the determinant of a non-matrix should fail
		_, err = R.Determinant(integer.Zero())
		assert.Error(err)
	}
}
