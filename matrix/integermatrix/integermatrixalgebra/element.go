//go:generate ../../internal/template/template.py ../../internal/template/matrixalgebra/element.txt element.go "package=integermatrixalgebra" "dir=integermatrix" "description=integer entries" "entryPackage=bitbucket.org/pcas/math/integer" "entryElement=*integer.Element"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/matrixalgebra/element.txt".
To modify this file, edit "../../internal/template/matrixalgebra/element.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Element defines a matrix with integer entries.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/integermatrix/integermatrixalgebra
/////////////////////////////////////////////////////////////////////////
// element.go
/////////////////////////////////////////////////////////////////////////

package integermatrixalgebra

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// oneOrPanic is an alias of One.
func oneOrPanic(R *Parent) *Element {
	return One(R)
}

// createMatrix returns a matrix in R with given entries S. Assumes that the entries are of the correct length, and have the correct universe.
func createMatrix(R *Parent, S entries.Interface) *Element {
	// If we've been passed the nil parent, move to the default parent
	if R == nil {
		R = defaultParent()
	}
	// Assert that the slice is of the correct length
	n := R.NumberOfRows()
	if S.NumberOfRows() != n || S.NumberOfColumns() != n {
		panic(errors.MatrixOfIncompatibleSize.New(
			S.NumberOfRows(), S.NumberOfColumns(), n, n))
	}
	// Is this the zero slice? Is it the identity slice?
	if entries.IsZero(S) {
		return Zero(R)
	} else if entries.IsIdentity(S) {
		return One(R)
	}
	// Return the new element
	return &Element{
		parent: R,
		es:     S,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// One returns the identity matrix of R.
func One(R *Parent) *Element {
	if R == nil {
		R = defaultParent()
	}
	return R.one
}

// Multiply returns the product x * y of two matrices x and y. This will return an error if x and y do not have the same parent.
func Multiply(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	R := parentOfElement(x)
	if !isParent(R, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the easy cases
	if quickIsZero(x) || quickIsZero(y) {
		return Zero(R), nil
	} else if quickIsOne(x) {
		return y, nil
	} else if quickIsOne(y) {
		return x, nil
	}
	// Perform the multiplication
	S, err := entries.RowRowMultiply(R.entriesHelper(), x.es, y.Transpose().es)
	if err != nil {
		return nil, err
	}
	return createMatrix(R, S), nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Determinant returns the determinant of the matrix x.
func (x *Element) Determinant() *integer.Element {
	d, err := getDeterminant(x)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return d
}

// Transpose returns the transpose of the matrix x.
func (x *Element) Transpose() *Element {
	// Get the zero case out of the way
	if quickIsZero(x) {
		return Zero(parentOfElement(x))
	}
	// Do we know the answer?
	var y *Element
	x.m.Lock()
	y = x.trans
	x.m.Unlock()
	// If we don't know the answer, we need to compute it
	if y == nil {
		// Compute the transpose
		R := parentOfElement(x)
		T, err := entries.Transpose(R.entriesHelper(), x.es)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		// Is x equal to its transpose?
		if areEqual, err := entries.AreEqual(R.entriesHelper(), x.es, T); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if areEqual {
			// Yes -- x is equal to its transpose
			y = x
			x.m.Lock()
			x.trans = x
			x.m.Unlock()
		} else {
			// No -- x is not equal to its transpose
			y = createMatrix(R, T)
			y.trans = x
			x.m.Lock()
			if x.trans == nil {
				x.trans = y
				if x.det != nil {
					y.det = x.det
				}
			} else {
				y = x.trans
			}
			x.m.Unlock()
		}
	}
	return y
}

// IsSquare returns true iff the matrix x is square.
func (x *Element) IsSquare() bool {
	return true
}
