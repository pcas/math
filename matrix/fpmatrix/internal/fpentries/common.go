// Common defines low-level basic functions on the entries of a matrix with entries in ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Row returns (a copy of) the i-th row of S. Rows are indexed from 0. It panics if i is out of range
func (H helper) Row(i int, S entries.Interface) []object.Element {
	// convert the type of S
	SS := S.(*fpslice)
	// grab the ground field GF(p)
	k := SS.groundField
	// Create the row
	ncols := SS.ncols
	offset := ncols * i
	row := make([]object.Element, 0, ncols)
	for j := 0; j < ncols; j++ {
		row = append(row, gfp.FromInt64(k, SS.es[offset+j]))
	}
	return row
}

// Column returns (a copy of) the i-th column of S. Columns are indexed from 0. It panics if the index i is out of range.
func (H helper) Column(i int, S entries.Interface) []object.Element {
	// convert the type of S
	SS := S.(*fpslice)
	// grab the ground field GF(p)
	k := SS.groundField
	// Create the column
	nrows := SS.nrows
	ncols := SS.ncols
	col := make([]object.Element, 0, nrows)
	for j := 0; j < nrows; j++ {
		col = append(col, gfp.FromInt64(k, SS.es[ncols*j+i]))
	}
	return col
}

// AreEqual returns true iff S1 = S2.
func (H helper) AreEqual(S1 entries.Interface, S2 entries.Interface) (bool, error) {
	// Get the easy case out of the way
	if S1 == S2 {
		return true, nil
	}
	// convert the types of S1 and S2
	SS1 := S1.(*fpslice)
	SS2 := S2.(*fpslice)
	// check more easy cases
	if SS1.nrows != SS2.nrows || SS1.ncols != SS2.ncols {
		return false, nil
	}
	k := SS1.groundField
	if k != SS2.groundField || SS1.p != SS2.p {
		return false, nil
	}
	// Start comparing the entries
	for i, x := range SS1.es {
		if x != SS2.es[i] {
			return false, nil
		}
	}
	return true, nil
}

// ZeroSlice returns a *fpslice representing the nrows x ncols zero matrix
func (H helper) ZeroSlice(nrows int, ncols int) (entries.Interface, error) {
	// Sanity check
	if err := entries.ValidateSize(nrows, ncols); err != nil {
		return nil, err
	}
	// Create the zero slice
	p := H.p
	k, err := gfp.GF(p)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	result := &fpslice{
		p:           p,
		groundField: k,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, nrows*ncols),
	}
	return result, nil
}

// IdentitySlice returns a *fpslice representing the n x n identity matrix
func (H helper) IdentitySlice(n int) (entries.Interface, error) {
	// Sanity check
	if err := entries.ValidateSize(n, n); err != nil {
		return nil, err
	}
	// create the identity slice
	p := H.p
	k, err := gfp.GF(p)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	result := &fpslice{
		p:           p,
		groundField: k,
		nrows:       n,
		ncols:       n,
		es:          make([]int64, n*n),
	}
	if p != 1 {
		for i := 0; i < n; i++ {
			result.es[i*n+i] = 1
		}
	}
	return result, nil
}
