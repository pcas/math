// Scalarmultiply defines scalar multiplication at the level of entries of a matrix over ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/mathutil"
	"bitbucket.org/pcas/math/worker"
)

// ConcurrentScalarMultiplyBlockSize is the (approximate) size of the matrices ScalarMultiplyByInteger and ScalarMultiplyByCoefficient will subdivide into.
const ConcurrentScalarMultiplyBlockSize = 300

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// ScalarMultiplyByInteger returns the (entrywise) scalar multiple of S by n
func (S *fpslice) ScalarMultiplyByInteger(n *integer.Element) entries.Interface {
	// create the result
	p := S.p
	nrows := S.nrows
	ncols := S.ncols
	nentries := nrows * ncols
	result := &fpslice{
		p:           p,
		groundField: S.groundField,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, nentries),
	}
	// if n is too big, we reduce it modulo p
	var nn int64
	var err error
	if nn, err = n.Int64(); err != nil {
		if nn, err = n.ModInt64(p); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	}
	// a function to process the i-th block of the result slice, setting the entries
	// equal to n times the corresponding entry from S, modulo p
	f := func(i int) error {
		start := i * ConcurrentScalarMultiplyBlockSize
		end := (i + 1) * ConcurrentScalarMultiplyBlockSize
		var block []int64
		if end < nentries {
			block = S.es[start:end]
		} else {
			block = S.es[start:]
		}
		for j, x := range block {
			jj := start + j
			result.es[jj] = mathutil.MulModInt64(x, nn, p)
		}
		return nil
	}
	// do the multiplication
	var numBlocks int
	if nentries%ConcurrentScalarMultiplyBlockSize == 0 {
		numBlocks = nentries / ConcurrentScalarMultiplyBlockSize
	} else {
		numBlocks = nentries/ConcurrentScalarMultiplyBlockSize + 1
	}
	if err := worker.Range(f, 0, numBlocks, 1); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return result
}

// ScalarMultiplyByCoefficient returns the (entrywise) scalar multiple of S by c
func (S *fpslice) ScalarMultiplyByCoefficient(c *gfp.Element) entries.Interface {
	// create the result
	p := S.p
	nrows := S.nrows
	ncols := S.ncols
	nentries := nrows * ncols
	result := &fpslice{
		p:           p,
		groundField: S.groundField,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, nentries),
	}
	// lift c to an int64
	cc := gfp.ToInt64(c)
	// a function to process the i-th block of the result slice, setting the entries
	// equal to n times the corresponding entry from S, modulo p
	f := func(i int) error {
		start := i * ConcurrentScalarMultiplyBlockSize
		end := (i + 1) * ConcurrentScalarMultiplyBlockSize
		var block []int64
		if end < nentries {
			block = S.es[start:end]
		} else {
			block = S.es[start:]
		}
		for j, x := range block {
			jj := start + j
			result.es[jj] = mathutil.MulModInt64(x, cc, p)
		}
		return nil
	}
	// do the multiplication
	var numBlocks int
	if nentries%ConcurrentScalarMultiplyBlockSize == 0 {
		numBlocks = nentries / ConcurrentScalarMultiplyBlockSize
	} else {
		numBlocks = nentries/ConcurrentScalarMultiplyBlockSize + 1
	}
	if err := worker.Range(f, 0, numBlocks, 1); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return result
}
