// Helper defines the helper for matrices with ZZ/pZZ entries.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
)

// helper is the helper for matrices with ZZ/pZZ entries.
type helper struct {
	p int64
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// This returns GF(p), or panics if p is not a positive prime number
func gfOrPanic(p int64) *gfp.Parent {
	result, err := gfp.GF(p)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return result
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the common parent of the entries.
func (H helper) Universe() ring.Interface {
	return gfOrPanic(H.p)
}

// FromSlice attempts to create a slice of entries for a matrix of size nrows x ncols, with entries given by the slice S.
func (H helper) FromSlice(S slice.Interface, nrows int, ncols int) (entries.Interface, error) {
	// Sanity check
	if nrows < 0 || ncols < 0 {
		return nil, errors.IllegalMatrixSize.New(nrows, ncols)
	} else if S.Len() != nrows*ncols {
		return nil, errors.SliceLengthNotEqualTo.New(S.Len(), nrows*ncols)
	}
	// the modulus and the ground field F := ZZ/pZZ
	p := H.p
	FF := gfOrPanic(p)
	// How we create the slice of entries depends on the underlying type of S
	es := make([]int64, 0, S.Len())
	switch T := S.(type) {
	case *fpslice:
		// check that the ground rings are the same
		if T.p != p {
			return nil, errors.ParentsDoNotAgree.New()
		}
		es = append(es, T.es...)
	// NB we deliberately don't special-case gfp.Slice here, because in that case we
	// would need to check that each element was in the correct ZZ/pZZ, and that is
	// handled efficiently by the default case
	case integer.IntSlice:
		for _, c := range T {
			es = append(es, int64(c)%p)
		}
	case integer.Int32Slice:
		for _, c := range T {
			es = append(es, int64(c)%p)
		}
	case integer.Int64Slice:
		for _, c := range T {
			es = append(es, c%p)
		}
	case integer.Uint64Slice:
		for _, c := range T {
			es = append(es, int64(c%uint64(p)))
		}
	case rational.Slice:
		for _, c := range T {
			x, err := gfp.FromRational(FF, c)
			if err != nil {
				return nil, err
			}
			es = append(es, gfp.ToInt64(x))
		}
	default:
		// We don't recognise the type of the slice S, so we do this slowly
		n := S.Len()
		for i := 0; i < n; i++ {
			x, err := gfp.ToElement(FF, S.Entry(i))
			if err != nil {
				return nil, err
			}
			es = append(es, gfp.ToInt64(x))
		}
	}
	// Return the new slice of entries for the matrix
	return &fpslice{
		p:           p,
		groundField: FF,
		nrows:       nrows,
		ncols:       ncols,
		es:          es,
	}, nil
}

// Helper returns a helper for matrices with entries in ZZ/pZZ.
func Helper(p int64) entries.Helper {
	// Sanity checks
	if p <= 0 {
		panic("p must be non-negative")
	} else if p != 1 && !integer.IsPrimeInt64(int64(p)) {
		panic("p must be prime")
	}
	// create the helper and return
	return helper{
		p: p,
	}
}
