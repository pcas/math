// Echelon defines functions to compute echelon form, at the level of entries of a matrix, for matrices over ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/mathutil"
	"bitbucket.org/pcastools/sort"
	"runtime"
)

// A type representing a slice of rows
type rowSlice [][]int64

func (S rowSlice) Swap(i int, j int) {
	S[i], S[j] = S[j], S[i]
}

// the number of cores available
var nCPUs int = runtime.NumCPU()

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// inverseModP returns the inverse mod p of x.  It is assumed that p is a positive prime and that x lies in the range 0..p-1.
func inverseModP(x int64, p int64) int64 {
	gcd, inv, _ := mathutil.XGCDOfPairInt64(x, p)
	if gcd != 1 {
		// By assumption, p and x are coprime
		panic(fatal.ImpossibleToGetHere()) // This should never happen
	}
	return inv
}

// normalizeAndReturnPivot returns the location of the pivot in row, or len(row)
// if row contains only zeroes.  If there is a pivot it also rescales the
// entries of row, in place, such that the pivot is 1, working modulo the prime
// p.  The entries of row are assumed to lie in the range 0..p-1, and p is
// assumed to be a positive prime.
func normalizeAndReturnPivot(row []int64, p int64) int {
	// find the pivot
	nentries := len(row)
	var entry int64
	var i int
	for i = 0; i < nentries; i++ {
		if entry = row[i]; entry != 0 {
			break
		}
	}
	// if the row is non-zero then rescale it, unless the pivot is already 1
	if i != nentries && entry != 1 {
		inv := inverseModP(entry, p)
		row[i] = 1
		for j, x := range row[i+1:] {
			if x != 0 {
				row[i+1+j] = mathutil.MulModInt64(x, inv, p)
			}
		}
	}
	// return the pivot, or nentries if there was no pivot
	return i
}

// rowOperation replaces the row y, in place, with a x + y mod p where a is chosen so
// as to zero the i-th entry in y.  It is assumed that i is a pivot for x, that is, that
// x[i]==1, that p is a positive prime, that x and y are slices of the same length, and
// that this length is not zero.
func rowOperation(x []int64, i int, y []int64, p int64) {
	a := -y[i]
	// is there anything to do?
	if a == 0 {
		return
	}
	// do the operation
	for j, b := range x {
		y[j] = mathutil.MulThenAddModInt64(a, b, y[j], p)
	}
}

// rowEchelonFormSingleThreaded returns S in reduced row echelon form
func rowEchelonFormSingleThreaded(S *fpslice) *fpslice {
	// create the result
	nrows := S.nrows
	ncols := S.ncols
	nentries := nrows * ncols
	p := S.p
	result := &fpslice{
		p:           p,
		groundField: S.groundField,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, 0, nentries),
	}
	// working space: these will eventually hold the rows of the echelon form
	efRows := make(rowSlice, 0, nrows)
	for i := 0; i < nrows; i++ {
		row := make([]int64, ncols)
		copy(row, S.es[i*ncols:(i+1)*ncols])
		efRows = append(efRows, row)
	}
	// the location of the pivots
	pivots := make([]int, 0, nrows)
	// normalize the rows and record the pivots
	for _, row := range efRows {
		// reduce with respect to the known pivot rows
		for i, piv := range pivots {
			if piv != ncols {
				rowOperation(efRows[i], piv, row, p)
			}
		}
		// find the pivot
		pivot := normalizeAndReturnPivot(row, p)
		// if we found a pivot, reduce the known pivot rows with respect to this row
		if pivot != ncols {
			for i, _ := range pivots {
				rowOperation(row, pivot, efRows[i], p)
			}
		}
		// record this pivot and continue
		pivots = append(pivots, pivot)
	}
	// sort the rows so that the pivots are in increasing order
	sort.ParallelSort(sort.IntSlice(pivots), efRows)
	// copy the sorted rows into result and return
	for _, row := range efRows {
		result.es = append(result.es, row...)
	}
	return result
}

// rowEchelonFormConcurrent returns S in reduced row echelon form
func rowEchelonFormConcurrent(S *fpslice) *fpslice {
	//////////////////////////////////////////////////////////////////////
	// create the result
	//////////////////////////////////////////////////////////////////////
	nrows := S.nrows
	ncols := S.ncols
	nentries := nrows * ncols
	p := S.p
	result := &fpslice{
		p:           p,
		groundField: S.groundField,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, 0, nentries),
	}
	//////////////////////////////////////////////////////////////////////
	// working space: these will eventually hold the rows of the echelon
	// form
	//////////////////////////////////////////////////////////////////////
	efRows := make(rowSlice, 0, nrows)
	for i := 0; i < nrows; i++ {
		row := make([]int64, ncols)
		copy(row, S.es[i*ncols:(i+1)*ncols])
		efRows = append(efRows, row)
	}
	//////////////////////////////////////////////////////////////////////
	// A type representing a row-reduction step
	//////////////////////////////////////////////////////////////////////
	type reductionStep struct {
		rowIdx int // the index in efRows of the row to reduce
		state  int // this row is reduced with respect to the first state pivot rows

		// reducedRows and pivots are parallel slices of, respectively,
		// pivot rows to reduce with respect to and the pivots in these rows
		reducedRows [][]int64
		pivots      []int
	}
	//////////////////////////////////////////////////////////////////////
	// The first step puts the matrix into row-echelon form (not reduced
	// row-echelon form).  How this works:
	//
	// the workers listen on channel input.  If that channel is closed
	// then the worker shuts down.  If it reads a reductionStep from input
	// then for each row in reducedRows it reduces efRows[rowIdx] with
	// respect to row, pivoting at the corresponding element of pivots.
	// It then adds len(reducedRows) to state, passes the modified
	// reductionStep down the channel output, and listens again on input
	//
	// the main routine maintains variables:
	//
	// allPivots: a slice of indices of all pivots found, where the pivot
	//            in a zero row is defined to be nrows
	// pivotRows: a slice containing the pivot rows (which are just copies
	//            of the relevant entries of efRows)
	// pivotRowPivots: a slice containing the indices of the pivots in
	//                 the corresponding rows of pivotRows
	// numPivotRows: the number of pivot rows
	// numZeroRows: the number of zero rows
	//
	// the main routine listens on the worker-output channel output.
	// When it reads a reductionStep x from output it looks to see if
	// x.state is equal to len(pivotRows).  If not then we need to reduce
	// efRows[x.rowIdx] with respect to more pivot rows, so we set
	// x.reducedRows equal to pivotRows[x.state:], x.pivots equal to
	// pivotRowPivots[x.state:], and then pass x back down input.
	// Otherwise we are done reducing efRows[x.rowIdx], so we check if it
	// is zero.  If so we increment the number of zero rows and continue
	// listening on output.  If not then we normalize it, add a copy of
	// it to pivotRows, add its pivot to allPivots and pivotRowPivots,
	// increment the number of pivot rows, and continue listening on
	// output.
	//
	// The initialization step is to feed all rows, marked as having
	// state 0, to the worker output channel.
	//
	// Eventually all rows are removed from the input, so are either
	// zero or pivot rows, and at that point we stop.
	//////////////////////////////////////////////////////////////////////
	// the communication channels
	// make the communication channels
	input := make(chan *reductionStep, nCPUs)
	output := make(chan *reductionStep, nrows)
	// the worker
	workerReductionStep := func(input <-chan *reductionStep, output chan<- *reductionStep) {
		for x := range input {
			// do the row reductions specified by x
			for i, redRow := range x.reducedRows {
				rowOperation(redRow, x.pivots[i], efRows[x.rowIdx], p)
			}
			// update the state
			x.state += len(x.reducedRows)
			// pass x down the output channel and continue
			output <- x
		}
	}
	// launch the workers
	for i := 0; i < nCPUs; i++ {
		go workerReductionStep(input, output)
	}
	// we initialize by feeding all the rows into the worker-output channel
	go func() {
		for i, _ := range efRows {
			output <- &reductionStep{
				rowIdx: i,
				state:  0,
			}
		}
	}()
	// the location of the pivots
	allPivots := make([]int, nrows)
	// the number of zero rows and pivot rows
	numZeroRows := 0
	numPivotRows := 0
	// the pivot rows, and the pivots therein
	pivotRows := make([][]int64, 0, nrows)
	pivotRowPivots := make([]int, 0, nrows)
	// do the computation
	for numZeroRows+numPivotRows < nrows {
		x := <-output
		if x.state < numPivotRows {
			// we need to reduce x with respect to the rest of the pivot rows
			// so indicate which rows to reduce with respect to
			x.reducedRows = pivotRows[x.state:]
			x.pivots = pivotRowPivots[x.state:]
			// and pass x back to the workers
			input <- x
		} else {
			// x is reduced with respect to all the pivot rows we know
			thisRow := efRows[x.rowIdx]
			pivot := normalizeAndReturnPivot(thisRow, p)
			allPivots[x.rowIdx] = pivot
			// was this row zero?
			if pivot == ncols {
				// yes, it was zero
				numZeroRows++
			} else {
				// no, so this is a new pivot row
				pivotRows = append(pivotRows, thisRow)
				pivotRowPivots = append(pivotRowPivots, pivot)
				numPivotRows++
			}
		}
	}
	// shut down the workers
	close(input)
	//////////////////////////////////////////////////////////////////////
	// the second step clears pivot columns upwards so as to put the
	// matrix in reduced row echelon form.  This works much as for the
	// first step.  The main differences are:
	// * we process the pivot rows only, in reverse order; and
	// * we use a WaitGroup to ensure that pivot row k is processed
	//   completely before pivot row k-1 is touched
	// So:
	//
	// the workers listen on the channel clearUpwardsQueue.  If this
	// channel is closed then they shut down.  If a worker reads a
	// clearUpwardsStep from clearUpwardsQueue, which is:
	//
	// rowIdx
	// reducedRowIdx
	// pivot
	//
	// then it reduces pivotRows[rowIdx] with respect to
	// pivotRows[reducedRowIdx], pivoting on pivot.  It then decrements
	// the WaitGroup and listens again.
	//
	// the main routine works backwards through the pivot rows starting
	// with the penultimate one (because the last one is already fully
	// reduced).  It adds len(pivotRows) - indexOfCurrentPivotRow to
	// the WaitGroup, then pushes clearUpwardsSteps onto clearUpwardsQueue
	// with instructions to reduce all pivot rows below the current pivot
	// row with respect to the current pivot row, then waits on the
	// WaitGroup (so that all these reductions complete before continuing)
	// and then continues
	//////////////////////////////////////////////////////////////////////
	//
	// FIX ME there is lots of scope for optimisation here.  We could do
	// the reductions in blocks, rather tham one per clearUpwardsStep.
	// Also for the first few rows there will be very few tasks and so
	// most of the workers will be blocked.  It might be worth only
	// launching all the workers once there are enough rows to keep them
	// busy.
	//
	//////////////////////////////////////////////////////////////////////
	// a channel for synchronization
	done := make(chan struct{})
	// a type representing a clearing upwards step
	type clearUpwardsStep struct {
		rowIdx        int // the index in pivotRows of the row to clear
		reducedRowIdx int // the index in pivotRows of the row to clear with
		pivot         int // the index of the pivot in PivotRows[reducedRowIdx]
	}
	// a channel to pass row-reduction instructions to the workers
	clearUpwardsQueue := make(chan *clearUpwardsStep, numPivotRows)
	// the worker
	workerClearUpwardsStep := func(input <-chan *clearUpwardsStep) {
		for x := range input {
			// do the row reduction specified by x
			rowOperation(pivotRows[x.reducedRowIdx], x.pivot, pivotRows[x.rowIdx], p)
			// decrement the WaitGroup and continue
			done <- struct{}{}
		}
	}
	// launch the workers
	for i := 0; i < nCPUs; i++ {
		go workerClearUpwardsStep(clearUpwardsQueue)
	}
	// feed the column-clearing instructions to the workers, waiting between
	// those instructions that modify the same pivot row
	for i := numPivotRows - 1; i >= 1; i-- {
		pivot := pivotRowPivots[i]
		for j := 0; j < i; j++ {
			clearUpwardsQueue <- &clearUpwardsStep{
				rowIdx:        j,
				reducedRowIdx: i,
				pivot:         pivot,
			}
		}
		// wait for thus batch of operations to complete
		for j := 0; j < i; j++ {
			<-done
		}
	}
	//////////////////////////////////////////////////////////////////////
	// sort the rows so that the pivots are in increasing order
	//////////////////////////////////////////////////////////////////////
	sort.ParallelSort(sort.IntSlice(allPivots), efRows)
	// copy the sorted rows into result and return
	for _, row := range efRows {
		result.es = append(result.es, row...)
	}
	return result
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

func (S *fpslice) EchelonForm() entries.Interface {
	return rowEchelonFormConcurrent(S)
}
