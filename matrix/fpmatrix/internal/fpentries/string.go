// String contains pretty-printing functions for the entries of a matrix over ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"strconv"
)

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// PrettyPrint returns a string representation of the i-th entry in S
func (S *fpslice) PrettyPrint(i int) string {
	return strconv.FormatInt(S.es[i], 10)
}
