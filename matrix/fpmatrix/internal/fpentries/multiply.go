// multiply defines basic functions for multiplication of matrices with ZZ/pZZ entries.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/mathutil"
	"bitbucket.org/pcas/math/worker"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

func (H helper) RowRowMultiply(S1 entries.Interface, S2 entries.Interface) (entries.Interface, error) {
	// convert the slice types: S1 and S2 are actually *fpslices
	SS1 := S1.(*fpslice)
	SS2 := S2.(*fpslice)
	// sanity checks
	ncols := SS1.ncols
	if ncols != SS2.ncols {
		return nil, errors.NumberOfColumnsOfMatricesDoNotAgree.New(ncols, SS2.ncols)
	}
	if SS1.p != SS2.p || SS1.groundField != SS2.groundField {
		return nil, errors.ParentsOfEntriesDoNotAgree.New()
	}
	// create the result
	nentries := SS1.nrows * SS2.nrows
	result := &fpslice{
		p:           SS1.p,
		groundField: SS1.groundField,
		nrows:       SS1.nrows,
		ncols:       SS2.nrows,
		es:          make([]int64, nentries),
	}
	// a function to set the entries of result equal to the dot products of the rows
	f := func(n int) error {
		// the row and column indices
		i := n / result.ncols
		j := n % result.ncols
		// the rows
		rowS1 := SS1.es[i*SS1.ncols : (i+1)*SS1.ncols]
		rowS2 := SS2.es[j*SS2.ncols : (j+1)*SS2.ncols]
		result.es[n] = mathutil.DotProductInt64ModInt64(rowS1, rowS2, result.p)
		return nil
	}
	// do the row multiplications
	if err := worker.Range(f, 0, nentries, 1); err != nil {
		return nil, err
	}
	return result, nil
}
