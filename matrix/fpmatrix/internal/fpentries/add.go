// Add defines addition, subtraction, and negation at the level of entries of a matrix, for matrices over ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/mathutil"
	"bitbucket.org/pcas/math/worker"
)

// ConcurrentAddBlockSize is the (approximate) size of the matrices Add will subdivide into.
const ConcurrentAddBlockSize = 500

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// validateSlicesAreCompatible returns true if and only if S1 and S2 represent matrices that can be added or subtracted.  That is, it returns true iff the ground fields and dimensions of the matrices represented by S1 and S2 are the same.  If false, it also returns an error describing the failure.
func validateSlicesAreCompatible(S1 *fpslice, S2 *fpslice) (bool, error) {
	if S1.p != S2.p || S1.groundField != S2.groundField {
		return false, errors.ParentsDoNotAgree.New()
	} else if S1.nrows != S2.nrows || S1.ncols != S2.ncols {
		return false, errors.MatrixSizesMustAgree.New(S1.nrows, S1.ncols, S2.nrows, S2.ncols)
	}
	return true, nil
}

// addOrSubtract returns either the sum S1 + S2 or the difference S1 - S2.  The argument c must be 1 or -1.  If c is 1, the function returns S1 + S2; if c is -1, the function returns S1 - S2.
func addOrSubtract(S1 entries.Interface, S2 entries.Interface, c int64) (entries.Interface, error) {
	// convert the types of S1 and S2
	SS1 := S1.(*fpslice)
	SS2 := S2.(*fpslice)
	// sanity checks
	if c != 1 && c != -1 {
		panic("The third argument must be +1 or -1.")
	} else if ok, err := validateSlicesAreCompatible(SS1, SS2); !ok {
		return nil, err
	}
	// create the result
	nrows := SS1.nrows
	ncols := SS1.ncols
	nentries := nrows * ncols
	p := SS1.p
	result := &fpslice{
		p:           p,
		groundField: SS1.groundField,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, nentries),
	}
	// a function to process the i-th block of the result slice, setting the entries
	// equal to the sum of the corresponding entries from SS1 and SS2, modulo p
	f := func(i int) error {
		start := i * ConcurrentAddBlockSize
		end := (i + 1) * ConcurrentAddBlockSize
		var block []int64
		if end < nentries {
			block = SS1.es[start:end]
		} else {
			block = SS1.es[start:]
		}
		for j, x := range block {
			jj := start + j
			result.es[jj] = mathutil.AddModInt64(x, c*SS2.es[jj], p)
		}
		return nil
	}
	// do the addition
	var numBlocks int
	if nentries%ConcurrentAddBlockSize == 0 {
		numBlocks = nentries / ConcurrentAddBlockSize
	} else {
		numBlocks = nentries/ConcurrentAddBlockSize + 1
	}
	if err := worker.Range(f, 0, numBlocks, 1); err != nil {
		return nil, err
	}
	return result, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns the sum S1 + S2
func (H helper) Add(S1 entries.Interface, S2 entries.Interface) (entries.Interface, error) {
	return addOrSubtract(S1, S2, 1)
}

// Subtract returns the difference S1 - S2
func (H helper) Subtract(S1 entries.Interface, S2 entries.Interface) (entries.Interface, error) {
	return addOrSubtract(S1, S2, -1)
}

// Negate negates the matrix represented by S
func (S *fpslice) Negate() entries.Interface {
	// make the result
	p := S.p
	result := &fpslice{
		p:           p,
		groundField: S.groundField,
		nrows:       S.nrows,
		ncols:       S.ncols,
		es:          make([]int64, 0, len(S.es)),
	}
	// populate the entries
	for _, x := range S.es {
		if x != 0 {
			result.es = append(result.es, p-x)
		} else {
			result.es = append(result.es, 0)
		}
	}
	return result
}

// Sum returns the sum of the matrices described by the slices of entries Ss.  If the slice is empty, an error is returned.
func (H helper) Sum(Ss []entries.Interface) (entries.Interface, error) {
	// deal with the easy cases
	n := len(Ss)
	if n == 0 {
		return nil, errors.EmptySlice.New()
	} else if n == 1 {
		return Ss[0], nil
	} else if n == 2 {
		return H.Add(Ss[0], Ss[1])
	}
	// convert the slice types
	SSs := make([]*fpslice, 0, n)
	for _, S := range Ss {
		SSs = append(SSs, S.(*fpslice))
	}
	// check that the parents agree
	p := SSs[0].p
	k := SSs[0].groundField
	nrows := SSs[0].nrows
	ncols := SSs[0].ncols
	nentries := nrows * ncols
	for _, SS := range SSs[1:] {
		if SS.p != p || SS.groundField != k || SS.nrows != nrows || SS.ncols != ncols {
			return nil, errors.ParentsDoNotAgree.New()
		}
	}
	// make the result
	result := &fpslice{
		p:           p,
		groundField: k,
		nrows:       nrows,
		ncols:       ncols,
		es:          make([]int64, nentries),
	}
	//////////////////////////////////////////////////////////////////////
	// Do the sum.  We reduce only where necessary.
	//////////////////////////////////////////////////////////////////////
	// find the least integer pBits such that p<2^pBits
	pBits := uint(1)
	for x := p >> 1; x > 0; x = x >> 1 {
		pBits += 1
	}
	// if we do k additions then the total is less than k*2^pBits
	numberOfSafeAdditions := 1 << (63 - pBits)
	// a function to set the i-th entry in result equal to the i-th entry in the sum
	f := func(i int) error {
		count := 0
		for _, SS := range SSs {
			if count < numberOfSafeAdditions {
				result.es[i] += SS.es[i]
				count += 1
			} else {
				result.es[i] = mathutil.AddModInt64(result.es[i], SS.es[i], p)
				count = 0
			}
		}
		return nil
	}
	// do the sum
	if err := worker.Range(f, 0, nentries, 1); err != nil {
		return nil, err
	}
	return result, nil
}
