// Slice defines the slice of entries of a matrix over ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpentries

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
)

// fpslice is a slice of entries of a matrix over ZZ/pZZ.
type fpslice struct {
	p           int64
	groundField *gfp.Parent // GF(p)
	nrows       int         // The number of rows
	ncols       int         // The number of columns
	es          []int64     // The entries of the matrix, in row-major order, with 0 <= entry < p
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Universe returns the common parent of the entries in the slice.
func (S *fpslice) Universe() ring.Interface {
	if S == nil {
		return gfOrPanic(1)
	}
	return S.groundField
}

// Len returns the length of the slice.
func (S *fpslice) Len() int {
	if S == nil {
		return 0
	}
	return len(S.es)
}

// NumberOfRows returns the number of rows of the matrix represented by the entries in the slice.
func (S *fpslice) NumberOfRows() int {
	if S == nil {
		return 0
	}
	return S.nrows
}

// NumberOfColumns returns the number of columns of the matrix represented by the entries in the slice.
func (S *fpslice) NumberOfColumns() int {
	if S == nil {
		return 0
	}
	return S.ncols
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *fpslice) Entry(i int) object.Element {
	return gfp.FromInt64(S.groundField, S.es[i])
}

// IsZero returns true if and only if the matrix defined by S is zero.
func (S *fpslice) IsZero() bool {
	for _, x := range S.es {
		if x != 0 {
			return false
		}
	}
	return true
}

// IsIdentity returns true if and only if the matrix defined by S is the identity matrix.
func (S *fpslice) IsIdentity() bool {
	// deal with the easy cases
	ncols := S.ncols
	if S.nrows != ncols {
		return false
	} else if S.p == 1 {
		return true
	}
	// check the entries
	for i := 0; i < S.ncols; i++ {
		for j := 0; j < S.ncols; j++ {
			entry := S.es[i*ncols+j]
			if (i == j && entry != 1) || (i != j && entry != 0) {
				return false
			}
		}
	}
	return true
}

// Transpose returns the *fpslice that represents the transpose of the matrix defined by S
func (S *fpslice) Transpose() entries.Interface {
	nrows := S.nrows
	ncols := S.ncols
	result := &fpslice{
		p:           S.p,
		groundField: S.groundField,
		nrows:       ncols,
		ncols:       nrows,
		es:          make([]int64, 0, nrows*ncols),
	}
	// populate the result slice
	for j := 0; j < ncols; j++ {
		for i := 0; i < nrows; i++ {
			result.es = append(result.es, S.es[i*ncols+j])
		}
	}
	return result
}
