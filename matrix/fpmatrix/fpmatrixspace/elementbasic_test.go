// elementbasic_test contains tests for elementbasic.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcastools/hash"
	"math"
	"testing"
)

// TestString tests String
func TestString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// make some matrices.
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]*integer.Element, 0, 24)
	for i := 0; i < 24; i++ {
		entrySlice = append(entrySlice, integer.FromInt(i))
	}
	// M will be a slice of matrices.  The last element of M is the nil
	// *Element.  Each of the other elements is a 4x6 matrix over one of
	// the fields above with entries 0,1,...,23
	Ms := make([]*Element, 5)
	for i, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		if MM, err := NewSpace(k, 4, 6); assert.NoError(err) {
			Ms[i], err = FromIntegerSlice(MM, entrySlice)
			assert.NoError(err)
		}
	}
	//////////////////////////////////////////////////////////////////////
	// check that these produce the correct strings
	//////////////////////////////////////////////////////////////////////
	assert.Equal(Ms[0].String(), "⎛ 0  1  2  3  4  5⎞\n⎜ 6  7  8  9 10 11⎟\n⎜12 13 14 15 16 17⎟\n⎝18  0  1  2  3  4⎠")
	assert.Equal(Ms[1].String(), "⎛ 0  1  2  3  4  5⎞\n⎜ 6  7  8  9 10 11⎟\n⎜12 13 14 15 16 17⎟\n⎝18 19 20 21 22 23⎠")
	assert.Equal(Ms[2].String(), "⎛0 0 0 0 0 0⎞\n⎜0 0 0 0 0 0⎟\n⎜0 0 0 0 0 0⎟\n⎝0 0 0 0 0 0⎠")
	assert.Equal(Ms[3].String(), "⎛0 0 0 0 0 0⎞\n⎜0 0 0 0 0 0⎟\n⎜0 0 0 0 0 0⎟\n⎝0 0 0 0 0 0⎠")
	assert.Equal(Ms[4].String(), "()")
	//////////////////////////////////////////////////////////////////////
	// check the unique element of the nil matrix space
	//////////////////////////////////////////////////////////////////////
	var nilMatrixSpace *Parent
	assert.Equal(Zero(nilMatrixSpace).String(), "()")
}

// TestAreEqual tests AreEqual
func TestAreEqual(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// make some matrix spaces and matrices
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]int64, 0, 48)
	for i := int64(0); i < 48; i++ {
		entrySlice = append(entrySlice, i)
	}
	Ms := make([]*Parent, 0, 4)
	As := make([]*Element, 0, 4)
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		if M, err := NewSpace(k, 4, 12); assert.NoError(err) {
			Ms = append(Ms, M)
			if A, err := FromInt64Slice(M, entrySlice); assert.NoError(err) {
				As = append(As, A)
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// some tests
	//////////////////////////////////////////////////////////////////////
	for i, A := range As {
		for j, B := range As {
			if i == j || (i == 2 && j == 3) || (i == 3 && j == 2) {
				assert.True(AreEqual(A, B))
				assert.True(AreEqual(B, A))
			} else {
				assert.False(AreEqual(A, B))
				assert.False(AreEqual(B, A))
			}
		}
		if B, err := FromInt64Slice(Ms[i], entrySlice); assert.NoError(err) {
			assert.True(AreEqual(A, B))
			assert.True(AreEqual(B, A))
		}
	}
	for _, k := range []*gfp.Parent{k1, k2} {
		M, err := NewSpace(k, 4, 4)
		assert.NoError(err)
		one, err := One(M)
		assert.NoError(err)
		assert.False(AreEqual(Zero(M), one))
		assert.False(AreEqual(one, Zero(M)))
		assert.True(AreEqual(Zero(M), Zero(M)))
		assert.True(AreEqual(one, one))
	}
	for _, k := range []*gfp.Parent{k3, nilField} {
		M, err := NewSpace(k, 4, 4)
		assert.NoError(err)
		one, err := One(M)
		assert.NoError(err)
		assert.True(AreEqual(Zero(M), one))
		assert.True(AreEqual(one, Zero(M)))
		assert.True(AreEqual(Zero(M), Zero(M)))
		assert.True(AreEqual(one, one))
	}
}

// TestNegate tests Negate
func TestNegate(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	entrySlice := []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		M, err := NewSpace(k, 5, 2)
		assert.NoError(err)
		assert.IsZero(Zero(M).Negate())
		if A, err := FromInt64Slice(M, entrySlice); assert.NoError(err) {
			if B, err := Add(A, A.Negate()); assert.NoError(err) {
				assert.IsZero(B)
			}
		}
	}
}

// TestPower tests Power and PowerInt64
func TestPower(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]int64, 0, 16)
	for i := int64(0); i < 16; i++ {
		entrySlice = append(entrySlice, i)
	}
	veryLargePower := integer.FromInt64(math.MaxInt64).Increment()
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		//////////////////////////////////////////////////////////////////////
		// taking the power of a non-square matrix gives an error
		//////////////////////////////////////////////////////////////////////
		M, err := NewSpace(k, 2, 8)
		assert.NoError(err)
		A, err := FromInt64Slice(M, entrySlice)
		assert.NoError(err)
		// tests for Power
		_, err = A.Power(integer.Zero())
		assert.Error(err)
		_, err = A.Power(integer.One())
		assert.Error(err)
		_, err = A.Power(integer.FromInt(5))
		assert.Error(err)
		// tests for PowerInt64
		_, err = A.PowerInt64(0)
		assert.Error(err)
		_, err = A.PowerInt64(1)
		assert.Error(err)
		_, err = A.PowerInt64(5)
		assert.Error(err)
		//////////////////////////////////////////////////////////////////////
		// the zero-th power of the zero matrix is the identity
		//////////////////////////////////////////////////////////////////////
		M, err = NewSpace(k, 4, 4)
		// test for Power
		if B, err := Zero(M).Power(integer.Zero()); assert.NoError(err) {
			assert.IsOne(B)
		}
		// test for PowerInt64
		if B, err := Zero(M).PowerInt64(0); assert.NoError(err) {
			assert.IsOne(B)
		}
		//////////////////////////////////////////////////////////////////////
		// positive powers of the zero matrix are zero
		//////////////////////////////////////////////////////////////////////
		for _, n := range []int{1, 5, 10} {
			// tests for Power
			if B, err := Zero(M).Power(integer.FromInt(n)); assert.NoError(err) {
				assert.IsZero(B)
			}
			// tests for PowerInt64
			if B, err := Zero(M).PowerInt64(int64(n)); assert.NoError(err) {
				assert.IsZero(B)
			}
		}
		//////////////////////////////////////////////////////////////////////
		// a very large power of the zero matrix is zero
		//////////////////////////////////////////////////////////////////////
		if B, err := Zero(M).Power(veryLargePower); assert.NoError(err) {
			assert.IsZero(B)
		}
		//////////////////////////////////////////////////////////////////////
		// negative powers of the zero matrix error, unless we are over GF(1)
		//////////////////////////////////////////////////////////////////////
		for _, n := range []int{-1, -5, -10} {
			if k.CharacteristicInt64() != 1 {
				// tests for Power
				_, err := Zero(M).Power(integer.FromInt(n))
				assert.Error(err)
				// tests for PowerInt64
				_, err = Zero(M).PowerInt64(int64(n))
				assert.Error(err)
			} else {
				// tests for Power
				if B, err := Zero(M).Power(integer.FromInt(n)); assert.NoError(err) {
					assert.IsZero(B)
					assert.IsOne(B)
				}
				// tests for PowerInt64
				if B, err := Zero(M).PowerInt64(int64(n)); assert.NoError(err) {
					assert.IsZero(B)
					assert.IsOne(B)
				}
			}
		}
		//////////////////////////////////////////////////////////////////////
		// powers of the identity matrix are the identity
		//////////////////////////////////////////////////////////////////////
		one, err := One(M)
		assert.NoError(err)
		for _, n := range []int{-10, -5, -1, 0, 1, 5, 10} {
			// tests for Power
			if B, err := one.Power(integer.FromInt(n)); assert.NoError(err) {
				assert.IsOne(B)
			}
			// tests for PowerInt64
			if B, err := one.PowerInt64(int64(n)); assert.NoError(err) {
				assert.IsOne(B)
			}
		}
		//////////////////////////////////////////////////////////////////////
		// a very large power of the identity matrix is the identity
		//////////////////////////////////////////////////////////////////////
		if B, err := one.Power(veryLargePower); assert.NoError(err) {
			assert.IsOne(B)
		}
		//////////////////////////////////////////////////////////////////////
		// powers of the 4x4 matrix with entries 0..15 (in row-major order)
		//////////////////////////////////////////////////////////////////////
		A, err = FromInt64Slice(M, entrySlice)
		assert.NoError(err)
		// tests for Power
		if B, err := A.Power(integer.Zero()); assert.NoError(err) {
			assert.IsOne(B)
		}
		if B, err := A.Power(integer.One()); assert.NoError(err) {
			assert.AreEqual(M, A, B)
		}
		if B, err := A.Power(integer.FromInt(5)); assert.NoError(err) {
			if expected, err := FromInt64Slice(M, []int64{1780800, 2050000, 2319200, 2588400, 5147200, 5925200, 6703200, 7481200, 8513600, 9800400, 11087200, 12374000, 11880000, 13675600, 15471200, 17266800}); assert.NoError(err) {
				assert.AreEqual(M, expected, B)
			}
		}
		if B, err := A.Power(integer.FromInt(10)); assert.NoError(err) {
			if expected, err := FromInt64Slice(M, []int64{64217941760000, 73924310720000, 83630679680000, 93337048640000, 185609342720000, 213663695040000, 241718047360000, 269772399680000, 307000743680000, 353403079360000, 399805415040000, 446207750720000, 428392144640000, 493142463680000, 557892782720000, 622643101760000}); assert.NoError(err) {
				assert.AreEqual(M, expected, B)
			}
		}
		// tests for PowerInt64
		if B, err := A.PowerInt64(0); assert.NoError(err) {
			assert.IsOne(B)
		}
		if B, err := A.PowerInt64(1); assert.NoError(err) {
			assert.AreEqual(M, A, B)
		}
		if B, err := A.PowerInt64(5); assert.NoError(err) {
			if expected, err := FromInt64Slice(M, []int64{1780800, 2050000, 2319200, 2588400, 5147200, 5925200, 6703200, 7481200, 8513600, 9800400, 11087200, 12374000, 11880000, 13675600, 15471200, 17266800}); assert.NoError(err) {
				assert.AreEqual(M, expected, B)
			}
		}
		if B, err := A.PowerInt64(10); assert.NoError(err) {
			if expected, err := FromInt64Slice(M, []int64{64217941760000, 73924310720000, 83630679680000, 93337048640000, 185609342720000, 213663695040000, 241718047360000, 269772399680000, 307000743680000, 353403079360000, 399805415040000, 446207750720000, 428392144640000, 493142463680000, 557892782720000, 622643101760000}); assert.NoError(err) {
				assert.AreEqual(M, expected, B)
			}
		}
		//////////////////////////////////////////////////////////////////////
		// a very large power of a non-zero, non-unit 4x4 matrix gives an
		// error, unless we are over GF(1)
		//////////////////////////////////////////////////////////////////////
		if k.CharacteristicInt64() != 1 {
			_, err = A.Power(veryLargePower)
			assert.Error(err)
		} else {
			if B, err := A.Power(veryLargePower); assert.NoError(err) {
				assert.IsZero(B)
			}
		}
	}
}

// TestScalarMultiply tests ScalarMultiplyByInteger, ScalarMultiplyByInt64, and ScalarMultiplyByCoefficient
func TestScalarMultiply(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// prepare the entry slice and some scalars
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]int64, 0, 16)
	for i := int64(0); i < 16; i++ {
		entrySlice = append(entrySlice, i)
	}
	// a large scalar and its reductions into k1, k2, k3, nilField
	largeInteger := integer.FromInt64(math.MaxInt64).Increment()
	largeIntegerModP := []int64{18, 1151568, 0, 0}
	//////////////////////////////////////////////////////////////////////
	// some tests
	//////////////////////////////////////////////////////////////////////
	for i, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		//////////////////////////////////////////////////////////////////////
		// make a 2x8 matrix with entries 0..15 (in row-major order)
		//////////////////////////////////////////////////////////////////////
		M, err := NewSpace(k, 2, 8)
		assert.NoError(err)
		elementOfM, err := FromInt64Slice(M, entrySlice)
		assert.NoError(err)
		for _, A := range []*Element{elementOfM, Zero(M)} {
			//////////////////////////////////////////////////////////////////////
			// multiplying by zero gives zero
			//////////////////////////////////////////////////////////////////////
			assert.IsZero(A.ScalarMultiplyByInteger(integer.Zero()))
			assert.IsZero(A.ScalarMultiplyByInt64(0))
			if B, err := A.ScalarMultiplyByCoefficient(gfp.Zero(k)); assert.NoError(err) {
				assert.IsZero(B)
			}
			//////////////////////////////////////////////////////////////////////
			// multiplying by 1 is the identity map
			//////////////////////////////////////////////////////////////////////
			assert.AreEqual(M, A, A.ScalarMultiplyByInteger(integer.One()))
			assert.AreEqual(M, A, A.ScalarMultiplyByInt64(1))
			if B, err := A.ScalarMultiplyByCoefficient(gfp.One(k)); assert.NoError(err) {
				assert.AreEqual(M, A, B)
			}
			//////////////////////////////////////////////////////////////////////
			// multiplying by -1 is negation
			//////////////////////////////////////////////////////////////////////
			assert.AreEqual(M, A.Negate(), A.ScalarMultiplyByInteger(integer.FromInt(-1)))
			assert.AreEqual(M, A.Negate(), A.ScalarMultiplyByInt64(-1))
			if B, err := A.ScalarMultiplyByCoefficient(gfp.One(k).Negate()); assert.NoError(err) {
				assert.AreEqual(M, A.Negate(), B)
			}
			// sanity check
			if B, err := Add(A.ScalarMultiplyByInteger(integer.FromInt(-1)), A); assert.NoError(err) {
				assert.IsZero(B)
			}
			if B, err := Add(A.ScalarMultiplyByInt64(-1), A); assert.NoError(err) {
				assert.IsZero(B)
			}
			if B, err := A.ScalarMultiplyByCoefficient(gfp.One(k).Negate()); assert.NoError(err) {
				if C, err := Add(B, A); assert.NoError(err) {
					assert.IsZero(C)
				}
			}
			//////////////////////////////////////////////////////////////////////
			// multiplying by 3 gives what you expect
			//////////////////////////////////////////////////////////////////////
			if twoA, err := Add(A, A); assert.NoError(err) {
				if threeA, err := Add(twoA, A); assert.NoError(err) {
					assert.AreEqual(M, threeA, A.ScalarMultiplyByInteger(integer.FromInt(3)))
					assert.AreEqual(M, threeA, A.ScalarMultiplyByInt64(3))
					if B, err := A.ScalarMultiplyByCoefficient(gfp.FromInt(k, 3)); assert.NoError(err) {
						assert.AreEqual(M, threeA, B)
					}
				}
			}
			//////////////////////////////////////////////////////////////////////
			// multiplying by a very large integer gives what is expected
			//////////////////////////////////////////////////////////////////////
			assert.AreEqual(M, A.ScalarMultiplyByInteger(largeInteger), A.ScalarMultiplyByInt64(largeIntegerModP[i]))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// error tests for ScalarMultiplyByCoefficient
	//////////////////////////////////////////////////////////////////////
	M1, err := NewSpace(k1, 5, 5)
	assert.NoError(err)
	one1, err := One(M1)
	assert.NoError(err)
	M3, err := NewSpace(k3, 5, 5)
	assert.NoError(err)
	one3, err := One(M3)
	assert.NoError(err)
	M4, err := NewSpace(nilField, 5, 5)
	assert.NoError(err)
	one4, err := One(M4)
	assert.NoError(err)
	// multiplying by an element of the wrong field should give an error
	_, err = one1.ScalarMultiplyByCoefficient(gfp.One(k2))
	assert.Error(err)
	_, err = one1.Negate().ScalarMultiplyByCoefficient(gfp.One(k2))
	assert.Error(err)
	_, err = one1.ScalarMultiplyByCoefficient(gfp.One(k3))
	assert.Error(err)
	_, err = one1.Negate().ScalarMultiplyByCoefficient(gfp.One(k3))
	assert.Error(err)
	_, err = one3.ScalarMultiplyByCoefficient(gfp.One(k2))
	assert.Error(err)
	_, err = one4.ScalarMultiplyByCoefficient(gfp.One(k2))
	assert.Error(err)
}

// TestAccess tests various access functions: Row, Rows, Column, Columns, FlatEntry, Entries, FlatEntryOrPanic, NumberOfRows, NumberOfColumns, and Len
func TestAccess(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// the nil matrix
	//////////////////////////////////////////////////////////////////////
	var nilElement *Element
	//////////////////////////////////////////////////////////////////////
	// prepare the entry slice and a list of matrix sizes
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]int64, 0, 35)
	for i := int64(0); i < 35; i++ {
		entrySlice = append(entrySlice, i)
	}
	sizes := [][]int{{5, 7}, {7, 5}, {5, 5}, {0, 3}, {3, 0}, {0, 0}}
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		for _, s := range sizes {
			nrows := s[0]
			ncols := s[1]
			nentries := nrows * ncols
			//////////////////////////////////////////////////////////////////////
			// make a matrix
			//////////////////////////////////////////////////////////////////////
			M, err := NewSpace(k, nrows, ncols)
			assert.NoError(err)
			A, err := FromInt64Slice(M, entrySlice[:nentries])
			assert.NoError(err)
			//////////////////////////////////////////////////////////////////////
			// test NumberOfRows, NumberOfColumns, and Len
			//////////////////////////////////////////////////////////////////////
			assert.Equal(nrows, A.NumberOfRows())
			assert.Equal(0, nilElement.NumberOfRows())
			assert.Equal(ncols, A.NumberOfColumns())
			assert.Equal(0, nilElement.NumberOfColumns())
			assert.Equal(nentries, A.Len())
			assert.Equal(0, nilElement.Len())
			//////////////////////////////////////////////////////////////////////
			// test Entries
			//////////////////////////////////////////////////////////////////////
			assert.Len(A.Entries(), nentries)
			for i, x := range A.Entries() {
				assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i]))
			}
			// the zero matrix has all entries zero
			assert.Len(Zero(M).Entries(), nentries)
			for _, x := range Zero(M).Entries() {
				assert.IsZero(x)
			}
			//////////////////////////////////////////////////////////////////////
			// test Row
			//////////////////////////////////////////////////////////////////////
			for i := 0; i < nrows; i++ {
				row, err := A.Row(i)
				assert.NoError(err)
				assert.Len(row, ncols)
				for j, x := range row {
					assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i*ncols+j]))
				}
			}
			// rows of the zero matrix are zero
			for i := 0; i < nrows; i++ {
				row, err := Zero(M).Row(i)
				assert.NoError(err)
				assert.Len(row, ncols)
				for _, x := range row {
					assert.IsZero(x)
				}
			}
			// out-of-bounds errors
			for _, B := range []*Element{A, Zero(M)} {
				_, err := B.Row(nrows)
				assert.Error(err)
				_, err = B.Row(-1)
				assert.Error(err)
			}
			//////////////////////////////////////////////////////////////////////
			// test Rows
			//////////////////////////////////////////////////////////////////////
			rows := A.Rows()
			assert.Len(rows, nrows)
			for i, row := range rows {
				assert.Len(row, ncols)
				for j, x := range row {
					assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i*ncols+j]))
				}
			}
			// all rows of the zero matrix are zero
			rows = Zero(M).Rows()
			assert.Len(rows, nrows)
			for _, row := range rows {
				assert.Len(row, ncols)
				for _, x := range row {
					assert.IsZero(x)
				}
			}
			//////////////////////////////////////////////////////////////////////
			// test Column
			//////////////////////////////////////////////////////////////////////
			for j := 0; j < ncols; j++ {
				col, err := A.Column(j)
				assert.NoError(err)
				assert.Len(col, nrows)
				for i, x := range col {
					assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i*ncols+j]))
				}
			}
			// columns of the zero matrix are zero
			for j := 0; j < ncols; j++ {
				col, err := Zero(M).Column(j)
				assert.NoError(err)
				assert.Len(col, nrows)
				for _, x := range col {
					assert.IsZero(x)
				}
			}
			// out-of-bounds errors
			for _, B := range []*Element{A, Zero(M)} {
				_, err := B.Column(ncols)
				assert.Error(err)
				_, err = B.Column(-1)
				assert.Error(err)
			}
			//////////////////////////////////////////////////////////////////////
			// test Columns
			//////////////////////////////////////////////////////////////////////
			cols := A.Columns()
			assert.Len(cols, ncols)
			for j, col := range cols {
				assert.Len(col, nrows)
				for i, x := range col {
					assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i*ncols+j]))
				}
			}
			// all columns of the zero matrix are zero
			cols = Zero(M).Columns()
			assert.Len(cols, ncols)
			for _, col := range cols {
				assert.Len(col, nrows)
				for _, x := range col {
					assert.IsZero(x)
				}
			}
			//////////////////////////////////////////////////////////////////////
			// test FlatEntry and FlatEntryOrPanic
			//////////////////////////////////////////////////////////////////////
			for i := 0; i < nentries; i++ {
				if x, err := A.FlatEntry(i); assert.NoError(err) {
					assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i]))
				}
				assert.AreEqual(k, A.FlatEntryOrPanic(i), gfp.FromInt64(k, entrySlice[i]))
			}
			// all entries of the zero matrix are zero
			for i := 0; i < nentries; i++ {
				if x, err := Zero(M).FlatEntry(i); assert.NoError(err) {
					assert.IsZero(x)
				}
				assert.IsZero(Zero(M).FlatEntryOrPanic(i))
			}

			// out-of-bounds behaviour
			for _, B := range []*Element{A, Zero(M)} {
				_, err = B.FlatEntry(-1)
				assert.Error(err)
				_, err = B.FlatEntry(nentries)
				assert.Error(err)
				assert.Panics(func() { B.FlatEntryOrPanic(-1) })
				assert.Panics(func() { B.FlatEntryOrPanic(nentries) })
			}
			//////////////////////////////////////////////////////////////////////
			// test Entry and EntryOrPanic
			//////////////////////////////////////////////////////////////////////
			for i := -1; i <= nrows; i++ {
				for j := -1; j <= ncols; j++ {
					if i >= 0 && i < nrows && j >= 0 && j < ncols {
						if x, err := A.Entry(i, j); assert.NoError(err) {
							assert.AreEqual(k, x, gfp.FromInt64(k, entrySlice[i*ncols+j]))
						}
						assert.AreEqual(k, A.EntryOrPanic(i, j), gfp.FromInt64(k, entrySlice[i*ncols+j]))
						// all entries of the zero matrix are zero
						if x, err := Zero(M).Entry(i, j); assert.NoError(err) {
							assert.IsZero(x)
						}
						assert.IsZero(Zero(M).EntryOrPanic(i, j))
					} else {
						// test out-of-bounds behaviour
						for _, B := range []*Element{A, Zero(M)} {
							_, err = B.Entry(i, j)
							assert.Error(err)
							assert.Panics(func() { B.EntryOrPanic(i, j) })
						}
					}
				}
			}
		}
	}
}

// TestMiscElementBasic tests Hash and Parent
func TestMiscElementBasic(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// the nil matrix
	//////////////////////////////////////////////////////////////////////
	var nilElement *Element
	//////////////////////////////////////////////////////////////////////
	// the Parent of the nil *Element should be Mat(GF(1),0,0)
	//////////////////////////////////////////////////////////////////////
	if R, err := NewSpace(k3, 0, 0); assert.NoError(err) {
		assert.Equal(R, nilElement.Parent())
	}
	//////////////////////////////////////////////////////////////////////
	// prepare the entry slice and a list of matrix sizes
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]int64, 0, 35)
	for i := int64(0); i < 35; i++ {
		entrySlice = append(entrySlice, i)
	}
	sizes := [][]int{{5, 7}, {7, 5}, {5, 5}, {0, 3}, {3, 0}, {0, 0}}
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		for _, s := range sizes {
			nrows := s[0]
			ncols := s[1]
			nentries := nrows * ncols
			//////////////////////////////////////////////////////////////////////
			// make a matrix
			//////////////////////////////////////////////////////////////////////
			M, err := NewSpace(k, nrows, ncols)
			assert.NoError(err)
			A, err := FromInt64Slice(M, entrySlice[:nentries])
			assert.NoError(err)
			//////////////////////////////////////////////////////////////////////
			// test Parent
			//////////////////////////////////////////////////////////////////////
			assert.Equal(A.Parent(), M)
			assert.Equal(Zero(M).Parent(), M)
			//////////////////////////////////////////////////////////////////////
			// test Hash
			//////////////////////////////////////////////////////////////////////
			if B, err := Add(A, A); assert.NoError(err) {
				assert.Equal(B.Hash(), A.ScalarMultiplyByInt64(2).Hash())
			}
			assert.Equal(Zero(M).Hash(), hash.Value(0))
		}
	}
}
