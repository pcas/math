// arithmetic_test contains tests for arithmetic of matrices over ZZ/pZZ

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestAddSubtract tests Add and Subtract
func TestAddSubtract(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make a matrix over GF(3387353)
	//////////////////////////////////////////////////////////////////////
	// make the ground field
	k, err := gfp.GF(3387353)
	assert.NoError(err)
	// make the matrix space
	M, err := NewSpace(k, 3, 4)
	assert.NoError(err)
	// make the entry slice
	es := make([]*gfp.Element, 0, 12)
	for i := 0; i < 12; i++ {
		es = append(es, gfp.FromInt(k, i))
	}
	// make the matrix
	A, err := FromSlice(M, es)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// do some additions
	//////////////////////////////////////////////////////////////////////
	total := Zero(M)
	for count := 0; count < 1000; count++ {
		total, err = Add(total, A)
		assert.NoError(err)
		if x, err := A.ScalarMultiplyByCoefficient(gfp.FromInt(k, count+1)); assert.NoError(err) {
			assert.AreEqual(M, total, x)
		}
	}
	// check the result
	for i := 0; i < 12; i++ {
		es[i] = gfp.FromInt(k, 1000*i)
	}
	result, err := FromSlice(M, es)
	assert.AreEqual(M, total, result)
	//////////////////////////////////////////////////////////////////////
	// now do some subtractions
	//////////////////////////////////////////////////////////////////////
	for count := 0; count < 1000; count++ {
		total, err = Subtract(total, A)
		assert.NoError(err)
		if x, err := A.ScalarMultiplyByCoefficient(gfp.FromInt(k, 1000-count-1)); assert.NoError(err) {
			assert.AreEqual(M, total, x)
		}
	}
	// check the result
	assert.IsZero(total)
	//////////////////////////////////////////////////////////////////////
	// tests involving zero
	//////////////////////////////////////////////////////////////////////
	if x, err := Add(A, Zero(M)); assert.NoError(err) {
		assert.AreEqual(M, A, x)
	}
	if x, err := Add(Zero(M), A); assert.NoError(err) {
		assert.AreEqual(M, A, x)
	}
	if x, err := Subtract(A, Zero(M)); assert.NoError(err) {
		assert.AreEqual(M, A, x)
	}
	if x, err := Subtract(Zero(M), A); assert.NoError(err) {
		if y, err := Add(x, A); assert.NoError(err) {
			assert.IsZero(y)
		}
	}
	if x, err := Subtract(A, A); assert.NoError(err) {
		assert.IsZero(x)
	}
	//////////////////////////////////////////////////////////////////////
	// error cases
	//////////////////////////////////////////////////////////////////////
	// make another matrix space
	M2, err := NewSpace(k, 12, 12)
	assert.NoError(err)
	// addition with the wrong parent
	_, err = Add(total, Zero(M2))
	assert.Error(err)
	_, err = Add(Zero(M2), total)
	assert.Error(err)
	// subtraction with the wrong parent
	_, err = Subtract(total, Zero(M2))
	assert.Error(err)
	_, err = Subtract(Zero(M2), total)
	assert.Error(err)
	// make the same size matrix space over a different field
	k2, err := gfp.GF(19)
	assert.NoError(err)
	M3, err := NewSpace(k2, M.NumberOfRows(), M.NumberOfColumns())
	assert.NoError(err)
	// addition with the wrong parent
	_, err = Add(total, Zero(M3))
	assert.Error(err)
	_, err = Add(Zero(M3), total)
	assert.Error(err)
	// subtraction with the wrong parent
	_, err = Subtract(total, Zero(M3))
	assert.Error(err)
	_, err = Subtract(Zero(M3), total)
	assert.Error(err)
}

// TestSum tests Sum
func TestSum(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make a matrix over GF(3387353)
	//////////////////////////////////////////////////////////////////////
	// make the ground field
	k, err := gfp.GF(3387353)
	assert.NoError(err)
	// make the matrix space
	M, err := NewSpace(k, 3, 4)
	assert.NoError(err)
	// make the entry slice
	es := make([]*gfp.Element, 0, 12)
	for i := 0; i < 12; i++ {
		es = append(es, gfp.FromInt(k, i))
	}
	// make the matrix
	A, err := FromSlice(M, es)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// make a long slice to sum
	//////////////////////////////////////////////////////////////////////
	S := make([]*Element, 0, 10000)
	for i := 0; i < cap(S); i++ {
		S = append(S, A)
	}
	//////////////////////////////////////////////////////////////////////
	// do some sums
	//////////////////////////////////////////////////////////////////////
	// length zero case
	sum, err := Sum(M)
	assert.NoError(err)
	assert.IsZero(sum)
	// length one case
	sum, err = Sum(M, S[:1]...)
	assert.NoError(err)
	assert.AreEqual(M, sum, A)
	// length two case
	sum, err = Sum(M, S[:2]...)
	assert.NoError(err)
	assert.AreEqual(M, sum, A.ScalarMultiplyByInt64(2))
	// short sum case
	sum, err = Sum(M, S[:100]...)
	assert.NoError(err)
	assert.AreEqual(M, sum, A.ScalarMultiplyByInt64(100))
	// long sum case
	sum, err = Sum(M, S...)
	assert.NoError(err)
	assert.AreEqual(M, sum, A.ScalarMultiplyByInt64(int64(len(S))))
	//////////////////////////////////////////////////////////////////////
	// same thing but with all zeroes
	//////////////////////////////////////////////////////////////////////
	for i := range S {
		S[i] = Zero(M)
	}
	// length zero case
	sum, err = Sum(M)
	assert.NoError(err)
	assert.IsZero(sum)
	// length one case
	sum, err = Sum(M, S[:1]...)
	assert.NoError(err)
	assert.IsZero(sum)
	// length two case
	sum, err = Sum(M, S[:2]...)
	assert.NoError(err)
	assert.IsZero(sum)
	// short sum case
	sum, err = Sum(M, S[:100]...)
	assert.NoError(err)
	assert.IsZero(sum)
	// long sum case
	sum, err = Sum(M, S...)
	assert.NoError(err)
	assert.IsZero(sum)
	//////////////////////////////////////////////////////////////////////
	// mismatched parents
	//////////////////////////////////////////////////////////////////////
	for i := range S {
		S[i] = A
	}
	// mismatched dimensions
	M2, err := NewSpace(Ring(M), M.NumberOfRows()+1, M.NumberOfColumns()+2)
	assert.NoError(err)
	S[0] = Zero(M2)
	// length one case
	_, err = Sum(M, S[:1]...)
	assert.Error(err)
	// length two case
	_, err = Sum(M, S[:2]...)
	assert.Error(err)
	// short sum case
	_, err = Sum(M, S[:100]...)
	assert.Error(err)
	// long sum case
	_, err = Sum(M, S...)
	assert.Error(err)
	// mismatched ground field
	k2, err := gfp.GF(19)
	assert.NoError(err)
	M3, err := NewSpace(k2, M.NumberOfRows(), M.NumberOfColumns())
	assert.NoError(err)
	S[0] = Zero(M3)
	// length one case
	_, err = Sum(M, S[:1]...)
	assert.Error(err)
	// length two case
	_, err = Sum(M, S[:2]...)
	assert.Error(err)
	// short sum case
	_, err = Sum(M, S[:100]...)
	assert.Error(err)
	// long sum case
	_, err = Sum(M, S...)
	assert.Error(err)
}
