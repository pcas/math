// normalform_test contains tests for normalform.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcastools/fatal"
	"fmt"
	"math"
	"testing"
)

type matrixTestBlock struct {
	field     *gfp.Parent // the ground field
	fieldName string      // a description of the ground field
	matrices  []*Element  // the matrices to test
}

var matrixTestBlocks []matrixTestBlock

//////////////////////////////////////////////////////////////////////
// setup functions
//////////////////////////////////////////////////////////////////////

// initializeMatrices initializes the matrices to test
func initializeMatrices() {
	// did we do the setup already?
	if matrixTestBlocks != nil {
		return
	}
	// no, so we need to do the setup
	sizes := [][]int{
		/*		{0, 0},
				{10, 0},
				{0, 10},
				{1, 1},
				{1, 10},
				{10, 1},*/
		{10, 10},
		{10, 100},
		{100, 10},
		{10, 1000},
		{100, 100},
		{1000, 10},
	}
	//////////////////////////////////////////////////////////////////////
	// set up the ground fields
	//////////////////////////////////////////////////////////////////////
	// 2^63 - 25 is the largest prime that fits in an int64
	const bigprime = math.MaxInt64 - 24
	// a prime of size roughly 2^62
	const mediumprime = 1122331314832901153
	// a prime of size roughly 2^16
	const smallprime = 24979
	fields := make([]*gfp.Parent, 6)
	primes := []int64{1, 19, smallprime, mediumprime, bigprime}
	names := []string{"GF(1)", "GF(19)", "GF(p) with p a 16-bit prime", "GF(p) with p a 62-bit prime", "GF(p) with p a 63-bit prime", "the nil GF(p)"}
	for i, p := range primes {
		var err error
		fields[i], err = gfp.GF(p)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	}
	//////////////////////////////////////////////////////////////////////
	// set up the matrices
	//////////////////////////////////////////////////////////////////////
	matrixTestBlocks = make([]matrixTestBlock, 0, len(fields))
	for i, k := range fields {
		// initialize the block
		b := matrixTestBlock{
			field:     k,
			fieldName: names[i],
			matrices:  make([]*Element, 0, len(sizes)),
		}
		// set up the entry slice
		entrySlice := make([]*gfp.Element, 0, 10000)
		x := gfp.FromInt64(k, 1)
		entrySlice = append(entrySlice, x)
		for j := 1; j < cap(entrySlice); j++ {
			x = x.ScalarMultiplyByInteger(integer.FromInt(3 * j))
			entrySlice = append(entrySlice, x)
		}
		// make the matrices
		for _, s := range sizes {
			nrows := s[0]
			ncols := s[1]
			M, err := NewSpace(k, nrows, ncols)
			if err != nil {
				panic("fatal error in initializeMatrices")
			}
			if y, err := FromSlice(M, entrySlice[:nrows*ncols]); err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			} else {
				b.matrices = append(b.matrices, y)
			}
		}
		// store the block
		matrixTestBlocks = append(matrixTestBlocks, b)
	}
	return
}

// a package-level variable to make sure the compiler doesn't optimise away the benchmark
var dummyResult *Element

func multiplyOrPanic(A *Element, B *Element) *Element {
	result, err := Multiply(A, B)
	if err != nil {
		panic("Fatal error in multiplyOrPanic")
	}
	return result
}

// BenchmarkMatrixMultiplication benchmarks matrix multiplication
func BenchmarkMatrixMultiplication(b *testing.B) {
	// set up the matrices to test
	fmt.Printf("Initializing...")
	initializeMatrices()
	fmt.Printf("done\n")
	// run the benchmarks, skipping those where the matrix multiplication doesn't make sense
	for _, block := range matrixTestBlocks {
		for _, m1 := range block.matrices {
			for _, m2 := range block.matrices {
				if m1.NumberOfColumns() == m2.NumberOfRows() {
					testName := fmt.Sprintf("Multiplying %vx%v by %vx%v over %v", m1.NumberOfRows(), m1.NumberOfColumns(), m2.NumberOfRows(), m2.NumberOfColumns(), block.fieldName)
					b.Run(testName, func(b *testing.B) {
						for i := 0; i < b.N; i++ {
							dummyResult = multiplyOrPanic(m1, m2)
						}
					})
				}
			}
		}
	}
}
