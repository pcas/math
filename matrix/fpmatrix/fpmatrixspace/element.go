//go:generate ../../internal/template/template.py ../../internal/template/matrixspace/element.txt element.go "package=fpmatrixspace" "dir=fpmatrix" "description=entries in ZZ/pZZ" "entryPackage=bitbucket.org/pcas/math/finitefield/gfp" "entryElement=*gfp.Element"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/matrixspace/element.txt".
To modify this file, edit "../../internal/template/matrixspace/element.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Element defines a matrix with entries in ZZ/pZZ.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/fpmatrix/fpmatrixspace
/////////////////////////////////////////////////////////////////////////
// element.go
/////////////////////////////////////////////////////////////////////////
/*
Requires the functions:

// parentOfTranspose returns the parent that contains the transposes of matrices in R (i.e. the parent of Transpose(x), where x is a matrix in R).
func parentOfTranspose(R *Parent) *Parent {
	...
}

// parentOfProduct returns the parent that contains the product of matrices in R1 and R2 (i.e. the parent of x1 * x2, where x1 is a matrix in R1, and x2 is a matrix in R2). This will panic if R1 and R2 are incompatible.
func parentOfProduct(R1 *Parent, R2 *Parent) *Parent {
    ...
}
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// oneOrPanic returns the identity matrix of R if R is a space of square matrices. Otherwise it panics.
func oneOrPanic(R *Parent) *Element {
	x, err := One(R)
	if err != nil {
		panic(err)
	}
	return x
}

// createMatrix returns a matrix in R with given entries S. Assumes that the entries are of the correct length, and have the correct universe.
func createMatrix(R *Parent, S entries.Interface) *Element {
	// If we've been passed the nil parent, move to the default parent
	if R == nil {
		R = defaultParent()
	}
	// Assert that the slice is of the correct length
	nrows, ncols := S.NumberOfRows(), S.NumberOfColumns()
	if R.NumberOfRows() != nrows || R.NumberOfColumns() != ncols {
		panic(errors.MatrixOfIncompatibleSize.New(
			nrows, ncols, R.NumberOfRows(), R.NumberOfColumns()))
	}
	// Is this the zero slice?
	if entries.IsZero(S) {
		return Zero(R)
	}
	// Is this the identity slice?
	if entries.IsIdentity(S) {
		x, err := One(R)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return x
	}
	// Return the new element
	return &Element{
		parent: R,
		es:     S,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// One returns the identity matrix of R.
func One(R *Parent) (*Element, error) {
	if R == nil {
		R = defaultParent()
	}
	if R.one == nil {
		return nil, errors.MatrixNotSquare.New()
	}
	return R.one, nil
}

// Multiply returns the product x * y of two matrices x and y. This will return an error if x and y do not have compatible parents (i.e. we require that the entries in x and y share a common parent, and that the number of columns of x is equal to the number of rows of y).
func Multiply(x *Element, y *Element) (*Element, error) {
	// Check that the parents are compatible
	if x.NumberOfColumns() != y.NumberOfRows() {
		return nil, errors.MatricesOfIncompatibleSizes.New(x.NumberOfRows(), x.NumberOfColumns(), y.NumberOfRows(), y.NumberOfColumns())
	}
	Rx, Ry := parentOfElement(x), parentOfElement(y)
	h := Rx.entriesHelper()
	if Ry.entriesHelper().Universe() != h.Universe() {
		return nil, errors.ParentsOfEntriesDoNotAgree.New()
	}
	// Fast-track the easy cases
	if quickIsZero(x) || quickIsZero(y) {
		return Zero(parentOfProduct(Rx, Ry)), nil
	} else if quickIsOne(x) {
		return y, nil
	} else if quickIsOne(y) {
		return x, nil
	}
	// Perform the multiplication
	S, err := entries.RowRowMultiply(h, x.es, y.Transpose().es)
	if err != nil {
		return nil, err
	}
	return createMatrix(parentOfProduct(Rx, Ry), S), nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Determinant returns the determinant of the matrix x (if x is square).
func (x *Element) Determinant() (*gfp.Element, error) {
	return getDeterminant(x)
}

// Transpose returns the transpose of the matrix x.
func (x *Element) Transpose() *Element {
	// Get the zero case out of the way
	if quickIsZero(x) {
		return Zero(parentOfTranspose(parentOfElement(x)))
	}
	// Do we know the answer?
	var y *Element
	x.m.Lock()
	y = x.trans
	x.m.Unlock()
	// If we don't know the answer, we need to compute it
	if y == nil {
		// Compute the transpose
		R := parentOfElement(x)
		T, err := entries.Transpose(R.entriesHelper(), x.es)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		// If x is square, is it equal to its transpose?
		areEqual := false
		if x.IsSquare() {
			var err error
			areEqual, err = entries.AreEqual(R.entriesHelper(), x.es, T)
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
		}
		if areEqual {
			// Yes -- x is equal to its transpose
			y = x
			x.m.Lock()
			x.trans = x
			x.m.Unlock()
		} else {
			// No -- x is not equal to its transpose
			y = createMatrix(parentOfTranspose(R), T)
			y.trans = x
			x.m.Lock()
			if x.trans == nil {
				x.trans = y
				if x.det != nil {
					y.det = x.det
				}
			} else {
				y = x.trans
			}
			x.m.Unlock()
		}
	}
	return y
}

// EchelonForm returns the reduced row echelon form of the matrix x, or an error if row echelon form does not exist, or is not implemented, over this ground ring.
func (x *Element) EchelonForm() (*Element, error) {
	// Get the zero case out of the way
	if quickIsZero(x) {
		return Zero(parentOfElement(x)), nil
	}
	// Do we know the answer?
	var y *Element
	x.m.Lock()
	y = x.ech
	x.m.Unlock()
	// If we don't know the answer, we need to compute it
	if y == nil {
		// Compute the echelon form, if possible
		R := parentOfElement(x)
		T, err := entries.EchelonForm(R.entriesHelper(), x.es)
		if err != nil {
			return nil, err
		}
		// Is x already in echelon form?
		if areEqual, err := entries.AreEqual(R.entriesHelper(), x.es, T); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if areEqual {
			// Yes -- x is in echelon form
			y = x
			x.m.Lock()
			x.ech = x
			x.m.Unlock()
		} else {
			// No -- x is not equal to its echelon form
			y = createMatrix(R, T)
			y.ech = y
			x.m.Lock()
			if x.ech == nil {
				x.ech = y
			} else {
				y = x.ech
			}
			x.m.Unlock()
		}
	}
	return y, nil
}

// IsSquare returns true iff the matrix x is square.
func (x *Element) IsSquare() bool {
	return x.NumberOfRows() == x.NumberOfColumns()
}
