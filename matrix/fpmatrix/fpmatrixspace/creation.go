// Creation creates the space of n x m ZZ/pZZ-valued matrices.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/fpmatrix/internal/fpentries"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// The cached Parents and a mutex controlling access.
// Parents are indexed by p, m, n
var (
	parentCache map[[3]int]*Parent
	parentMutex sync.RWMutex
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the cache of parents
func init() {
	parentCache = make(map[[3]int]*Parent)
}

// defaultParent returns the parent of the nil element.
func defaultParent() *Parent {
	FF, err := gfp.GF(1)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	R, err := NewSpace(FF, 0, 0)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return R
}

// parentOfTranspose returns the parent that contains the transposes of matrices in R (i.e. the parent of Transpose(x), where x is a matrix in R).
func parentOfTranspose(R *Parent) *Parent {
	// Is there anything to do?
	nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
	if nrows == ncols {
		return R
	}
	// Create and return the space
	T, err := NewSpace(R.Ring().(*gfp.Parent), ncols, nrows)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return T
}

// parentOfProduct returns the parent that contains the product of matrices in R1 and R2 (i.e. the parent of x1 * x2, where x1 is a matrix in R1, and x2 is a matrix in R2). This will panic if R1 and R2 are incompatible.
func parentOfProduct(R1 *Parent, R2 *Parent) *Parent {
	// Sanity check
	nrows1, ncols1 := R1.NumberOfRows(), R1.NumberOfColumns()
	nrows2, ncols2 := R2.NumberOfRows(), R2.NumberOfColumns()
	if ncols1 != nrows2 {
		panic(errors.MatricesOfIncompatibleSizes.New(nrows1, ncols1, nrows2, ncols2))
	}
	if R1.Ring() != R2.Ring() {
		panic(errors.GroundRingsDoNotAgree.New())
	}
	// Is there anything to do?
	if ncols1 == ncols2 {
		return R1
	} else if nrows1 == nrows2 {
		return R2
	}
	// Create and return the space
	T, err := NewSpace(R1.Ring().(*gfp.Parent), nrows1, ncols2)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewSpace returns the space of n x m matrices with entries in FF, which should be ZZ/pZZ for some prime p.
func NewSpace(FF *gfp.Parent, n int, m int) (*Parent, error) {
	p := FF.CharacteristicInt64()
	// Aquire a read lock on the mutex and check the cache
	parentMutex.RLock()
	label := [3]int{int(p), m, n}
	if R, ok := parentCache[label]; ok {
		// The Parent is in the cache so return it
		parentMutex.RUnlock()
		return R, nil
	}
	// No luck -- release the read lock and create the new Parent
	parentMutex.RUnlock()
	R, err := createParent(fpentries.Helper(p), n, m)
	if err != nil {
		return nil, err
	}
	// Acquire a write lock and check the cache once again before returning
	parentMutex.Lock()
	defer parentMutex.Unlock()
	if RR, ok := parentCache[label]; ok {
		// The Parent is now in the cache, so return it
		return RR, nil
	}
	parentCache[label] = R
	return R, nil
}

// FromSlice returns a matrix in the matrix space R, with entries S. The entries are read row-by-row.
func FromSlice(R *Parent, S []*gfp.Element) (*Element, error) {
	return fromSlice(R, gfp.Slice(S))
}

// FromIntegerSlice returns a matrix in the matrix space R, with entries S. The entries are read row-by-row.
func FromIntegerSlice(R *Parent, S []*integer.Element) (*Element, error) {
	return fromSlice(R, integer.Slice(S))
}

// FromIntegerSlice returns a matrix in the matrix space R, with entries S. The entries are read row-by-row.
func FromInt64Slice(R *Parent, S []int64) (*Element, error) {
	return fromSlice(R, integer.Int64Slice(S))
}
