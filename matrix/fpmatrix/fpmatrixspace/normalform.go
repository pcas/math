// Normalform defines Echelon form-related functions for working with n x m matrices with entries in ZZ/pZZ.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcastools/fatal"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// inverseMatrix returns the inverse of the matrix x, or an error if x is not invertible. Assumes that x is a non-nil square matrix. This does NOT cache the inverse on x.
func inverseMatrix(x *Element) (*Element, error) {
	panic(fatal.NotImplemented()) // FIX ME: Needs implementing
}

// computeDeterminant computes and returns the determinant of the matrix x. Assumes that x is a non-nil square matrix. This does NOT cache the determinant on x.
func computeDeterminant(x *Element) (*gfp.Element, error) {
	// FIX ME: This is a stupid way to compute the determinant
	d, err := entries.DeterminantViaLaplace(x.es)
	if err != nil {
		return nil, err
	}
	return d.(*gfp.Element), nil
}
