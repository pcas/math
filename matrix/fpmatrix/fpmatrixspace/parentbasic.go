//go:generate ../../internal/template/template.py ../../internal/template/parentbasic.txt parentbasic.go "package=fpmatrixspace" "dir=fpmatrix" "description=entries in ZZ/pZZ"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/parentbasic.txt".
To modify this file, edit "../../internal/template/parentbasic.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Parentbasic defines basic parent methods for matrices with entries in ZZ/pZZ.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/fpmatrix/fpmatrixspace
/////////////////////////////////////////////////////////////////////////
// parentbasic.go
/////////////////////////////////////////////////////////////////////////

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// objectsToElements returns the arguments as an element in the given parent, if that's where they belong.
func objectsToElements(R *Parent, x object.Element, y object.Element) (*Element, *Element, error) {
	xx, err1 := ToElement(R, x)
	if err1 != nil {
		return nil, nil, errors.Arg1NotContainedInParent.New()
	}
	yy, err2 := ToElement(R, y)
	if err2 != nil {
		return nil, nil, errors.Arg2NotContainedInParent.New()
	}
	return xx, yy, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// entriesHelper returns the entries helper for this parent.
func (R *Parent) entriesHelper() entries.Helper {
	if R == nil {
		R = defaultParent()
	}
	return R.h
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (R *Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := objectsToElements(R, x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// Zero returns the additive identity of the ring.
func (R *Parent) Zero() object.Element {
	return Zero(R)
}

// IsZero returns true iff x is the additive identity element of the ring.
func (R *Parent) IsZero(x object.Element) (bool, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// Add returns x + y.
func (R *Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(R, x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy)
}

// Subtract returns x - y.
func (R *Parent) Subtract(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(R, x, y)
	if err != nil {
		return nil, err
	}
	return Subtract(xx, yy)
}

// Negate returns -x.
func (R *Parent) Negate(x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Negate(), nil
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
func (R *Parent) Sum(S ...object.Element) (object.Element, error) {
	// Handle the easy cases
	if n := len(S); n == 0 {
		return Zero(R), nil
	} else if n == 1 {
		return ToElement(R, S[0])
	} else if n == 2 {
		return R.Add(S[0], S[1])
	}
	// Convert the slice to a slice of elements
	T := make([]*Element, 0, len(S))
	for _, x := range S {
		xx, err := ToElement(R, x)
		if err != nil {
			return nil, err
		}
		T = append(T, xx)
	}
	// Return the sum
	return Sum(R, T...)
}

// Power returns x^k.
func (R *Parent) Power(x object.Element, k *integer.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return Power(xx, k)
}

// Inverse returns the inverse of the matrix x, if x is invertible.
func (R *Parent) Inverse(x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Inverse()
}

// ScalarMultiplyByInteger returns n * x, where this is defined to be:
//		  x + ... + x (n times) 	if n > 0;
//		  the zero element 			if n = 0;
//		  -x - ... - x (|n| times) 	if n < 0.
func (R *Parent) ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.ScalarMultiplyByInteger(n), nil
}

// ScalarMultiplyByCoefficient returns c * x, where c is an element in the ring containing the entries of the matrices in R.
func (R *Parent) ScalarMultiplyByCoefficient(c object.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return scalarMultiplyByCoefficient(c, xx)
}

// Ring returns the ring containing the entries of the matrices in R.
func (R *Parent) Ring() ring.Interface {
	return R.entriesHelper().Universe()
}

// Row returns the i-th row of the matrix x. Rows are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices in R.
func (R *Parent) Row(x object.Element, i int) ([]object.Element, error) {
	// Convert x to a *Element
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	// Get the zero case out of the way
	if quickIsZero(xx) {
		ncols := R.NumberOfColumns()
		zero := R.Ring().Zero()
		row := make([]object.Element, 0, ncols)
		for i := 0; i < ncols; i++ {
			row = append(row, zero)
		}
		return row, nil
	}
	// Recover the row
	return entries.Row(i, xx.es)
}

// Rows returns the rows of the matrix x. The entries are elements of the ring containing the entries of the matrices in R.
func (R *Parent) Rows(x object.Element) ([][]object.Element, error) {
	nrows := R.NumberOfRows()
	rows := make([][]object.Element, 0, nrows)
	for i := 0; i < nrows; i++ {
		row, err := R.Row(x, i)
		if err != nil {
			return nil, err
		}
		rows = append(rows, row)
	}
	return rows, nil
}

// Column returns the i-th column of the matrix x. Columns are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices in R.
func (R *Parent) Column(x object.Element, i int) ([]object.Element, error) {
	// Convert x to a *Element
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	// Get the zero case out of the way
	if quickIsZero(xx) {
		nrows := R.NumberOfRows()
		zero := R.Ring().Zero()
		col := make([]object.Element, 0, nrows)
		for i := 0; i < nrows; i++ {
			col = append(col, zero)
		}
		return col, nil
	}
	// If we know the transpose, then we'll return the transpose's row
	var trans *Element
	xx.m.Lock()
	trans = xx.trans
	xx.m.Unlock()
	if trans != nil {
		return entries.Row(i, trans.es)
	}
	// Recover the column
	return entries.Column(i, xx.es)
}

// Columns returns the columns of the matrix x. The entries are elements of the ring containing the entries of the matrices in R.
func (R *Parent) Columns(x object.Element) ([][]object.Element, error) {
	ncols := R.NumberOfColumns()
	cols := make([][]object.Element, 0, ncols)
	for i := 0; i < ncols; i++ {
		col, err := R.Column(x, i)
		if err != nil {
			return nil, err
		}
		cols = append(cols, col)
	}
	return cols, nil
}

// Entries returns the entries of the matrix x, row by row. The entries are elements of the ring containing the entries of the matrices in R.
func (R *Parent) Entries(x object.Element) ([]object.Element, error) {
	// Convert x to a *Element
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	// Get the zero case out of the way
	if quickIsZero(xx) {
		size := xx.Len()
		zero := R.Ring().Zero()
		entries := make([]object.Element, 0, size)
		for i := 0; i < size; i++ {
			entries = append(entries, zero)
		}
		return entries, nil
	}
	// Recover the entries
	return slice.ToElementSlice(xx.es), nil
}

// Entry returns the (i,j)-th entry of the matrix x. Rows and columns are indexed from zero. It returns an error if either i or j is negative or too large (i.e. if i and j refer to a non-existent entry of x). The returned entry is an element of the ring containing the entries of the matrices in R.
func (R *Parent) Entry(x object.Element, i int, j int) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Entry(i, j)
}

// Transpose returns the transpose of the matrix x.
func (R *Parent) Transpose(x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Transpose(), nil
}

// FromSlice attempts to convert the slice S to a matrix, row by row.
func (R *Parent) FromSlice(S []object.Element) (object.Element, error) {
	return fromSlice(R, slice.ElementSlice(S))
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (R *Parent) Contains(x object.Element) bool {
	_, err := ToElement(R, x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (R *Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(R, x)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToElement returns x as a matrix with entries in ZZ/pZZ in R, if x can naturally be regarded as belonging there.
func ToElement(R *Parent, x object.Element) (*Element, error) {
	if xx, ok := x.(*Element); ok {
		// Is x a *Element? Is R the parent?
		if isParent(R, xx) {
			return xx, nil
		}
	}
	// x is not naturally in R
	return nil, errors.ArgNotContainedInParent.New()
}
