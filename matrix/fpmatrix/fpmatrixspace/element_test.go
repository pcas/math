// element_test contains tests for element.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestOne tests One and creation of the identity matrix
func TestOne(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// matrices of various sizes
	//////////////////////////////////////////////////////////////////////
	sizes := []int{0, 1, 3, 6, 2560}
	for _, i := range sizes {
		for _, j := range sizes {
			for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
				M, err := NewSpace(k, i, j)
				assert.NoError(err)
				one, err := One(M)
				if i == j {
					assert.NoError(err)
					assert.True(one.IsOne())
				} else {
					assert.Error(err)
				}
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// the nil matrix space
	//////////////////////////////////////////////////////////////////////
	var nilMatrixSpace *Parent
	one, err := One(nilMatrixSpace)
	assert.NoError(err)
	assert.IsOne(one)
	assert.IsZero(one)
	//////////////////////////////////////////////////////////////////////
	// creating the identity matrix directly should give the cached
	// identity of the matrix algebra
	//////////////////////////////////////////////////////////////////////
	for _, i := range sizes {
		for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
			M, err := NewSpace(k, i, i)
			assert.NoError(err)
			// get the standard identity matrix
			one, err := One(M)
			assert.NoError(err)
			// now create an identity matrix directly
			entrySlice := make([]int64, 0, i*i)
			for a := 0; a < i; a++ {
				for b := 0; b < i; b++ {
					if a == b {
						entrySlice = append(entrySlice, 1)
					} else {
						entrySlice = append(entrySlice, 0)
					}
				}
			}
			fakeOne, err := FromInt64Slice(M, entrySlice)
			assert.NoError(err)
			assert.Equal(one, fakeOne)
		}
	}
}

// TestMultiply tests Multiply
func TestMultiply(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// the sizes of matrices we loop over, and the expected results
	//////////////////////////////////////////////////////////////////////
	sizes := [][]int{{0, 0}, {0, 3}, {4, 0}, {3, 6}, {7, 3}, {100, 7}, {6, 6}}
	expected := [][]int64{
		{},
		{},
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{},
		nil,
		nil,
		nil,
		{},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{441, 462, 483, 504, 525, 546, 1017, 1074, 1131, 1188, 1245, 1302, 1593, 1686,
			1779, 1872, 1965, 2058},
		nil,
		nil,
		nil,
		{54, 60, 66, 72, 78, 84, 117, 132, 147, 162, 177, 192, 180, 204, 228, 252, 276,
			300, 243, 276, 309, 342, 375, 408, 306, 348, 390, 432, 474, 516, 369, 420, 471,
			522, 573, 624, 432, 492, 552, 612, 672, 732},
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{364, 392, 420, 854, 931, 1008, 1344, 1470, 1596, 1834, 2009, 2184, 2324, 2548,
			2772, 2814, 3087, 3360, 3304, 3626, 3948, 3794, 4165, 4536, 4284, 4704, 5124,
			4774, 5243, 5712, 5264, 5782, 6300, 5754, 6321, 6888, 6244, 6860, 7476, 6734,
			7399, 8064, 7224, 7938, 8652, 7714, 8477, 9240, 8204, 9016, 9828, 8694, 9555,
			10416, 9184, 10094, 11004, 9674, 10633, 11592, 10164, 11172, 12180, 10654,
			11711, 12768, 11144, 12250, 13356, 11634, 12789, 13944, 12124, 13328, 14532,
			12614, 13867, 15120, 13104, 14406, 15708, 13594, 14945, 16296, 14084, 15484,
			16884, 14574, 16023, 17472, 15064, 16562, 18060, 15554, 17101, 18648, 16044,
			17640, 19236, 16534, 18179, 19824, 17024, 18718, 20412, 17514, 19257, 21000,
			18004, 19796, 21588, 18494, 20335, 22176, 18984, 20874, 22764, 19474, 21413,
			23352, 19964, 21952, 23940, 20454, 22491, 24528, 20944, 23030, 25116, 21434,
			23569, 25704, 21924, 24108, 26292, 22414, 24647, 26880, 22904, 25186, 27468,
			23394, 25725, 28056, 23884, 26264, 28644, 24374, 26803, 29232, 24864, 27342,
			29820, 25354, 27881, 30408, 25844, 28420, 30996, 26334, 28959, 31584, 26824,
			29498, 32172, 27314, 30037, 32760, 27804, 30576, 33348, 28294, 31115, 33936,
			28784, 31654, 34524, 29274, 32193, 35112, 29764, 32732, 35700, 30254, 33271,
			36288, 30744, 33810, 36876, 31234, 34349, 37464, 31724, 34888, 38052, 32214,
			35427, 38640, 32704, 35966, 39228, 33194, 36505, 39816, 33684, 37044, 40404,
			34174, 37583, 40992, 34664, 38122, 41580, 35154, 38661, 42168, 35644, 39200,
			42756, 36134, 39739, 43344, 36624, 40278, 43932, 37114, 40817, 44520, 37604,
			41356, 45108, 38094, 41895, 45696, 38584, 42434, 46284, 39074, 42973, 46872,
			39564, 43512, 47460, 40054, 44051, 48048, 40544, 44590, 48636, 41034, 45129,
			49224, 41524, 45668, 49812, 42014, 46207, 50400, 42504, 46746, 50988, 42994,
			47285, 51576, 43484, 47824, 52164, 43974, 48363, 52752, 44464, 48902, 53340,
			44954, 49441, 53928, 45444, 49980, 54516, 45934, 50519, 55104, 46424, 51058,
			55692, 46914, 51597, 56280, 47404, 52136, 56868, 47894, 52675, 57456, 48384,
			53214, 58044, 48874, 53753, 58632},
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		{441, 462, 483, 504, 525, 546, 1017, 1074, 1131, 1188, 1245, 1302, 1593, 1686,
			1779, 1872, 1965, 2058, 2169, 2298, 2427, 2556, 2685, 2814, 2745, 2910, 3075,
			3240, 3405, 3570, 3321, 3522, 3723, 3924, 4125, 4326},
	}
	//////////////////////////////////////////////////////////////////////
	// loop over the sizes, building the matrices to multiply
	//////////////////////////////////////////////////////////////////////
	entrySlice := make([]*integer.Element, 0, 700)
	for i := 1; i <= 700; i++ {
		entrySlice = append(entrySlice, integer.FromInt(i))
	}
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		var expectedIndex int = 0 // the index in the slice of expected results
		for _, sizeA := range sizes {
			for _, sizeB := range sizes {
				// build the matrix spaces
				MA, err := NewSpace(k, sizeA[0], sizeA[1])
				assert.NoError(err)
				MB, err := NewSpace(k, sizeB[0], sizeB[1])
				assert.NoError(err)
				// build the matrices
				A, err := FromIntegerSlice(MA, entrySlice[:sizeA[0]*sizeA[1]])
				assert.NoError(err)
				B, err := FromIntegerSlice(MB, entrySlice[:sizeB[0]*sizeB[1]])
				assert.NoError(err)
				// do the multiplication and check
				result, err := Multiply(A, B)
				if expectedSlice := expected[expectedIndex]; expectedSlice == nil {
					// the matrices have incompatible sizes
					assert.Error(err)
				} else {
					assert.NoError(err)
					MC, err := NewSpace(k, sizeA[0], sizeB[1])
					assert.NoError(err)
					C, err := FromInt64Slice(MC, expectedSlice)
					assert.NoError(err)
					assert.AreEqual(MC, C, result)
				}
				// move along to the next test
				expectedIndex++
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test multiplication by the identity matrix
	//////////////////////////////////////////////////////////////////////
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		for _, sizeA := range sizes {
			// build the matrix spaces
			MA, err := NewSpace(k, sizeA[0], sizeA[1])
			assert.NoError(err)
			ML, err := NewSpace(k, sizeA[0], sizeA[0])
			assert.NoError(err)
			MR, err := NewSpace(k, sizeA[1], sizeA[1])
			assert.NoError(err)
			// build the matrices
			A, err := FromIntegerSlice(MA, entrySlice[:sizeA[0]*sizeA[1]])
			assert.NoError(err)
			idL, err := One(ML)
			assert.NoError(err)
			idR, err := One(MR)
			assert.NoError(err)
			// do the multiplications and check
			result, err := Multiply(A, idR)
			assert.NoError(err)
			assert.AreEqual(MA, A, result)
			result, err = Multiply(idL, A)
			assert.NoError(err)
			assert.AreEqual(MA, A, result)
		}
	}
	//////////////////////////////////////////////////////////////////////
	// multiplying matrices over different ground fields should error
	//////////////////////////////////////////////////////////////////////
	for _, firstField := range []*gfp.Parent{k1, k2, k3, nilField} {
		for _, secondField := range []*gfp.Parent{k1, k2, k3, nilField} {
			// we only want the case where the fields are different
			if secondField == firstField || (firstField == k3 && secondField == nilField) || (firstField == nilField && secondField == k3) {
				continue
			}
			// do many multiplications
			for _, sizeA := range sizes {
				for _, sizeB := range sizes {
					// build the matrix spaces
					MA, err := NewSpace(firstField, sizeA[0], sizeA[1])
					assert.NoError(err)
					MB, err := NewSpace(secondField, sizeB[0], sizeB[1])
					assert.NoError(err)
					// build the matrices
					A, err := FromIntegerSlice(MA, entrySlice[:sizeA[0]*sizeA[1]])
					assert.NoError(err)
					B, err := FromIntegerSlice(MB, entrySlice[:sizeB[0]*sizeB[1]])
					assert.NoError(err)
					// the multiplication should give an error
					_, err = Multiply(A, B)
					assert.Error(err)
				}
			}
		}
	}
}

// TestTranspose tests Transpose
func TestTranspose(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	//////////////////////////////////////////////////////////////////////
	// Test four matrices A, B, C, D, with A^T=B, C is symmetric, D is
	// antisymmetric
	//////////////////////////////////////////////////////////////////////
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		// make A
		MA, err := NewSpace(k, 2, 3)
		assert.NoError(err)
		A, err := FromInt64Slice(MA, []int64{1, 2, 3, 4, 5, 6})
		assert.NoError(err)
		// make B
		MB, err := NewSpace(k, 3, 2)
		assert.NoError(err)
		B, err := FromInt64Slice(MB, []int64{1, 4, 2, 5, 3, 6})
		assert.NoError(err)
		// make C
		MC, err := NewSpace(k, 4, 4)
		assert.NoError(err)
		// 1  3  5  7
		// 3  9 11 13
		// 5 11 15 17
		// 7 13 17 19
		C, err := FromInt64Slice(MC, []int64{1, 3, 5, 7, 3, 9, 11, 13, 5, 11, 15, 17, 7, 13, 17, 19})
		assert.NoError(err)
		// make D
		MD, err := NewSpace(k, 4, 4)
		assert.NoError(err)
		// same as C but with some sign flips and zero diagonal
		D, err := FromInt64Slice(MD, []int64{0, -3, 5, -7, 3, 0, 11, -13, -5, -11, 0, 17, 7, 13, -17, 0})
		assert.NoError(err)
		//////////////////////////////////////////////////////////////////////
		// the tests
		//////////////////////////////////////////////////////////////////////
		assert.AreEqual(MA, A, A.Transpose().Transpose())
		assert.AreEqual(MA, A, B.Transpose())
		assert.AreEqual(MB, B, A.Transpose())
		assert.AreEqual(MB, B, B.Transpose().Transpose())
		assert.AreEqual(MC, C, C.Transpose().Transpose())
		// C is symmetric
		assert.AreEqual(MC, C, C.Transpose())
		assert.AreEqual(MD, D, D.Transpose().Transpose())
		// D is antisymmetric
		if x, err := Add(D, D.Transpose()); assert.NoError(err) {
			assert.IsZero(x)
		}
	}
}
