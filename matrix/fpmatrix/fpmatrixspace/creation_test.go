// creation_test contains tests for creation.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fpmatrixspace

import (
	"bitbucket.org/pcas/math/finitefield/gfp"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestNewSpace tests NewSpace
func TestNewSpace(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make some fields
	//////////////////////////////////////////////////////////////////////
	k1, err := gfp.GF(19)
	assert.NoError(err)
	k2, err := gfp.GF(3387353)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// make some matrix spaces
	//////////////////////////////////////////////////////////////////////
	M1, err := NewSpace(k1, 254, 27)
	assert.NoError(err)
	// there should be exactly one copy of each matrix space
	M2, err := NewSpace(k1, 254, 27)
	assert.NoError(err)
	assert.True(M1 == M2)
	// the ground field matters
	M3, err := NewSpace(k2, 254, 27)
	assert.NoError(err)
	assert.False(M1 == M3)
	// matrix spaces can have zero rows or zero columns.
	M4, err := NewSpace(k1, 0, 6)
	assert.NoError(err)
	M5, err := NewSpace(k1, 6, 0)
	assert.NoError(err)
	M6, err := NewSpace(k1, 0, 0)
	assert.NoError(err)
	assert.False(M4 == M5)
	assert.False(M4 == M6)
	assert.False(M5 == M6)
	// matrix spaces over GF(1) are fine
	k3, err := gfp.GF(1)
	assert.NoError(err)
	var nilField *gfp.Parent
	M7, err := NewSpace(k3, 254, 27)
	assert.NoError(err)
	M8, err := NewSpace(k3, 254, 27)
	assert.NoError(err)
	M9, err := NewSpace(nilField, 254, 27)
	assert.NoError(err)
	assert.True(M7 == M8)
	assert.True(M8 == M9)
	M10, err := NewSpace(k3, 0, 6)
	assert.NoError(err)
	// matrix spaces over GF(1) with zero rows or columns
	M11, err := NewSpace(nilField, 0, 6)
	assert.NoError(err)
	assert.True(M10 == M11)
	assert.False(M10 == M4)
	M12, err := NewSpace(k3, 6, 0)
	assert.NoError(err)
	M13, err := NewSpace(k3, 6, 0)
	assert.NoError(err)
	assert.True(M12 == M13)
	assert.False(M12 == M5)
	M14, err := NewSpace(k3, 0, 0)
	assert.NoError(err)
	M15, err := NewSpace(k3, 0, 0)
	assert.NoError(err)
	assert.True(M14 == M15)
	assert.False(M14 == M6)
	//////////////////////////////////////////////////////////////////////
	// test errors
	//////////////////////////////////////////////////////////////////////
	// negative numbers of rows and columns fail
	for _, k := range []*gfp.Parent{k1, k2, k3, nilField} {
		_, err := NewSpace(k, -3, 5)
		assert.Error(err)
		_, err = NewSpace(k, -3, 0)
		assert.Error(err)
		_, err = NewSpace(k, 3, -5)
		assert.Error(err)
		_, err = NewSpace(k, 0, -5)
		assert.Error(err)
	}
}

// TestFrom tests FromSlice, FromIntegerSlice and FromInt64Slice
func TestFrom(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make a field and a matrix space
	//////////////////////////////////////////////////////////////////////
	k, err := gfp.GF(19)
	assert.NoError(err)
	M, err := NewSpace(k, 4, 8)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// make matrices from slices and check they agree
	//////////////////////////////////////////////////////////////////////
	int64S := make([]int64, 0, 32)
	integerS := make([]*integer.Element, 0, 32)
	gfpS := make([]*gfp.Element, 0, 32)
	for i := 0; i < 32; i++ {
		int64S = append(int64S, int64(i))
		integerS = append(integerS, integer.FromInt(i))
		gfpS = append(gfpS, gfp.FromInt(k, i))
	}
	if Aint64, err := FromInt64Slice(M, int64S); assert.NoError(err) {
		if Ainteger, err := FromIntegerSlice(M, integerS); assert.NoError(err) {
			assert.AreEqual(M, Aint64, Ainteger)
		}
		if Agfp, err := FromSlice(M, gfpS); assert.NoError(err) {
			assert.AreEqual(M, Aint64, Agfp)
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests where the number of entries in the slice is wrong
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt64Slice(M, int64S[1:])
	assert.Error(err)
	_, err = FromInt64Slice(M, append(int64S, int64S...))
	assert.Error(err)
	_, err = FromIntegerSlice(M, integerS[1:])
	assert.Error(err)
	_, err = FromIntegerSlice(M, append(integerS, integerS...))
	assert.Error(err)
	_, err = FromSlice(M, gfpS[1:])
	assert.Error(err)
	_, err = FromSlice(M, append(gfpS, gfpS...))
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests involving the nil groundField
	//////////////////////////////////////////////////////////////////////
	gf1, err := gfp.GF(1)
	var nilField *gfp.Parent
	for _, kk := range []*gfp.Parent{gf1, nilField} {
		MM, err := NewSpace(kk, 4, 8)
		assert.NoError(err)
		if Aint64, err := FromInt64Slice(MM, int64S); assert.NoError(err) {
			assert.IsZero(Aint64)
		}
		if Ainteger, err := FromIntegerSlice(MM, integerS); assert.NoError(err) {
			assert.IsZero(Ainteger)
		}
		// this should fail because elements of gfpS are in the wrong field
		_, err = FromSlice(MM, gfpS)
		assert.Error(err)
		// tests where the number of entries in the slice is wrong
		_, err = FromInt64Slice(MM, int64S[1:])
		assert.Error(err)
		_, err = FromInt64Slice(MM, append(int64S, int64S...))
		assert.Error(err)
		_, err = FromIntegerSlice(MM, integerS[1:])
		assert.Error(err)
		_, err = FromIntegerSlice(MM, append(integerS, integerS...))
		assert.Error(err)
		_, err = FromSlice(MM, gfpS[1:])
		assert.Error(err)
		_, err = FromSlice(MM, append(gfpS, gfpS...))
		assert.Error(err)
	}
	//////////////////////////////////////////////////////////////////////
	// tests involving matrices with zero rows or zero columns
	//////////////////////////////////////////////////////////////////////
	for _, kk := range []*gfp.Parent{k, gf1, nilField} {
		M1, err := NewSpace(kk, 0, 6)
		assert.NoError(err)
		M2, err := NewSpace(kk, 3, 0)
		assert.NoError(err)
		M3, err := NewSpace(kk, 0, 0)
		assert.NoError(err)
		for _, M := range []*Parent{M1, M2, M3} {
			if A, err := FromInt64Slice(M, []int64{}); assert.NoError(err) {
				assert.IsZero(A)
			}
			if A, err := FromIntegerSlice(M, []*integer.Element{}); assert.NoError(err) {
				assert.IsZero(A)
			}
			if A, err := FromSlice(M, []*gfp.Element{}); assert.NoError(err) {
				assert.IsZero(A)
			}
			// tests where the number of entries in the slice is wrong
			_, err = FromInt64Slice(M, int64S)
			assert.Error(err)
			_, err = FromIntegerSlice(M, integerS)
			assert.Error(err)
			_, err = FromSlice(M, gfpS)
			assert.Error(err)
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests where some of the finite field elements live in the wrong
	// GF(p)
	//////////////////////////////////////////////////////////////////////
	k2, err := gfp.GF(7)
	assert.NoError(err)
	gfpS[1] = k2.One().(*gfp.Element)
	_, err = FromSlice(M, gfpS)
	assert.Error(err)
	M, err = NewSpace(gf1, 4, 8)
	_, err = FromSlice(M, gfpS)
	assert.Error(err)
	M, err = NewSpace(nilField, 4, 8)
	_, err = FromSlice(M, gfpS)
	assert.Error(err)
}
