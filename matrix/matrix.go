// Matrix defines the interface satisfied by a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package matrix

import (
	"bitbucket.org/pcas/math/abeliangroup"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
)

// NumberOfRowsColumns is the interface satisfied by the NumberOfRows and NumberOfColumns methods.
type NumberOfRowsColumns interface {
	NumberOfRows() int    // NumberOfRows returns the number of rows of a matrix in this matrix space.
	NumberOfColumns() int // NumberOfColumns returns the number of columns of a matrix in this matrix space.
}

// Common defines the interfaces common to a Space and Algebra.
type Common interface {
	NumberOfRowsColumns
	Entry(x object.Element, i int, j int) (object.Element, error)                           // Entry returns the (i,j)-th entry of the matrix x. Rows and columns are indexed from zero. It returns an error if either i or j is negative or too large (i.e. if i and j refer to a non-existent entry of x). The returned entry is an element of the ring containing the entries of the matrices.
	ScalarMultiplyByCoefficient(c object.Element, x object.Element) (object.Element, error) // ScalarMultiplyByCoefficient returns c * x, where c is an element in the ring containing the entries of the matrices.
	Transpose(x object.Element) (object.Element, error)                                     // Transpose returns the transpose of the matrix x. Note that the transpose might not lie in the same parent as x.
	Determinant(x object.Element) (object.Element, error)                                   // Determinant returns the determinant of the matrix x. This is given as an element in the ring of entries.
	Ring() ring.Interface                                                                   // Ring returns the ring containing the entries of the matrices in this matrix space.
	FromSlice(S []object.Element) (object.Element, error)                                   // FromSlice attempts to convert the slice S to a matrix, row by row.
}

// Space defines the interface that all matrix spaces must satisfy.
type Space interface {
	abeliangroup.Interface
	Common
}

// Algebra defines the interface that all matrix algebras must satisfy.
type Algebra interface {
	ring.Interface
	Common
	Inverse(x object.Element) (object.Element, error) // Inverse returns the inverse x^-1 of x, or an error.
}

// Element defines the interface that all matrices must satisfy.
type Element interface {
	object.Element
	NumberOfRowsColumns
}
