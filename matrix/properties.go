// Properties defines common functions for working with matrices.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package matrix

import (
	"bitbucket.org/pcas/math/object"
)

// rower is the interface satisfied by the Row method.
type rower interface {
	Row(x object.Element, i int) ([]object.Element, error) // Row returns the i-th row of the matrix x. Rows are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices.
}

// rowser is the interface satisfied by the Rows method.
type rowser interface {
	Rows(x object.Element) ([][]object.Element, error) // Rows returns the rows of the matrix x. The entries are elements of the ring containing the entries of the matrices.
}

// columner is the interface satisfied by the Column method.
type columner interface {
	Column(x object.Element, i int) ([]object.Element, error) // Column returns the i-th column of the matrix x. Columns are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices.
}

// columnser is the interface satisfied by the Columns method.
type columnser interface {
	Columns(x object.Element) ([][]object.Element, error) // Columns returns the columns of the matrix x. The entries are elements of the ring containing the entries of the matrices.
}

// entrieser is the interface satisfied by the Entries method.
type entrieser interface {
	Entries(x object.Element) ([]object.Element, error) // Entries returns the entries of the matrix x, row by row. The entries are elements of the ring containing the entries of the matrices.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Row returns the i-th row of the matrix x. Rows are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices in R. If R satisfies the interface:
//		type Rower interface {
//			Row(x object.Element, i int) ([]object.Element, error) // Row
//			   returns the i-th row of the matrix x. Rows are indexed from 0.
//			   It returns an error if the index i is out of range. The entries
//			   are elements of the ring containing the entries of the matrices.
//			}
// then R's Row method will be used.
func Row(R Space, x object.Element, i int) ([]object.Element, error) {
	// Can R do this for us?
	if RR, ok := R.(rower); ok {
		return RR.Row(x, i)
	}
	// Create the slice
	ncols := R.NumberOfColumns()
	S := make([]object.Element, 0, ncols)
	// Populate the slice
	for j := 0; j < ncols; j++ {
		c, err := R.Entry(x, i, j)
		if err != nil {
			return nil, err
		}
		S = append(S, c)
	}
	return S, nil
}

// Rows returns the rows of the matrix x. The entries are elements of the ring containing the entries of the matrices in R. If R satisfies the interface:
//		type Rowser interface {
//			Rows(x object.Element) ([][]object.Element, error) // Rows returns
//			   the rows of the matrix x. The entries are elements of the ring
//			   containing the entries of the matrices.
//		}
// then R's Rows method will be used.
func Rows(R Space, x object.Element) ([][]object.Element, error) {
	// Can R do this for us?
	if RR, ok := R.(rowser); ok {
		return RR.Rows(x)
	} else if RR, ok := R.(entrieser); ok {
		// Recover the entries
		es, err := RR.Entries(x)
		if err != nil {
			return nil, err
		}
		// Split the entries into rows
		nrows, ncols := R.NumberOfRows(), R.NumberOfColumns()
		S := make([][]object.Element, 0, nrows)
		for i := 0; i < nrows; i++ {
			S = append(S, es[i*ncols:(i+1)*ncols])
		}
		return S, nil
	}
	// Create the slice
	nrows := R.NumberOfRows()
	S := make([][]object.Element, 0, nrows)
	// Populate the slice
	for i := 0; i < nrows; i++ {
		T, err := Row(R, x, i)
		if err != nil {
			return nil, err
		}
		S = append(S, T)
	}
	return S, nil
}

// Column returns the i-th column of the matrix x. Columns are indexed from 0. It returns an error if the index i is out of range. The entries are elements of the ring containing the entries of the matrices in R. If R satisfies the interface:
//		type Columner interface {
//			Column(x object.Element, i int) ([]object.Element, error) // Column
//			   returns the i-th column of the matrix x. Columns are indexed
//			   from 0. It returns an error if the index i is out of range. The
//			   entries are elements of the ring containing the entries of the
//			   matrices.
//		}
// then R's Column method will be used.
func Column(R Space, x object.Element, i int) ([]object.Element, error) {
	// Can R do this for us?
	if RR, ok := R.(columner); ok {
		return RR.Column(x, i)
	}
	// Create the slice
	nrows := R.NumberOfRows()
	S := make([]object.Element, 0, nrows)
	// Populate the slice
	for j := 0; j < nrows; j++ {
		c, err := R.Entry(x, j, i)
		if err != nil {
			return nil, err
		}
		S = append(S, c)
	}
	return S, nil
}

// Columns returns the columns of the matrix x. The entries are elements of the ring containing the entries of the matrices in R. If R satisfies the interface:
//		type Columnser interface {
//			Columns(x object.Element) ([][]object.Element, error) // Columns
//			   returns the columns of the matrix x. The entries are elements of
//			   the ring containing the entries of the matrices.
//		}
// then R's Columns method will be used.
func Columns(R Space, x object.Element) ([][]object.Element, error) {
	// Can R do this for us?
	if RR, ok := R.(columnser); ok {
		return RR.Columns(x)
	}
	// Create the slice
	ncols := R.NumberOfColumns()
	S := make([][]object.Element, 0, ncols)
	// Populate the slice
	for i := 0; i < ncols; i++ {
		T, err := Column(R, x, i)
		if err != nil {
			return nil, err
		}
		S = append(S, T)
	}
	return S, nil
}

// Entries returns the entries of the matrix x, row by row. The entries are elements of the ring containing the entries of the matrices in R. If R satisfies the interface:
//		type Entrieser interface {
//			Entries(x object.Element) ([]object.Element, error) // Entries
//			   returns the entries of the matrix x, row by row. The entries are
//			   elements of the ring containing the entries of the matrices.
//		}
// then R's Entries method will be used.
func Entries(R Space, x object.Element) ([]object.Element, error) {
	// Can R do this for us?
	if RR, ok := R.(entrieser); ok {
		return RR.Entries(x)
	}
	// Recover the rows of x
	rows, err := Rows(R, x)
	if err != nil {
		return nil, err
	}
	// Concatenate the rows
	S := make([]object.Element, R.NumberOfRows()*R.NumberOfColumns())
	offset := 0
	for _, row := range rows {
		copy(S[offset:offset+len(row)], row)
		offset += len(row)
	}
	return S, nil
}
