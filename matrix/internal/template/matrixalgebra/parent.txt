//go:generate ../../internal/template/template.py ../../internal/template/matrixalgebra/parent.txt parent.go "package={{package}}" "dir={{dir}}" "description={{description}}"
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 *** DO NOT DIRECTLY EDIT THIS FILE ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
This file is auto-generated from "../../internal/template/matrixalgebra/parent.txt".
To modify this file, edit "../../internal/template/matrixalgebra/parent.txt" and run "go generate".
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// Parent defines the parent matrix algebra for matrices with {{description}}.
/*
This work is licensed under CC0 1.0 Universal (CC0 1.0), Public Domain Dedication. For details see:
   http://creativecommons.org/publicdomain/zero/1.0/
*/
// Online go-documentation is available at:
//   https://godoc.org/bitbucket.org/pcas/math/matrix/{{dir}}/{{package}}
/////////////////////////////////////////////////////////////////////////
// parent.go
/////////////////////////////////////////////////////////////////////////

package {{package}}

import (
	"bitbucket.org/pcas/math/matrix/internal/entries"
	"bitbucket.org/pcas/math/object"
	"strconv"
)

// Parent is the parent matrix algebra for matrices with {{description}}.
type Parent struct {
	h    entries.Helper // The helper for the entries
	n    int            // The number of rows/columns
	zero *Element       // The additive identity element
    one  *Element       // The multiplicative identity element
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createParent returns a new matrix algebra for n x n matrices, using the given entries helper.
func createParent(h entries.Helper, n int) (*Parent, error) {
	// Sanity check
	if err := entries.ValidateSize(n, n); err != nil {
		return nil, err
	}
	// Create and return the parent
	R := &Parent{
		h: h,
		n: n,
	}
	R.zero = createZero(R)
    R.one = createOne(R)
	return R, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// One returns the multiplicative identity of the ring.
func (R *Parent) One() object.Element {
	return One(R)
}

// IsOne returns true iff x is the multiplicative identity element of the ring.
func (R *Parent) IsOne(x object.Element) (bool, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return false, err
	}
	return xx.IsOne(), nil
}

// Multiply returns x * y.
func (R *Parent) Multiply(x object.Element, y object.Element) (object.Element, error) {
    xx, yy, err := objectsToElements(R, x, y)
	if err != nil {
		return nil, err
	}
    return Multiply(xx, yy)
}

// Determinant returns the determinant of the matrix x. This is given as an element in the ring of entries.
func (R *Parent) Determinant(x object.Element) (object.Element, error) {
	xx, err := ToElement(R, x)
	if err != nil {
		return nil, err
	}
	return xx.Determinant(), nil
}

// NumberOfRows returns the number of rows of a matrix in R.
func (R *Parent) NumberOfRows() int {
	if R == nil {
		R = defaultParent()
	}
	return R.n
}

// NumberOfColumns returns the number of columns of a matrix in R.
func (R *Parent) NumberOfColumns() int {
	return R.NumberOfRows()
}

// String returns a string description of this parent.
func (R *Parent) String() string {
	return "MatrixAlgebra(" + R.Ring().String() + ", " + strconv.Itoa(R.NumberOfRows()) + ")"
}

