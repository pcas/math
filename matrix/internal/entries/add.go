// Add defines addition, subtraction, and negation at the level of entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/abelianmonoid"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/worker"
)

// adder is an optional interface that a helper may choose to satisfy in order to add two slices.
type adder interface {
	Add(S1 Interface, S2 Interface) (Interface, error) // Add returns the sum S1 + S2.
}

// subtracter is an optional interface that a helper may choose to satisfy in order to calculate the difference of two slices.
type subtracter interface {
	Subtract(S1 Interface, S2 Interface) (Interface, error) // Subtract returns the difference S1 - S2.
}

// sumer is an optional interface that a helper may choose to satisfy in order to add a slice of slices.
type sumer interface {
	Sum(Es []Interface) (Interface, error) // Sum returns the sum of the matrices described by the slices of entries Es. The slice Es must not be empty.
}

// negater is an optional interface that a slice may choose to satisfy in order to negate the slice.
type negater interface {
	Negate() Interface // Negate returns the negation of the entries.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// validateSizesAreEqual checks that the sizes of the two matrices are equal.
func validateSizesAreEqual(S1 Interface, S2 Interface) error {
	if S1.NumberOfRows() != S2.NumberOfRows() || S1.NumberOfColumns() != S2.NumberOfColumns() {
		return errors.MatrixSizesMustAgree.New(S1.NumberOfRows(), S1.NumberOfColumns(), S2.NumberOfRows(), S2.NumberOfColumns())
	}
	return nil
}

// createAddFunc returns a function to set res[i] = S1[i] + S2[i].
func createAddFunc(S1 Interface, S2 Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, S1.Len())
	// Note the data we'll need
	ncols := S1.NumberOfColumns()
	R := S1.Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		for j := 0; j < ncols; j++ {
			x, err := R.Add(S1.Entry(offset+j), S2.Entry(offset+j))
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

// createSubtractFunc returns a function to set res[i] = S1[i] - S2[i].
func createSubtractFunc(S1 Interface, S2 Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, S1.Len())
	// Note the data we'll need
	ncols := S1.NumberOfColumns()
	R := S1.Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		for j := 0; j < ncols; j++ {
			x, err := R.Subtract(S1.Entry(offset+j), S2.Entry(offset+j))
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

// createSumFunc returns a function to set res[i] = Es[0][i] + ... + Es[n][i], where n = len(Es) - 1.
func createSumFunc(Es []Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, Es[0].Len())
	// Note the data we'll need
	ncols := Es[0].NumberOfColumns()
	R := Es[0].Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		vals := make([]object.Element, len(Es))
		for j := 0; j < ncols; j++ {
			for k, S := range Es {
				vals[k] = S.Entry(offset + j)
			}
			x, err := abelianmonoid.Sum(R, vals...)
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

// createNegateFunc returns a function to set res[i] = -S[i].
func createNegateFunc(S Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, S.Len())
	// Note the data we'll need
	ncols := S.NumberOfColumns()
	R := S.Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		for j := 0; j < ncols; j++ {
			x, err := R.Negate(S.Entry(offset + j))
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns the sum S1 + S2. If h satisfies the interface:
//		type Adder interface {
//			Add(S1 Interface, S2 Interface) (Interface, error) // Add
//			   returns the sum S1 + S2.
//		}
// then h's Add method will be used.
func Add(h Helper, S1 Interface, S2 Interface) (Interface, error) {
	// Sanity check
	if err := validateSizesAreEqual(S1, S2); err != nil {
		return nil, err
	}
	// Check if h supports this method
	if hh, ok := h.(adder); ok {
		return hh.Add(S1, S2)
	}
	// Perform the addition
	res, f := createAddFunc(S1, S2)
	if err := worker.Range(f, 0, S1.NumberOfRows(), 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), S1.NumberOfRows(), S1.NumberOfColumns())
}

// Subtract returns the difference S1 - S2. If h satisfies the interface:
//		type Subtracter interface {
//			Subtract(S1 Interface, S2 Interface) (Interface, error)
//			   // Subtract returns the difference S1 - S2.
//		}
// then h's Subtract method will be used.
func Subtract(h Helper, S1 Interface, S2 Interface) (Interface, error) {
	// Sanity check
	if err := validateSizesAreEqual(S1, S2); err != nil {
		return nil, err
	}
	// Check if h supports this method
	if hh, ok := h.(subtracter); ok {
		return hh.Subtract(S1, S2)
	}
	// Perform the subtraction
	res, f := createSubtractFunc(S1, S2)
	if err := worker.Range(f, 0, S1.NumberOfRows(), 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), S1.NumberOfRows(), S1.NumberOfColumns())
}

// Sum returns the sum of the matrices described by the slices of entries Es. The slice Es must not be empty. If h satisfies the interface:
//		type Sumer interface {
//			Sum(Es []Interface) (Interface, error) // Sum returns the sum of
//				   the matrices described by the slices of entries Es.
//				   The slice Es must not be empty.
//		}
// then h's Sum method will be used.
func Sum(h Helper, Es []Interface) (Interface, error) {
	// Deal with the easy cases
	if size := len(Es); size == 0 {
		return nil, errors.EmptySlice.New()
	} else if size == 1 {
		S := Es[0]
		return h.FromSlice(S, S.NumberOfRows(), S.NumberOfColumns())
	} else if size == 2 {
		return Add(h, Es[0], Es[1])
	}
	// Sanity check
	nrows, ncols := Es[0].NumberOfRows(), Es[0].NumberOfColumns()
	for _, S := range Es {
		if S.NumberOfRows() != nrows || S.NumberOfColumns() != ncols {
			return nil, errors.MatrixSizesMustAgreeNoDetails.New()
		}
	}
	// Check if h supports this method
	if hh, ok := h.(sumer); ok {
		return hh.Sum(Es)
	}
	// Perform the sum
	res, f := createSumFunc(Es)
	if err := worker.Range(f, 0, nrows, 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), nrows, ncols)
}

// Negate returns the negation -S. If S satisfies the interface:
//		type Negater interface {
//			Negate() Interface // Negate returns the negation of the
//				entries.
//		}
// then S's Negate method will be used.
func Negate(h Helper, S Interface) (Interface, error) {
	// Check if S supports this method
	if T, ok := S.(negater); ok {
		return T.Negate(), nil
	}
	// Perform the negation
	res, f := createNegateFunc(S)
	if err := worker.Range(f, 0, S.NumberOfRows(), 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), S.NumberOfRows(), S.NumberOfColumns())
}
