// Multiply defines multiplication at the level of entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/worker"
)

// DotProductFunc defines a function to compute the dot-product of the i-th row with the j-th row.
type DotProductFunc func(i int, j int) (object.Element, error)

// rowrowmultiplier is an optional interface a helper can satisfy to calculate row-by-row products.
type rowrowmultiplier interface {
	RowRowMultiply(S1 Interface, S2 Interface) (Interface, error) // RowRowMultiply returns the slice of entries of pair-wise dot-products of the rows of S1 and S2. That is, the returned slice has (i,j)-th entry equal to the dot-product of the i-th row of S1 with the j-th row of S2. This is equivalent to the matrix multiplication represented by S1 * Transpose(S2). Requires that S1 and S2 have the same number of columns. The returned slice will represent an nrows1 x nrow2 matrix, where nrows1 is the number of rows of S1, and nrows2 is the number of rows of S2.
}

// dotproducter is an optional interface a helper can satisfy to assist with row-by-row products.
type dotproducter interface {
	DotProduct(S1 Interface, S2 Interface) (DotProductFunc, error) // DotProduct returns a dot-product function to compute S1[i] \dot S2[j] of the i-th row of S1 with the j-th row of S2, where S1 and S2 are slices of entries. S1 and S2 must have the same number of columns.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDotProductFunc returns a dot-product function to compute S1[i] \dot S2[j] of the i-th row of S1 with the j-th row of S2, where S1 and S2 are slices of entries. S1 and S2 must have the same number of columns. If h satisfies the dotproducter interface that h's DotProduct method will be used to generate the dot-product function.
func createDotProductFunc(h Helper, S1 Interface, S2 Interface) (DotProductFunc, error) {
	// Sanity check
	ncols := S1.NumberOfColumns()
	if S2.NumberOfColumns() != ncols {
		return nil, errors.NumberOfColumnsOfMatricesDoNotAgree.New(ncols, S2.NumberOfColumns())
	}
	// Does h have a DotProduct method?
	if hh, ok := h.(dotproducter); ok {
		return hh.DotProduct(S1, S2)
	}
	// No luck -- we need to compute the dot-product ourselves
	R := S1.Universe()
	return func(i int, j int) (object.Element, error) {
		// Fetch the rows
		row1, err := Row(i, S1)
		if err != nil {
			return nil, err
		}
		row2, err := Row(j, S2)
		if err != nil {
			return nil, err
		}
		// Return the dot-product
		return ring.DotProduct(R, row1, row2)
	}, nil
}

// createRowRowMultiplyFunc returns a function to set the (i,j)-th entry of res equal to the (i,j)-th dot-product.
func createRowRowMultiplyFunc(dot DotProductFunc, nrows int, ncols int) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, nrows*ncols)
	// Return the function
	return res, func(i int) error {
		offset := ncols * i
		for j := 0; j < ncols; j++ {
			x, err := dot(i, j)
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// RowRowMultiply returns the slice of entries of pair-wise dot-products of the rows of S1 and S2. That is, the returned slice has (i,j)-th entry equal to the dot-product of the i-th row of S1 with the j-th row of S2. This is equivalent to the matrix multiplication represented by S1 * Transpose(S2). Requires that S1 and S2 have the same number of columns. The returned slice will represent an nrows1 x nrow2 matrix, where nrows1 is the number of rows of S1, and nrows2 is the number of rows of S2. If h satisfies the interface:
//		type RowRowMultiplier interface {
//			RowRowMultiply(S1 Interface, S2 Interface) (Interface, error)
//				// RowRowMultiply returns the slice of entries of
//				   pair-wise dot-products of the rows of S1 and S2. That
//				   is, the returned slice has (i,j)-th entry equal to
//				   the dot-product of the i-th row of S1 with the j-th
//				   row of S2. This is equivalent to the matrix
//				   multiplication represented by S1 * Transpose(S2).
//				   Requires that S1 and S2 have the same number of
//				   columns. The returned slice will represent an
//				   nrows1 x nrow2 matrix, where nrows1 is the number of
//				   rows of S1, and nrows2 is the number of rows of S2.
//		}
// then h's RowRowMultiply method will be used. Otherwise, if h satisfies the interface:
//		type DotProducter interface {
//			DotProduct(S1 Interface, S2 Interface) (DotProductFunc, error)
//				// DotProduct returns a dot-product function to compute
//				   S1[i] \dot S2[j] of the i-th row of S1 with the j-th
//				   row of S2, where S1 and S2 are slices of entries. S1
//				   and S2 must have the same number of columns.
//		}
// then h's DotProduct method will be used to assist with the calculation.
func RowRowMultiply(h Helper, S1 Interface, S2 Interface) (Interface, error) {
	// Sanity check
	ncols := S1.NumberOfColumns()
	if S2.NumberOfColumns() != ncols {
		return nil, errors.NumberOfColumnsOfMatricesDoNotAgree.New(ncols, S2.NumberOfColumns())
	}
	// Check if h supports this method
	if hh, ok := h.(rowrowmultiplier); ok {
		return hh.RowRowMultiply(S1, S2)
	}
	// Create the dot-product function
	dot, err := createDotProductFunc(h, S1, S2)
	if err != nil {
		return nil, err
	}
	// Perform the row-row multiplication
	nrows1, nrows2 := S1.NumberOfRows(), S2.NumberOfRows()
	res, f := createRowRowMultiplyFunc(dot, nrows1, nrows2)
	if err := worker.Range(f, 0, nrows1, 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), nrows1, nrows2)
}
