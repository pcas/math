// Common defines low-level basic functions on the entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/mathutil"
)

// rower is an optional interface that a slice may choose to satisfy in order to return the i-th row.
type rower interface {
	Row(i int) []object.Element // Row returns (a copy of) the i-th row. Rows are indexed from 0. This will panic if the index is out of range.
}

// columner is an optional interface that a slice may choose to satisfy in order to return the i-th column.
type columner interface {
	Column(i int) []object.Element // Column returns (a copy of) the i-th column. Columns are indexed from 0. This will panic if the index is out of range.
}

// areEqualer is an optional interface that a helper may choose to satisfy in order to determine when two slices are equal.
type areEqualer interface {
	AreEqual(S1 Interface, S2 Interface) (bool, error) // AreEqual returns true iff S1 = S2.
}

// isZeroer is an optional interface that a slice may choose to satisfy in order to determine whether all entries are zero.
type isZeroer interface {
	IsZero() bool // IsZero returns true iff the matrix defined by the slice is the zero matrix.
}

// zeroSlicer is an optional interface that a helper may choose to satisfy in order to create a slice of zeros.
type zeroSlicer interface {
	ZeroSlice(nrows int, ncols int) (Interface, error) // ZeroSlice returns a slice representing the nrows x ncols zero matrix.
}

// isIdentitier is an optional interface that a slice may choose to satisfy in order to determine whether it corresponds to the identity matrix.
type isIdentitier interface {
	IsIdentity() bool // IsIdentity returns true iff the matrix defined by the slice is the identity matrix.
}

// identitySlicer is an optional interface that a helper may choose to satisfy in order to create a slice corresponding to the identity matrix.
type identitySlicer interface {
	IdentitySlice(n int) (Interface, error) // IdentitySlice returns a slice representing the n x n identity matrix.
}

// transposer is an optional interface that a slice may choose to satisfy in order to construct the transpose.
type transposer interface {
	Transpose() Interface // Transpose returns the transpose of the matrix described by the slice of entries.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ValidateSize checks that the matrix size is valid (i.e. that the number of rows and number of columns are non-negative, and that the size fits in an int).
func ValidateSize(nrows int, ncols int) error {
	if nrows < 0 || ncols < 0 {
		return errors.IllegalMatrixSize.New(nrows, ncols)
	} else if size, ok := mathutil.MultiplyInt64(int64(nrows), int64(ncols)); !ok || size > mathutil.MaxInt {
		return errors.MatrixTooLarge.New()
	}
	return nil
}

// Row returns (a copy of) the i-th row of S. Rows are indexed from 0. It returns an error if the index i is out of range. If S satisfies the interface:
//		type Rower interface {
//			Row(i int) []object.Element // Row returns (a copy of) the i-th
//			   row. Rows are indexed from 0. This will panic if the index is
//			   out of range.
//		}
// then S's Row method will be used.
func Row(i int, S Interface) ([]object.Element, error) {
	// Sanity check
	if i < 0 || i >= S.NumberOfRows() {
		return nil, errors.InvalidIndexRange.New(i, S.NumberOfRows()-1)
	}
	// Check if S supports this method
	if T, ok := S.(rower); ok {
		return T.Row(i), nil
	}
	// Create the row
	ncols := S.NumberOfColumns()
	offset := ncols * i
	row := make([]object.Element, 0, ncols)
	for j := 0; j < ncols; j++ {
		row = append(row, S.Entry(offset+j))
	}
	return row, nil
}

// Column returns (a copy of) the i-th column of S. Columns are indexed from 0. It returns an error if the index i is out of range. If S satisfies the interface:
//		type Columner interface {
//			Column(i int) []object.Element // Column returns (a copy of) the
//			   i-th column. Columns are indexed from 0. This will panic if
//			   the index is out of range.
//		}
// then S's Column method will be used.
func Column(i int, S Interface) ([]object.Element, error) {
	// Sanity check
	ncols := S.NumberOfColumns()
	if i < 0 || i >= ncols {
		return nil, errors.InvalidIndexRange.New(i, ncols-1)
	}
	// Check if S supports this method
	if T, ok := S.(columner); ok {
		return T.Column(i), nil
	}
	// Create the row
	nrows := S.NumberOfRows()
	col := make([]object.Element, 0, nrows)
	for j := 0; j < nrows; j++ {
		col = append(col, S.Entry(ncols*j+i))
	}
	return col, nil
}

// AreEqual returns true iff S1 = S2. If h satisfies the interface:
//		type AreEqualer interface {
//			AreEqual(S1 Interface, S2 Interface) (bool, error) // AreEqual
//			   returns true iff S1 = S2.
//		}
// then h's AreEqual method will be used.
func AreEqual(h Helper, S1 Interface, S2 Interface) (bool, error) {
	// Get the easy checks out of the way
	if S1 == S2 {
		return true, nil
	} else if S1.NumberOfRows() != S2.NumberOfRows() || S1.NumberOfColumns() != S2.NumberOfColumns() {
		return false, nil
	}
	// Check if h supports this method
	if hh, ok := h.(areEqualer); ok {
		return hh.AreEqual(S1, S2)
	}
	// Check that the universes agree
	R := S1.Universe()
	if S2.Universe() != R {
		return false, nil
	}
	// Start comparing the entries
	size := S1.Len()
	for i := 0; i < size; i++ {
		areEqual, err := R.AreEqual(S1.Entry(i), S2.Entry(i))
		if err != nil || !areEqual {
			return false, err
		}
	}
	return true, nil
}

// IsZero returns true iff the matrix defined by the slice of entries S is the zero matrix. If S satisfies the interface:
//		type IsZeroer interface {
//			IsZero() bool // IsZero returns true iff the matrix defined by
//			   the slice is the zero matrix.
//		}
// then S's IsZero method will be used.
func IsZero(S Interface) bool {
	// Check if S supports this method
	if T, ok := S.(isZeroer); ok {
		return T.IsZero()
	}
	// Check that the entries are all zero
	size := S.Len()
	R := S.Universe()
	for i := 0; i < size; i++ {
		isZero, err := R.IsZero(S.Entry(i))
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		} else if !isZero {
			return false
		}
	}
	return true
}

// ZeroSlice returns a slice representing the nrows x ncols zero matrix, using the given helper. If h satisfies the interface:
//		type ZeroSlicer interface {
//			ZeroSlice(nrows int, ncols int) (Interface, error) // ZeroSlice
//			   returns a slice representing the nrows x ncols zero matrix.
//		}
// then h's ZeroSlice method will be used.
func ZeroSlice(h Helper, nrows int, ncols int) (Interface, error) {
	// Sanity check
	if err := ValidateSize(nrows, ncols); err != nil {
		return nil, err
	}
	// Check if h supports this method
	if hh, ok := h.(zeroSlicer); ok {
		return hh.ZeroSlice(nrows, ncols)
	}
	// Create the zero slice
	size := nrows * ncols
	zero := h.Universe().Zero()
	S := make(slice.ElementSlice, 0, size)
	for i := 0; i < size; i++ {
		S = append(S, zero)
	}
	return h.FromSlice(S, nrows, ncols)
}

// IsIdentity returns true iff the matrix defined by the slice of entries S is the identity matrix. If S satisfies the interface:
//		type IsIdentitier interface {
//			IsIdentity() bool // IsIdentity returns true iff the matrix
//				   defined by the slice is the zero matrix.
//		}
// then S's IsIdentity method will be used.
func IsIdentity(S Interface) bool {
	// Check that S is square
	n := S.NumberOfRows()
	if n != S.NumberOfColumns() {
		return false
	}
	// Check if S supports this method
	if T, ok := S.(isIdentitier); ok {
		return T.IsIdentity()
	}
	// Check the entries of S
	R := S.Universe()
	idx := 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if i == j {
				// This is a diagonal entry, so should be one
				if isOne, err := R.IsOne(S.Entry(idx)); err != nil {
					panic(fatal.ImpossibleError(err)) // This should never happen
				} else if !isOne {
					return false
				}
			} else {
				// This is not a diagonal entry, so should be zero
				if isZero, err := R.IsZero(S.Entry(idx)); err != nil {
					panic(fatal.ImpossibleError(err)) // This should never happen
				} else if !isZero {
					return false
				}
			}
			idx++
		}
	}
	return true
}

// IdentitySlice returns a slice representing the n x n identity matrix, using the given helper. If h satisfies the interface:
//		type IdentitySlicer interface {
//			IdentitySlice(n int) (Interface, error) // IdentitySlice returns
//			   a slice representing the n x n identity matrix.
//		}
// then h's IdentitySlice method will be used.
func IdentitySlice(h Helper, n int) (Interface, error) {
	// Sanity check
	if err := ValidateSize(n, n); err != nil {
		return nil, err
	}
	// Check if h supports this method
	if hh, ok := h.(identitySlicer); ok {
		return hh.IdentitySlice(n)
	}
	// Create the identity slice
	R := h.Universe()
	zero, one := R.Zero(), R.One()
	S := make(slice.ElementSlice, 0, n*n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if i == j {
				// This is a diagonal entry, so should be one
				S = append(S, one)
			} else {
				// This is not a diagonal entry, so should be zero
				S = append(S, zero)
			}
		}
	}
	return h.FromSlice(S, n, n)
}

// Transpose returns the transpose of S. If S satisfies the interface:
//		type Transposer interface {
//			Transpose() Interface // Transpose returns the transpose of the
//			   matrix described by the slice of entries.
//		}
// then S's Transpose method will be used.
func Transpose(h Helper, S Interface) (Interface, error) {
	// Check if h supports this method
	if T, ok := S.(transposer); ok {
		return T.Transpose(), nil
	}
	// Note the data we'll need
	nrows, ncols := S.NumberOfRows(), S.NumberOfColumns()
	// Create the transpose
	T := make(slice.ElementSlice, 0, S.Len())
	for i := 0; i < ncols; i++ {
		col, err := Column(i, S)
		if err != nil {
			return nil, err
		}
		T = append(T, col...)
	}
	return h.FromSlice(T, ncols, nrows)
}
