// Entries defines the interface satisfied by the slice of entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/ring"
	"bitbucket.org/pcas/math/slice"
)

// Interface is the interface satisfied by the slice of entries of a matrix.
type Interface interface {
	slice.Interface
	Universe() ring.Interface // Universe returns the common parent of the entries.
	NumberOfRows() int        // NumberOfRows returns the number of rows of the matrix represented by these entries.
	NumberOfColumns() int     // NumberOfColumns returns the number of columns of the matrix represented by these entries.
}

// Helper is the interface satisfied by the helper for a slice of entries of a matrix.
type Helper interface {
	Universe() ring.Interface                                             // Universe returns the common parent of the entries.
	FromSlice(S slice.Interface, nrows int, ncols int) (Interface, error) // FromSlice attempts to create a slice of entries for a matrix of size nrows x ncols, with entries in S.
}
