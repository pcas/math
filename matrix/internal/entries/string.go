// String converts a slice of entries of a matrix to a string representation.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcastools/stringsbuilder"
	"bytes"
	"strings"
	"unicode/utf8"
)

// prettyPrintFunc returns a string str for the i-th entry in the slice. This will panic if i is out of range.
type prettyPrintFunc func(i int) string

// prettyPrinter is an optional interface that a slice of entries may choose to satisfy to assist with printing matrices.
type prettyPrinter interface {
	PrettyPrint(i int) string // PrettyPrint returns a string representation of the i-th entry. This will panic if the index is out of range.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createPrettyPrintFunc returns a pretty-print function for the entries of the slice S.
func createPrettyPrintFunc(S Interface) prettyPrintFunc {
	if T, ok := S.(prettyPrinter); ok {
		return T.PrettyPrint
	}
	return func(i int) string {
		return S.Entry(i).String()
	}
}

// rowsToString writes the rows to the slice of bytes buffers, using the pretty-print function f.
func rowsToString(rows []*bytes.Buffer, ncols int, f prettyPrintFunc) {
	// Create a buffer for the column strings and lengths
	nrows := len(rows)
	colstrs := make([]string, nrows)
	collens := make([]int, nrows)
	// Start working through the columns
	for i := 0; i < ncols; i++ {
		// Start working through the rows converting the column to strings; as
		// we do this we keep track of the maximum width
		var maxLen int
		for j := 0; j < nrows; j++ {
			s := f(ncols*j + i)
			slen := utf8.RuneCountInString(s)
			if slen > maxLen {
				maxLen = slen
			}
			colstrs[j], collens[j] = s, slen
		}
		// If this isn't the first column, we add an extra space
		if i != 0 {
			maxLen++
		}
		// Now add the strings to the rows with padding
		for j, b := range rows {
			b.WriteString(strings.Repeat(" ", maxLen-collens[j]))
			b.WriteString(colstrs[j])
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToString returns a string representation of the matrix described by the slice S. If S satisfies the interface:
//		type PrettyPrinter interface {
//			PrettyPrint(i int) string // PrettyPrint returns a string
//			   representation of the i-th entry. This will panic if the
//			   index is out of range.
//		}
// then S's PrettyPrint method will be used to convert the entries to strings.
func ToString(S Interface) string {
	// Get the zero length case out of the way
	if S.Len() == 0 {
		return "()"
	}
	// Allocate the slice of strings builders (one for each row) and write the
	// opening brackets
	nrows := S.NumberOfRows()
	rows := make([]*strings.Builder, 0, nrows)
	for i := 0; i < nrows; i++ {
		b := stringsbuilder.New()
		if nrows == 1 {
			b.WriteString("(")
		} else if i == 0 {
			b.WriteString("\u239b")
		} else if i == nrows-1 {
			b.WriteString("\u239d")
		} else {
			b.WriteString("\u239c")
		}
		rows = append(rows, b)
	}
	// Write the rows
	rowsToString(rows, S.NumberOfColumns(), createPrettyPrintFunc(S))
	// Write the closing brackets
	for i, b := range rows {
		if nrows == 1 {
			b.WriteString(")")
		} else if i == 0 {
			b.WriteString("\u239e")
		} else if i == nrows-1 {
			b.WriteString("\u23a0")
		} else {
			b.WriteString("\u239f")
		}
	}
	// Concatenate the rows and return the strings builders to the pool ready
	// for reuse as we go along
	res := stringsbuilder.New()
	for i, b := range rows {
		if i != 0 {
			res.WriteString("\n")
		}
		b.WriteTo(res)
		stringsbuilder.Reuse(b)
	}
	// Recover the string and return
	str := res.String()
	stringsbuilder.Reuse(res)
	return str
}
