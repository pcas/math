// Normalform defines optional normal form functions at the level of the entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/errors"
)

// echelonformer is an optional interface that a slice may choose to satisfy in order to return the reduced row echelon form.
type echelonformer interface {
	EchelonForm() Interface // EchelonForm returns the reduced row echelon form of the matrix described by the slice of entries.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// EchelonForm returns the reduced row echelon form of S, or an error if reduced row echelon form is not defined or not implemented over this ground ring.
func EchelonForm(h Helper, S Interface) (Interface, error) {
	// Check if S supports this method
	if T, ok := S.(echelonformer); ok {
		return T.EchelonForm(), nil
	}
	return nil, errors.NotImplemented.New()
}
