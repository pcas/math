// Scalarmultiply defines scalar multiplication at the level of entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcas/math/worker"
)

// scalarMultiplyByIntegerer is an optional interface that a slice may choose to satisfy in order to calculate the (entry-wise) scalar multiple by an integer.
type scalarMultiplyByIntegerer interface {
	ScalarMultiplyByInteger(n *integer.Element) Interface // ScalarMultiplyByInteger of a slice S returns n * S, where this is defined to be S + ... + S (n times) if n is positive, -S - ... - S (|n| times) if n is negative, and 0 if n is zero.
}

// scalarMultiplyByCoefficienter is an optional interface that a slice may choose to satisfy in order to calculate the (entry-wise) scalar multiple by an element in the coefficient ring.
type scalarMultiplyByCoefficienter interface {
	ScalarMultiplyByCoefficient(c object.Element) (Interface, error) // ScalarMultiplyByCoefficient of a slice S returns c * S, where c is an element in the  ring containing the entries of S.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createScalarMultiplyByIntegerFunc returns a function to set res[i] = n * S[i].
func createScalarMultiplyByIntegerFunc(n *integer.Element, S Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, S.Len())
	// Note the data we'll need
	ncols := S.NumberOfColumns()
	R := S.Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		for j := 0; j < ncols; j++ {
			x, err := R.ScalarMultiplyByInteger(n, S.Entry(offset+j))
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

// createScalarMultiplyByCoefficientFunc returns a function to set res[i] = c * S[i].
func createScalarMultiplyByCoefficientFunc(c object.Element, S Interface) ([]object.Element, worker.RangeFunc) {
	// Create the storage for the result
	res := make([]object.Element, S.Len())
	// Note the data we'll need
	ncols := S.NumberOfColumns()
	R := S.Universe()
	// Return the function
	return res, func(row int) error {
		offset := row * ncols
		for j := 0; j < ncols; j++ {
			x, err := R.Multiply(c, S.Entry(offset+j))
			if err != nil {
				return err
			}
			res[offset+j] = x
		}
		return nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ScalarMultiplyByInteger returns n * S, where this is defined to be S + ... + S (n times) if n is positive, -S - ... - S (|n| times) if n is negative, and 0 if n is zero. If S satisfies the interface:
//		type ScalarMultiplyByIntegerer interface {
//			ScalarMultiplyByInteger(n *integer.Element) Interface
//				// ScalarMultiplyByInteger of a slice S returns n * S,
//				   where this is defined to be S + ... + S (n times) if
//				   n is positive, -S - ... - S (|n| times) if n is
//				   negative, and 0 if n is zero.
//		}
// then S's ScalarMultiplyByInteger method will be used.
func ScalarMultiplyByInteger(h Helper, n *integer.Element, S Interface) (Interface, error) {
	// Handle the case when n is 0, 1, or -1
	if n.IsZero() {
		return ZeroSlice(h, S.NumberOfRows(), S.NumberOfColumns())
	} else if n.IsOne() {
		return h.FromSlice(S, S.NumberOfRows(), S.NumberOfColumns())
	} else if n.IsEqualToInt64(-1) {
		return Negate(h, S)
	}
	// Check if S supports this method
	if T, ok := S.(scalarMultiplyByIntegerer); ok {
		return T.ScalarMultiplyByInteger(n), nil
	}
	// Perform the scalar multiplication
	res, f := createScalarMultiplyByIntegerFunc(n, S)
	if err := worker.Range(f, 0, S.NumberOfRows(), 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), S.NumberOfRows(), S.NumberOfColumns())
}

// ScalarMultiplyByCoefficient returns c * S, where c is an element in the  ring containing the entries of S. If S satisfies the interface:
//		type ScalarMultiplyByCoefficienter interface {
//			ScalarMultiplyByCoefficient(c object.Element) (Interface, error)
//				// ScalarMultiplyByCoefficient of a slice S returns
//				   c * S, where c is an element in the  ring containing
//				   the entries of S.
//		}
// then S's ScalarMultiplyByInteger method will be used.
func ScalarMultiplyByCoefficient(h Helper, c object.Element, S Interface) (Interface, error) {
	// Check if S supports this method
	if T, ok := S.(scalarMultiplyByCoefficienter); ok {
		return T.ScalarMultiplyByCoefficient(c)
	}
	// Perform the scalar multiplication
	res, f := createScalarMultiplyByCoefficientFunc(c, S)
	if err := worker.Range(f, 0, S.NumberOfRows(), 1); err != nil {
		return nil, err
	}
	return h.FromSlice(slice.ElementSlice(res), S.NumberOfRows(), S.NumberOfColumns())
}
