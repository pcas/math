// Laplace defines a generic determinant computation via Laplace's method at the level of entries of a matrix.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package entries

import (
	"bitbucket.org/pcas/math/abelianmonoid"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/worker"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createInitialRowColumnSlices returns slices [0..nrows - 1] and [0..ncols - 1] for S.
func createInitialRowColumnSlices(S Interface) (rows []int, cols []int) {
	nrows := S.NumberOfRows()
	rows = make([]int, 0, nrows)
	for i := 0; i < nrows; i++ {
		rows = append(rows, i)
	}
	ncols := S.NumberOfColumns()
	cols = make([]int, 0, ncols)
	for i := 0; i < ncols; i++ {
		cols = append(cols, i)
	}
	return
}

// removeIndex returns a copy of the slice I with the i-th entry removed. This will panic if i is out of range.
func removeIndex(I []int, i int) []int {
	J := make([]int, len(I)-1)
	copy(J, I[:i])
	copy(J[i:], I[i+1:])
	return J
}

// indexOfRowWithMaximumZeros returns the index in 'rows' of the row in S contains the largest number of zero entries in the columns given by 'cols'. Also returns the number of zeros.
func indexOfRowWithMaximumZeros(S Interface, rows []int, cols []int) (int, int, error) {
	ncols := S.NumberOfColumns()
	R := S.Universe()
	max := -1
	idx := 0
	for n, i := range rows {
		offset := ncols * i
		count := 0
		for _, j := range cols {
			if isZero, err := R.IsZero(S.Entry(offset + j)); err != nil {
				return 0, 0, err
			} else if isZero {
				count++
			}
		}
		if count > max {
			max = count
			idx = n
		}
	}
	return idx, max, nil
}

// indexOfColumnWithMaximumZeros returns the index in 'cols' of the column in S contains the largest number of zero entries in the rows given by 'rows'. Also returns the number of zeros.
func indexOfColumnWithMaximumZeros(S Interface, rows []int, cols []int) (int, int, error) {
	ncols := S.NumberOfColumns()
	R := S.Universe()
	max := -1
	idx := 0
	for n, j := range cols {
		count := 0
		for _, i := range rows {
			if isZero, err := R.IsZero(S.Entry(ncols*i + j)); err != nil {
				return 0, 0, err
			} else if isZero {
				count++
			}
		}
		if count > max {
			max = count
			idx = n
		}
	}
	return idx, max, nil
}

// expandAlongRowInParallel recursively expands along the rows[idx]-th row of S, returning the determinant. The calculation is done in parallel.
func expandAlongRowInParallel(S Interface, rows []int, cols []int, idx int) (object.Element, error) {
	// Note the offset
	offset := S.NumberOfColumns() * rows[idx]
	// Remove this row from the slice of rows
	rows = removeIndex(rows, idx)
	// Record the indices of the non-zero entries in this row
	R := S.Universe()
	idxs := make([]int, 0, len(cols))
	for n, j := range cols {
		if isZero, err := R.IsZero(S.Entry(offset + j)); err != nil {
			return nil, err
		} else if !isZero {
			idxs = append(idxs, n)
		}
	}
	// Allocate space to store the non-zero minors
	dets := make([]object.Element, len(idxs))
	// Set the workers recursing
	if err := worker.Range(func(k int) error {
		// Note the column index
		n := idxs[k]
		// Recurse on this entry
		d, err := determinantViaLaplaceRecurse(S, rows, removeIndex(cols, n))
		if err != nil {
			return err
		}
		// Multiply the minor by the signed entry
		if d, err = R.Multiply(S.Entry(offset+cols[n]), d); err != nil {
			return err
		} else if (idx+n)%2 == 1 {
			if d, err = R.Negate(d); err != nil {
				return err
			}
		}
		// Record the result
		dets[k] = d
		return nil
	}, 0, len(idxs), 1); err != nil {
		return nil, err
	}
	// Return the result
	return abelianmonoid.Sum(R, dets...)
}

// expandAlongRow recursively expands along the rows[idx]-th row of S, returning the determinant.
func expandAlongRow(S Interface, rows []int, cols []int, idx int) (object.Element, error) {
	// Note the offset
	offset := S.NumberOfColumns() * rows[idx]
	// Remove this row from the slice of rows
	rows = removeIndex(rows, idx)
	// Allocate space to store the non-zero minors
	dets := make([]object.Element, 0, len(cols))
	// Start working along the row
	R := S.Universe()
	sgn := 1 - 2*(idx%2)
	for n, j := range cols {
		// Is this entry non-zero?
		c := S.Entry(offset + j)
		if isZero, err := R.IsZero(c); err != nil {
			return nil, err
		} else if !isZero {
			// Recurse on this entry
			var d object.Element
			if d, err = determinantViaLaplaceRecurse(S, rows, removeIndex(cols, n)); err != nil {
				return nil, err
			} else if d, err = R.Multiply(c, d); err != nil {
				return nil, err
			} else if sgn == -1 {
				if d, err = R.Negate(d); err != nil {
					return nil, err
				}
			}
			dets = append(dets, d)
		}
		// Move on
		sgn *= -1
	}
	// Return the result
	return abelianmonoid.Sum(R, dets...)
}

// expandDownColumnInParallel recursively expands down the cols[idx]-th column of S, returning the determinant. The calculation is done in parallel.
func expandDownColumnInParallel(S Interface, rows []int, cols []int, idx int) (object.Element, error) {
	// Note the total number of columns in S
	ncols := S.NumberOfColumns()
	// Note the column, and remove this column from the slice of columns
	j := cols[idx]
	cols = removeIndex(cols, idx)
	// Record the indices of the non-zero entries in this column
	R := S.Universe()
	idxs := make([]int, 0, len(rows))
	for n, i := range rows {
		if isZero, err := R.IsZero(S.Entry(ncols*i + j)); err != nil {
			return nil, err
		} else if !isZero {
			idxs = append(idxs, n)
		}
	}
	// Allocate space to store the non-zero minors
	dets := make([]object.Element, len(idxs))
	// Set the workers recursing
	if err := worker.Range(func(k int) error {
		// Note the row index
		n := idxs[k]
		// Recurse on this entry
		d, err := determinantViaLaplaceRecurse(S, removeIndex(rows, n), cols)
		if err != nil {
			return err
		}
		// Multiply the minor by the signed entry
		if d, err = R.Multiply(S.Entry(ncols*rows[n]+j), d); err != nil {
			return err
		} else if (idx+n)%2 == 1 {
			if d, err = R.Negate(d); err != nil {
				return err
			}
		}
		// Record the result
		dets[k] = d
		return nil
	}, 0, len(idxs)-1, 1); err != nil {
		return nil, err
	}
	// Return the result
	return abelianmonoid.Sum(R, dets...)
}

// expandDownColumn recursively expands down the cols[idx]-th column of S, returning the determinant.
func expandDownColumn(S Interface, rows []int, cols []int, idx int) (object.Element, error) {
	// Note the total number of columns in S
	ncols := S.NumberOfColumns()
	// Note the column, and remove this column from the slice of columns
	j := cols[idx]
	cols = removeIndex(cols, idx)
	// Allocate space to store the non-zero minors
	dets := make([]object.Element, 0, len(rows))
	// Start working down the column
	R := S.Universe()
	sgn := 1 - 2*(idx%2)
	for n, i := range rows {
		// Is this entry non-zero?
		c := S.Entry(ncols*i + j)
		if isZero, err := R.IsZero(c); err != nil {
			return nil, err
		} else if !isZero {
			// Recurse on this entry
			var d object.Element
			if d, err = determinantViaLaplaceRecurse(S, removeIndex(rows, n), cols); err != nil {
				return nil, err
			} else if d, err = R.Multiply(c, d); err != nil {
				return nil, err
			} else if sgn == -1 {
				if d, err = R.Negate(d); err != nil {
					return nil, err
				}
			}
			dets = append(dets, d)
		}
		// Move on
		sgn *= -1
	}
	// Return the result
	return abelianmonoid.Sum(R, dets...)
}

// determinantViaLaplaceRecurse recursively calculates the determinant by expanding along a row or down a column of S.
func determinantViaLaplaceRecurse(S Interface, rows []int, cols []int) (object.Element, error) {
	// If this is a 2x2 block, calculate the determinant
	if len(rows) == 2 {
		ncols := S.NumberOfColumns()
		R := S.Universe()
		x1, err := R.Multiply(S.Entry(ncols*rows[0]+cols[0]), S.Entry(ncols*rows[1]+cols[1]))
		if err != nil {
			return nil, err
		}
		x2, err := R.Multiply(S.Entry(ncols*rows[0]+cols[1]), S.Entry(ncols*rows[1]+cols[0]))
		if err != nil {
			return nil, err
		}
		return R.Subtract(x1, x2)
	}
	// Find the row or column with the maximum number of zeros
	rowidx, rownum, err := indexOfRowWithMaximumZeros(S, rows, cols)
	if err != nil {
		return nil, err
	}
	colidx, colnum, err := indexOfColumnWithMaximumZeros(S, rows, cols)
	if err != nil {
		return nil, err
	}
	// Are we going to expand down the row or the column?
	if colnum > rownum {
		return expandDownColumn(S, rows, cols, colidx)
	}
	return expandAlongRow(S, rows, cols, rowidx)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// DeterminantViaLaplace computes the determinant of the matrix represented by the slice of entries S using Laplace's algorithm.
func DeterminantViaLaplace(S Interface) (object.Element, error) {
	// Sanity check
	n := S.NumberOfRows()
	if S.NumberOfColumns() != n {
		return nil, errors.MatrixNotSquare.New()
	}
	// Handle the very small cases
	if n == 0 {
		return S.Universe().Zero(), nil
	} else if n == 1 {
		return S.Entry(0), nil
	} else if n == 2 {
		R := S.Universe()
		x1, err := R.Multiply(S.Entry(0), S.Entry(3))
		if err != nil {
			return nil, err
		}
		x2, err := R.Multiply(S.Entry(1), S.Entry(2))
		if err != nil {
			return nil, err
		}
		return R.Subtract(x1, x2)
	}
	// Create the initial slices of rows and columns
	rows, cols := createInitialRowColumnSlices(S)
	// Find the row or column with the maximum number of zeros
	rowidx, rownum, err := indexOfRowWithMaximumZeros(S, rows, cols)
	if err != nil {
		return nil, err
	}
	colidx, colnum, err := indexOfColumnWithMaximumZeros(S, rows, cols)
	if err != nil {
		return nil, err
	}
	// Are we going to expand down the row or the column?
	if colnum > rownum {
		return expandDownColumnInParallel(S, rows, cols, colidx)
	}
	return expandAlongRowInParallel(S, rows, cols, rowidx)
}
