// State describes a worker's state.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package worker

import (
	"strconv"
)

// State represents the state of a worker.
type State int

// The valid worker states.
const (
	Uninitialised = State(iota) // Uninitialised
	Waiting                     // Waiting for a task
	Running                     // Running
	Error                       // Exited in an error state
	Shutdown                    // Exited cleanly
)

/////////////////////////////////////////////////////////////////////////
// State functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the state.
func (t State) String() (s string) {
	switch t {
	case Uninitialised:
		s = "uninitialised"
	case Waiting:
		s = "waiting"
	case Running:
		s = "running"
	case Error:
		s = "error"
	case Shutdown:
		s = "shutdown"
	default:
		s = "unknown [" + strconv.Itoa(int(t)) + "]"
	}
	return
}
