// Group provides a way to collect together a collection of workers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package worker

import (
	"strings"
	"sync"
)

// Group represents a group of workers, and satisfies the worker Interface.
type Group struct {
	queue    chan interface{} // The worker task channel
	done     chan struct{}    // Closed when the group's workers have shut down
	finished chan bool        // Signalled when a worker exits
	m        *sync.RWMutex    // Controls access to the following variables
	ws       []Interface      // The workers in the group
	lastErr  error            // The last error (if any)
	isOpen   bool             // Is the group open for use?
}

/////////////////////////////////////////////////////////////////////////
// Group functions
/////////////////////////////////////////////////////////////////////////

// setError records the error in lastErr (if not already set).
func (g *Group) setError(err error) {
	if err != nil {
		g.m.Lock()
		if g.lastErr == nil {
			g.lastErr = err
		}
		g.m.Unlock()
	}
}

// AddWorker adds a worker to the group.
func (g *Group) AddWorker(w Interface) error {
	// Acquire a write lock on the group
	g.m.Lock()
	defer g.m.Unlock()
	// Sanity check
	if g.lastErr != nil {
		return g.lastErr
	} else if !g.isOpen {
		return ErrShutdown
	}
	// Add the worker to the group
	if g.ws == nil {
		g.ws = make([]Interface, 0, 3)
	}
	g.ws = append(g.ws, w)
	// Feed the worker with the queue
	Feed(g.queue, w)
	// Watch for the worker to exit and trigger a shutdown
	go func() {
		<-w.Done()
		g.Shutdown()
	}()
	return nil
}

// Add adds a task for a worker in the group. This will block until space becomes available on the worker's queue.
func (g *Group) Add(x interface{}) error {
	// Acquire a read-lock on the group
	g.m.RLock()
	defer g.m.RUnlock()
	// Sanity check
	if g.lastErr != nil {
		return g.lastErr
	} else if !g.isOpen {
		return ErrShutdown
	}
	// Feed the task to the workers
	g.queue <- x
	return nil
}

// Done returns a channel that will be closed when the workers in the group have shut down (or have entered an error state).
func (g *Group) Done() <-chan struct{} {
	return g.done
}

// Status returns the group's current state, as determined by the workers in the group.
func (g *Group) Status() State {
	// Aquire a read-lock on the group
	g.m.RLock()
	defer g.m.RUnlock()
	// Are we registered in an error state?
	if g.lastErr != nil {
		return Error
	}
	// Walk the workers assessing their state
	var state State
	for _, w := range g.ws {
		switch w.Status() {
		case Waiting:
			if state != Running {
				state = Waiting
			}
		case Running:
			state = Running
		case Shuttingdown:
			if state == Initializing || state == Shutdown {
				state = Shuttingdown
			}
		case Error:
			g.lastErr = w.Err()
			return Error
		case Shutdown:
			if state == Initializing {
				state = Shutdown
			}
		}
	}
	return state
}

// Err returns an error if one of the workers in the group has entered an error state.
func (g *Group) Err() error {
	g.m.RLock()
	err := g.lastErr
	g.m.RUnlock()
	return err
}

// Shutdown asks the workers in the group to shut down. This will block until all the workers have shut down (or have entered an error state). If after shut down one of the workers in the group is in an error state, that error will be returned.
func (g *Group) Shutdown() error {
	// Ensure that the queue is drained
	go drainInterfaceChannel(g.queue)
	// Acquire a write-lock on the group
	g.m.Lock()
	// Is there anything to do?
	if !g.isOpen {
		g.m.Unlock()
		<-g.done
		return g.Err()
	}
	// Mark the group as closed and close the queue
	g.isOpen = false
	close(g.queue)
	g.m.Unlock()
	// Ask the workers to shut down
	wg := &sync.WaitGroup{}
	for _, w := range g.ws {
		wg.Add(1)
		go func(w Interface) {
			g.setError(w.Shutdown())
			wg.Done()
		}(w)
	}
	wg.Wait()
	// Close the done channel and return any errors
	close(g.done)
	return g.Err()
}

// String returns a string description of this group of workers in the form "Workers(%s1,%s2,...,%sn)" where "%s1", "%s2", ..., "%sn" are the states of the workers in this group.
func (g *Group) String() string {
	g.m.RLock()
	s := make([]string, 0, len(g.ws))
	for _, w := range g.ws {
		s = append(s, w.Status().String())
	}
	g.m.RUnlock()
	return "Workers(" + strings.Join(s, ",") + ")"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewGroup returns a new group for the given workers.
func NewGroup(ws ...Interface) *Group {
	// Create the new group
	g := &Group{
		queue:    make(chan interface{}),
		done:     make(chan struct{}),
		finished: make(chan bool),
		m:        &sync.RWMutex{},
		isOpen:   true,
	}
	// Add the workers
	for _, w := range ws {
		g.AddWorker(w)
	}
	// Return the group
	return g
}

// NewGroupUsingWorkFunc returns a new group populated with n workers created by wrapping the given WorkFunc f (which must be non-nil).
func NewGroupFromWorkFunc(ctx context.Context, f WorkFunc, n int) *Group {
	if n < 0 {
		panic("The number of workers must be a non-negative  integer")
	}
	ws := make([]Interface, 0, n)
	for i := 0; i < n; i++ {
		ws = append(ws, Wrap(ctx, f))
	}
	return NewGroup(ws...)
}
