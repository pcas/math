// Range provides a way to call a function for each value in a given range.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package worker

import (
	"github.com/pkg/errors"
	"runtime"
	"runtime/debug"
	"sync"
)

// RangeFunc defines the function used by Range.
type RangeFunc func(i int) error

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// chooseNumWorkers picks the number of workers to use to process the data of size 'size'. The minimum range per worker can be specified by 'minRangeSize', however this is only used as a guide.
func chooseNumWorkers(size int, minRangeSize int) int {
	if minRangeSize < 1 {
		minRangeSize = 1
	}
	if size <= minRangeSize {
		return 1
	}
	numWorkers := size / minRangeSize
	if size%minRangeSize != 0 {
		numWorkers++
	}
	if numCpus := runtime.NumCPU(); numWorkers > numCpus {
		numWorkers = numCpus
	}
	return numWorkers
}

// rangeWorker calls the function f for the range start <= i < finish. Any error will be recorded in err, which is protected by the mutex m. On finish, the wait group wg will be decremented.
func rangeWorker(start int, finish int, m *sync.Mutex, err *error, wg *sync.WaitGroup, f RangeFunc) {
	defer wg.Done()
	for i := start; i < finish; i++ {
		if e := f(i); e != nil {
			m.Lock()
			*err = e
			m.Unlock()
			return
		}
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Range applies the given function f to the range start <= i < finish, returning any errors. The minimum range per worker can be specified by 'minRangeSize', however this is only used as a guide.
func Range(f RangeFunc, start int, finish int, minRangeSize int) (err error) {
	// Is there anything to do?
	if start >= finish {
		return
	}
	// Recover from any panics caused by the user's function f
	defer func() {
		if r := recover(); r != nil {
			if e, ok := r.(error); ok {
				err = errors.Wrapf(e, "Panic in Range\n%s", debug.Stack())
			} else {
				err = errors.Errorf("Panic in Range: %v\n%s", r, debug.Stack())
			}
		}
	}()
	// Decide how many workers to use
	size := finish - start
	numWorkers := chooseNumWorkers(size, minRangeSize)
	// If there is only one worker, we do the calculation now
	if numWorkers == 1 {
		for i := start; i < finish; i++ {
			if err = f(i); err != nil {
				return
			}
		}
		return
	}
	// Allocate the data we need for communicating with the workers
	var m sync.Mutex
	var wg sync.WaitGroup
	// Set the workers running and wait for them to finish
	block := size / numWorkers
	wg.Add(numWorkers)
	for i := 0; i < numWorkers-1; i++ {
		go rangeWorker(start+i*block, start+(i+1)*block, &m, &err, &wg, f)
	}
	go rangeWorker(start+(numWorkers-1)*block, finish, &m, &err, &wg, f)
	wg.Wait()
	// Return any errors
	return
}
