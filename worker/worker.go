// Worker describes the interface a Worker must satisfy, and provides wrappers to simplify creating a basic Worker.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package worker

import (
	"context"
	"github.com/pkg/errors"
	"sync/atomic"
)

// Interface defines the interface satisfied by a worker.
type Interface interface {
	Add(x interface{}) error // Add submits the task x to the worker.
	Close() error            // Close asks the worker to exit. This will block until the worker has exited.
}

// WorkFunc defines a function that can be wrapped by Wrap to create a worker. A task x will be passed to the function. If the function returns an error then the worker will be placed in an error state and will stop accepting tasks.
type WorkFunc func(ctx context.Context, x interface{}) error

// token provides a unique type for communication with the worker.
type token int

// The valid tokens for communication with the worker.
const (
	shutdownToken = token(iota) // Shutdown
)

// Worker provides a worker by wrapping a WorkFunc.
type Worker struct {
	queueC   chan<- interface{} // The worker task channel
	doneC    <-chan struct{}    // Closed when then worker exits
	state    int32              // The worker's state
	lastErr  error              // The worker's last error (if any)
	lastTask interface{}        // The task that caused the last error (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// execWorkerFunc executes the work function, recovering from any panics.
func execWorkerFunc(ctx context.Context, f WorkFunc, x interface{}) (err error) {
	// Recover from any panics caused by the user's worker function f
	defer func() {
		if e := recover(); e != nil {
			err = errors.Errorf("Panic in WorkerFunc: %s", e)
		}
	}()
	// Execute the function
	err = f(ctx, x)
	return
}

/////////////////////////////////////////////////////////////////////////
// Worker functions
/////////////////////////////////////////////////////////////////////////

// run starts the background worker. Tasks are read from the channel queueC and passed to the work function f for processing; send shutdownToken down queueC to ask the worker to exit. The channel doneC is closed on exit. If the work function returns an error then the worker will be placed in an Error state, the task that caused the error will be recorded in w.lastTask, and the error will be recorded in w.lastErr. Intended to be run in its own go-routine.
func (w *Worker) run(ctx context.Context, f WorkFunc, queueC <-chan interface{}, doneC chan<- struct{}) {
	// Defer closing the doneC channel on exit
	defer close(doneC)
	// Place the worker in the Waiting state
	atomic.StoreInt32(&w.state, int32(Waiting))
	// Work through the tasks
	for x := range queueC {
		// Have we been asked to Shutdown?
		if t, ok := x.(token); ok && t == shutdownToken {
			break
		}
		// Place the worker in the Running state
		atomic.StoreInt32(&w.state, int32(Running))
		// Pass the task to the worker function
		if err := execWorkerFunc(ctx, f, x); err != nil {
			// Place the worker in the Error state
			w.lastErr, w.lastTask = err, x
			atomic.StoreInt32(&w.state, int32(Error))
			return
		}
		// Place the worker in the Waiting state
		atomic.StoreInt32(&w.state, int32(Waiting))
	}
	// Place the worker in the Shutdown state
	atomic.StoreInt32(&w.state, int32(Shutdown))
}

// Add submits the task x to the worker. This will block until the worker is free to accept the task. If the worker has exited (because it has Shutdown or is in an Error state) then ErrWorkerNotRunning will be returned.
//
// Note that Add returning nil only guarantees that the task was passed to the worker's WorkFunc, and not that the WorkFunc successfully processed the task. If the WorkFunc fails to successfully process the task (i.e. if the WorkFunc returns an error) then the worker will be placed in an Error state, and the task x, along with the resulting error, can be recovered by calling TaskAndErr.
func (w *Worker) Add(x interface{}) error {
	if w == nil || w.doneC == nil {
		return ErrWorkerNotRunning
	}
	select {
	case w.queueC <- x:
	case <-w.doneC:
		return ErrWorkerNotRunning
	}
	return nil
}

// Close asks the worker to exit. This will block until the worker has exited (because it has Shutdown or is in an Error state). If the worker exited in an Error state, the worker's last error will be returned. On return, Done is closed.
func (w *Worker) Close() error {
	if w == nil || w.doneC == nil {
		return nil
	}
	select {
	case w.queueC <- shutdownToken:
		<-w.doneC
	case <-w.doneC:
	}
	return w.lastErr
}

// Done returns a channel that will be closed when the worker has exited (because it has Shutdown or is in an Error state).
func (w *Worker) Done() <-chan struct{} {
	if w == nil || w.doneC == nil {
		c := make(chan struct{})
		close(c)
		return c
	}
	return w.doneC
}

// State returns the worker's current state.
func (w *Worker) State() State {
	if w == nil || w.doneC == nil {
		return Uninitialised
	}
	s := State(atomic.LoadInt32(&w.state))
	// If the worker is exiting, wait for it to exit before returning
	if s == Shutdown || s == Error {
		<-w.doneC
	}
	return s
}

// Err returns the worker's last error (if any). This is non-nil if and only if the worker is in an Error state. After Err returns a non-nil error, successive calls to Err return the same error.
func (w *Worker) Err() error {
	_, err := w.TaskAndErr()
	return err
}

// TaskAndErr returns the worker's last error err (if any) and the task x that caused this error. This is non-nil if and only if the worker is in an Error state. After TaskAndErr returns a non-nil task and error, successive calls to TaskAndErr return the same task and error.
func (w *Worker) TaskAndErr() (x interface{}, err error) {
	if w == nil || w.doneC == nil {
		return nil, nil
	}
	select {
	case <-w.doneC:
		x, err = w.lastTask, w.lastErr
	default:
	}
	return
}

// String returns the string "Worker(%s)" where "%s" is the worker's current state.
func (w *Worker) String() string {
	return "Worker(" + w.State().String() + ")"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Wrap returns a new worker wrapping the given function. The work function f (which must be non-nil) will be called for every task to be processed by the worker. If the work function returns an error then the worker will be placed into an error state and will stop. The given context will be passed to f.
func Wrap(ctx context.Context, f WorkFunc) *Worker {
	// Sanity check
	if f == nil {
		panic("Illegal nil work function")
	} else if ctx == nil {
		ctx = context.Background()
	}
	// Create the communication channels (the queue must not be buffered)
	queueC, doneC := make(chan interface{}), make(chan struct{})
	// Create the worker
	w := &Worker{
		queueC: queueC,
		doneC:  doneC,
	}
	// Set the worker running and return
	go w.run(ctx, f, queueC, doneC)
	return w
}
