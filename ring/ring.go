// Ring defines the ring interface and common functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package ring

import (
	"bitbucket.org/pcas/math/abeliangroup"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
)

// IsUniter is an optional interface a ring may support, providing an IsUnit method.
type IsUniter interface {
	IsUnit(x object.Element) (bool, object.Element, error) // IsUnit returns true iff x is a multiplicative unit in the ring. If true, also returns the inverse element y such that x * y = 1.
}

// Interface defines the ring interface that all rings must satisfy.
type Interface interface {
	abeliangroup.Interface
	One() object.Element                                                 // One returns the multiplicative identity of the ring.
	IsOne(x object.Element) (bool, error)                                // IsOne returns true iff x is the multiplicative identity element of the ring.
	Multiply(x object.Element, y object.Element) (object.Element, error) // Multiply returns the product x * y.
	Power(x object.Element, k *integer.Element) (object.Element, error)  // Power returns x^k.
}

// producter defines an optional interface that a ring can satisfy, providing a Product method.
type producter interface {
	Product(S ...object.Element) (object.Element, error) // Product returns the product of the elements in the slice S. The product of the empty slice is one.
}

// dotproducter is an optional interface a ring may support, providing a DotProduct method.
type dotproducter interface {
	DotProduct(S1 []object.Element, S2 []object.Element) (object.Element, error) // DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.
}

// Encapsulates a (object.Element, error) pair.
type resultOrError struct {
	Value object.Element // The value
	Err   error          // The error (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// productSliceInternal passes the product to the given channel.
func productSliceInternal(R Interface, S []object.Element, c chan<- *resultOrError) {
	x, err := productSlice(R, S)
	c <- &resultOrError{Value: x, Err: err}
}

// productSlice returns the product of the given slice.
func productSlice(R Interface, S []object.Element) (object.Element, error) {
	if len(S) <= 10 {
		x := S[0]
		for _, y := range S[1:] {
			var err error
			if x, err = R.Multiply(x, y); err != nil {
				return nil, err
			}
		}
		return x, nil
	}
	// Perform a recursive split
	cLeft := make(chan *resultOrError)
	cRight := make(chan *resultOrError)
	div := len(S) / 2
	go productSliceInternal(R, S[:div], cLeft)
	go productSliceInternal(R, S[div:], cRight)
	// Recover the results
	x := <-cLeft
	y := <-cRight
	// Check for errors
	if x.Err != nil {
		return nil, x.Err
	} else if y.Err != nil {
		return nil, y.Err
	}
	// Compute the result
	return R.Multiply(x.Value, y.Value)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element. If R satisfies the interface:
//		type Sumer interface {
//			Sum(S ...object.Element) (object.Element, error) // Sum returns
//			   the sum of the elements in the slice S. The sum of the empty
//			   slice is the zero element.
//		}
// then R's Sum method will be called.
func Sum(R Interface, S ...object.Element) (object.Element, error) {
	return abeliangroup.Sum(R, S...)
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one. If R satisfies the interface:
//		type Producter interface {
//			Product(S ...object.Element) (object.Element, error) // Product
//			   returns the product of the elements in the slice S. The product
//			   of the empty slice is one.
//		}
// then R's Product method will be called.
func Product(R Interface, S ...object.Element) (object.Element, error) {
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return R.One(), nil
	}
	// If the ring has its own Product method we call that
	if RR, ok := R.(producter); ok {
		return RR.Product(S...)
	}
	// Fast-track the length 1 case
	if len(S) == 1 {
		if !R.Contains(S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return S[0], nil
	}
	// Perform the multiplication
	return productSlice(R, S)
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element. If R satisfies the interface:
//		type DotProducter interface {
//			DotProduct(S1 []object.Element, S2 []object.Element)
//				(object.Element, error) // DotProduct returns the dot-product
//					 of the slices S1 and S2. That is, it returns
//					 S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the
//					 minimum of len(S1) and len(S2). The dot-product of two
//					 empty slices is the zero element.
//		}
// then R's DotProduct method will be called.
func DotProduct(R Interface, S1 []object.Element, S2 []object.Element) (object.Element, error) {
	// If the ring has its own DotProduct method we call that
	if RR, ok := R.(dotproducter); ok {
		return RR.DotProduct(S1, S2)
	}
	// Note the minimum length
	size := len(S1)
	if len(S2) < size {
		size = len(S2)
	}
	// Get the easy cases out of the way
	if size == 0 {
		return R.Zero(), nil
	} else if size == 1 {
		return R.Multiply(S1[0], S2[0])
	} else if size == 2 {
		x1, err := R.Multiply(S1[0], S2[0])
		if err != nil {
			return nil, err
		}
		x2, err := R.Multiply(S1[1], S2[1])
		if err != nil {
			return nil, err
		}
		return R.Add(x1, x2)
	}
	// Calculate the products S1[i] * S2[i]
	products := make([]object.Element, 0, size)
	for i := 0; i < size; i++ {
		x, err := R.Multiply(S1[i], S2[i])
		if err != nil {
			return nil, err
		}
		products = append(products, x)
	}
	// Return the sum of the products
	return Sum(R, products...)
}
