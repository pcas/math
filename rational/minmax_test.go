// Tests for minmax.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestMinMax tests MinOfPair, Min, MinAndIndex, Max, MaxOfPair, and MaxAndIndex
func TestMinMax(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	tests := []string{
		"0",
		"1",
		"-1",
		"5",
		"-5",
		"1267650600228229401496703205376",
		"-1267650600228229401496703205376", // this is the minimum value
		"1267650600228229401496703205377",  // this is the maximum value
		"7/3",
		"-7/3",
		"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
		"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
	}
	// make a slice of rationals.
	S := make([]*Element, 0, len(tests))
	for _, t := range tests {
		q, err := FromString(t)
		assert.NoError(err)
		S = append(S, q)
	}
	// the same slice, as object.Elements
	SS := make([]object.Element, 0, len(S))
	for _, x := range S {
		SS = append(SS, x)
	}
	// a long slice
	T := make([]*Element, 0, 100*len(S))
	for i := 0; i < 100; i++ {
		T = append(T, S...)
	}
	// the same slice, as object.Elements
	TT := make([]object.Element, 0, len(SS))
	for _, x := range T {
		TT = append(TT, x)
	}
	// some other rationals
	twoThirds, err := FromString("2/3")
	assert.NoError(err)
	threeQuarters, err := FromString("3/4")
	assert.NoError(err)
	var nilRational *Element
	five := FromInt(5)
	maxVal, err := FromString("1267650600228229401496703205377")
	assert.NoError(err)
	minVal, err := FromString("-1267650600228229401496703205376")
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// perform the tests
	//////////////////////////////////////////////////////////////////////
	qQ := five.Parent().(Parent)
	// test MinOfPair
	assert.True(MinOfPair(twoThirds, twoThirds).IsEqualTo(twoThirds))
	assert.True(MinOfPair(twoThirds, threeQuarters).IsEqualTo(twoThirds))
	assert.True(MinOfPair(threeQuarters, twoThirds).IsEqualTo(twoThirds))
	assert.True(MinOfPair(threeQuarters, threeQuarters).IsEqualTo(threeQuarters))
	assert.True(MinOfPair(twoThirds, nilRational).IsZero())
	assert.True(MinOfPair(nilRational, twoThirds).IsZero())
	assert.True(MinOfPair(nilRational, nilRational).IsZero())
	// test MaxOfPair
	assert.True(MaxOfPair(twoThirds, twoThirds).IsEqualTo(twoThirds))
	assert.True(MaxOfPair(twoThirds, threeQuarters).IsEqualTo(threeQuarters))
	assert.True(MaxOfPair(threeQuarters, twoThirds).IsEqualTo(threeQuarters))
	assert.True(MaxOfPair(threeQuarters, threeQuarters).IsEqualTo(threeQuarters))
	assert.True(MaxOfPair(twoThirds, nilRational).IsEqualTo(twoThirds))
	assert.True(MaxOfPair(nilRational, twoThirds).IsEqualTo(twoThirds))
	assert.True(MaxOfPair(nilRational, nilRational).IsZero())
	//////////////////////////////////////////////////////////////////////
	// test Min
	//////////////////////////////////////////////////////////////////////
	// empty slice
	_, err = Min()
	assert.Error(err)
	_, err = qQ.Min()
	assert.Error(err)
	// length one
	if x, err := Min(S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.Min(S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	// length two
	if x, err := Min(S[3], S[4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
	}
	if x, err := qQ.Min(S[3], S[4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
	}
	if x, err := Min(S[4], S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
	}
	if x, err := qQ.Min(S[4], S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
	}
	// short slice
	if x, err := Min(S...); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
	}
	if x, err := qQ.Min(SS...); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
	}
	// long slice
	if x, err := Min(T...); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
	}
	if x, err := qQ.Min(TT...); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
	}
	//////////////////////////////////////////////////////////////////////
	// test Max
	//////////////////////////////////////////////////////////////////////
	// empty slice
	_, err = Max()
	assert.Error(err)
	_, err = qQ.Max()
	assert.Error(err)
	// length one
	if x, err := Max(S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.Max(S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	// length two
	if x, err := Max(S[3], S[4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.Max(S[3], S[4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := Max(S[4], S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.Max(S[4], S[3]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	// short slice
	if x, err := Max(S...); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
	}
	if x, err := qQ.Max(SS...); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
	}
	// long slice
	if x, err := Max(T...); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
	}
	if x, err := qQ.Max(TT...); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
	}
	//////////////////////////////////////////////////////////////////////
	// test MinAndIndex
	//////////////////////////////////////////////////////////////////////
	// empty slice
	emptySlice := make([]*Element, 0)
	_, _, err = MinAndIndex(emptySlice)
	assert.Error(err)
	emptyObjectElementSlice := make([]object.Element, 0)
	_, _, err = qQ.MinAndIndex(emptyObjectElementSlice)
	assert.Error(err)
	// length one
	if x, idx, err := MinAndIndex(S[3:4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	if x, idx, err := qQ.MinAndIndex(SS[3:4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	// length two
	if x, idx, err := MinAndIndex(S[3:5]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
		assert.Equal(idx, 1)
	}
	if x, idx, err := qQ.MinAndIndex(SS[3:5]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
		assert.Equal(idx, 1)
	}
	if x, idx, err := MinAndIndex([]*Element{S[4], S[3]}); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
		assert.Equal(idx, 0)
	}
	if x, idx, err := qQ.MinAndIndex([]object.Element{S[4], S[3]}); assert.NoError(err) {
		assert.AreEqual(qQ, x, five.Negate())
		assert.Equal(idx, 0)
	}
	// short slice
	if x, idx, err := MinAndIndex(S); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
		assert.Equal(idx, 6)
	}
	if x, idx, err := qQ.MinAndIndex(SS); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
		assert.Equal(idx, 6)

	}
	// long slice
	if x, idx, err := MinAndIndex(T); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
		assert.Equal(idx, 6)

	}
	if x, idx, err := qQ.MinAndIndex(TT); assert.NoError(err) {
		assert.AreEqual(qQ, x, minVal)
		assert.Equal(idx, 6)

	}
	//////////////////////////////////////////////////////////////////////
	// test MaxAndIndex
	//////////////////////////////////////////////////////////////////////
	// empty slice
	_, _, err = MaxAndIndex(emptySlice)
	assert.Error(err)
	_, _, err = qQ.MaxAndIndex(emptyObjectElementSlice)
	assert.Error(err)
	// length one
	if x, idx, err := MaxAndIndex(S[3:4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	if x, idx, err := qQ.MaxAndIndex(SS[3:4]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	// length two
	if x, idx, err := MaxAndIndex(S[3:5]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	if x, idx, err := qQ.MaxAndIndex(SS[3:5]); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 0)
	}
	if x, idx, err := MaxAndIndex([]*Element{S[4], S[3]}); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 1)
	}
	if x, idx, err := qQ.MaxAndIndex([]object.Element{S[4], S[3]}); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
		assert.Equal(idx, 1)
	}
	// short slice
	if x, idx, err := MaxAndIndex(S); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
		assert.Equal(idx, 7)
	}
	if x, idx, err := qQ.MaxAndIndex(SS); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
		assert.Equal(idx, 7)

	}
	// long slice
	if x, idx, err := MaxAndIndex(T); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
		assert.Equal(idx, 7)

	}
	if x, idx, err := qQ.MaxAndIndex(TT); assert.NoError(err) {
		assert.AreEqual(qQ, x, maxVal)
		assert.Equal(idx, 7)

	}
	//////////////////////////////////////////////////////////////////////
	// error cases
	//////////////////////////////////////////////////////////////////////
	SS[1] = nil
	_, _, err = qQ.MinAndIndex(SS)
	assert.Error(err)
	_, _, err = qQ.MaxAndIndex(SS)
	assert.Error(err)
	_, _, err = qQ.MinAndIndex(SS[:2])
	assert.Error(err)
	_, _, err = qQ.MaxAndIndex(SS[:2])
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// extra tests to catch some code paths missed above
	//////////////////////////////////////////////////////////////////////
	if min, err := FromString("-1267650600228229401496703205377"); assert.NoError(err) {
		T[len(T)-1] = min
		_, idx, err2 := MinAndIndex(T)
		assert.NoError(err2)
		assert.Equal(idx, len(T)-1)
	}
	if max, err := FromString("1267650600228229401496703205378"); assert.NoError(err) {
		T[len(T)-1] = max
		_, idx, err2 := MaxAndIndex(T)
		assert.NoError(err2)
		assert.Equal(idx, len(T)-1)
	}

}
