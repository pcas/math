// Other tests involving rational Elements

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/integer"
	"testing"
)

////////////////////////////////////////////////////////////////////////////////
// tests for Contains() in the integers package
////////////////////////////////////////////////////////////////////////////////

// fromIntPair wraps FromIntPair and panics on error.
func fromIntPair(a int, b int) *Element {
	q, err := FromIntPair(a, b)
	if err != nil {
		panic(err)
	}
	return q
}

func TestIntegersContains(t *testing.T) {
	integersContainsTests := []struct {
		value    *Element // input
		expected bool     //output
	}{
		{fromIntPair(0, 1), true},   // 0
		{fromIntPair(0, 2), true},   // 0
		{fromIntPair(1, 2), false},  // 1/2
		{fromIntPair(-2, 2), true},  // -1
		{fromIntPair(-2, 3), false}, // -2/3
	}
	zZ := integer.Ring()
	for _, test := range integersContainsTests {
		result := zZ.Contains(test.value)
		if result != test.expected {
			t.Errorf("Contains (in the integers package) with rational input(%s): expected %v, got %v", test.value, test.expected, result)
		}
	}
}
