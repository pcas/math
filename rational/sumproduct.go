// Sumproduct defines the Sum, Product, and similar functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/rational/internal/bigratfactory"
	"bitbucket.org/pcastools/mathutil"
	"math/big"
)

// The maximum number of elements in the slice before we compute the sum, product, or dot-product concurrently.
const maxSumProd = 16
const maxDotProd = maxSumProd / 2

/////////////////////////////////////////////////////////////////////////
// Sum functions
/////////////////////////////////////////////////////////////////////////

// sumAsBigRatInternal passes the sum as a *big.Rat to the given channel.
func sumAsBigRatInternal(c chan<- *big.Rat, S []*Element) {
	c <- sumAsBigRat(S)
}

// sumAsBigRat returns the sum as a *big.Rat.
func sumAsBigRat(S []*Element) *big.Rat {
	if len(S) <= maxSumProd {
		// Calculate the sum
		n := bigratfactory.New().SetInt64(0)
		y := bigratfactory.New()
		for _, x := range S {
			if !x.IsZero() {
				if x.IsInt64() {
					n.Add(n, y.SetInt64(x.int64()))
				} else {
					n.Add(n, x.bigRat())
				}
			}
		}
		bigratfactory.Reuse(y)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Rat)
	div := len(S) / 2
	go sumAsBigRatInternal(c, S[:div])
	go sumAsBigRatInternal(c, S[div:])
	n, m := <-c, <-c
	n.Add(n, m)
	bigratfactory.Reuse(m)
	return n
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
func Sum(S ...*Element) *Element {
	if k := len(S); k == 0 {
		return Zero()
	} else if k == 1 {
		return S[0]
	} else if k == 2 {
		return Add(S[0], S[1])
	}
	return fromBigRatAndReuse(sumAsBigRat(S))
}

/////////////////////////////////////////////////////////////////////////
// Product functions
/////////////////////////////////////////////////////////////////////////

// productAsBigRatInternal passes the product as a *big.Rat to the given channel.
func productAsBigRatInternal(c chan<- *big.Rat, S []*Element) {
	c <- productAsBigRat(S)
}

// productAsBigRat returns the product as a *big.Rat.
func productAsBigRat(S []*Element) *big.Rat {
	if len(S) <= maxSumProd {
		// Calculate the product
		n := bigratfactory.New().SetInt64(1)
		y := bigratfactory.New()
		for _, x := range S {
			if x.IsZero() {
				n.SetInt64(0)
				return n
			} else if !x.IsOne() {
				if x.IsInt64() {
					n.Mul(n, y.SetInt64(x.int64()))
				} else {
					n.Mul(n, x.bigRat())
				}
			}
		}
		bigratfactory.Reuse(y)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Rat)
	div := len(S) / 2
	go productAsBigRatInternal(c, S[:div])
	go productAsBigRatInternal(c, S[div:])
	n, m := <-c, <-c
	n.Mul(n, m)
	bigratfactory.Reuse(m)
	return n
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one.
func Product(S ...*Element) *Element {
	if k := len(S); k == 0 {
		return One()
	} else if k == 1 {
		return S[0]
	} else if k == 2 {
		return Multiply(S[0], S[1])
	}
	return fromBigRatAndReuse(productAsBigRat(S))
}

/////////////////////////////////////////////////////////////////////////
// DotProduct functions
/////////////////////////////////////////////////////////////////////////

// dotProductAsBigRatInternal passes the dot-product of the slices S1 and S2 as a *big.Rat to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductAsBigRatInternal(c chan<- *big.Rat, S1 []*Element, S2 []*Element) {
	c <- dotProductAsBigRat(S1, S2)
}

// dotProductAsBigRat returns the dot-product of the slices S1 and S2 as a *big.Rat. The slices are assumed to be non-empty and of the same length.
func dotProductAsBigRat(S1 []*Element, S2 []*Element) *big.Rat {
	if len(S1) <= maxDotProd {
		// Calculate the dot-product
		n := bigratfactory.New().SetInt64(0)
		m := bigratfactory.New()
		for i, x := range S1 {
			y := S2[i]
			if !x.IsZero() && !y.IsZero() {
				if x.IsOne() {
					if y.IsInt64() {
						n.Add(n, m.SetInt64(y.int64()))
					} else {
						n.Add(n, y.bigRat())
					}
				} else if y.IsOne() {
					if x.IsInt64() {
						n.Add(n, m.SetInt64(x.int64()))
					} else {
						n.Add(n, x.bigRat())
					}
				} else if x.IsInt64() {
					if y.IsInt64() {
						if k, ok := mathutil.MultiplyInt64(x.int64(), y.int64()); ok {
							n.Add(n, m.SetInt64(k))
						} else {
							n.Add(n, m.Mul(m.SetInt64(x.int64()), y.bigRat()))
						}
					} else {
						n.Add(n, m.Mul(m.SetInt64(x.int64()), y.bigRat()))
					}
				} else if y.IsInt64() {
					n.Add(n, m.Mul(x.bigRat(), m.SetInt64(y.int64())))
				} else {
					n.Add(n, m.Mul(x.bigRat(), y.bigRat()))
				}
			}
		}
		bigratfactory.Reuse(m)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Rat)
	div := len(S1) / 2
	go dotProductAsBigRatInternal(c, S1[:div], S2[:div])
	go dotProductAsBigRatInternal(c, S1[div:], S2[div:])
	n, m := <-c, <-c
	n.Add(n, m)
	bigratfactory.Reuse(m)
	return n
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.
func DotProduct(S1 []*Element, S2 []*Element) *Element {
	// Resize the slices to that they're both of the same length
	k := len(S1)
	if k2 := len(S2); k != k2 {
		if k < k2 {
			S2 = S2[:k]
		} else {
			k = k2
			S1 = S1[:k]
		}
	}
	// Fast-track the easy cases
	if k <= 2 {
		if k == 0 {
			return Zero()
		} else if k == 1 {
			return Multiply(S1[0], S2[0])
		}
		return MultiplyMultiplyThenAdd(S1[0], S2[0], S1[1], S2[1])
	}
	// Perform the dot-product
	return fromBigRatAndReuse(dotProductAsBigRat(S1, S2))
}
