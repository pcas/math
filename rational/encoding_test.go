// Tests for encoding.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/testing/assert"
	"bytes"
	"encoding/gob"
	"testing"
)

// TestEncoding tests GobEncode, GobDecode, and GobDecodeSlice
func TestEncoding(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	tests := []string{
		"0",
		"1",
		"-1",
		"5",
		"-5",
		"1267650600228229401496703205376",
		"-1267650600228229401496703205376",
		"1267650600228229401496703205377",
		"7/3",
		"-7/3",
		"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
		"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
	}
	rationals := make([]*Element, 0, len(tests))
	for _, t := range tests {
		q, err := FromString(t)
		assert.NoError(err)
		rationals = append(rationals, q)
	}
	//////////////////////////////////////////////////////////////////////
	// test GobEncode and GobDecode
	//////////////////////////////////////////////////////////////////////
	// set up the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	for _, q := range rationals {
		// first encode and then decode
		if assert.NoError(enc.Encode(q)) {
			if qDecoded, err := GobDecode(dec); assert.NoError(err) {
				assert.True(q.IsEqualTo(qDecoded))
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test encoding and decoding of slices
	//////////////////////////////////////////////////////////////////////
	// Encode the slice
	if assert.NoError(enc.Encode(rationals)) {
		// Decode the slice of vectors
		if S, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Len(S, len(rationals)) {
			// Check for equality
			for i, s := range rationals {
				assert.True(s.IsEqualTo(S[i]))
			}
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var nilElement *Element
	if b, err := nilElement.GobEncode(); assert.NoError(err) {
		// Decoding should give zero
		q := &Element{}
		if assert.NoError(q.GobDecode(b)) {
			assert.True(q.IsZero())
			assert.NotNil(q)
		}
	}
}

// TestGobDecodeEmpty tests gob decoding an empty slice of bytes
func TestGobDecodeEmpty(t *testing.T) {
	assert := assert.New(t)
	// Decoding the empty slice of bytes should give an error
	q := &Element{}
	assert.Error(q.GobDecode(nil))
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	assert := assert.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	q := One()
	if assert.NoError(enc.Encode(q)) {
		// Try to decode directly into a nil object -- this should error
		var m *Element
		assert.Error(m.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeExisting tests gob decoding into an existing *Element.
func TestGobDecodeExisting(t *testing.T) {
	assert := assert.New(t)
	q1 := One()
	q2 := q1.Increment()
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode, and then try to decode
	if assert.NoError(enc.Encode(q1)) {
		// Try to decode directly into q2 -- this should error
		assert.Error(q2.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	assert := assert.New(t)
	// Create the byte slice: the first byte is the version number, which we
	// set to "future" version.
	var b bytes.Buffer
	var illegalEncodingVersion byte = encodingVersion + 1
	enc := gob.NewEncoder(&b)
	err := enc.Encode(illegalEncodingVersion)
	assert.NoError(err)
	// Try to decode as a *Element -- this should error
	q := &Element{}
	assert.Error(q.GobDecode(b.Bytes()))
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	assert := assert.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
	// Try to decode as a []*Element -- this should error
	_, err = GobDecodeSlice(gob.NewDecoder(bytes.NewReader(b)))
	assert.Error(err)
}
