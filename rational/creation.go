// Creation handles creation of new *Elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/rational/internal/bigratfactory"
	"bitbucket.org/pcastools/fatal"
	"math"
	"math/big"
	"strings"
	"sync"
)

// _elementFactory is a the factory used to efficiently generate new *Elements. Do NOT access the factory directly -- use the function newElement.
var _elementFactory = struct {
	m     sync.Mutex // The mutex controlling access
	idx   int        // The current index in the cache
	cache []Element  // The underlying factory cache
}{}

// _elementFactorySize is the size of the factory cache.
const _elementFactorySize = 1000

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// rawElement returns a new *Element.
func rawElement() *Element {
	_elementFactory.m.Lock()
	idx := _elementFactory.idx
	if _elementFactory.cache == nil {
		_elementFactory.cache = make([]Element, _elementFactorySize)
		idx = 0
	}
	e := &_elementFactory.cache[idx]
	idx++
	if idx == _elementFactorySize {
		_elementFactory.cache = nil
	} else {
		_elementFactory.idx = idx
	}
	_elementFactory.m.Unlock()
	return e
}

// fromBigRatAndReuse returns a *Element with value q. Warning: The argument q will be made available for reuse, hence you must NOT modify q after calling this function.
func fromBigRatAndReuse(q *big.Rat) *Element {
	if q == nil {
		return Zero()
	} else if q.IsInt() {
		if n := q.Num(); n.IsInt64() {
			m := FromInt64(n.Int64())
			bigratfactory.Reuse(q)
			return m
		}
	}
	m := rawElement()
	m.q = *q
	return m
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Zero returns 0.
func Zero() *Element {
	return &_smallIntegerCache[-smallIntegerCacheMin]
}

// One returns 1.
func One() *Element {
	return &_smallIntegerCache[1-smallIntegerCacheMin]
}

// FromInt returns n as a rational.
func FromInt(n int) *Element {
	return FromInt64(int64(n))
}

// FromInt32 returns n as a rational.
func FromInt32(n int32) *Element {
	return FromInt64(int64(n))
}

// FromInt64 returns a *Element with value n.
func FromInt64(n int64) *Element {
	if n >= smallIntegerCacheMin && n <= smallIntegerCacheMax {
		return &_smallIntegerCache[int(n)-smallIntegerCacheMin]
	}
	m := rawElement()
	m.k = n
	return m
}

// FromUint64 returns a *Element with value n.
func FromUint64(n uint64) *Element {
	if n <= math.MaxInt64 {
		return FromInt64(int64(n))
	}
	m := rawElement()
	m.q.SetFrac(big.NewInt(0).SetUint64(n), oneAsBigInt)
	return m
}

// FromBigInt returns n as a rational.
func FromBigInt(n *big.Int) *Element {
	if n == nil {
		return Zero()
	} else if n.IsInt64() {
		return FromInt64(n.Int64())
	}
	m := rawElement()
	m.q.SetFrac(n, oneAsBigInt)
	return m
}

// FromInteger returns n as a rational.
func FromInteger(n *integer.Element) *Element {
	if n.IsInt64() {
		k, err := n.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return FromInt64(k)
	}
	m := rawElement()
	m.q.SetFrac(n.BigInt(), oneAsBigInt)
	return m
}

// FromBigRat returns a *Element with value q.
func FromBigRat(q *big.Rat) *Element {
	if q == nil {
		return Zero()
	} else if q.IsInt() {
		if n := q.Num(); n.IsInt64() {
			return FromInt64(n.Int64())
		}
	}
	m := rawElement()
	m.q.Set(q)
	return m
}

// FromIntPair returns a/b as a rational. It will return an error if b is zero.
func FromIntPair(a int, b int) (*Element, error) {
	return FromInt64Pair(int64(a), int64(b))
}

// FromInt32Pair returns a/b as a rational. It will return an error if b is zero.
func FromInt32Pair(a int32, b int32) (*Element, error) {
	return FromInt64Pair(int64(a), int64(b))
}

// FromInt64Pair returns a/b as a rational. It will return an error if b is zero.
func FromInt64Pair(a int64, b int64) (*Element, error) {
	if b == 0 {
		return nil, errors.DivisionByZero.New()
	} else if a == 0 {
		return Zero(), nil
	} else if a == b {
		return One(), nil
	} else if b == 1 {
		return FromInt64(a), nil
	} else if b == -1 && a != math.MinInt64 {
		return FromInt64(-a), nil
	}
	return fromBigRatAndReuse(bigratfactory.New().SetFrac64(a, b)), nil
}

// FromUint64Pair returns a/b as a rational. It will return an error if b is zero.
func FromUint64Pair(a uint64, b uint64) (*Element, error) {
	if b == 0 {
		return nil, errors.DivisionByZero.New()
	} else if a == 0 {
		return Zero(), nil
	} else if a == b {
		return One(), nil
	} else if b == 1 {
		return FromUint64(a), nil
	}
	return fromBigRatAndReuse(bigratfactory.New().SetFrac(big.NewInt(0).SetUint64(a), big.NewInt(0).SetUint64(b))), nil
}

// FromBigIntPair returns a/b as a rational. It will return an error if b is zero.
func FromBigIntPair(a *big.Int, b *big.Int) (*Element, error) {
	if b.BitLen() == 0 {
		return nil, errors.DivisionByZero.New()
	} else if a.BitLen() == 0 {
		return Zero(), nil
	}
	return fromBigRatAndReuse(bigratfactory.New().SetFrac(a, b)), nil
}

// FromIntegerPair returns a/b as a rational. It will return an error if b is zero.
func FromIntegerPair(a *integer.Element, b *integer.Element) (*Element, error) {
	if b.IsZero() {
		return nil, errors.DivisionByZero.New()
	} else if a.IsZero() {
		return Zero(), nil
	} else if integer.AreEqual(a, b) {
		return One(), nil
	} else if b.IsOne() {
		return FromInteger(a), nil
	}
	return fromBigRatAndReuse(bigratfactory.New().SetFrac(a.BigInt(), b.BigInt())), nil
}

// FromString returns the rational represented by the string s.
func FromString(s string) (*Element, error) {
	// the *big.Rat SetString method has some peculiarities (e.g. "4/3/2" is legal input, and returns 4/3, whereas 4/-3 is not legal input) so we parse the numerator and denominator separately.
	idx := strings.IndexByte(s, '/')
	if idx == -1 {
		// there is no "/", so parse the whole thing as an integer
		if n, err := integer.FromString(s); err != nil {
			return nil, errors.StringNotARational.New()
		} else {
			return FromInteger(n), nil
		}
	} else if idx == len(s)-1 {
		// the denominator is malformed
		return nil, errors.StringNotARational.New()
	}
	// parse the numerator and denominator as integers
	num, err1 := integer.FromString(s[:idx])
	den, err2 := integer.FromString(s[idx+1:])
	if err1 != nil || err2 != nil {
		return nil, errors.StringNotARational.New()
	}
	return FromIntegerPair(num, den)
}
