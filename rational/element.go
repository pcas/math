// Element represents a rational number.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

/*
Implementation notes:

The sumAsBigRat and productAsBigRat functions take the sum and product of slices of *Elements, doing recursive splits until the slices are of length <=16 and then taking the product of these short sequences. The number 16 was chosen by testing on sequences of random rationals (with denominator bounded by N, for various values of N). This value should be changed if you modify the code for multiplying or adding elements.
*/

/////////////////////////////////////////////////////////////////////////
// element.go
/////////////////////////////////////////////////////////////////////////

package rational

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/rational/internal/bigratfactory"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"math"
	"math/big"
)

// Element is an element of the field of rational numbers.
type Element struct {
	k int64   // The underlying rational as an int64 (if q below is zero)
	q big.Rat // The underlying rational as a big.Rat (if non-zero)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialised our global variables.
func init() {
	// Create the cache of small-valued integers
	initSmallIntegerCache()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns x + y.
func Add(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() {
		return y
	} else if y.IsZero() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Rat
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.AddInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigratfactory.New().SetInt64(x.int64())
		return fromBigRatAndReuse(xx.Add(xx, y.bigRat()))
	} else if y.IsInt64() {
		yy := bigratfactory.New().SetInt64(y.int64())
		return fromBigRatAndReuse(yy.Add(x.bigRat(), yy))
	}
	return fromBigRatAndReuse(bigratfactory.New().Add(x.bigRat(), y.bigRat()))
}

// AddInteger returns x + y.
func AddInteger(x *integer.Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() {
		return y
	} else if y.IsZero() {
		return FromInteger(x)
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Rat
	// computation.
	if x.IsInt64() {
		k, err := x.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		if y.IsInt64() {
			if val, ok := mathutil.AddInt64(k, y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigratfactory.New().SetInt64(k)
		return fromBigRatAndReuse(xx.Add(xx, y.bigRat()))
	} else if y.IsInt64() {
		xx := bigratfactory.New().SetInt(x.BigInt())
		yy := bigratfactory.New().SetInt64(y.int64())
		xx.Add(xx, yy)
		bigratfactory.Reuse(yy)
		return fromBigRatAndReuse(xx)
	}
	xx := bigratfactory.New().SetInt(x.BigInt())
	return fromBigRatAndReuse(xx.Add(xx, y.bigRat()))
}

// AddInt64 returns x + y.
func AddInt64(x int64, y *Element) *Element {
	// Fast-track the easy cases
	if x == 0 {
		return y
	} else if y.IsZero() {
		return FromInt64(x)
	}
	// Can we do this as an int64 computation?
	if y.IsInt64() {
		if val, ok := mathutil.AddInt64(x, y.int64()); ok {
			return FromInt64(val)
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt64(x)
	return fromBigRatAndReuse(r.Add(r, y.bigRat()))
}

// Subtract returns x - y.
func Subtract(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() {
		return Negate(y)
	} else if y.IsZero() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Rat
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.SubtractInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigratfactory.New().SetInt64(x.int64())
		return fromBigRatAndReuse(xx.Sub(xx, y.bigRat()))
	} else if y.IsInt64() {
		yy := bigratfactory.New().SetInt64(y.int64())
		return fromBigRatAndReuse(yy.Sub(x.bigRat(), yy))
	}
	return fromBigRatAndReuse(bigratfactory.New().Sub(x.bigRat(), y.bigRat()))
}

// Multiply returns the product x * y.
func Multiply(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() || y.IsZero() {
		return Zero()
	} else if x.IsOne() {
		return y
	} else if y.IsOne() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Rat
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.MultiplyInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigratfactory.New().SetInt64(x.int64())
		return fromBigRatAndReuse(xx.Mul(xx, y.bigRat()))
	} else if y.IsInt64() {
		yy := bigratfactory.New().SetInt64(y.int64())
		return fromBigRatAndReuse(yy.Mul(x.bigRat(), yy))
	}
	return fromBigRatAndReuse(bigratfactory.New().Mul(x.bigRat(), y.bigRat()))
}

// MultiplyByIntegerThenAdd returns the value a * b + c.
func MultiplyByIntegerThenAdd(a *integer.Element, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return c
	} else if c.IsZero() {
		return b.ScalarMultiplyByInteger(a)
	} else if a.IsOne() {
		return Add(b, c)
	} else if b.IsOne() {
		return AddInteger(a, c)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() {
		aa, err := a.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		if b.IsInt64() && c.IsInt64() {
			if val, ok := mathutil.MultiplyInt64(aa, b.int64()); ok {
				if val, ok = mathutil.AddInt64(val, c.int64()); ok {
					return FromInt64(val)
				}
			}
		}
		r := bigratfactory.New().SetInt64(aa)
		return fromBigRatAndReuse(r.Add(r.Mul(r, b.bigRat()), c.bigRat()))
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt(a.BigInt())
	return fromBigRatAndReuse(r.Add(r.Mul(r, b.bigRat()), c.bigRat()))
}

// MultiplyByInt64ThenAdd returns the value a * b + c.
func MultiplyByInt64ThenAdd(a int64, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a == 0 || b.IsZero() {
		return c
	} else if c.IsZero() {
		return b.ScalarMultiplyByInt64(a)
	} else if a == 1 {
		return Add(b, c)
	} else if b.IsOne() {
		return AddInt64(a, c)
	}
	// Can we do this as an int64 computation?
	if b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a, b.int64()); ok {
			if val, ok = mathutil.AddInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt64(a)
	return fromBigRatAndReuse(r.Add(r.Mul(r, b.bigRat()), c.bigRat()))
}

// MultiplyThenAdd returns the value a * b + c.
func MultiplyThenAdd(a *Element, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return c
	} else if c.IsZero() {
		return Multiply(a, b)
	} else if a.IsOne() {
		return Add(b, c)
	} else if b.IsOne() {
		return Add(a, c)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val, ok = mathutil.AddInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().Mul(a.bigRat(), b.bigRat())
	return fromBigRatAndReuse(r.Add(r, c.bigRat()))
}

// MultiplyThenSubtract returns the value a * b - c.
func MultiplyThenSubtract(a *Element, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return Negate(c)
	} else if c.IsZero() {
		return Multiply(a, b)
	} else if a.IsOne() {
		return Subtract(b, c)
	} else if b.IsOne() {
		return Subtract(a, c)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val, ok = mathutil.SubtractInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().Mul(a.bigRat(), b.bigRat())
	return fromBigRatAndReuse(r.Sub(r, c.bigRat()))
}

// MultiplyMultiplyThenAdd returns the value a * b + c * d.
func MultiplyMultiplyThenAdd(a *Element, b *Element, c *Element, d *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return Multiply(c, d)
	} else if c.IsZero() || d.IsZero() {
		return Multiply(a, b)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() && d.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val2, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok {
				if val, ok = mathutil.AddInt64(val, val2); ok {
					return FromInt64(val)
				}
			}
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().Mul(a.bigRat(), b.bigRat())
	s := bigratfactory.New().Mul(c.bigRat(), d.bigRat())
	z := fromBigRatAndReuse(r.Add(r, s))
	bigratfactory.Reuse(s)
	return z
}

// MultiplyMultiplyThenSubtract returns the value a * b - c * d.
func MultiplyMultiplyThenSubtract(a *Element, b *Element, c *Element, d *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		if c.IsZero() || d.IsZero() {
			return Zero()
		}
		// Can we do this as an int64 computation?
		if c.IsInt64() && d.IsInt64() {
			if val, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok && val != math.MinInt64 {
				return FromInt64(-val)
			}
		}
		// No luck -- do this as a big.Rat computation
		r := bigratfactory.New().Mul(c.bigRat(), d.bigRat())
		return fromBigRatAndReuse(r.Neg(r))
	} else if c.IsZero() || d.IsZero() {
		return Multiply(a, b)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() && d.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val2, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok {
				if val, ok = mathutil.SubtractInt64(val, val2); ok {
					return FromInt64(val)
				}
			}
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().Mul(a.bigRat(), b.bigRat())
	s := bigratfactory.New().Mul(c.bigRat(), d.bigRat())
	z := fromBigRatAndReuse(r.Sub(r, s))
	bigratfactory.Reuse(s)
	return z
}

// Negate returns -q.
func Negate(q *Element) *Element {
	if q.IsInt64() {
		if k := q.int64(); k != math.MinInt64 {
			return FromInt64(-k)
		}
	}
	return fromBigRatAndReuse(bigratfactory.New().Neg(q.bigRat()))
}

// PowerInt64 returns q^k.
func PowerInt64(q *Element, k int64) (*Element, error) {
	// Fast-track the easy cases
	if k == 0 {
		return One(), nil
	} else if k == 1 || q.IsOne() {
		return q, nil
	} else if k == -1 {
		return Inverse(q)
	} else if k < 0 && q.IsZero() {
		return nil, errors.DivisionByZero.New()
	} else if q.IsZero() {
		return Zero(), nil
	}
	// Can we do this as an int64 calculation?
	if q.IsInt64() {
		n := q.int64()
		if k > 0 {
			if c, ok := mathutil.PowerInt64(n, k); ok {
				return FromInt64(c), nil
			}
			// We can do powers of two via bit shifting
			if n == 2 && int64(uint(k)) == k {
				m := big.NewInt(1)
				return FromBigInt(m.Lsh(m, uint(k))), nil
			}
		} else if k != math.MinInt64 {
			if c, ok := mathutil.PowerInt64(n, -k); ok {
				return FromInt64Pair(1, c)
			}
			// We can do powers of two via bit shifting
			if n == 2 && int64(uint(-k)) == -k {
				m := big.NewInt(1)
				return FromBigIntPair(oneAsBigInt, m.Lsh(m, uint(-k)))
			}
		}
	}
	// Recover q as a *big.Rat, and k as a *big.Int
	qq, kk := q.bigRat(), big.NewInt(k)
	// Note the numerator and denominator
	num := big.NewInt(0).Set(qq.Num())
	den := big.NewInt(0).Set(qq.Denom())
	// If k < 0 swap the numerator and denominator
	if k < 0 {
		kk.Abs(kk)
		num, den = den, num
	}
	// Perform the calculation
	num.Exp(num, kk, nil)
	den.Exp(den, kk, nil)
	return fromBigRatAndReuse(bigratfactory.New().SetFrac(num, den)), nil
}

// Power returns q^k.
func Power(q *Element, k *integer.Element) (*Element, error) {
	// Fast-track the easy cases
	if k.IsInt64() {
		kk, err := k.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return PowerInt64(q, kk)
	} else if q.IsOne() {
		return q, nil
	} else if q.IsEqualToInt64(-1) {
		if k.IsEven() {
			return One(), nil
		}
		return q, nil
	} else if k.IsNegative() && q.IsZero() {
		return nil, errors.DivisionByZero.New()
	} else if q.IsZero() {
		return Zero(), nil
	}
	//////////////////////////////////////////////////////////////////////
	// in practice if this code ever gets called it is likely to
	// quickly exhaust all the available memory.  In particular, the
	// remaining code in this function is not tested in element_test.go
	// (It looks OK, though)
	//////////////////////////////////////////////////////////////////////
	// Recover q as a *big.Rat, and k as a *big.Int
	qq, kk := q.bigRat(), k.BigInt()
	// Note the numerator and denominator
	num := big.NewInt(0).Set(qq.Num())
	den := big.NewInt(0).Set(qq.Denom())
	// If k < 0 swap the numerator and denominator
	if kk.Sign() < 0 {
		kk.Abs(kk)
		num, den = den, num
	}
	// Perform the calculation
	num.Exp(num, kk, nil)
	den.Exp(den, kk, nil)
	return fromBigRatAndReuse(bigratfactory.New().SetFrac(num, den)), nil
}

// Inverse returns 1/q. It will return an error if q is zero.
func Inverse(q *Element) (*Element, error) {
	if q.IsInt64() {
		return FromInt64Pair(1, q.int64())
	}
	return fromBigRatAndReuse(bigratfactory.New().Inv(q.bigRat())), nil
}

// Divide returns the quotient x / y. It will return an error if y is zero.
func Divide(x *Element, y *Element) (*Element, error) {
	// Can we do this as an int64 computation?
	if y.IsInt64() {
		yy := y.int64()
		if yy == 0 {
			return nil, errors.DivisionByZero.New()
		} else if x.IsInt64() {
			return FromInt64Pair(x.int64(), yy)
		}
		s := bigratfactory.New().SetInt64(yy)
		return fromBigRatAndReuse(s.Quo(x.bigRat(), s)), nil
	} else if x.IsInt64() {
		s := bigratfactory.New().SetInt64(x.int64())
		return fromBigRatAndReuse(s.Quo(s, y.bigRat())), nil
	}
	// No luck -- do this as a big.Rat computation
	s := bigratfactory.New().Quo(x.bigRat(), y.bigRat())
	return fromBigRatAndReuse(s), nil
}

// AreEqual returns true iff x equals y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func Cmp(x *Element, y *Element) int {
	// Fast-track the easy cases
	if x == y {
		return 0
	} else if x.IsZero() {
		return -y.Sign()
	} else if y.IsZero() {
		return x.Sign()
	}
	// Can we do this as an int64 computation?
	if x.IsInt64() {
		if y.IsInt64() {
			if xx, yy := x.int64(), y.int64(); xx < yy {
				return -1
			} else if xx == yy {
				return 0
			}
			return 1
		}
		s := bigratfactory.New().SetInt64(x.int64())
		sgn := s.Cmp(y.bigRat())
		bigratfactory.Reuse(s)
		return sgn
	} else if y.IsInt64() {
		s := bigratfactory.New().SetInt64(y.int64())
		sgn := x.bigRat().Cmp(s)
		bigratfactory.Reuse(s)
		return sgn
	}
	// No luck -- do this as a big.Rat computation
	return x.bigRat().Cmp(y.bigRat())
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff q is zero.
func (q *Element) IsZero() bool {
	return q.IsInt64() && q.int64() == 0
}

// IsOne returns true iff q is one.
func (q *Element) IsOne() bool {
	return q.IsInt64() && q.int64() == 1
}

// IsNegative returns true iff q < 0.
func (q *Element) IsNegative() bool {
	if q.IsInt64() {
		return q.int64() < 0
	}
	return q.bigRat().Sign() < 0
}

// IsPositive returns true iff q > 0.
func (q *Element) IsPositive() bool {
	if q.IsInt64() {
		return q.int64() > 0
	}
	return q.bigRat().Sign() > 0
}

// IsEqualTo returns true iff q = r.
func (q *Element) IsEqualTo(r *Element) bool {
	if q == r {
		return true
	} else if q.IsInt64() {
		return r.IsInt64() && q.int64() == r.int64()
	} else if r.IsInt64() {
		return false
	}
	return q.bigRat().Cmp(r.bigRat()) == 0
}

// IsEqualToInteger returns true iff q = r.
func (q *Element) IsEqualToInteger(r *integer.Element) bool {
	if q.IsInt64() {
		if r.IsInt64() {
			k, err := r.Int64()
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			return q.int64() == k
		}
	} else if !r.IsInt64() && q.IsIntegral() {
		return q.bigRat().Num().Cmp(r.BigInt()) == 0
	}
	return false
}

// IsEqualToInt64 returns true iff q = r.
func (q *Element) IsEqualToInt64(r int64) bool {
	return q.IsInt64() && q.int64() == r
}

// IsEqualToUint64 returns true iff q = r.
func (q *Element) IsEqualToUint64(r uint64) bool {
	if r <= math.MaxInt64 {
		return q.IsInt64() && q.int64() == int64(r)
	} else if !q.IsInt64() && q.IsIntegral() {
		if k := q.bigRat().Num(); k.IsUint64() {
			return k.Uint64() == r
		}
	}
	return false
}

// IsGreaterThan returns true iff q > r.
func (q *Element) IsGreaterThan(r *Element) bool {
	if q == r {
		return false
	} else if q.IsZero() {
		return r.IsNegative()
	} else if r.IsZero() {
		return q.IsPositive()
	} else if q.IsInt64() {
		if r.IsInt64() {
			return q.int64() > r.int64()
		}
		s := bigratfactory.New().SetInt64(q.int64())
		sgn := s.Cmp(r.bigRat())
		bigratfactory.Reuse(s)
		return sgn > 0
	} else if r.IsInt64() {
		s := bigratfactory.New().SetInt64(r.int64())
		sgn := q.bigRat().Cmp(s)
		bigratfactory.Reuse(s)
		return sgn > 0
	}
	return q.bigRat().Cmp(r.bigRat()) > 0
}

// IsGreaterThanInteger returns true iff q > r.
func (q *Element) IsGreaterThanInteger(r *integer.Element) bool {
	if q.IsZero() {
		return r.IsNegative()
	} else if r.IsZero() {
		return q.IsPositive()
	} else if q.IsInt64() {
		return r.IsLessThanInt64(q.int64())
	} else if r.IsInt64() {
		k, err := r.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		s := bigratfactory.New().SetInt64(k)
		sgn := q.bigRat().Cmp(s)
		bigratfactory.Reuse(s)
		return sgn > 0
	}
	s := bigratfactory.New().SetInt(r.BigInt())
	sgn := q.bigRat().Cmp(s)
	bigratfactory.Reuse(s)
	return sgn > 0
}

// IsGreaterThanInt64 returns true iff q > r.
func (q *Element) IsGreaterThanInt64(r int64) bool {
	if q.IsInt64() {
		return q.int64() > r
	}
	s := bigratfactory.New().SetInt64(r)
	sgn := q.bigRat().Cmp(s)
	bigratfactory.Reuse(s)
	return sgn > 0
}

// IsGreaterThanOrEqualTo returns true iff q >= r.
func (q *Element) IsGreaterThanOrEqualTo(r *Element) bool {
	return !q.IsLessThan(r)
}

// IsGreaterThanOrEqualToInteger returns true iff q >= r.
func (q *Element) IsGreaterThanOrEqualToInteger(r *integer.Element) bool {
	return !q.IsLessThanInteger(r)
}

// IsGreaterThanOrEqualToInt64 returns true iff q >= r.
func (q *Element) IsGreaterThanOrEqualToInt64(r int64) bool {
	return !q.IsLessThanInt64(r)
}

// IsLessThan returns true iff q < r.
func (q *Element) IsLessThan(r *Element) bool {
	if q == r {
		return false
	} else if q.IsZero() {
		return r.IsPositive()
	} else if r.IsZero() {
		return q.IsNegative()
	} else if q.IsInt64() {
		if r.IsInt64() {
			return q.int64() < r.int64()
		}
		s := bigratfactory.New().SetInt64(q.int64())
		sgn := s.Cmp(r.bigRat())
		bigratfactory.Reuse(s)
		return sgn < 0
	} else if r.IsInt64() {
		s := bigratfactory.New().SetInt64(r.int64())
		sgn := q.bigRat().Cmp(s)
		bigratfactory.Reuse(s)
		return sgn < 0
	}
	return q.bigRat().Cmp(r.bigRat()) < 0
}

// IsLessThanInteger returns true iff q < r.
func (q *Element) IsLessThanInteger(r *integer.Element) bool {
	if q.IsZero() {
		return r.IsPositive()
	} else if r.IsZero() {
		return q.IsNegative()
	} else if q.IsInt64() {
		return r.IsGreaterThanInt64(q.int64())
	} else if r.IsInt64() {
		k, err := r.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		s := bigratfactory.New().SetInt64(k)
		sgn := q.bigRat().Cmp(s)
		bigratfactory.Reuse(s)
		return sgn < 0
	}
	s := bigratfactory.New().SetInt(r.BigInt())
	sgn := q.bigRat().Cmp(s)
	bigratfactory.Reuse(s)
	return sgn < 0
}

// IsLessThanInt64 returns true iff q < r.
func (q *Element) IsLessThanInt64(r int64) bool {
	if q.IsInt64() {
		return q.int64() < r
	}
	s := bigratfactory.New().SetInt64(r)
	sgn := q.bigRat().Cmp(s)
	bigratfactory.Reuse(s)
	return sgn < 0
}

// IsLessThanOrEqualTo returns true iff q <= r.
func (q *Element) IsLessThanOrEqualTo(r *Element) bool {
	return !q.IsGreaterThan(r)
}

// IsLessThanOrEqualToInteger returns true iff q <= r.
func (q *Element) IsLessThanOrEqualToInteger(r *integer.Element) bool {
	return !q.IsGreaterThanInteger(r)
}

// IsLessThanOrEqualToInt64 returns true iff q <= r.
func (q *Element) IsLessThanOrEqualToInt64(r int64) bool {
	return !q.IsGreaterThanInt64(r)
}

// Sign returns the sign of q, i.e. -1 if q < 0; 0 if n == 0; +1 if q > 0.
func (q *Element) Sign() int {
	if q.IsInt64() {
		if k := q.int64(); k < 0 {
			return -1
		} else if k > 0 {
			return 1
		}
		return 0
	}
	return q.bigRat().Sign()
}

// Abs returns the absolute value of q.
func (q *Element) Abs() *Element {
	if q.IsNegative() {
		q = Negate(q)
	}
	return q
}

// Increment returns q + 1.
func (q *Element) Increment() *Element {
	// Can we do this as an int64 computation?
	if q.IsInt64() {
		if k := q.int64(); k != math.MaxInt64 {
			return FromInt64(k + 1)
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt64(1)
	return fromBigRatAndReuse(r.Add(r, q.bigRat()))
}

// Decrement returns q - 1.
func (q *Element) Decrement() *Element {
	// Can we do this as an int64 computation?
	if q.IsInt64() {
		if k := q.int64(); k != math.MinInt64 {
			return FromInt64(k - 1)
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt64(-1)
	return fromBigRatAndReuse(r.Add(r, q.bigRat()))
}

// ScalarMultiplyByInt64 returns the product a * q.
func (q *Element) ScalarMultiplyByInt64(a int64) *Element {
	// Fast-track the easy cases
	if a == 0 || q.IsZero() {
		return Zero()
	} else if q.IsOne() {
		return FromInt64(a)
	} else if a == 1 {
		return q
	}
	// Can we do this as an int64 computation?
	if q.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a, q.int64()); ok {
			return FromInt64(val)
		}
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt64(a)
	return fromBigRatAndReuse(r.Mul(r, q.bigRat()))
}

// ScalarMultiplyByUint64 returns the product a * q.
func (q *Element) ScalarMultiplyByUint64(a uint64) *Element {
	// Fast-track the easy cases
	if a <= math.MaxInt64 {
		return q.ScalarMultiplyByInt64(int64(a))
	} else if q.IsZero() {
		return Zero()
	} else if q.IsOne() {
		return FromUint64(a)
	}
	// Perform the calculation
	r := bigratfactory.New().SetInt(big.NewInt(0).SetUint64(a))
	return fromBigRatAndReuse(r.Mul(r, q.bigRat()))
}

// ScalarMultiplyByInteger returns the product a * q.
func (q *Element) ScalarMultiplyByInteger(a *integer.Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || q.IsZero() {
		return Zero()
	} else if q.IsOne() {
		return FromInteger(a)
	} else if a.IsOne() {
		return q
	}
	// Can we do this as an int64 computation?
	if q.IsInt64() {
		if a.IsInt64() {
			if aa, err := a.Int64(); err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			} else if val, ok := mathutil.MultiplyInt64(aa, q.int64()); ok {
				return FromInt64(val)
			}
		}
		r := bigratfactory.New().SetInt64(q.int64())
		s := bigratfactory.New().SetInt(a.BigInt())
		r.Mul(r, s)
		bigratfactory.Reuse(s)
		return fromBigRatAndReuse(r)
	} else if a.IsInt64() {
		aa, err := a.Int64()
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		r := bigratfactory.New().SetInt64(aa)
		return fromBigRatAndReuse(r.Mul(r, q.bigRat()))
	}
	// No luck -- do this as a big.Rat computation
	r := bigratfactory.New().SetInt(a.BigInt())
	return fromBigRatAndReuse(r.Mul(r, q.bigRat()))
}

// ScalarMultiplyByRational returns the product a * q.
func (q *Element) ScalarMultiplyByRational(a *Element) *Element {
	return Multiply(a, q)
}

// Negate return -q.
func (q *Element) Negate() *Element {
	return Negate(q)
}

// PowerInt64 returns q^k.
func (q *Element) PowerInt64(k int64) (*Element, error) {
	return PowerInt64(q, k)
}

// Power returns q^k.
func (q *Element) Power(k *integer.Element) (*Element, error) {
	return Power(q, k)
}

// Inverse returns 1/q. It will return an error if q is zero.
func (q *Element) Inverse() (*Element, error) {
	return Inverse(q)
}

// Numerator returns the numerator of q as an integer.
func (q *Element) Numerator() *integer.Element {
	if q.IsInt64() {
		return integer.FromInt64(q.int64())
	}
	return integer.FromBigInt(q.bigRat().Num())
}

// Denominator returns the denominator of q as an integer.
func (q *Element) Denominator() *integer.Element {
	if q.IsIntegral() {
		return integer.One()
	}
	return integer.FromBigInt(q.bigRat().Denom())
}

// Floor returns the biggest integer less than or equal to q
func (q *Element) Floor() *integer.Element {
	n, err := integer.Quotient(q.Numerator(), q.Denominator())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return n
}

// Ceiling returns the smallest integer greater than or equal to q
func (q *Element) Ceiling() *integer.Element {
	n, r, err := integer.QuotientAndRemainder(q.Numerator(), q.Denominator())
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	} else if r.IsZero() {
		return n
	}
	return n.Increment()
}

// Parent returns the field of rational numbers as a object.Parent. This can safely be coerced to type Parent.
func (q *Element) Parent() object.Parent {
	return qQ
}

// Hash returns a hash value for the given rational number.
func (q *Element) Hash() hash.Value {
	if q.IsInt64() {
		return hash.Int64(q.int64())
	}
	return hash.BigRat(q.bigRat())
}
