// Tests for the comparison functions in element.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestCmp tests Cmp, IsNegative, IsPositive, IsEqualTo, IsEqualToInteger, IsEqualToInt64, IsEqualToUint64, IsGreaterThan, IsGreaterThanInteger, IsGreaterThanInt64, IsGreaterThanOrEqualTo, IsGreaterThanOrEqualToInteger, IsGreaterThanOrEqualToInt64, IsLessThan, IsLessThanInteger, IsLessThanInt64, IsLessThanOrEqualTo, IsLessThanOrEqualToInteger, IsLessThanOrEqualToInt64, and Sign
func TestCmp(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some tests, in the form {"a/b","c/d"} where a/b < c/d
	//
	// the rationals a/b and c/d here range over
	//
	// 0, ±1, ±5, ±2^100, 2^100+1, ±7/3, ±(3^50-1)/3^100
	//
	//////////////////////////////////////////////////////////////////////
	tests := [][]string{
		[]string{"0", "1"},
		[]string{"-0", "1"},
		[]string{"0", "5"},
		[]string{"-0", "5"},
		[]string{"0", "1267650600228229401496703205376"},
		[]string{"0", "1267650600228229401496703205377"},
		[]string{"-0", "1267650600228229401496703205376"},
		[]string{"-0", "1267650600228229401496703205377"},
		[]string{"0", "7/3"},
		[]string{"-0", "7/3"},
		[]string{"0", "580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-0", "580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-1", "0"},
		[]string{"-1", "-0"},
		[]string{"-1", "1"},
		[]string{"1", "5"},
		[]string{"-1", "5"},
		[]string{"1", "1267650600228229401496703205376"},
		[]string{"1", "1267650600228229401496703205377"},
		[]string{"-1", "1267650600228229401496703205376"},
		[]string{"-1", "1267650600228229401496703205377"},
		[]string{"1", "7/3"},
		[]string{"-1", "7/3"},
		[]string{"-1", "580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-1", "-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-5", "0"},
		[]string{"-5", "-0"},
		[]string{"-5", "1"},
		[]string{"-5", "-1"},
		[]string{"-5", "5"},
		[]string{"5", "1267650600228229401496703205376"},
		[]string{"5", "1267650600228229401496703205377"},
		[]string{"-5", "1267650600228229401496703205376"},
		[]string{"-5", "1267650600228229401496703205377"},
		[]string{"-5", "7/3"},
		[]string{"-5", "-7/3"},
		[]string{"-5", "580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-5", "-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-1267650600228229401496703205376", "0"},
		[]string{"-1267650600228229401496703205376", "-0"},
		[]string{"-1267650600228229401496703205376", "1"},
		[]string{"-1267650600228229401496703205376", "-1"},
		[]string{"-1267650600228229401496703205376", "5"},
		[]string{"-1267650600228229401496703205376", "-5"},
		[]string{"1267650600228229401496703205376", "1267650600228229401496703205377"},
		[]string{"-1267650600228229401496703205376", "1267650600228229401496703205376"},
		[]string{"-1267650600228229401496703205376", "1267650600228229401496703205377"},
		[]string{"-1267650600228229401496703205376", "7/3"},
		[]string{"-1267650600228229401496703205376", "-7/3"},
		[]string{"-1267650600228229401496703205376",
			"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-1267650600228229401496703205376",
			"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-7/3", "0"},
		[]string{"-7/3", "-0"},
		[]string{"-7/3", "1"},
		[]string{"-7/3", "-1"},
		[]string{"7/3", "5"},
		[]string{"-7/3", "5"},
		[]string{"7/3", "1267650600228229401496703205376"},
		[]string{"7/3", "1267650600228229401496703205377"},
		[]string{"-7/3", "1267650600228229401496703205376"},
		[]string{"-7/3", "1267650600228229401496703205377"},
		[]string{"-7/3", "7/3"},
		[]string{"-7/3", "580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-7/3", "-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"0"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"-0"},
		[]string{"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1"},
		[]string{"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"5"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"5"},
		[]string{"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1267650600228229401496703205376"},
		[]string{"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1267650600228229401496703205377"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1267650600228229401496703205376"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"1267650600228229401496703205377"},
		[]string{"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"7/3"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"7/3"},
		[]string{"-580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376",
			"580263503490997376456871029700460039397056722954200542039501871/653318623500070906096690267158057820537143710472954871543071966369497141477376"},
	}
	//////////////////////////////////////////////////////////////////////
	// perform the tests
	//////////////////////////////////////////////////////////////////////
	for _, t := range tests {
		x, err := FromString(t[0])
		assert.NoError(err)
		y, err := FromString(t[1])
		assert.NoError(err)
		// test Cmp
		assert.Equal(Cmp(x, y), -1)
		assert.Equal(Cmp(x, x), 0)
		assert.Equal(Cmp(y, y), 0)
		assert.Equal(Cmp(y, x), 1)
		// test AreEqual
		assert.True(AreEqual(x, x))
		assert.False(AreEqual(x, y))
		assert.True(AreEqual(y, y))
		// test IsNegative and IsPositive
		if y.IsZero() {
			assert.True(x.IsNegative())
			assert.False(x.IsPositive())
			assert.False(y.IsNegative())
		}
		if x.IsZero() {
			assert.True(y.IsPositive())
			assert.False(y.IsNegative())
			assert.False(x.IsPositive())
		}
		// test IsEqualTo
		assert.True(x.IsEqualTo(x))
		assert.False(x.IsEqualTo(y))
		assert.False(y.IsEqualTo(x))
		assert.True(y.IsEqualTo(y))
		// test IsGreaterThan
		assert.False(x.IsGreaterThan(x))
		assert.False(x.IsGreaterThan(y))
		assert.True(y.IsGreaterThan(x))
		assert.False(y.IsGreaterThan(y))
		// test IsGreaterThanOrEqualTo
		assert.True(x.IsGreaterThanOrEqualTo(x))
		assert.False(x.IsGreaterThanOrEqualTo(y))
		assert.True(y.IsGreaterThanOrEqualTo(x))
		assert.True(y.IsGreaterThanOrEqualTo(y))
		// test IsLessThan
		assert.False(x.IsLessThan(x))
		assert.True(x.IsLessThan(y))
		assert.False(y.IsLessThan(x))
		assert.False(y.IsLessThan(y))
		// test IsLessThanOrEqualTo
		assert.True(x.IsLessThanOrEqualTo(x))
		assert.True(x.IsLessThanOrEqualTo(y))
		assert.False(y.IsLessThanOrEqualTo(x))
		assert.True(y.IsLessThanOrEqualTo(y))
		// test Sign
		if y.IsZero() {
			assert.Equal(x.Sign(), -1)
			assert.Equal(y.Sign(), 0)
		}
		if x.IsZero() {
			assert.Equal(x.Sign(), 0)
			assert.Equal(y.Sign(), 1)
		}
		//////////////////////////////////////////////////////////////////////
		// test comparisons with various sorts of integers
		//////////////////////////////////////////////////////////////////////
		// first when x is actually an integer
		if xInt, err := integer.FromString(t[0]); err == nil {
			// test IsEqualToInteger
			assert.True(x.IsEqualToInteger(xInt))
			assert.False(y.IsEqualToInteger(xInt))
			// test IsGreaterThanInteger
			assert.False(x.IsGreaterThanInteger(xInt))
			assert.True(y.IsGreaterThanInteger(xInt))
			// test IsGreaterThanOrEqualToInteger
			assert.True(x.IsGreaterThanOrEqualToInteger(xInt))
			assert.True(y.IsGreaterThanOrEqualToInteger(xInt))
			// test IsLessThanInteger
			assert.False(x.IsLessThanInteger(xInt))
			assert.False(y.IsLessThanInteger(xInt))
			// test IsLessThanOrEqualToInteger
			assert.True(x.IsLessThanOrEqualToInteger(xInt))
			assert.False(y.IsLessThanOrEqualToInteger(xInt))
			// convert x to an int64 if possible
			if xInt64, err := xInt.Int64(); err == nil {
				// test IsEqualToInt64
				assert.True(x.IsEqualToInt64(xInt64))
				assert.False(y.IsEqualToInt64(xInt64))
				// test IsGreaterThanInt64
				assert.False(x.IsGreaterThanInt64(xInt64))
				assert.True(y.IsGreaterThanInt64(xInt64))
				// test IsGreaterThanOrEqualToInt64
				assert.True(x.IsGreaterThanOrEqualToInt64(xInt64))
				assert.True(y.IsGreaterThanOrEqualToInt64(xInt64))
				// test IsLessThanInt64
				assert.False(x.IsLessThanInt64(xInt64))
				assert.False(y.IsLessThanInt64(xInt64))
				// test IsLessThanOrEqualToInt64
				assert.True(x.IsLessThanOrEqualToInt64(xInt64))
				assert.False(y.IsLessThanOrEqualToInt64(xInt64))
			}
			// test IsEqualToUint64
			if xUint64, err := xInt.Uint64(); err == nil {
				assert.True(x.IsEqualToUint64(xUint64))
				assert.False(y.IsEqualToUint64(xUint64))
			}
		}
		// then when y is actually an integer
		if yInt, err := integer.FromString(t[1]); err == nil {
			// test IsEqualToInteger
			assert.False(x.IsEqualToInteger(yInt))
			assert.True(y.IsEqualToInteger(yInt))
			// test IsGreaterThanInteger
			assert.False(x.IsGreaterThanInteger(yInt))
			assert.False(y.IsGreaterThanInteger(yInt))
			// test IsGreaterThanOrEqualToInteger
			assert.False(x.IsGreaterThanOrEqualToInteger(yInt))
			assert.True(y.IsGreaterThanOrEqualToInteger(yInt))
			// test IsLessThanInteger
			assert.True(x.IsLessThanInteger(yInt))
			assert.False(y.IsLessThanInteger(yInt))
			// test IsLessThanOrEqualToInteger
			assert.True(x.IsLessThanOrEqualToInteger(yInt))
			assert.True(y.IsLessThanOrEqualToInteger(yInt))
			// convert x to an int64 if possible
			if yInt64, err := yInt.Int64(); err == nil {
				// test IsEqualToInt64
				assert.False(x.IsEqualToInt64(yInt64))
				assert.True(y.IsEqualToInt64(yInt64))
				// test IsGreaterThanInt64
				assert.False(x.IsGreaterThanInt64(yInt64))
				assert.False(y.IsGreaterThanInt64(yInt64))
				// test IsGreaterThanOrEqualToInt64
				assert.False(x.IsGreaterThanOrEqualToInt64(yInt64))
				assert.True(y.IsGreaterThanOrEqualToInt64(yInt64))
				// test IsLessThanInt64
				assert.True(x.IsLessThanInt64(yInt64))
				assert.False(y.IsLessThanInt64(yInt64))
				// test IsLessThanOrEqualToInt64
				assert.True(x.IsLessThanOrEqualToInt64(yInt64))
				assert.True(y.IsLessThanOrEqualToInt64(yInt64))
			}
			// test IsEqualToUint64
			if yUint64, err := yInt.Uint64(); err == nil {
				assert.False(x.IsEqualToUint64(yUint64))
				assert.True(y.IsEqualToUint64(yUint64))
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// more tests, to catch a couple of code paths that were missed above
	//////////////////////////////////////////////////////////////////////
	// test Cmp
	x, err := FromString("287364")
	assert.NoError(err)
	y, err := FromString("287364")
	assert.NoError(err)
	assert.Equal(Cmp(x, y), 0)
	// test IsEqualToUint64
	big, err := FromString("18446744073709551615") // 2^64-1
	var bigUint64 uint64 = 18446744073709551615
	assert.False(x.IsEqualToUint64(bigUint64))
	assert.True(big.IsEqualToUint64(bigUint64))
}
