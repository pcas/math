// Tests for slice.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestSlice tests Len, Entry, Slice, SliceFromIntPairs, and SliceFromIntSlice
func TestSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some slices
	//////////////////////////////////////////////////////////////////////
	intSlice := []int{1, 2, 3, 4, 5, 6}
	divisionByZeroSlice := []int{1, 2, 3, 0, 5, 6}
	stringSlice := []string{"1/2", "3/4", "5/6"}
	//////////////////////////////////////////////////////////////////////
	// tests for SliceFromIntPairs
	//////////////////////////////////////////////////////////////////////
	// a slice of odd length
	_, err := SliceFromIntPairs(intSlice[:5])
	assert.Error(err)
	// an empty slice
	var emptySlice Slice
	emptySlice, err = SliceFromIntPairs([]int{})
	assert.NoError(err)
	assert.Len(emptySlice, 0)
	assert.Equal(emptySlice.Len(), 0)
	// an even length slice with a zero denominator
	_, err = SliceFromIntPairs(divisionByZeroSlice)
	assert.Error(err)
	// an even length slice without zero denominators
	var S Slice
	S, err = SliceFromIntPairs(intSlice)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Len and Entry
	//////////////////////////////////////////////////////////////////////
	assert.Equal(S.Len(), 3)
	for i := 0; i < S.Len(); i++ {
		x, err := FromString(stringSlice[i])
		assert.NoError(err)
		assert.True(x.IsEqualTo(S.Entry(i).(*Element)))
		assert.True(x.IsEqualTo(S[i]))
	}
	assert.Panics(func() { S.Entry(-1) })
	assert.Panics(func() { S.Entry(3) })
	//////////////////////////////////////////////////////////////////////
	// tests for Slice
	//////////////////////////////////////////////////////////////////////
	assert.Panics(func() { S.Slice(-1, 3) })
	assert.Panics(func() { S.Slice(0, 4) })
	SS := S.Slice(1, 3)
	for i := 1; i < 3; i++ {
		x, err := FromString(stringSlice[i])
		assert.NoError(err)
		assert.True(x.IsEqualTo(SS.Entry(i - 1).(*Element)))
	}
	//////////////////////////////////////////////////////////////////////
	// tests for SliceFromIntSlice
	//////////////////////////////////////////////////////////////////////
	// an empty slice
	emptySlice = SliceFromIntSlice([]int{})
	assert.Len(emptySlice, 0)
	assert.Equal(emptySlice.Len(), 0)
	// a non-empty slice
	var T Slice
	T = SliceFromIntSlice([]int{-5, -4, -3, -2, -1, 0, 1, 2})
	assert.Len(T, 8)
	assert.Equal(T.Len(), 8)
	for i := -5; i < 3; i++ {
		assert.AreEqual(Field(), T.Entry(i+5), FromInt(i))
	}
	//////////////////////////////////////////////////////////////////////
	// tests for CopySlice
	//////////////////////////////////////////////////////////////////////
	TT := CopySlice(T)
	assert.NotEqual(T, TT)
	assert.Equal(len(T), len(TT))
	for i, x := range T {
		assert.True(x.IsEqualTo(TT[i]))
	}
	//////////////////////////////////////////////////////////////////////
	// tests for Hash
	//////////////////////////////////////////////////////////////////////
	// equal slices should hash to the same value
	var U Slice
	U = make([]*Element, 0, len(T))
	for i, _ := range T {
		U = append(U, FromInt(i-5))
	}
	assert.Equal(T.Hash(), U.Hash())
}
