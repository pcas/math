// Parent represents the field of rational numbers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
)

// Parent represents the (unique) field of rational numbers.
type Parent struct{}

// The unique field of rational numbers.
var qQ = Parent{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// objectsToElements converts the two arguments to *Elements.
func objectsToElements(x1 object.Element, x2 object.Element) (*Element, *Element, error) {
	y1, err1 := ToElement(x1)
	if err1 != nil {
		return nil, nil, errors.Arg1NotARational.New()
	}
	y2, err2 := ToElement(x2)
	if err2 != nil {
		return nil, nil, errors.Arg2NotARational.New()
	}
	return y1, y2, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Contains returns true iff x is a rational number, or can naturally be regarded as a rational number.
func (R Parent) Contains(x object.Element) bool {
	if _, ok := x.(*Element); ok {
		return true
	} else if _, ok := x.(*integer.Element); ok {
		return true
	}
	_, err := ToElement(x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (R Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(x)
}

// String returns a string representation of the field of rational numbers.
func (R Parent) String() string {
	return "Field of rational numbers"
}

// Zero returns 0.
func (R Parent) Zero() object.Element {
	return Zero()
}

// IsZero returns true iff x is the rational number 0.
func (R Parent) IsZero(x object.Element) (bool, error) {
	if xx, ok := x.(*Element); ok {
		return xx.IsZero(), nil
	} else if xx, ok := x.(*integer.Element); ok {
		return xx.IsZero(), nil
	}
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// One returns 1.
func (R Parent) One() object.Element {
	return One()
}

// IsOne returns true iff x is the rational number 1.
func (R Parent) IsOne(x object.Element) (bool, error) {
	if xx, ok := x.(*Element); ok {
		return xx.IsOne(), nil
	} else if xx, ok := x.(*integer.Element); ok {
		return xx.IsOne(), nil
	}
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsOne(), nil
}

// Add returns x + y.
func (R Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	if xx, ok := x.(*Element); ok {
		if yy, ok := y.(*Element); ok {
			return Add(xx, yy), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return AddInteger(yy, xx), nil
		}
	} else if xx, ok := x.(*integer.Element); ok {
		if yy, ok := y.(*Element); ok {
			return AddInteger(xx, yy), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return FromInteger(integer.Add(xx, yy)), nil
		}
	}
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy), nil
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
func (R Parent) Sum(S ...object.Element) (object.Element, error) {
	// Get the easy cases out of the way
	if size := len(S); size == 0 {
		return Zero(), nil
	} else if size == 1 {
		return ToElement(S[0])
	} else if size == 2 {
		return R.Add(S[0], S[1])
	}
	// Convert the slice type
	T := make([]*Element, 0, len(S))
	for _, q := range S {
		qq, err := ToElement(q)
		if err != nil {
			return nil, err
		}
		T = append(T, qq)
	}
	// Return the result
	return Sum(T...), nil
}

// Subtract returns x - y.
func (R Parent) Subtract(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Subtract(xx, yy), nil
}

// Multiply returns the product x * y.
func (R Parent) Multiply(x object.Element, y object.Element) (object.Element, error) {
	if xx, ok := x.(*Element); ok {
		if yy, ok := y.(*Element); ok {
			return Multiply(xx, yy), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return xx.ScalarMultiplyByInteger(yy), nil
		}
	} else if xx, ok := x.(*integer.Element); ok {
		if yy, ok := y.(*Element); ok {
			return yy.ScalarMultiplyByInteger(xx), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return FromInteger(integer.Multiply(xx, yy)), nil
		}
	}
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Multiply(xx, yy), nil
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one.
func (R Parent) Product(S ...object.Element) (object.Element, error) {
	// Get the easy cases out of the way
	if size := len(S); size == 0 {
		return One(), nil
	} else if size == 1 {
		return ToElement(S[0])
	} else if size == 2 {
		return R.Multiply(S[0], S[1])
	}
	// Convert the slice type
	T := make([]*Element, 0, len(S))
	for _, q := range S {
		qq, err := ToElement(q)
		if err != nil {
			return nil, err
		}
		T = append(T, qq)
	}
	// Return the result
	return Product(T...), nil
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.
func (R Parent) DotProduct(S1 []object.Element, S2 []object.Element) (object.Element, error) {
	// Convert S1 and S2 to slices of rationals
	T1 := make([]*Element, 0, len(S1))
	for _, q := range S1 {
		qq, err := ToElement(q)
		if err != nil {
			return nil, err
		}
		T1 = append(T1, qq)
	}
	T2 := make([]*Element, 0, len(S2))
	for _, q := range S2 {
		qq, err := ToElement(q)
		if err != nil {
			return nil, err
		}
		T2 = append(T2, qq)
	}
	// Return the dot-product
	return DotProduct(T1, T2), nil
}

// Negate returns -q.
func (R Parent) Negate(q object.Element) (object.Element, error) {
	qq, err := ToElement(q)
	if err != nil {
		return nil, err
	}
	return Negate(qq), nil
}

// Power returns q^k.
func (R Parent) Power(q object.Element, k *integer.Element) (object.Element, error) {
	qq, err := ToElement(q)
	if err != nil {
		return nil, err
	}
	return Power(qq, k)
}

// Inverse returns 1/q. Returns an errors.DivisionByZero error if q is zero.
func (R Parent) Inverse(q object.Element) (object.Element, error) {
	qq, err := ToElement(q)
	if err != nil {
		return nil, err
	}
	return Inverse(qq)
}

// IsUnit returns true iff q is a multiplicative unit (i.e. q is non-zero). If true, also returns the inverse element 1/q.
func (R Parent) IsUnit(q object.Element) (bool, object.Element, error) {
	qq, err := ToElement(q)
	if err != nil {
		return false, nil, err
	} else if qq.IsZero() {
		return false, nil, nil
	}
	qq, err = Inverse(qq)
	if err != nil {
		return false, nil, err
	}
	return true, qq, nil
}

// Divide returns the quotient x / y. Returns a errors.DivisionByZero error if y is zero.
func (R Parent) Divide(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Divide(xx, yy)
}

// ScalarMultiplyByInteger returns nx, where this is defined to be x + ... + x (n times) if n is positive, and -x - ... - x (|n| times) if n is negative.
func (R Parent) ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, errors.Arg2NotARational.New()
	}
	return xx.ScalarMultiplyByInteger(n), nil
}

// AreEqual returns true iff x equals y.
func (R Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	if xx, ok := x.(*Element); ok {
		if yy, ok := y.(*Element); ok {
			return xx.IsEqualTo(yy), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return xx.IsEqualToInteger(yy), nil
		}
	} else if xx, ok := x.(*integer.Element); ok {
		if yy, ok := y.(*Element); ok {
			return yy.IsEqualToInteger(xx), nil
		} else if yy, ok := y.(*integer.Element); ok {
			return xx.IsEqualTo(yy), nil
		}
	}
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func (R Parent) Cmp(x object.Element, y object.Element) (int, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return 0, err
	}
	return Cmp(xx, yy), nil
}

// FromInteger returns n as rational number.
func (R Parent) FromInteger(n *integer.Element) object.Element {
	return FromInteger(n)
}

// FromRational returns q.
func (R Parent) FromRational(q *Element) (object.Element, error) {
	return q, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Field returns the (unique) field of rational numbers
func Field() Parent {
	return qQ
}

// ToElement attempts to convert the given object.Element to a rational number. If necessary this will be done by first attempting to convert x to an integer, and then from an integer to a rational.
func ToElement(x object.Element) (*Element, error) {
	if y, ok := x.(*Element); ok {
		return y, nil
	} else if y, err := integer.ToElement(x); err == nil {
		return FromInteger(y), nil
	}
	return nil, errors.ArgNotARational.New()
}
