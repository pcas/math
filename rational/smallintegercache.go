// Smallintegercache maintains a cache of small-valued integer elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"math/big"
)

// The minimum and maximum integer values stored in the small-valued integers cache.
const (
	smallIntegerCacheMin = -1 << 10
	smallIntegerCacheMax = 1 << 10
)

// The minimum and maximum integer values stored in the small-valued integers cache, represented as a *big.Int. Treat these values as constants.
var (
	smallIntegerCacheMinAsBigInt = big.NewInt(smallIntegerCacheMin)
	smallIntegerCacheMaxAsBigInt = big.NewInt(smallIntegerCacheMax)
)

// _smallIntegerCache is a cache of Element objects associated with the small integers. Do NOT access the cache directly -- instead use the standard creation functions.
var _smallIntegerCache []Element

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// initSmallIntegerCache initialises the cache of small-valued Elements. This is called from the package's init function, and should NOT be called directly.
func initSmallIntegerCache() {
	_smallIntegerCache = make([]Element, smallIntegerCacheMax-smallIntegerCacheMin+1)
	for i := smallIntegerCacheMin; i <= smallIntegerCacheMax; i++ {
		_smallIntegerCache[i-smallIntegerCacheMin].k = int64(i)
	}
}
