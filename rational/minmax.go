// Minmax implements functions for finding the minimum and maximum in a slice of elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
)

// The maximum length of the slice if calculate the min/max directly.
const maxMinMaxLen = 900

/////////////////////////////////////////////////////////////////////////
// Min functions
/////////////////////////////////////////////////////////////////////////

// indexOfMinInternal passes the index + offset of the first occurrence of the minimum element in the slice S to the channel c. Assumes that S is non-empty.
func indexOfMinInternal(S []*Element, offset int, resc chan<- int) {
	resc <- indexOfMin(S, offset)
}

// indexOfMin returns the index + offset of the first occurrence of the minimum element in the slice S. Assumes that S is non-empty.
func indexOfMin(S []*Element, offset int) int {
	// If S is short enough, we do it now
	if len(S) <= maxMinMaxLen {
		// Fast-track the length 1 and 2 cases
		if len(S) == 1 {
			return offset
		} else if len(S) == 2 {
			if S[1].IsLessThan(S[0]) {
				return offset + 1
			}
			return offset
		}
		// Work through S looking for the smallest element
		idx := 0
		x := S[0]
		for i, y := range S[1:] {
			if y.IsLessThan(x) {
				idx = i + 1
				x = y
			}
		}
		// Return the result
		return offset + idx
	}
	// Create the communication channel
	resc := make(chan int)
	// Partition the task
	div := len(S) / 2
	go indexOfMinInternal(S[:div], offset, resc)
	go indexOfMinInternal(S[div:], offset+div, resc)
	// Process the result
	idx1 := <-resc
	idx2 := <-resc
	if sgn := Cmp(S[idx1-offset], S[idx2-offset]); sgn < 0 || (sgn == 0 && idx1 < idx2) {
		return idx1
	}
	return idx2
}

// MinOfPair returns the minimum element of x and y.
func MinOfPair(x *Element, y *Element) *Element {
	if x.IsLessThanOrEqualTo(y) {
		return x
	}
	return y
}

// Min returns the minimum element in the slice S. If S is empty then an error is returned.
func Min(S ...*Element) (*Element, error) {
	if len(S) == 0 {
		return nil, errors.EmptySlice.New()
	}
	idx := indexOfMin(S, 0)
	return S[idx], nil
}

// Min returns the maximum element in the slice S. If S is empty then an error is returned.
func (R Parent) Min(S ...object.Element) (object.Element, error) {
	x, _, err := R.MinAndIndex(S)
	return x, err
}

// MinAndIndex returns the minimum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
func MinAndIndex(S []*Element) (*Element, int, error) {
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	}
	idx := indexOfMin(S, 0)
	return S[idx], idx, nil
}

// MinAndIndex returns the minimum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
func (R Parent) MinAndIndex(S []object.Element) (object.Element, int, error) {
	// Get the easy cases out of the way
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	} else if len(S) == 1 {
		x, err := ToElement(S[0])
		return x, 0, err
	} else if len(S) == 2 {
		x, y, err := objectsToElements(S[0], S[1])
		if err != nil {
			return nil, 0, err
		}
		if x.IsLessThanOrEqualTo(y) {
			return x, 0, nil
		}
		return y, 1, nil
	}
	// Convert the slice type
	sS := make([]*Element, 0, len(S))
	for _, x := range S {
		xx, err := ToElement(x)
		if err != nil {
			return nil, 0, err
		}
		sS = append(sS, xx)
	}
	// Return the minimum
	idx := indexOfMin(sS, 0)
	return sS[idx], idx, nil
}

/////////////////////////////////////////////////////////////////////////
// Max functions
/////////////////////////////////////////////////////////////////////////

// indexOfMaxInternal passes the index + offset of the first occurrence of the maximum element in the slice S to the channel c. Assumes that S is non-empty.
func indexOfMaxInternal(S []*Element, offset int, resc chan<- int) {
	resc <- indexOfMax(S, offset)
}

// indexOfMax returns the index + offset of the first occurrence of the maximum element in the slice S. Assumes that S is non-empty.
func indexOfMax(S []*Element, offset int) int {
	// If S is short enough, we do it now
	if len(S) <= maxMinMaxLen {
		// Fast-track the length 1 and 2 cases
		if len(S) == 1 {
			return offset
		} else if len(S) == 2 {
			if S[1].IsGreaterThan(S[0]) {
				return offset + 1
			}
			return offset
		}
		// Work through S looking for the largest element
		idx := 0
		x := S[0]
		for i, y := range S[1:] {
			if x.IsLessThan(y) {
				idx = i + 1
				x = y
			}
		}
		// Return the result
		return offset + idx
	}
	// Create the communication channel
	resc := make(chan int)
	// Partition the task
	div := len(S) / 2
	go indexOfMaxInternal(S[:div], offset, resc)
	go indexOfMaxInternal(S[div:], offset+div, resc)
	// Process the result
	idx1 := <-resc
	idx2 := <-resc
	if sgn := Cmp(S[idx1-offset], S[idx2-offset]); sgn > 0 {
		return idx1
	} else if sgn == 0 && idx1 < idx2 {
		return idx1
	}
	return idx2
}

// MaxOfPair returns the maximum element of x and y.
func MaxOfPair(x *Element, y *Element) *Element {
	if x.IsGreaterThanOrEqualTo(y) {
		return x
	}
	return y
}

// Max returns the maximum element in the slice S. If S is empty then an error is returned.
func Max(S ...*Element) (*Element, error) {
	if len(S) == 0 {
		return nil, errors.EmptySlice.New()
	}
	idx := indexOfMax(S, 0)
	return S[idx], nil
}

// Max returns the maximum element in the slice S. If S is empty then an error is returned.
func (R Parent) Max(S ...object.Element) (object.Element, error) {
	x, _, err := R.MaxAndIndex(S)
	return x, err
}

// MaxAndIndex returns the maximum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
func MaxAndIndex(S []*Element) (*Element, int, error) {
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	}
	idx := indexOfMax(S, 0)
	return S[idx], idx, nil
}

// MaxAndIndex returns the maximum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
func (R Parent) MaxAndIndex(S []object.Element) (object.Element, int, error) {
	// Get the easy cases out of the way
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	} else if len(S) == 1 {
		x, err := ToElement(S[0])
		return x, 0, err
	} else if len(S) == 2 {
		x, y, err := objectsToElements(S[0], S[1])
		if err != nil {
			return nil, 0, err
		}
		if x.IsGreaterThanOrEqualTo(y) {
			return x, 0, nil
		}
		return y, 1, nil
	}
	// Convert the slice type
	sS := make([]*Element, 0, len(S))
	for _, x := range S {
		xx, err := ToElement(x)
		if err != nil {
			return nil, 0, err
		}
		sS = append(sS, xx)
	}
	// Return the maximum
	idx := indexOfMax(sS, 0)
	return sS[idx], idx, nil
}
