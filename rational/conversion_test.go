// Tests for conversion.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"math"
	"math/big"
	"testing"
)

// TestInt64 tests Int64 and IsInt64
func TestInt64(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusSeven := fromIntPair(-7, 1)
	maxInt64 := fromIntPair(math.MaxInt64, 1)
	minInt64 := fromIntPair(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// tests for IsInt64
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsInt64())
	assert.False(half.IsInt64())
	assert.True(one.IsInt64())
	assert.True(minusSeven.IsInt64())
	assert.True(nilRational.IsInt64())
	assert.True(maxInt64.IsInt64())
	assert.True(minInt64.IsInt64())
	assert.False(maxInt64.Increment().IsInt64())
	assert.False(minInt64.Decrement().IsInt64())
	//////////////////////////////////////////////////////////////////////
	// tests for Int64
	//////////////////////////////////////////////////////////////////////
	if x, err := zero.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(0))
	}
	_, err := half.Int64()
	assert.Error(err)
	if x, err := one.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(1))
	}
	if x, err := minusSeven.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(-7))
	}
	if x, err := nilRational.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(0))
	}
	if x, err := maxInt64.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(math.MaxInt64))
	}
	if x, err := minInt64.Int64(); assert.NoError(err) {
		assert.Equal(x, int64(math.MinInt64))
	}
	_, err = maxInt64.Increment().Int64()
	assert.Error(err)
	_, err = minInt64.Decrement().Int64()
	assert.Error(err)
}

// TestUint64 tests Uint64 and IsUint64
func TestUint64(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusOne := fromIntPair(-1, 1)
	minusSeven := fromIntPair(-7, 1)
	maxUint64 := FromUint64(math.MaxUint64)
	//////////////////////////////////////////////////////////////////////
	// tests for IsUint64
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsUint64())
	assert.False(half.IsUint64())
	assert.True(one.IsUint64())
	assert.False(minusSeven.IsUint64())
	assert.True(nilRational.IsUint64())
	assert.True(maxUint64.IsUint64())
	assert.False(minusOne.IsUint64())
	assert.False(maxUint64.Increment().IsUint64())
	//////////////////////////////////////////////////////////////////////
	// tests for Uint64
	//////////////////////////////////////////////////////////////////////
	if x, err := zero.Uint64(); assert.NoError(err) {
		assert.Equal(x, uint64(0))
	}
	_, err := half.Uint64()
	assert.Error(err)
	if x, err := one.Uint64(); assert.NoError(err) {
		assert.Equal(x, uint64(1))
	}
	_, err = minusSeven.Uint64()
	assert.Error(err)
	if x, err := nilRational.Uint64(); assert.NoError(err) {
		assert.Equal(x, uint64(0))
	}
	if x, err := maxUint64.Uint64(); assert.NoError(err) {
		assert.Equal(x, uint64(math.MaxUint64))
	}
	_, err = minusOne.Uint64()
	assert.Error(err)
	_, err = maxUint64.Increment().Uint64()
	assert.Error(err)
}

// TestToInteger tests ToInteger
func TestToInteger(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusSeven := fromIntPair(-7, 1)
	maxInt64 := fromIntPair(math.MaxInt64, 1)
	minInt64 := fromIntPair(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if x, err := zero.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.Zero()))
	}
	_, err := half.ToInteger()
	assert.Error(err)
	if x, err := one.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.One()))
	}
	if x, err := minusSeven.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt(-7)))
	}
	if x, err := nilRational.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.Zero()))
	}
	if x, err := maxInt64.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt64(int64(math.MaxInt64))))
	}
	if x, err := minInt64.ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt64(int64(math.MinInt64))))
	}
	if x, err := maxInt64.Increment().ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt64(int64(math.MaxInt64)).Increment()))
	}
	if x, err := minInt64.Decrement().ToInteger(); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt64(int64(math.MinInt64)).Decrement()))
	}
}

// TestBigInt tests BigInt
func TestBigInt(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusSeven := fromIntPair(-7, 1)
	maxInt64 := fromIntPair(math.MaxInt64, 1)
	minInt64 := fromIntPair(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// the same rationals as (*big.Int)s
	//////////////////////////////////////////////////////////////////////
	bigIntZero := big.NewInt(0)
	bigIntOne := big.NewInt(1)
	bigIntMinusSeven := big.NewInt(-7)
	bigIntMaxInt64 := big.NewInt(math.MaxInt64)
	bigIntMinInt64 := big.NewInt(math.MinInt64)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if x, err := zero.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntZero))
	}
	_, err := half.BigInt()
	assert.Error(err)
	if x, err := one.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntOne))
	}
	if x, err := minusSeven.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntMinusSeven))
	}
	if x, err := nilRational.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntZero))
	}
	if x, err := maxInt64.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntMaxInt64))
	}
	if x, err := minInt64.BigInt(); assert.NoError(err) {
		assert.Zero(x.Cmp(bigIntMinInt64))
	}
	// n is a *big.Int to do arithmetic with
	n := big.NewInt(0)
	if x, err := maxInt64.Increment().BigInt(); assert.NoError(err) {
		n.Add(bigIntMaxInt64, bigIntOne)
		assert.Zero(x.Cmp(n))
	}
	if x, err := minInt64.Decrement().BigInt(); assert.NoError(err) {
		n.Sub(bigIntMinInt64, bigIntOne)
		assert.Zero(x.Cmp(n))
	}
}

// TestBigRat tests BigRat and SetBigRat
func TestBigRat(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusSeven := fromIntPair(-7, 1)
	maxInt64 := fromIntPair(math.MaxInt64, 1)
	minInt64 := fromIntPair(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// the same rationals as (*big.Rat)s
	//////////////////////////////////////////////////////////////////////
	bigRatZero := big.NewRat(0, 1)
	bigRatHalf := big.NewRat(1, 2)
	bigRatOne := big.NewRat(1, 1)
	bigRatMinusSeven := big.NewRat(-7, 1)
	bigRatMaxInt64 := big.NewRat(math.MaxInt64, 1)
	bigRatMinInt64 := big.NewRat(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	input := []*Element{
		nilRational,
		zero,
		half,
		one,
		minusSeven,
		maxInt64,
		minInt64,
	}
	expected := []*big.Rat{
		bigRatZero,
		bigRatZero,
		bigRatHalf,
		bigRatOne,
		bigRatMinusSeven,
		bigRatMaxInt64,
		bigRatMinInt64,
	}
	n := big.NewRat(0, 1)
	m := big.NewRat(0, 1)
	n.Add(bigRatMaxInt64, bigRatOne)
	m.Sub(bigRatMinInt64, bigRatOne)
	input = append(input, maxInt64.Increment())
	expected = append(expected, n)
	input = append(input, minInt64.Decrement())
	expected = append(expected, m)
	// a *big.Rat that gets set during the test
	q := big.NewRat(1, 2)
	for i, x := range input {
		// test BigRat
		assert.Zero(x.BigRat().Cmp(expected[i]))
		// test SetBigRat
		SetBigRat(q, x)
		assert.Zero(x.BigRat().Cmp(q))
		r := big.NewRat(0, 1)
		SetBigRat(r, x)
		assert.Zero(x.BigRat().Cmp(r))
	}
}

// TestString tests String
func TestString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	zero := Zero()
	half := fromIntPair(1, 2)
	one := fromIntPair(7, 7)
	minusSeven := fromIntPair(-7, 1)
	maxInt64 := fromIntPair(math.MaxInt64, 1)
	minInt64 := fromIntPair(math.MinInt64, 1)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	input := []*Element{
		nilRational,
		zero,
		half,
		one,
		minusSeven,
		maxInt64,
		minInt64,
		maxInt64.Increment(),
		minInt64.Decrement(),
	}
	expected := []string{
		"0",
		"0",
		"1/2",
		"1",
		"-7",
		"9223372036854775807",
		"-9223372036854775808",
		"9223372036854775808",
		"-9223372036854775809",
	}
	for i, x := range input {
		assert.Equal(x.String(), expected[i])
	}
}
