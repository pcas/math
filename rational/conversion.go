// Conversion contains functions for converting a rational to a different type.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/rational/internal/bigratfactory"
	"math"
	"math/big"
	"strconv"
)

// Various math constants as big.Ints.
var (
	oneAsBigInt = big.NewInt(1)
	minInt64    = big.NewInt(math.MinInt64)
	maxInt64    = big.NewInt(math.MaxInt64)
	maxUint64   = big.NewInt(0).SetUint64(math.MaxUint64)
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SetBigRat sets the given *big.Rat q to the value of the given rational r. Returns the *big.Rat q.
func SetBigRat(q *big.Rat, r *Element) *big.Rat {
	if r.IsInt64() {
		return q.SetInt64(r.int64())
	}
	return q.Set(r.bigRat())
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsInt64 returns true iff the rational can be expressed as an int64.
func (q *Element) IsInt64() bool {
	return q == nil || q.q.Sign() == 0
}

// int64 returns q as an int64. Warning: If q does not fit in an int64 then the returned value is undefined.
func (q *Element) int64() int64 {
	if q == nil {
		return 0
	}
	return q.k
}

// Int64 returns the rational as an int64, or an error if this is not possible.
func (q *Element) Int64() (int64, error) {
	if q.IsInt64() {
		return q.int64(), nil
	} else if q.bigRat().IsInt() {
		return 0, errors.OutOfRange.New()
	}
	return 0, errors.RationalNotAnInteger.New()
}

// IsUint64 returns true iff the rational can be expressed as a uint64.
func (q *Element) IsUint64() bool {
	if q.IsInt64() {
		return q.int64() >= 0
	} else if r := q.bigRat(); r.IsInt() {
		return r.Num().IsUint64()
	}
	return false
}

// Uint64 returns the rational as a uint64, or an error if this is not possible.
func (q *Element) Uint64() (uint64, error) {
	if q.IsInt64() {
		if k := q.int64(); k >= 0 {
			return uint64(k), nil
		}
		return 0, errors.OutOfRange.New()
	} else if r := q.bigRat(); r.IsInt() {
		k := r.Num()
		if k.IsUint64() {
			return k.Uint64(), nil
		}
		return 0, errors.OutOfRange.New()
	}
	return 0, errors.RationalNotAnInteger.New()
}

// IsIntegral returns true iff q can be represented by an integer.
func (q *Element) IsIntegral() bool {
	return q.IsInt64() || q.bigRat().IsInt()
}

// ToInteger return the rational as an integer, or an error if this is not possible.
func (q *Element) ToInteger() (*integer.Element, error) {
	if q.IsInt64() {
		return integer.FromInt64(q.int64()), nil
	} else if r := q.bigRat(); r.IsInt() {
		return integer.FromBigInt(r.Num()), nil
	}
	return nil, errors.RationalNotAnInteger.New()
}

// BigInt returns the rational as a *big.Int, or an error if this is not possible.
func (q *Element) BigInt() (*big.Int, error) {
	if q.IsInt64() {
		return big.NewInt(q.int64()), nil
	} else if r := q.bigRat(); r.IsInt() {
		return big.NewInt(0).Set(r.Num()), nil
	}
	return nil, errors.RationalNotAnInteger.New()
}

// bigRat exposes the underlying *big.Rat defining q. Warning: The returned value must NOT be modified, and must NOT be exposed to the user.
func (q *Element) bigRat() *big.Rat {
	if q.IsInt64() {
		return bigratfactory.New().SetInt64(q.int64())
	}
	return &q.q
}

// BigRat returns the rational as a *big.Rat.
func (q *Element) BigRat() *big.Rat {
	r := bigratfactory.New()
	if q.IsInt64() {
		return r.SetInt64(q.int64())
	}
	return r.Set(q.bigRat())
}

// String returns a string representation of the given rational.
func (q *Element) String() string {
	if q.IsInt64() {
		return strconv.FormatInt(q.int64(), 10)
	}
	r := q.bigRat()
	if r.IsInt() {
		return r.Num().String()
	}
	return r.String()
}
