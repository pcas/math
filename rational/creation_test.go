// Tests for creation.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"math"
	"math/big"
	"testing"
)

// TestZeroOne tests Zero and One
func TestZeroOne(t *testing.T) {
	assert := assert.New(t)
	assert.True(Zero().IsEqualTo(FromInt(0)))
	assert.Equal(Zero().String(), "0")
	assert.True(One().IsEqualTo(FromInt(1)))
	assert.Equal(One().String(), "1")
}

// TestFromInt tests FromInt and FromInt64
func TestFromInt(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := []int{
		0,
		1,
		-1,
		-7,
		math.MaxInt64,
		math.MinInt64}
	expected := []string{
		"0",
		"1",
		"-1",
		"-7",
		"9223372036854775807",
		"-9223372036854775808",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, x := range input {
		// test FromInt
		y := FromInt(x)
		assert.Equal(y.String(), expected[i])
		// test FromInt64
		y = FromInt64(int64(x))
		assert.Equal(y.String(), expected[i])
	}
}

// TestFromInt32 tests FromInt32
func TestFromInt32(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := []int32{
		0,
		1,
		-1,
		-7,
		math.MaxInt32,
		math.MinInt32}
	expected := []string{
		"0",
		"1",
		"-1",
		"-7",
		"2147483647",
		"-2147483648",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, x := range input {
		// test FromInt
		y := FromInt32(x)
		assert.Equal(y.String(), expected[i])
	}
}

// TestFromUint64 tests FromUint64
func TestFromUint64(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := []uint64{
		0,
		1,
		math.MaxInt32,
		math.MaxInt64,
		math.MaxInt64 + 1,
		math.MaxUint64,
	}
	expected := []string{
		"0",
		"1",
		"2147483647",
		"9223372036854775807",
		"9223372036854775808",
		"18446744073709551615",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, x := range input {
		// test FromInt
		y := FromUint64(x)
		assert.Equal(y.String(), expected[i])
	}
}

// TestFromInteger tests FromInteger and FromBigInt
func TestFromInteger(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := []*integer.Element{
		integer.FromInt(0),
		integer.FromInt(1),
		integer.FromInt(-1),
		integer.FromInt(7),
		integer.FromInt(-7),
		integer.FromInt(math.MaxInt64),
		integer.FromInt(math.MaxInt64).Increment(),
		integer.FromInt(math.MinInt64),
		integer.FromInt(math.MinInt64).Decrement(),
	}
	expected := []string{
		"0",
		"1",
		"-1",
		"7",
		"-7",
		"9223372036854775807",
		"9223372036854775808",
		"-9223372036854775808",
		"-9223372036854775809",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, x := range input {
		// test FromInteger
		assert.Equal(FromInteger(x).String(), expected[i])
		// test FromBigInt
		if y, err := FromInteger(x).BigInt(); assert.NoError(err) {
			assert.True(FromBigInt(y).IsEqualTo(FromInteger(x)))
		}
	}
	var nilBigInt *big.Int
	assert.True(FromBigInt(nilBigInt).IsZero())
}

// TestFromBigRat tests FromBigRat
func TestFromBigRat(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := []*big.Rat{
		nil,
		big.NewRat(0, 1),
		big.NewRat(1, 2),
		big.NewRat(-1, 2),
		big.NewRat(12, 25),
		big.NewRat(math.MaxInt64, 2),
		big.NewRat(math.MinInt64, 3),
		big.NewRat(0, 1).Mul(big.NewRat(math.MaxInt64, 2), big.NewRat(11, 3)),
		big.NewRat(0, 1).Mul(big.NewRat(math.MinInt64, 3), big.NewRat(17, 2)),
	}
	expected := []string{
		"0",
		"0",
		"1/2",
		"-1/2",
		"12/25",
		"9223372036854775807/2",
		"-9223372036854775808/3",
		"101457092405402533877/6",
		"-78398662313265594368/3",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, x := range input {
		assert.Equal(FromBigRat(x).String(), expected[i])
	}
}

// TestFromIntPair tests FromIntPair, FromInt32Pair, FromInt64Pair, and FromUint64Pair
func TestFromIntPair(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// test values
	//////////////////////////////////////////////////////////////////////
	input := [][]int{
		[]int{0, 1},
		[]int{0, 7},
		[]int{0, -7},
		[]int{15, 15},
		[]int{-3, -3},
		[]int{5, 1},
		[]int{3, -1},
		[]int{-3, -1},
		[]int{2, 5},
		[]int{2, 6},
		[]int{-2, 6},
		[]int{math.MaxInt64 - 1, math.MaxInt64},
		[]int{math.MinInt64 + 1, math.MinInt64},
	}
	expected := []string{
		"0",
		"0",
		"0",
		"1",
		"1",
		"5",
		"-3",
		"3",
		"2/5",
		"1/3",
		"-1/3",
		"9223372036854775806/9223372036854775807",
		"9223372036854775807/9223372036854775808",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, xx := range input {
		num := xx[0]
		den := xx[1]
		// test FromIntPair
		if x, err := FromIntPair(num, den); assert.NoError(err) {
			assert.Equal(x.String(), expected[i])
		}
		_, err := FromIntPair(num, 0)
		assert.Error(err)
		// test FromInt64Pair
		if x, err := FromInt64Pair(int64(num), int64(den)); assert.NoError(err) {
			assert.Equal(x.String(), expected[i])
		}
		_, err = FromInt64Pair(int64(num), 0)
		assert.Error(err)
		// test FromInt32Pair
		if num >= math.MinInt32 && num <= math.MaxInt32 && den >= math.MinInt32 && den <= math.MaxInt32 {
			if x, err := FromInt32Pair(int32(num), int32(den)); assert.NoError(err) {
				assert.Equal(x.String(), expected[i])
			}
			_, err = FromInt32Pair(int32(num), 0)
			assert.Error(err)
		}
		// test FromUint64Pair
		if num >= 0 && den >= 0 {
			if x, err := FromUint64Pair(uint64(num), uint64(den)); assert.NoError(err) {
				assert.Equal(x.String(), expected[i])
			}
			_, err = FromUint64Pair(uint64(num), 0)
			assert.Error(err)
		}

	}
}

// TestFromIntegerPair tests FromIntegerPair and FromBigIntPair
func TestFromIntegerPair(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	twoPower100, err := integer.PowerInt64(integer.FromInt(2), 100)
	assert.NoError(err)
	fiftyFactorial, err := integer.FactorialInt64(50)
	assert.NoError(err)
	input := [][]*integer.Element{
		[]*integer.Element{integer.FromInt(0), integer.FromInt(1)},
		[]*integer.Element{integer.FromInt(0), integer.FromInt(7)},
		[]*integer.Element{integer.FromInt(0), integer.FromInt(-7)},
		[]*integer.Element{integer.FromInt(15), integer.FromInt(15)},
		[]*integer.Element{integer.FromInt(-3), integer.FromInt(-3)},
		[]*integer.Element{integer.FromInt(5), integer.FromInt(1)},
		[]*integer.Element{integer.FromInt(3), integer.FromInt(-1)},
		[]*integer.Element{integer.FromInt(-3), integer.FromInt(-1)},
		[]*integer.Element{integer.FromInt(2), integer.FromInt(5)},
		[]*integer.Element{integer.FromInt(2), integer.FromInt(6)},
		[]*integer.Element{integer.FromInt(-2), integer.FromInt(6)},
		[]*integer.Element{integer.FromInt(math.MaxInt64 - 1), integer.FromInt(math.MaxInt64)},
		[]*integer.Element{integer.FromInt(math.MinInt64 + 1), integer.FromInt(math.MinInt64)},
		[]*integer.Element{twoPower100, fiftyFactorial},
		[]*integer.Element{twoPower100, fiftyFactorial.Negate()},
		[]*integer.Element{twoPower100, twoPower100},
	}
	expected := []string{
		"0",
		"0",
		"0",
		"1",
		"1",
		"5",
		"-3",
		"3",
		"2/5",
		"1/3",
		"-1/3",
		"9223372036854775806/9223372036854775807",
		"9223372036854775807/9223372036854775808",
		"9007199254740992/216105129892080882169214875191192738017616943359375",
		"-9007199254740992/216105129892080882169214875191192738017616943359375",
		"1",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, xx := range input {
		num := xx[0]
		den := xx[1]
		// tests for FromIntegerPair
		if x, err := FromIntegerPair(num, den); assert.NoError(err) {
			assert.Equal(x.String(), expected[i])
		}
		_, err := FromIntegerPair(num, integer.Zero())
		assert.Error(err)
		// tests for FromBigIntPair
		if x, err := FromBigIntPair(num.BigInt(), den.BigInt()); assert.NoError(err) {
			assert.Equal(x.String(), expected[i])
		}
		_, err = FromBigIntPair(num.BigInt(), integer.Zero().BigInt())
		assert.Error(err)
	}
}

// TestFromString tests  FromString
func TestFromString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	input := []string{
		"-212",
		"   -212",
		"-212   ",
		"    -212        ",
		"0/1",
		"0/7",
		"0/-7",
		"15/15",
		"-3/-3",
		"5/1",
		"3/-1",
		"-3/-1",
		"2/5",
		"2/6",
		"-2/6",
		"  -2/6",
		"-2/6  ",
		"  -2  /6",
		"-2/ 6    ",
		"   -2     /     6      ",
		"9223372036854775806/9223372036854775807",
		"9223372036854775807/9223372036854775808",
		"1267650600228229401496703205376/30414093201713378043612608166064768844377641568960512000000000000",
		"-1267650600228229401496703205376/30414093201713378043612608166064768844377641568960512000000000000",
		"1267650600228229401496703205376/1267650600228229401496703205376",
	}
	expected := []string{
		"-212",
		"-212",
		"-212",
		"-212",
		"0",
		"0",
		"0",
		"1",
		"1",
		"5",
		"-3",
		"3",
		"2/5",
		"1/3",
		"-1/3",
		"-1/3",
		"-1/3",
		"-1/3",
		"-1/3",
		"-1/3",
		"9223372036854775806/9223372036854775807",
		"9223372036854775807/9223372036854775808",
		"9007199254740992/216105129892080882169214875191192738017616943359375",
		"-9007199254740992/216105129892080882169214875191192738017616943359375",
		"1",
	}
	junkInput := []string{
		"4/0",
		"fiddlesticks/27",
		"qpoiwru",
		"6/1/4",
		"2^100/Factorial(50)",
		"4.0/3",
		"/23",
		"23/",
	}
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i, xx := range input {
		if x, err := FromString(xx); assert.NoError(err) {
			assert.Equal(x.String(), expected[i])
		}
	}
	for _, xx := range junkInput {
		_, err := FromString(xx)
		assert.Error(err)
	}
}
