// Tests for parent.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package rational

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"math"
	"testing"
)

// TestContains tests Contains and ToElement
func TestContains(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	// rationals with small numerator and denominator
	fourFifths, err := FromString("4/5")
	assert.NoError(err)
	minusFourFifths := fourFifths.Negate()
	// rationals with large numerator and denominator
	rat, err := FromString("717897987691852588770248/515377520732011331036461129765621272702107522001")
	assert.NoError(err)
	minusRat := rat.Negate()
	// various forms of 5
	five := FromInt(5)
	fiveInteger := integer.FromInt(5)
	// various forms of -5
	minusFive := five.Negate()
	minusFiveInteger := fiveInteger.Negate()
	// math.MaxInt64 = 2^63-1
	big := FromInt(math.MaxInt64)
	bigInteger := integer.FromInt(math.MaxInt64)
	// 2^63
	bigger := big.Increment()
	biggerInteger := integer.FromInt(math.MaxInt64).Increment()
	//////////////////////////////////////////////////////////////////////
	// test Contains
	//////////////////////////////////////////////////////////////////////
	assert.True(qQ.Contains(fourFifths))
	assert.True(qQ.Contains(minusFourFifths))
	assert.True(qQ.Contains(rat))
	assert.True(qQ.Contains(minusRat))
	assert.True(qQ.Contains(five))
	assert.True(qQ.Contains(fiveInteger))
	assert.True(qQ.Contains(minusFive))
	assert.True(qQ.Contains(minusFiveInteger))
	assert.True(qQ.Contains(big))
	assert.True(qQ.Contains(bigInteger))
	assert.True(qQ.Contains(bigger))
	assert.True(qQ.Contains(biggerInteger))
	assert.False(qQ.Contains(nil))
	//////////////////////////////////////////////////////////////////////
	// test ToElement
	//////////////////////////////////////////////////////////////////////
	if x, err := qQ.ToElement(fourFifths); assert.NoError(err) {
		assert.AreEqual(qQ, x, fourFifths)
	}
	if x, err := qQ.ToElement(minusFourFifths); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusFourFifths)
	}
	if x, err := qQ.ToElement(rat); assert.NoError(err) {
		assert.AreEqual(qQ, x, rat)
	}
	if x, err := qQ.ToElement(minusRat); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusRat)
	}
	if x, err := qQ.ToElement(five); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.ToElement(fiveInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, five)
	}
	if x, err := qQ.ToElement(minusFive); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusFive)
	}
	if x, err := qQ.ToElement(minusFiveInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusFive)
	}
	if x, err := qQ.ToElement(big); assert.NoError(err) {
		assert.AreEqual(qQ, x, big)
	}
	if x, err := qQ.ToElement(bigInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, big)
	}
	if x, err := qQ.ToElement(bigger); assert.NoError(err) {
		assert.AreEqual(qQ, x, bigger)
	}
	if x, err := qQ.ToElement(biggerInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, bigger)
	}
	_, err = qQ.ToElement(nil)
	assert.Error(err)
}

// TestParentZeroOne tests Zero, One, IsZero, and IsOne
func TestParentZeroOne(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	twoThirds, err := FromIntPair(2, 3)
	assert.NoError(err)
	minusOne := FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	zeroInteger := integer.Zero()
	oneInteger := integer.One()
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	// test IsZero
	if bool, err := qQ.IsZero(qQ.Zero()); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.IsZero(qQ.One()); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsZero(twoThirds); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsZero(minusOne); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsZero(zeroInteger); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.IsZero(oneInteger); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsZero(minusOneInteger); assert.NoError(err) {
		assert.False(bool)
	}
	_, err = qQ.IsZero(nil)
	assert.Error(err)
	// test IsOne
	if bool, err := qQ.IsOne(qQ.Zero()); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsOne(qQ.One()); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.IsOne(twoThirds); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsOne(minusOne); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsOne(zeroInteger); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.IsOne(oneInteger); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.IsOne(minusOneInteger); assert.NoError(err) {
		assert.False(bool)
	}
	_, err = qQ.IsOne(nil)
	assert.Error(err)
}

// TestParentAddSubtract tests Add and Subtract.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the actual addition and subtraction code are in add_test.go.
func TestParentAddSubtract(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	zero := Zero()
	one := One()
	two := one.Increment()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// tests for Add
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if x, err := qQ.Add(half, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Add(one, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Add(oneInteger, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Add(oneInteger, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Add(minusOneInteger, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	if x, err := qQ.Add(oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	_, err = qQ.Add(nil, nil)
	assert.Error(err)
	_, err = qQ.Add(nil, half)
	assert.Error(err)
	_, err = qQ.Add(half, nil)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Subtract
	//////////////////////////////////////////////////////////////////////
	if x, err := qQ.Subtract(one, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Subtract(oneInteger, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	_, err = qQ.Subtract(nil, nil)
	assert.Error(err)
	_, err = qQ.Subtract(nil, half)
	assert.Error(err)
	_, err = qQ.Subtract(half, nil)
	assert.Error(err)
}

// TestParentSum tests Sum.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for sum code are (FIX ME) in sumproduct_test.go.
func TestParentSum(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	zero := Zero()
	one := One()
	two := one.Increment()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	// the empty sum
	if x, err := qQ.Sum(); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	// length one
	if x, err := qQ.Sum(half); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Sum(oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, oneInteger)
	}
	_, err = qQ.Sum(nil)
	assert.Error(err)
	// length two
	if x, err := qQ.Sum(half, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Sum(one, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Sum(oneInteger, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Sum(oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	_, err = qQ.Sum(nil, half)
	assert.Error(err)
	_, err = qQ.Sum(half, nil)
	assert.Error(err)
	_, err = qQ.Sum(nil, nil)
	assert.Error(err)
	// length three or more
	if x, err := qQ.Sum(half, half, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Sum(half, oneInteger, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Sum(oneInteger, oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	_, err = qQ.Sum(nil, half, half)
	assert.Error(err)
	_, err = qQ.Sum(half, nil, half)
	assert.Error(err)
	_, err = qQ.Sum(half, half, nil)
	assert.Error(err)
}

// TestParentMultiplyDivide tests Multiply and Divide.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the actual multiplication and division code are in multiply_test.go.
func TestParentMultiplyDivide(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	quarter, err := FromIntPair(1, 4)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// tests for Add
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if x, err := qQ.Multiply(half, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, quarter)
	}
	if x, err := qQ.Multiply(one, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Multiply(oneInteger, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Multiply(oneInteger, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Multiply(minusOneInteger, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	if x, err := qQ.Multiply(oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	_, err = qQ.Multiply(nil, nil)
	assert.Error(err)
	_, err = qQ.Multiply(nil, half)
	assert.Error(err)
	_, err = qQ.Multiply(half, nil)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Divide
	//////////////////////////////////////////////////////////////////////
	if x, err := qQ.Divide(half, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Divide(half, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	_, err = qQ.Divide(nil, nil)
	assert.Error(err)
	_, err = qQ.Divide(nil, half)
	assert.Error(err)
	_, err = qQ.Divide(half, nil)
	assert.Error(err)
}

// TestParentProduct tests Product.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for sum code are (FIX ME) in sumproduct_test.go.
func TestParentProduct(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	quarter, err := FromIntPair(1, 4)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	// the empty product
	if x, err := qQ.Product(); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	// length one
	if x, err := qQ.Product(half); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Product(oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	_, err = qQ.Product(nil)
	assert.Error(err)
	// length two
	if x, err := qQ.Product(half, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, quarter)
	}
	if x, err := qQ.Product(one, oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Product(oneInteger, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Product(oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	_, err = qQ.Product(nil, half)
	assert.Error(err)
	_, err = qQ.Product(half, nil)
	assert.Error(err)
	_, err = qQ.Product(nil, nil)
	assert.Error(err)
	// length three or more
	if x, err := qQ.Product(half, half, one); assert.NoError(err) {
		assert.AreEqual(qQ, x, quarter)
	}
	if x, err := qQ.Product(half, oneInteger, half); assert.NoError(err) {
		assert.AreEqual(qQ, x, quarter)
	}
	if x, err := qQ.Product(oneInteger, oneInteger, minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	_, err = qQ.Product(nil, half, half)
	assert.Error(err)
	_, err = qQ.Product(half, nil, half)
	assert.Error(err)
	_, err = qQ.Product(half, half, nil)
	assert.Error(err)
}

// TestNegate tests Negate.
func TestNegate(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	minusHalf, err := FromIntPair(-1, 2)
	assert.NoError(err)
	big, err := FromString("276701161105643274241/15") // 2^64+1/15
	assert.NoError(err)
	minusBig, err := FromString("-276701161105643274241/15") // -2^64-1/15
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if x, err := qQ.Negate(one); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	if x, err := qQ.Negate(minusOne); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Negate(half); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusHalf)
	}
	if x, err := qQ.Negate(minusHalf); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Negate(big); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusBig)
	}
	if x, err := qQ.Negate(minusBig); assert.NoError(err) {
		assert.AreEqual(qQ, x, big)
	}
	if x, err := qQ.Negate(oneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	if x, err := qQ.Negate(minusOneInteger); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	_, err = qQ.Negate(nil)
	assert.Error(err)
}

// TestParentPower tests Power.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the underlying power code are in element_test.go.
func TestParentPower(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	zero := Zero()
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	eighth, err := FromIntPair(1, 8)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if x, err := qQ.Power(zero, integer.FromInt(5)); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	_, err = qQ.Power(zero, integer.FromInt(-5))
	assert.Error(err)
	if x, err := qQ.Power(minusOne, integer.FromInt(178)); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Power(half, integer.FromInt(3)); assert.NoError(err) {
		assert.AreEqual(qQ, x, eighth)
	}
	_, err = qQ.Power(nil, integer.FromInt(3))
	assert.Error(err)
}

// TestParentInverse tests Inverse.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the underlying inversion code are in element_test.go.
func TestParentInverse(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	zero := Zero()
	one := One()
	minusOne := one.Negate()
	two := one.Increment()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	minusBig, err := FromString("-276701161105643274241/15") // -2^64 - 1/15
	assert.NoError(err)
	minusSmall, err := FromString("-15/276701161105643274241") // -1/(2^64 + 1/15)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	_, err = qQ.Inverse(zero)
	assert.Error(err)
	if x, err := qQ.Inverse(one); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.Inverse(minusOne); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	if x, err := qQ.Inverse(two); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.Inverse(half); assert.NoError(err) {
		assert.AreEqual(qQ, x, two)
	}
	if x, err := qQ.Inverse(minusBig); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusSmall)
	}
	if x, err := qQ.Inverse(minusSmall); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusBig)
	}
	_, err = qQ.Inverse(nil)
	assert.Error(err)
}

// TestParentScalarMultiplyByInteger tests ScalarMultiplyByInteger.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the underlying multiplication code are in element_test.go.
func TestParentScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	zero := Zero()
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	eighth, err := FromIntPair(1, 8)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if x, err := qQ.ScalarMultiplyByInteger(integer.FromInt(5), zero); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	if x, err := qQ.ScalarMultiplyByInteger(integer.FromInt(-5), zero); assert.NoError(err) {
		assert.AreEqual(qQ, x, zero)
	}
	if x, err := qQ.ScalarMultiplyByInteger(integer.FromInt(-1), minusOne); assert.NoError(err) {
		assert.AreEqual(qQ, x, one)
	}
	if x, err := qQ.ScalarMultiplyByInteger(integer.FromInt(4), eighth); assert.NoError(err) {
		assert.AreEqual(qQ, x, half)
	}
	if x, err := qQ.ScalarMultiplyByInteger(integer.FromInt(-8), eighth); assert.NoError(err) {
		assert.AreEqual(qQ, x, minusOne)
	}
	_, err = qQ.ScalarMultiplyByInteger(integer.FromInt(3), nil)
	assert.Error(err)
}

// TestParentCmp tests Cmp and AreEqual.  These tests mainly check the conversions from object.Elements are handled correctly.  Tests for the underlying comparison code are in cmp_test.go.
func TestParentCmp(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some rationals
	//////////////////////////////////////////////////////////////////////
	one := One()
	minusOne := one.Negate()
	half, err := FromIntPair(1, 2)
	assert.NoError(err)
	eighth, err := FromIntPair(1, 8)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some integers
	//////////////////////////////////////////////////////////////////////
	oneInteger := integer.FromInt(1)
	minusOneInteger := integer.FromInt(-1)
	//////////////////////////////////////////////////////////////////////
	// tests for AreEqual
	//////////////////////////////////////////////////////////////////////
	qQ := Field()
	if bool, err := qQ.AreEqual(one, one); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.AreEqual(one, minusOne); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.AreEqual(minusOne, one); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.AreEqual(minusOne, minusOne); assert.NoError(err) {
		assert.True(bool)
	}
	if x, err1 := qQ.Sum(eighth, eighth, eighth, eighth); assert.NoError(err1) {
		if bool, err2 := qQ.AreEqual(half, x); assert.NoError(err2) {
			assert.True(bool)
		}
	}
	if bool, err := qQ.AreEqual(one, oneInteger); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.AreEqual(oneInteger, one); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.AreEqual(oneInteger, minusOne); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.AreEqual(one, minusOneInteger); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.AreEqual(minusOneInteger, minusOneInteger); assert.NoError(err) {
		assert.True(bool)
	}
	if bool, err := qQ.AreEqual(one, half); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, err := qQ.AreEqual(half, one); assert.NoError(err) {
		assert.False(bool)
	}
	_, err = qQ.AreEqual(eighth, nil)
	assert.Error(err)
	_, err = qQ.AreEqual(nil, eighth)
	assert.Error(err)
	_, err = qQ.AreEqual(nil, nil)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Cmp
	//////////////////////////////////////////////////////////////////////
	if x, err := qQ.Cmp(half, half); assert.NoError(err) {
		assert.Equal(x, 0)
	}
	if x, err := qQ.Cmp(eighth, half); assert.NoError(err) {
		assert.Equal(x, -1)
	}
	if x, err := qQ.Cmp(half, eighth); assert.NoError(err) {
		assert.Equal(x, 1)
	}
	if x, err := qQ.Cmp(oneInteger, half); assert.NoError(err) {
		assert.Equal(x, 1)
	}
	if x, err := qQ.Cmp(half, oneInteger); assert.NoError(err) {
		assert.Equal(x, -1)
	}
	if x, err := qQ.Cmp(oneInteger, oneInteger); assert.NoError(err) {
		assert.Equal(x, 0)
	}
	_, err = qQ.Cmp(nil, half)
	assert.Error(err)
	_, err = qQ.Cmp(half, nil)
	assert.Error(err)
	_, err = qQ.Cmp(nil, nil)
	assert.Error(err)
}

// TestParentFrom tests FromInteger and FromRational
func TestParentFrom(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// an integer and a rational
	//////////////////////////////////////////////////////////////////////
	sevenInteger := integer.FromInt(7)
	seven := FromInt(7)
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	var nilRational *Element
	var nilInteger *integer.Element
	qQ := Field()
	// tests for FromInteger
	assert.AreEqual(qQ, qQ.FromInteger(sevenInteger), seven)
	assert.AreEqual(qQ, qQ.FromInteger(nilInteger), Zero())
	// tests for FromRational
	if x, err := qQ.FromRational(seven); assert.NoError(err) {
		assert.AreEqual(qQ, x, sevenInteger)
	}
	if x, err := qQ.FromRational(nilRational); assert.NoError(err) {
		assert.AreEqual(qQ, x, Zero())
	}
	if x, err := qQ.FromRational(nil); assert.NoError(err) {
		assert.AreEqual(qQ, x, Zero())
	}
}
