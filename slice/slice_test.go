// Tests of slice.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcas/math/boolean"
	"bitbucket.org/pcas/math/object"
	"github.com/stretchr/testify/assert"
	"testing"
)

// dummySlice is a minimal implementation of the Interface.
type dummySlice []object.Element

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// areSlicesEqual returns true iff the elements in the two slices agree.
func areSlicesEqual(S1 []object.Element, S2 []object.Element) bool {
	if len(S1) != len(S2) {
		return false
	}
	for i, x := range S1 {
		if y := S2[i]; x != y {
			if R := x.Parent(); y.Parent() != R {
				return false
			} else if areEqual, err := R.AreEqual(x, y); err != nil {
				panic(err)
			} else if !areEqual {
				return false
			}
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// dummySlice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S dummySlice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S dummySlice) Entry(i int) object.Element {
	return S[i]
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestElementSlice tests ElementSlice and ToElementSlice.
func TestElementSlice(t *testing.T) {
	assert := assert.New(t)
	R := boolean.Booleans()
	// Create a slice of object.Elements
	S := make(ElementSlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, boolean.Element(i%2 == 0))
	}
	// Check that we can correctly recover the data
	if assert.Equal(S.Len(), len(S)) {
		for i, s := range S {
			if ok, err := R.AreEqual(S.Entry(i), s); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Asking for an out-of-range entry should panic
	assert.Panics(func() { S.Entry(-1) })
	assert.Panics(func() { S.Entry(len(S)) })
	// Recover the slice of object.Elemets
	T := ToElementSlice(S)
	if assert.Equal(len(T), len(S)) {
		for i, s := range S {
			if ok, err := R.AreEqual(T[i], s); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Test the subslice
	S2 := Slice(S, 3, 7)
	assert.Equal(S2.Len(), 4)
	_, ok := S2.(ElementSlice)
	assert.True(ok)
	for i := 0; i < S2.Len(); i++ {
		if ok, err := R.AreEqual(S[i+3], S2.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// Rebuild the subslice directly
	Ssubslice := make(ElementSlice, 0, S2.Len())
	for i := 0; i < S2.Len(); i++ {
		Ssubslice = append(Ssubslice, S[i+3])
	}
	// Check that the hashes agree
	assert.Equal(Hash(S2), Hash(Ssubslice))
	// Check that the element slices agree
	assert.True(areSlicesEqual(ToElementSlice(S2), ToElementSlice(Ssubslice)))
}

// TestSlice tests the Slice function.
func TestSlice(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of object.Elements
	R := boolean.Booleans()
	S := make(dummySlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, boolean.Element(i%2 == 0))
	}
	// Check that the hash agrees with the generic hash
	T := ElementSlice(S)
	assert.Equal(Hash(S), Hash(T))
	// Check that the element slices agree
	assert.True(areSlicesEqual(ToElementSlice(S), ToElementSlice(T)))
	// Asking for illegal subslices should panic
	assert.Panics(func() { Slice(S, -1, 0) })
	assert.Panics(func() { Slice(S, 0, -1) })
	assert.Panics(func() { Slice(S, 3, 2) })
	assert.Panics(func() { Slice(S, 0, 11) })
	assert.Panics(func() { Slice(S, 11, 12) })
	// Test the subslice
	S2 := Slice(S, 3, 7)
	assert.Equal(S2.Len(), 4)
	for i := 0; i < S2.Len(); i++ {
		if ok, err := R.AreEqual(S[i+3], S2.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// Rebuild the subslice directly
	Ssubslice := make(ElementSlice, 0, S2.Len())
	for i := 0; i < S2.Len(); i++ {
		Ssubslice = append(Ssubslice, S[i+3])
	}
	// Check that the hashes agree
	assert.Equal(Hash(S2), Hash(Ssubslice))
	// Check that the element slices agree
	assert.True(areSlicesEqual(ToElementSlice(S2), ToElementSlice(Ssubslice)))
	// Asking for invalid entries should panic
	assert.Panics(func() { S2.Entry(-1) })
	assert.Panics(func() { S2.Entry(9) })
	// Test taking a slice of the subslice
	S3 := Slice(S2, 1, 3)
	assert.Equal(S3.Len(), 2)
	for i := 0; i < S3.Len(); i++ {
		if ok, err := R.AreEqual(S[i+4], S3.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// Asking for illegal subslices should panic
	assert.Panics(func() { Slice(S2, -1, 0) })
	assert.Panics(func() { Slice(S2, 0, -1) })
	assert.Panics(func() { Slice(S2, 3, 2) })
	assert.Panics(func() { Slice(S2, 0, 9) })
	assert.Panics(func() { Slice(S2, 9, 10) })
}
