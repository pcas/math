// Tests of reverse.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcas/math/boolean"
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestReverse tests Reverse.
func TestReverse(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of object.Elements
	R := boolean.Booleans()
	S := make(ElementSlice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, boolean.Element(i%2 == 0))
	}
	// Reverse the slice
	T := ReverseSlice(S)
	// Check the values
	k := S.Len()
	if assert.Equal(T.Len(), k) {
		for i := 0; i < k; i++ {
			if ok, err := R.AreEqual(T.Entry(i), S.Entry(k-i-1)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Reversing the reversed slice should return the original slice
	assert.Equal(S, ReverseSlice(T))
	// Rebuild S in reverse order
	Sreverse := make(ElementSlice, 10)
	for i, s := range S {
		Sreverse[k-i-1] = s
	}
	// Check that the hashes agree
	assert.Equal(Hash(T), Hash(Sreverse))
	// Check that the element slices agree
	assert.True(areSlicesEqual(ToElementSlice(T), ToElementSlice(Sreverse)))
}
