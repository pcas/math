// Generator dynamically creates the entries of a slice.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/hash"
)

// GeneratorFunc is used to dynamically generate the i-th entry in a slice.
type GeneratorFunc func(i int) object.Element

// generator implements the Interface for a dynamically generated slice.
type generator struct {
	f      GeneratorFunc // The underlying generator
	size   int           // The size
	offset int           // Any index offset
}

/////////////////////////////////////////////////////////////////////////
// generator functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S *generator) Len() int {
	return S.size
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S *generator) Entry(i int) object.Element {
	if n := S.Len(); i < 0 || i >= n {
		panic(errors.InvalidIndexRange.New(i, n-1))
	}
	return S.f(i + S.offset)
}

// Hash returns a hash value for the slice.
func (S *generator) Hash() hash.Value {
	n := S.Len()
	if n == 0 {
		return 0
	}
	offset := S.offset
	f := S.f
	v := hash.NewSequenceHasher()
	defer hash.ReuseSequenceHasher(v)
	for i := 0; i < n; i++ {
		v.Add(f(i + offset))
	}
	return v.Hash()
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S *generator) Slice(k int, m int) Interface {
	// Sanity check
	n := S.Len()
	if k < 0 {
		panic(errors.InvalidIndexNonNegative.New(k))
	} else if k > n {
		panic(errors.InvalidIndexRange.New(k, n))
	} else if m < 0 {
		panic(errors.InvalidIndexNonNegative.New(m))
	} else if m > n {
		panic(errors.InvalidIndexRange.New(m, n))
	} else if k > m {
		panic(errors.InvalidIndexInequality.New(k, m))
	}
	// Return the subslice
	return &generator{
		f:      S.f,
		size:   m - k,
		offset: k,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NewGenerator returns a new dynamic slice of length "size" using the given generator function.
func NewGenerator(size int, f GeneratorFunc) (Interface, error) {
	if size < 0 {
		return nil, errors.SliceLengthMustBeNonNegative.New(size)
	}
	return &generator{
		f:    f,
		size: size,
	}, nil
}
