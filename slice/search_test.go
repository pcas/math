// Tests of search.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package slice

import (
	"bitbucket.org/pcas/math/boolean"
	"bitbucket.org/pcas/math/object"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// brokenCmp returns an error on use.
func brokenCmp(_ object.Element, _ object.Element) (int, error) {
	return 0, errors.New("Always errors")
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestSearchAndIndex tests Search and Index.
func TestSearchAndIndex(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of booleans where all but the final entry is false
	S := make([]object.Element, 0, 10)
	for i := 0; i < 9; i++ {
		S = append(S, boolean.Element(false))
	}
	S = append(S, boolean.Element(true))
	// Check that we can recover this entry
	if idx, err := Search(boolean.Booleans().Cmp, ElementSlice(S), boolean.Element(true)); assert.NoError(err) {
		assert.Equal(idx, len(S)-1)
	}
	if idx, err := Index(boolean.Booleans().Cmp, ElementSlice(S), boolean.Element(true)); assert.NoError(err) {
		assert.Equal(idx, len(S)-1)
	}
	// Set the final entry to false
	S[len(S)-1] = boolean.Element(false)
	// Check that searching for true returns the length...
	if idx, err := Search(boolean.Booleans().Cmp, ElementSlice(S), boolean.Element(true)); assert.NoError(err) {
		assert.Equal(idx, len(S))
	}
	// ...and that the index of true is -1
	if idx, err := Index(boolean.Booleans().Cmp, ElementSlice(S), boolean.Element(true)); assert.NoError(err) {
		assert.Equal(idx, -1)
	}
	// Let's use a broken comparator that throws an error on use
	_, err := Search(brokenCmp, ElementSlice(S), boolean.Element(true))
	assert.Error(err)
	_, err = Index(brokenCmp, ElementSlice(S), boolean.Element(true))
	assert.Error(err)
}
