// Objectmap defines the general interface all maps between parents must satisfy, along with a general creation method.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package objectmap

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"fmt"
	"strings"
)

// rightArrow defines the right arrow symbol used when printing maps.
const rightArrow = "->"

// Interface defines the interface satisfied by a map between parents.
type Interface interface {
	fmt.Stringer
	Domain() object.Parent                             // Domain returns the domain.
	Codomain() object.Parent                           // Codomain returns the codomain.
	Evaluate(x object.Element) (object.Element, error) // Evaluate returns the image of x.
}

// Func defines a function that can be evaluated at x.
type Func func(x object.Element) (object.Element, error)

// generalMap is a general implementation of the Interface wrapping a Func.
type generalMap struct {
	domain   object.Parent // The domain
	codomain object.Parent // The codomain
	f        Func          // The function
}

// compositionMap is implements composition.
type compositionMap struct {
	fi []Interface // The maps
}

/////////////////////////////////////////////////////////////////////////
// generalMap functions
/////////////////////////////////////////////////////////////////////////

// Domain returns the domain.
func (m *generalMap) Domain() object.Parent {
	return m.domain
}

// Codomain returns the codomain.
func (m *generalMap) Codomain() object.Parent {
	return m.codomain
}

// Evaluate returns the image of x.
func (m *generalMap) Evaluate(x object.Element) (object.Element, error) {
	xx, err := m.Domain().ToElement(x)
	if err != nil {
		return nil, err
	}
	return m.f(xx)
}

// String returns a string description of the map.
func (m *generalMap) String() string {
	return m.Domain().String() + " " + rightArrow + " " + m.Codomain().String()
}

/////////////////////////////////////////////////////////////////////////
// compositionMap functions
/////////////////////////////////////////////////////////////////////////

// Domain returns the domain.
func (m *compositionMap) Domain() object.Parent {
	return m.fi[0].Domain()
}

// Codomain returns the codomain.
func (m *compositionMap) Codomain() object.Parent {
	return m.fi[len(m.fi)-1].Codomain()
}

// Evaluate returns the image of x.
func (m *compositionMap) Evaluate(x object.Element) (object.Element, error) {
	var err error
	for _, f := range m.fi {
		if x, err = f.Evaluate(x); err != nil {
			return nil, err
		}
	}
	return x, nil
}

// String returns a string description of the map.
func (m *compositionMap) String() string {
	strs := make([]string, 0, len(m.fi)+1)
	strs = append(strs, m.Domain().String())
	for _, f := range m.fi {
		strs = append(strs, f.Codomain().String())
	}
	return strings.Join(strs, " "+rightArrow+" ")
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new map from the given domain to the given codomain defined by the function f.
func New(domain object.Parent, codomain object.Parent, f Func) Interface {
	return &generalMap{
		domain:   domain,
		codomain: codomain,
		f:        f,
	}
}

// Compose return the composition h of the maps f1, f2, ..., fk, given by:
//		h(x) = (fk ○ ... ○ f2 ○ f1)(x) = fk(...(f2((f1(x))))...).
func Compose(f1 Interface, fi ...Interface) (Interface, error) {
	// Get the trivial case out of the way
	if len(fi) == 0 {
		return f1, nil
	}
	// Sanity check on the domains/codomains
	codomain := f1.Codomain()
	for _, f := range fi {
		if f.Domain() != codomain {
			return nil, errors.DomainNotEqualCodomainInComposition.New(codomain.String(), f.Domain().String())
		}
		codomain = f.Codomain()
	}
	// Create the new slice of maps
	maps := make([]Interface, 0, len(fi)+1)
	maps = append(maps, f1)
	maps = append(maps, fi...)
	// Return the composition
	return &compositionMap{fi: maps}, nil
}
