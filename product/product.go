// Product defines the general interface of a Cartesian product S_1 x S_2 x ... x S_k.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package product

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/objectmap"
)

// Interface is the interface satisfied by a Cartesian product S_1 x S_2 x ... x S_k of object.Parents.
type Interface interface {
	Rank() int                                        // Rank returns the number of factors k of the Cartesian product S_1 x S_2 x ... x S_k.
	ProjectionMap(i int) (objectmap.Interface, error) // ProjectionMap returns the projection S_1 x S_2 x ... x S_k -> S_{i+1} onto the (i+1)-th factor of the product. That is, the maps are indexed from 0.
}

// InclusionMaper is the interface satisfied by the InclusionMap method.
type InclusionMaper interface {
	InclusionMap(i int) (objectmap.Interface, error) // InclusionMap returns the inclusion S_{i+1} -> S_1 x S_2 x ... x S_k into the (i+1)-th factor of the product. This is, the maps are indexed from 0.
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ProjectionMaps returns the projection maps of S.
func ProjectionMaps(S Interface) ([]objectmap.Interface, error) {
	rk := S.Rank()
	projs := make([]objectmap.Interface, 0, rk)
	for i := 0; i < rk; i++ {
		proj, err := S.ProjectionMap(i)
		if err != nil {
			return nil, err
		}
		projs = append(projs, proj)
	}
	return projs, nil
}

// InclusionMaps returns the inclusion maps of S.
func InclusionMaps(S Interface) ([]objectmap.Interface, error) {
	// Does S support inclusion maps?
	SS, ok := S.(InclusionMaper)
	if !ok {
		return nil, errors.ProductDoesNotHaveInclusionMaps.New()
	}
	// Build the inclusion maps
	rk := S.Rank()
	embs := make([]objectmap.Interface, 0, rk)
	for i := 0; i < rk; i++ {
		emb, err := SS.InclusionMap(i)
		if err != nil {
			return nil, err
		}
		embs = append(embs, emb)
	}
	return embs, nil
}
