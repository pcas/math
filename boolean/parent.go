// Parent defines the parent of boolean elements, which is a boolean algebra.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package boolean

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
)

// Parent is the parent of a boolean element.
type Parent struct{}

// There is a (unique) parent for all boolean elements.
var bB = Parent{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// elementsToBooleanElements converts the two arguments to Elements.
func elementsToBooleanElements(x1 object.Element, x2 object.Element) (Element, Element, error) {
	b1, err1 := ToElement(x1)
	if err1 != nil {
		return false, false, errors.Arg1NotABoolean.New()
	}
	b2, err2 := ToElement(x2)
	if err2 != nil {
		return false, false, errors.Arg2NotABoolean.New()
	}
	return b1, b2, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// True returns the value corresponding to true.
func (B Parent) True() object.Element {
	return Element(true)
}

// False returns the value corresponding to false.
func (B Parent) False() object.Element {
	return Element(false)
}

// IsTrue returns true iff x is equal to the value corresponding to true.
func (B Parent) IsTrue(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return bool(xx), nil
}

// IsFalse returns true iff x is equal to the value corresponding to false.
func (B Parent) IsFalse(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return !bool(xx), nil
}

// And returns the result x AND y.
func (B Parent) And(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := elementsToBooleanElements(x, y)
	if err != nil {
		return nil, err
	}
	return xx && yy, nil
}

// Or returns the result x OR y.
func (B Parent) Or(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := elementsToBooleanElements(x, y)
	if err != nil {
		return nil, err
	}
	return xx || yy, nil
}

// Not returns the negation NOT x.
func (B Parent) Not(x object.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, err
	}
	return !xx, nil
}

// AreEqual returns true iff x equals y.
func (B Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := elementsToBooleanElements(x, y)
	if err != nil {
		return false, err
	}
	return xx == yy, nil
}

// Cmp returns -1 if x is false and y is true, 0 if x == y, and +1 if x is true and y is false.
func (B Parent) Cmp(x object.Element, y object.Element) (int, error) {
	xx, yy, err := elementsToBooleanElements(x, y)
	if err != nil {
		return 0, err
	}
	if xx == yy {
		return 0, nil
	} else if xx && !yy {
		return 1, nil
	}
	return -1, nil
}

// Contains returns true iff x is a boolean.
func (B Parent) Contains(x object.Element) bool {
	_, err := ToElement(x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (B Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(x)
}

// String returns a string description of the space of booleans.
func (B Parent) String() string {
	return "Booleans"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToElement attempts to convert the argument to an Element.
func ToElement(x object.Element) (Element, error) {
	if b, ok := x.(Element); ok {
		return b, nil
	}
	return false, errors.ArgNotABoolean.New()
}

// Booleans returns the parent object for boolean values.
func Booleans() Parent {
	return bB
}
