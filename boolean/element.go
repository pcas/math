// Element wraps a bool to define a boolean element.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package boolean

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/hash"
	"strconv"
	"strings"
)

// Element satisfies the object.Element interface.
type Element bool

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromString returns the boolean represented by the string s.
func FromString(s string) (Element, error) {
	b, err := strconv.ParseBool(strings.TrimSpace(s))
	return Element(b), err
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// String returns "true" if the boolean is true, "false" otherwise.
func (b Element) String() string {
	if b {
		return "true"
	}
	return "false"
}

// Hash return the hash value.
func (b Element) Hash() hash.Value {
	return hash.Bool(bool(b))
}

// Parent returns the parent object for the boolean. This can safely be coerced to type Parent.
func (b Element) Parent() object.Parent {
	return bB
}
