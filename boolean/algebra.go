// Algebra defines the interface for a boolean algebra.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package boolean

import (
	"bitbucket.org/pcas/math/object"
)

// Algebra is the interface satisfied by a boolean algebra.
type Algebra interface {
	object.Parent
	True() object.Element                                           // True returns the value corresponding to true.
	False() object.Element                                          // False returns the value corresponding to false.
	IsTrue(x object.Element) (bool, error)                          // IsTrue returns true iff x is equal to the value corresponding to true.
	IsFalse(x object.Element) (bool, error)                         // IsFalse returns true iff x is equal to the value corresponding to false.
	And(x object.Element, y object.Element) (object.Element, error) // And returns the result x AND y.
	Or(x object.Element, y object.Element) (object.Element, error)  // Or returns the result x OR y.
	Not(x object.Element) (object.Element, error)                   // Not returns the negation NOT x.
}
