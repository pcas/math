// Abelianmonoid defines the abelian monoid interface and common functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package abelianmonoid

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
)

// Interface defines the interface that all abelian monoids must satisfy.
type Interface interface {
	object.Parent
	Zero() object.Element                                                                 // Zero returns the additive identity of the ring.
	IsZero(x object.Element) (bool, error)                                                // IsZero returns true iff x is the additive identity element of the ring.
	Add(x object.Element, y object.Element) (object.Element, error)                       // Add returns x + y.]
	ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) // ScalarMultiplyByInteger returns nx, where this is defined to be:
	//		  x + ... + x (n times) 	if n > 0;
	//		  and the zero element 		if n = 0.
	// If this is also an abelian group then
	//		  -x - ... - x (|n| times) 	if n < 0,
	// otherwise a negative value of n will return an error.
}

// sumer defines an optional interface that an abelian monoid can satisfy.
type sumer interface {
	Sum(S ...object.Element) (object.Element, error) // Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
}

// resultOrError describes an (object.Element, error) pair.
type resultOrError struct {
	Value object.Element // The value
	Err   error          // The error (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// sumSliceInternal passes the sum to the given channel.
func sumSliceInternal(G Interface, S []object.Element, c chan<- *resultOrError) {
	x, err := sumSlice(G, S)
	c <- &resultOrError{Value: x, Err: err}
}

// sumSlice returns the sum of the given slice.
func sumSlice(G Interface, S []object.Element) (object.Element, error) {
	if len(S) <= 10 {
		x := S[0]
		for _, y := range S[1:] {
			var err error
			if x, err = G.Add(x, y); err != nil {
				return nil, err
			}
		}
		return x, nil
	}
	// Perform a recursive split
	cLeft := make(chan *resultOrError)
	cRight := make(chan *resultOrError)
	div := len(S) / 2
	go sumSliceInternal(G, S[:div], cLeft)
	go sumSliceInternal(G, S[div:], cRight)
	// Recover the results
	x := <-cLeft
	y := <-cRight
	// Check for errors
	if x.Err != nil {
		return nil, x.Err
	} else if y.Err != nil {
		return nil, y.Err
	}
	// Compute the result
	return G.Add(x.Value, y.Value)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element. If G satisfies the interface:
//		type Sumer interface {
//			Sum(S ...object.Element) (object.Element, error) // Sum returns
//			   the sum of the elements in the slice S. The sum of the empty
//			   slice is the zero element.
//		}
// then G's Sum method will be called.
func Sum(G Interface, S ...object.Element) (object.Element, error) {
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return G.Zero(), nil
	}
	// If the ring has its own Sum method we call that
	if GG, ok := G.(sumer); ok {
		return GG.Sum(S...)
	}
	// Fast-track the length 1 case
	if len(S) == 1 {
		if !G.Contains(S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return S[0], nil
	}
	// Perform the addition
	return sumSlice(G, S)
}
