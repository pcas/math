// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package object

import (
	"bitbucket.org/pcastools/hash"
	"errors"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

// dummyParent is a dummy parent used for testing.
type dummyParent struct{}

// dummyElement is a dummy element used for testing.
type dummyElement int

/////////////////////////////////////////////////////////////////////////
// dummyParent functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the parent.
func (dummyParent) String() string {
	return "zZ(int)"
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (dummyParent) Contains(x Element) bool {
	_, ok := x.(dummyElement)
	return ok
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (dummyParent) ToElement(x Element) (Element, error) {
	y, ok := x.(dummyElement)
	if !ok {
		return nil, errors.New("Argument is not a dummyElement.")
	}
	return y, nil
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (dummyParent) AreEqual(x Element, y Element) (bool, error) {
	xx, ok := x.(dummyElement)
	if !ok {
		return false, errors.New("Argument 1 is not a dummyElement.")
	}
	yy, ok := y.(dummyElement)
	if !ok {
		return false, errors.New("Argument 2 is not a dummyElement.")
	}
	return xx == yy, nil
}

/////////////////////////////////////////////////////////////////////////
// dummyElement functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the element.
func (x dummyElement) String() string {
	return strconv.Itoa(int(x))
}

// Hash returns a hash value for the element.
func (x dummyElement) Hash() hash.Value {
	return hash.Value(x)
}

// Parent returns the parent of the element.
func (dummyElement) Parent() Parent {
	return dummyParent{}
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestSliceToElementSlice tests SliceToElementSlice.
func TestSliceToElementSlice(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of dummy objects
	S := make([]dummyElement, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(i))
	}
	// Convert the slice to a slice of object.Elements
	P := dummyParent{}
	if T, err := SliceToElementSlice(S); assert.NoError(err) && assert.Len(T, len(S)) {
		for i, x := range S {
			if ok, err := P.AreEqual(x, T[i]); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Now create a slice of non-object.Elements
	U := make([]int, 0, 10)
	for i := 0; i < cap(U); i++ {
		U = append(U, i)
	}
	// Attempting to convert this to a slice of object.Elements should error
	_, err := SliceToElementSlice(U)
	assert.Error(err)
	// Finally, attempting to convert a non-slice should error
	_, err = SliceToElementSlice(dummyElement(1))
	assert.Error(err)
	_, err = SliceToElementSlice(int(1))
	assert.Error(err)
}
