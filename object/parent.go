// Parent defines the general interface all parents must satisfy.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package object

import (
	"fmt"
)

// Containser is the interface satisfied by the Contains method.
type Containser interface {
	Contains(x Element) bool // Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
}

// Parent is the interface satisfied by an object that has child Elements.
type Parent interface {
	fmt.Stringer
	Containser
	ToElement(x Element) (Element, error)        // ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
	AreEqual(x Element, y Element) (bool, error) // AreEqual returns true iff x and y are both contained in the parent, and x = y.
}
