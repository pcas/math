module bitbucket.org/pcas/math

go 1.13

require (
	bitbucket.org/pcastools/errors v1.0.1
	bitbucket.org/pcastools/fatal v1.0.1
	bitbucket.org/pcastools/gobutil v1.0.1
	bitbucket.org/pcastools/hash v1.0.1
	bitbucket.org/pcastools/mathutil v1.0.2
	bitbucket.org/pcastools/rand v1.0.1
	bitbucket.org/pcastools/slice v1.0.1
	bitbucket.org/pcastools/sort v1.0.1
	bitbucket.org/pcastools/stringsbuilder v1.0.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
