// Cache maintains a cache of small int64-valued elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64number

// The minimum and maximum integer values stored in the cache.
const (
	cacheMin = -1 << 10
	cacheMax = 1 << 10
)

// A cache of *Element objects associated with the int64 values.
var cache []*Element

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// initElementCache initialises the cache of small-valued *Element objects.
func init() {
	// Create the integers cache
	cache = make([]*Element, 0, cacheMax-cacheMin+1)
	for n := int64(cacheMin); n <= cacheMax; n++ {
		cache = append(cache, &Element{n: n})
	}
}

/////////////////////////////////////////////////////////////////////////
// Cache access functions
/////////////////////////////////////////////////////////////////////////

// elementFromCache returns the *Element in the cache corresponding to the given value, or nil if the value is outside the cache range.
func elementFromCache(n int64) *Element {
	if n < cacheMin || n > cacheMax {
		return nil
	}
	return cache[int(n-cacheMin)]
}
