// Element defines int64-valued elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64number

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"math"
	"strconv"
)

// Element is an int64-valued element.
type Element struct {
	n int64 // The value
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Zero returns 0.
func Zero() *Element {
	return FromInt64(0)
}

// One returns 1.
func One() *Element {
	return FromInt64(1)
}

// FromInt returns a new int64-valued element.
func FromInt(n int) *Element {
	return FromInt64(int64(n))
}

// FromInt32 returns a new int64-valued element.
func FromInt32(n int32) *Element {
	return FromInt64(int64(n))
}

// FromInt64 returns a new int64-valued element.
func FromInt64(n int64) *Element {
	if x := elementFromCache(n); x != nil {
		return x
	}
	return &Element{n: n}
}

// FromUint64 returns a new int64-valued element, or an error if out of range.
func FromUint64(n uint64) (*Element, error) {
	if n > math.MaxInt64 {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(int64(n)), nil
}

// FromInteger returns a new int64-valued element, or an error if out of range.
func FromInteger(n *integer.Element) (*Element, error) {
	k, err := n.Int64()
	if err != nil {
		return nil, err
	}
	return FromInt64(k), nil
}

// FromString returns the integer represented by the string s.
func FromString(s string) (*Element, error) {
	n, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return nil, err
	}
	return FromInt64(n), nil
}

// Add returns the sum x + y, or an error if out of range.
func Add(x *Element, y *Element) (*Element, error) {
	if x.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x, nil
	}
	z, ok := mathutil.AddInt64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(z), nil
}

// Subtract returns the difference x - y, or an error if out of range.
func Subtract(x *Element, y *Element) (*Element, error) {
	if x.IsZero() {
		return Negate(y)
	} else if y.IsZero() {
		return x, nil
	}
	z, ok := mathutil.SubtractInt64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(z), nil
}

// SubtractInt64 returns the difference x - y, or an error if out of range.
func SubtractInt64(x *Element, y int64) (*Element, error) {
	if x.IsZero() {
		if y == math.MinInt64 {
			return nil, errors.OutOfRange.New()
		}
		return FromInt64(-y), nil
	} else if y == 0 {
		return x, nil
	}
	z, ok := mathutil.SubtractInt64(x.n, y)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(z), nil
}

// Negate returns the negation -x, or an error if out of range.
func Negate(x *Element) (*Element, error) {
	if x.IsZero() {
		return x, nil
	} else if x.n == math.MinInt64 {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(-x.n), nil
}

// Multiply returns the product x * y, or an error if out of range.
func Multiply(x *Element, y *Element) (*Element, error) {
	if x.IsZero() || y.IsOne() {
		return x, nil
	} else if y.IsZero() || x.IsOne() {
		return y, nil
	}
	z, ok := mathutil.MultiplyInt64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(z), nil
}

// MultiplyThenAdd returns x * y + z, or an error if out of range
func MultiplyThenAdd(x *Element, y *Element, z *Element) (*Element, error) {
	// Deal with the easy cases
	if z.IsZero() {
		return Multiply(x, y)
	} else if x.IsZero() || y.IsZero() {
		return z, nil
	}
	// First we try this as int64s
	if result, ok := mathutil.MultiplyInt64(x.n, y.n); ok {
		if result, ok = mathutil.AddInt64(result, z.n); ok {
			return FromInt64(result), nil
		}
	}
	// No good -- try as integers and see if we can convert back
	if result, err := integer.MultiplyThenAdd(integer.FromInt64(x.n), integer.FromInt64(y.n), integer.FromInt64(z.n)).Int64(); err != nil {
		return nil, err
	} else {
		return FromInt64(result), nil
	}
}

// MultiplyThenSubtract returns x * y - z, or an error if out of range
func MultiplyThenSubtract(x *Element, y *Element, z *Element) (*Element, error) {
	// Deal with the easy cases
	if z.IsZero() {
		return Multiply(x, y)
	} else if x.IsZero() || y.IsZero() {
		return Negate(z)
	}
	// First we try this as int64s
	if result, ok := mathutil.MultiplyInt64(x.n, y.n); ok {
		if result, ok = mathutil.SubtractInt64(result, z.n); ok {
			return FromInt64(result), nil
		}
	}
	// No good -- try as integers and see if we can convert back
	if result, err := integer.MultiplyThenSubtract(integer.FromInt64(x.n), integer.FromInt64(y.n), integer.FromInt64(z.n)).Int64(); err != nil {
		return nil, err
	} else {
		return FromInt64(result), nil
	}

}

// PowerInt64 returns x^k, or an error if the result is out of range.
func PowerInt64(x *Element, k int64) (*Element, error) {
	if k == 0 || x.IsOne() {
		return One(), nil
	} else if k == 1 || (x.IsZero() && k > 0) {
		return x, nil
	}
	z, err := integer.PowerInt64(x.ToInteger(), k)
	if err != nil {
		return nil, err
	}
	return FromInteger(z)
}

// Power returns x^k, or an error if the result is out of range.
func Power(x *Element, k *integer.Element) (*Element, error) {
	if k.IsZero() || x.IsOne() {
		return One(), nil
	} else if k.IsOne() || (x.IsZero() && k.IsPositive()) {
		return x, nil
	}
	z, err := integer.Power(x.ToInteger(), k)
	if err != nil {
		return nil, err
	}
	return FromInteger(z)
}

// AreEqual returns true iff x equals y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func Cmp(x *Element, y *Element) int {
	if x == y {
		return 0
	} else if x.IsZero() {
		return -y.Sign()
	} else if y.IsZero() {
		return x.Sign()
	} else if x.n > y.n {
		return 1
	} else if x.n < y.n {
		return -1
	}
	return 0
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff x is 0.
func (x *Element) IsZero() bool {
	return x == nil || x.n == 0
}

// IsOne returns true iff x is 1.
func (x *Element) IsOne() bool {
	return x != nil && x.n == 1
}

// IsEqualTo returns true iff x is equal to y.
func (x *Element) IsEqualTo(y *Element) bool {
	if x.IsZero() {
		return y.IsZero()
	} else if y.IsZero() {
		return false
	}
	return x.n == y.n
}

// IsEqualToInt64 returns true iff x is equal to y.
func (x *Element) IsEqualToInt64(y int64) bool {
	if x.IsZero() {
		return y == 0
	} else if y == 0 {
		return false
	}
	return x.n == y
}

// IsGreaterThan returns true iff x > y.
func (x *Element) IsGreaterThan(y *Element) bool {
	if x == y {
		return false
	} else if x.IsZero() {
		return y.IsNegative()
	} else if y.IsZero() {
		return x.IsPositive()
	}
	return x.n > y.n
}

// IsGreaterThanInt64 returns true iff x > y.
func (x *Element) IsGreaterThanInt64(y int64) bool {
	if x.IsZero() {
		return y < 0
	}
	return x.n > y
}

// IsGreaterThanOrEqualTo returns true iff x >= y.
func (x *Element) IsGreaterThanOrEqualTo(y *Element) bool {
	return !x.IsLessThan(y)
}

// IsGreaterThanOrEqualToInt64 returns true iff x >= y.
func (x *Element) IsGreaterThanOrEqualToInt64(y int64) bool {
	if x.IsZero() {
		return y <= 0
	}
	return x.n >= y
}

// IsLessThan returns true iff x < y.
func (x *Element) IsLessThan(y *Element) bool {
	if x == y {
		return false
	} else if x.IsZero() {
		return y.IsPositive()
	} else if y.IsZero() {
		return x.IsNegative()
	}
	return x.n < y.n
}

// IsLessThanInt64 returns true iff x < y.
func (x *Element) IsLessThanInt64(y int64) bool {
	if x.IsZero() {
		return y > 0
	}
	return x.n < y
}

// IsLessThanOrEqualTo returns true iff x <= y.
func (x *Element) IsLessThanOrEqualTo(y *Element) bool {
	return !x.IsGreaterThan(y)
}

// IsLessThanOrEqualToInt64 returns true iff x <= y.
func (x *Element) IsLessThanOrEqualToInt64(y int64) bool {
	if x.IsZero() {
		return y >= 0
	}
	return x.n <= y
}

// IsNegative returns true iff x < 0.
func (x *Element) IsNegative() bool {
	return x != nil && x.n < 0
}

// IsPositive returns true iff x > 0.
func (x *Element) IsPositive() bool {
	return x != nil && x.n > 0
}

// Sign returns the sign of x, i.e. -1 if x < 0; 0 if x == 0; +1 if x > 0.
func (x *Element) Sign() int {
	if x.IsZero() {
		return 0
	} else if x.n > 0 {
		return 1
	}
	return -1
}

// Abs returns the absolute value of x, or an error if out of range.
func (x *Element) Abs() (*Element, error) {
	if x.IsNegative() {
		return Negate(x)
	}
	return x, nil
}

// Increment returns x + 1, or an error if out of range.
func (x *Element) Increment() (*Element, error) {
	if x.IsZero() {
		return One(), nil
	} else if x.n == math.MaxInt64 {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(x.n + 1), nil
}

// Decrement returns x - 1, or an error if out of range.
func (x *Element) Decrement() (*Element, error) {
	if x.IsZero() {
		return FromInt64(-1), nil
	} else if x.n == math.MinInt64 {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(x.n - 1), nil
}

// ScalarMultiplyByInteger returns n * x, or an error if out of range.
func (x *Element) ScalarMultiplyByInteger(n *integer.Element) (*Element, error) {
	if x.IsZero() {
		return x, nil
	}
	k, err := n.Int64()
	if err != nil {
		return nil, err
	}
	return x.ScalarMultiplyByInt64(k)
}

// ScalarMultiplyByInt64 returns n * x, or an error if out of range.
func (x *Element) ScalarMultiplyByInt64(n int64) (*Element, error) {
	if x.IsZero() || n == 1 {
		return x, nil
	} else if n == 0 {
		return Zero(), nil
	} else if n == -1 {
		return Negate(x)
	}
	z, ok := mathutil.MultiplyInt64(n, x.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromInt64(z), nil
}

// Int64 returns the value as an int64.
func (x *Element) Int64() int64 {
	if x.IsZero() {
		return 0
	}
	return x.n
}

// Uint64 returns the value as a uint64, or an error if out of range.
func (x *Element) Uint64() (uint64, error) {
	if x.IsZero() {
		return 0, nil
	} else if x.n < 0 {
		return 0, errors.OutOfRange.New()
	}
	return uint64(x.n), nil
}

// ToInteger returns the value as an integer.
func (x *Element) ToInteger() *integer.Element {
	if x.IsZero() {
		return integer.Zero()
	}
	return integer.FromInt64(x.n)
}

// Parent returns the parent of the element.
func (x *Element) Parent() object.Parent {
	return zZ
}

// Hash returns a hash value for the element.
func (x *Element) Hash() hash.Value {
	if x.IsZero() {
		return 0
	}
	return hash.Int64(x.n)
}

// String returns a string representation of the element.
func (x *Element) String() string {
	if x.IsZero() {
		return "0"
	}
	return strconv.FormatInt(x.n, 10)
}
