// Tests parent.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64number

import (
	"bitbucket.org/pcas/math/integer"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Tests
/////////////////////////////////////////////////////////////////////////

// TestParent_String tests Parent's String method.
func TestParent_String(t *testing.T) {
	assert.Equal(t, Set().String(), defaultName)
}

// TestParent_Contains tests Parent's Contains method.
func TestParent_Contains(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	assert.True(R.Contains(R.Zero()))
	assert.True(R.Contains(R.One()))
	assert.True(R.Contains(integer.Zero()))
	assert.False(R.Contains(nil))
	assert.False(R.Contains(integer.FromInt64(math.MaxInt64).Increment()))
}

// TestParent_ToElement tests Parent's ToElement method.
func TestParent_ToElement(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	x := R.One()
	if y, err := R.ToElement(x); assert.NoError(err) {
		if ok, err := R.AreEqual(x, y); assert.NoError(err) {
			assert.True(ok)
		}
	}
	_, err := R.ToElement(nil)
	assert.Error(err)
}

// TestParent_AreEqual tests Parent's AreEqual method.
func TestParent_AreEqual(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// Create some elements
	zero := R.Zero()
	one := R.One()
	// Test equality
	if ok, err := R.AreEqual(zero, zero); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(zero, one); assert.NoError(err) {
		assert.False(ok)
	}
	_, err := R.AreEqual(zero, nil)
	assert.Error(err)
}

// TestParent_Zero tests Parent's Zero method.
func TestParent_Zero(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// Zero should equal 0
	var x *Element
	zero := R.Zero()
	if ok, err := R.AreEqual(zero, x); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(R.Zero(), Zero()); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(zero, FromInt64(0)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(zero, integer.Zero()); assert.NoError(err) {
		assert.True(ok)
	}
}

// TestParent_One tests Parent's One method.
func TestParent_One(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// One should equal 1
	one := R.One()
	if ok, err := R.AreEqual(one, One()); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(one, FromInt64(1)); assert.NoError(err) {
		assert.True(ok)
	}
	if ok, err := R.AreEqual(one, integer.One()); assert.NoError(err) {
		assert.True(ok)
	}
}

// TestParent_IsZero tests Parent's IsZero method.
func TestParent_IsZero(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// Only 0 should be zero
	for i := -10; i <= 10; i++ {
		if ok, err := R.IsZero(FromInt(i)); assert.NoError(err) {
			if i == 0 {
				assert.True(ok)
			} else {
				assert.False(ok)
			}
		}
	}
	_, err := R.IsZero(nil)
	assert.Error(err)
}

// TestParent_IsOne tests Parent's IsOne method.
func TestParent_IsOne(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// One 1 should be one
	for i := -10; i <= 10; i++ {
		if ok, err := R.IsOne(FromInt(i)); assert.NoError(err) {
			if i == 1 {
				assert.True(ok)
			} else {
				assert.False(ok)
			}
		}
	}
	_, err := R.IsOne(nil)
	assert.Error(err)
}

// TestParent_Add tests Parent's Add method.
func TestParent_Add(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests to perform, presented in the form {a, b, a + b}
	tests := [][]int{
		{0, 0, 0},
		{0, 1, 1},
		{1, 2, 3},
		{-1, 1, 0},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt(test[0])
		b := FromInt(test[1])
		// Test a + b
		if res, err := R.Add(a, b); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
		// Test b + a
		if res, err := R.Add(b, a); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Results out of range should return an error
	_, err := R.Add(FromInt64(math.MaxInt64), One())
	assert.Error(err)
	// Adding an element not in R should error
	_, err = R.Add(R.Zero(), nil)
	assert.Error(err)
	_, err = R.Add(nil, R.Zero())
	assert.Error(err)
}

// TestParent_Subtract tests Parent's Subtract method.
func TestParent_Subtract(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests to perform, presented in the form {a, b, a - b}
	tests := [][]int{
		{0, 0, 0},
		{0, 1, -1},
		{1, 0, 1},
		{1, 2, -1},
		{-1, 1, -2},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt(test[0])
		b := FromInt(test[1])
		if res, err := R.Subtract(a, b); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Results out of range should return an error
	_, err := R.Subtract(FromInt64(math.MinInt64), One())
	assert.Error(err)
	// Subtracting an element not in R should error
	_, err = R.Subtract(R.Zero(), nil)
	assert.Error(err)
	_, err = R.Subtract(nil, R.Zero())
	assert.Error(err)
}

// TestParent_Negate tests Parent's Negate method.
func TestParent_Negate(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// Run the tests
	for i := -10; i <= 10; i++ {
		if res, err := R.Negate(FromInt(i)); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(-i)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Results out of range should return an error
	_, err := R.Negate(FromInt64(math.MinInt64))
	assert.Error(err)
	// Negating an element not in R should error
	_, err = R.Negate(nil)
	assert.Error(err)
}

// TestParent_Multiply tests Parent's Multiply method.
func TestParent_Multiply(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests to perform, presented in the form {a, b, a * b}
	tests := [][]int{
		{0, 0, 0},
		{0, 1, 0},
		{1, 1, 1},
		{1, 2, 2},
		{-1, -1, 1},
		{5, 7, 35},
	}
	// Run the tests
	for _, test := range tests {
		a := FromInt(test[0])
		b := FromInt(test[1])
		// Test a * b
		if res, err := R.Multiply(a, b); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
		// Test b * a
		if res, err := R.Multiply(b, a); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Results out of range should return an error
	_, err := R.Multiply(FromInt64(math.MaxInt64/2), FromInt(3))
	assert.Error(err)
	// Multiplying by elements not in R should error
	_, err = R.Multiply(R.Zero(), nil)
	assert.Error(err)
	_, err = R.Multiply(nil, R.Zero())
	assert.Error(err)
}

// TestParent_Power tests Parent's Power method, and the PowerInt64 function.
func TestParent_Power(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests to perform, presented in the form {x, n, x^n}
	tests := [][]int64{
		{0, 0, 1},
		{1, 0, 1},
		{2, 0, 1},
		{-1, 0, 1},
		{0, 1, 0},
		{1, 1, 1},
		{2, 1, 2},
		{-1, 1, -1},
		{1, -1, 1},
		{-1, -1, -1},
		{1, -2, 1},
		{-1, -2, 1},
		{1, -3, 1},
		{-1, -3, -1},
		{2, 3, 8},
		{3, 4, 81},
		{5, 2, 25},
	}
	// Run the tests
	for _, test := range tests {
		if res, err := R.Power(FromInt64(test[0]), integer.FromInt64(test[1])); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromInt64(test[2])); assert.NoError(err) {
				assert.True(ok, "%d^%d=%s", test[0], test[1], res)
			}
		}
		if res, err := PowerInt64(FromInt64(test[0]), test[1]); assert.NoError(err) {
			assert.True(res.IsEqualToInt64(test[2]))
		}
	}
	// Results out of range should return an error
	_, err := R.Power(FromInt(2), integer.FromInt(-1))
	assert.Error(err)
	_, err = PowerInt64(FromInt(2), -1)
	assert.Error(err)
	_, err = R.Power(FromInt(2), integer.FromInt(100))
	assert.Error(err)
	_, err = PowerInt64(FromInt(2), 100)
	assert.Error(err)
	// Attempting to take the power of an element not in R should error
	_, err = R.Power(nil, integer.One())
	assert.Error(err)
}

// TestParent_ScalarMultiplyByInteger tests Parent's ScalarMultiplyByInteger method.
func TestParent_ScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// Run the tests
	for i := -10; i <= 10; i++ {
		x := FromInt(i)
		for j := -10; j <= 10; j++ {
			n := integer.FromInt(j)
			if y, err := R.ScalarMultiplyByInteger(n, x); assert.NoError(err) {
				if ok, err := R.AreEqual(y, FromInt(i*j)); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// Results out of range should return an error
	_, err := R.ScalarMultiplyByInteger(integer.FromInt64(math.MaxInt64/2), FromInt(3))
	assert.Error(err)
	_, err = R.ScalarMultiplyByInteger(integer.FromInt64(math.MaxInt64).Increment(), FromInt(7))
	assert.Error(err)
	// Attempting to multiply an element not in R should error
	_, err = R.ScalarMultiplyByInteger(integer.One(), nil)
	assert.Error(err)
}

// TestParent_Cmp tests Parent's Cmp method.
func TestParent_Cmp(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests in the form {a,b,sgn}
	tests := [][]int{
		{0, 0, 0},
		{0, 1, -1},
		{2, 3, -1},
		{7, -9, 1},
		{5, 5, 0},
		{cacheMax + 1, cacheMax + 1, 0},
	}
	// Perform the tests
	for _, test := range tests {
		a := FromInt(test[0])
		b := FromInt(test[1])
		// Test a > b
		if sgn, err := R.Cmp(a, b); assert.NoError(err) {
			assert.Equal(sgn, test[2])
		}
		// Test b > a
		if sgn, err := R.Cmp(b, a); assert.NoError(err) {
			assert.Equal(sgn, -test[2])
		}
	}
	// Elements from a different parent should return an error
	_, err := R.Cmp(nil, Zero())
	assert.Error(err)
	_, err = R.Cmp(Zero(), nil)
	assert.Error(err)
}
