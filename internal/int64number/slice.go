// Slice provides tools for working with slices of int64-valued elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64number

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
)

// Slice wraps an []*Element, implementing slice.Interface.
type Slice []*Element

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Slice) Entry(i int) object.Element {
	return S[i]
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Slice) Hash() hash.Value {
	v := hash.NewSequenceHasher()
	for _, s := range S {
		v.Add(s)
	}
	value := v.Hash()
	defer hash.ReuseSequenceHasher(v)
	return value
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SliceFromIntSlice returns a slice of Elements corresponding to the integers in S.
func SliceFromIntSlice(S []int) []*Element {
	Q := make([]*Element, 0, len(S))
	for _, s := range S {
		Q = append(Q, FromInt(s))
	}
	return Q
}

// SliceFromInt64Slice returns a slice of Elements corresponding to the integers in S.
func SliceFromInt64Slice(S []int64) []*Element {
	Q := make([]*Element, 0, len(S))
	for _, s := range S {
		Q = append(Q, FromInt64(s))
	}
	return Q
}

// SliceFromIntegerSlice returns a slice of Elements corresponding to the integers in S.
func SliceFromIntegerSlice(S []*integer.Element) ([]*Element, error) {
	Q := make([]*Element, 0, len(S))
	for _, s := range S {
		x, err := FromInteger(s)
		if err != nil {
			return nil, err
		}
		Q = append(Q, x)
	}
	return Q, nil
}

// CopySlice returns a copy of the slice S. The capacity will be preserved.
func CopySlice(S []*Element) []*Element {
	T := make([]*Element, len(S), cap(S))
	copy(T, S)
	return T
}
