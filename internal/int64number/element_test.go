// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64number

import (
	"bitbucket.org/pcas/math/integer"
	"github.com/stretchr/testify/assert"
	"math"
	"strconv"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// assertAreEqual asserts that i == i in many different ways.
func assertAreEqual(assert *assert.Assertions, i int) {
	a := FromInt(i)
	assert.True(a.IsEqualTo(FromInt(i)))
	assert.True(a.IsEqualTo(FromInt32(int32(i))))
	assert.True(a.IsEqualTo(FromInt64(int64(i))))
	if b, err := FromInteger(integer.FromInt(i)); assert.NoError(err) {
		assert.True(a.IsEqualTo(b))
	}
	if i >= 0 {
		if b, err := FromUint64(uint64(i)); assert.NoError(err) {
			assert.True(a.IsEqualTo(b))
		}
	}
	assert.True(a.IsEqualToInt64(int64(i)))
	assert.True(AreEqual(a, FromInt(i)))
	assert.False(a.IsEqualTo(FromInt(i + 1)))
	assert.False(a.IsEqualToInt64(int64(i + 1)))
	assert.False(AreEqual(a, FromInt(i+1)))
}

/////////////////////////////////////////////////////////////////////////
// Tests
/////////////////////////////////////////////////////////////////////////

// TestElement_Creation tests that FromInteger and FromUint64 error when expected.
func TestElement_Creation(t *testing.T) {
	assert := assert.New(t)
	// Test around the range boundaries
	_, err := FromInteger(integer.FromInt64(math.MaxInt64))
	assert.NoError(err)
	_, err = FromInteger(integer.FromInt64(math.MaxInt64).Increment())
	assert.Error(err)
	_, err = FromInteger(integer.FromInt64(math.MinInt64))
	assert.NoError(err)
	_, err = FromInteger(integer.FromInt64(math.MinInt64).Decrement())
	assert.Error(err)
	_, err = FromUint64(math.MaxInt64)
	assert.NoError(err)
	_, err = FromUint64(math.MaxInt64 + 1)
	assert.Error(err)
}

// TestElement_IsEqualTo tests Element's IsEqualTo and IsEqualToInt64 methods, and the AreEqual function.
func TestElement_IsEqualTo(t *testing.T) {
	assert := assert.New(t)
	// Run the tests
	for i := 0; i <= 20; i++ {
		assertAreEqual(assert, i-10)
		assertAreEqual(assert, i+cacheMax)
	}
}

// TestElement_String tests Element's String and FromString method.
func TestElement_String(t *testing.T) {
	assert := assert.New(t)
	// String should return the integer as a string
	for i := -10; i <= 10; i++ {
		x := FromInt(i)
		assert.Equal(x.String(), strconv.Itoa(i))
		if y, err := FromString(x.String()); assert.NoError(err) {
			assert.True(x.IsEqualTo(y))
		}
	}
	for i := 1; i <= 10; i++ {
		assert.Equal(FromInt(cacheMax+i).String(), strconv.Itoa(cacheMax+i))
	}
	// Nonsense strings should return an error
	_, err := FromString(" - + 10 ")
	assert.Error(err)
}

// TestElement_Hash tests Element's Hash method.
func TestElement_Hash(t *testing.T) {
	// Equal objects should have equal hashes
	for i := -10; i <= 10; i++ {
		assert.Equal(t, FromInt(i).Hash(), FromInt(i).Hash())
	}
	for i := 1; i <= 10; i++ {
		assert.Equal(t, FromInt(cacheMax+i).Hash(), FromInt(cacheMax+i).Hash())
	}
}

// TestElement_Parent tests Element's Parent method.
func TestElement_Parent(t *testing.T) {
	// Check that an element of R returns R as the parent
	for i := -10; i <= 10; i++ {
		assert.Equal(t, FromInt(i).Parent(), Set())
	}
	for i := 1; i <= 10; i++ {
		assert.Equal(t, FromInt(cacheMax+i).Parent(), Set())
	}
}

// TestElement_IsZero tests Element's IsZero method.
func TestElement_IsZero(t *testing.T) {
	// Only 0 should be zero
	for i := -10; i <= 10; i++ {
		if i == 0 {
			assert.True(t, FromInt(i).IsZero())
		} else {
			assert.False(t, FromInt(i).IsZero())
		}
	}
}

// TestElement_IsOne tests Element's IsOne method.
func TestElement_IsOne(t *testing.T) {
	// Only 1 should be one
	for i := -10; i <= 10; i++ {
		if i == 1 {
			assert.True(t, FromInt(i).IsOne())
		} else {
			assert.False(t, FromInt(i).IsOne())
		}
	}
}

// TestElement_Int64 tests Element's Int64, Uint64, and ToInteger methods.
func TestElement_Int64(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(FromInt(0).Int64(), int64(0))
	assert.Equal(FromInt(1).Int64(), int64(1))
	assert.Equal(FromInt(-10).Int64(), int64(-10))
	if n, err := FromInt(0).Uint64(); assert.NoError(err) {
		assert.Equal(n, uint64(0))
	}
	if n, err := FromInt(1).Uint64(); assert.NoError(err) {
		assert.Equal(n, uint64(1))
	}
	assert.True(FromInt(0).ToInteger().IsZero())
	assert.True(FromInt(1).ToInteger().IsOne())
	assert.True(FromInt(-10).ToInteger().IsEqualToInt64(-10))
	// A negative value should generate an error if we try to convert it to a
	// uint64
	_, err := FromInt(-10).Uint64()
	assert.Error(err)
}

// TestComparison tests IsGreaterThan, IsGreaterThanOrEqualTo, IsLessThan, IsLessThanOrEqualTo, and Sign
func TestComparison(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	zero := FromInt(0)
	four := FromInt(4)
	mthree := FromInt(-3)
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThan
	//////////////////////////////////////////////////////////////////////
	assert.False(zero.IsGreaterThan(zero))
	assert.False(zero.IsGreaterThan(four))
	assert.True(zero.IsGreaterThan(mthree))
	assert.True(four.IsGreaterThan(zero))
	assert.False(four.IsGreaterThan(four))
	assert.True(four.IsGreaterThan(mthree))
	assert.False(mthree.IsGreaterThan(zero))
	assert.False(mthree.IsGreaterThan(four))
	assert.False(mthree.IsGreaterThan(mthree))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsGreaterThanOrEqualTo(zero))
	assert.False(zero.IsGreaterThanOrEqualTo(four))
	assert.True(zero.IsGreaterThanOrEqualTo(mthree))
	assert.True(four.IsGreaterThanOrEqualTo(zero))
	assert.True(four.IsGreaterThanOrEqualTo(four))
	assert.True(four.IsGreaterThanOrEqualTo(mthree))
	assert.False(mthree.IsGreaterThanOrEqualTo(zero))
	assert.False(mthree.IsGreaterThanOrEqualTo(four))
	assert.True(mthree.IsGreaterThanOrEqualTo(mthree))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThan
	//////////////////////////////////////////////////////////////////////
	assert.False(zero.IsLessThan(zero))
	assert.True(zero.IsLessThan(four))
	assert.False(zero.IsLessThan(mthree))
	assert.False(four.IsLessThan(zero))
	assert.False(four.IsLessThan(four))
	assert.False(four.IsLessThan(mthree))
	assert.True(mthree.IsLessThan(zero))
	assert.True(mthree.IsLessThan(four))
	assert.False(mthree.IsLessThan(mthree))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsLessThanOrEqualTo(zero))
	assert.True(zero.IsLessThanOrEqualTo(four))
	assert.False(zero.IsLessThanOrEqualTo(mthree))
	assert.False(four.IsLessThanOrEqualTo(zero))
	assert.True(four.IsLessThanOrEqualTo(four))
	assert.False(four.IsLessThanOrEqualTo(mthree))
	assert.True(mthree.IsLessThanOrEqualTo(zero))
	assert.True(mthree.IsLessThanOrEqualTo(four))
	assert.True(mthree.IsLessThanOrEqualTo(mthree))
	//////////////////////////////////////////////////////////////////////
	// tests for Sign
	//////////////////////////////////////////////////////////////////////
	assert.Equal(zero.Sign(), 0)
	assert.Equal(four.Sign(), 1)
	assert.Equal(mthree.Sign(), -1)
}

// TestAbs tests Abs
func TestAbs(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	zero := FromInt(0)
	four := FromInt(4)
	mfour := FromInt(-4)
	big := FromInt64(math.MaxInt64)
	mbig := FromInt64(math.MinInt64)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if x, err := zero.Abs(); assert.NoError(err) {
		assert.True(x.IsEqualTo(zero))
	}
	if x, err := four.Abs(); assert.NoError(err) {
		assert.True(x.IsEqualTo(four))
	}
	if x, err := mfour.Abs(); assert.NoError(err) {
		assert.True(x.IsEqualTo(four))
	}
	if x, err := big.Abs(); assert.NoError(err) {
		assert.True(x.IsEqualTo(big))
	}
	_, err := mbig.Abs()
	assert.Error(err)
}

// TestIncrementDecrement tests Increment and Decrement
func TestIncrementDecrement(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	big := FromInt64(math.MaxInt64)
	mbig := FromInt64(math.MinInt64)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if x, err := FromInt(0).Increment(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromInt(1)))
	}
	if x, err := FromInt(1).Decrement(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromInt(0)))
	}
	if x, err := FromInt(-1).Increment(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromInt(0)))
	}
	if x, err := FromInt(0).Decrement(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromInt(-1)))
	}
	_, err := big.Increment()
	assert.Error(err)
	_, err = big.Decrement()
	assert.NoError(err)
	_, err = mbig.Increment()
	assert.NoError(err)
	_, err = mbig.Decrement()
	assert.Error(err)
}

// TestMultiplyThen tests MultiplyThenAdd and MultiplyThenSubtract
func TestMultiplyThen(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	tests := [][]int{
		[]int{0, 2, 7},
		[]int{2, 0, 7},
		[]int{5, 2, 0},
		[]int{0, -2, 7},
		[]int{2, 0, -7},
		[]int{-5, 2, 0},
		[]int{0, -2, -7},
		[]int{-2, 0, -7},
		[]int{-5, -2, 0},
		[]int{93, 2652, 71},
		[]int{-93, 2652, 71},
		[]int{93, -2652, 71},
		[]int{93, 2652, -71},
		[]int{-93, -2652, 71},
		[]int{-93, 2652, -71},
		[]int{93, -2652, -71},
		[]int{-93, -2652, -71},
	}
	//////////////////////////////////////////////////////////////////////
	// run the tests
	//////////////////////////////////////////////////////////////////////
	for _, T := range tests {
		if x, err := MultiplyThenAdd(FromInt(T[0]), FromInt(T[1]), FromInt(T[2])); assert.NoError(err) {
			assert.True(x.IsEqualTo(FromInt(T[0]*T[1] + T[2])))
		}
		if x, err := MultiplyThenSubtract(FromInt(T[0]), FromInt(T[1]), FromInt(T[2])); assert.NoError(err) {
			assert.True(x.IsEqualTo(FromInt(T[0]*T[1] - T[2])))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test overflow near the boundary of the int64 range
	//////////////////////////////////////////////////////////////////////
	// MultiplyThenAdd
	_, err := MultiplyThenAdd(FromInt(2), FromInt64(math.MaxInt64), FromInt(0))
	assert.Error(err)
	_, err = MultiplyThenAdd(FromInt(1), FromInt64(math.MaxInt64), FromInt(1))
	assert.Error(err)
	_, err = MultiplyThenAdd(FromInt(2), FromInt64(math.MaxInt64), FromInt64(math.MinInt64))
	assert.NoError(err)
	_, err = MultiplyThenAdd(FromInt(1), FromInt64(math.MaxInt64), FromInt(-1))
	assert.NoError(err)
	_, err = MultiplyThenAdd(FromInt(-1), FromInt64(math.MinInt64), FromInt(0))
	assert.Error(err)
	_, err = MultiplyThenAdd(FromInt(1), FromInt64(math.MinInt64), FromInt(-1))
	assert.Error(err)
	_, err = MultiplyThenAdd(FromInt(-1), FromInt64(math.MinInt64), FromInt(-1))
	assert.NoError(err)
	// MultiplyThenSubtract
	_, err = MultiplyThenSubtract(FromInt(2), FromInt64(math.MaxInt64), FromInt(0))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromInt(1), FromInt64(math.MaxInt64), FromInt(-1))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromInt(2), FromInt64(math.MaxInt64), FromInt64(math.MaxInt64))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromInt(1), FromInt64(math.MaxInt64), FromInt(1))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromInt(-1), FromInt64(math.MinInt64), FromInt(0))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromInt(-1), FromInt64(math.MinInt64), FromInt(1))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromInt(1), FromInt64(math.MinInt64), FromInt(1))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromInt(1), FromInt64(math.MinInt64), FromInt(-1))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromInt(2), FromInt64(math.MinInt64), FromInt64(math.MinInt64))
	assert.NoError(err)
}
