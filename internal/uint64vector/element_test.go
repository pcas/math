// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64vector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcastools/hash"
	"math"
	"testing"
)

//////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////

// TestFromIntegerSlice tests FromIntegerSlice
func TestFromIntegerSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// test that we get out of range errors when we should, and not when
	// we shouldn't
	//////////////////////////////////////////////////////////////////////
	integerSlice := make([]*integer.Element, 0, 3)
	// biggest possible entries
	for i := 0; i < 3; i++ {
		integerSlice = append(integerSlice, integer.FromUint64(math.MaxUint64))
	}
	_, err = FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	// a vector with an entry that is too large
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if i == j {
				integerSlice[j] = integer.FromUint64(math.MaxUint64).Increment()
			} else {
				integerSlice[j] = integer.FromUint64(math.MaxUint64)
			}
		}
		_, err = FromIntegerSlice(ZZ3, integerSlice)
		assert.Error(err)
	}
	// most negative possible entries
	for i := 0; i < 3; i++ {
		integerSlice[i] = integer.Zero()
	}
	_, err = FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	// a vector with an entry that is too negative
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if i == j {
				integerSlice[j] = integer.FromInt(-1)
			} else {
				integerSlice[j] = integer.Zero()
			}
		}
		_, err = FromIntegerSlice(ZZ3, integerSlice)
		assert.Error(err)
	}
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		integerSlice[i] = integer.FromInt(100 + i)
	}
	_, err = FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail, because the dimension doesn't match the
	// length of the slice
	//////////////////////////////////////////////////////////////////////
	_, err = FromIntegerSlice(ZZ2, integerSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test zero input
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		integerSlice[i] = integer.Zero()
	}
	zeroZeroZero, err := FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	assert.Equal(zeroZeroZero, Zero(ZZ3))
	//////////////////////////////////////////////////////////////////////
	// test the nil case
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []*integer.Element
	_, err = FromIntegerSlice(nilLattice, integerSlice)
	assert.Error(err)
	_, err = FromIntegerSlice(ZZ3, nilSlice)
	assert.Error(err)
	v, err := FromIntegerSlice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v)
	assert.IsZero(v)
}

// TestFromString tests FromString
func TestFromString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// a slice to test against
	//////////////////////////////////////////////////////////////////////
	integerSlice := make([]*integer.Element, 0, 3)
	for i := 100; i < 103; i++ {
		integerSlice = append(integerSlice, integer.FromInt(i))
	}
	v1, err := FromIntegerSlice(ZZ3, integerSlice)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed and give v1
	//////////////////////////////////////////////////////////////////////
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should also succeed and give v1
	//////////////////////////////////////////////////////////////////////
	v3, err := FromString(ZZ3, "[100,\t         101,102\n\n\n             ]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v3)
	//////////////////////////////////////////////////////////////////////
	// this should fail for dimension reasons
	//////////////////////////////////////////////////////////////////////
	_, err = FromString(ZZ2, "[100, 101, 102]")
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail, as the input is garbage
	//////////////////////////////////////////////////////////////////////
	_, err = FromString(ZZ2, "[100, 101, a little turnip of my own 102]")
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilString string
	_, err = FromString(nilLattice, "[100, 101, 102]")
	assert.Error(err)
	_, err = FromString(ZZ2, nilString)
	assert.Error(err)
	v4, err := FromString(nilLattice, nilString)
	assert.NoError(err)
	assert.Nil(v4)
	v5, err := FromString(nilLattice, "\t\t      \t      \n\n      \n         ")
	assert.NoError(err)
	assert.Nil(v5)
	_, err = FromString(ZZ3, "\t\t      \t      \n\n      \n         ")
	assert.Error(err)
}

// TestFromIntSlice tests FromIntSlice
func TestFromIntSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int, 0, 3)
	for i := 100; i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromIntSlice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromIntSlice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromIntSlice(ZZ2, make([]int, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int
	_, err = FromIntSlice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromIntSlice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromIntSlice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromIntSlice(nilLattice, make([]int, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromInt32Slice tests FromInt32Slice
func TestFromInt32Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int32, 0, 3)
	for i := int32(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromInt32Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt32Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail because of negative entries
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt32Slice(ZZ2, []int32{1, -1})
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromInt32Slice(ZZ2, make([]int32, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int32
	_, err = FromInt32Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromInt32Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromInt32Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromInt32Slice(nilLattice, make([]int32, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromUint64Slice tests FromUint64Slice
func TestFromUint64Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]uint64, 0, 3)
	for i := uint64(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromUint64Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromUint64Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromUint64Slice(ZZ2, make([]uint64, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []uint64
	_, err = FromUint64Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromUint64Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromUint64Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromUint64Slice(nilLattice, make([]uint64, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestFromInt64Slice tests FromInt64Slice
func TestFromInt64Slice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	intSlice := make([]int64, 0, 3)
	for i := int64(100); i < 103; i++ {
		intSlice = append(intSlice, i)
	}
	v1, err := FromInt64Slice(ZZ3, intSlice)
	assert.NoError(err)
	v2, err := FromString(ZZ3, "[100, 101, 102]")
	assert.NoError(err)
	assert.AreEqual(ZZ3, v1, v2)
	//////////////////////////////////////////////////////////////////////
	// this should fail because the dimensions don't match
	//////////////////////////////////////////////////////////////////////
	_, err = FromInt64Slice(ZZ2, intSlice)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the zero slice should be zero
	//////////////////////////////////////////////////////////////////////
	if v, err := FromInt64Slice(ZZ2, make([]int64, 2)); assert.NoError(err) {
		assert.Equal(Zero(ZZ2), v)
	}
	//////////////////////////////////////////////////////////////////////
	// make sure that entries that are negative cause errors
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if i == j {
				intSlice[j] = -1
			} else {
				intSlice[j] = 7
			}
		}
		_, err = FromInt64Slice(ZZ2, intSlice)
		assert.Error(err)
	}
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	var nilLattice *Parent
	var nilSlice []int64
	_, err = FromInt64Slice(ZZ2, nilSlice)
	assert.Error(err)
	_, err = FromInt64Slice(nilLattice, intSlice)
	assert.Error(err)
	v3, err := FromInt64Slice(nilLattice, nilSlice)
	assert.NoError(err)
	assert.Nil(v3)
	v4, err := FromInt64Slice(nilLattice, make([]int64, 0))
	assert.NoError(err)
	assert.Nil(v4)
}

// TestConstantVector tests ConstantVector
func TestConstantVector(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// the constant zero vector should be zero
	//////////////////////////////////////////////////////////////////////
	zeroZeroZero, err := ConstantVector(ZZ3, integer.Zero())
	assert.NoError(err)
	assert.Equal(zeroZeroZero, Zero(ZZ3))
	v1, err := ConstantVector(nilLattice, integer.FromInt(7))
	assert.NoError(err)
	assert.IsZero(v1)
	//////////////////////////////////////////////////////////////////////
	// this should succeed
	//////////////////////////////////////////////////////////////////////
	_, err = ConstantVector(ZZ3, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail, as the constant is too large
	//////////////////////////////////////////////////////////////////////
	_, err = ConstantVector(ZZ3, integer.FromUint64(math.MaxUint64).Increment())
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// this should fail, as the constant is negative
	//////////////////////////////////////////////////////////////////////
	_, err = ConstantVector(ZZ3, integer.FromInt(-1))
	assert.Error(err)
}

// TestSumElement tests Sum in element.go
func TestSumParent(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero().(*Element)
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ2, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	// empty sum
	if v, err := Sum(ZZ2); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
	}
	// one-element sum
	if v, err := Sum(ZZ2, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v12)
	}
	// two-element sum
	if v, err := Sum(ZZ2, v12, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	// bigger sum
	if v, err := Sum(ZZ2, v12, v12, v24, zero, v12); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{5, 10})
	}
	// test overflow near the boundary of the uint64 range
	_, err = Sum(ZZ2, zero, vbig, zero)
	assert.NoError(err)
	_, err = Sum(ZZ2, v12, vbig, zero)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = Sum(ZZ2, v111)
	assert.Error(err)
	// two-element sum
	_, err = Sum(ZZ2, v111, v24)
	assert.Error(err)
	_, err = Sum(ZZ2, v24, v111)
	assert.Error(err)
	// bigger sum
	_, err = Sum(ZZ2, v12, v111, v24, zero, v24)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the nil case
	//////////////////////////////////////////////////////////////////////
	nilZero := nilLattice.Zero().(*Element)
	// empty sum
	if v, err := Sum(nilLattice); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// one-element sum
	if v, err := Sum(nilLattice, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// two-element sum
	if v, err := Sum(nilLattice, nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// bigger sum
	if v, err := Sum(nilLattice, nilZero, nilZero, nilZero, nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = Sum(ZZ2, v111)
	assert.Error(err)
	_, err = Sum(nilLattice, v111)
	assert.Error(err)
	// two-element sum
	_, err = Sum(ZZ2, v111, v24)
	assert.Error(err)
	_, err = Sum(ZZ2, v24, v111)
	assert.Error(err)
	_, err = Sum(nilLattice, v111, nilZero)
	assert.Error(err)
	_, err = Sum(nilLattice, nilZero, v111)
	assert.Error(err)
	// bigger sum
	_, err = Sum(ZZ2, v24, v111, v24, zero, v24)
	assert.Error(err)
	_, err = Sum(nilLattice, nilZero, nilZero, nilZero, zero, nilZero)
	assert.Error(err)
}

// TestBadParents tests that errors occur in various functions when they are passed arguments that lie in incompatible or wrong parents
func TestBadParents(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// the tests for Cmp
	//////////////////////////////////////////////////////////////////////
	_, err = Cmp(v24, v111)
	assert.Error(err)
	_, err = Cmp(v111, v24)
	assert.Error(err)
	_, err = Cmp(v24, nilVector)
	assert.Error(err)
	_, err = Cmp(nilVector, v24)
	assert.Error(err)
	_, err = Cmp(v24, nilZero)
	assert.Error(err)
	_, err = Cmp(nilZero, v24)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the tests for Add
	//////////////////////////////////////////////////////////////////////
	_, err = Add(v24, v111)
	assert.Error(err)
	_, err = Add(v111, v24)
	assert.Error(err)
	_, err = Add(v24, nilVector)
	assert.Error(err)
	_, err = Add(nilVector, v24)
	assert.Error(err)
	_, err = Add(v24, nilZero)
	assert.Error(err)
	_, err = Add(nilZero, v24)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the tests for Subtract
	//////////////////////////////////////////////////////////////////////
	_, err = Subtract(v24, v111)
	assert.Error(err)
	_, err = Subtract(v111, v24)
	assert.Error(err)
	_, err = Subtract(v24, nilVector)
	assert.Error(err)
	_, err = Subtract(nilVector, v24)
	assert.Error(err)
	_, err = Subtract(v24, nilZero)
	assert.Error(err)
	_, err = Subtract(nilZero, v24)
	assert.Error(err)
}

// TestEquality tests AreEqual
func TestEquality(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(AreEqual(v24, v24))
	assert.False(AreEqual(v111, v24))
	assert.False(AreEqual(v24, v111))
	assert.False(AreEqual(nilVector, v24))
	assert.False(AreEqual(v24, nilVector))
	assert.False(AreEqual(nilZero, v24))
	assert.False(AreEqual(v24, nilZero))
	assert.False(nilZero == nilVector)
	assert.True(AreEqual(nilZero, nilVector))
	assert.True(AreEqual(nilVector, nilZero))
}

// TestDotProduct tests DotProduct
func TestDotProduct(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	L3, err := NewLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	v511, err := FromIntSlice(L3, []int{5, 1, 1})
	assert.NoError(err)
	vbig, err := ConstantVector(L3, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// basic tests
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v24, Zero(ZZ2)); assert.NoError(err) {
		assert.IsZero(x)
	}
	if x, err := DotProduct(v24, v12); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt(10)))
	}
	//////////////////////////////////////////////////////////////////////
	// dot products of elements of different lattices are fine, provided
	// that they have the same dimension
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v111, v511); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.FromInt(7)))
	}
	_, err = DotProduct(v111, v24)
	assert.Error(err)
	_, err = DotProduct(v24, v111)
	assert.Error(err)
	_, err = DotProduct(nilVector, v24)
	assert.Error(err)
	_, err = DotProduct(v24, nilVector)
	assert.Error(err)
	_, err = DotProduct(nilZero, v24)
	assert.Error(err)
	_, err = DotProduct(v24, nilZero)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// large values for dot products are fine
	//////////////////////////////////////////////////////////////////////
	if x, err := DotProduct(v111, vbig); assert.NoError(err) {
		assert.True(x.IsEqualTo(integer.Multiply(integer.FromInt(3), integer.FromUint64(math.MaxUint64))))
	}
}

// TestScalarMultiplyByThen tests ScalarMultiplyByIntegerThenAdd and ScalarMultiplyByIntegerThenSubtract
func TestScalarMultiplyByThen(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v16, err := FromIntSlice(ZZ2, []int{1, 6})
	assert.NoError(err)
	v25, err := FromIntSlice(ZZ2, []int{2, 5})
	assert.NoError(err)
	v10, err := FromIntSlice(ZZ2, []int{1, 0})
	assert.NoError(err)
	v20, err := FromIntSlice(ZZ2, []int{2, 0})
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ2, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for ScalarMultiplyByIntegerThenAdd
	//////////////////////////////////////////////////////////////////////
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), v16, Zero(ZZ2)); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v16)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.FromInt(4), v16, v25); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{6, 29})
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.FromInt(-4), Zero(ZZ2), v25); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v25)
	}
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(-4), v16, v25)
	assert.Error(err)
	// test mismatched parents
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v25, v111)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v111, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), nilVector, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v16, nilVector)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), nilZero, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(7), v16, nilZero)
	assert.Error(err)
	// test nil cases
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilVector, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilZero, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenAdd(integer.One(), nilVector, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// test overflow near the boundary of the range
	if biggish, err := vbig.DecrementEntry(0); assert.NoError(err) {
		_, err2 := ScalarMultiplyByIntegerThenAdd(integer.One(), biggish, v10)
		assert.NoError(err2)
	}
	_, err = ScalarMultiplyByIntegerThenAdd(integer.One(), vbig, v25)
	assert.Error(err)
	// if the scalar is too big, and the multiplicand is non-zero we just fail
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromUint64(math.MaxUint64).Increment(), v10, v25)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromUint64(math.MaxUint64).Increment(), Zero(ZZ2), v25)
	assert.NoError(err)
	// naively, this would overflow at an intermediate step in the calculation, but actually it works because it falls back to using integers under the hood
	_, err = ScalarMultiplyByIntegerThenAdd(integer.FromInt(-1), v25, vbig)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests for ScalarMultiplyByIntegerThenSubtract
	//////////////////////////////////////////////////////////////////////
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), v16, Zero(ZZ2)); assert.NoError(err) {
		assert.AreEqual(v.Parent(), v, v16)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.FromInt(4), v16, v25); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{2, 19})
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.FromInt(-4), Zero(ZZ2), Zero(ZZ2)); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, Zero(ZZ2))
	}
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(1), v16, v25)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(-4), v16, v25)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(-4), Zero(ZZ2), v25)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(-4), v16, Zero(ZZ2))
	assert.Error(err)
	// test mismatched parents
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v25, v111)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v111, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), nilVector, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v16, nilVector)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), nilZero, v16)
	assert.Error(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(7), v16, nilZero)
	assert.Error(err)
	// test nil cases
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilVector, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilZero, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	if v, err := ScalarMultiplyByIntegerThenSubtract(integer.One(), nilVector, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// test overflow near the boundary of the range
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.One(), vbig, v25)
	assert.NoError(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.One(), Zero(ZZ2), v16)
	assert.Error(err)
	// naively, this would overflow at an intermediate step in the calculation, but actually it works because it falls back to using integers under the hood
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromUint64(math.MaxUint64).Increment(), v10, v20)
	assert.NoError(err)
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromInt(2), vbig, vbig)
	assert.NoError(err)
	// for genuine overflows, we fail
	_, err = ScalarMultiplyByIntegerThenSubtract(integer.FromUint64(math.MaxUint64).Increment(), Zero(ZZ2), v25)
	assert.Error(err)
}

// TestEntry tests Entry, EntryOrPanic, EntryAsUint64, and EntryAsUint64OrPanic
func TestEntry(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests that succeed
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < v123.Dimension(); i++ {
		if v, err := v123.Entry(i); assert.NoError(err) {
			assert.True(v.IsEqualToUint64(uint64(i + 1)))
		}
		assert.True(v123.EntryOrPanic(i).IsEqualToUint64(uint64(i + 1)))
		if v, err := v123.EntryAsUint64(i); assert.NoError(err) {
			assert.Equal(v, uint64(i+1))
		}
		assert.Equal(v123.EntryAsUint64OrPanic(i), uint64(i+1))
	}
	//////////////////////////////////////////////////////////////////////
	// tests that fail or panic
	//////////////////////////////////////////////////////////////////////
	_, err = v123.Entry(3)
	assert.Error(err)
	assert.Panics(func() { v123.EntryOrPanic(3) })
	_, err = v123.EntryAsUint64(3)
	assert.Error(err)
	assert.Panics(func() { v123.EntryAsUint64OrPanic(3) })
	_, err = v123.Entry(-1)
	assert.Error(err)
	assert.Panics(func() { v123.EntryOrPanic(-1) })
	_, err = v123.EntryAsUint64(-1)
	assert.Error(err)
	assert.Panics(func() { v123.EntryAsUint64OrPanic(-1) })
	_, err = nilVector.Entry(0)
	assert.Error(err)
	assert.Panics(func() { nilVector.EntryOrPanic(0) })
	_, err = nilZero.Entry(0)
	assert.Error(err)
	assert.Panics(func() { nilZero.EntryOrPanic(0) })
}

// TestIncrementDecrement tests IncrementEntry and DecrementEntry
func TestIncrementDecrement(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v555, err := FromIntSlice(ZZ3, []int{5, 5, 5})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ3, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests that should pass
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		if v, err := v555.IncrementEntry(i); assert.NoError(err) {
			for j := 0; j < 3; j++ {
				if i == j {
					assert.True(v.EntryAsUint64OrPanic(j) == 6)
				} else {
					assert.True(v.EntryAsUint64OrPanic(j) == 5)
				}
			}
		}
		if v, err := v555.DecrementEntry(i); assert.NoError(err) {
			for j := 0; j < 3; j++ {
				if i == j {
					assert.True(v.EntryAsUint64OrPanic(j) == 4)
				} else {
					assert.True(v.EntryAsUint64OrPanic(j) == 5)
				}
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests that should fail
	//////////////////////////////////////////////////////////////////////
	_, err = v555.IncrementEntry(-1)
	assert.Error(err)
	_, err = v555.IncrementEntry(3)
	assert.Error(err)
	_, err = v555.DecrementEntry(-1)
	assert.Error(err)
	_, err = v555.DecrementEntry(3)
	assert.Error(err)
	_, err = nilVector.IncrementEntry(0)
	assert.Error(err)
	_, err = nilVector.DecrementEntry(0)
	assert.Error(err)
	_, err = nilZero.IncrementEntry(0)
	assert.Error(err)
	_, err = nilZero.DecrementEntry(0)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test out of range errors near the boundary of the uint64 range
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 3; i++ {
		_, err = vbig.IncrementEntry(i)
		assert.Error(err)
		_, err = vbig.DecrementEntry(i)
		assert.NoError(err)
		_, err = Zero(ZZ3).IncrementEntry(i)
		assert.NoError(err)
		_, err = Zero(ZZ3).DecrementEntry(i)
		assert.Error(err)
	}
}

// TestToSlice tests ToIntegerSlice, ToInt64Slice, and ToUint64Slice
func TestToSlice(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v555, err := FromIntSlice(ZZ3, []int{5, 5, 5})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ3, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	vmedium, err := ConstantVector(ZZ3, integer.FromInt64(math.MaxInt64))
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for ToInt64Slice
	//////////////////////////////////////////////////////////////////////
	if s, err := v555.ToInt64Slice(); assert.NoError(err) {
		assert.Equal(s, []int64{5, 5, 5})
	}
	_, err = vbig.ToInt64Slice()
	assert.Error(err)
	_, err = vmedium.ToInt64Slice()
	assert.NoError(err)
	if s, err := nilElement.ToInt64Slice(); assert.NoError(err) {
		assert.Len(s, 0)
	}
	if s, err := nilZero.ToInt64Slice(); assert.NoError(err) {
		assert.Len(s, 0)
	}
	//////////////////////////////////////////////////////////////////////
	// the rest of the code paths for the dimension gt 0 case seem to have
	// been tested already
	//////////////////////////////////////////////////////////////////////
	assert.Len(nilElement.ToIntegerSlice(), 0)
	assert.Len(nilElement.ToUint64Slice(), 0)
	assert.Len(nilZero.ToIntegerSlice(), 0)
	assert.Len(nilZero.ToUint64Slice(), 0)
}

// TestIsPositive tests IsPositive
func TestIsPositive(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v32, err := FromIntSlice(ZZ2, []int{3, 2})
	assert.NoError(err)
	zero := Zero(ZZ2)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(v32.IsPositive())
	assert.False(zero.IsPositive())
	assert.False(nilElement.IsPositive())
	assert.False(nilZero.IsPositive())
}

// TestComparison tests IsEqualTo, IsGreaterThan, IsGreaterThanOrEqualTo, IsLessThan, IsLessThanOrEqualTo, and TotalDegree
func TestComparison(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v11, err := FromIntSlice(ZZ2, []int{1, 1})
	assert.NoError(err)
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	v231, err := FromIntSlice(ZZ3, []int{2, 3, 1})
	assert.NoError(err)
	v125, err := FromIntSlice(ZZ3, []int{1, 2, 5})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ3, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for IsEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsEqualTo(v123))
	assert.False(v123.IsEqualTo(v231))
	assert.False(v123.IsEqualTo(v11))
	assert.False(v123.IsEqualTo(nilElement))
	assert.False(v123.IsEqualTo(nilZero))
	assert.True(nilZero.IsEqualTo(nilElement))
	assert.True(nilZero.IsEqualTo(nilZero))
	assert.False(nilZero.IsEqualTo(v11))
	assert.True(nilElement.IsEqualTo(nilElement))
	assert.True(nilElement.IsEqualTo(nilZero))
	assert.False(nilElement.IsEqualTo(v11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThan
	//////////////////////////////////////////////////////////////////////
	assert.False(v123.IsGreaterThan(v123))
	assert.False(v123.IsGreaterThan(v231))
	assert.False(v123.IsGreaterThan(v125))
	assert.False(v123.IsGreaterThan(v11))
	assert.False(v123.IsGreaterThan(nilElement))
	assert.False(v123.IsGreaterThan(nilZero))
	assert.False(nilZero.IsGreaterThan(nilElement))
	assert.False(nilZero.IsGreaterThan(nilZero))
	assert.False(nilZero.IsGreaterThan(v11))
	assert.False(nilElement.IsGreaterThan(nilElement))
	assert.False(nilElement.IsGreaterThan(nilZero))
	assert.False(nilElement.IsGreaterThan(v11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsGreaterThanOrEqualTo(v123))
	assert.False(v123.IsGreaterThanOrEqualTo(v231))
	assert.False(v123.IsGreaterThanOrEqualTo(v125))
	assert.False(v123.IsGreaterThanOrEqualTo(v11))
	assert.False(v123.IsGreaterThanOrEqualTo(nilElement))
	assert.False(v123.IsGreaterThanOrEqualTo(nilZero))
	assert.True(nilZero.IsGreaterThanOrEqualTo(nilElement))
	assert.True(nilZero.IsGreaterThanOrEqualTo(nilZero))
	assert.False(nilZero.IsGreaterThanOrEqualTo(v11))
	assert.True(nilElement.IsGreaterThanOrEqualTo(nilElement))
	assert.True(nilElement.IsGreaterThanOrEqualTo(nilZero))
	assert.False(nilElement.IsGreaterThanOrEqualTo(v11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThan
	//////////////////////////////////////////////////////////////////////
	assert.False(v123.IsLessThan(v123))
	assert.True(v123.IsLessThan(v231))
	assert.True(v123.IsLessThan(v125))
	assert.False(v123.IsLessThan(v11))
	assert.False(v123.IsLessThan(nilElement))
	assert.False(v123.IsLessThan(nilZero))
	assert.False(nilZero.IsLessThan(nilElement))
	assert.False(nilZero.IsLessThan(nilZero))
	assert.False(nilZero.IsLessThan(v11))
	assert.False(nilElement.IsLessThan(nilElement))
	assert.False(nilElement.IsLessThan(nilZero))
	assert.False(nilElement.IsLessThan(v11))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.IsLessThanOrEqualTo(v123))
	assert.True(v123.IsLessThanOrEqualTo(v231))
	assert.True(v123.IsLessThanOrEqualTo(v125))
	assert.False(v123.IsLessThanOrEqualTo(v11))
	assert.False(v123.IsLessThanOrEqualTo(nilElement))
	assert.False(v123.IsLessThanOrEqualTo(nilZero))
	assert.True(nilZero.IsLessThanOrEqualTo(nilElement))
	assert.True(nilZero.IsLessThanOrEqualTo(nilZero))
	assert.False(nilZero.IsLessThanOrEqualTo(v11))
	assert.True(nilElement.IsLessThanOrEqualTo(nilElement))
	assert.True(nilElement.IsLessThanOrEqualTo(nilZero))
	assert.False(nilElement.IsLessThanOrEqualTo(v11))
	//////////////////////////////////////////////////////////////////////
	// tests for TotalDegree
	//////////////////////////////////////////////////////////////////////
	assert.True(v123.TotalDegree().IsEqualTo(integer.FromInt(6)))
	assert.True(v231.TotalDegree().IsEqualTo(integer.FromInt(6)))
	assert.True(v125.TotalDegree().IsEqualTo(integer.FromInt(8)))
	assert.True(v11.TotalDegree().IsEqualTo(integer.FromInt(2)))
	assert.True(nilZero.TotalDegree().IsZero())
	assert.True(nilElement.TotalDegree().IsZero())
	assert.True(vbig.TotalDegree().IsEqualTo(integer.Multiply(integer.FromInt(3), integer.FromUint64(math.MaxUint64))))
}

// TestScalarMultiplyBy tests ScalarMultiplyByInteger, ScalarMultiplyByUint64, and ScalarMultiplyByInt64
func TestScalarMultiplyBy(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ3, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v123, err := FromIntSlice(ZZ3, []int{1, 2, 3})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByInteger
	//////////////////////////////////////////////////////////////////////
	if v, err := v123.ScalarMultiplyByInteger(integer.Zero()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := v123.ScalarMultiplyByInteger(integer.FromInt(6)); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{6, 12, 18})
	}
	if v, err := nilElement.ScalarMultiplyByInteger(integer.Zero()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByInteger(integer.FromInt(6)); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByInteger(integer.Zero()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByInteger(integer.FromInt(6)); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := Zero(ZZ3).ScalarMultiplyByInteger(integer.FromUint64(math.MaxUint64).Increment()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	_, err = v123.ScalarMultiplyByInteger(integer.FromUint64(math.MaxUint64).Increment())
	assert.Error(err)
	if v, err := nilZero.ScalarMultiplyByInteger(integer.FromUint64(math.MaxUint64).Increment()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByInteger(integer.FromUint64(math.MaxUint64).Increment()); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByUint64
	//////////////////////////////////////////////////////////////////////
	if v, err := v123.ScalarMultiplyByUint64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := v123.ScalarMultiplyByUint64(2); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{2, 4, 6})
	}
	if v, err := v123.ScalarMultiplyByUint64(7); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{7, 14, 21})
	}
	if v, err := nilElement.ScalarMultiplyByUint64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByUint64(3); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByUint64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByUint64(3); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	_, err = v123.ScalarMultiplyByUint64(math.MaxUint64)
	assert.Error(err)
	if v, err := nilZero.ScalarMultiplyByUint64(math.MaxUint64); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByUint64(math.MaxUint64); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	//////////////////////////////////////////////////////////////////////
	// test ScalarMultiplyByInt64
	//////////////////////////////////////////////////////////////////////
	if v, err := v123.ScalarMultiplyByInt64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	_, err = v123.ScalarMultiplyByInt64(-1)
	assert.Error(err)
	if v, err := v123.ScalarMultiplyByInt64(1); assert.NoError(err) {
		assert.AreEqual(ZZ3, v, v123)
	}
	if v, err := v123.ScalarMultiplyByInt64(7); assert.NoError(err) {
		assert.Equal(v.ToUint64Slice(), []uint64{7, 14, 21})
	}
	if v, err := nilElement.ScalarMultiplyByInt64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByInt64(3); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByInt64(0); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilZero.ScalarMultiplyByInt64(3); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	_, err = v123.ScalarMultiplyByInt64(math.MaxInt64)
	assert.Error(err)
	if v, err := nilZero.ScalarMultiplyByInt64(math.MaxInt64); assert.NoError(err) {
		assert.True(v.IsZero())
	}
	if v, err := nilElement.ScalarMultiplyByInt64(math.MaxInt64); assert.NoError(err) {
		assert.True(v.IsZero())
	}

}

// TestHash tests Hash
func TestHash(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v51, err := FromIntSlice(ZZ2, []int{5, 1})
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// equal elements should hash to the same thing
	//////////////////////////////////////////////////////////////////////
	if v, err := FromString(ZZ2, "(5,1)"); assert.NoError(err) {
		assert.Equal(v51.Hash(), v.Hash())
	}
	assert.Equal(nilElement.Hash(), hash.Value(0))
	assert.Equal(nilZero.Hash(), hash.Value(0))
}

// TestElementString tests String in element.go
func TestElementString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v51, err := FromIntSlice(ZZ2, []int{5, 1})
	assert.NoError(err)
	zero := Zero(ZZ2)
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.Equal(v51.String(), "(5,1)")
	assert.Equal(zero.String(), "(0,0)")
	assert.Equal(nilElement.String(), "()")
	assert.Equal(nilZero.String(), "()")
}

// TestSubtract tests Subtract
func TestSubtract(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v51, err := FromIntSlice(ZZ2, []int{5, 1})
	assert.NoError(err)
	v31, err := FromIntSlice(ZZ2, []int{3, 1})
	assert.NoError(err)
	v20, err := FromIntSlice(ZZ2, []int{2, 0})
	assert.NoError(err)
	zero := Zero(ZZ2)
	assert.NoError(err)
	var nilElement *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if v, err := Subtract(v51, zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v51)
	}
	if v, err := Subtract(v51, v31); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v20)
	}
	if v, err := Subtract(v51, v51); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
	}
	_, err = Subtract(zero, v51)
	assert.Error(err)
	_, err = Subtract(v31, v51)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	if v, err := Subtract(nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilElement)
	}
	if v, err := Subtract(nilZero, nilElement); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilElement)
	}
	if v, err := Subtract(nilElement, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilElement)
	}
	if v, err := Subtract(nilElement, nilElement); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilElement)
	}
	//////////////////////////////////////////////////////////////////////
	// mismatched parents
	//////////////////////////////////////////////////////////////////////
	_, err = Subtract(v51, nilElement)
	assert.Error(err)
	_, err = Subtract(nilElement, v51)
	assert.Error(err)
	_, err = Subtract(v51, nilZero)
	assert.Error(err)
	_, err = Subtract(nilZero, v51)
	assert.Error(err)
}
