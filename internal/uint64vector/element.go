// Element defines an n-dimensional uint64-valued vector in the vector-space uint64^n.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64vector

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"bitbucket.org/pcastools/stringsbuilder"
	"math"
	"strconv"
)

// Element describes an uint64-valued vector.
type Element struct {
	parent *Parent  // The parent of this element
	value  []uint64 // The underlying slice of uint64s
}

// verticalSum gives the result of summing the i-th entry (indexed from 0) over all vectors.
type verticalSum struct {
	i     int    // The entry (indexed from 0)
	value uint64 // The sum...
	err   error  // ...or an error
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createZero creates a new zero element for the given parent M. It does not assign zero to the parent.
func createZero(M *Parent) *Element {
	return &Element{parent: M, value: make([]uint64, M.Dimension())}
}

// parentOfElement returns the parent for x.
func parentOfElement(x *Element) *Parent {
	if x == nil {
		return nil
	}
	return x.parent
}

// doParentsAgree returns true iff x and y have the same parents.
func doParentsAgree(x *Element, y *Element) bool {
	return parentOfElement(x) == parentOfElement(y)
}

// isZeroSlice returns true iff all the entries in the given slice are zero.
func isZeroSlice(S []uint64) bool {
	for _, v := range S {
		if v != 0 {
			return false
		}
	}
	return true
}

// fromIntegerSlice returns a vector element with the given parent set to the value S. If the length of the slice does not agree with the dimension of the parent, or the entries in the slice cannot be converted to uint64s, then the function will return an error.
func fromIntegerSlice(M *Parent, S []*integer.Element) (*Element, error) {
	// Sanity check on the dimension/length
	if M.Dimension() != len(S) {
		return nil, errors.SliceLengthNotEqualDimension.New(len(S), M.Dimension())
	}
	// Attempt to convert the slice to a slice of uint64s
	s, err := integer.SliceToUint64Slice(S)
	if err != nil {
		return nil, err
	}
	// Handle the zero vector
	if isZeroSlice(s) {
		if M == nil {
			return nil, nil
		}
		return M.zero, nil
	}
	// Create a new element using the slice
	return &Element{parent: M, value: s}, nil
}

// fromUint64Slice returns a vector element with the given parent set to the value S. If the length of the slice does not agree with the dimension of the parent then the function will return an error. Note that the slice S is NOT copied, hence must not be modified after this call.
func fromUint64Slice(M *Parent, S []uint64) (*Element, error) {
	// Sanity check on the dimension/length
	if M.Dimension() != len(S) {
		return nil, errors.SliceLengthNotEqualDimension.New(len(S), M.Dimension())
	}
	// Handle the zero vector
	if isZeroSlice(S) {
		if M == nil {
			return nil, nil
		}
		return M.zero, nil
	}
	// Create a new element using the slice
	return &Element{parent: M, value: S}, nil
}

// scalarMultiplyThenAdd computes a * b + c, if it fits in an uint64.
func scalarMultiplyThenAdd(a uint64, b uint64, c uint64) (uint64, bool) {
	// First we try this as uint64s
	if d, ok := mathutil.MultiplyUint64(a, b); ok {
		if d, ok = mathutil.AddUint64(d, c); ok {
			return d, true
		}
	}
	// No good -- try as integers and see if we can convert back
	d, err := integer.MultiplyThenAdd(integer.FromUint64(a), integer.FromUint64(b), integer.FromUint64(c)).Uint64()
	if err != nil {
		return 0, false
	}
	return d, true
}

// scalarMultiplyThenSubtract computes a * b - c, if it fits in an uint64.
func scalarMultiplyThenSubtract(a uint64, b uint64, c uint64) (uint64, bool) {
	// First we try this as uint64s
	if d, ok := mathutil.MultiplyUint64(a, b); ok {
		if d, ok = mathutil.SubtractUint64(d, c); ok {
			return d, true
		}
	}
	// No good -- try as integers and see if we can convert back
	d, err := integer.MultiplyThenSubtract(integer.FromUint64(a), integer.FromUint64(b), integer.FromUint64(c)).Uint64()
	if err != nil {
		return 0, false
	}
	return d, true
}

// sumUint64 returns the sum of the given slice, if it fits without any effort in an uint64.
func sumUint64(S []uint64) (uint64, bool) {
	var c uint64
	for _, s := range S {
		var ok bool
		if c, ok = mathutil.AddUint64(s, c); !ok {
			return 0, false
		}
	}
	return c, true
}

/////////////////////////////////////////////////////////////////////////
// Sum functions
/////////////////////////////////////////////////////////////////////////

// sumForIndex sums the i-th entries (indexed from 0) of the vectors in S. The result is returned down the resc channel. If the result doesn't fit in an uint64 then an error is set and returned down the resc channel.
func sumForIndex(S []*Element, i int, resc chan<- *verticalSum) {
	// Create a slice of integers
	xs := make([]*integer.Element, 0, len(S))
	for _, v := range S {
		if v != nil && v.value[i] != 0 {
			xs = append(xs, integer.FromUint64(v.value[i]))
		}
	}
	// Perform the sum
	value, err := integer.Sum(xs...).Uint64()
	// Check that the result fits in an uint64
	if err != nil {
		resc <- &verticalSum{i: i, err: err}
	}
	resc <- &verticalSum{i: i, value: value}
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element of M. This will return an error if the elements of S do not all have M as their parent.
func Sum(M *Parent, S ...*Element) (*Element, error) {
	// Handle the empty case
	if len(S) == 0 {
		return Zero(M), nil
	}
	// Sanity check on the parents
	for _, v := range S {
		if parentOfElement(v) != M {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	// Do the length 1 and 2 cases
	if len(S) == 1 {
		return S[0], nil
	} else if len(S) == 2 {
		return Add(S[0], S[1])
	}
	// Calculate the sum
	resc := make(chan *verticalSum)
	n := M.Dimension()
	for i := 0; i < n; i++ {
		go sumForIndex(S, i, resc)
	}
	// Collect the results, checking for errors
	var err error
	value := make([]uint64, n)
	for i := 0; i < n; i++ {
		if res := <-resc; res.err == nil {
			value[res.i] = res.value
		} else {
			err = res.err
		}
	}
	// Handle any errors and return
	if err != nil {
		return nil, err
	}
	return fromUint64Slice(M, value)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// FromIntegerSlice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M, or if the entries do not fit in an uint64.
func FromIntegerSlice(M *Parent, S []*integer.Element) (*Element, error) {
	return fromIntegerSlice(M, S)
}

// FromIntSlice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M, or if any of the entries are negative.
func FromIntSlice(M *Parent, S []int) (*Element, error) {
	// Handle the nil case
	if M == nil && len(S) == 0 {
		return nil, nil
	}
	// Convert the slice to a slice of uint64s
	value := make([]uint64, 0, len(S))
	for _, v := range S {
		if v < 0 {
			return nil, errors.IllegalNegativeArg.New()
		}
		value = append(value, uint64(v))
	}
	// Return the copy as a vector
	return fromUint64Slice(M, value)
}

// FromInt32Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M, or if any of the entries are negative.
func FromInt32Slice(M *Parent, S []int32) (*Element, error) {
	// Handle the nil case
	if M == nil && len(S) == 0 {
		return nil, nil
	}
	// Convert the slice to a slice of uint64s
	value := make([]uint64, 0, len(S))
	for _, v := range S {
		if v < 0 {
			return nil, errors.IllegalNegativeArg.New()
		}
		value = append(value, uint64(v))
	}
	// Return the copy as a vector
	return fromUint64Slice(M, value)
}

// FromUint64Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M.
func FromUint64Slice(M *Parent, S []uint64) (*Element, error) {
	// Handle the nil case
	if M == nil && len(S) == 0 {
		return nil, nil
	}
	// Make a copy of the slice
	value := make([]uint64, len(S))
	copy(value, S)
	// Return the copy as a vector
	return fromUint64Slice(M, value)
}

// FromInt64Slice returns an n-dimensional vector in M with value S. This will return an error if the length of the slice does not agree with the dimension of M, or if any of the entries are negative.
func FromInt64Slice(M *Parent, S []int64) (*Element, error) {
	// Handle the nil case
	if M == nil && len(S) == 0 {
		return nil, nil
	}
	// Convert the slice to a slice of uint64s
	value := make([]uint64, 0, len(S))
	for _, v := range S {
		if v < 0 {
			return nil, errors.IllegalNegativeArg.New()
		}
		value = append(value, uint64(v))
	}
	// Return the copy as a vector
	return fromUint64Slice(M, value)
}

// FromString returns the n-dimensional vector in M represented by the string s.  It returns an error if s is malformed, or if any of the entries of the vector represented by s are negative.
func FromString(M *Parent, s string) (*Element, error) {
	// Begin by converting the string into a slice of integers
	value, err := integer.SliceFromString(s)
	if err != nil {
		return nil, err
	}
	// Return the slice as a vector
	return fromIntegerSlice(M, value)
}

// ConstantVector returns an n-dimensional vector in M all of whose entries are c.
func ConstantVector(M *Parent, c *integer.Element) (*Element, error) {
	// Handle the nil case and the case when c is zero
	n := M.Dimension()
	if n == 0 || c.IsZero() {
		return Zero(M), nil
	}
	// Convert c to an uint64
	d, err := c.Uint64()
	if err != nil {
		return nil, err
	}
	// Create the slice
	value := make([]uint64, 0, n)
	for i := 0; i < n; i++ {
		value = append(value, d)
	}
	// Return the vector
	return fromUint64Slice(M, value)
}

// Basis returns the n-dimensional standard basis of M.
func Basis(M *Parent) []*Element {
	// Note the dimension
	n := M.Dimension()
	// Create a slice of zeros of length 2n - 1, and set the (n-1)-th entry to 1
	value := make([]uint64, 2*n-1)
	value[n-1] = 1
	// Create the basis elements
	basis := make([]*Element, 0, n)
	for i := 0; i < n; i++ {
		v, err := fromUint64Slice(M, value[n-i-1:2*n-i-1])
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		basis = append(basis, v)
	}
	return basis
}

// NthBasisElement returns the n-th standard basis element of M. Here the basis elements are indexed from 0 to d-1 (inclusive), where d is the dimension of M.
func NthBasisElement(M *Parent, n int) (*Element, error) {
	// Sanity check
	rk := M.Dimension()
	if n < 0 || n >= rk {
		return nil, errors.InvalidIndexRange.New(n, rk-1)
	}
	// Create the slice describing the n-th basis element
	value := make([]uint64, rk)
	value[n] = 1
	// Return the vector
	return fromUint64Slice(M, value)
}

// Zero returns the n-dimensional zero vector in M.
func Zero(M *Parent) *Element {
	if M == nil {
		return createZero(M)
	}
	return M.zero
}

// AreEqual returns true iff x and y have the same parent, and x = y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y. This will return an error if x and y do not have the same parent.
func Cmp(x *Element, y *Element) (int, error) {
	// Check that the parents agree and get the easy case out of the way
	if !doParentsAgree(x, y) {
		return 0, errors.ParentsDoNotAgree.New()
	} else if x == nil || y == nil || x == y {
		return 0, nil
	}
	// Perform the comparison
	return parentOfElement(x).MonomialOrder().CmpUint64(x.value, y.value)
}

// DotProduct returns the dot-product of the vectors x and y. Here there is no condition on the parents of x and y. This will return an error if the dimensions differ.
func DotProduct(x *Element, y *Element) (*integer.Element, error) {
	// Sanity check on the dimension
	if x.Dimension() != y.Dimension() {
		return nil, errors.DimensionsDoNotAgree.New()
	}
	// Get the zero cases out of the way
	if x.IsZero() || y.IsZero() {
		return integer.Zero(), nil
	}
	// Return the dot product
	return integer.DotProduct(x.ToIntegerSlice(), y.ToIntegerSlice()), nil
}

// Add returns the vector sum x + y. This will return an error if x and y do not have the same parent, or if the result is out of range.
func Add(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the case where one of the arguments is known to be zero
	if x.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x, nil
	}
	// Calculate the sum
	value := make([]uint64, 0, x.Dimension())
	for i, v := range x.value {
		c, ok := mathutil.AddUint64(v, y.value[i])
		if !ok {
			return nil, errors.OutOfRange.New()
		}
		value = append(value, c)
	}
	return fromUint64Slice(parentOfElement(x), value)
}

// Subtract returns the vector different x - y. This will return an error if x and y do not have the same parent, or if the result is out of range.
func Subtract(x *Element, y *Element) (*Element, error) {
	// Check that the parents agree
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	}
	// Fast-track the case where one of the arguments is known to be zero
	if y.IsZero() {
		return x, nil
	} else if x.IsZero() {
		return nil, errors.OutOfRange.New()
	} else if x == y {
		return Zero(parentOfElement(x)), nil
	}
	// Calculate the difference
	value := make([]uint64, 0, x.Dimension())
	for i, v := range x.value {
		c, ok := mathutil.SubtractUint64(v, y.value[i])
		if !ok {
			return nil, errors.OutOfRange.New()
		}
		value = append(value, c)
	}
	return fromUint64Slice(parentOfElement(x), value)
}

// ScalarMultiplyByIntegerThenAdd returns a * x + y, where a is an integer, and x and y are vectors. This will return an error if x and y do not have the same parent, or if the result is out of range.
func ScalarMultiplyByIntegerThenAdd(a *integer.Element, x *Element, y *Element) (*Element, error) {
	// Check that the parents agree and get the easy cases out of the way
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if x.IsZero() || a.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x.ScalarMultiplyByInteger(a)
	}
	// first try this with everything as uint64s
	if aa, err := a.Uint64(); err == nil {
		// Do the calculation
		value := make([]uint64, 0, x.Dimension())
		var c uint64
		var ok bool
		for i, xi := range x.value {
			if c, ok = scalarMultiplyThenAdd(aa, xi, y.value[i]); !ok {
				break
			} else {
				value = append(value, c)
			}
		}
		if ok {
			return fromUint64Slice(parentOfElement(x), value)
		}
	}
	// that didn't work, so do the whole calculation over the integers and try to convert back
	S := make([]*integer.Element, 0, x.Dimension())
	for i, b := range x.value {
		S = append(S, integer.MultiplyThenAdd(a, integer.FromUint64(b), integer.FromUint64(y.value[i])))
	}
	return FromIntegerSlice(parentOfElement(x), S)
}

// ScalarMultiplyByIntegerThenSubtract returns a * x - y, where a is an integer, and x and y are vectors. This will return an error if x and y do not have the same parent, or if the result is out of range.
func ScalarMultiplyByIntegerThenSubtract(a *integer.Element, x *Element, y *Element) (*Element, error) {
	// Check that the parents agree and get the easy cases out of the way
	if !doParentsAgree(x, y) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if y.IsZero() {
		return x.ScalarMultiplyByInteger(a)
	} else if x.IsZero() || a.IsZero() {
		return nil, errors.OutOfRange.New()
	}
	// first try this with everything as uint64s
	if aa, err := a.Uint64(); err == nil {
		// Do the calculation
		value := make([]uint64, 0, x.Dimension())
		var c uint64
		var ok bool
		for i, xi := range x.value {
			if c, ok = scalarMultiplyThenSubtract(aa, xi, y.value[i]); !ok {
				break
			} else {
				value = append(value, c)
			}
		}
		if ok {
			return fromUint64Slice(parentOfElement(x), value)
		}
	}
	// that didn't work, so do the whole calculation over the integers and try to convert back
	S := make([]*integer.Element, 0, x.Dimension())
	for i, b := range x.value {
		S = append(S, integer.MultiplyThenSubtract(a, integer.FromUint64(b), integer.FromUint64(y.value[i])))
	}
	return FromIntegerSlice(parentOfElement(x), S)
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// Dimension returns the dimension of the parent of the vector x.
func (x *Element) Dimension() int {
	if x == nil {
		return 0
	}
	return len(x.value)
}

// EntryOrPanic returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will panic if i is out of range.
func (x *Element) EntryOrPanic(i int) *integer.Element {
	return integer.FromUint64(x.value[i])
}

// Entry returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range.
func (x *Element) Entry(i int) (*integer.Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	}
	return integer.FromUint64(x.value[i]), nil
}

// EntryAsUint64OrPanic returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will panic if i is out of range.
func (x *Element) EntryAsUint64OrPanic(i int) uint64 {
	return x.value[i]
}

// EntryAsUint64 returns the i-th entry in the vector x, where the entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range.
func (x *Element) EntryAsUint64(i int) (uint64, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return 0, errors.InvalidIndexRange.New(i, n-1)
	}
	return x.value[i], nil
}

// IncrementEntry returns a new n-dimensional vector equal to x, but with the i-th entry incremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range, or if the result is out of range.
func (x *Element) IncrementEntry(i int) (*Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	} else if x.value[i] == math.MaxUint64 {
		return nil, errors.OutOfRange.New()
	}
	value := make([]uint64, n)
	copy(value, x.value)
	value[i]++
	return fromUint64Slice(parentOfElement(x), value)
}

// DecrementEntry returns a new n-dimensional vector equal to x, but with the i-th entry decremented by one. Entries are indexed from 0 up to one less than the dimension of the parent of x (inclusive). This will return an error if i is out of range, or if the result is out of range.
func (x *Element) DecrementEntry(i int) (*Element, error) {
	n := x.Dimension()
	if i < 0 || i >= n {
		return nil, errors.InvalidIndexRange.New(i, n-1)
	} else if x.value[i] == 0 {
		return nil, errors.OutOfRange.New()
	}
	value := make([]uint64, n)
	copy(value, x.value)
	value[i]--
	return fromUint64Slice(parentOfElement(x), value)
}

// ToIntegerSlice returns the vector as a slice of integers. Note that you can modify the returned slice without affecting the vector.
func (x *Element) ToIntegerSlice() []*integer.Element {
	if x.Dimension() == 0 {
		return make([]*integer.Element, 0)
	}
	return integer.SliceFromUint64Slice(x.value)
}

// ToUint64Slice returns the vector as a slice of uint64s.
func (x *Element) ToUint64Slice() []uint64 {
	n := x.Dimension()
	if n == 0 {
		return make([]uint64, 0)
	}
	S := make([]uint64, n)
	copy(S, x.value)
	return S
}

// ToInt64Slice returns the vector as a slice of int64s, or an error if any entry in the vector overflows an int64.
func (x *Element) ToInt64Slice() ([]int64, error) {
	n := x.Dimension()
	if n == 0 {
		return make([]int64, 0), nil
	}
	S := make([]int64, 0, n)
	for _, v := range x.value {
		if v > math.MaxInt64 {
			return nil, errors.OutOfRange.New()
		}
		S = append(S, int64(v))
	}
	return S, nil
}

// IsZero returns true iff x is the zero vector (0,0,...,0).
func (x *Element) IsZero() bool {
	if M := parentOfElement(x); M == nil || M.zero == x {
		return true
	}
	return isZeroSlice(x.value)
}

// IsPositive returns true iff x > 0.
func (x *Element) IsPositive() bool {
	if x.IsZero() {
		return false
	}
	sgn, err := Cmp(x, Zero(parentOfElement(x)))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return sgn > 0
}

// IsEqualTo returns true iff x and y have the same parent, and x = y.
func (x *Element) IsEqualTo(y *Element) bool {
	if !doParentsAgree(x, y) {
		return false
	}
	if x != y && x != nil {
		for i, v := range x.value {
			if v != y.value[i] {
				return false
			}
		}
	}
	return true
}

// IsGreaterThan returns true iff x and y have the same parent, and x > y.
func (x *Element) IsGreaterThan(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val > 0
}

// IsGreaterThanOrEqualTo returns true iff x and y have the same parent, and x >= y.
func (x *Element) IsGreaterThanOrEqualTo(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val >= 0
}

// IsLessThan returns true iff x and y have the same parent, and x < y.
func (x *Element) IsLessThan(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val < 0
}

// IsLessThanOrEqualTo returns true iff x and y have the same parent, and x <= y.
func (x *Element) IsLessThanOrEqualTo(y *Element) bool {
	val, err := Cmp(x, y)
	if err != nil {
		return false
	}
	return val <= 0
}

// ScalarMultiplyByUint64 returns the scalar product a * x, where a is an integer. This will return an error if the result is out of range.
func (x *Element) ScalarMultiplyByUint64(a uint64) (*Element, error) {
	if x.IsZero() || a == 1 {
		return x, nil
	} else if a == 0 {
		return Zero(parentOfElement(x)), nil
	}
	value := make([]uint64, 0, x.Dimension())
	for _, v := range x.value {
		b, ok := mathutil.MultiplyUint64(a, v)
		if !ok {
			return nil, errors.OutOfRange.New()
		}
		value = append(value, b)
	}
	return fromUint64Slice(parentOfElement(x), value)
}

// ScalarMultiplyByInt64 returns the scalar product a * x, where a is an integer. This will return an error if the result is out of range.
func (x *Element) ScalarMultiplyByInt64(a int64) (*Element, error) {
	if x.IsZero() {
		return x, nil
	} else if a < 0 {
		return nil, errors.OutOfRange.New()
	}
	return x.ScalarMultiplyByUint64(uint64(a))
}

// ScalarMultiplyByInteger returns the scalar product a * x, where a is an integer. This will return an error if the result is out of range.
func (x *Element) ScalarMultiplyByInteger(a *integer.Element) (*Element, error) {
	if x.IsZero() {
		return x, nil
	}
	b, err := a.Uint64()
	if err != nil {
		return nil, err
	}
	return x.ScalarMultiplyByUint64(b)
}

// TotalDegree returns the total degree of the vector x. That is, it returns the sum of the entries in x.
func (x *Element) TotalDegree() *integer.Element {
	if x.IsZero() {
		return integer.Zero()
	}
	// We try and do the sum as uint64s
	if c, ok := sumUint64(x.value); ok {
		return integer.FromUint64(c)
	}
	// No luck -- do it as integers
	return integer.Sum(x.ToIntegerSlice()...)
}

// Hash returns a hash value for this vector.
func (x *Element) Hash() hash.Value {
	if x.Dimension() == 0 {
		return 0
	}
	return hash.Uint64Slice(x.value)
}

// Parent returns the parent of this vector.
func (x *Element) Parent() object.Parent {
	return parentOfElement(x)
}

// String returns a string representation of the vector.
func (x *Element) String() string {
	if x.Dimension() == 0 {
		return "()"
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('(')
	for i, v := range x.value {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteString(strconv.FormatUint(v, 10))
	}
	b.WriteByte(')')
	return b.String()
}
