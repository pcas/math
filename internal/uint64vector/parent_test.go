// Tests parent.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64vector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/testing/assert"
	"bitbucket.org/pcas/math/vector/integervector"
	"math"
	"testing"
)

//////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////

// TestLatticeCreation tests DefaultLattice, DefaultLatticeWithOrder, NewLattice, NewLatticeWithOrder, and MonomialOrder
func TestLatticeCreation(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// tests for DefaultLattice
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	// repeated calls should give the same value
	if x, err := DefaultLattice(2); assert.NoError(err) {
		assert.True(x == ZZ2)
	}
	// negative dimensions should give an error
	_, err = DefaultLattice(-57)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for DefaultLatticeWithOrder
	//////////////////////////////////////////////////////////////////////
	ZZ3Grevlex, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	// repeated calls should give the same value
	if x, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex); assert.NoError(err) {
		assert.True(x == ZZ3Grevlex)
	}
	// the monomial order should matter
	ZZ3Grlex, err := DefaultLatticeWithOrder(3, monomialorder.Grlex)
	assert.NoError(err)
	assert.False(ZZ3Grlex == ZZ3Grevlex)
	// negative dimensions should give an error
	_, err = DefaultLatticeWithOrder(-57, monomialorder.Grlex)
	// illegal monomial orders should give an error
	var notAMonomialOrder monomialorder.Type = 255
	_, err = DefaultLatticeWithOrder(3, notAMonomialOrder)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for MonomialOrder
	//////////////////////////////////////////////////////////////////////
	assert.Equal(ZZ3Grlex.MonomialOrder(), monomialorder.Grlex)
	assert.Equal(ZZ3Grevlex.MonomialOrder(), monomialorder.Grevlex)
	assert.Equal(ZZ2.MonomialOrder(), monomialorder.Lex)
	var nilLattice *Parent
	assert.Equal(nilLattice.MonomialOrder(), monomialorder.Lex)
	//////////////////////////////////////////////////////////////////////
	// tests for NewLattice
	//////////////////////////////////////////////////////////////////////
	L1, err := NewLattice(2)
	assert.NoError(err)
	// repeated calls should give different lattices
	L2, err := NewLattice(2)
	assert.NoError(err)
	assert.False(L1 == L2)
	// they are also not equal to the default lattice
	assert.False(ZZ2 == L1)
	assert.False(ZZ2 == L2)
	// negative dimensions should give an error
	_, err = NewLattice(-1)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for NewLatticeWithOrder
	//////////////////////////////////////////////////////////////////////
	L3, err := NewLatticeWithOrder(2, monomialorder.Grlex)
	assert.NoError(err)
	// repeated calls should give different lattices
	L4, err := NewLatticeWithOrder(2, monomialorder.Grlex)
	assert.NoError(err)
	assert.False(L1 == L2)
	// they are also not equal to the default lattice
	L5, err := DefaultLatticeWithOrder(2, monomialorder.Grlex)
	assert.False(L3 == L5)
	assert.False(L4 == L5)
	// negative dimensions should give an error
	_, err = NewLatticeWithOrder(-4, monomialorder.Lex)
	assert.Error(err)
	// illegal monomial orders should give an error
	_, err = NewLatticeWithOrder(3, notAMonomialOrder)
	assert.Error(err)
}

// TestZero tests Zero and IsZero
func TestZero(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(ZZ3.IsZero(ZZ3.Zero()))
	assert.False(ZZ3.IsZero(ZZ2.Zero()))
	assert.False(ZZ3.IsZero(nilLattice.Zero()))
}

// TestContains tests Contains, ToElement, and AreEqual
func TestContains(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	ZZ1, err := DefaultLattice(1)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some elements
	//////////////////////////////////////////////////////////////////////
	v2, err := FromIntSlice(ZZ2, []int{4, 5})
	assert.NoError(err)
	v3, err := FromIntSlice(ZZ3, []int{4, 0, 5})
	assert.NoError(err)
	v := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests for Contains
	//////////////////////////////////////////////////////////////////////
	assert.False(ZZ3.Contains(v))
	assert.False(ZZ3.Contains(v2))
	assert.True(ZZ3.Contains(v3))
	assert.False(ZZ2.Contains(v))
	assert.True(ZZ2.Contains(v2))
	assert.False(ZZ2.Contains(v3))
	assert.True(nilLattice.Contains(v))
	assert.False(nilLattice.Contains(v2))
	assert.False(nilLattice.Contains(v3))
	//////////////////////////////////////////////////////////////////////
	// tests for ToElement
	//////////////////////////////////////////////////////////////////////
	_, err = ZZ3.ToElement(v)
	assert.Error(err)
	_, err = ZZ3.ToElement(v2)
	assert.Error(err)
	if x, err := ZZ3.ToElement(v3); assert.NoError(err) {
		assert.AreEqual(ZZ3, x, v3)
	}
	_, err = ZZ2.ToElement(v)
	assert.Error(err)
	if x, err := ZZ2.ToElement(v2); assert.NoError(err) {
		assert.AreEqual(ZZ2, x, v2)
	}
	_, err = ZZ2.ToElement(v3)
	assert.Error(err)
	if x, err := nilLattice.ToElement(v); assert.NoError(err) {
		assert.AreEqual(nilLattice, x, v)
	}
	_, err = nilLattice.ToElement(v2)
	assert.Error(err)
	_, err = nilLattice.ToElement(v3)
	assert.Error(err)
	// test the one-dimensional special cases
	if twothirds, err := rational.FromString("2/3"); assert.NoError(err) {
		_, err2 := ZZ1.ToElement(twothirds)
		assert.Error(err2)
	}
	_, err = ZZ1.ToElement(integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	_, err = ZZ1.ToElement(integer.FromUint64(math.MaxUint64).Increment())
	assert.Error(err)
	_, err = ZZ1.ToElement(integer.FromInt(-4))
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for AreEqual
	/////////////////////////////////////////////////////////////////////
	_, err = ZZ3.AreEqual(v2, v3)
	assert.Error(err)
	_, err = ZZ3.AreEqual(v3, v2)
	assert.Error(err)
	_, err = nilLattice.AreEqual(v2, v3)
	assert.Error(err)

}

// TestString tests String and AssignName
func TestString(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// a lattice
	//////////////////////////////////////////////////////////////////////
	L, err := DefaultLattice(5)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.Equal(L.String(), "{uint64}^5")
	L.AssignName("Alice")
	assert.Equal(L.String(), "Alice")
	L.AssignName("Bob")
	assert.Equal(L.String(), "Bob")
	L.AssignName("")
	assert.Equal(L.String(), "{uint64}^5")
}

// TestCmp tests Cmp
func TestCmp(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// three-dimensional lattices with different monomial orders
	//////////////////////////////////////////////////////////////////////
	ZZ3Lex, err := DefaultLattice(3)
	assert.NoError(err)
	ZZ3Grlex, err := DefaultLatticeWithOrder(3, monomialorder.Grlex)
	assert.NoError(err)
	ZZ3Grevlex, err := DefaultLatticeWithOrder(3, monomialorder.Grevlex)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	v125, err := FromUint64Slice(ZZ3Lex, []uint64{1, 2, 5})
	assert.NoError(err)
	v322, err := FromUint64Slice(ZZ3Lex, []uint64{3, 2, 2})
	assert.NoError(err)
	v142, err := FromUint64Slice(ZZ3Lex, []uint64{1, 4, 2})
	assert.NoError(err)
	v502, err := FromUint64Slice(ZZ3Lex, []uint64{5, 0, 2})
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// testing Lex
	//////////////////////////////////////////////////////////////////////
	if sign, err := ZZ3Lex.Cmp(v125, v125); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Lex.Cmp(v125, v322); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Lex.Cmp(v125, v142); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Lex.Cmp(v125, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Lex.Cmp(v322, v322); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Lex.Cmp(v322, v142); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Lex.Cmp(v322, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Lex.Cmp(v142, v142); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Lex.Cmp(v142, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Lex.Cmp(v502, v502); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	//////////////////////////////////////////////////////////////////////
	// testing Grlex
	//////////////////////////////////////////////////////////////////////
	v125, err = ChangeParent(ZZ3Grlex, v125)
	assert.NoError(err)
	v322, err = ChangeParent(ZZ3Grlex, v322)
	assert.NoError(err)
	v142, err = ChangeParent(ZZ3Grlex, v142)
	assert.NoError(err)
	v502, err = ChangeParent(ZZ3Grlex, v502)
	assert.NoError(err)
	if sign, err := ZZ3Grlex.Cmp(v125, v125); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grlex.Cmp(v125, v322); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grlex.Cmp(v125, v142); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grlex.Cmp(v125, v502); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grlex.Cmp(v322, v322); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grlex.Cmp(v322, v142); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grlex.Cmp(v322, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Grlex.Cmp(v142, v142); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grlex.Cmp(v142, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Grlex.Cmp(v502, v502); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	//////////////////////////////////////////////////////////////////////
	// testing Grevlex
	//////////////////////////////////////////////////////////////////////
	v125, err = ChangeParent(ZZ3Grevlex, v125)
	assert.NoError(err)
	v322, err = ChangeParent(ZZ3Grevlex, v322)
	assert.NoError(err)
	v142, err = ChangeParent(ZZ3Grevlex, v142)
	assert.NoError(err)
	v502, err = ChangeParent(ZZ3Grevlex, v502)
	assert.NoError(err)
	if sign, err := ZZ3Grevlex.Cmp(v125, v125); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grevlex.Cmp(v125, v322); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v125, v142); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v125, v502); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v322, v322); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grevlex.Cmp(v322, v142); assert.NoError(err) {
		assert.Equal(sign, 1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v322, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v142, v142); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	if sign, err := ZZ3Grevlex.Cmp(v142, v502); assert.NoError(err) {
		assert.Equal(sign, -1)
	}
	if sign, err := ZZ3Grevlex.Cmp(v502, v502); assert.NoError(err) {
		assert.Equal(sign, 0)
	}
	//////////////////////////////////////////////////////////////////////
	// testing some error cases
	//////////////////////////////////////////////////////////////////////
	var nilVector *Element
	_, err = ZZ3Grevlex.Cmp(v502, nilVector)
	assert.Error(err)
	_, err = ZZ3Grevlex.Cmp(nilVector, v502)
	assert.Error(err)
	// an attempted comparison between three-dimensional vectors in
	// different parents
	v125, err = ChangeParent(ZZ3Grlex, v125)
	assert.NoError(err)
	_, err = ZZ3Grevlex.Cmp(v502, v125)
	assert.Error(err)
}

// TestAdd tests Add
func TestAdd(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero()
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ2, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Add
	//////////////////////////////////////////////////////////////////////
	if v, err := ZZ2.Add(v12, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	if v, err := ZZ2.Add(zero, v24); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	if v, err := ZZ2.Add(v24, zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	// adding vectors in the wrong parent should give an error
	_, err = ZZ2.Add(v12, v111)
	assert.Error(err)
	_, err = ZZ2.Add(v111, v12)
	assert.Error(err)
	// check the nil case
	if v, err := nilLattice.Add(Zero(nilLattice), Zero(nilLattice)); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, Zero(nilLattice))
	}
	// check overflow at near the boundary of the range
	_, err = ZZ2.Add(vbig, v12)
	assert.Error(err)
}

// TestSum tests Sum
func TestSum(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero()
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v24, err := FromIntSlice(ZZ2, []int{2, 4})
	assert.NoError(err)
	vbig, err := ConstantVector(ZZ2, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	v111, err := FromIntSlice(ZZ3, []int{1, 1, 1})
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	// empty sum
	if v, err := ZZ2.Sum(); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
	}
	// one-element sum
	if v, err := ZZ2.Sum(v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v12)
	}
	// two-element sum
	if v, err := ZZ2.Sum(v12, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v24)
	}
	// bigger sum
	if v, err := ZZ2.Sum(v12, v12, v24, zero, v12); assert.NoError(err) {
		assert.Equal(v.(*Element).ToUint64Slice(), []uint64{5, 10})
	}
	// test overflow near the boundary of the uint64 range
	_, err = ZZ2.Sum(v12, vbig, v12)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = ZZ2.Sum(v111)
	assert.Error(err)
	// two-element sum
	_, err = ZZ2.Sum(v111, v24)
	assert.Error(err)
	_, err = ZZ2.Sum(v24, v111)
	assert.Error(err)
	// bigger sum
	_, err = ZZ2.Sum(v12, v111, v24, zero, v12)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// the nil case
	//////////////////////////////////////////////////////////////////////
	nilZero := nilLattice.Zero()
	// empty sum
	if v, err := nilLattice.Sum(); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// one-element sum
	if v, err := nilLattice.Sum(nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// two-element sum
	if v, err := nilLattice.Sum(nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	// bigger sum
	if v, err := nilLattice.Sum(nilZero, nilZero, nilZero, nilZero, nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
	}
	//////////////////////////////////////////////////////////////////////
	// cases where one of the elements is in the wrong parent
	//////////////////////////////////////////////////////////////////////
	// one-element sum
	_, err = ZZ2.Sum(v111)
	assert.Error(err)
	_, err = nilLattice.Sum(v111)
	assert.Error(err)
	// two-element sum
	_, err = ZZ2.Sum(v111, v24)
	assert.Error(err)
	_, err = ZZ2.Sum(v24, v111)
	assert.Error(err)
	_, err = nilLattice.Sum(v111, nilZero)
	assert.Error(err)
	_, err = nilLattice.Sum(nilZero, v111)
	assert.Error(err)
	// bigger sum
	_, err = ZZ2.Sum(v12, v111, v24, zero, v12)
	assert.Error(err)
	_, err = nilLattice.Sum(nilZero, nilZero, nilZero, zero, nilZero)
	assert.Error(err)
}

// TestScalarMultiplyByInteger tests ScalarMultiplyByInteger
func TestScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero()
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	v36, err := FromIntSlice(ZZ2, []int{3, 6})
	assert.NoError(err)
	v456, err := FromIntSlice(ZZ3, []int{4, 5, 6})
	assert.NoError(err)
	var nilVector *Element
	nilZero := Zero(nilLattice)
	//////////////////////////////////////////////////////////////////////
	// tests that should pass
	//////////////////////////////////////////////////////////////////////
	if v, err := ZZ2.ScalarMultiplyByInteger(integer.One(), v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v12)
		assert.AreEqual(ZZ2, v12, v)
	}
	if v, err := ZZ2.ScalarMultiplyByInteger(integer.FromInt(3), v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v36)
		assert.AreEqual(ZZ2, v36, v)
	}
	if v, err := ZZ2.ScalarMultiplyByInteger(integer.FromInt(4), zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
		assert.AreEqual(ZZ2, zero, v)
	}
	if v, err := ZZ2.ScalarMultiplyByInteger(integer.FromInt(-4), zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
		assert.AreEqual(ZZ2, zero, v)
	}
	if v, err := ZZ2.ScalarMultiplyByInteger(integer.One(), zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
		assert.AreEqual(ZZ2, zero, v)
	}
	//////////////////////////////////////////////////////////////////////
	// tests that should fail
	//////////////////////////////////////////////////////////////////////
	_, err = ZZ2.ScalarMultiplyByInteger(integer.FromInt(-1), v12)
	assert.Error(err)
	_, err = ZZ2.ScalarMultiplyByInteger(integer.FromInt(3), v456)
	assert.Error(err)
	_, err = ZZ2.ScalarMultiplyByInteger(integer.FromInt(1), v456)
	assert.Error(err)
	_, err = ZZ2.ScalarMultiplyByInteger(integer.FromInt(3), nilVector)
	assert.Error(err)
	_, err = ZZ2.ScalarMultiplyByInteger(integer.FromInt(1), nilVector)
	assert.Error(err)
	_, err = nilLattice.ScalarMultiplyByInteger(integer.FromInt(1), v12)
	assert.Error(err)
	_, err = nilLattice.ScalarMultiplyByInteger(integer.FromInt(4), v12)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// nil cases
	//////////////////////////////////////////////////////////////////////
	if v, err := nilLattice.ScalarMultiplyByInteger(integer.One(), nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, nilZero, v)
		assert.AreEqual(nilLattice, nilVector, v)
	}
	if v, err := nilLattice.ScalarMultiplyByInteger(integer.FromInt(-3), nilZero); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilZero)
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, nilZero, v)
		assert.AreEqual(nilLattice, nilVector, v)
	}

}

// TestChangeParent tests ChangeParent
func TestChangeParent(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero()
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	var nilVector *Element
	//////////////////////////////////////////////////////////////////////
	// changing parents from ZZ2 and the nil lattice to ZZ3 should fail
	//////////////////////////////////////////////////////////////////////
	_, err = ChangeParent(ZZ3, zero)
	assert.Error(err)
	_, err = ChangeParent(ZZ3, v12)
	assert.Error(err)
	_, err = ChangeParent(ZZ3, nilVector)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// changing parents from non-nil lattices into the nil lattice should
	// fail
	//////////////////////////////////////////////////////////////////////
	_, err = ChangeParent(nilLattice, zero)
	assert.Error(err)
	_, err = ChangeParent(nilLattice, v12)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// changing parents from nil lattices into the nil lattice should
	// succeed
	//////////////////////////////////////////////////////////////////////
	if v, err := ChangeParent(nilLattice, nilVector); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, Zero(nilLattice))
	}
	if v, err := ChangeParent(nilLattice, Zero(nilLattice)); assert.NoError(err) {
		assert.AreEqual(nilLattice, v, nilVector)
		assert.AreEqual(nilLattice, v, Zero(nilLattice))
	}
	//////////////////////////////////////////////////////////////////////
	// changing parents back to the same parent should succeed and give
	// the original object
	//////////////////////////////////////////////////////////////////////
	if v, err := ChangeParent(ZZ2, zero); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, zero)
		assert.True(v == zero)
	}
	if v, err := ChangeParent(ZZ2, v12); assert.NoError(err) {
		assert.AreEqual(ZZ2, v, v12)
		assert.True(v == v12)
	}
	//////////////////////////////////////////////////////////////////////
	// test changing parents from another lattice
	//////////////////////////////////////////////////////////////////////
	L, err := NewLattice(3)
	assert.NoError(err)
	if v, err := ChangeParent(ZZ3, Zero(L)); assert.NoError(err) {
		assert.AreEqual(ZZ3, v, Zero(ZZ3))
		assert.False(v == Zero(L))
	}
	v1, err := FromIntSlice(L, []int{5, 6, 7})
	assert.NoError(err)
	v2, err := FromIntSlice(ZZ3, []int{5, 6, 7})
	assert.NoError(err)
	if v, err := ChangeParent(ZZ3, v1); assert.NoError(err) {
		assert.AreEqual(ZZ3, v, v2)
		assert.False(v == v2)
	}
	//////////////////////////////////////////////////////////////////////
	// test changing parents from a space of integer vectors
	//////////////////////////////////////////////////////////////////////
	L2, err := integervector.DefaultLattice(3)
	assert.NoError(err)
	v3, err := integervector.FromIntSlice(L2, []int{5, 6, 7})
	assert.NoError(err)
	if v, err := ChangeParent(ZZ3, v3); assert.NoError(err) {
		assert.AreEqual(ZZ3, v, v2)
	}
	if v, err := ChangeParent(ZZ3, L2.Zero()); assert.NoError(err) {
		assert.AreEqual(ZZ3, v, Zero(ZZ3))
	}
	// test overflow near the range boundaries
	vbig := integervector.ConstantVector(L2, integer.FromUint64(math.MaxUint64))
	vbigger := integervector.ConstantVector(L2, integer.FromUint64(math.MaxUint64).Increment())
	_, err = ChangeParent(ZZ3, vbig)
	assert.NoError(err)
	_, err = ChangeParent(ZZ3, vbigger)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// test the one-dimensional case
	//////////////////////////////////////////////////////////////////////
	ZZ1, err := DefaultLattice(1)
	assert.NoError(err)
	v7, err := FromIntSlice(ZZ1, []int{7})
	assert.NoError(err)
	_, err = FromIntSlice(ZZ1, []int{-2})
	assert.Error(err)
	if v, err := ChangeParent(ZZ1, integer.FromInt(7)); assert.NoError(err) {
		assert.AreEqual(ZZ1, v7, v)
	}
	// this should fail because it can't be converted to an integer
	twoThirds, err := rational.FromString("2/3")
	assert.NoError(err)
	_, err = ChangeParent(ZZ1, twoThirds)
	assert.Error(err)
	// test overflow near the boundaries of the range
	_, err = ChangeParent(ZZ1, integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	_, err = ChangeParent(ZZ1, integer.FromUint64(math.MaxUint64).Increment())
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// FIX ME need to test the toIntegerSlicer and
	// toIntegerSlicerWithError code paths, but right now there aren't
	// any types that implement these interfaces other than uint64vector.
	// Need to write tests for these paths once we have meaningful
	// objects to test on.
	//////////////////////////////////////////////////////////////////////
}

// TestProjectionMap tests ProjectionMap
func TestProjectionMap(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some lattices
	//////////////////////////////////////////////////////////////////////
	ZZ2, err := DefaultLattice(2)
	assert.NoError(err)
	ZZ3, err := DefaultLattice(3)
	assert.NoError(err)
	var nilLattice *Parent
	//////////////////////////////////////////////////////////////////////
	// some vectors
	//////////////////////////////////////////////////////////////////////
	zero := ZZ2.Zero()
	v12, err := FromIntSlice(ZZ2, []int{1, 2})
	assert.NoError(err)
	var nilElement *Element
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	for i := 0; i < 2; i++ {
		f, err := ZZ2.ProjectionMap(i)
		assert.NoError(err)
		assert.Equal(f.Domain(), ZZ2)
		assert.Equal(f.Codomain(), integer.Ring())
		if x, err := f.Evaluate(v12); assert.NoError(err) {
			assert.True(x.(*integer.Element).IsEqualTo(v12.EntryOrPanic(i)))
		}
		if x, err := f.Evaluate(zero); assert.NoError(err) {
			assert.True(x.(*integer.Element).IsZero())
		}
		_, err = f.Evaluate(Zero(ZZ3))
		assert.Error(err)
		_, err = f.Evaluate(nilElement)
		assert.Error(err)
		_, err = f.Evaluate(Zero(nilLattice))
		assert.Error(err)
	}
	//////////////////////////////////////////////////////////////////////
	// test out-of-range errors
	//////////////////////////////////////////////////////////////////////
	_, err = ZZ2.ProjectionMap(3)
	assert.Error(err)
	_, err = ZZ2.ProjectionMap(-1)
	assert.Error(err)
	_, err = nilLattice.ProjectionMap(0)
	assert.Error(err)
}
