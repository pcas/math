// Cache maintains a cache of small uint64-valued elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"math"
)

// The maximum integer value stored in the cache.
const (
	cacheMax = math.MaxInt8
)

// A cache of *Element objects associated with the uint64 values.
var cache []*Element

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// initElementCache initialises the cache of small-valued *Element objects.
func init() {
	// Create the integers cache
	cache = make([]*Element, 0, cacheMax+1)
	for n := uint64(0); n <= cacheMax; n++ {
		cache = append(cache, &Element{n: n})
	}
}

/////////////////////////////////////////////////////////////////////////
// Cache access functions
/////////////////////////////////////////////////////////////////////////

// elementFromCache returns the *Element in the cache corresponding to the given value, or nil if the value is outside the cache range.
func elementFromCache(n uint64) *Element {
	if n < 0 || n > cacheMax {
		return nil
	}
	return cache[int(n)]
}
