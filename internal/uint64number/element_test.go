// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"bitbucket.org/pcas/math/integer"
	"github.com/stretchr/testify/assert"
	"math"
	"strconv"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// assertAreEqual asserts that i == i in many different ways.
func assertAreEqual(assert *assert.Assertions, i uint64) {
	a := FromUint64(i)
	if b, err := FromInt(int(i)); assert.NoError(err) {
		assert.True(a.IsEqualTo(b))
	}
	if b, err := FromInt32(int32(i)); assert.NoError(err) {
		assert.True(a.IsEqualTo(b))
	}
	if b, err := FromInt64(int64(i)); assert.NoError(err) {
		assert.True(a.IsEqualTo(b))
	}
	if b, err := FromInteger(integer.FromUint64(i)); assert.NoError(err) {
		assert.True(a.IsEqualTo(b))
	}
	assert.True(a.IsEqualToUint64(i))
	assert.True(AreEqual(a, FromUint64(i)))
	assert.False(a.IsEqualTo(FromUint64(i + 1)))
	assert.False(a.IsEqualToUint64(i + 1))
	if b, err := FromInt(int(i + 1)); assert.NoError(err) {
		assert.False(AreEqual(a, b))
	}
}

/////////////////////////////////////////////////////////////////////////
// Tests
/////////////////////////////////////////////////////////////////////////

// TestElement_Creation tests that FromInteger, FromInt, FromInt32, and FromInt64 error when expected.
func TestElement_Creation(t *testing.T) {
	assert := assert.New(t)
	// Test around the range boundaries
	_, err := FromInteger(integer.FromUint64(math.MaxUint64))
	assert.NoError(err)
	_, err = FromInteger(integer.FromUint64(math.MaxUint64).Increment())
	assert.Error(err)
	_, err = FromInteger(integer.Zero())
	assert.NoError(err)
	_, err = FromInteger(integer.FromInt64(-1))
	assert.Error(err)
	_, err = FromInt(0)
	assert.NoError(err)
	_, err = FromInt(-27)
	assert.Error(err)
	_, err = FromInt32(0)
	assert.NoError(err)
	_, err = FromInt32(-2)
	assert.Error(err)
	_, err = FromInt64(0)
	assert.NoError(err)
	_, err = FromInt64(-7)
	assert.Error(err)
}

// TestElement_IsEqualTo tests Element's IsEqualTo and IsEqualToUint64 methods, and the AreEqual function.
func TestElement_IsEqualTo(t *testing.T) {
	assert := assert.New(t)
	// Run the tests
	for i := uint64(0); i <= 10; i++ {
		assertAreEqual(assert, i)
		assertAreEqual(assert, i+cacheMax+1)
	}
}

// TestElement_String tests Element's String and FromString method.
func TestElement_String(t *testing.T) {
	assert := assert.New(t)
	// String should return the integer as a string
	for i := -10; i <= 10; i++ {
		xStr := strconv.Itoa(i)
		if i < 0 {
			_, err := FromString(xStr)
			assert.Error(err)
		} else {
			x, err := FromInt(i)
			assert.NoError(err)
			assert.Equal(x.String(), xStr)
			if y, err := FromString(x.String()); assert.NoError(err) {
				assert.True(x.IsEqualTo(y))
			}
		}
	}
	for i := 1; i <= 10; i++ {
		assert.Equal(FromUint64(uint64(cacheMax+i)).String(), strconv.Itoa(cacheMax+i))
	}
	// Nonsense strings should return an error
	_, err := FromString(" - + 10 ")
	assert.Error(err)
}

// TestElement_Hash tests Element's Hash method.
func TestElement_Hash(t *testing.T) {
	// Equal objects should have equal hashes
	for i := uint64(0); i <= 10; i++ {
		assert.Equal(t, FromUint64(i).Hash(), FromUint64(i).Hash())
	}
	for i := uint64(1); i <= 10; i++ {
		assert.Equal(t, FromUint64(cacheMax+i).Hash(), FromUint64(cacheMax+i).Hash())
	}
}

// TestElement_Parent tests Element's Parent method.
func TestElement_Parent(t *testing.T) {
	// Check that an element of R returns R as the parent
	for i := uint64(0); i <= 10; i++ {
		assert.Equal(t, FromUint64(i).Parent(), Set())
	}
	for i := uint64(1); i <= 10; i++ {
		assert.Equal(t, FromUint64(cacheMax+i).Parent(), Set())
	}
}

// TestElement_IsZero tests Element's IsZero method.
func TestElement_IsZero(t *testing.T) {
	// Only 0 should be zero
	for i := uint64(0); i <= 10; i++ {
		if i == 0 {
			assert.True(t, FromUint64(i).IsZero())
		} else {
			assert.False(t, FromUint64(i).IsZero())
		}
	}
}

// TestElement_IsOne tests Element's IsOne method.
func TestElement_IsOne(t *testing.T) {
	// Only 1 should be one
	for i := uint64(0); i <= 10; i++ {
		if i == 1 {
			assert.True(t, FromUint64(i).IsOne())
		} else {
			assert.False(t, FromUint64(i).IsOne())
		}
	}
}

// TestElement_Int64 tests Element's Int64, Uint64, and ToInteger methods.
func TestElement_Int64(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(FromUint64(0).Uint64(), uint64(0))
	assert.Equal(FromUint64(1).Uint64(), uint64(1))
	assert.Equal(FromUint64(510).Uint64(), uint64(510))
	if n, err := FromUint64(0).Int64(); assert.NoError(err) {
		assert.Equal(n, int64(0))
	}
	if n, err := FromUint64(1).Int64(); assert.NoError(err) {
		assert.Equal(n, int64(1))
	}
	assert.True(FromUint64(0).ToInteger().IsZero())
	assert.True(FromUint64(1).ToInteger().IsOne())
	assert.True(FromUint64(510).ToInteger().IsEqualToInt64(510))
	// Test near the boundary of the int64 range
	if n, err := FromUint64(math.MaxInt64).Int64(); assert.NoError(err) {
		assert.Equal(n, int64(math.MaxInt64))
	}
	_, err := FromUint64(math.MaxInt64 + 1).Int64()
	assert.Error(err)
}

// TestSubtract tests Subtract.
func TestSubtract(t *testing.T) {
	assert := assert.New(t)
	R := Set()
	// The tests to perform, presented in the form {a, b, a - b}
	tests := [][]uint64{
		{0, 0, 0},
		{2, 1, 1},
		{1, 0, 1},
		{5, 5, 0},
		{6, 1, 5},
	}
	// Run the tests
	for _, test := range tests {
		a := FromUint64(test[0])
		b := FromUint64(test[1])
		if res, err := Subtract(a, b); assert.NoError(err) {
			if ok, err := R.AreEqual(res, FromUint64(test[2])); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Results out of range should return an error
	_, err := Subtract(Zero(), One())
	assert.Error(err)
	// test the nil case
	if x, err := Subtract(One(), nil); assert.NoError(err) {
		assert.True(x.IsOne())
	}
	if x, err := Subtract(nil, Zero()); assert.NoError(err) {
		assert.True(x.IsZero())
	}
}

// TestComparison tests IsGreaterThan, IsGreaterThanOrEqualTo, IsLessThan, IsLessThanOrEqualTo, and IsPositive
func TestComparison(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	zero := FromUint64(0)
	three := FromUint64(3)
	four := FromUint64(4)
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThan
	//////////////////////////////////////////////////////////////////////
	assert.False(zero.IsGreaterThan(zero))
	assert.False(zero.IsGreaterThan(three))
	assert.False(zero.IsGreaterThan(four))
	assert.True(three.IsGreaterThan(zero))
	assert.False(three.IsGreaterThan(three))
	assert.False(three.IsGreaterThan(four))
	assert.True(four.IsGreaterThan(zero))
	assert.True(four.IsGreaterThan(three))
	assert.False(four.IsGreaterThan(four))
	//////////////////////////////////////////////////////////////////////
	// tests for IsGreaterThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsGreaterThanOrEqualTo(zero))
	assert.False(zero.IsGreaterThanOrEqualTo(three))
	assert.False(zero.IsGreaterThanOrEqualTo(four))
	assert.True(three.IsGreaterThanOrEqualTo(zero))
	assert.True(three.IsGreaterThanOrEqualTo(three))
	assert.False(three.IsGreaterThanOrEqualTo(four))
	assert.True(four.IsGreaterThanOrEqualTo(zero))
	assert.True(four.IsGreaterThanOrEqualTo(three))
	assert.True(four.IsGreaterThanOrEqualTo(four))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThan
	//////////////////////////////////////////////////////////////////////
	assert.False(zero.IsLessThan(zero))
	assert.True(zero.IsLessThan(three))
	assert.True(zero.IsLessThan(four))
	assert.False(three.IsLessThan(zero))
	assert.False(three.IsLessThan(three))
	assert.True(three.IsLessThan(four))
	assert.False(four.IsLessThan(zero))
	assert.False(four.IsLessThan(three))
	assert.False(four.IsLessThan(four))
	//////////////////////////////////////////////////////////////////////
	// tests for IsLessThanOrEqualTo
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsLessThanOrEqualTo(zero))
	assert.True(zero.IsLessThanOrEqualTo(three))
	assert.True(zero.IsLessThanOrEqualTo(four))
	assert.False(three.IsLessThanOrEqualTo(zero))
	assert.True(three.IsLessThanOrEqualTo(three))
	assert.True(three.IsLessThanOrEqualTo(four))
	assert.False(four.IsLessThanOrEqualTo(zero))
	assert.False(four.IsLessThanOrEqualTo(three))
	assert.True(four.IsLessThanOrEqualTo(four))
	//////////////////////////////////////////////////////////////////////
	// tests for IsPositive
	//////////////////////////////////////////////////////////////////////
	assert.False(zero.IsPositive())
	assert.True(three.IsPositive())
	assert.True(four.IsPositive())
}

// TestEquality tests IsEqualTo and IsEqualToUint64
func TestEquality(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	zero := FromUint64(0)
	three := FromUint64(3)
	four := FromUint64(4)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	assert.True(zero.IsEqualTo(Zero()))
	assert.False(zero.IsEqualTo(three))
	assert.False(three.IsEqualTo(zero))
	assert.False(three.IsEqualTo(four))
	assert.True(zero.IsEqualToUint64(0))
	assert.False(zero.IsEqualToUint64(3))
	assert.False(three.IsEqualToUint64(4))
	assert.False(three.IsEqualToUint64(0))
}

// TestIncrementDecrement tests Increment and Decrement
func TestIncrementDecrement(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// a big number
	//////////////////////////////////////////////////////////////////////
	big := FromUint64(math.MaxUint64)
	//////////////////////////////////////////////////////////////////////
	// tests
	//////////////////////////////////////////////////////////////////////
	if x, err := FromUint64(0).Increment(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromUint64(1)))
	}
	if x, err := FromUint64(1).Decrement(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromUint64(0)))
	}
	if x, err := FromUint64(3).Increment(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromUint64(4)))
	}
	if x, err := FromUint64(4).Decrement(); assert.NoError(err) {
		assert.True(x.IsEqualTo(FromUint64(3)))
	}
	_, err := big.Increment()
	assert.Error(err)
	_, err = big.Decrement()
	assert.NoError(err)
	_, err = Zero().Decrement()
	assert.Error(err)
}

// TestMultiplyThen tests MultiplyThenAdd and MultiplyThenSubtract
func TestMultiplyThen(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// some numbers
	//////////////////////////////////////////////////////////////////////
	tests := [][]uint64{
		[]uint64{0, 2, 7},
		[]uint64{2, 0, 7},
		[]uint64{5, 2, 0},
		[]uint64{93, 2652, 71},
	}
	//////////////////////////////////////////////////////////////////////
	// run the tests
	//////////////////////////////////////////////////////////////////////
	for _, T := range tests {
		if x, err := MultiplyThenAdd(FromUint64(T[0]), FromUint64(T[1]), FromUint64(T[2])); assert.NoError(err) {
			assert.True(x.IsEqualTo(FromUint64(T[0]*T[1] + T[2])))
		}
		if T[0]*T[1] >= T[2] {
			if x, err := MultiplyThenSubtract(FromUint64(T[0]), FromUint64(T[1]), FromUint64(T[2])); assert.NoError(err) {
				assert.True(x.IsEqualTo(FromUint64(T[0]*T[1] - T[2])))
			}
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test overflow near the boundary of the int64 range
	//////////////////////////////////////////////////////////////////////
	// MultiplyThenAdd
	_, err := MultiplyThenAdd(FromUint64(2), FromUint64(math.MaxUint64), FromUint64(0))
	assert.Error(err)
	_, err = MultiplyThenAdd(FromUint64(1), FromUint64(math.MaxUint64), FromUint64(0))
	assert.NoError(err)
	_, err = MultiplyThenAdd(FromUint64(1), FromUint64(math.MaxUint64), FromUint64(1))
	assert.Error(err)
	// MultiplyThenSubtract
	_, err = MultiplyThenSubtract(FromUint64(2), FromUint64(math.MaxUint64), FromUint64(0))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromUint64(2), FromUint64(math.MaxUint64), FromUint64(math.MaxUint64))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromUint64(1), FromUint64(math.MaxUint64), FromUint64(0))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromUint64(1), FromUint64(math.MaxUint64), FromUint64(1))
	assert.NoError(err)
	_, err = MultiplyThenSubtract(FromUint64(3), FromUint64(7), FromUint64(100))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromUint64(0), FromUint64(7), FromUint64(100))
	assert.Error(err)
	_, err = MultiplyThenSubtract(FromUint64(3), FromUint64(0), FromUint64(100))
	assert.Error(err)
}
