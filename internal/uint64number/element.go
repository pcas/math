// Element defines uint64-valued elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"math"
	"strconv"
)

// Element is an uint64-valued element.
type Element struct {
	n uint64 // The value
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Zero returns the additive identity element.
func Zero() *Element {
	return FromUint64(0)
}

// One returns the multiplicative identity element.
func One() *Element {
	return FromUint64(1)
}

// FromInt returns a new uint64-valued element, or an error if n is negative.
func FromInt(n int) (*Element, error) {
	if n < 0 {
		return nil, errors.IllegalNegativeArg.New()
	}
	return FromUint64(uint64(n)), nil
}

// FromInt32 returns a new uint64-valued element, or an error if n is negative.
func FromInt32(n int32) (*Element, error) {
	if n < 0 {
		return nil, errors.IllegalNegativeArg.New()
	}
	return FromUint64(uint64(n)), nil
}

// FromUint64 returns a new uint64-valued element.
func FromUint64(n uint64) *Element {
	if x := elementFromCache(n); x != nil {
		return x
	}
	return &Element{n: n}
}

// FromInt64 returns a new uint64-valued element, or an error if n is negative.
func FromInt64(n int64) (*Element, error) {
	if n < 0 {
		return nil, errors.IllegalNegativeArg.New()
	}
	return FromUint64(uint64(n)), nil
}

// FromInteger returns a new uint64-valued element, or an error if out of range.
func FromInteger(n *integer.Element) (*Element, error) {
	k, err := n.Uint64()
	if err != nil {
		return nil, err
	}
	return FromUint64(k), nil
}

// FromString returns the uint64-valued element represented by the string s, or an error if s is malformed or the value is out of range.
func FromString(s string) (*Element, error) {
	n, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return nil, err
	}
	return FromUint64(n), nil
}

// Add returns the sum x + y, or an error if the result is out of range.
func Add(x *Element, y *Element) (*Element, error) {
	if x.IsZero() {
		return y, nil
	} else if y.IsZero() {
		return x, nil
	}
	z, ok := mathutil.AddUint64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(z), nil
}

// Subtract returns the difference x - y, or an error if the result is out of range.
func Subtract(x *Element, y *Element) (*Element, error) {
	if y.IsZero() {
		return x, nil
	}
	z, ok := mathutil.SubtractUint64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(z), nil
}

// Multiply returns the product x * y, or an error if the result is out of range.
func Multiply(x *Element, y *Element) (*Element, error) {
	if x.IsZero() || y.IsOne() {
		return x, nil
	} else if y.IsZero() || x.IsOne() {
		return y, nil
	}
	z, ok := mathutil.MultiplyUint64(x.n, y.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(z), nil
}

// MultiplyThenAdd returns x * y + z, or an error if out of range
func MultiplyThenAdd(x *Element, y *Element, z *Element) (*Element, error) {
	// Deal with the easy cases
	if z.IsZero() {
		return Multiply(x, y)
	} else if x.IsZero() || y.IsZero() {
		return z, nil
	}
	// Do the computation, checking for overflow
	if result, ok := mathutil.MultiplyUint64(x.n, y.n); ok {
		if result, ok = mathutil.AddUint64(result, z.n); ok {
			return FromUint64(result), nil
		}
	}
	// we overflowed
	return nil, errors.OutOfRange.New()
}

// MultiplyThenSubtract returns x * y - z, or an error if out of range
func MultiplyThenSubtract(x *Element, y *Element, z *Element) (*Element, error) {
	// Deal with the easy cases
	if z.IsZero() {
		return Multiply(x, y)
	} else if x.IsZero() || y.IsZero() {
		return nil, errors.OutOfRange.New()
	}
	// First we try this as uint64s
	if result, ok := mathutil.MultiplyUint64(x.n, y.n); ok {
		if result, ok = mathutil.SubtractUint64(result, z.n); ok {
			return FromUint64(result), nil
		}
	}
	// No good -- try as integers and see if we can convert back
	if result, err := integer.MultiplyThenSubtract(integer.FromUint64(x.n), integer.FromUint64(y.n), integer.FromUint64(z.n)).Uint64(); err != nil {
		return nil, err
	} else {
		return FromUint64(result), nil
	}

}

// PowerInt64 returns x^k, or an error if the result is out of range.
func PowerInt64(x *Element, k int64) (*Element, error) {
	if k == 0 || x.IsOne() {
		return One(), nil
	} else if k == 1 || (x.IsZero() && k > 0) {
		return x, nil
	}
	z, err := integer.PowerInt64(x.ToInteger(), k)
	if err != nil {
		return nil, err
	}
	return FromInteger(z)
}

// Power returns x^k, or an error if the result is out of range.
func Power(x *Element, k *integer.Element) (*Element, error) {
	if k.IsZero() || x.IsOne() {
		return One(), nil
	} else if k.IsOne() || (x.IsZero() && k.IsPositive()) {
		return x, nil
	}
	z, err := integer.Power(x.ToInteger(), k)
	if err != nil {
		return nil, err
	}
	return FromInteger(z)
}

// AreEqual returns true iff x equals y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func Cmp(x *Element, y *Element) int {
	if x == y {
		return 0
	} else if x.n > y.n {
		return 1
	} else if x.n < y.n {
		return -1
	}
	return 0
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff x is the additive identity element.
func (x *Element) IsZero() bool {
	return x == nil || x.n == 0
}

// IsOne returns true iff x is the multiplicative identity element.
func (x *Element) IsOne() bool {
	return x != nil && x.n == 1
}

// IsEqualTo returns true iff x is equal to y.
func (x *Element) IsEqualTo(y *Element) bool {
	if x.IsZero() {
		return y.IsZero()
	} else if y.IsZero() {
		return false
	}
	return x.n == y.n
}

// IsEqualToUint64 returns true iff x is equal to y.
func (x *Element) IsEqualToUint64(y uint64) bool {
	if x.IsZero() {
		return y == 0
	} else if y == 0 {
		return false
	}
	return x.n == y
}

// IsGreaterThan returns true iff x > y.
func (x *Element) IsGreaterThan(y *Element) bool {
	if x == y {
		return false
	} else if x.IsZero() {
		return false
	} else if y.IsZero() {
		return x.n > 0
	}
	return x.n > y.n
}

// IsGreaterThanOrEqualTo returns true iff x >= y.
func (x *Element) IsGreaterThanOrEqualTo(y *Element) bool {
	return !x.IsLessThan(y)
}

// IsLessThan returns true iff x < y.
func (x *Element) IsLessThan(y *Element) bool {
	if x == y {
		return false
	} else if y.IsZero() {
		return false
	} else if x.IsZero() {
		return y.n > 0
	}
	return x.n < y.n
}

// IsLessThanOrEqualTo returns true iff x <= y.
func (x *Element) IsLessThanOrEqualTo(y *Element) bool {
	return !x.IsGreaterThan(y)
}

// IsPositive returns true iff x > 0.
func (x *Element) IsPositive() bool {
	return x != nil && x.n > 0
}

// Increment returns x + 1, or an error if out of range.
func (x *Element) Increment() (*Element, error) {
	if x.IsZero() {
		return One(), nil
	} else if x.n == math.MaxUint64 {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(x.n + 1), nil
}

// Decrement returns x - 1, or an error if out of range.
func (x *Element) Decrement() (*Element, error) {
	if x.IsZero() {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(x.n - 1), nil
}

// ScalarMultiplyByInteger returns n * x, or an error if out of range.
func (x *Element) ScalarMultiplyByInteger(n *integer.Element) (*Element, error) {
	if x.IsZero() {
		return x, nil
	}
	k, err := n.Uint64()
	if err != nil {
		return nil, err
	}
	return x.ScalarMultiplyByUint64(k)
}

// ScalarMultiplyByUint64 returns n * x, or an error if out of range.
func (x *Element) ScalarMultiplyByUint64(n uint64) (*Element, error) {
	if x.IsZero() || n == 1 {
		return x, nil
	} else if n == 0 {
		return Zero(), nil
	}
	z, ok := mathutil.MultiplyUint64(n, x.n)
	if !ok {
		return nil, errors.OutOfRange.New()
	}
	return FromUint64(z), nil
}

// Uint64 returns the value as an uint64.
func (x *Element) Uint64() uint64 {
	if x.IsZero() {
		return 0
	}
	return x.n
}

// Int64 returns the value as a int64, or an error if out of range.
func (x *Element) Int64() (int64, error) {
	if x.IsZero() {
		return 0, nil
	} else if x.n > math.MaxInt64 {
		return 0, errors.OutOfRange.New()
	}
	return int64(x.n), nil
}

// ToInteger returns the value as an integer.
func (x *Element) ToInteger() *integer.Element {
	if x.IsZero() {
		return integer.Zero()
	}
	return integer.FromUint64(x.n)
}

// Parent returns the parent of the element.
func (x *Element) Parent() object.Parent {
	return nN
}

// Hash returns a hash value for the element.
func (x *Element) Hash() hash.Value {
	if x.IsZero() {
		return 0
	}
	return hash.Uint64(x.n)
}

// String returns a string representation of the element.
func (x *Element) String() string {
	if x.IsZero() {
		return "0"
	}
	return strconv.FormatUint(x.n, 10)
}
