// Tests of uint64-valued elements gob encoding/decoding.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"bytes"
	"encoding/gob"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobEncoding tests gob encoding/decoding.
func TestGobEncoding(t *testing.T) {
	assert := assert.New(t)
	// Work through the tests
	for i := uint64(0); i <= 10; i++ {
		x := FromUint64(i)
		// Create the encoder and decoder
		var b bytes.Buffer
		enc := gob.NewEncoder(&b)
		dec := gob.NewDecoder(&b)
		// Encode the element
		if assert.NoError(enc.Encode(x)) {
			// Decode the element
			if y, err := GobDecode(dec); assert.NoError(err) {
				assert.True(x.IsEqualTo(y))
			}
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var x *Element
	if b, err := x.GobEncode(); assert.NoError(err) {
		// Decoding should give zero
		y := &Element{}
		if assert.NoError(y.GobDecode(b)) {
			assert.True(y.IsZero())
		}
	}
}

// TestGobDecodeEmpty tests gob decoding an empty slice of bytes
func TestGobDecodeEmpty(t *testing.T) {
	assert := assert.New(t)
	// This should give zero
	x := &Element{}
	if assert.NoError(x.GobDecode(nil)) {
		assert.True(x.IsZero())
	}
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	assert := assert.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode a value
	if assert.NoError(enc.Encode(One())) {
		// Try to decode directly into a nil object -- this should error
		var x *Element
		assert.Error(x.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	assert := assert.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5}
	dec := gob.NewDecoder(bytes.NewReader(b))
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(dec)
	assert.Error(err)
	x := &Element{}
	assert.Error(x.GobDecode(b))
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	assert := assert.New(t)
	// Encode a version number from the future
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	if assert.NoError(enc.Encode(byte(encodingVersion + 1))) {
		// Try decoding -- this should error
		x := &Element{}
		assert.Error(x.GobDecode(b.Bytes()))
	}
}

// TestGobDecodeExisting tests gob decoding into an existing element.
func TestGobDecodeExisting(t *testing.T) {
	require := require.New(t)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode a value
	require.NoError(enc.Encode(One()))
	// Try to decode directly into an existing value -- this should error
	require.Error(Zero().GobDecode(b.Bytes()))
}

// TestGobSliceEncoding tests gob encoding/decoding of slices.
func TestGobSliceEncoding(t *testing.T) {
	assert := assert.New(t)
	// The slices  to test
	tests := [][]uint64{
		{0},
		{1},
		{2},
		{0, 1},
		{0, 0, 10},
		{0, 0, 0, 1, 1},
		{1, 0, 0, 1, 510, 3},
		{25, 2, 0, 0, 0, 0, 0, 3, 0, 0, 1},
	}
	// Work through the tests
	for _, test := range tests {
		// Create the slice
		S := make([]*Element, 0, len(test))
		for _, i := range test {
			S = append(S, FromUint64(i))
		}
		// Create the encoder and decoder
		var b bytes.Buffer
		enc := gob.NewEncoder(&b)
		dec := gob.NewDecoder(&b)
		// Encode the slice
		if assert.NoError(enc.Encode(S)) {
			// Decode the slice
			if T, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Equal(len(T), len(S)) {
				// Check for equality
				for i, x := range S {
					assert.True(x.IsEqualTo(T[i]))
				}
			}
		}
	}
}
