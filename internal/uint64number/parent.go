// Parent defines the parent for uint64-valued integers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
)

// Parent is the (unique) set of uint64-valued elements.
type Parent struct{}

// nN is the (unique) set of uint64-valued elements.
var nN = Parent{}

// The string representation of the set of uint64-valued elements.
const defaultName = "{uint64}"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// argsToElements returns the object.Elements x and y as Elements in nN, if that's where they belong.
func argsToElements(x object.Element, y object.Element) (*Element, *Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, nil, errors.Arg1NotContainedInParent.New()
	}
	yy, err := ToElement(y)
	if err != nil {
		return nil, nil, errors.Arg2NotContainedInParent.New()
	}
	return xx, yy, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Zero returns the additive identity element.
func (R Parent) Zero() object.Element {
	return Zero()
}

// One returns the multiplicative identity element.
func (R Parent) One() object.Element {
	return One()
}

// IsZero returns true iff x is the additive identity element.
func (R Parent) IsZero(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// IsOne returns true iff x is the multiplicative identity element.
func (R Parent) IsOne(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsOne(), nil
}

// Add returns x + y, or an error if the result is out of range.
func (R Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := argsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy)
}

// Multiply returns the product x * y, or an error if the result is out of range.
func (R Parent) Multiply(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := argsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Multiply(xx, yy)
}

// Power returns x^k, or an error if the result is out of range.
func (R Parent) Power(x object.Element, k *integer.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, err
	}
	return Power(xx, k)
}

// ScalarMultiplyByInteger returns the product n * x, or an error if the result is out of range.
func (R Parent) ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, errors.Arg2NotContainedInParent.New()
	}
	return xx.ScalarMultiplyByInteger(n)
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (R Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := argsToElements(x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func (R Parent) Cmp(x object.Element, y object.Element) (int, error) {
	xx, yy, err := argsToElements(x, y)
	if err != nil {
		return 0, err
	}
	return Cmp(xx, yy), nil
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (R Parent) Contains(x object.Element) bool {
	_, err := ToElement(x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (R Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(x)
}

// String returns a string representation of the parent.
func (R Parent) String() string {
	return defaultName
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Set returns the (unique) set of uint64-valued elements. Note that this isn't really a ring since, for example, the range of the values is bounded and hence isn't closed under addition.
func Set() Parent {
	return nN
}

// ToElement attempts to convert the given object.Element to an uint64-valued element.
func ToElement(x object.Element) (*Element, error) {
	if y, ok := x.(*Element); ok {
		return y, nil
	} else if y, err := integer.ToElement(x); err == nil {
		z, err := y.Uint64()
		if err != nil {
			return nil, err
		}
		return FromUint64(z), nil
	}
	return nil, errors.ArgNotContainedInParent.New()
}
