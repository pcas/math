// Encoding contains functions for encoding/decoding an uint64-valued element.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package uint64number

import (
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
)

// The internal version number.This will permit backwards-compatible changes to
// the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (x *Element) GobEncode() ([]byte, error) {
	// Deal with the nil case
	if x == nil {
		return Zero().GobEncode()
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the value
	if err := enc.Encode(x.n); err != nil {
		return nil, err
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface. Important: Take great care that you are decoding into a new *Element; the safe way to do this is to use the GobDecode(dec *gob.Decode) function.
func (x *Element) GobDecode(buf []byte) error {
	// Sanity check
	if x == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if x.n != 0 || x == Zero() {
		return gobutil.DecodingIntoExistingObject.New()
	} else if len(buf) == 0 {
		return nil
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the value
	var n uint64
	if err := dec.Decode(&n); err != nil {
		return err
	}
	// Set the value
	x.n = n
	return nil
}

// GobDecode reads the next value from the given gob.Decoder and decodes it as a uint64-valued element.
func GobDecode(dec *gob.Decoder) (*Element, error) {
	// Decode into a new uint64-valued element
	x := &Element{}
	if err := dec.Decode(x); err != nil {
		return nil, err
	}
	// If this should really be an object from the cache, replace it
	if y := elementFromCache(x.n); y != nil {
		x = y
	}
	// Return the element
	return x, nil
}

// GobDecodeSlice reads the next value from the given gob.Decoder and decodes it as a slice of uint64-valued elements.
func GobDecodeSlice(dec *gob.Decoder) ([]*Element, error) {
	// Decode into a slice
	var S []*Element
	if err := dec.Decode(&S); err != nil {
		return nil, err
	}
	// Replace any instances of objects that should really be from the cache
	for i, x := range S {
		if y := elementFromCache(x.n); y != nil {
			S[i] = y
		}
	}
	// Return the slice
	return S, nil
}
