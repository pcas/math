// Cache defines caches of int64 slices and Elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64vector

import (
	"bitbucket.org/pcas/math/errors"
	"sync"
)

// sliceCache defines the cache of int64 slices.
type sliceCache struct {
	d     int        // The size of the returned slices
	m     sync.Mutex // The mutex controlling access to the cache
	idx   int        // The index in the cache
	cache []int64    // The underlying cache
}

// sliceCacheSize is the default size of a sliceCache.
const sliceCacheSize = 1000

// elementCache defines the cache of Elements.
type elementCache struct {
	m     sync.Mutex // The mutex controlling access to the cache
	idx   int        // The index in the cache
	cache []Element  // The underlying cache
}

// elementCacheSize is the default size of an elementCache.
const elementCacheSize = 1000

/////////////////////////////////////////////////////////////////////////
// sliceCache functions
/////////////////////////////////////////////////////////////////////////

// Get returns a new int64 slice.
func (c *sliceCache) Get() []int64 {
	d := c.d
	if d == 0 {
		return []int64{}
	}
	c.m.Lock()
	idx := c.idx
	if c.cache == nil {
		c.cache = make([]int64, sliceCacheSize*d)
		idx = 0
	}
	S := c.cache[d*idx : d*idx+d]
	idx++
	if idx == sliceCacheSize {
		c.cache = nil
	} else {
		c.idx = idx
	}
	c.m.Unlock()
	return S
}

// newSlicecache returns a new sliceCache.
func newSlicecache(d int) *sliceCache {
	if d < 0 {
		panic(errors.IllegalNegativeArg.New())
	}
	return &sliceCache{d: d}
}

/////////////////////////////////////////////////////////////////////////
// elementCache functions
/////////////////////////////////////////////////////////////////////////

// Get returns a new Element.
func (c *elementCache) Get() *Element {
	c.m.Lock()
	idx := c.idx
	if c.cache == nil {
		c.cache = make([]Element, elementCacheSize)
		idx = 0
	}
	e := &c.cache[idx]
	idx++
	if idx == elementCacheSize {
		c.cache = nil
	} else {
		c.idx = idx
	}
	c.m.Unlock()
	return e
}

// newElementcache returns a new elementCache.
func newElementcache() *elementCache {
	return &elementCache{}
}
