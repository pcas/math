// Encoding contains functions for encoding/decoding an int64-valued vector.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package int64vector

import (
	"bitbucket.org/pcas/math/compare/monomialorder"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
)

// encodingVersion is the internal version number. This will permit backwards-compatible changes to the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (x *Element) GobEncode() ([]byte, error) {
	// Deal with the nil case
	if x == nil {
		L, err := DefaultLattice(0)
		if err != nil {
			return nil, err
		}
		return Zero(L).GobEncode()
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the parent's monomial order
	if err := enc.Encode(parentOfElement(x).MonomialOrder()); err != nil {
		return nil, err
	}
	// Add the underlying value
	if err := enc.Encode(x.value); err != nil {
		return nil, err
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface. Important: Take great care that you are decoding into a new *Element; the safe way to do this is to use the GobDecode(dec *gob.Decode) function. The decoded vector will lie in the default lattice of appropriate dimension.
func (x *Element) GobDecode(buf []byte) error {
	// Sanity check
	if x == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if parentOfElement(x) != nil {
		return gobutil.DecodingIntoExistingObject.New()
	} else if len(buf) == 0 {
		var err error
		x.parent, err = DefaultLattice(0)
		if err != nil {
			return err
		}
		x.value = make([]int64, 0)
		return nil
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the parent's monomial order
	var c monomialorder.Type
	if err := dec.Decode(&c); err != nil {
		return err
	} else if !c.IsValid() {
		return errors.UnknownMonomialOrder.New(c)
	}
	// Read in the value
	var value []int64
	if err := dec.Decode(&value); err != nil {
		return err
	}
	// Fetch the default parent lattice
	L, err := DefaultLatticeWithOrder(len(value), c)
	if err != nil {
		return err
	}
	// Set the parent and values
	x.parent = L
	x.value = value
	return nil
}

// GobDecode reads the next value from the given gob.Decoder and decodes it as an int64-valued vector. The decoded vector will lie in the default lattice of appropriate dimension.
func GobDecode(dec *gob.Decoder) (*Element, error) {
	// Decode into a new vector
	x := &Element{}
	if err := dec.Decode(x); err != nil {
		return nil, err
	}
	// Handle the zero case
	if x.IsZero() {
		return Zero(parentOfElement(x)), nil
	}
	// Return the vector
	return x, nil
}

// GobDecodeSlice reads the next value from the given gob.Decoder and decodes it as a slice of int64-valued vectors. The decoded vectors will lie in the default lattice of appropriate dimension.
func GobDecodeSlice(dec *gob.Decoder) ([]*Element, error) {
	// Decode into a slice
	var S []*Element
	if err := dec.Decode(&S); err != nil {
		return nil, err
	}
	// Replace any instances of zero
	for i, x := range S {
		if x.IsZero() {
			S[i] = Zero(parentOfElement(x))
		}
	}
	// Return the slice
	return S, nil
}
