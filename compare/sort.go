// Sort implements sorting for elements whose parent has a total order.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compare

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
	"sort"
)

// LessFunc can be used to sort a slice of elements. It should return true iff x < y. The function must be safe to be called concurrently by multiple go routines.
type LessFunc func(x object.Element, y object.Element) (bool, error)

// isSorter defines an optional interface that a TotalOrder can satisfy in order to do its own sorting of elements.
type isSorter interface {
	Sort(S []object.Element) ([]object.Element, error) // Sort non-destructively sorts the given slice of elements. The sorted slice is returned.
}

// sortableSlice implements the sort.Interface interface.
type sortableSlice struct {
	less LessFunc         // The function to use when sorting
	S    []object.Element // The underlying slice
	Err  error            // The last error to occur during sorting
}

// The maximum length of the slice if use the standard sort package.
const maxSortLen = 1000

// The channel buffer size.
const sortBufSize = maxSortLen / 10

/////////////////////////////////////////////////////////////////////////
// sortableSlice functions
/////////////////////////////////////////////////////////////////////////

// Len is the number of elements in the slice.
func (s *sortableSlice) Len() int {
	return len(s.S)
}

// Less reports whether the element with index i should sort before the element with index j.
func (s *sortableSlice) Less(i int, j int) bool {
	isLess, err := s.less(s.S[i], s.S[j])
	if err != nil {
		s.Err = err
	}
	return isLess
}

// Swap swaps the elements with indexes i and j.
func (s *sortableSlice) Swap(i int, j int) {
	s.S[i], s.S[j] = s.S[j], s.S[i]
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// totalOrderToLessFunc returns a LessFunc for the given TotalOrder R.
func totalOrderToLessFunc(R TotalOrder) LessFunc {
	return func(x object.Element, y object.Element) (bool, error) {
		sgn, err := R.Cmp(x, y)
		if err != nil {
			return false, err
		}
		return sgn < 0, nil
	}
}

// dainChannels drains the resc channel, then drains the errc channel. Intended
// to be run as a background go routine.
func drainChannels(resc <-chan object.Element, errc <-chan error) {
	for _ = range resc {
	}
	for _ = range errc {
	}
}

// pipeResults pipes the contents of the src channel down the dst channel. The
// dst channel will NOT be closed upon return.
func pipeResults(src <-chan object.Element, dst chan<- object.Element) {
	for x := range src {
		dst <- x
	}
}

// fetchError checks the two channels for an error. Returns the error.
func fetchError(errc1 <-chan error, errc2 <-chan error) error {
	err1 := <-errc1
	err2 := <-errc2
	if err1 != nil {
		return err1
	} else if err2 != nil {
		return err2
	}
	return nil
}

// sortSliceStandard sorts the slice S using the given LessFunc. This is done using the standard sort package. Returns the sorted slice, and any error.
func sortSliceStandard(less LessFunc, S []object.Element) ([]object.Element, error) {
	s := sortableSlice{less: less, S: make([]object.Element, len(S))}
	copy(s.S, S)
	sort.Sort(&s)
	if s.Err != nil {
		return nil, s.Err
	}
	return s.S, nil
}

// sortSliceStandardToChan sorts the slice S using the given LessFunc. This is done using the standard sort package. The sorted elements are returned down the resc channel. If sorting completes normally then the resc channel is closed and the errc channel is closed. If an error is encountered during sorting then sorting stops, the resc channel is closed, the error submitted down the errc channel, and the errc channel is closed.
func sortSliceStandardToChan(less LessFunc, S []object.Element, resc chan<- object.Element, errc chan<- error) {
	if sortedS, err := sortSliceStandard(less, S); err != nil {
		close(resc)
		errc <- err
	} else {
		for _, x := range sortedS {
			resc <- x
		}
		close(resc)
	}
	close(errc)
}

// sortSlice sorts the slice S using the given LessFunc. The sorted elements are returned down the resc channel. If sorting completes normally then the resc channel is closed and the errc channel is closed. If an error is encountered during sorting then sorting stops, the resc channel is closed, the error submitted down the errc channel, and the errc channel is closed.
func sortSlice(less LessFunc, S []object.Element, resc chan<- object.Element, errc chan<- error) {
	// If the slice is small enough we use the standard sort package
	if len(S) <= maxSortLen {
		sortSliceStandardToChan(less, S, resc, errc)
		return
	}
	// Create the communication channels
	rescLeft := make(chan object.Element, sortBufSize)
	errcLeft := make(chan error)
	rescRight := make(chan object.Element, sortBufSize)
	errcRight := make(chan error)
	// Partition the task
	div := len(S) / 2
	go sortSlice(less, S[:div], rescLeft, errcLeft)
	go sortSlice(less, S[div:], rescRight, errcRight)
	// Start merging the results
	xL, okLeft := <-rescLeft
	xR, okRight := <-rescRight
	for okLeft && okRight {
		if isLess, err := less(xL, xR); err != nil {
			// Close out results channel, pass the error down the error channel,
			// and close our error channel
			close(resc)
			errc <- err
			close(errc)
			// Set two background go routines running to drain the child
			// channels and return
			go drainChannels(rescLeft, errcLeft)
			go drainChannels(rescRight, errcRight)
			return
		} else if isLess {
			// xL < xR
			resc <- xL
			xL, okLeft = <-rescLeft
		} else {
			// xL >= xR
			resc <- xR
			xR, okRight = <-rescRight
		}
	}
	// Forward any remaining results down the results channel
	if okLeft {
		resc <- xL
		pipeResults(rescLeft, resc)
	} else if okRight {
		resc <- xR
		pipeResults(rescRight, resc)
	}
	close(resc)
	// Handle any errors
	if err := fetchError(errcLeft, errcRight); err != nil {
		errc <- err
	}
	close(errc)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sort non-destructively sorts the given slice of elements using the given TotalOrder. The sorted slice is returned. If R satisfies the interface:
//		type Sorter interface {
//			Sort(S []object.Element) ([]object.Element, error) // Sort
//			  non-destructively sorts the given slice of elements. The sorted
// 			  slice is returned.
//		}
// then R's Sort method will be called.
func Sort(R TotalOrder, S []object.Element) ([]object.Element, error) {
	// Get the empty slice out of the way
	if len(S) == 0 {
		return S, nil
	}
	// If the TotalOrder has its own Sort method, we call that instead
	if RR, ok := R.(isSorter); ok {
		return RR.Sort(S)
	}
	// Fast-track the length 1 case
	if len(S) == 1 {
		if !R.Contains(S[0]) {
			return nil, errors.ArgNotContainedInParent.New()
		}
		return []object.Element{S[0]}, nil
	}
	// If the slice is small enough we do it with the standard sort package
	if len(S) <= maxSortLen {
		return sortSliceStandard(totalOrderToLessFunc(R), S)
	}
	// Create the communication channels
	resc := make(chan object.Element, sortBufSize)
	errc := make(chan error)
	// Set a background go routine sorting
	go sortSlice(totalOrderToLessFunc(R), S, resc, errc)
	// Extract the results into a slice
	sortedS := make([]object.Element, 0, cap(S))
	for x := range resc {
		sortedS = append(sortedS, x)
	}
	// Handle any errors and return
	if err := <-errc; err != nil {
		return nil, err
	}
	return sortedS, nil
}

// SortWithFunc non-destructively sorts the given slice of elements using the given function. The sorted slice is returned.
func SortWithFunc(less LessFunc, S []object.Element) ([]object.Element, error) {
	// Get the empty slice out of the way
	if len(S) == 0 {
		return S, nil
	}
	// Fast-track the length 1 case
	if len(S) == 1 {
		return []object.Element{S[0]}, nil
	}
	// If the slice is small enough we do it with the standard sort package
	if len(S) <= maxSortLen {
		return sortSliceStandard(less, S)
	}
	// Create the communication channels
	resc := make(chan object.Element, sortBufSize)
	errc := make(chan error)
	// Set a background go routine sorting
	go sortSlice(less, S, resc, errc)
	// Extract the results into a slice
	sortedS := make([]object.Element, 0, cap(S))
	for x := range resc {
		sortedS = append(sortedS, x)
	}
	// Handle any errors and return
	if err := <-errc; err != nil {
		return nil, err
	}
	return sortedS, nil
}
