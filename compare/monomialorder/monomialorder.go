// Monomialorder provides some useful constants and functions describing the standard monomial orders: lexicographic order (lex), graded lexicographic order (grlex), and graded reverse lexicographic order (grevlex).

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package monomialorder

import (
	"bitbucket.org/pcas/math/abelianmonoid"
	"bitbucket.org/pcas/math/compare"
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/rational"
	"math"
	"strconv"
)

// Type represents one of the standard monomial orders as defined by the constants "Lex", "Grlex", and "Grevlex".
type Type uint8

// The three standard monomial orders.
const (
	Lex     Type = iota // Lexicographic order. We say that x > y if the left-most non-zero entry of x - y is positive.
	Grlex               // Graded lexicographic order. We say that x > y if either the total order |x| > |y|, or if |x| = |y| and the left-most non-zero entry of x - y is positive.
	Grevlex             // Graded reverse lexicographic order. We say that x > y if either the total order |x| > |y|, or if |x| = |y| and the right-most non-zero entry of x - y is negative.
)

// Interface defines the interface that must be satisfied in order to use one of the standard monomial orders.
type Interface interface {
	abelianmonoid.Interface
	compare.Cmper
}

// MonomialOrderer defines the interface satisfied by an object with a MonomialOrder method.
type MonomialOrderer interface {
	MonomialOrder() Type // MonomialOrder returns the monomial order.
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// addWithOverflow returns an integer c = int64(x + y) along with an integer k \in {-1,0,1}. The integer k indicates whether the sum overflowed the range of an int64 and, if so, in which direction.
func addWithOverflow(x int64, y int64) (int64, int) {
	var overflow int
	if x > 0 {
		if y > 0 {
			y += x
			if y <= 0 {
				overflow = 1
			}
		} else {
			y += x
		}
	} else if x < 0 {
		if y >= 0 {
			y += x
		} else if y < 0 {
			y += x
			if y >= 0 {
				overflow = -1
			}
		}
	}
	return y, overflow
}

// subtractWithOverflow returns an integer c = int64(x - y) along with an integer k \in {-1,0,1}. The integer k indicates whether the sum overflowed the range of an int64 and, if so, in which direction.
func subtractWithOverflow(x int64, y int64) (int64, int) {
	if y == math.MinInt64 {
		var overflow1, overflow2 int
		y, overflow1 = addWithOverflow(x, math.MaxInt64)
		y, overflow2 = addWithOverflow(y, 1)
		return y, overflow1 + overflow2
	}
	return addWithOverflow(x, -y)
}

// addWithOverflowUint64 returns an integer c = uint64(x + y) along with an integer k \in {0,1}. The integer k indicates whether the sum overflowed the range of a uint64.
func addWithOverflowUint64(x uint64, y uint64) (uint64, int) {
	if x == 0 {
		return y, 0
	} else if y == 0 {
		return x, 0
	}
	result := x + y
	if result < x || result < y {
		return result, 1
	}
	return result, 0
}

// subtractWithOverflowUint64 returns an integer c = uint64(x - y) along with an integer k \in {-1,0} that indicates whether the sum overflowed the range of a uint64.
func subtractWithOverflowUint64(x uint64, y uint64) (uint64, int) {
	if y > x {
		return x - y, -1
	}
	return x - y, 0
}

/////////////////////////////////////////////////////////////////////////
// Type functions
/////////////////////////////////////////////////////////////////////////

// IsValid returns true iff the monomial order is valid (that is, is one of the defined types).
func (c Type) IsValid() bool {
	switch c {
	case Lex, Grlex, Grevlex:
		return true
	}
	return false
}

// Cmp compares the slices S1 and S2 using the monomial order, where the elements are compared using R. This will return an error if len(S1) does not equal len(S2).
func (c Type) Cmp(R Interface, S1 []object.Element, S2 []object.Element) (int, error) {
	switch c {
	case Lex:
		return CmpLex(R, S1, S2)
	case Grlex:
		return CmpGrlex(R, S1, S2)
	case Grevlex:
		return CmpGrevlex(R, S1, S2)
	}
	return 0, errors.UnknownMonomialOrder.New(c)
}

// CmpInt64 compares the slices S1 and S2 using the monomial order. This will return an error if len(S1) does not equal len(S2).
func (c Type) CmpInt64(S1 []int64, S2 []int64) (int, error) {
	switch c {
	case Lex:
		return CmpLexInt64(S1, S2)
	case Grlex:
		return CmpGrlexInt64(S1, S2)
	case Grevlex:
		return CmpGrevlexInt64(S1, S2)
	}
	return 0, errors.UnknownMonomialOrder.New(c)
}

// CmpUint64 compares the slices S1 and S2 using the monomial order. This will return an error if len(S1) does not equal len(S2).
func (c Type) CmpUint64(S1 []uint64, S2 []uint64) (int, error) {
	switch c {
	case Lex:
		return CmpLexUint64(S1, S2)
	case Grlex:
		return CmpGrlexUint64(S1, S2)
	case Grevlex:
		return CmpGrevlexUint64(S1, S2)
	}
	return 0, errors.UnknownMonomialOrder.New(c)
}

// CmpInteger compares the slices S1 and S2 using the monomial order. This will return an error if len(S1) does not equal len(S2).
func (c Type) CmpInteger(S1 []*integer.Element, S2 []*integer.Element) (int, error) {
	switch c {
	case Lex:
		return CmpLexInteger(S1, S2)
	case Grlex:
		return CmpGrlexInteger(S1, S2)
	case Grevlex:
		return CmpGrevlexInteger(S1, S2)
	}
	return 0, errors.UnknownMonomialOrder.New(c)
}

// CmpRational compares the slices S1 and S2 using the monomial order. This will return an error if len(S1) does not equal len(S2).
func (c Type) CmpRational(S1 []*rational.Element, S2 []*rational.Element) (int, error) {
	switch c {
	case Lex:
		return CmpLexRational(S1, S2)
	case Grlex:
		return CmpGrlexRational(S1, S2)
	case Grevlex:
		return CmpGrevlexRational(S1, S2)
	}
	return 0, errors.UnknownMonomialOrder.New(c)
}

// String returns a string description of the order.
func (c Type) String() string {
	switch c {
	case Lex:
		return "lex"
	case Grlex:
		return "grlex"
	case Grevlex:
		return "grevlex"
	}
	return "unknown [" + strconv.Itoa(int(c)) + "]"
}

/////////////////////////////////////////////////////////////////////////
// Lex functions
/////////////////////////////////////////////////////////////////////////

// CmpLex compares the slices S1 and S2 using lexicographic (lex) order, where the elements are compared using R. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpLex(R compare.TotalOrder, S1 []object.Element, S2 []object.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// Perform the comparison
	for i, x := range S1 {
		if sgn, err := R.Cmp(x, S2[i]); err != nil {
			return 0, err
		} else if sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpLexInt64 compares the slices S1 and S2 using lexicographic (lex) order.  Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpLexInt64(S1 []int64, S2 []int64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// Perform the comparison
	for i, x := range S1 {
		if y := S2[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpLexUint64 compares the slices S1 and S2 using lexicographic (lex) order.  Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpLexUint64(S1 []uint64, S2 []uint64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// Perform the comparison
	for i, x := range S1 {
		if y := S2[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpLexInteger compares the slices S1 and S2 using lexicographic (lex) order.  Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpLexInteger(S1 []*integer.Element, S2 []*integer.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// Perform the comparison
	for i, x := range S1 {
		if sgn := integer.Cmp(x, S2[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpLexRational compares the slices S1 and S2 using lexicographic (lex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpLexRational(S1 []*rational.Element, S2 []*rational.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// Perform the comparison
	for i, x := range S1 {
		if sgn := rational.Cmp(x, S2[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

/////////////////////////////////////////////////////////////////////////
// Grlex functions
/////////////////////////////////////////////////////////////////////////

// CmpGrlex compares the slices S1 and S2 using graded lexicographic (grlex) order, where the elements are compared using R. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrlex(R Interface, S1 []object.Element, S2 []object.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	t1, err := abelianmonoid.Sum(R, S1...)
	if err != nil {
		return 0, err
	}
	if t2, err := abelianmonoid.Sum(R, S2...); err != nil {
		return 0, err
	} else if sgn, err := R.Cmp(t1, t2); err != nil {
		return 0, err
	} else if sgn != 0 {
		return sgn, nil
	}
	// Break ties with lex comparison
	for i, x := range S1 {
		if sgn, err := R.Cmp(x, S2[i]); err != nil {
			return 0, err
		} else if sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpGrlexInt64 compares the slices S1 and S2 using graded lexicographic (grlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrlexInt64(S1 []int64, S2 []int64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total degree: we calculate totaldeg(S1) - totaldeg(S2)
	var diff int64
	var overflow int
	for i, x := range S1 {
		var newoverflow int
		diff, newoverflow = addWithOverflow(diff, x)
		overflow += newoverflow
		diff, newoverflow = subtractWithOverflow(diff, S2[i])
		overflow += newoverflow
	}
	if overflow < 0 {
		return -1, nil
	} else if overflow > 0 {
		return 1, nil
	} else if diff < 0 {
		return -1, nil
	} else if diff > 0 {
		return 1, nil
	}
	// Break ties with lex comparison
	for i, x := range S1 {
		if y := S2[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpGrlexUint64 compares the slices S1 and S2 using graded lexicographic (grlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrlexUint64(S1 []uint64, S2 []uint64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total degree: we calculate totaldeg(S1) - totaldeg(S2)
	var diff uint64
	var overflow int
	for i, x := range S1 {
		var newoverflow int
		diff, newoverflow = addWithOverflowUint64(diff, x)
		overflow += newoverflow
		diff, newoverflow = subtractWithOverflowUint64(diff, S2[i])
		overflow += newoverflow
	}
	if overflow < 0 {
		return -1, nil
	} else if overflow > 0 {
		return 1, nil
	} else if diff > 0 {
		return 1, nil
	}
	// Break ties with lex comparison
	for i, x := range S1 {
		if y := S2[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpGrlexInteger compares the slices S1 and S2 using graded lexicographic (grlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrlexInteger(S1 []*integer.Element, S2 []*integer.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	if sgn := integer.Cmp(integer.Sum(S1...), integer.Sum(S2...)); sgn != 0 {
		return sgn, nil
	}
	// Break ties with lex comparison
	for i, x := range S1 {
		if sgn := integer.Cmp(x, S2[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpGrlexRational compares the slices S1 and S2 using graded lexicographic (grlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrlexRational(S1 []*rational.Element, S2 []*rational.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	if sgn := rational.Cmp(rational.Sum(S1...), rational.Sum(S2...)); sgn != 0 {
		return sgn, nil
	}
	// Break ties with lex comparison
	for i, x := range S1 {
		if sgn := rational.Cmp(x, S2[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

/////////////////////////////////////////////////////////////////////////
// Grevlex functions
/////////////////////////////////////////////////////////////////////////

// CmpGrevlex compares the slices S1 and S2 using graded reverse lexicographic (grevlex) order, where the elements are compared using R.. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrevlex(R Interface, S1 []object.Element, S2 []object.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	t1, err := abelianmonoid.Sum(R, S1...)
	if err != nil {
		return 0, err
	}
	if t2, err := abelianmonoid.Sum(R, S2...); err != nil {
		return 0, err
	} else if sgn, err := R.Cmp(t1, t2); err != nil {
		return 0, err
	} else if sgn != 0 {
		return sgn, nil
	}
	// Break ties by comparing from the right
	for i := len(S1) - 1; i >= 0; i-- {
		if sgn, err := R.Cmp(S2[i], S1[i]); err != nil {
			return 0, err
		} else if sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpGrevlexInt64 compares the slices S1 and S2 using graded reverse lexicographic (grevlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrevlexInt64(S1 []int64, S2 []int64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total degree: we calculate totaldeg(S1) - totaldeg(S2)
	var diff int64
	var overflow int
	for i, x := range S1 {
		var newoverflow int
		diff, newoverflow = addWithOverflow(diff, x)
		overflow += newoverflow
		diff, newoverflow = subtractWithOverflow(diff, S2[i])
		overflow += newoverflow
	}
	if overflow < 0 {
		return -1, nil
	} else if overflow > 0 {
		return 1, nil
	} else if diff < 0 {
		return -1, nil
	} else if diff > 0 {
		return 1, nil
	}
	// Break ties with lex comparison
	for i := len(S1) - 1; i >= 0; i-- {
		if x, y := S2[i], S1[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpGrevlexUint64 compares the slices S1 and S2 using graded reverse lexicographic (grevlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrevlexUint64(S1 []uint64, S2 []uint64) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total degree: we calculate totaldeg(S1) - totaldeg(S2)
	var diff uint64
	var overflow int
	for i, x := range S1 {
		var newoverflow int
		diff, newoverflow = addWithOverflowUint64(diff, x)
		overflow += newoverflow
		diff, newoverflow = subtractWithOverflowUint64(diff, S2[i])
		overflow += newoverflow
	}
	if overflow < 0 {
		return -1, nil
	} else if overflow > 0 {
		return 1, nil
	} else if diff > 0 {
		return 1, nil
	}
	// Break ties with lex comparison
	for i := len(S1) - 1; i >= 0; i-- {
		if x, y := S2[i], S1[i]; x < y {
			return -1, nil
		} else if x > y {
			return 1, nil
		}
	}
	return 0, nil
}

// CmpGrevlexInteger compares the slices S1 and S2 using graded reverse lexicographic (grevlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrevlexInteger(S1 []*integer.Element, S2 []*integer.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	if sgn := integer.Cmp(integer.Sum(S1...), integer.Sum(S2...)); sgn != 0 {
		return sgn, nil
	}
	// Break ties by comparing from the right
	for i := len(S1) - 1; i >= 0; i-- {
		if sgn := integer.Cmp(S2[i], S1[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}

// CmpGrevlexRational compares the slices S1 and S2 using graded reverse lexicographic (grevlex) order. Returns -1 if S1 < S2, 0 if S1 = S2, and +1 if S1 > S2. This will return an error if len(S1) does not equal len(S2).
func CmpGrevlexRational(S1 []*rational.Element, S2 []*rational.Element) (int, error) {
	// Sanity check
	if len(S1) != len(S2) {
		return 0, errors.SliceLengthNotEqual.New()
	}
	// First check the total orders
	if sgn := rational.Cmp(rational.Sum(S1...), rational.Sum(S2...)); sgn != 0 {
		return sgn, nil
	}
	// Break ties by comparing from the right
	for i := len(S1) - 1; i >= 0; i-- {
		if sgn := rational.Cmp(S2[i], S1[i]); sgn != 0 {
			return sgn, nil
		}
	}
	return 0, nil
}
