// Minmax implements functions for finding the minimum and maximum in a slice of elements whose parent has a total order.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compare

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
)

// isMiner defines an optional interface that a TotalOrder can satisfy in order to find the minimum of a sequence of elements.
type isMiner interface {
	Min(S ...object.Element) (object.Element, error) // Min returns the minimum element in the slice S. If S is empty then an error is returned.
}

// isMinAndIndexer defines an optional interface that a TotalOrder can satisfy in order to find the minimum of a sequence of elements, along with the index of the first occurrence of that element in the slice.
type isMinAndIndexer interface {
	MinAndIndex(S []object.Element) (object.Element, int, error) // MinAndIndex returns the minimum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
}

// isMaxer defines an optional interface that a TotalOrder can satisfy in order to find the maximum of a sequence of elements.
type isMaxer interface {
	Max(S ...object.Element) (object.Element, error) // Max returns the maximum element in the slice S. If S is empty then an error is returned.
}

// isMaxAndIndexer defines an optional interface that a TotalOrder can satisfy in order to find the maximum of a sequence of elements, along with the index of the first occurrence of that element in the slice.
type isMaxAndIndexer interface {
	MaxAndIndex(S []object.Element) (object.Element, int, error) // MaxAndIndex returns the maximum element in the slice S, along with the index of the first occurrence of that element in S. If S is empty then an error is returned.
}

// elementIndexErrTriple is a triple used to encapsulate an element and its index, along with a possible error value.
type elementIndexErrTriple struct {
	Obj object.Element // The element
	Idx int            // The index
	Err error          // The error
}

// The maximum length of the slice if calculate the min/max directly.
const maxMinMaxLen = 500

/////////////////////////////////////////////////////////////////////////
// Min functions
/////////////////////////////////////////////////////////////////////////

// minAndIndexInternal passes minimum element in the slice S using the TotalOrder R to the channel c. The index + offset of the first occurrence of this element is also given. Assumes that S is non-empty.
func minAndIndexInternal(R TotalOrder, S []object.Element, offset int, resc chan<- *elementIndexErrTriple) {
	x, idx, err := minAndIndex(R, S, offset)
	resc <- &elementIndexErrTriple{Obj: x, Idx: idx, Err: err}
}

// minAndIndex returns the minimum element in the slice S using the TotalOrder R. The index + offset of the first occurrence of this element is also returned. Assumes that S is non-empty.
func minAndIndex(R TotalOrder, S []object.Element, offset int) (object.Element, int, error) {
	// If S is short enough, we do it now
	if len(S) <= maxMinMaxLen {
		// Fast-track the length 1 and 2 cases
		if len(S) == 1 {
			if !R.Contains(S[0]) {
				return nil, 0, errors.ArgNotContainedInParent.New()
			}
			return S[0], offset, nil
		} else if len(S) == 2 {
			if sgn, err := R.Cmp(S[0], S[1]); err != nil {
				return nil, 0, err
			} else if sgn <= 0 {
				return S[0], offset, nil
			}
			return S[1], offset + 1, nil
		}
		// Work through S looking for the smallest element
		idx := 0
		x := S[0]
		for i, y := range S[1:] {
			if sgn, err := R.Cmp(y, x); err != nil {
				return nil, 0, err
			} else if sgn < 0 {
				idx = i + 1
				x = y
			}
		}
		// Return the result
		return x, offset + idx, nil
	}
	// Create the communication channel
	resc := make(chan *elementIndexErrTriple)
	// Partition the task
	div := len(S) / 2
	go minAndIndexInternal(R, S[:div], offset, resc)
	go minAndIndexInternal(R, S[div:], offset+div, resc)
	// Process the result
	res1 := <-resc
	res2 := <-resc
	if res1.Err != nil {
		return nil, 0, res1.Err
	} else if res2.Err != nil {
		return nil, 0, res2.Err
	} else if sgn, err := R.Cmp(res1.Obj, res2.Obj); err != nil {
		return nil, 0, err
	} else if sgn < 0 {
		return res1.Obj, res1.Idx, nil
	} else if sgn == 0 && res1.Idx < res2.Idx {
		return res1.Obj, res1.Idx, nil
	}
	return res2.Obj, res2.Idx, nil
}

// MinOfPair returns the minimum element of x and y. The minimum is calculated with respect to the total order R.
func MinOfPair(R TotalOrder, x object.Element, y object.Element) (object.Element, error) {
	if sgn, err := R.Cmp(x, y); err != nil {
		return nil, err
	} else if sgn <= 0 {
		return x, nil
	}
	return y, nil
}

// Min returns the minimum element in the slice S. The minimum is calculated with respect to the total order R. If S is empty then an error is returned. If R satisfies the interface:
//		type Miner interface {
//			Min(S ...object.Element) (object.Element, error) // Min returns the
//			   minimum element in the slice S. If S is empty then an error is
//			   returned.
//		}
// then R's Min method will be called.
func Min(R TotalOrder, S ...object.Element) (object.Element, error) {
	// Get empty slice out of the way
	if len(S) == 0 {
		return nil, errors.EmptySlice.New()
	}
	// If the TotalOrder has its own Min method, or a MinAndIndex method, we
	// call that instead
	if RR, ok := R.(isMiner); ok {
		return RR.Min(S...)
	} else if RR, ok := R.(isMinAndIndexer); ok {
		x, _, err := RR.MinAndIndex(S)
		return x, err
	}
	// Calculate the minimum
	x, _, err := minAndIndex(R, S, 0)
	return x, err
}

// MinAndIndex returns the minimum element in the slice S, along with the index of the first occurrence of that element in S. The minimum is calculated with respect to the total order R. If S is empty then an error is returned. If R satisfies the interface:
//		type MinAndIndexer interface {
//			MinAndIndex(S []object.Element) (object.Element, int, error)
//				// MinAndIndex returns the minimum element in the slice S,
//				   along with the index of the first occurrence of that element
//				   in S. If S is empty then an error is returned.
//		}
// then R's MinAndIndex method will be called.
func MinAndIndex(R TotalOrder, S []object.Element) (object.Element, int, error) {
	// Get empty slice out of the way
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	}
	// If the TotalOrder has its own MinAndIndex method, we call that instead
	if RR, ok := R.(isMinAndIndexer); ok {
		return RR.MinAndIndex(S)
	}
	// Calculate the minimum
	return minAndIndex(R, S, 0)
}

/////////////////////////////////////////////////////////////////////////
// Max functions
/////////////////////////////////////////////////////////////////////////

// maxAndIndexInternal passes maximum element in the slice S using the TotalOrder R to the channel c. The index + offset of the first occurrence of this element is also given. Assumes that S is non-empty.
func maxAndIndexInternal(R TotalOrder, S []object.Element, offset int, resc chan<- *elementIndexErrTriple) {
	x, idx, err := maxAndIndex(R, S, offset)
	resc <- &elementIndexErrTriple{Obj: x, Idx: idx, Err: err}
}

// maxAndIndex returns the maximum element in the slice S using the TotalOrder R. The index + offset of the first occurrence of this element is also returned. Assumes that S is non-empty.
func maxAndIndex(R TotalOrder, S []object.Element, offset int) (object.Element, int, error) {
	// If S is short enough, we do it now
	if len(S) <= maxMinMaxLen {
		// Fast-track the length 1 and 2 cases
		if len(S) == 1 {
			if !R.Contains(S[0]) {
				return nil, 0, errors.ArgNotContainedInParent.New()
			}
			return S[0], offset, nil
		} else if len(S) == 2 {
			if sgn, err := R.Cmp(S[0], S[1]); err != nil {
				return nil, 0, err
			} else if sgn >= 0 {
				return S[0], offset, nil
			}
			return S[1], offset + 1, nil
		}
		// Work through S looking for the largest element
		idx := 0
		x := S[0]
		for i, y := range S[1:] {
			if sgn, err := R.Cmp(y, x); err != nil {
				return nil, 0, err
			} else if sgn > 0 {
				idx = i + 1
				x = y
			}
		}
		// Return the result
		return x, offset + idx, nil
	}
	// Create the communication channel
	resc := make(chan *elementIndexErrTriple)
	// Partition the task
	div := len(S) / 2
	go maxAndIndexInternal(R, S[:div], offset, resc)
	go maxAndIndexInternal(R, S[div:], offset+div, resc)
	// Process the result
	res1 := <-resc
	res2 := <-resc
	if res1.Err != nil {
		return nil, 0, res1.Err
	} else if res2.Err != nil {
		return nil, 0, res2.Err
	} else if sgn, err := R.Cmp(res1.Obj, res2.Obj); err != nil {
		return nil, 0, err
	} else if sgn > 0 {
		return res1.Obj, res1.Idx, nil
	} else if sgn == 0 && res1.Idx < res2.Idx {
		return res1.Obj, res1.Idx, nil
	}
	return res2.Obj, res2.Idx, nil
}

// MaxOfPair returns the maximum element of x and y. The maximum is calculated with respect to the total order R.
func MaxOfPair(R TotalOrder, x object.Element, y object.Element) (object.Element, error) {
	if sgn, err := R.Cmp(x, y); err != nil {
		return nil, err
	} else if sgn >= 0 {
		return x, nil
	}
	return y, nil
}

// Max returns the maximum element in the slice S. The maximum is calculated with respect to the total order R. If S is empty then an error is returned. If R satisfies the interface:
//		type Maxer interface {
//			Max(S ...object.Element) (object.Element, error) // Max returns the
//			   maximum element in the slice S. If S is empty then an error is
//			   returned.
//		}
// then R's Max method will be called.
func Max(R TotalOrder, S ...object.Element) (object.Element, error) {
	// Get empty slice out of the way
	if len(S) == 0 {
		return nil, errors.EmptySlice.New()
	}
	// If the TotalOrder has its own Max method, or a MaxAndIndex method, we
	// call that instead
	if RR, ok := R.(isMaxer); ok {
		return RR.Max(S...)
	} else if RR, ok := R.(isMaxAndIndexer); ok {
		x, _, err := RR.MaxAndIndex(S)
		return x, err
	}
	// Calculate the maximum
	x, _, err := maxAndIndex(R, S, 0)
	return x, err
}

// MaxAndIndex returns the maximum element in the slice S, along with the index of the first occurrence of that element in S. The maximum is calculated with respect to the total order R. If S is empty then an error is returned. If R satisfies the interface:
//		type MaxAndIndexer interface {
//			MaxAndIndex(S []object.Element) (object.Element, int, error)
//				// MaxAndIndex returns the maximum element in the slice S,
//				   along with the index of the first occurrence of that element
//				   in S. If S is empty then an error is returned.
//		}
// then R's MaxAndIndex method will be called.
func MaxAndIndex(R TotalOrder, S []object.Element) (object.Element, int, error) {
	// Get empty slice out of the way
	if len(S) == 0 {
		return nil, 0, errors.EmptySlice.New()
	}
	// If the TotalOrder has its own MaxAndIndex method, we call that instead
	if RR, ok := R.(isMaxAndIndexer); ok {
		return RR.MaxAndIndex(S)
	}
	// Calculate the maximum
	return maxAndIndex(R, S, 0)
}
