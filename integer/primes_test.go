// Tests for primes.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

// TestPrimeFactorsInt64 tests TestPrimeFactorsInt64
func TestPrimeFactorsInt64(t *testing.T) {
	assert := assert.New(t)
	// we test the factorizations of 2^n + 1 for n in [1..62]
	results := [][]int64{
		{3},
		{5},
		{3, 3},
		{17},
		{3, 11},
		{5, 13},
		{3, 43},
		{257},
		{3, 3, 3, 19},
		{5, 5, 41},
		{3, 683},
		{17, 241},
		{3, 2731},
		{5, 29, 113},
		{3, 3, 11, 331},
		{65537},
		{3, 43691},
		{5, 13, 37, 109},
		{3, 174763},
		{17, 61681},
		{3, 3, 43, 5419},
		{5, 397, 2113},
		{3, 2796203},
		{97, 257, 673},
		{3, 11, 251, 4051},
		{5, 53, 157, 1613},
		{3, 3, 3, 3, 19, 87211},
		{17, 15790321},
		{3, 59, 3033169},
		{5, 5, 13, 41, 61, 1321},
		{3, 715827883},
		{641, 6700417},
		{3, 3, 67, 683, 20857},
		{5, 137, 953, 26317},
		{3, 11, 43, 281, 86171},
		{17, 241, 433, 38737},
		{3, 1777, 25781083},
		{5, 229, 457, 525313},
		{3, 3, 2731, 22366891},
		{257, 4278255361},
		{3, 83, 8831418697},
		{5, 13, 29, 113, 1429, 14449},
		{3, 2932031007403},
		{17, 353, 2931542417},
		{3, 3, 3, 11, 19, 331, 18837001},
		{5, 277, 1013, 1657, 30269},
		{3, 283, 165768537521},
		{193, 65537, 22253377},
		{3, 43, 4363953127297},
		{5, 5, 5, 41, 101, 8101, 268501},
		{3, 3, 307, 2857, 6529, 43691},
		{17, 858001, 308761441},
		{3, 107, 28059810762433},
		{5, 13, 37, 109, 246241, 279073},
		{3, 11, 11, 683, 2971, 48912491},
		{257, 5153, 54410972897},
		{3, 3, 571, 174763, 160465489},
		{5, 107367629, 536903681},
		{3, 2833, 37171, 1824726041},
		{17, 241, 61681, 4562284561},
		{3, 768614336404564651},
		{5, 5581, 8681, 49477, 384773},
	}
	for n := uint(1); n < 63; n++ {
		if n == 58 {
			// FIX ME skip this because it takes too long.  Need to implement Elliptic Curve Factorization or Number Field Sieve!
			continue
		}
		if result, err := PrimeFactorsInt64(1<<n + 1); assert.NoError(err) {
			assert.Equal(result, results[n-1])
		}
	}
	// Check that the factors of a negative number are the same as the positive
	if result, err := PrimeFactorsInt64(-101); assert.NoError(err) {
		if assert.Equal(len(result), 1) {
			assert.Equal(result[0], int64(101))
		}
	}
	// Check the factors of 1 is empty
	if result, err := PrimeFactorsInt64(1); assert.NoError(err) {
		assert.Equal(len(result), 0)
	}
	// Check the minimum value of an int64
	if result, err := PrimeFactorsInt64(math.MinInt64); assert.NoError(err) {
		if assert.Equal(len(result), 63) {
			for _, n := range result {
				assert.Equal(n, int64(2))
			}
		}
	}
	// Passing 0 should return an error
	_, err := PrimeFactorsInt64(0)
	assert.Error(err)
}

// TestPrimeFactors tests TestPrimeFactors
func TestPrimeFactors(t *testing.T) {
	assert := assert.New(t)
	// we test the factorizations of 2^n + 1 for n in [1..100]
	results := [][]string{
		{"3"},
		{"5"},
		{"3", "3"},
		{"17"},
		{"3", "11"},
		{"5", "13"},
		{"3", "43"},
		{"257"},
		{"3", "3", "3", "19"},
		{"5", "5", "41"},
		{"3", "683"},
		{"17", "241"},
		{"3", "2731"},
		{"5", "29", "113"},
		{"3", "3", "11", "331"},
		{"65537"},
		{"3", "43691"},
		{"5", "13", "37", "109"},
		{"3", "174763"},
		{"17", "61681"},
		{"3", "3", "43", "5419"},
		{"5", "397", "2113"},
		{"3", "2796203"},
		{"97", "257", "673"},
		{"3", "11", "251", "4051"},
		{"5", "53", "157", "1613"},
		{"3", "3", "3", "3", "19", "87211"},
		{"17", "15790321"},
		{"3", "59", "3033169"},
		{"5", "5", "13", "41", "61", "1321"},
		{"3", "715827883"},
		{"641", "6700417"},
		{"3", "3", "67", "683", "20857"},
		{"5", "137", "953", "26317"},
		{"3", "11", "43", "281", "86171"},
		{"17", "241", "433", "38737"},
		{"3", "1777", "25781083"},
		{"5", "229", "457", "525313"},
		{"3", "3", "2731", "22366891"},
		{"257", "4278255361"},
		{"3", "83", "8831418697"},
		{"5", "13", "29", "113", "1429", "14449"},
		{"3", "2932031007403"},
		{"17", "353", "2931542417"},
		{"3", "3", "3", "11", "19", "331", "18837001"},
		{"5", "277", "1013", "1657", "30269"},
		{"3", "283", "165768537521"},
		{"193", "65537", "22253377"},
		{"3", "43", "4363953127297"},
		{"5", "5", "5", "41", "101", "8101", "268501"},
		{"3", "3", "307", "2857", "6529", "43691"},
		{"17", "858001", "308761441"},
		{"3", "107", "28059810762433"},
		{"5", "13", "37", "109", "246241", "279073"},
		{"3", "11", "11", "683", "2971", "48912491"},
		{"257", "5153", "54410972897"},
		{"3", "3", "571", "174763", "160465489"},
		{"5", "107367629", "536903681"},
		{"3", "2833", "37171", "1824726041"},
		{"17", "241", "61681", "4562284561"},
		{"3", "768614336404564651"},
		{"5", "5581", "8681", "49477", "384773"},
		{"3", "3", "3", "19", "43", "5419", "77158673929"},
		{"274177", "67280421310721"},
		{"3", "11", "131", "2731", "409891", "7623851"},
		{"5", "13", "397", "2113", "312709", "4327489"},
		{"3", "7327657", "6713103182899"},
		{"17", "17", "354689", "2879347902817"},
		{"3", "3", "139", "2796203", "168749965921"},
		{"5", "5", "29", "41", "113", "7416361", "47392381"},
		{"3", "56409643", "13952598148481"},
		{"97", "257", "577", "673", "487824887233"},
		{"3", "1753", "1795918038741070627"},
		{"5", "149", "593", "184481113", "231769777"},
		{"3", "3", "11", "251", "331", "4051", "1133836730401"},
		{"17", "1217", "148961", "24517014940753"},
		{"3", "43", "617", "683", "78233", "35532364099"},
		{"5", "13", "13", "53", "157", "313", "1249", "1613", "3121", "21841"},
		{"3", "201487636602438195784363"},
		{"65537", "414721", "44479210368001"},
		{"3", "3", "3", "3", "3", "19", "163", "87211", "135433", "272010961"},
		{"5", "10169", "181549", "12112549", "43249589"},
		{"3", "499", "1163", "2657", "155377", "13455809771"},
		{"17", "241", "3361", "15790321", "88959882481"},
		{"3", "11", "43691", "26831423036065352611"},
		{"5", "173", "101653", "500177", "1759217765581"},
		{"3", "3", "59", "3033169", "96076791871613611"},
		{"257", "229153", "119782433", "43872038849"},
		{"3", "179", "62020897", "18584774046020617"},
		{"5", "5", "13", "37", "41", "61", "109", "181", "1321", "54001", "29247661"},
		{"3", "43", "2731", "224771", "1210483", "25829691707"},
		{"17", "291280009243618888211558641"},
		{"3", "3", "529510939", "715827883", "2903110321"},
		{"5", "3761", "7484047069", "140737471578113"},
		{"3", "11", "2281", "174763", "3011347479614249131"},
		{"641", "6700417", "18446744069414584321"},
		{"3", "971", "1553", "31817", "1100876018364883721"},
		{"5", "29", "113", "197", "19707683773", "4981857697937"},
		{"3", "3", "3", "19", "67", "683", "5347", "20857", "242099935645987"},
		{"17", "401", "61681", "340801", "2787601", "3173389601"},
	}
	two := One().Increment()
	for n := int64(63); n <= 85; n++ {
		testMe, _ := two.PowerInt64(n)
		testMe = testMe.Increment()
		// FIX ME reinstate these tests once we have a decent factorization algorithm
		if n == 58 || n == 71 || n == 74 || n == 82 || n == 84 /* || n == 88 || n == 89 || n == 93 || n == 94 || n == 98 */ {
			// FIX ME skip these because they take too long.  Need to implement Elliptic Curve Factorization or Number Field Sieve!
			continue
		}
		ans := make([]*Element, 0, 5)
		for _, s := range results[n-1] {
			factor, err := FromString(s)
			assert.NoError(err)
			ans = append(ans, factor)
		}
		if result, err := PrimeFactors(testMe); assert.NoError(err) {
			assert.Equal(result, ans)
		}
	}
	// Passing 0 should return an error
	_, err := PrimeFactors(Zero())
	assert.Error(err)
	// Check a Mersenne number that is prime
	n := toMersenneNumber(57885161)
	if result, err := PrimeFactors(n); assert.NoError(err) {
		if assert.Equal(len(result), 1) {
			assert.True(n.IsEqualTo(result[0]))
		}
	}
	// Check a Mersenne number that is not prime
	n = toMersenneNumber(68)
	factors := []int64{3, 5, 137, 953, 26317, 43691, 131071}
	if result, err := PrimeFactors(n); assert.NoError(err) {
		if assert.Equal(len(result), len(factors)) {
			for i, m := range factors {
				assert.True(FromInt64(m).IsEqualTo(result[i]))
			}
		}
	}
}

// TestGCD tests GCD.
func TestGCD(t *testing.T) {
	assert := assert.New(t)
	// Some test slices -- the first element of each slice is the expected GCD
	tests := [][]int{
		{0},
		{7, 7},
		{3, -3},
		{0, 0},
		{1, 2, 3},
		{1, -2, 3},
		{3, 0, 3},
		{3, 0, -3},
		{3, 3, 0},
		{3, -3, 0},
		{0, 0, 0},
		{5, 10, 15, 20, 30, 60},
		{5, 10, -15, 20, -30, 60},
		{5, -10, -15, -20, -30, -60},
	}
	for _, test := range tests {
		expected := FromInt(test[0])
		S := SliceFromIntSlice(test[1:len(test)])
		assert.True(expected.IsEqualTo(GCD(S...)))
	}
	// Create some very long slices with known GCD
	size := 5000
	for i := 2; i <= 7; i++ {
		expected := FromInt(i)
		S := make([]*Element, 0, size)
		for j := 0; j < size; j++ {
			if j%7 == 0 {
				S = append(S, Multiply(expected, FromInt(-j-2)))
			} else if j%7 == 1 {
				S = append(S, Zero())
			} else {
				S = append(S, Multiply(expected, FromInt(j+2)))
			}
		}
		assert.True(expected.IsEqualTo(GCD(S...)))
	}
}

// TestXGCDOfPair tests XGCDOfPair.
func TestXGCDOfPair(t *testing.T) {
	assert := assert.New(t)
	// Tests in the format {a,b,gcd,u,v} where u * a + v * b = gcd.
	tests := [][]int{
		{0, 0, 0, 0, 0},
		{0, 1, 1, 0, 1},
		{1, 0, 1, 1, 0},
		{0, -1, 1, 0, -1},
		{-1, 0, 1, -1, 0},
		{1, 1, 1, 0, 1},
		{-1, 1, 1, 0, 1},
		{1, -1, 1, 0, -1},
		{-1, -1, 1, 0, -1},
		{3, 11, 1, 4, -1},
		{-14, 21, 7, 1, 1},
		{100, -64, 4, -7, -11},
	}
	// Run the tests
	for _, test := range tests {
		gcd, a, b := XGCDOfPair(FromInt(test[0]), FromInt(test[1]))
		if assert.True(gcd.IsEqualToInt64(int64(test[2])), "Expected: %d * %d + %d * %d = %d\nGot: %s * %d + %s * %d = %s", test[3], test[0], test[4], test[1], test[2], a.String(), test[0], b.String(), test[1], gcd.String()) {
			assert.True(a.IsEqualToInt64(int64(test[3])), "Expected: %d * %d + %d * %d = %d\nGot: %s * %d + %s * %d = %s", test[3], test[0], test[4], test[1], test[2], a.String(), test[0], b.String(), test[1], gcd.String())
			assert.True(b.IsEqualToInt64(int64(test[4])), "Expected: %d * %d + %d * %d = %d\nGot: %s * %d + %s * %d = %s", test[3], test[0], test[4], test[1], test[2], a.String(), test[0], b.String(), test[1], gcd.String())
		}
	}
}
