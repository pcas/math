// Slice contains utilities for working with slices of integers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"bitbucket.org/pcastools/hash"
	sliceutil "bitbucket.org/pcastools/slice"
	"fmt"
	"io"
)

// Slice wraps an []*Element, implementing slice.Interface.
type Slice []*Element

// IntSlice wraps an []int, implementing slice.Interface and returning the entries as *Elements.
type IntSlice []int

// Int32Slice wraps an []int32, implementing slice.Interface and returning the entries as *Elements.
type Int32Slice []int32

// Int64Slice wraps an []int64, implementing slice.Interface and returning the entries as *Elements.
type Int64Slice []int64

// Uint64Slice wraps an []uint64, implementing slice.Interface and returning the entries as *Elements.
type Uint64Slice []uint64

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// copySliceInt64 returns a copy of the slice S. The capacity will be preserved.
func copySliceInt64(S []int64) []int64 {
	T := make([]int64, len(S), cap(S))
	copy(T, S)
	return T
}

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Slice) Entry(i int) object.Element {
	return S[i]
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Slice) Hash() hash.Value {
	v := hash.NewSequenceHasher()
	defer hash.ReuseSequenceHasher(v)
	for _, s := range S {
		v.Add(s)
	}
	return v.Hash()
}

/////////////////////////////////////////////////////////////////////////
// IntSlice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S IntSlice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S IntSlice) Entry(i int) object.Element {
	return FromInt(S[i])
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S IntSlice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S IntSlice) Hash() hash.Value {
	return hash.IntSlice(S)
}

/////////////////////////////////////////////////////////////////////////
// Int32Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Int32Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Int32Slice) Entry(i int) object.Element {
	return FromInt32(S[i])
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Int32Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Int32Slice) Hash() hash.Value {
	return hash.Int32Slice(S)
}

/////////////////////////////////////////////////////////////////////////
// Int64Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Int64Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Int64Slice) Entry(i int) object.Element {
	return FromInt64(S[i])
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Int64Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Int64Slice) Hash() hash.Value {
	return hash.Int64Slice(S)
}

/////////////////////////////////////////////////////////////////////////
// Uint64Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (S Uint64Slice) Len() int {
	return len(S)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (S Uint64Slice) Entry(i int) object.Element {
	return FromUint64(S[i])
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (S Uint64Slice) Slice(k int, m int) slice.Interface {
	return S[k:m]
}

// Hash returns a hash value for this slice.
func (S Uint64Slice) Hash() hash.Value {
	return hash.Uint64Slice(S)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SliceFromString converts a string into a slice of integers.
func SliceFromString(str string) ([]*Element, error) {
	// Normalize the string
	str = sliceutil.TrimSpaceAndSurroundingBrackets(str)
	// Get the empty string out of the way
	if len(str) == 0 {
		return make([]*Element, 0), nil
	}
	// Validate the string
	ok, length := sliceutil.AreIntegers(str, sliceutil.Sep, 0)
	if !ok {
		return nil, fmt.Errorf(sliceutil.MsgFailedToConvertIndex, length)
	}
	// Create the destination slice and splitter
	vals := make([]*Element, 0, length)
	S := sliceutil.DefaultStringSplitter(str, sliceutil.Sep)
	// Start converting the entries
	c, err := S.Next()
	for err == nil {
		var n *Element
		if n, err = FromString(c); err == nil {
			vals = append(vals, n)
			c, err = S.Next()
		}
	}
	// Handle any errors
	if err != io.EOF {
		return nil, fmt.Errorf(sliceutil.MsgFailedToConvertIndex, len(vals))
	}
	// Looks good
	return vals, nil
}

// CopySlice returns a copy of the slice S. The capacity will be preserved.
func CopySlice(S []*Element) []*Element {
	T := make([]*Element, len(S), cap(S))
	copy(T, S)
	return T
}

// ReverseSlice returns a new slice whose entries are equal to the entries of S, but in the reverse order.
func ReverseSlice(S []*Element) []*Element {
	n := len(S)
	T := make([]*Element, 0, n)
	for i := n - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}

// ZeroSlice returns a slice of length n populated with the zero element.
func ZeroSlice(n int) []*Element {
	if n <= 0 {
		return []*Element{}
	}
	zero := Zero()
	S := make([]*Element, 0, n)
	for i := 0; i < n; i++ {
		S = append(S, zero)
	}
	return S
}

// SliceFromIntSlice returns a slice of integers given by the slice S.
func SliceFromIntSlice(S []int) []*Element {
	T := make([]*Element, 0, len(S))
	for _, n := range S {
		T = append(T, FromInt(n))
	}
	return T
}

// SliceFromInt32Slice returns a slice of integers given by the slice S.
func SliceFromInt32Slice(S []int32) []*Element {
	T := make([]*Element, 0, len(S))
	for _, n := range S {
		T = append(T, FromInt32(n))
	}
	return T
}

// SliceFromInt64Slice returns a slice of integers given by the slice S.
func SliceFromInt64Slice(S []int64) []*Element {
	T := make([]*Element, 0, len(S))
	for _, n := range S {
		T = append(T, FromInt64(n))
	}
	return T
}

// SliceFromUint64Slice returns a slice of integers given by the slice S.
func SliceFromUint64Slice(S []uint64) []*Element {
	T := make([]*Element, 0, len(S))
	for _, n := range S {
		T = append(T, FromUint64(n))
	}
	return T
}

// SliceToInt64Slice attempts to convert the given slice of integers to a slice of int64s. Returns the new slice on success.
func SliceToInt64Slice(S []*Element) ([]int64, error) {
	T := make([]int64, 0, len(S))
	for _, n := range S {
		k, err := n.Int64()
		if err != nil {
			return nil, err
		}
		T = append(T, k)
	}
	return T, nil
}

// SliceToUint64Slice attempts to convert the given slice of integers to a slice of uint64s. Returns the new slice on success.
func SliceToUint64Slice(S []*Element) ([]uint64, error) {
	T := make([]uint64, 0, len(S))
	for _, n := range S {
		k, err := n.Uint64()
		if err != nil {
			return nil, err
		}
		T = append(T, k)
	}
	return T, nil
}
