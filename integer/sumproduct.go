// Sumproduct defines the Sum, Product, and similar functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/mathutil"
	"math/big"
)

// The maximum number of elements in the slice before we compute the sum, product, or dot-product concurrently.
const (
	maxSumProd = 500
	maxDotProd = maxSumProd / 2
)

// maxProdRange is the maximum range before we compute the product concurrently. Treat this value as a constant.
var maxProdRange = big.NewInt(200)

/////////////////////////////////////////////////////////////////////////
// Sum functions
/////////////////////////////////////////////////////////////////////////

// sumAsBigIntInternal passes the sum as a *big.Int to the given channel.
func sumAsBigIntInternal(c chan<- *big.Int, S []*Element) {
	c <- sumAsBigInt(S)
}

// sumAsBigInt returns the sum as a *big.Int.
func sumAsBigInt(S []*Element) *big.Int {
	if len(S) <= maxSumProd {
		// Calculate the sum
		n := bigintfactory.New().SetInt64(0)
		y := bigintfactory.New()
		for _, x := range S {
			if !x.IsZero() {
				if x.IsInt64() {
					n.Add(n, y.SetInt64(x.int64()))
				} else {
					n.Add(n, x.bigInt())
				}
			}
		}
		bigintfactory.Reuse(y)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Int)
	div := len(S) / 2
	go sumAsBigIntInternal(c, S[:div])
	go sumAsBigIntInternal(c, S[div:])
	n, m := <-c, <-c
	n.Add(n, m)
	bigintfactory.Reuse(m)
	return n
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
func Sum(S ...*Element) *Element {
	if k := len(S); k == 0 {
		return Zero()
	} else if k == 1 {
		return S[0]
	} else if k == 2 {
		return Add(S[0], S[1])
	}
	return fromBigIntAndReuse(sumAsBigInt(S))
}

/////////////////////////////////////////////////////////////////////////
// Product functions
/////////////////////////////////////////////////////////////////////////

// productAsBigIntInternal passes the product as a *big.Int to the given channel.
func productAsBigIntInternal(c chan<- *big.Int, S []*Element) {
	c <- productAsBigInt(S)
}

// productAsBigInt returns the product as a *big.Int.
func productAsBigInt(S []*Element) *big.Int {
	if len(S) <= maxSumProd {
		// Calculate the product
		n := bigintfactory.New().SetInt64(1)
		y := bigintfactory.New()
		for _, x := range S {
			if x.IsZero() {
				n.SetInt64(0)
				return n
			} else if !x.IsOne() {
				if x.IsInt64() {
					n.Mul(n, y.SetInt64(x.int64()))
				} else {
					n.Mul(n, x.bigInt())
				}
			}
		}
		bigintfactory.Reuse(y)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Int)
	div := len(S) / 2
	go productAsBigIntInternal(c, S[:div])
	go productAsBigIntInternal(c, S[div:])
	n, m := <-c, <-c
	n.Mul(n, m)
	bigintfactory.Reuse(m)
	return n
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one.
func Product(S ...*Element) *Element {
	if k := len(S); k == 0 {
		return One()
	} else if k == 1 {
		return S[0]
	} else if k == 2 {
		return Multiply(S[0], S[1])
	}
	return fromBigIntAndReuse(productAsBigInt(S))
}

/////////////////////////////////////////////////////////////////////////
// ProductOfRange functions
/////////////////////////////////////////////////////////////////////////

// productOfRangeAsBigIntInternal passes the product as a *big.Int to the given channel.
func productOfRangeAsBigIntInternal(c chan<- *big.Int, x1 *big.Int, x2 *big.Int) {
	c <- productOfRangeAsBigInt(x1, x2)
}

// productOfRangeAsBigInt returns the product x1 * (x1 + 1) * ... * (x2 - 1) as a *big.Int. Note: The product here is to x2 - 1 and NOT to x2 for implementation reasons.
func productOfRangeAsBigInt(x1 *big.Int, x2 *big.Int) *big.Int {
	// Should we do this in-place?
	y := bigintfactory.New().Add(x1, maxProdRange)
	if y.Cmp(x2) > 0 {
		// Are x1 and x2 small enough to fit in an int64?
		if x1.IsInt64() && x2.IsInt64() {
			return y.MulRange(x1.Int64(), x2.Int64()-1)
		}
		// No use -- we do this the naive way
		n := bigintfactory.New().Set(x1)
		y.Add(x1, oneAsBigInt)
		for y.Cmp(x2) < 0 {
			n.Mul(n, y)
			y.Add(y, oneAsBigInt)
		}
		bigintfactory.Reuse(y)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Int)
	y.Rsh(y.Add(x1, x2), 1)
	go productOfRangeAsBigIntInternal(c, x1, y)
	go productOfRangeAsBigIntInternal(c, y, x2)
	n, m := <-c, <-c
	bigintfactory.Reuse(y)
	n.Mul(n, m)
	bigintfactory.Reuse(m)
	return n
}

// ProductOfRangeInt64 returns the product of the elements x1 * (x1 + 1) * ... * x2. If x1 > x2 then ProductOfRange returns one.
func ProductOfRangeInt64(x1 int64, x2 int64) *Element {
	if x1 > x2 {
		return One()
	} else if x1 == x2 {
		return FromInt64(x1)
	} else if x1 <= 0 && x2 >= 0 {
		return Zero()
	} else if x1 == 1 && x2 <= factorialTableMaxN {
		return factorialTable[int(x2)]
	}
	t1, t2 := bigintfactory.New().SetInt64(x1), bigintfactory.New().SetInt64(x2)
	t2.Add(t2, oneAsBigInt)
	z := fromBigIntAndReuse(productOfRangeAsBigInt(t1, t2))
	bigintfactory.Reuse(t1)
	bigintfactory.Reuse(t2)
	return z
}

// ProductOfRange returns the product of the elements x1 * (x1 + 1) * ... * x2. If x1 > x2 then ProductOfRange returns one.
func ProductOfRange(x1 *Element, x2 *Element) *Element {
	if x1.IsInt64() && x2.IsInt64() {
		return ProductOfRangeInt64(x1.int64(), x2.int64())
	} else if x1.IsGreaterThan(x2) {
		return One()
	} else if x1.IsEqualTo(x2) {
		return x1
	} else if !x1.IsPositive() && !x2.IsNegative() {
		return Zero()
	}
	y := bigintfactory.New().Set(x2.bigInt())
	y.Add(y, oneAsBigInt)
	z := fromBigIntAndReuse(productOfRangeAsBigInt(x1.bigInt(), y))
	bigintfactory.Reuse(y)
	return z
}

/////////////////////////////////////////////////////////////////////////
// DotProduct functions
/////////////////////////////////////////////////////////////////////////

// dotProductAsBigIntInternal passes the dot-product of the slices S1 and S2 as a *big.Int to the given channel. The slices are assumed to be non-empty and of the same length.
func dotProductAsBigIntInternal(c chan<- *big.Int, S1 []*Element, S2 []*Element) {
	c <- dotProductAsBigInt(S1, S2)
}

// dotProductAsBigInt returns the dot-product of the slices S1 and S2 as a *big.Int. The slices are assumed to be non-empty and of the same length.
func dotProductAsBigInt(S1 []*Element, S2 []*Element) *big.Int {
	if len(S1) <= maxDotProd {
		// Calculate the dot-product
		n := bigintfactory.New().SetInt64(0)
		m := bigintfactory.New()
		for i, x := range S1 {
			y := S2[i]
			if !x.IsZero() && !y.IsZero() {
				if x.IsOne() {
					if y.IsInt64() {
						n.Add(n, m.SetInt64(y.int64()))
					} else {
						n.Add(n, y.bigInt())
					}
				} else if y.IsOne() {
					if x.IsInt64() {
						n.Add(n, m.SetInt64(x.int64()))
					} else {
						n.Add(n, x.bigInt())
					}
				} else if x.IsInt64() {
					if y.IsInt64() {
						if k, ok := mathutil.MultiplyInt64(x.int64(), y.int64()); ok {
							n.Add(n, m.SetInt64(k))
						} else {
							n.Add(n, m.Mul(m.SetInt64(x.int64()), y.bigInt()))
						}
					} else {
						n.Add(n, m.Mul(m.SetInt64(x.int64()), y.bigInt()))
					}
				} else if y.IsInt64() {
					n.Add(n, m.Mul(x.bigInt(), m.SetInt64(y.int64())))
				} else {
					n.Add(n, m.Mul(x.bigInt(), y.bigInt()))
				}
			}
		}
		bigintfactory.Reuse(m)
		return n
	}
	// Perform a recursive split
	c := make(chan *big.Int)
	div := len(S1) / 2
	go dotProductAsBigIntInternal(c, S1[:div], S2[:div])
	go dotProductAsBigIntInternal(c, S1[div:], S2[div:])
	n, m := <-c, <-c
	n.Add(n, m)
	bigintfactory.Reuse(m)
	return n
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.
func DotProduct(S1 []*Element, S2 []*Element) *Element {
	// Resize the slices to that they're both of the same length
	k := len(S1)
	if k2 := len(S2); k != k2 {
		if k < k2 {
			S2 = S2[:k]
		} else {
			k = k2
			S1 = S1[:k]
		}
	}
	// Fast-track the easy cases
	if k <= 2 {
		if k == 0 {
			return Zero()
		} else if k == 1 {
			return Multiply(S1[0], S2[0])
		}
		return MultiplyMultiplyThenAdd(S1[0], S2[0], S1[1], S2[1])
	}
	// Perform the dot-product
	return fromBigIntAndReuse(dotProductAsBigInt(S1, S2))
}
