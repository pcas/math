// Tests for naivefactorisation.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNaivePrimeFactorsInt64(t *testing.T) {
	assert := assert.New(t)
	// We test some small products of primes
	tests := [][]int64{
		{2, 2, 5, 5, 11, 17, 19, 29, 29},
		{2, 2, 7, 11, 17, 23, 29, 29},
		{2, 3, 19, 19, 19, 23, 23, 29},
		{2, 5, 5, 11, 11, 17, 23, 29},
		{2, 5, 7, 7, 13, 19, 29, 29},
		{2, 5, 17, 17, 17, 19, 29},
		{3, 7, 11, 11, 13, 17, 29, 29},
		{5, 5, 7, 7, 7, 7, 17, 23, 23, 29},
		{5, 5, 7, 7, 13, 13, 13, 23, 23, 29},
		{5, 5, 13, 13, 23, 23, 29},
	}
	for _, test := range tests {
		// Calculate the product
		n := int64(1)
		for _, p := range test {
			n *= p
		}
		// Factorise the product
		S := naivePrimeFactorsInt64StartingFrom(n, 2)
		// Assert that the result agrees with the input
		if assert.Len(S, len(test)) {
			assert.Equal(S, test)
		}
	}
}
