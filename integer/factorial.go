// Factorial defines the Factorial and similar functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/fatal"
	"math/big"
)

// factorialTable is a table of small-valued factorials as *Element objects. Here the entry at index n is equal to n!.
var factorialTable []*Element

// factorialTableMaxN is the maximum value of n such that n! is in the factorial table.
const factorialTableMaxN = 512

// binomialTable is a table of small-valued binomial coefficients as *Element objects. Here the entry at position [n][k] is equal to (n + 2) \choose (k + 2), with k < n / 2.
var binomialTable [][]*Element

// binomialTableMaxN is the maximum value of n such that n \choose k is in the binomial table.
const binomialTableMaxN = 128

/////////////////////////////////////////////////////////////////////////
// Private functions
/////////////////////////////////////////////////////////////////////////

// initFactorialTable is called from the package's init function, and should NOT be called directly.
func initFactorialTable() {
	// The values of n! that fit in an int64
	factorials := [...]int64{
		1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800,
		479001600, 6227020800, 87178291200, 1307674368000, 20922789888000,
		355687428096000, 6402373705728000, 121645100408832000,
		2432902008176640000,
	}
	// Seed the factorial look-up table with the pre-computed factorials above
	factorialTable = make([]*Element, 0, factorialTableMaxN+1)
	for _, n := range factorials {
		factorialTable = append(factorialTable, FromInt64(n))
	}
	// Extend the factorial look-up table out to the desired size
	for i := len(factorials); i <= factorialTableMaxN; i++ {
		factorialTable = append(factorialTable, factorialTable[i-1].ScalarMultiplyByInt64(int64(i)))
	}
}

// initBinomialTable is called from the package's init function, and should NOT be called directly.
func initBinomialTable() {
	// Build the binomial look-up table
	binomialTable = make([][]*Element, 0, binomialTableMaxN-2)
	for i := 0; i < binomialTableMaxN-2; i++ {
		bs := make([]*Element, 0, i/2)
		for j := 0; j < i/2; j++ {
			bs = append(bs, BinomialInt64(int64(i+2), int64(j+2)))
		}
		binomialTable = append(binomialTable, bs)
	}
}

/////////////////////////////////////////////////////////////////////////
// Factorial functions
/////////////////////////////////////////////////////////////////////////

// factorialAsBigInt returns the value n!, for a non-negative value of n. By convention 0! is 1. Assumes that n is non-negative.
func factorialAsBigInt(n *big.Int) *big.Int {
	// Get the easy cases out of the way
	if n.BitLen() == 0 || n.Cmp(oneAsBigInt) == 0 {
		return bigintfactory.New().SetInt64(1)
	}
	// If n is small the result might be in the table
	if n.IsInt64() {
		if k := n.Int64(); k <= factorialTableMaxN {
			m := factorialTable[int(k)]
			if m.IsInt64() {
				return bigintfactory.New().SetInt64(m.int64())
			}
			return bigintfactory.New().Set(m.bigInt())
		}
	}
	// No luck -- we need to calculate it
	t1 := bigintfactory.New().SetInt64(factorialTableMaxN + 1)
	t2 := bigintfactory.New().Add(n, oneAsBigInt)
	res := productOfRangeAsBigInt(t1, t2)
	bigintfactory.Reuse(t1)
	bigintfactory.Reuse(t2)
	return res.Mul(factorialTable[factorialTableMaxN].bigInt(), res)
}

// FactorialInt64 returns the value n!, for a non-negative value of n. By convention 0! is 1.
func FactorialInt64(n int64) (*Element, error) {
	// Get the easy cases out of the way
	if n < 0 {
		return nil, errors.IllegalNegativeArg.New()
	} else if n == 0 || n == 1 {
		return One(), nil
	}
	// If n is small the result might be in the table
	if n <= factorialTableMaxN {
		return factorialTable[int(n)], nil
	}
	// No luck -- we need to calculate it
	t1 := bigintfactory.New().SetInt64(factorialTableMaxN + 1)
	t2 := bigintfactory.New().SetInt64(n)
	res := productOfRangeAsBigInt(t1, t2.Add(t2, oneAsBigInt))
	bigintfactory.Reuse(t1)
	bigintfactory.Reuse(t2)
	res.Mul(factorialTable[factorialTableMaxN].bigInt(), res)
	return fromBigIntAndReuse(res), nil
}

// Factorial returns the value n!, for a non-negative value of n. By convention 0! is 1.
func Factorial(n *Element) (*Element, error) {
	// Get the easy cases out of the way
	if n.IsZero() || n.IsOne() {
		return One(), nil
	} else if n.IsNegative() {
		return nil, errors.IllegalNegativeArg.New()
	}
	// If n is small the result might be in the table
	if n.IsInt64() {
		if k := n.int64(); k <= factorialTableMaxN {
			return factorialTable[int(k)], nil
		}
	}
	// No luck -- we need to calculate it
	t1 := bigintfactory.New().SetInt64(factorialTableMaxN + 1)
	t2 := bigintfactory.New().Add(n.bigInt(), oneAsBigInt)
	res := productOfRangeAsBigInt(t1, t2)
	bigintfactory.Reuse(t1)
	bigintfactory.Reuse(t2)
	res.Mul(factorialTable[factorialTableMaxN].bigInt(), res)
	return fromBigIntAndReuse(res), nil
}

/////////////////////////////////////////////////////////////////////////
// Binomial functions
/////////////////////////////////////////////////////////////////////////

// BinomialInt64 return the binomial coefficient of n \choose k. By convention this returns zero if either n or k are negative.
func BinomialInt64(n int64, k int64) *Element {
	// Get the easy cases out of the way
	if n < 0 || k < 0 || k > n {
		return Zero()
	} else if k == 0 || k == n {
		return One()
	} else if k == 1 || k == n-1 {
		return FromInt64(n)
	}
	// If necessary shift k into the lower half 1 <= k <= n/2
	if k > n/2 {
		k = n - k
	}
	// Possibly this is in the binomial table
	if n-2 < int64(len(binomialTable)) {
		return binomialTable[int(n-2)][int(k-2)]
	}
	// Calculate the result
	b, err := FactorialInt64(k)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	var quo *Element
	if quo, err = Quotient(ProductOfRangeInt64(n-k+1, n), b); err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return quo
}

// Binomial return the binomial coefficient of n \choose k. By convention this returns zero if either n or k are negative.
func Binomial(n *Element, k *Element) *Element {
	// Get the easy cases out of the way
	if n.IsNegative() || k.IsNegative() || k.IsGreaterThan(n) {
		return Zero()
	} else if k.IsZero() || k.IsEqualTo(n) {
		return One()
	} else if k.IsOne() {
		return n
	}
	// Try to work directly with int64s if possible
	if n.IsInt64() {
		return BinomialInt64(n.int64(), k.int64())
	}
	// If necessary shift k into the lower half 1 <= k <= n/2
	nn := n.bigInt()
	kk := bigintfactory.New().Set(k.bigInt())
	t1 := bigintfactory.New().SetInt64(2)
	t2 := bigintfactory.New().Mul(t1, kk)
	if t2.Cmp(nn) > 0 {
		kk.Sub(nn, kk)
		if kk.Cmp(oneAsBigInt) == 0 {
			bigintfactory.Reuse(kk)
			bigintfactory.Reuse(t1)
			bigintfactory.Reuse(t2)
			return n
		}
	}
	// Calculate the result
	a := productOfRangeAsBigInt(
		t1.Sub(nn, kk).Add(t1, oneAsBigInt),
		t2.Add(nn, oneAsBigInt))
	bigintfactory.Reuse(t1)
	bigintfactory.Reuse(t2)
	res := fromBigIntAndReuse(a.Div(a, factorialAsBigInt(kk)))
	bigintfactory.Reuse(kk)
	return res
}
