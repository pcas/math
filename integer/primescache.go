// Primescache implements NthPrime, which returns the N-th prime from a cache.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// nthPrimeBlockSize is the block size used by the N-th prime cache. Important: If you adjust this constant you will have to recalculate the values in nthPrimeSeed below!
const nthPrimeBlockSize = 1024

// nthPrimeMaxIndex is the largest value of N contained in the N-th prime cache.
const nthPrimeMaxIndex = (int64(len(nthPrimeSeed)) + 1) * nthPrimeBlockSize

// nthPrimeSeed is a pre-computed set of seeds for the N-th prime data, where the i-th entry is the ((i+1) * nthPrimeBlockSize + 1)-th prime.
var nthPrimeSeed = [...]int64{
	8167,
	17881,
	28183,
	38891,
	49871,
	60961,
	72469,
	84047,
	95617,
	107609,
	119633,
	131543,
	143617,
	155719,
	168293,
	180511,
	192931,
	205559,
	218143,
	230471,
	243479,
	256033,
	268897,
	281719,
	294821,
	307651,
	320471,
	333427,
	346441,
	359419,
	372941,
	386117,
	399151,
	412591,
	425851,
	438983,
	452213,
	465977,
	479029,
	492619,
	505871,
	519349,
	532739,
	546509,
	560093,
	573647,
	587057,
	600703,
	614267,
	628057,
	642049,
	655637,
	669527,
	683251,
	696853,
	710531,
	724441,
	738197,
	752299,
	766079,
	779971,
	793757,
	807629,
	821647,
	835441,
	849539,
	863633,
	877397,
	891491,
	905651,
	919591,
	933643,
	947819,
	962033,
	976091,
	990637,
	1004677,
	1018733,
	1032841,
	1047127,
	1061623,
	1075727,
	1090217,
	1104193,
	1118023,
	1132519,
	1146967,
	1161137,
	1175257,
	1189121,
	1203641,
	1217963,
	1232531,
	1247189,
	1261373,
	1275749,
	1289933,
	1304581,
	1319083,
	1333621,
	1348129,
	1362643,
	1377169,
	1391521,
	1405879,
	1420831,
	1434997,
	1449551,
	1463897,
	1478513,
	1492823,
	1507111,
	1521853,
	1536679,
	1551463,
	1565929,
	1580699,
	1595611,
	1610149,
	1624963,
	1639471,
	1654127,
	1668683,
	1683271,
	1698089,
	1712759,
	1727597,
}

// _primesCache is a cache of primes. Do not access this cache directly -- instead use the function nthPrimeInt64.
var _primesCache = struct {
	rw     sync.RWMutex     // Mutex controlling access to the slice of blocks
	blocks []*nthPrimeBlock // The slice of blocks
}{}

// nthPrimeBlock manages a block of data for the N-th prime cache.
type nthPrimeBlock struct {
	N         int64        // The initial N in this block
	rw        sync.RWMutex // Mutex controlling access to the slice
	c         *sync.Cond   // Condition used to broadcast updates to the slice
	extending bool         // Is the slice being extended?
	primes    []int64      // The slice of primes
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// extendPrimeSlice fills us the given slice to its capacity with consecutive primes, and returns the slice. Assumes that the slice is non-empty.
func extendPrimeSlice(primes []int64) []int64 {
	// Make a note of the last-known prime
	lastP := primes[len(primes)-1]
	// Loop until the slice is full
	num := cap(primes) - len(primes)
	if num < 4 {
		// We only need a small number of primes, so we use NextPrimeInt64
		for i := 0; i < num; i++ {
			// Calculate the next prime
			p, err := NextPrimeInt64(lastP)
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			// Add the new prime to the slice and move on
			primes = append(primes, p)
			lastP = p
		}
	} else {
		// We need quite a few primes, so we use an iterator
		np := NewPrimeIteratorInt64(lastP)
		defer np.Close()
		for i := 0; i < num; i++ {
			// Fetch the next prime from the iterator
			p, err := np.Next()
			if err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			// Add the new prime to the slice
			primes = append(primes, p)
		}
	}
	// Return the slice
	return primes
}

// nthPrimeInt64 returns the N-th prime, or 0 if the value of N is out-of-range. Note that primes are indexed from 1.
func nthPrimeInt64(N int64) int64 {
	// Sanity check
	if N <= 0 || N > int64(len(nthPrimeSeed)+1)*nthPrimeBlockSize {
		return 0
	}
	// The "first block" is read directly from the table of small primes
	if N <= nthPrimeBlockSize {
		return smallPrimesTable[N-1]
	}
	// Calculate the block to query
	idx := int((N-1)/nthPrimeBlockSize) - 1
	// Is this block in the slice?
	_primesCache.rw.RLock()
	if len(_primesCache.blocks) > idx {
		// Yes -- note the block, release the lock, and return the prime
		b := _primesCache.blocks[idx]
		_primesCache.rw.RUnlock()
		return b.NthPrime(N)
	}
	// No luck -- we might need to extend the block slice. Move to a write lock.
	_primesCache.rw.RUnlock()
	_primesCache.rw.Lock()
	// Did somebody do the work for us whilst we were acquiring our lock?
	if len(_primesCache.blocks) > idx {
		// Yes -- great
		b := _primesCache.blocks[idx]
		_primesCache.rw.Unlock()
		return b.NthPrime(N)
	}
	// No such luck -- extend the slice using the pre-computed seeds
	if len(_primesCache.blocks) == 0 {
		_primesCache.blocks = make([]*nthPrimeBlock, 0, idx+1)
	}
	for i := len(_primesCache.blocks); i <= idx; i++ {
		_primesCache.blocks = append(_primesCache.blocks, newNthPrimeBlock(int64(i+1)*nthPrimeBlockSize+1, nthPrimeSeed[i]))
	}
	// Note the block, release the lock, and return the prime
	b := _primesCache.blocks[idx]
	_primesCache.rw.Unlock()
	return b.NthPrime(N)
}

/////////////////////////////////////////////////////////////////////////
// nthPrimeBlock functions
/////////////////////////////////////////////////////////////////////////

// newNthPrimeBlock returns a new nthPrimeBlock starting at the given N and prime p.
func newNthPrimeBlock(N int64, p int64) *nthPrimeBlock {
	b := &nthPrimeBlock{
		N:      N,
		primes: []int64{p},
	}
	b.c = sync.NewCond(&b.rw)
	return b
}

// ExtendTo attempts to extend the block out to the N-th prime. Returns true on success, false otherwise.
func (b *nthPrimeBlock) ExtendTo(N int64) bool {
	// Sanity check
	idx := int(N - b.N)
	if idx < 0 || idx >= nthPrimeBlockSize {
		return false
	}
	// Acquire a read lock to check if the slice is already long enough
	b.rw.RLock()
	if len(b.primes) > idx {
		b.rw.RUnlock()
		return true
	}
	// The slice is too short -- release the read lock and acquire a write lock
	b.rw.RUnlock()
	b.rw.Lock()
	// Loop until either the slice is long enough, or we are free to extend it
	for len(b.primes) <= idx && b.extending {
		b.c.Wait()
	}
	// Has somebody else made the slice long enough?
	if len(b.primes) > idx {
		b.rw.Unlock()
		return true
	}
	// No luck -- we need to extend the slice
	b.extending = true
	// Make a copy of the slice so far, then release the write lock
	newPrimes := make([]int64, len(b.primes), idx+1)
	copy(newPrimes, b.primes)
	b.rw.Unlock()
	// Extend the slice to the required length
	newPrimes = extendPrimeSlice(newPrimes)
	// Acquire a write lock and swap the slices
	b.rw.Lock()
	b.primes = newPrimes
	b.extending = false
	// Broadcast that we're done, release the lock, and return success
	b.c.Broadcast()
	b.rw.Unlock()
	return true
}

// NthPrime returns the N-th prime if it is contained in this block, 0 otherwise.
func (b *nthPrimeBlock) NthPrime(N int64) int64 {
	// Sanity check
	idx := int(N - b.N)
	if idx < 0 || idx >= nthPrimeBlockSize {
		return 0
	}
	// Is this in the slice?
	b.rw.RLock()
	if len(b.primes) > idx {
		p := b.primes[idx]
		b.rw.RUnlock()
		return p
	}
	b.rw.RUnlock()
	// No -- we need to extend the slice to the correct length
	if !b.ExtendTo(N) {
		return 0
	}
	// The prime should now be in the slice
	var p int64
	b.rw.RLock()
	if len(b.primes) > idx {
		p = b.primes[idx]
	}
	b.rw.RUnlock()
	return p
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NthPrime returns the N-th prime p if 1 <= N <= 2^17, or an error if N is out of range. Note that primes are indexed from 1.
func NthPrime(N int64) (*Element, error) {
	if N <= 0 || N > nthPrimeMaxIndex {
		return nil, errors.InvalidArgRange.New(N, 1, nthPrimeMaxIndex)
	}
	return FromInt64(nthPrimeInt64(N)), nil
}

// NthPrimeInt64 returns the N-th prime p if 1 <= N <= 2^17, or an error if N is out of range. Note that primes are indexed from 1.
func NthPrimeInt64(N int64) (int64, error) {
	if N <= 0 || N > nthPrimeMaxIndex {
		return 0, errors.InvalidArgRange.New(N, 1, nthPrimeMaxIndex)
	}
	return nthPrimeInt64(N), nil
}
