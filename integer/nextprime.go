// Next prime calculates the smallest prime > |n|.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/fatal"
	"sync"
)

// maxPrimeInt64 is the largest prime that fits in an int64
const maxPrimeInt64 = 9223372036854775783

// PrimeIteratorInt64 provides an efficient way to iterate over the primes that fit in an int64.
type PrimeIteratorInt64 struct {
	m        sync.Mutex // The mutex controlling access
	consumed *sync.Cond // Signal that the next prime should be computed
	ready    *sync.Cond // Signal that the next prime is ready
	p        int64      // The next prime (or 0)
	lastErr  error      // The last error (if any)
	done     bool       // Are we done?
}

/////////////////////////////////////////////////////////////////////////
// PrimeIteratorInt64 functions
/////////////////////////////////////////////////////////////////////////

// NewPrimeIteratorInt64 returns a new prime iterator starting at the smallest prime number > |n|.
func NewPrimeIteratorInt64(n int64) *PrimeIteratorInt64 {
	// Create the iterator
	np := &PrimeIteratorInt64{}
	np.consumed = sync.NewCond(&np.m)
	np.ready = sync.NewCond(&np.m)
	// Start the background worker running
	go np.worker(n)
	// Return the iterator
	return np
}

// worker computes the next prime. Intended to be run as a separate go routine.
func (np *PrimeIteratorInt64) worker(n int64) {
	// Acquire a lock
	np.m.Lock()
	defer np.m.Unlock()
	// Check that |n| < the largest prime that will fit in an int64
	if n >= maxPrimeInt64 || n <= -maxPrimeInt64 {
		np.lastErr = errors.ArgOutOfRange.New()
		np.done = true
		np.ready.Broadcast()
		return
	}
	// Move to the absolute value
	if n < 0 {
		n = -n
	}
	// Handle the small cases
	if n < maxSmallCachedPrime {
		// Find the next prime in the small primes table
		var err error
		n, err = nextPrimeFromSmallPrimesTable(n)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		// Set the prime and wait for it to be consumed
		np.p = n
		np.ready.Signal()
		np.consumed.Wait()
		// Should we exit?
		if np.done {
			return
		}
		// Note the index in the small primes table
		idx := indexInSmallPrimesTable(n)
		if idx == -1 {
			panic("Illegal invalid index")
		}
		// Start working up through the table
		for idx++; idx < len(smallPrimesTable); idx++ {
			n = smallPrimesTable[idx]
			np.p = n
			np.ready.Signal()
			np.consumed.Wait()
			if np.done {
				return
			}
		}
	}
	// Find the next multiple of 6
	multiple := n
	for multiple%6 != 0 {
		multiple++
	}
	// If necessary, check multiple-5 and multiple-1
	if multiple-5 > n && IsPrimeInt64(multiple-5) {
		np.p = multiple - 5
		np.ready.Signal()
		np.consumed.Wait()
		if np.done {
			return
		}
	}
	if multiple-1 > n && IsPrimeInt64(multiple-1) {
		np.p = multiple - 1
		np.ready.Signal()
		np.consumed.Wait()
		if np.done {
			return
		}
	}
	// Step up in multiples of 6, checking multiple+1 and multiple+5
	for multiple < maxPrimeInt64 {
		if IsPrimeInt64(multiple + 1) {
			np.p = multiple + 1
			np.ready.Signal()
			np.consumed.Wait()
			if np.done {
				return
			}
		}
		if IsPrimeInt64(multiple + 5) {
			np.p = multiple + 5
			np.ready.Signal()
			np.consumed.Wait()
			if np.done {
				return
			}
		}
		multiple += 6
	}
	// We've reached the end of iteration
	np.lastErr = errors.ArgOutOfRange.New()
	np.done = true
	np.ready.Broadcast()
}

// Next returns the next prime from the iterator, or an error.
func (np *PrimeIteratorInt64) Next() (int64, error) {
	// Acquire a lock
	np.m.Lock()
	defer np.m.Unlock()
	// Are we in an error state? Has iteration stopped?
	if np.lastErr != nil {
		return 0, np.lastErr
	} else if np.done {
		return 0, errors.IteratorIsClosed.New()
	}
	// Wait for a prime to be ready
	for np.p == 0 {
		np.ready.Wait()
		// Are we in an error state? Has iteration stopped?
		if np.lastErr != nil {
			return 0, np.lastErr
		} else if np.done {
			return 0, errors.IteratorIsClosed.New()
		}
	}
	// Record the prime and signal that it's been consumed
	p := np.p
	np.p = 0
	np.consumed.Signal()
	// Return the prime
	return p, nil
}

// Close stops the iterator.
func (np *PrimeIteratorInt64) Close() error {
	// Acquire a lock
	np.m.Lock()
	defer np.m.Unlock()
	// Are we in an error state? Has iteration stopped?
	if np.lastErr != nil || np.done {
		return np.lastErr
	}
	// Ask the iterator to exit and zero the prime
	np.done = true
	np.p = 0
	// Wake up any waiting go routines
	np.ready.Broadcast()
	np.consumed.Signal()
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NextPrimeInt64 returns the smallest prime number > |n| if this fits in an int64, and returns an error otherwise
func NextPrimeInt64(n int64) (int64, error) {
	// Check that |n| < the largest prime that will fit in an int64 and move to
	// the absolute value
	if n >= maxPrimeInt64 || n <= -maxPrimeInt64 {
		return 0, errors.ArgOutOfRange.New()
	} else if n < 0 {
		n = -n
	}
	// Get the small cases out of the way
	if n < 2 {
		return 2, nil
	} else if n < maxSmallCachedPrime {
		return nextPrimeFromSmallPrimesTable(n)
	}
	// Find the next multiple of 6
	multiple := n
	for multiple%6 != 0 {
		multiple++
	}
	// If necessary, check multiple-5 and multiple-1
	if multiple-5 > n && IsPrimeInt64(multiple-5) {
		return multiple - 5, nil
	} else if multiple-1 > n && IsPrimeInt64(multiple-1) {
		return multiple - 1, nil
	}
	// Now step up in multiples of 6, checking multiple+1 and multiple+5
	for {
		if IsPrimeInt64(multiple + 1) {
			return multiple + 1, nil
		} else if IsPrimeInt64(multiple + 5) {
			return multiple + 5, nil
		}
		multiple += 6
	}
}

// NextPrime returns the smallest prime number > |n|. Primality testing is done by IsPrime, which in particular is probabilistic for large values.
func NextPrime(n *Element) *Element {
	// Try to use NextPrimeInt64
	if n.IsInt64() {
		if p, err := NextPrimeInt64(n.int64()); err == nil {
			return FromInt64(p)
		}
	}
	// Move to a *big.Int and use the absolute value
	m := bigintfactory.New().Abs(n.bigInt())
	// Find the next multiple of 6
	six := bigintfactory.New().SetInt64(6)
	multiple := bigintfactory.New().Set(m)
	t := bigintfactory.New()
	for t.Mod(multiple, six).BitLen() != 0 {
		multiple.Add(multiple, oneAsBigInt)
	}
	// If necessary, check multiple-5 and multiple-1
	five := bigintfactory.New().SetInt64(5)
	if t.Sub(multiple, five).Cmp(m) > 0 && isPrimeBigInt(t) {
		goto result
	} else if t.Sub(multiple, oneAsBigInt).Cmp(m) > 0 && isPrimeBigInt(t) {
		goto result
	}
	// Now step up in multiples of 6, checking multiple+1 and multiple+5
	for {
		if isPrimeBigInt(t.Add(multiple, oneAsBigInt)) {
			goto result
		} else if isPrimeBigInt(t.Add(multiple, five)) {
			goto result
		}
		multiple.Add(multiple, six)
	}
result:
	bigintfactory.Reuse(m)
	bigintfactory.Reuse(six)
	bigintfactory.Reuse(multiple)
	bigintfactory.Reuse(five)
	return fromBigIntAndReuse(t)
}
