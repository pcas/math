// Tests for slice.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// testSubslice tests subslices of the slice S. Requires that S is of length at least three.
func testSubslice(t *testing.T, S slice.Interface) {
	assert := assert.New(t)
	R := Ring()
	// Sanity check
	n := S.Len()
	if n < 3 {
		panic("Slice must be of length at least three.")
	}
	// Create a subslice
	T := slice.Slice(S, 1, n-1)
	assert.Equal(T.Len(), n-2)
	for i := 0; i < n-2; i++ {
		assert.True(R.AreEqual(S.Entry(i+1), T.Entry(i)))
	}
	// Asking for illegal subslices should panic
	assert.Panics(func() { slice.Slice(S, -1, 0) })
	assert.Panics(func() { slice.Slice(S, 0, -1) })
	assert.Panics(func() { slice.Slice(S, 1, 0) })
	assert.Panics(func() { slice.Slice(S, 0, n+1) })
	assert.Panics(func() { slice.Slice(S, n, n+1) })
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestSliceFromString tests FromString.
func TestSliceFromString(t *testing.T) {
	assert := assert.New(t)
	// An empty slice
	if S, err := SliceFromString(""); assert.NoError(err) {
		assert.Len(S, 0)
	}
	if S, err := SliceFromString("  [  ]  "); assert.NoError(err) {
		assert.Len(S, 0)
	}
	// A non-empty slice
	if S, err := SliceFromString("   1,   -3   ,   40  "); assert.NoError(err) && assert.Len(S, 3) {
		assert.True(S[0].IsEqualToInt64(1))
		assert.True(S[1].IsEqualToInt64(-3))
		assert.True(S[2].IsEqualToInt64(40))
	}
	if S, err := SliceFromString("  [   -1 ,  17  ,    1112    ] "); assert.NoError(err) && assert.Len(S, 3) {
		assert.True(S[0].IsEqualToInt64(-1))
		assert.True(S[1].IsEqualToInt64(17))
		assert.True(S[2].IsEqualToInt64(1112))
	}
	if S, err := SliceFromString("[1,2,3,4,-5]"); assert.NoError(err) && assert.Len(S, 5) {
		assert.True(S[0].IsEqualToInt64(1))
		assert.True(S[1].IsEqualToInt64(2))
		assert.True(S[2].IsEqualToInt64(3))
		assert.True(S[3].IsEqualToInt64(4))
		assert.True(S[4].IsEqualToInt64(-5))
	}
	// An invalid string should return an error
	_, err := SliceFromString(" [  1, 2,   3")
	assert.Error(err)
	_, err = SliceFromString("   1, 2,   3x")
	assert.Error(err)
}

// TestCopySlice tests CopySlice.
func TestCopySlice(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of integers
	S := []int{5, 0, 1, 10}
	T := make([]*Element, 0, len(S))
	for _, s := range S {
		T = append(T, FromInt(s))
	}
	// Make a copy
	U := CopySlice(T)
	// Assert equality
	assert.Len(U, len(T))
	for i, t := range T {
		assert.True(t.IsEqualTo(U[i]))
	}
}

// TestReverseSlice tests ReverseSlice.
func TestReverseSlice(t *testing.T) {
	assert := assert.New(t)
	// Create a slice of integers
	S := []int{5, 0, 1, 10}
	T := make([]*Element, 0, len(S))
	for _, s := range S {
		T = append(T, FromInt(s))
	}
	// Reverse the slice
	U := ReverseSlice(T)
	// Check the result
	assert.Len(U, len(T))
	for i, t := range T {
		assert.True(t.IsEqualTo(U[len(U)-i-1]))
	}
}

// TestSliceFromSlice tests SliceFromIntSlice, SliceFromInt32Slice, SliceFromInt64Slice, and SliceFromUint64Slice.
func TestSliceFromSlice(t *testing.T) {
	assert := assert.New(t)
	// Create the model slice of integers
	valsAsInt := []int{5, 0, 1, 10, 7}
	S := make([]*Element, 0, len(valsAsInt))
	for _, n := range valsAsInt {
		S = append(S, FromInt(n))
	}
	// Rebuild the input slice as int32s, int64s, and uint64s
	valsAsInt32 := make([]int32, 0, len(valsAsInt))
	valsAsInt64 := make([]int64, 0, len(valsAsInt))
	valsAsUint64 := make([]uint64, 0, len(valsAsInt))
	for _, n := range valsAsInt {
		valsAsInt32 = append(valsAsInt32, int32(n))
		valsAsInt64 = append(valsAsInt64, int64(n))
		valsAsUint64 = append(valsAsUint64, uint64(n))
	}
	// Create the slices of integers
	S1 := SliceFromIntSlice(valsAsInt)
	S2 := SliceFromInt32Slice(valsAsInt32)
	S3 := SliceFromInt64Slice(valsAsInt64)
	S4 := SliceFromUint64Slice(valsAsUint64)
	// Assert equality of the results
	assert.Equal(len(S), len(S1))
	assert.Equal(len(S), len(S2))
	assert.Equal(len(S), len(S3))
	assert.Equal(len(S), len(S4))
	for i, n := range S {
		assert.True(n.IsEqualTo(S1[i]))
		assert.True(n.IsEqualTo(S2[i]))
		assert.True(n.IsEqualTo(S3[i]))
		assert.True(n.IsEqualTo(S4[i]))
	}
}

// TestSliceToInt64Slice tests SliceToInt64Slice.
func TestSliceToInt64Slice(t *testing.T) {
	assert := assert.New(t)
	// Create a test slice
	vals := []int64{0, -7, 1, 99}
	S := SliceFromInt64Slice(vals)
	// Check that we recover the correct values
	if T, err := SliceToInt64Slice(S); assert.NoError(err) {
		assert.Equal(len(vals), len(T))
		for i, n := range vals {
			assert.Equal(n, T[i])
		}
	}
	// Append an integer too large to be converted to an int64
	if pow2, err := FromInt(2).PowerInt64(100); assert.NoError(err) {
		_, err = SliceToInt64Slice(append(S, pow2))
		assert.Error(err)
	}
}

// TestSlice tests Slice, IntSlice, Int32Slice, Int64Slice, and Uint64Slice.
func TestSlice(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)
	R := Ring()
	// Create the test slices
	k := 10
	S := make(Slice, 0, k)
	Sint := make(IntSlice, 0, k)
	Sint32 := make(Int32Slice, 0, k)
	Sint64 := make(Int64Slice, 0, k)
	Suint64 := make(Uint64Slice, 0, k)
	for i := 0; i < k; i++ {
		S = append(S, FromInt(i))
		Sint = append(Sint, i)
		Sint32 = append(Sint32, int32(i))
		Sint64 = append(Sint64, int64(i))
		Suint64 = append(Suint64, uint64(i))
	}
	// Check that they report the correct lengths
	require.Equal(S.Len(), k)
	require.Equal(Sint.Len(), k)
	require.Equal(Sint32.Len(), k)
	require.Equal(Sint64.Len(), k)
	require.Equal(Suint64.Len(), k)
	// Check that the entries agree
	for i := 0; i < k; i++ {
		if ok, err := R.AreEqual(S.Entry(i), Sint.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
		if ok, err := R.AreEqual(S.Entry(i), Sint32.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
		if ok, err := R.AreEqual(S.Entry(i), Sint64.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
		if ok, err := R.AreEqual(S.Entry(i), Suint64.Entry(i)); assert.NoError(err) {
			assert.True(ok)
		}
	}
	// Assert that the hashes agree
	value := S.Hash()
	assert.Equal(value, Sint.Hash())
	assert.Equal(value, Sint32.Hash())
	assert.Equal(value, Sint64.Hash())
	assert.Equal(value, Suint64.Hash())
	// Check that the hash agrees with the generic slice of object.Elements
	if Sgeneric, err := object.SliceToElementSlice(S); assert.NoError(err) {
		assert.Equal(value, slice.ElementSlice(Sgeneric).Hash())
	}
	// Asking for an out-of-range entry should panic
	assert.Panics(func() { S.Entry(-1) })
	assert.Panics(func() { S.Entry(k) })
	assert.Panics(func() { Sint.Entry(-1) })
	assert.Panics(func() { Sint.Entry(k) })
	assert.Panics(func() { Sint32.Entry(-1) })
	assert.Panics(func() { Sint32.Entry(k) })
	assert.Panics(func() { Sint64.Entry(-1) })
	assert.Panics(func() { Sint64.Entry(k) })
	assert.Panics(func() { Suint64.Entry(-1) })
	assert.Panics(func() { Suint64.Entry(k) })
	// Test subslices
	testSubslice(t, S)
	testSubslice(t, Sint)
	testSubslice(t, Sint32)
	testSubslice(t, Sint64)
	testSubslice(t, Suint64)
}
