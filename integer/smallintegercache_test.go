// Tests for smallintegercache.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestCache(t *testing.T) {
	assert := assert.New(t)
	// The integer cache means that, for each value in the range of the cache,
	// we should always get the same integer object back.
	val := FromInt64(smallIntegerCacheMin - 1)
	for i := smallIntegerCacheMin; i <= smallIntegerCacheMax; i++ {
		val = val.Increment()
		assert.True(val == FromInt64(int64(i)))
		assert.True(val == fromBigIntAndReuse(big.NewInt(int64(i))))
		if i >= 0 {
			assert.True(val == FromUint64(uint64(i)))
		}
	}
	// Test the nil case of fromBigIntAndReuse
	assert.True(fromBigIntAndReuse(nil).IsZero())
}
