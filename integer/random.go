// Random provides functions to generate random integers

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcastools/rand"
)

// Random returns a random integer in the range [0,max].  It will panic if max is negative.  This wraps crypto/rand.
func Random(max *Element) *Element {
	return FromBigInt(rand.BigInt(max.BigInt()))
}
