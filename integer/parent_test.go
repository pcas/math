// Tests for parent.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"fmt"
	"github.com/stretchr/testify/assert"
	"math/big"
	"math/rand"
	"testing"
	"time"
)

////////////////////////////////////////////////////////////////////////////////
// dummy types
////////////////////////////////////////////////////////////////////////////////

// We build two dummy types that implement the Element interface in object/element.go
// The first dummy type doesn't have a ToInteger method
type dummyElement int

func (x dummyElement) Hash() hash.Value {
	panic(fatal.NeverCalled())
}

func (x dummyElement) String() string {
	return fmt.Sprintf("%v", int(x))
}

func (x dummyElement) Parent() object.Parent {
	panic(fatal.NeverCalled())
}

// The second dummy type is identical except that it has a ToInteger method
type dummyToIntegerer int

func (x dummyToIntegerer) Hash() hash.Value {
	panic(fatal.NeverCalled())
}

func (x dummyToIntegerer) String() string {
	return fmt.Sprintf("%v", int(x))
}

func (x dummyToIntegerer) Parent() object.Parent {
	panic(fatal.NeverCalled())
}

func (x dummyToIntegerer) ToInteger() (*Element, error) {
	return FromInt(int(x)), nil
}

////////////////////////////////////////////////////////////////////////////////
// tests for Parent()
////////////////////////////////////////////////////////////////////////////////

func TestParent(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// The parent of an integer should be the (unique) ring of integers
	assert.Equal(Zero().Parent(), zZ)
	assert.Equal(One().Parent(), zZ)
	assert.Equal(FromInt(7).Parent(), zZ)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Contains()
////////////////////////////////////////////////////////////////////////////////

// Note that there are more meaningful tests of Contains in rationals/other_test.go

var containsDummyElementTests = []struct {
	value    dummyElement // input
	expected bool         // output
}{
	{0, false}, // expected is always false, because there is no ToInteger method
	{1, false},
	{23475629836, false},
	{-59187491287, false},
	{203987502938475, false},
}

var containsDummyToIntegererTests = []struct {
	value    dummyToIntegerer // input
	expected bool             // output
}{
	{0, true}, // expected is always true, because the ToInteger method always returns a nil error
	{1, true},
	{23475629836, true},
	{-59187491287, true},
	{203987502938475, true},
}

func TestContains(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	for _, test := range containsDummyElementTests {
		assert.Equal(zZ.Contains(test.value), test.expected)
	}
	for _, test := range containsDummyToIntegererTests {
		assert.Equal(zZ.Contains(test.value), test.expected)
	}
}

////////////////////////////////////////////////////////////////////////////////
// tests for String()
////////////////////////////////////////////////////////////////////////////////

func TestString(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	assert.Equal(zZ.String(), defaultName)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Zero()
////////////////////////////////////////////////////////////////////////////////

func TestZero(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// is the result of Zero() zero
	assert.Equal(zZ.Zero(), Zero())
	// is the string value correct?
	assert.Equal(Zero().String(), "0")
	// A nil Element should be equivalent to zero
	var nilElement *Element
	assert.Equal(nilElement.String(), "0")
}

////////////////////////////////////////////////////////////////////////////////
// tests for IsZero
////////////////////////////////////////////////////////////////////////////////

var isZeroTests = []struct {
	value    int64 // input
	expected bool  // output
}{
	{0, true},
	{1, false},
	{2519628152355403326, false},
	{-6633608913944366796, false},
	{-2570164108470030874, false},
}

func TestIsZero(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range isZeroTests {
		if ok, err := zZ.IsZero(FromInt64(test.value)); assert.NoError(err) {
			assert.Equal(ok, test.expected)
		}
	}
	// what about something that isn't contained in the Integers (i.e. isn't a toIntegerer)?
	var dummyzero dummyElement
	if ok, err := zZ.IsZero(dummyzero); assert.Error(err) {
		assert.False(ok)
	}
}

////////////////////////////////////////////////////////////////////////////////
// tests for One()
////////////////////////////////////////////////////////////////////////////////

func TestOne(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// is the result of One() one?
	assert.Equal(zZ.One(), One())
	// is the string value correct?
	assert.Equal(One().String(), "1")
}

////////////////////////////////////////////////////////////////////////////////
// tests for IsOne
////////////////////////////////////////////////////////////////////////////////

var isOneTests = []struct {
	value    int64 // input
	expected bool  // output
}{
	{0, false},
	{1, true},
	{2839456768, false},
	{-96148236827364, false},
	{-82462848624, false},
}

func TestIsOne(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range isOneTests {
		if ok, err := zZ.IsOne(FromInt64(test.value)); assert.NoError(err) {
			assert.Equal(ok, test.expected)
		}
	}
	// what about something that isn't contained in the Integers (i.e. isn't a toIntegerer)?
	var dummyone dummyElement = 1
	if ok, err := zZ.IsOne(dummyone); assert.Error(err) {
		assert.False(ok)
	}
}

////////////////////////////////////////////////////////////////////////////////
// tests for Add
////////////////////////////////////////////////////////////////////////////////

var addTests = []struct {
	input1   int64
	input2   int64
	expected int64
}{
	{2, 2, 4},
	{-1, 0, -1},
	{0, -8, -8},
	{2, -1, 1},
	{6107391297023812121, -538284141773095275, 5569107155250716846},
	{-8469872610549154651, 2266588354354472836, -6203284256194681815},
	{2780433034865515224, 1441223782790007373, 4221656817655522597},
	{1021316513242532787, -407189621864440949, 614126891378091838},
}

func TestAdd(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range addTests {
		if val, err := zZ.Add(FromInt64(test.input1), FromInt64(test.input2)); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// adding things that aren't in the Integers should result in an error
	var dummyzero dummyElement
	_, err := zZ.Add(dummyzero, dummyzero)
	assert.Error(err)
	_, err = zZ.Add(Zero(), dummyzero)
	assert.Error(err)
	_, err = zZ.Add(dummyzero, Zero())
	assert.Error(err)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Sum
////////////////////////////////////////////////////////////////////////////////

var sumTests = []struct {
	input    []int64
	expected int64
}{
	{input: []int64{2619431531439415447, -4287838741839121372, 1340830313952643612,
		3472342280694540543, 2975702878870232210, -3058853021326096206,
		1894766842521304180, -1040400630289336089, -184668514936159493,
		2780673677230039410},
		expected: 6511986616317462242},
	{input: []int64{-3227863064432207403, -1678456293825772698, 1917241035226240492,
		3733908616588510344, -3604878788910566191, -3109068230253633698,
		3806978491962059743, -2085787244620769999, 3057158193244865235,
		-285417185860865486},
		expected: -1476184470882139661},
	{input: []int64{-3415082993460010569, -804233385857094446, 2622024134095505930,
		-3698626153701090322, 4344509380122529832, 1898345932799204879,
		-3601466481534102494, -1654230514111019104, -3057811128668291130,
		3883754321942837107},
		expected: -3482816888371530317},
	{input: []int64{2959535881246778131, 1397275616783589006, 3187736368352247286,
		2608962890589860910, 487502967325412368, 3559487338139812582,
		-4380753828004402272, -2225247198193206489, -2747055397394985856,
		-224253700762278719},
		expected: 4623190938082826947},
	{input: []int64{-130815078854054985, 452989196627156267, -3419067492370456750,
		-374512874286790225, 4124160248280882821, -207029392285571157,
		1523920982459424489, 1863133310219519194, 1568219000296962277,
		-4384209269512046569},
		expected: 1016788630575025362},
}

func TestSum(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range sumTests {
		// convert the input *Elements
		seq := make([]*Element, 0, 10)
		for _, x := range test.input {
			seq = append(seq, FromInt64(x))
		}
		// convert the slice to object.Elements, do the sum, and then check
		if objs, err := object.SliceToElementSlice(seq); assert.NoError(err) {
			if val, err := zZ.Sum(objs...); assert.NoError(err) {
				if ok, err := zZ.AreEqual(val, Sum(seq...)); assert.NoError(err) {
					assert.True(ok)
				}
				if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// try a longer sum
	seq := make([]*Element, 0, 10000)
	for i := 0; i < 10000; i++ {
		seq = append(seq, FromInt(i))
	}
	expected := FromInt64(5000 * 9999)
	if objs, err := object.SliceToElementSlice(seq); assert.NoError(err) {
		if val, err := zZ.Sum(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, Sum(seq...)); assert.NoError(err) {
				assert.True(ok)
			}
			if ok, err := zZ.AreEqual(val, expected); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// make sure the length-0 special case is handled correctly
	input := make([]*Element, 0, 2)
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		if val, err := zZ.Sum(objs...); assert.NoError(err) {
			if ok, err := zZ.IsZero(val); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	assert.True(Sum(input...).IsZero())
	// make sure the length-1 special case is handled correctly
	input = append(input, FromInt(68))
	expected = FromInt(68)
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		if val, err := zZ.Sum(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, expected); assert.NoError(err) {
				assert.True(ok)
			}
			if ok, err := zZ.AreEqual(val, Sum(input...)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// summing things that aren't in the integers should result in an error
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		objs = append(objs, dummyElement(0))
		_, err = zZ.Sum(objs...)
		assert.Error(err)
	}
}

////////////////////////////////////////////////////////////////////////////////
// Benchmarks for Sum
////////////////////////////////////////////////////////////////////////////////

// randomObjectElementSequence returns a sequence of len integers, as
// object.Elements, of absolute value less than max.  This sequence is
// signed if signed is true, and unsigned if signed is false.
func randomObjectElementSequence(len int, max int, signed bool) []object.Element {
	result := make([]object.Element, len)
	var sign int
	// seed the random number generator
	seed := rand.NewSource(time.Now().UnixNano())
	rng := rand.New(seed)
	// populate the slice
	for i := 0; i < len; i++ {
		if signed {
			switch rng.Intn(2) { // this is 0 or 1
			case 0:
				sign = -1
			case 1:
				sign = 1
			}
		} else {
			sign = 1
		}
		result[i] = FromInt(sign * rng.Intn(max))
	}
	return result
}

// BenchmarkSum tests Sum with a sequence of 10^4 random Elements of absolute value less than 2^32-1
func BenchmarkSum_10000(b *testing.B) {
	zZ := Ring()
	seq := randomObjectElementSequence(10000, 2^32-1, true)
	b.ResetTimer()
	var result object.Element
	for i := 0; i < b.N; i++ {
		result, _ = zZ.Sum(seq...)
	}
	_ = result
}

// BenchmarkSum tests Sum with a sequence of 10^5 random Elements of absolute value less than 2^32-1
func BenchmarkSum_100000(b *testing.B) {
	zZ := Ring()
	seq := randomObjectElementSequence(100000, 2^32-1, true)
	b.ResetTimer()
	var result object.Element
	for i := 0; i < b.N; i++ {
		result, _ = zZ.Sum(seq...)
	}
	_ = result
}

// BenchmarkSum tests Sum with a sequence of 10^6 random Elements of absolute value less than 2^32-1
func BenchmarkSum_1000000(b *testing.B) {
	zZ := Ring()
	seq := randomObjectElementSequence(1000000, 2^32-1, true)
	b.ResetTimer()
	var result object.Element
	for i := 0; i < b.N; i++ {
		result, _ = zZ.Sum(seq...)
	}
	_ = result
}

/* BENCHMARK RESULTS 17 Sep 2015 on quad-core iMac with 3.4 GHz Intel Core i7  (Tom's office Mac)

                                          Go: single core    Go quad core      Magma        Mathematica (8 kernels, so 4 cores hyperthreaded)

BenchmarkSum_10000    	    3000	    421311 ns/op     222123 ns/op    500000 ns/op    1693000 ns/op
BenchmarkSum_100000   	     300	   4793621 ns/op    2209997 ns/op   4700000 ns/op   15978000 ns/op
BenchmarkSum_1000000  	      30	  59900860 ns/op   20738106 ns/op  52500000 ns/op  163088000 ns/op

so we roughly tie with Magma when single core, beating it by a factor of 2-3 on quad-core
we beat Mathematica by a factor of 2-3 single core, and by a factor of roughly 8 quad-core
*/

////////////////////////////////////////////////////////////////////////////////
// tests for Subtract
////////////////////////////////////////////////////////////////////////////////

var subtractTests = []struct {
	input1   int64
	input2   int64
	expected int64
}{
	{2, 2, 0},
	{-1, 0, -1},
	{0, -5, 5},
	{-3233329209106466404, -2793924701011454712, -439404508095011692},
	{1137976879401432351, -3885383001705755182, 5023359881107187533},
	{-1058879417797831678, -788607128118540945, -270272289679290733},
	{-1189275968893935794, 855383443640798874, -2044659412534734668},
	{1849422026583432340, -2675040546483902447, 4524462573067334787},
}

func TestSubtract(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range subtractTests {
		if val, err := zZ.Subtract(FromInt64(test.input1), FromInt64(test.input2)); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// subtracting things that aren't in the integers should result in an error
	var dummyzero dummyElement
	_, err := zZ.Subtract(dummyzero, dummyzero)
	assert.Error(err)
	_, err = zZ.Subtract(Zero(), dummyzero)
	assert.Error(err)
	_, err = zZ.Subtract(dummyzero, Zero())
	assert.Error(err)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Multiply
////////////////////////////////////////////////////////////////////////////////

var multiplyTests = []struct {
	input1   int64
	input2   int64
	expected int64
}{
	{2, 2, 4},
	{1, 2, 2},
	{-5, 1, -5},
	{0, 298347652, 0},
	{-2938765, 0, 0},
	{1141565257, -914071145, -1043471861558209265},
	{745441733, 2053867090, 1531038242921266970},
	{-1841527057, -701943173, 1292647345555931861},
	{-111809998, 1099970151, -122987660383369698},
}

func TestMultiply(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range multiplyTests {
		if val, err := zZ.Multiply(FromInt64(test.input1), FromInt64(test.input2)); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// multiplying things that aren't in the integers should result in an error
	var dummyzero dummyElement
	_, err := zZ.Multiply(dummyzero, dummyzero)
	assert.Error(err)
	_, err = zZ.Multiply(Zero(), dummyzero)
	assert.Error(err)
	_, err = zZ.Multiply(dummyzero, Zero())
	assert.Error(err)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Product
////////////////////////////////////////////////////////////////////////////////

var productTests = []struct {
	input    []int64
	expected int64
}{
	{input: []int64{1, 2, 3, 4, 5},
		expected: 120},
	{input: []int64{0, -1, 2, -3, 4},
		expected: 0},
	{input: []int64{1559, -518, 3429, -2997, -1131},
		expected: -9386262764091486},
	{input: []int64{933, 2657, 2240, 1440, 3596},
		expected: 28754339204505600},
	{input: []int64{-515, -10, 2055, 2633, -3435},
		expected: -95718670053750},
	{input: []int64{477, -1783, 3536, -1454, -4094},
		expected: -17901697878806976},
}

func TestProduct(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range productTests {
		// convert the input to *Elements
		seq := make([]*Element, 0, 10000)
		for _, x := range test.input {
			seq = append(seq, FromInt64(x))
		}
		// do the sum and then check
		if objs, err := object.SliceToElementSlice(seq); assert.NoError(err) {
			if val, err := zZ.Product(objs...); assert.NoError(err) {
				if ok, err := zZ.AreEqual(val, Product(seq...)); assert.NoError(err) {
					assert.True(ok)
				}
				if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// try a longer product
	seq := make([]*Element, 0, 10000)
	for i := 0; i < 10000; i++ {
		seq = append(seq, FromInt(i+1))
	}
	expected := fromBigIntAndReuse(big.NewInt(1).MulRange(1, 10000))
	if objs, err := object.SliceToElementSlice(seq); assert.NoError(err) {
		if val, err := zZ.Product(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, Product(seq...)); assert.NoError(err) {
				assert.True(ok)
			}
			if ok, err := zZ.AreEqual(val, expected); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// make sure the length-0 special case is handled correctly
	input := make([]*Element, 0, 2)
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		if val, err := zZ.Product(objs...); assert.NoError(err) {
			if ok, err := zZ.IsOne(val); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	assert.True(Product(input...).IsOne())
	// make sure the length-1 special case is handled correctly
	input = append(input, FromInt(121))
	expected = FromInt(121)
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		if val, err := zZ.Product(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, expected); assert.NoError(err) {
				assert.True(ok)
			}
			if ok, err := zZ.AreEqual(val, Product(input...)); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// taking the product of things that aren't in the integers should result in an error
	if objs, err := object.SliceToElementSlice(input); assert.NoError(err) {
		objs = append(objs, dummyElement(0))
		_, err = zZ.Product(objs...)
		assert.Error(err)
	}
}

// TestParent_ToElement tests ToElement.
func TestParent_ToElement(t *testing.T) {
	assert := assert.New(t)
	R := Ring()
	// ToElement should return the integer passed to it
	for i := -5; i <= 5; i++ {
		x := FromInt(i)
		if y, err := R.ToElement(x); assert.NoError(err) {
			if yy, ok := y.(*Element); assert.True(ok) {
				assert.True(x.IsEqualTo(yy))
			}
		}
	}
	// If passed a non-integer, this should error
	_, err := R.ToElement(dummyElement(7))
	assert.Error(err)
}

// TestParent_ScalarMultiplyByInteger tests ScalarMultiplyByInteger.
func TestParent_ScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	R := Ring()
	for n := -5; n <= 5; n++ {
		nn := FromInt(n)
		for c := -5; c <= 5; c++ {
			if m, err := R.ScalarMultiplyByInteger(FromInt(c), nn); assert.NoError(err) {
				if ok, err := R.AreEqual(m, FromInt(c*n)); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// If passed a non-integer, this should error
	_, err := R.ScalarMultiplyByInteger(One(), dummyElement(7))
	assert.Error(err)
}

// TestParent_FromInteger tests FromInteger.
func TestParent_FromInteger(t *testing.T) {
	assert := assert.New(t)
	R := Ring()
	for n := -10; n <= 10; n++ {
		nn := FromInt(n)
		if m, ok := R.FromInteger(nn).(*Element); assert.True(ok) {
			assert.True(nn.IsEqualTo(m))
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// tests for Power
////////////////////////////////////////////////////////////////////////////////

var powerTests = []struct {
	input    int64
	k        int64
	expected int64
}{
	{0, 0, 1},
	{0, 1, 0},
	{0, 2, 0},
	{0, 100000, 0},
	{0, 100001, 0},
	{1, 0, 1},
	{1, 1, 1},
	{1, 2, 1},
	{1, 100000, 1},
	{1, 100001, 1},
	{-1, 0, 1},
	{-1, 1, -1},
	{-1, 2, 1},
	{-1, 3, -1},
	{-1, 100000, 1},
	{-1, 100001, -1},
	{2, 0, 1},
	{2, 1, 2},
	{2, 2, 4},
	{2, 3, 8},
	{2, 4, 16},
	{-2, 0, 1},
	{-2, 1, -2},
	{-2, 2, 4},
	{-2, 3, -8},
	{-2, 4, 16},
	{3, 0, 1},
	{3, 1, 3},
	{3, 2, 9},
	{3, 3, 27},
	{3, 4, 81},
	{-3, 0, 1},
	{-3, 1, -3},
	{-3, 2, 9},
	{-3, 3, -27},
	{-3, 4, 81},
}

func TestPower(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values above
	for _, test := range powerTests {
		x := FromInt64(test.input)
		if val, err := zZ.Power(x, FromInt64(test.k)); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, FromInt64(test.expected)); assert.NoError(err) {
				assert.True(ok)
			}
		}
		if val, err := Power(x, FromInt64(test.k)); assert.NoError(err) {
			assert.True(val.IsEqualToInt64(test.expected))
		}
		if val, err := PowerInt64(x, test.k); assert.NoError(err) {
			assert.True(val.IsEqualToInt64(test.expected))
		}
		if val, err := x.Power(FromInt64(test.k)); assert.NoError(err) {
			assert.True(val.IsEqualToInt64(test.expected))
		}
		if val, err := x.PowerInt64(test.k); assert.NoError(err) {
			assert.True(val.IsEqualToInt64(test.expected))
		}
	}
	// powering by a negative value should result in an error
	x := FromInt(3)
	_, err := zZ.Power(x, FromInt(-1))
	assert.Error(err)
	_, err = Power(x, FromInt(-1))
	assert.Error(err)
	_, err = PowerInt64(x, -1)
	assert.Error(err)
	_, err = x.Power(FromInt(-1))
	assert.Error(err)
	_, err = x.PowerInt64(-1)
	assert.Error(err)
	// powering things that aren't in the integers should result in an error
	var dummyzero dummyElement
	_, err = zZ.Power(dummyzero, Zero())
	assert.Error(err)
	_, err = zZ.Power(dummyzero, One())
	assert.Error(err)
	_, err = zZ.Power(dummyzero, FromInt(2))
	assert.Error(err)
}

////////////////////////////////////////////////////////////////////////////////
// tests for Negate
////////////////////////////////////////////////////////////////////////////////

func TestNegate(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// Negating zero should give zero
	if val, err := zZ.Negate(Zero()); assert.NoError(err) {
		if ok, err := zZ.IsZero(val); assert.NoError(err) {
			assert.True(ok)
		}
	}
	assert.True(Zero().Negate().IsZero())
	assert.True(Negate(Zero()).IsZero())
	// Negating k should give -k
	for k := -10; k <= 10; k++ {
		input := FromInt(k)
		expected := FromInt(-k)
		if val, err := zZ.Negate(input); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, expected); assert.NoError(err) {
				assert.True(ok)
			}
		}
		assert.True(input.Negate().IsEqualTo(expected))
		assert.True(Negate(input).IsEqualTo(expected))
	}
	// Negating a non-Integer should give an error
	_, err := zZ.Negate(dummyElement(0))
	assert.Error(err)
}

////////////////////////////////////////////////////////////////////////////////
// tests for AreEqual
////////////////////////////////////////////////////////////////////////////////

func TestAreEqual(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// Zero and one are different
	if ok, err := zZ.AreEqual(Zero(), One()); assert.NoError(err) {
		assert.False(ok)
	}
	assert.False(AreEqual(Zero(), One()))
	assert.False(Zero().IsEqualTo(One()))
	// Equal things should be equal
	for k := -300; k <= 300; k++ {
		v1 := FromInt(5 * k)
		v2 := FromInt(5 * k)
		if ok, err := zZ.AreEqual(v1, v2); assert.NoError(err) {
			assert.True(ok)
		}
		assert.True(AreEqual(v1, v2))
		assert.True(v1.IsEqualTo(v2))
		assert.True(v1.IsEqualToInt64(int64(5 * k)))
	}
	// Unequal things should be unequal
	for k := -10; k <= 10; k++ {
		v1 := FromInt(k)
		v2 := FromInt(k + 1)
		if ok, err := zZ.AreEqual(v1, v2); assert.NoError(err) {
			assert.False(ok)
		}
		assert.False(AreEqual(v1, v2))
		assert.False(v1.IsEqualTo(v2))
		assert.False(v1.IsEqualToInt64(int64(k + 1)))
	}
	// Testing equality of non-Integers should give an error
	_, err := zZ.AreEqual(dummyElement(0), dummyElement(1))
	assert.Error(err)
	// The following should be false -- the point is that x is larger than an
	// int64, which is relevant for the code.
	if x, err := FromInt(2).PowerInt64(65); assert.NoError(err) {
		assert.False(x.IsEqualToInt64(17))
	}

}

////////////////////////////////////////////////////////////////////////////////
// tests for Cmp
////////////////////////////////////////////////////////////////////////////////

func TestCmp(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// test the table of values
	cmpTests := []struct {
		input1   int64
		input2   int64
		expected int
	}{
		{-587500394814627965, 2879987967086689218, -1},
		{-659900268985222827, 2988693584445282452, -1},
		{3798362369790351182, 544907789857774311, 1},
		{2533620412774656844, 9966307623987333, 1},
		{83762876, 83762876, 0},
	}
	for _, test := range cmpTests {
		v1 := FromInt64(test.input1)
		v2 := FromInt64(test.input2)
		if sgn, err := zZ.Cmp(v1, v2); assert.NoError(err) {
			assert.Equal(sgn, test.expected)
		}
		assert.Equal(Cmp(v1, v2), test.expected)
	}
	// test the special cases in integer.Cmp()
	cmpRingTests := []struct {
		input1   *Element
		input2   *Element
		expected int
	}{
		{One(), One(), 0},
		{nil, Zero(), 0},
		{Zero(), nil, 0},
		{nil, One(), -1},
		{One(), nil, 1},
	}
	for _, test := range cmpRingTests {
		assert.Equal(test.expected, Cmp(test.input1, test.input2), "Input: %s, %s", test.input1, test.input2)
	}
	// calling Cmp on things that aren't in the Integers should result in an error
	_, err := zZ.Cmp(dummyElement(0), Zero())
	assert.Error(err)
	_, err = zZ.Cmp(Zero(), dummyElement(0))
	assert.Error(err)
	_, err = zZ.Cmp(dummyElement(0), dummyElement(0))
	assert.Error(err)
}
