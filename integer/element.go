// Element represents an integer.

/*
Notes for the programmer:
-------------------------
The underlying value describing an *Element will be an int64 when possible; otherwise it will be a *big.Int. You should attempt to gain the performance boost of using int64 versions of your computations when possible. Typical code will branch into two cases:
	if x.IsInt64() {
		n := x.int64()	// Expose the underlying int64
		...
	} else {
		n := x.bigInt()	// Expose the underlying *big.Int
		...
	}
Take care not to modify the *big.Int returned by x.bigInt(), as this will modify the corresponding *Element (thus breaking our contract of immutability). Similarly, do not allow this *big.Int to escape into user-land.

The implementation attempts to guarantee that there is a unique 0 and 1 *Element element, returned via the functions Zero() and One(). However this isn't completely true: following good Go standard, a nil *Element behaves identically to 0. Furthermore, there's nothing stoping a user from writing:
	x := &integers.Element{}
and thus creating a new, distinct 0. Care must be taken when adding new package functions: you should use the methods x.IsZero() and x.IsOne(). It's good practice to try to reuse Zero() and One(), rather than allowing additional copies of 0 or 1 to leak out of the package. The easiest way to ensure this is to use FromInt64(n) and fromBigIntAndReuse(n) to convert your working value into a *Element suitable for returning to the user.

Performance-wise, the biggest issue is big.Int.Mul(x,y), when x and y are large. Here Magma vastly outperforms big.Int. A quick search on the web shows that there exist several different algorithms for computing x * y, with different performance characteristics that depend on the size of x and y. Implementing these different algorithms, and modifying big.Int so that it uses the appropriate algorithm in each case, is a clear future task.
*/
package integer

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"math"
	"math/big"
)

// Element is an element of the ring of integers.
type Element struct {
	k int64   // The underlying integer as an int64 (if n below is zero)
	n big.Int // The underlying integer as a big.Int (if non-zero)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialised our global variables.
func init() {
	// Create the cache of small-valued integers
	initSmallIntegerCache()
	// Initialise the primes data
	initMaxExactValueForPrime()
	// Create the look-up tables of factorials and binomials
	initFactorialTable()
	initBinomialTable()
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add returns x + y.
func Add(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() {
		return y
	} else if y.IsZero() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Int
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.AddInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigintfactory.New().SetInt64(x.int64())
		return fromBigIntAndReuse(xx.Add(xx, y.bigInt()))
	} else if y.IsInt64() {
		yy := bigintfactory.New().SetInt64(y.int64())
		return fromBigIntAndReuse(yy.Add(x.bigInt(), yy))
	}
	return fromBigIntAndReuse(bigintfactory.New().Add(x.bigInt(), y.bigInt()))
}

// AddInt64 returns x + y.
func AddInt64(x int64, y *Element) *Element {
	// Fast-track the easy cases
	if x == 0 {
		return y
	} else if y.IsZero() {
		return FromInt64(x)
	}
	// Can we do this as an int64 computation?
	if y.IsInt64() {
		if val, ok := mathutil.AddInt64(x, y.int64()); ok {
			return FromInt64(val)
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().SetInt64(x)
	return fromBigIntAndReuse(n.Add(n, y.bigInt()))
}

// Subtract returns x - y.
func Subtract(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() {
		return Negate(y)
	} else if y.IsZero() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Int
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.SubtractInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigintfactory.New().SetInt64(x.int64())
		return fromBigIntAndReuse(xx.Sub(xx, y.bigInt()))
	} else if y.IsInt64() {
		yy := bigintfactory.New().SetInt64(y.int64())
		return fromBigIntAndReuse(yy.Sub(x.bigInt(), yy))
	}
	return fromBigIntAndReuse(bigintfactory.New().Sub(x.bigInt(), y.bigInt()))
}

// Multiply returns the product x * y.
func Multiply(x *Element, y *Element) *Element {
	// Fast-track the easy cases
	if x.IsZero() || y.IsZero() {
		return Zero()
	} else if x.IsOne() {
		return y
	} else if y.IsOne() {
		return x
	}
	// Can we do this as an int64 computation? Otherwise fall back to a big.Int
	// computation.
	if x.IsInt64() {
		if y.IsInt64() {
			if val, ok := mathutil.MultiplyInt64(x.int64(), y.int64()); ok {
				return FromInt64(val)
			}
		}
		xx := bigintfactory.New().SetInt64(x.int64())
		return fromBigIntAndReuse(xx.Mul(xx, y.bigInt()))
	} else if y.IsInt64() {
		yy := bigintfactory.New().SetInt64(y.int64())
		return fromBigIntAndReuse(yy.Mul(x.bigInt(), yy))
	}
	return fromBigIntAndReuse(bigintfactory.New().Mul(x.bigInt(), y.bigInt()))
}

// MultiplyByInt64ThenAdd returns the value a * b + c.
func MultiplyByInt64ThenAdd(a int64, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a == 0 || b.IsZero() {
		return c
	} else if c.IsZero() {
		return b.ScalarMultiplyByInt64(a)
	} else if a == 1 {
		return Add(b, c)
	} else if b.IsOne() {
		return AddInt64(a, c)
	}
	// Can we do this as an int64 computation?
	if b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a, b.int64()); ok {
			if val, ok = mathutil.AddInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().SetInt64(a)
	return fromBigIntAndReuse(n.Add(n.Mul(n, b.bigInt()), c.bigInt()))
}

// MultiplyThenAdd returns the value a * b + c.
func MultiplyThenAdd(a *Element, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return c
	} else if c.IsZero() {
		return Multiply(a, b)
	} else if a.IsOne() {
		return Add(b, c)
	} else if b.IsOne() {
		return Add(a, c)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val, ok = mathutil.AddInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().Mul(a.bigInt(), b.bigInt())
	return fromBigIntAndReuse(n.Add(n, c.bigInt()))
}

// MultiplyThenSubtract returns the value a * b - c.
func MultiplyThenSubtract(a *Element, b *Element, c *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return Negate(c)
	} else if c.IsZero() {
		return Multiply(a, b)
	} else if a.IsOne() {
		return Subtract(b, c)
	} else if b.IsOne() {
		return Subtract(a, c)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val, ok = mathutil.SubtractInt64(val, c.int64()); ok {
				return FromInt64(val)
			}
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().Mul(a.bigInt(), b.bigInt())
	return fromBigIntAndReuse(n.Sub(n, c.bigInt()))
}

// MultiplyMultiplyThenAdd returns the value a * b + c * d.
func MultiplyMultiplyThenAdd(a *Element, b *Element, c *Element, d *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		return Multiply(c, d)
	} else if c.IsZero() || d.IsZero() {
		return Multiply(a, b)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() && d.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val2, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok {
				if val, ok = mathutil.AddInt64(val, val2); ok {
					return FromInt64(val)
				}
			}
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().Mul(a.bigInt(), b.bigInt())
	m := bigintfactory.New().Mul(c.bigInt(), d.bigInt())
	z := fromBigIntAndReuse(n.Add(n, m))
	bigintfactory.Reuse(m)
	return z
}

// MultiplyMultiplyThenSubtract returns the value a * b - c * d.
func MultiplyMultiplyThenSubtract(a *Element, b *Element, c *Element, d *Element) *Element {
	// Fast-track the easy cases
	if a.IsZero() || b.IsZero() {
		if c.IsZero() || d.IsZero() {
			return Zero()
		}
		// Can we do this as an int64 computation?
		if c.IsInt64() && d.IsInt64() {
			if val, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok && val != math.MinInt64 {
				return FromInt64(-val)
			}
		}
		// No luck -- do this as a big.Int computation
		n := bigintfactory.New().Mul(c.bigInt(), d.bigInt())
		return fromBigIntAndReuse(n.Neg(n))
	} else if c.IsZero() || d.IsZero() {
		return Multiply(a, b)
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() && c.IsInt64() && d.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a.int64(), b.int64()); ok {
			if val2, ok := mathutil.MultiplyInt64(c.int64(), d.int64()); ok {
				if val, ok = mathutil.SubtractInt64(val, val2); ok {
					return FromInt64(val)
				}
			}
		}
	}
	// No luck -- do this as a big.Int computation
	n := bigintfactory.New().Mul(a.bigInt(), b.bigInt())
	m := bigintfactory.New().Mul(c.bigInt(), d.bigInt())
	z := fromBigIntAndReuse(n.Sub(n, m))
	bigintfactory.Reuse(m)
	return z
}

// Negate returns -n.
func Negate(n *Element) *Element {
	if n.IsInt64() {
		if k := n.int64(); k != math.MinInt64 {
			return FromInt64(-k)
		}
	}
	return fromBigIntAndReuse(bigintfactory.New().Neg(n.bigInt()))
}

// PowerInt64 returns n^k.
func PowerInt64(n *Element, k int64) (*Element, error) {
	// Fast-track the easy cases
	if k == 0 {
		return One(), nil
	} else if k == 1 || n.IsOne() {
		return n, nil
	} else if n.IsEqualToInt64(-1) {
		if (k % 2) == 0 {
			return One(), nil
		}
		return n, nil
	} else if k < 0 {
		return nil, errors.IllegalNegativeExponent.New()
	} else if n.IsZero() {
		return Zero(), nil
	}
	// Can we do this as an int64 calculation?
	if n.IsInt64() {
		nn := n.int64()
		if c, ok := mathutil.PowerInt64(nn, k); ok {
			return FromInt64(c), nil
		}
		// We can do powers of two via bit shifting
		if nn == 2 && int64(uint(k)) == k {
			m := bigintfactory.New().SetInt64(1)
			return fromBigIntAndReuse(m.Lsh(m, uint(k))), nil
		}
	}
	// No luck -- do this as a big.Int computation
	m := bigintfactory.New().SetInt64(k)
	return fromBigIntAndReuse(m.Exp(n.bigInt(), m, nil)), nil
}

// Power returns n^k.
func Power(n *Element, k *Element) (*Element, error) {
	// Fast-track the easy cases
	if k.IsInt64() {
		return PowerInt64(n, k.int64())
	} else if n.IsOne() {
		return n, nil
	} else if n.IsEqualToInt64(-1) {
		if k.IsEven() {
			return One(), nil
		}
		return n, nil
	} else if k.IsNegative() {
		return nil, errors.IllegalNegativeExponent.New()
	} else if n.IsZero() {
		return Zero(), nil
	}
	// Perform the calculation
	return fromBigIntAndReuse(bigintfactory.New().Exp(n.bigInt(), k.bigInt(), nil)), nil
}

// AreEqual returns true iff x equals y.
func AreEqual(x *Element, y *Element) bool {
	return x.IsEqualTo(y)
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func Cmp(x *Element, y *Element) int {
	// Fast-track the easy cases
	if x == y {
		return 0
	} else if x.IsZero() {
		return -y.Sign()
	} else if y.IsZero() {
		return x.Sign()
	}
	// Can we do this as an int64 computation?
	if x.IsInt64() {
		if y.IsInt64() {
			if xx, yy := x.int64(), y.int64(); xx < yy {
				return -1
			} else if xx == yy {
				return 0
			}
		} else if y.bigInt().Cmp(maxInt64) > 0 {
			return -1
		}
		return 1
	} else if y.IsInt64() {
		if x.bigInt().Cmp(minInt64) < 0 {
			return -1
		}
		return 1
	}
	// No luck -- do this as a big.Int computation
	return x.bigInt().Cmp(y.bigInt())
}

// Quotient returns the quotient of x on division by y. This will return an error if y is zero.
func Quotient(x *Element, y *Element) (*Element, error) {
	// Fast-track the easy cases
	if y.IsZero() {
		return nil, errors.DivisionByZero.New()
	} else if y.IsOne() {
		return x, nil
	} else if x.IsZero() {
		return Zero(), nil
	}
	// Can we do this as an int64 computation?
	if x.IsInt64() && y.IsInt64() {
		q, _ := mathutil.QuotientAndRemainderInt64(x.int64(), y.int64())
		return FromInt64(q), nil
	}
	// No luck -- do this as a big.Int computation
	return fromBigIntAndReuse(bigintfactory.New().Div(x.bigInt(), y.bigInt())), nil
}

// QuotientAndRemainder returns the quotient and remainder of x on division by y. This will return an error if y is zero.
func QuotientAndRemainder(x *Element, y *Element) (*Element, *Element, error) {
	// Fast-track the easy cases
	if y.IsZero() {
		return nil, nil, errors.DivisionByZero.New()
	} else if y.IsOne() {
		return x, Zero(), nil
	} else if x.IsZero() {
		return Zero(), Zero(), nil
	}
	// Can we do this as an int64 computation?
	if x.IsInt64() && y.IsInt64() {
		q, r := mathutil.QuotientAndRemainderInt64(x.int64(), y.int64())
		return FromInt64(q), FromInt64(r), nil
	}
	// No luck -- do this as a big.Int computation
	q, r := bigintfactory.New(), bigintfactory.New()
	q.DivMod(x.bigInt(), y.bigInt(), r)
	return fromBigIntAndReuse(q), fromBigIntAndReuse(r), nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff n is 0.
func (n *Element) IsZero() bool {
	return n.IsInt64() && n.int64() == 0
}

// IsOne returns true iff n is 1.
func (n *Element) IsOne() bool {
	return n.IsInt64() && n.int64() == 1
}

// IsEven returns true iff n is even.
func (n *Element) IsEven() bool {
	if n.IsInt64() {
		return (n.int64() % 2) == 0
	}
	return n.bigInt().Bit(0) == 0
}

// IsOdd returns true iff n is odd.
func (n *Element) IsOdd() bool {
	if n.IsInt64() {
		return (n.int64() % 2) != 0
	}
	return n.bigInt().Bit(0) != 0
}

// IsDivisibleBy returns true iff n is divisible by m and m is non-zero. If m is zero then an error is returned.
func (n *Element) IsDivisibleBy(m *Element) (bool, error) {
	// Fast-track the easy cases
	if m.IsZero() {
		return false, errors.IllegalZeroArg.New()
	} else if n.IsZero() || m.IsOne() {
		return true, nil
	}
	// Can we do this as an int64 computation?
	if n.IsInt64() && m.IsInt64() {
		return (n.int64() % m.int64()) == 0, nil
	}
	// No luck -- do this as a big.Int computation
	z := bigintfactory.New().Mod(n.bigInt(), m.bigInt())
	ok := z.BitLen() == 0
	bigintfactory.Reuse(z)
	return ok, nil
}

// IsNegative returns true iff n < 0.
func (n *Element) IsNegative() bool {
	if n.IsInt64() {
		return n.int64() < 0
	}
	return n.bigInt().Sign() < 0
}

// IsPositive returns true iff n > 0.
func (n *Element) IsPositive() bool {
	if n.IsInt64() {
		return n.int64() > 0
	}
	return n.bigInt().Sign() > 0
}

// IsEqualTo returns true iff n = m.
func (n *Element) IsEqualTo(m *Element) bool {
	if n == m {
		return true
	} else if n.IsInt64() {
		return m.IsInt64() && n.int64() == m.int64()
	} else if m.IsInt64() {
		return false
	}
	return n.bigInt().Cmp(m.bigInt()) == 0
}

// IsEqualToInt returns true iff n = m.
func (n *Element) IsEqualToInt(m int) bool {
	return n.IsEqualToInt64(int64(m))
}

// IsEqualToInt64 returns true iff n = m.
func (n *Element) IsEqualToInt64(m int64) bool {
	return n.IsInt64() && n.int64() == m
}

// IsEqualToUint64 returns true iff n = m.
func (n *Element) IsEqualToUint64(m uint64) bool {
	if m <= math.MaxInt64 {
		return n.IsInt64() && n.int64() == int64(m)
	} else if n.IsInt64() {
		return false
	} else if nn := n.bigInt(); nn.IsUint64() {
		return nn.Uint64() == m
	}
	return false
}

// IsGreaterThan returns true iff n > m.
func (n *Element) IsGreaterThan(m *Element) bool {
	if n == m {
		return false
	} else if n.IsZero() {
		return m.IsNegative()
	} else if m.IsZero() {
		return n.IsPositive()
	} else if n.IsInt64() {
		if m.IsInt64() {
			return n.int64() > m.int64()
		}
		return m.bigInt().Cmp(minInt64) < 0
	} else if m.IsInt64() {
		return n.bigInt().Cmp(maxInt64) > 0
	}
	return n.bigInt().Cmp(m.bigInt()) > 0
}

// IsGreaterThanInt64 returns true iff n > m.
func (n *Element) IsGreaterThanInt64(m int64) bool {
	if n.IsInt64() {
		return n.int64() > m
	}
	return n.bigInt().Cmp(maxInt64) > 0
}

// IsGreaterThanOrEqualTo returns true iff n >= m.
func (n *Element) IsGreaterThanOrEqualTo(m *Element) bool {
	return !n.IsLessThan(m)
}

// IsGreaterThanOrEqualToInt64 returns true iff n >= m.
func (n *Element) IsGreaterThanOrEqualToInt64(m int64) bool {
	return !n.IsLessThanInt64(m)
}

// IsLessThan returns true iff n < m.
func (n *Element) IsLessThan(m *Element) bool {
	if n == m {
		return false
	} else if n.IsZero() {
		return m.IsPositive()
	} else if m.IsZero() {
		return n.IsNegative()
	} else if n.IsInt64() {
		if m.IsInt64() {
			return n.int64() < m.int64()
		}
		return m.bigInt().Cmp(maxInt64) > 0
	} else if m.IsInt64() {
		return n.bigInt().Cmp(minInt64) < 0
	}
	return n.bigInt().Cmp(m.bigInt()) < 0
}

// IsLessThanInt64 returns true iff n < m.
func (n *Element) IsLessThanInt64(m int64) bool {
	if n.IsInt64() {
		return n.int64() < m
	}
	return n.bigInt().Cmp(minInt64) < 0
}

// IsLessThanOrEqualTo returns true iff n <= m.
func (n *Element) IsLessThanOrEqualTo(m *Element) bool {
	return !n.IsGreaterThan(m)
}

// IsLessThanOrEqualToInt64 returns true iff n <= m.
func (n *Element) IsLessThanOrEqualToInt64(m int64) bool {
	return !n.IsGreaterThanInt64(m)
}

// Sign returns the sign of n, i.e. -1 if n < 0; 0 if n == 0; +1 if n > 0.
func (n *Element) Sign() int {
	if n.IsInt64() {
		if k := n.int64(); k < 0 {
			return -1
		} else if k > 0 {
			return 1
		}
		return 0
	}
	return n.bigInt().Sign()
}

// Abs returns the absolute value of n.
func (n *Element) Abs() *Element {
	if n.IsNegative() {
		n = Negate(n)
	}
	return n
}

// Mod returns n modulo m if m is non-zero, and an error otherwise. The returned modulus k is in the range 0 <= k < m if m is positive, and m < k <= 0 if m is negative.
func (n *Element) Mod(m *Element) (*Element, error) {
	// Fast-track the easy cases
	if m.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	} else if n.IsZero() || m.IsOne() {
		return Zero(), nil
	}
	// Can we do this as an int64 computation?
	if n.IsInt64() && m.IsInt64() {
		_, r := mathutil.QuotientAndRemainderInt64(n.int64(), m.int64())
		return FromInt64(r), nil
	}
	// No luck -- do this as a big.Int computation
	r := bigintfactory.New().Mod(n.bigInt(), m.bigInt())
	if m.IsNegative() && r.BitLen() != 0 {
		r.Add(r, m.bigInt())
	}
	return fromBigIntAndReuse(r), nil
}

// InverseMod returns an inverse of n modulo m if n and m are coprime, and an error otherwise. The returned inverse modulo m is in the range 0 < n^{-1} < m if m is positive, and m < n^{-1} < 0 if m is negative.
func (n *Element) InverseMod(m *Element) (*Element, error) {
	// Sanity check
	if m.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	} else if n.IsZero() || m.IsOne() {
		return nil, errors.ArgsNotCoprime.New()
	}
	// Can we do this as an int64 computation?
	if n.IsInt64() && m.IsInt64() {
		if nn, mm := n.int64(), m.int64(); nn != math.MinInt64 || mm != math.MinInt64 {
			// Compute the gcd and check co-primeness
			gcd, x, _ := mathutil.XGCDOfPairInt64(nn, mm)
			if gcd == 1 {
				// Shift the inverse into the correct range and return
				if (mm < 0 && x > 0) || (mm > 0 && x < 0) {
					x += mm
				}
				return FromInt64(x), nil
			}
		}
		return nil, errors.ArgsNotCoprime.New()
	}
	// No luck -- do this as a big.Int computation. Calculate the gcd -- we
	// need to handle the cases when m < 0 and m > 0 separately, and also the
	// cases when n < 0 and n > 0.
	gcd, x := bigintfactory.New(), bigintfactory.New()
	if n.IsPositive() {
		if m.IsPositive() {
			gcd.GCD(x, nil, n.bigInt(), m.bigInt())
		} else {
			t := bigintfactory.New().Abs(m.bigInt())
			gcd.GCD(x, nil, n.bigInt(), t)
			bigintfactory.Reuse(t)
		}
	} else {
		if m.IsPositive() {
			t := bigintfactory.New().Mod(n.bigInt(), m.bigInt())
			gcd.GCD(x, nil, t, m.bigInt())
			bigintfactory.Reuse(t)
		} else {
			t1 := bigintfactory.New().Mod(n.bigInt(), m.bigInt())
			t2 := bigintfactory.New().Abs(m.bigInt())
			gcd.GCD(x, nil, t1, t2)
			bigintfactory.Reuse(t1)
			bigintfactory.Reuse(t2)
		}
	}
	// Check co-primeness
	isCoprime := gcd.Cmp(oneAsBigInt) == 0
	bigintfactory.Reuse(gcd)
	if !isCoprime {
		bigintfactory.Reuse(x)
		return nil, errors.ArgsNotCoprime.New()
	}
	// Shift the inverse into the correct range
	if x.Sign() != m.Sign() {
		x.Add(x, m.bigInt())
	}
	// Return the inverse
	return fromBigIntAndReuse(x), nil
}

// ModInt64 returns n modulo m if m is non-zero, and an error otherwise. The returned modulus k is in the range 0 <= k < m if m is positive, and m < k <= 0 if m is negative.
func (n *Element) ModInt64(m int64) (int64, error) {
	// Fast-track the easy cases
	if m == 0 {
		return 0, errors.IllegalZeroArg.New()
	} else if n.IsZero() || m == 1 {
		return 0, nil
	}
	// Can we do this as an int64 computation?
	if n.IsInt64() {
		_, r := mathutil.QuotientAndRemainderInt64(n.int64(), m)
		return r, nil
	}
	// No luck -- do this as a big.Int computation
	r := bigintfactory.New().Mod(n.bigInt(), FromInt64(m).bigInt()).Int64()
	if m < 0 && r != 0 {
		r += m
	}
	return r, nil
}

// Negate return -n.
func (n *Element) Negate() *Element {
	return Negate(n)
}

// PowerInt64 returns n^k.
func (n *Element) PowerInt64(k int64) (*Element, error) {
	return PowerInt64(n, k)
}

// Power returns n^k.
func (n *Element) Power(k *Element) (*Element, error) {
	return Power(n, k)
}

// Increment returns n + 1.
func (n *Element) Increment() *Element {
	// Can we do this as an int64 computation?
	if n.IsInt64() {
		if k := n.int64(); k != math.MaxInt64 {
			return FromInt64(k + 1)
		}
	}
	// No luck -- do this as a big.Int computation
	m := bigintfactory.New().SetInt64(1)
	return fromBigIntAndReuse(m.Add(m, n.bigInt()))
}

// Decrement returns n - 1.
func (n *Element) Decrement() *Element {
	// Can we do this as an int64 computation?
	if n.IsInt64() {
		if k := n.int64(); k != math.MinInt64 {
			return FromInt64(k - 1)
		}
	}
	// No luck -- do this as a big.Int computation
	m := bigintfactory.New().SetInt64(-1)
	return fromBigIntAndReuse(m.Add(m, n.bigInt()))
}

// ScalarMultiplyByInt64 returns the product a * n.
func (n *Element) ScalarMultiplyByInt64(a int64) *Element {
	// Fast-track the easy cases
	if a == 0 || n.IsZero() {
		return Zero()
	} else if n.IsOne() {
		return FromInt64(a)
	} else if a == 1 {
		return n
	}
	// Can we do this as an int64 computation?
	if n.IsInt64() {
		if val, ok := mathutil.MultiplyInt64(a, n.int64()); ok {
			return FromInt64(val)
		}
	}
	// No luck -- do this as a big.Int computation
	m := bigintfactory.New().SetInt64(a)
	return fromBigIntAndReuse(m.Mul(m, n.bigInt()))
}

// ScalarMultiplyByUint64 returns the product a * n.
func (n *Element) ScalarMultiplyByUint64(a uint64) *Element {
	// Fast-track the easy cases
	if a <= math.MaxInt64 {
		return n.ScalarMultiplyByInt64(int64(a))
	} else if n.IsZero() {
		return Zero()
	} else if n.IsOne() {
		return FromUint64(a)
	}
	// Perform the calculation
	m := bigintfactory.New().SetUint64(a)
	return fromBigIntAndReuse(m.Mul(m, n.bigInt()))
}

// ScalarMultiplyByInteger returns the product a * n.
func (n *Element) ScalarMultiplyByInteger(a *Element) *Element {
	return Multiply(a, n)
}

// Parent returns the ring of integers as a object.Parent. This can safely be coerced to type Parent.
func (n *Element) Parent() object.Parent {
	return zZ
}

// Hash returns a hash value for the given integer.
func (n *Element) Hash() hash.Value {
	if n.IsInt64() {
		return hash.Int64(n.int64())
	}
	return hash.BigInt(n.bigInt())
}
