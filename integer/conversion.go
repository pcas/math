// Conversion contains functions for converting an integer to a different type.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"math"
	"math/big"
	"strconv"
)

// Various math constants as big.Ints.
var (
	oneAsBigInt = big.NewInt(1)
	minInt64    = big.NewInt(math.MinInt64)
	maxInt64    = big.NewInt(math.MaxInt64)
	maxUint64   = big.NewInt(0).SetUint64(math.MaxUint64)
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// SetBigInt sets the given *big.Int n to the value of the given integer m. Returns the *big.Int n.
func SetBigInt(n *big.Int, m *Element) *big.Int {
	if m.IsInt64() {
		return n.SetInt64(m.int64())
	}
	return n.Set(m.bigInt())
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsInt64 returns true iff the integer can be expressed as an int64.
func (n *Element) IsInt64() bool {
	return n == nil || n.n.BitLen() == 0
}

// int64 returns n as an int64. Warning: If n does not fit in an int64 then the returned value is undefined.
func (n *Element) int64() int64 {
	if n == nil {
		return 0
	}
	return n.k
}

// Int64 returns the integer as an int64.
func (n *Element) Int64() (int64, error) {
	if n.IsInt64() {
		return n.int64(), nil
	}
	return 0, errors.OutOfRange.New()
}

// IsUint64 returns true iff the integer can be expressed as a uint64.
func (n *Element) IsUint64() bool {
	if n.IsInt64() {
		return n.int64() >= 0
	}
	return n.bigInt().IsUint64()
}

// Uint64 returns the integer as a uint64.
func (n *Element) Uint64() (uint64, error) {
	if n.IsInt64() {
		if k := n.int64(); k >= 0 {
			return uint64(k), nil
		}
	} else if m := n.bigInt(); m.IsUint64() {
		return m.Uint64(), nil
	}
	return 0, errors.OutOfRange.New()
}

// bigInt exposes the underlying *big.Int defining n. Warning: The returned value must NOT be modified, and must NOT be exposed to the user.
func (n *Element) bigInt() *big.Int {
	if n.IsInt64() {
		return bigintfactory.New().SetInt64(n.int64())
	}
	return &n.n
}

// BigInt returns the integer as a *big.Int.
func (n *Element) BigInt() *big.Int {
	m := bigintfactory.New()
	if n.IsInt64() {
		return m.SetInt64(n.int64())
	}
	return m.Set(n.bigInt())
}

// ToInteger returns n.
func (n *Element) ToInteger() (*Element, error) {
	return n, nil
}

// String returns a string representation of the given integer.
func (n *Element) String() string {
	if n.IsInt64() {
		return strconv.FormatInt(n.int64(), 10)
	}
	return n.n.String()
}
