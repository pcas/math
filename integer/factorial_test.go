// Tests for factorial.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"math/big"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestFactorial(t *testing.T) {
	assert := assert.New(t)
	// The factorial table means that for all n <= factorialTableMaxN, the
	// result of n! comes from a table and we should always get the same
	// integer object back.
	expect := One()
	for n := int64(0); n <= factorialTableMaxN; n++ {
		// Calculate n! inductively by hand
		if n != 0 {
			expect = expect.ScalarMultiplyByInt64(n)
		}
		// Calculate n! via FactorialInt64
		if res, err := FactorialInt64(n); assert.NoError(err) {
			// Check that the value is correct
			assert.True(expect.IsEqualTo(res))
			// Calculate n! via Factorial
			if res2, err := Factorial(FromInt64(n)); assert.NoError(err) {
				// Check that this is the same object
				assert.True(res2 == res)
			}
		}
	}
	// We explore a little outside of the table range
	for n := int64(factorialTableMaxN + 1); n <= factorialTableMaxN+5; n++ {
		expect = expect.ScalarMultiplyByInt64(n)
		if res, err := FactorialInt64(n); assert.NoError(err) {
			assert.True(expect.IsEqualTo(res))
		}
		if res, err := Factorial(FromInt64(n)); assert.NoError(err) {
			assert.True(expect.IsEqualTo(res))
		}
	}
	// Factorial of a negative should return an error
	_, err := FactorialInt64(-1)
	assert.Error(err)
	_, err = Factorial(FromInt(-1))
	assert.Error(err)
	// Test factorialAsBigInt directly
	if val, err := FactorialInt64(factorialTableMaxN); assert.NoError(err) {
		val = Product(val, FromInt(factorialTableMaxN+1), FromInt(factorialTableMaxN+2))
		res := fromBigIntAndReuse(factorialAsBigInt(big.NewInt(factorialTableMaxN + 2)))
		assert.True(res.IsEqualTo(val))
	}
	// factorialAsBigInt or 0 and 1 should both return 1
	assert.Equal(factorialAsBigInt(Zero().BigInt()).Cmp(One().BigInt()), 0)
	assert.Equal(factorialAsBigInt(One().BigInt()).Cmp(One().BigInt()), 0)
}

func TestBinomial(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// The binomial table means that for all n <= binomialTableMaxN, the result
	// of n \choose k comes from a table, so we need to explore a little
	// beyond this.
	pow2 := One()
	for n := int64(0); n <= binomialTableMaxN+5; n++ {
		// Calculate n!
		nfactorial, err := FactorialInt64(n)
		require.NoError(err)
		// Start working through the k, keeping a running total
		total := Zero()
		for k := int64(0); k <= n; k++ {
			// Calculate n \choose k by hand
			kfactorial, err := FactorialInt64(k)
			require.NoError(err)
			nkfactorial, err := FactorialInt64(n - k)
			require.NoError(err)
			nCk, err := Quotient(nfactorial, Multiply(kfactorial, nkfactorial))
			require.NoError(err)
			// Calculate n \choose k using the function
			assert.True(nCk.IsEqualTo(BinomialInt64(n, k)))
			bin := Binomial(FromInt64(n), FromInt64(k))
			assert.True(nCk.IsEqualTo(bin), "%d C %d != %s", n, k, bin.String())
			// Update the total
			total = Add(total, nCk)
		}
		// The total should equal 2^n
		assert.True(pow2.IsEqualTo(total))
		// Move on
		pow2 = pow2.ScalarMultiplyByInt64(2)
	}
	// We do a large binomial computation with arguments outside of an int64:
	//   (2^64 + 1) \choose 2 = 170141183460469231740910675752738881536
	two := FromInt(2)
	if n, err := two.PowerInt64(64); assert.NoError(err) {
		n = n.Increment()
		assert.True(Binomial(n, Zero()).IsOne())
		assert.True(Binomial(n, n).IsOne())
		assert.True(n.IsEqualTo(Binomial(n, One())))
		assert.True(n.IsEqualTo(Binomial(n, n.Decrement())))
		res := Binomial(n, two)
		assert.Equal(res.String(), "170141183460469231740910675752738881536")
		res = Binomial(n, Subtract(n, two))
		assert.Equal(res.String(), "170141183460469231740910675752738881536")
	}
	// Out-of-range values should return zero by convention
	assert.True(BinomialInt64(2, 3).IsZero())
	assert.True(BinomialInt64(-1, -1).IsZero())
	assert.True(Binomial(FromInt(7), FromInt(9)).IsZero())
	assert.True(Binomial(FromInt(-2), FromInt(-1)).IsZero())
}
