// Tests for sumproduct.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestSum1ToN(t *testing.T) {
	assert := assert.New(t)
	// Test for large n
	for n := 1; n <= 5; n++ {
		// Create a long slice to sum
		S := make([]*Element, 0, (maxSumProd/2)*n)
		for i := 1; i <= cap(S); i++ {
			S = append(S, FromInt(i))
		}
		// Calculate the sum
		N := int64(len(S))
		assert.True(Sum(S...).IsEqualToInt64((N * (N + 1)) / 2))
	}
	// Test the edge cases for small n
	assert.True(Sum().IsZero())
	assert.True(Sum(One()).IsOne())
	assert.True(Sum(Zero(), One()).IsOne())
}

func TestProduct1ToN(t *testing.T) {
	assert := assert.New(t)
	// Test for large n
	for n := 1; n <= 5; n++ {
		// Create a long slice to multiply
		S := make([]*Element, 0, (maxSumProd/2)*n)
		for i := 1; i <= cap(S); i++ {
			S = append(S, FromInt(i))
		}
		// Calculate the product
		if expected, err := FactorialInt64(int64(len(S))); assert.NoError(err) {
			assert.True(Product(S...).IsEqualTo(expected))
		}
	}
	// Test the edge cases for small n
	assert.True(Product().IsOne())
	assert.True(Product(One()).IsOne())
	assert.True(Product(Zero(), One()).IsZero())
}

func TestProductOfRange(t *testing.T) {
	assert := assert.New(t)
	// Test for large ranges
	K := int(maxProdRange.Int64() / 2)
	for n := 1; n <= 5; n++ {
		// Create a slice of range to multiply
		S := make([]*Element, 0, K*n)
		for i := 0; i < cap(S); i++ {
			S = append(S, FromInt(i+2*n))
		}
		// Calculate the product the slow way
		expected := Product(S...)
		// Calculate the product of the range
		assert.True(ProductOfRange(S[0], S[len(S)-1]).IsEqualTo(expected))
		assert.True(ProductOfRangeInt64(int64(2*n), int64((2+K)*n-1)).IsEqualTo(expected))
	}
	// Test the edge cases for small ranges
	assert.True(ProductOfRangeInt64(5, 3).IsOne())
	assert.True(ProductOfRangeInt64(5, 5).IsEqualToInt64(5))
	assert.True(ProductOfRangeInt64(-3, 5).IsZero())
	five := FromInt(5)
	three := FromInt(3)
	assert.True(ProductOfRange(five, three).IsOne())
	assert.True(ProductOfRange(five, five).IsEqualTo(five))
	assert.True(ProductOfRange(three.Negate(), five).IsZero())
}

func TestDotProduct(t *testing.T) {
	assert := assert.New(t)
	// Test some long slices
	for n := 1; n <= 5; n++ {
		// Create a sequence to take the dot-product of
		S := make([]*Element, 0, (maxDotProd/2)*n)
		for i := 1; i <= cap(S); i++ {
			S = append(S, FromInt(i))
		}
		// Calculate the dot product of S with itself
		N := int64(len(S))
		assert.True(DotProduct(S, S).IsEqualToInt64((N * (N + 1) * (2*N + 1)) / 6))
	}
	// Test the short slices
	S := make([]*Element, 0, 2)
	assert.True(DotProduct(S, S).IsZero())
	S = append(S, FromInt(3))
	assert.True(DotProduct(S, S).IsEqualToInt64(9))
	S = append(S, One())
	assert.True(DotProduct(S, S).IsEqualToInt64(10))
	// Test the dot-product of unequal length slices
	T := make([]*Element, 0, 1)
	T = append(T, FromInt(5))
	assert.True(DotProduct(S, T).IsEqualToInt64(15))
	assert.True(DotProduct(T, S).IsEqualToInt64(15))
}
