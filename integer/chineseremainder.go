// Chineseremainder contains functions that implement the Chinese Remainder Theorem.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"math/big"
	"sync"
)

// crtResult is used to record partial results in the Chinese Remainder Theorem.
type crtResult struct {
	res big.Int // The result
	err error   // An error (if any)
}

// CRTFunc applies the Chinese Remainder Theorem to the given residues. If the number of residues does not match the number of moduli then an error is returned.
type CRTFunc func(residues ...*Element) (*Element, error)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// crtInverseMod computes a * prod * invProd, where prod is the product of the moduli Ms, excluding the i-th moduli, and invProd is the inverse of the product modulo the i-th moduli. The result is recorded in r.res. If the i-th moduli is not coprime with prod then r.err will be set to an error.
func crtInverseMod(i int, a *big.Int, Ms []*big.Int, r *crtResult, wg *sync.WaitGroup) {
	// Calculate the product of the residues Ms, excluding the i-th moduli
	prod := bigintfactory.New().SetInt64(1)
	for j, m := range Ms {
		if j != i {
			prod.Mul(prod, m)
		}
	}
	// Calculate the inverse of the product moduli the i-th moduli and check
	// that they're coprime
	t := bigintfactory.New().GCD(&r.res, nil, prod, Ms[i])
	if t.Cmp(oneAsBigInt) != 0 {
		r.err = errors.SliceNotCoprime.New()
	} else {
		// Calculate a * prod * r.res
		r.res.Mul(r.res.Mul(&r.res, prod), a)
	}
	bigintfactory.Reuse(t)
	bigintfactory.Reuse(prod)
	// Notify the WaitGroup that we've finished
	wg.Done()
}

// crtCalculateData computes prod * invProd mod N, where prod is the product of the moduli Ms, excluding the i-th moduli, invProd is the inverse of prod modulo the i-th moduli, and N is the product of all the moduli Ms. The result is recorded in r.res. If the i-th moduli is not coprime with prod then the r.err will be set to an error.
func crtCalculateData(i int, Ms []*big.Int, r *crtResult, wg *sync.WaitGroup) {
	// Calculate the product of the residues Ms, excluding the i-th moduli
	prod := bigintfactory.New().SetInt64(1)
	for j, m := range Ms {
		if j != i {
			prod.Mul(prod, m)
		}
	}
	// Calculate the inverse of the product moduli the i-th moduli and check
	// that they're coprime
	t := bigintfactory.New().GCD(&r.res, nil, prod, Ms[i])
	if t.Cmp(oneAsBigInt) != 0 {
		r.err = errors.SliceNotCoprime.New()
	} else {
		// Calculate prod * r.res mod N
		r.res.Mod(r.res.Mul(&r.res, prod), t.Mul(prod, Ms[i]))
	}
	bigintfactory.Reuse(t)
	bigintfactory.Reuse(prod)
	// Notify the WaitGroup that we've finished
	wg.Done()
}

// generateCRTFunc returns a CRTFunc for the given product-inverses and product N.
func generateCRTFunc(prodInvs []*big.Int, N *big.Int) CRTFunc {
	return func(residues ...*Element) (*Element, error) {
		// Sanity check
		if len(residues) != len(prodInvs) {
			return nil, errors.SliceLengthNotEqual.New()
		}
		// Calculate \sum residues[i] * prodInvs[i] mod N
		sum, n := bigintfactory.New().SetInt64(0), bigintfactory.New()
		for i, a := range residues {
			if !a.IsZero() {
				sum.Mod(sum.Add(sum, n.Mul(a.bigInt(), prodInvs[i])), N)
			}
		}
		bigintfactory.Reuse(n)
		// Return the result
		return fromBigIntAndReuse(sum), nil
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ChineseRemainderTheorem returns the unique non-negative integer a such that 0 <= a < N and, for each i, a is congruent to a_i mod n_i, where residues = [a_1,...,a_k], moduli = [n_1,...,n_k], and N = n_1*...*n_k. The n_i must be positive and pairwise coprime.
func ChineseRemainderTheorem(residues []*Element, moduli []*Element) (*Element, error) {
	// Sanity checks
	size := len(residues)
	if size != len(moduli) {
		return nil, errors.SliceLengthNotEqual.New()
	} else if size == 0 {
		return nil, errors.EmptySlice.New()
	}
	// Move to slices of big.Ints -- first the moduli (checking for positivity
	// as we go)...
	Ms := make([]*big.Int, 0, size)
	for _, m := range moduli {
		if !m.IsPositive() {
			return nil, errors.SliceNotPositive.New()
		}
		Ms = append(Ms, m.bigInt())
	}
	// ...and now the residues
	As := make([]*big.Int, 0, size)
	for _, a := range residues {
		As = append(As, a.bigInt())
	}
	// Set some go-routines calculating the products that we need
	wg := sync.WaitGroup{}
	wg.Add(size)
	Rs := make([]crtResult, size)
	for i, a := range As {
		go crtInverseMod(i, a, Ms, &Rs[i], &wg)
	}
	// Whilst the go-routines are working, compute the product of the moduli
	N := bigintfactory.New().SetInt64(1)
	for _, m := range Ms {
		N.Mul(N, m)
	}
	// Wait for the go-routines to finish
	wg.Wait()
	// Extract the results
	sum := bigintfactory.New().SetInt64(0)
	for i := 0; i < size; i++ {
		if Rs[i].err != nil {
			bigintfactory.Reuse(N)
			return nil, Rs[i].err
		}
		sum.Add(sum, &Rs[i].res)
	}
	// Reduce mod N and return
	res := fromBigIntAndReuse(sum.Mod(sum, N))
	bigintfactory.Reuse(N)
	return res, nil
}

// PrepareChineseRemainderTheorem returns a function f that applies the Chinese Remainder Theorem to residues a_1,...,a_k. More precisely, f(a_1,...,a_k) returns the unique non-negative integer a such that 0 <= a < N and, for each i, n is congruent to a_i mod n_i, where the moduli are n_1,...,n_k, and N = n_1*...*n_k. The n_i must be positive and pairwise coprime.
func PrepareChineseRemainderTheorem(moduli ...*Element) (CRTFunc, error) {
	// Sanity checks
	size := len(moduli)
	if size == 0 {
		return nil, errors.EmptySlice.New()
	}
	// Move to a slice of big.Ints, checking for positivity as we go
	Ms := make([]*big.Int, 0, size)
	for _, m := range moduli {
		if !m.IsPositive() {
			return nil, errors.SliceNotPositive.New()
		}
		Ms = append(Ms, m.bigInt())
	}
	// Set some go-routines calculating the data that we need
	wg := sync.WaitGroup{}
	wg.Add(size)
	Rs := make([]crtResult, size)
	for i := range Ms {
		go crtCalculateData(i, Ms, &Rs[i], &wg)
	}
	// Whilst the go-routines are working, compute the product of the moduli
	N := bigintfactory.New().SetInt64(1)
	for _, m := range Ms {
		N.Mul(N, m)
	}
	// Wait for the go-routines to finish
	wg.Wait()
	// Check for an error and repackage the data
	prodInvs := make([]*big.Int, 0, size)
	for i := 0; i < size; i++ {
		if Rs[i].err != nil {
			return nil, Rs[i].err
		}
		prodInvs = append(prodInvs, &Rs[i].res)
	}
	// Return the function
	return generateCRTFunc(prodInvs, N), nil
}
