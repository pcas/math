// Tests for mersenne.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestSumOfPowersOfTwoBigInt tests the private function sumOfPowersOfTwoBigInt.
func TestSumOfPowersOfTwoBigInt(t *testing.T) {
	assert := assert.New(t)
	// Test small examples (format: {a,k,result})
	tests := [][]int64{
		{1, 3, 15},
		{1, 0, 1},
		{3, 0, 1},
		{1, 7, 255},
		{2, 7, 21845},
		{3, 5, 37449},
		{4, 4, 69905},
		{5, 3, 33825},
	}
	for _, test := range tests {
		assert.Equal(test[2], sumOfPowersOfTwoBigInt(test[0], test[1]).Int64())
	}
	// A non-positive exponent or a negative step size should panic
	assert.Panics(func() { _ = sumOfPowersOfTwoBigInt(0, 3) })
	assert.Panics(func() { _ = sumOfPowersOfTwoBigInt(2, -1) })
}

// TestToMersenneNumberBigInt tests the private function toMersenneNumberBigInt.
func TestToMersenneNumberBigInt(t *testing.T) {
	assert := assert.New(t)
	// Test small exponents
	tests := []int64{0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023}
	for n, val := range tests {
		assert.Equal(val, toMersenneNumberBigInt(int64(n)).Int64())
	}
	// A negative exponent should panic
	assert.Panics(func() { _ = toMersenneNumberBigInt(-1) })
}

// TestMersennePrimeFactors tests the private function mersennePrimeFactors.
func TestMersennePrimeFactors(t *testing.T) {
	assert := assert.New(t)
	// Test some small examples
	test := []int64{2, 5, 100, 128}
	for _, n := range test {
		if ok, Fs := mersennePrimeFactors(n); assert.True(ok) {
			if ok, k := IsMersenneNumber(Product(Fs...)); assert.True(ok) {
				if kk, err := k.Int64(); assert.NoError(err) {
					assert.Equal(n, kk)
				}
			}
		}
	}
}

// TestIsMersenneNumber tests IsMersenneNumber.
func TestIsMersenneNumber(t *testing.T) {
	assert := assert.New(t)
	two := FromInt(2)
	// Some powers n for which 2^n - 1 is known to be prime.
	tests := []int64{2, 13, 61, 4253, 110503, 13466917, 42643801, 74207281}
	for _, n := range tests {
		// First we check that we recognise this is a Mersenne number
		if pow2, err := two.PowerInt64(n); assert.NoError(err) {
			if ok, nn := IsMersenneNumber(pow2.Decrement()); assert.True(ok) {
				assert.True(nn.IsEqualToInt64(n))
			}
		}
		// Now we check that we recognise that this is a prime
		isPrime := isMersennePrime(n)
		assert.True(isPrime)
	}
	// Some powers n for which 2^n - 1 is known not to be prime.
	tests = []int64{11, 23, 9511, 17783, 1257827, 32582658}
	for _, n := range tests {
		// First we check that we recognise this is a Mersenne number
		if pow2, err := two.PowerInt64(n); assert.NoError(err) {
			if ok, nn := IsMersenneNumber(pow2.Decrement()); assert.True(ok) {
				assert.True(nn.IsEqualToInt64(n))
			}
		}
		// Now we check that we recognise that this isn't a prime
		isPrime := isMersennePrime(n)
		assert.False(isPrime)
	}
	// Test the Lucas--Lehmer implementation, first for powers p for which we
	// know 2^p - 1 is not prime
	tests = []int64{131, 523, 613, 1283}
	for _, p := range tests {
		assert.False(isLucasLehmer(p))
	}
	// And now for powers p for which we know 2^p - 1 is prime
	tests = []int64{127, 521, 607, 1279}
	for _, p := range tests {
		assert.True(isLucasLehmer(p))
	}
	// Finally, test some numbers that aren't Mersenne numbers.
	tests = []int64{33, 2053, 1152921504606846969}
	for _, k := range tests {
		ok, _ := IsMersenneNumber(FromInt64(k))
		assert.False(ok)
	}
	if pow2, err := two.PowerInt64(100); assert.NoError(err) {
		ok, _ := IsMersenneNumber(pow2.Increment())
		assert.False(ok)
	}
}
