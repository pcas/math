// Tests for sort.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/object"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// testIsSorted returns true iff the given slice is sorted in increasing order.
func testIsSorted(S []*Element) bool {
	if len(S) == 0 {
		return true
	}
	for i, s := range S[1:] {
		if !s.IsGreaterThanOrEqualTo(S[i]) {
			return false
		}
	}
	return true
}

// testIsSortedObject returns true iff the given slice is sorted in increasing order.
func testIsSortedObject(S []object.Element) (bool, error) {
	if len(S) == 0 {
		return true, nil
	}
	zZ := Ring()
	for i, s := range S[1:] {
		if sgn, err := zZ.Cmp(s, S[i]); err != nil {
			return false, err
		} else if sgn < 0 {
			return false, nil
		}
	}
	return true, nil
}

// testReverseSlice reverses the slice.
func testReverseSlice(S []*Element) []*Element {
	T := make([]*Element, 0, len(S))
	for i := len(S) - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}

// testReverseSliceObject reverses the slice.
func testReverseSliceObject(S []object.Element) []object.Element {
	T := make([]object.Element, 0, len(S))
	for i := len(S) - 1; i >= 0; i-- {
		T = append(T, S[i])
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestSort(t *testing.T) {
	assert := assert.New(t)
	zZ := Ring()
	// Generate random slices of different lengths
	for _, size := range []int{0, 1, 2, 3, 100, maxSortLen / 2, 3*maxSortLen + 2} {
		// Generate a random slice
		S := make([]*Element, 0, size)
		objs := make([]object.Element, 0, size)
		for i := 0; i < cap(S); i++ {
			c := FromInt64(rand.Int63n(500))
			S = append(S, c)
			objs = append(objs, c)
		}
		// Sort the slice and test
		assert.True(testIsSorted(Sort(S)))
		if sorted, err := zZ.Sort(objs); assert.NoError(err) {
			if ok, err := testIsSortedObject(sorted); assert.NoError(err) {
				assert.True(ok)
			}
		}
		// Reverse the slice
		S = testReverseSlice(S)
		objs = testReverseSliceObject(objs)
		// Sort the slice and test
		assert.True(testIsSorted(Sort(S)))
		if sorted, err := zZ.Sort(objs); assert.NoError(err) {
			if ok, err := testIsSortedObject(sorted); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
}
