// Primepower tests whether an integer is a prime power.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"math"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// largestPower2AtMost returns the largest exponent k such that 2^k <= n. Requires that n is positive.
func largestPower2AtMost(n int64) int64 {
	// Sanity check
	if n < 1 {
		panic("Argument must be a positive integer.")
	}
	// Start bit shifting
	m := n >> 1
	k := uint(0)
	for m != 0 {
		m >>= 1
		k++
	}
	return int64(k)
}

// isFloat64AnInt64 attempts to determine whether the given float64 is an int64. You will have to somehow check whether the suggested int64 is correct.
func isFloat64AnInt64(x float64) (bool, int64) {
	if y := math.Floor(x); y+0.025 > x {
		n := int64(y)
		nn := float64(n)
		if nn-0.025 > y {
			return true, n - 1
		} else if nn+0.025 < y {
			return true, n + 1
		}
		return true, n
	} else if y := math.Ceil(x); y-0.025 < x {
		n := int64(y)
		nn := float64(n)
		if nn-0.025 > y {
			return true, n - 1
		} else if nn+0.025 < y {
			return true, n + 1
		}
		return true, n
	}
	return false, 0
}

// isPowerEqualToInt64 computes a^b and checks whether the result is equal to c. Assumes b > 0, and that a^b fits in an int64.
func isPowerEqualToInt64(a int64, b int64, c int64) bool {
	if b <= 0 {
		panic("Exponent must be a positive integer.")
	}
	val := a
	for ; b > 1; b-- {
		if val > c {
			return false
		}
		val *= a
	}
	return val == c
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsPrimePowerInt64 tests whether the integer n is equal to p^k for some prime p. If n < 2 this returns an error.
func IsPrimePowerInt64(n int64) (ok bool, p int64, k int64, err error) {
	// Sanity check
	if n < 2 {
		err = errors.ArgNotAnIntegerGreaterThan1.New()
		return
	}
	// Check whether n is prime
	if IsPrimeInt64(n) {
		return true, n, 1, nil
	}
	// Our strategy is to use the fact that log2(n) = k log2(p) to bound k
	logn := math.Log2(float64(n))
	k = largestPower2AtMost(n)
	for ; k >= 2; k-- {
		// Our candidate p = 2^(log2(n) / k)
		if ok, p = isFloat64AnInt64(math.Exp2(logn / float64(k))); ok && isPowerEqualToInt64(p, k, n) {
			if IsPrimeInt64(p) {
				return
			}
			return false, 0, 0, nil
		}
	}
	return false, 0, 0, nil
}
