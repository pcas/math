// Creation handles creation of new *Elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"math"
	"math/big"
	"strconv"
	"strings"
	"sync"
)

// _elementFactory is a the factory used to efficiently generate new *Elements. Do NOT access the factory directly -- use the function newElement.
var _elementFactory = struct {
	m     sync.Mutex // The mutex controlling access
	idx   int        // The current index in the cache
	cache []Element  // The underlying factory cache
}{}

// _elementFactorySize is the size of the factory cache.
const _elementFactorySize = 1000

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// rawElement returns a new *Element.
func rawElement() *Element {
	_elementFactory.m.Lock()
	idx := _elementFactory.idx
	if _elementFactory.cache == nil {
		_elementFactory.cache = make([]Element, _elementFactorySize)
		idx = 0
	}
	e := &_elementFactory.cache[idx]
	idx++
	if idx == _elementFactorySize {
		_elementFactory.cache = nil
	} else {
		_elementFactory.idx = idx
	}
	_elementFactory.m.Unlock()
	return e
}

// fromBigIntAndReuse returns a *Element with value n. Warning: The argument n will be made available for reuse, hence you must NOT modify n after calling this function.
func fromBigIntAndReuse(n *big.Int) *Element {
	if n == nil {
		return Zero()
	} else if n.IsInt64() {
		m := FromInt64(n.Int64())
		bigintfactory.Reuse(n)
		return m
	}
	m := rawElement()
	m.n = *n
	return m
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Zero returns 0.
func Zero() *Element {
	return &_smallIntegerCache[-smallIntegerCacheMin]
}

// One returns 1.
func One() *Element {
	return &_smallIntegerCache[1-smallIntegerCacheMin]
}

// FromInt returns n as an integer.
func FromInt(n int) *Element {
	return FromInt64(int64(n))
}

// FromInt32 returns n as an integer.
func FromInt32(n int32) *Element {
	return FromInt64(int64(n))
}

// FromInt64 returns a *Element with value n.
func FromInt64(n int64) *Element {
	if n >= smallIntegerCacheMin && n <= smallIntegerCacheMax {
		return &_smallIntegerCache[int(n)-smallIntegerCacheMin]
	}
	m := rawElement()
	m.k = n
	return m
}

// FromUint64 returns a *Element with value n.
func FromUint64(n uint64) *Element {
	if n <= math.MaxInt64 {
		return FromInt64(int64(n))
	}
	m := rawElement()
	m.n.SetUint64(n)
	return m
}

// FromBigInt returns a *Element with value n.
func FromBigInt(n *big.Int) *Element {
	if n == nil {
		return Zero()
	} else if n.IsInt64() {
		return FromInt64(n.Int64())
	}
	m := rawElement()
	m.n.Set(n)
	return m
}

// FromString returns the integer represented by the string s.
func FromString(s string) (*Element, error) {
	// strip leading and trailing whitespace
	ss := strings.TrimSpace(s)
	// try to parse it as an int64
	if val, err := strconv.ParseInt(ss, 10, 64); err == nil {
		return FromInt64(val), nil
	}
	// otherwise parse it as a big.Int
	n := bigintfactory.New()
	if _, ok := n.SetString(ss, 10); !ok {
		return nil, errors.StringNotAnInteger.New()
	}
	return fromBigIntAndReuse(n), nil
}
