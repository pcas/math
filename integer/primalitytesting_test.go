// Tests for primalitytesting.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

// TestIsPrime tests the primality checking.
func TestIsPrime(t *testing.T) {
	assert := assert.New(t)
	// Ranges to test: we call IsPrime(n) for all n with k-10<=n<=k+10
	testRanges := []int64{
		0,
		8161,
		1373653,
		1373677,
		25326001,
		25326023,
		3215031751,
		3215031767,
		118670087467,
		118670087489,
		2152302898747,
		2152302898771,
		3474749660383,
		3474749660401,
	}
	// A parallel sequence of results from these tests
	testResults := [][]bool{
		// centred at 0
		{false, false, false, true, false, true, false, true, true, false, false, false, true, true, false, true, false, true, false, false, false},
		// centred at 8161
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, true},
		// centred at 1373653
		{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 1373677
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false},
		// centred at 25326001
		{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 25326023
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false},
		// centred at 3215031751
		{false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 3215031767
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false},
		// centred at 118670087467
		{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 118670087489
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false},
		// centred at 2152302898747
		{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 2152302898771
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false},
		// centred at 3474749660383
		{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		// centred at 3474749660401
		{false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false},
	}
	for i, k := range testRanges {
		for j := 0; j <= 20; j++ {
			n := k + int64(j) - 10
			assert.Equal(testResults[i][j], IsPrimeInt64(n), "Calculated: IsPrime(%d)", n)
			assert.Equal(testResults[i][j], IsPrime(FromInt64(n)), "Calculated: IsPrime(%d)", n)
		}
	}
	// Assert that we are correct <= 13
	assert.True(IsPrime(FromInt(2)))
	assert.True(IsPrime(FromInt(3)))
	assert.False(IsPrime(FromInt(4)))
	assert.True(IsPrime(FromInt(5)))
	assert.False(IsPrime(FromInt(6)))
	assert.True(IsPrime(FromInt(7)))
	assert.False(IsPrime(FromInt(8)))
	assert.False(IsPrime(FromInt(9)))
	assert.False(IsPrime(FromInt(10)))
	assert.True(IsPrime(FromInt(11)))
	assert.False(IsPrime(FromInt(12)))
	assert.True(IsPrime(FromInt(13)))
	// There are 295947 primes <= 2^22
	count := 6
	for n := int64(15); n <= 1<<22; n += 2 {
		if n%3 != 0 && n%5 != 0 && n%7 != 0 && n%11 != 0 && n%13 != 0 && IsPrime(FromInt64(n)) {
			count++
		}
	}
	assert.Equal(295947, count, "There are 295947 primes <= 2^22")
	// The largest prime that fits in an int64
	assert.True(IsPrime(FromInt64(maxPrimeInt64)))
	for n := int64(maxPrimeInt64) + 1; n <= math.MaxInt64-1; n++ {
		assert.False(IsPrime(FromInt64(n)))
	}
	// Test the smallest int64
	assert.False(IsPrimeInt64(math.MinInt64))
	// Test a Mersenne prime larger that math.MaxInt64
	assert.True(IsPrime(toMersenneNumber(74207281)))
	// There are 25 primes in the range 2^64 <= n <= 2^64 + 1000
	if n, err := FromString("18446744073709551616"); assert.NoError(err) {
		count := 0
		for i := 0; i < 1000; i++ {
			if n.IsPrime() {
				count++
			}
			n = n.Increment()
		}
		assert.Equal(25, count, "There are 25 primes 2^64 <= n <= 2^64 + 1000")
	}
	// A value maxExactValueForPrime12 < n < maxExactValueForPrime13
	if n, err := FromString("3317044064679887385961813"); assert.NoError(err) {
		assert.True(IsPrime(n))
	}
	// A value n > maxExactValueForPrime13
	if n, err := FromString("1267650600228229401496703205377"); assert.NoError(err) { // 2^100+1
		assert.False(IsPrime(n), "This is a probabilistic test, so will fail occasionally (with very very low probability).")
	}
}

// TestIsProbablyPrime tests IsProbablyPrime. Note that this is a probabilistic test, so will fail occasionally (with very very low probability).
func TestIsProbablyPrime(t *testing.T) {
	assert := assert.New(t)
	// Assert that we are correct for small primes
	for n := 1; n <= 200; n++ {
		m := FromInt(n)
		assert.Equal(IsProbablyPrime(m, 10), IsPrime(m), "This is a probabilistic test, so will fail occasionally (with very very low probability).")
	}
}
