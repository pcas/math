// Encoding contains functions for encoding/decoding an integer.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
)

// encodingVersion is the internal version number. This will permit backwards-compatible changes to the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (n *Element) GobEncode() ([]byte, error) {
	// Deal with the nil case
	if n == nil {
		return Zero().GobEncode()
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// How we proceed depends on whether n is an int64
	if n.IsInt64() {
		// Record that this is an int64, then encode the int64
		if err := enc.Encode(true); err != nil {
			return nil, err
		} else if err = enc.Encode(n.int64()); err != nil {
			return nil, err
		}
	} else {
		// Record that this is not an int64, then encode the *big.Int
		if err := enc.Encode(false); err != nil {
			return nil, err
		} else if err = enc.Encode(n.bigInt()); err != nil {
			return nil, err
		}
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface. Important: Take great care that you are decoding into a new *Element; the safe way to do this is to use the GobDecode(dec *gob.Decode) function.
func (n *Element) GobDecode(buf []byte) error {
	// Sanity check
	if n == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if !n.IsZero() || n == Zero() {
		return gobutil.DecodingIntoExistingObject.New()
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Is this an int64? Or is this a *big.Int?
	var isInt64 bool
	if err := dec.Decode(&isInt64); err != nil {
		return err
	} else if isInt64 {
		if err = dec.Decode(&n.k); err != nil {
			return err
		}
	} else if err = dec.Decode(&n.n); err != nil {
		return err
	}
	return nil
}

// GobDecode reads the next value from the given gob.Decoder and decodes it as an integer.
func GobDecode(dec *gob.Decoder) (*Element, error) {
	// Decode into a new integer
	n := rawElement()
	if err := dec.Decode(n); err != nil {
		return nil, err
	}
	// If this should really be an object from the cache, replace it
	if n.IsInt64() {
		if k := n.int64(); k >= smallIntegerCacheMin && k <= smallIntegerCacheMax {
			return &_smallIntegerCache[int(k)-smallIntegerCacheMin], nil
		}
	}
	return n, nil
}

// GobDecodeSlice reads the next value from the given gob.Decoder and decodes it as a slice of integers.
func GobDecodeSlice(dec *gob.Decoder) ([]*Element, error) {
	// Decode into a slice
	var S []*Element
	if err := dec.Decode(&S); err != nil {
		return nil, err
	}
	// Replace any instances of objects that should really be from the cache
	for i, n := range S {
		if n.IsInt64() {
			if k := n.int64(); k >= smallIntegerCacheMin && k <= smallIntegerCacheMax {
				S[i] = &_smallIntegerCache[int(k)-smallIntegerCacheMin]
			}
		}
	}
	return S, nil
}
