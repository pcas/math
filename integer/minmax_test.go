// Tests for minmax.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/object"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func TestMinMaxOfPair(t *testing.T) {
	assert := assert.New(t)
	// Create a table of tests. Each row in the table is of the form:
	//	[]int{ x1, x2 }
	// where x1 <= x2.
	tests := [][]int{
		[]int{0, 1},
		[]int{-1, 7},
		[]int{-7, 0},
		[]int{12, 12},
	}
	// Run the tests
	for _, test := range tests {
		x1 := FromInt(test[0])
		x2 := FromInt(test[1])
		// Test MinOfPair
		assert.True(MinOfPair(x1, x2).IsEqualTo(x1))
		assert.True(MinOfPair(x2, x1).IsEqualTo(x1))
		// Test MaxOfPair
		assert.True(MaxOfPair(x1, x2).IsEqualTo(x2))
		assert.True(MaxOfPair(x2, x1).IsEqualTo(x2))
	}
}

func TestMinMax(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	zZ := Ring()
	// Create a table of tests. Each row in the table is of the form:
	//  []int{ x0, ..., xn, idx of min, idx of max }
	tests := [][]int{
		[]int{79, 0, 0},
		[]int{5, 54, 0, 1},
		[]int{42, 7, 1, 0},
		[]int{9, 0, 1, 0},
		[]int{34, 9, 6, 2, 0},
		[]int{1, 0, -1, 0, 7, 2, 4},
	}
	// Run the tests
	for _, test := range tests {
		S := make([]*Element, 0, len(test)-2)
		for _, s := range test[:len(test)-2] {
			S = append(S, FromInt(s))
		}
		objs, err := object.SliceToElementSlice(S)
		require.NoError(err)
		// Make a note of the min and max indices
		minIdx := test[len(test)-2]
		maxIdx := test[len(test)-1]
		// Test Min
		if val, err := Min(S...); assert.NoError(err) {
			assert.True(val.IsEqualTo(S[minIdx]))
		}
		if val, err := zZ.Min(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, S[minIdx]); assert.NoError(err) {
				assert.True(ok)
			}
		}
		// Test Max
		if val, err := Max(S...); assert.NoError(err) {
			assert.True(val.IsEqualTo(S[maxIdx]))
		}
		if val, err := zZ.Max(objs...); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, S[maxIdx]); assert.NoError(err) {
				assert.True(ok)
			}
		}
		// Test MinAndIndex
		if val, idx, err := MinAndIndex(S); assert.NoError(err) {
			assert.True(val.IsEqualTo(S[minIdx]))
			assert.Equal(idx, minIdx)
		}
		if val, idx, err := zZ.MinAndIndex(objs); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, S[minIdx]); assert.NoError(err) {
				assert.True(ok)
			}
			assert.Equal(idx, minIdx)
		}
		// Test MaxAndIndex
		if val, idx, err := MaxAndIndex(S); assert.NoError(err) {
			assert.True(val.IsEqualTo(S[maxIdx]))
			assert.Equal(idx, maxIdx)
		}
		if val, idx, err := zZ.MaxAndIndex(objs); assert.NoError(err) {
			if ok, err := zZ.AreEqual(val, S[maxIdx]); assert.NoError(err) {
				assert.True(ok)
			}
			assert.Equal(idx, maxIdx)
		}
	}
	// Generate a long slice
	S := make([]*Element, 0, 3*maxMinMaxLen+4)
	for i := 0; i < cap(S); i++ {
		S = append(S, FromInt(i))
	}
	minIdx := 3
	maxIdx := 100
	S[minIdx] = FromInt(-5)
	S[maxIdx] = FromInt(len(S) + 3)
	// Test Min
	if val, err := Min(S...); assert.NoError(err) {
		assert.True(val.IsEqualTo(S[minIdx]))
	}
	// Test Max
	if val, err := Max(S...); assert.NoError(err) {
		assert.True(val.IsEqualTo(S[maxIdx]))
	}
	// Test MinAndIndex
	if val, idx, err := MinAndIndex(S); assert.NoError(err) {
		assert.True(val.IsEqualTo(S[minIdx]))
		assert.Equal(idx, minIdx)
	}
	// Test MaxAndIndex
	if val, idx, err := MaxAndIndex(S); assert.NoError(err) {
		assert.True(val.IsEqualTo(S[maxIdx]))
		assert.Equal(idx, maxIdx)
	}
	// The empty slice should return an error
	S = make([]*Element, 0)
	var objs []object.Element
	_, err := Min(S...)
	assert.Error(err)
	_, err = zZ.Min(objs...)
	assert.Error(err)
	_, err = Max(S...)
	assert.Error(err)
	_, err = zZ.Max(objs...)
	assert.Error(err)
	_, _, err = MinAndIndex(S)
	assert.Error(err)
	_, _, err = zZ.MinAndIndex(objs)
	assert.Error(err)
	_, _, err = MaxAndIndex(S)
	assert.Error(err)
	_, _, err = zZ.MaxAndIndex(objs)
	assert.Error(err)
}
