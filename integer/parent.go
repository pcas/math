// Parent represents the ring of integers.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/object"
)

// Parent represents the (unique) ring of integers.
type Parent struct{}

// toIntegerer defines an optional interface that a object.Element can satisfy in order to convert it to an integer.
type toIntegerer interface {
	ToInteger() (*Element, error) // ToInteger attempts to convert the object.Element to an integer.
}

// toIntegererNoError defines an optional interface that a object.Element can satisfy in order to convert it to an integer.
type toIntegererNoError interface {
	ToInteger() *Element // ToInteger converts the object.Element to an integer.
}

// The unique ring of integers.
var zZ = Parent{}

// The string representation of the ring of integers.
const defaultName = "ZZ"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// objectsToElements converts the two arguments to Elements.
func objectsToElements(x1 object.Element, x2 object.Element) (*Element, *Element, error) {
	y1, err1 := ToElement(x1)
	if err1 != nil {
		return nil, nil, errors.Arg1NotAnInteger.New()
	}
	y2, err2 := ToElement(x2)
	if err2 != nil {
		return nil, nil, errors.Arg2NotAnInteger.New()
	}
	return y1, y2, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Contains returns true iff x is an integer, or can naturally be regarded as an integer.
func (R Parent) Contains(x object.Element) bool {
	_, err := ToElement(x)
	return err == nil
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (R Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(x)
}

// String returns a string representation of the ring of integers.
func (R Parent) String() string {
	return defaultName
}

// Zero returns 0.
func (R Parent) Zero() object.Element {
	return Zero()
}

// IsZero returns true iff x is the integer 0.
func (R Parent) IsZero(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// One returns 1.
func (R Parent) One() object.Element {
	return One()
}

// IsOne returns true iff x is the integer 1.
func (R Parent) IsOne(x object.Element) (bool, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, err
	}
	return xx.IsOne(), nil
}

// Add returns x + y.
func (R Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy), nil
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element.
func (R Parent) Sum(S ...object.Element) (object.Element, error) {
	// Get the length 0 and 1 cases out of the way
	size := len(S)
	if size == 0 {
		return Zero(), nil
	} else if size == 1 {
		return ToElement(S[0])
	}
	// Convert the slice type
	sS := make([]*Element, 0, size)
	for _, x := range S {
		xx, err := ToElement(x)
		if err != nil {
			return nil, err
		}
		sS = append(sS, xx)
	}
	// Return the result
	return Sum(sS...), nil
}

// Subtract returns x - y.
func (R Parent) Subtract(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Subtract(xx, yy), nil
}

// Multiply returns the product x * y.
func (R Parent) Multiply(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return nil, err
	}
	return Multiply(xx, yy), nil
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one.
func (R Parent) Product(S ...object.Element) (object.Element, error) {
	// Get the length 0 and 1 cases out of the way
	size := len(S)
	if size == 0 {
		return One(), nil
	} else if size == 1 {
		return ToElement(S[0])
	}
	// Convert the slice type
	sS := make([]*Element, 0, size)
	for _, x := range S {
		xx, err := ToElement(x)
		if err != nil {
			return nil, err
		}
		sS = append(sS, xx)
	}
	// Return the result
	return Product(sS...), nil
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.
func (R Parent) DotProduct(S1 []object.Element, S2 []object.Element) (object.Element, error) {
	// Convert S1 and S2 to slices of integers
	T1 := make([]*Element, 0, len(S1))
	for _, x := range S1 {
		xx, err := ToElement(x)
		if err != nil {
			return nil, err
		}
		T1 = append(T1, xx)
	}
	T2 := make([]*Element, 0, len(S2))
	for _, x := range S2 {
		xx, err := ToElement(x)
		if err != nil {
			return nil, err
		}
		T2 = append(T2, xx)
	}
	// Return the dot-product
	return DotProduct(T1, T2), nil
}

// Negate returns -x.
func (R Parent) Negate(x object.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, err
	}
	return xx.Negate(), nil
}

// Power returns x^k, where k is a non-negative integer.
func (R Parent) Power(x object.Element, k *Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, errors.Arg1NotAnInteger.New()
	}
	return Power(xx, k)
}

// IsUnit returns true iff x is a multiplicative unit in the ring of integers (i.e. x = 1 or -1). If true, also returns the inverse element y such that x * y = 1.
func (R Parent) IsUnit(x object.Element) (bool, object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return false, nil, err
	} else if xx.IsOne() {
		return true, xx, nil
	} else if xx.IsInt64() && xx.int64() == -1 {
		return true, xx, nil
	}
	return false, nil, nil
}

// ScalarMultiplyByInteger returns nx, where this is defined to be x + ... + x (n times) if n is positive, and -x - ... - x (|n| times) if n is negative.
func (R Parent) ScalarMultiplyByInteger(n *Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(x)
	if err != nil {
		return nil, errors.Arg2NotAnInteger.New()
	}
	return Multiply(n, xx), nil
}

// AreEqual returns true iff x equals y.
func (R Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
func (R Parent) Cmp(x object.Element, y object.Element) (int, error) {
	xx, yy, err := objectsToElements(x, y)
	if err != nil {
		return 0, err
	}
	return Cmp(xx, yy), nil
}

// FromInteger returns n.
func (R Parent) FromInteger(n *Element) object.Element {
	return n
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Ring returns the (unique) ring of integers.
func Ring() Parent {
	return zZ
}

// ToElement attempts to convert the given object.Element to an integer. If x satisfies either of the interfaces:
//		type ToIntegerer interface {
//			ToInteger() (*Element, error) // ToInteger attempts to convert the
//			   object.Element to an integer.
//		}
//
//		type ToIntegererNoError interface {
//			ToInteger() *Element // ToInteger converts the object.Element to an
//			   integer.
//		}
// then x's ToInteger method will be called.
func ToElement(x object.Element) (*Element, error) {
	if y, ok := x.(*Element); ok {
		return y, nil
	} else if y, ok := x.(toIntegerer); ok {
		if yy, err := y.ToInteger(); err == nil {
			return yy, nil
		}
	} else if y, ok := x.(toIntegererNoError); ok {
		return y.ToInteger(), nil
	}
	return nil, errors.ArgNotAnInteger.New()
}
