// Primality testing code for (probabilistic) prime testing via SPRP tests.

/*
Summary
-------
This is adapted from code by John Moyer <http://www.rsok.com/~jrm/>. Based on algorithms by Olivier Langlois <http://www.utm.edu/research/primes/prove/prove2_3.html>.

The following is based on a summary (and is in part just a verbatim copy) of <http://www.utm.edu/research/primes/prove/prove2_3.html>.

If n is an odd prime, then the number 1 has just two square roots modulo n: 1 and -1. So the square root of a^(n-1), a^((n-1)/2) (since n will be odd), is either 1 or -1. If (n-1)/2 is even, we can easily take another square root... Let's make this into an algorithm:

Write n-1 = 2^s * d where d is odd and s is non-negative. We say that n is a strong probable-prime base a (for short: an a-SPRP) if either a^d = 1 (mod n) or (a^d)^(2^r) = -1 (mod n) for some non-negative r less than s.

All integers n > 1 which fail this test are composite; integers that pass it might be prime.

Individually these tests are still weak (and there are infinitely many a-SPRP's for every base a > 1 [PSW80]), but we can combine these individual tests to make powerful tests for small integers n > 1 (these tests prove primality):
 * If n < 1,373,653 is a both 2 and 3-SPRP, then n is prime.
 * If n < 25,326,001 is a 2, 3 and 5-SPRP, then n is prime.
 * If n < 25,000,000,000 is a 2, 3, 5 and 7-SPRP, then either n == 3,215,031,751 or n is prime. (This is actually true for n < 118,670,087,467.)
 * If n < 2,152,302,898,747 is a 2, 3, 5, 7 and 11-SPRP, then n is prime.
 * If n < 3,474,749,660,383 is a 2, 3, 5, 7, 11 and 13-SPRP, then n is prime.
 * If n < 341,550,071,728,321 is a 2, 3, 5, 7, 11, 13 and 17-SPRP, then n is prime.
 * If n < 318,665,857,834,031,151,167,461 is a 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, and 37-SPRP, then n is prime.
 * If n < 3,317,044,064,679,887,385,961,981 is a 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, and 41-SPRP, then n is prime.

The first three of these are due to Pomerance, Selfridge and Wagstaff [PSW80], the parenthetical remark and all but the final two results are due to Jaeschke [Jaeschke93]. (These and related results are summarised in [Ribenboim95, Chpt 2viiib].) The last two results are due to [SW15].

Here is the way we use the above results to make a quick primality test: we check against a cache of small primes; then perform strong primality tests base 2, 3, 5,... until one of the criteria above is met. For example, if n < 25,326,001 we need only check bases 2, 3 and 5. This is much faster than trial division (because someone else has already done much of the work).

For numbers <= 2^63 - 1 (MaxInt64) we proceed as above. For n > 2^63 - 1, we give an exact answer if n < 3,317,044,064,679,887,385,961,981, and call ProbablyPrime from the Go Big library otherwise. Per the documentation, this does N Miller--Rabin tests -- that is, checks if n is an a-SPRP for N further values of a; per the source code (Go 1.5) the values of a are chosen at random. Then we return true if all these pass and false otherwise (in which case n is known to be composite).

The default value of N is the contant DefaultNumberOfMillerRabinTests; you can set a different value with n.ProbablyPrime(N). The probability of ProbablyPrime returning true if n is composite is (1/4)^N.

References
----------
[Jaeschke93] G. Jaeschke, "On strong pseudoprimes to several bases,"  Math. Comp., 61 (1993) 915--926.
[PSW80] C. Pomerance, J. L. Selfridge, and S. S. Wagstaff, "The pseudoprimes to 25 x 10^9," Math. Comp., 35:151 (1980) 1003--1026.
[Ribenboim95] P. Ribenboim, "The new book of prime number records", 3rd edition, Springer-Verlag, 1995.
[SW15] J. Sorenson and J. Webster, "Strong Pseudoprimes to Twelve Prime Bases", arXiv:1509.00864 [math.NT]
*/

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/mathutil"
	"fmt"
	"math"
	"math/big"
)

// DefaultNumberOfMillerRabinTests is the default number of Miller--Rabin tests when doing probabilistic primality testing in IsPrime(). This is only relevant for n such that |n| >= 2^63 - 1.
var DefaultNumberOfMillerRabinTests = 10

// maxExactValueForPrime12 is the maximum value 318,665,857,834,031,151,167,461 up to which we know that our SPRP tests give an exact answer using the first 12 prime bases.
var maxExactValueForPrime12 *big.Int

// maxExactValueForPrime13 is the maximum value 3,317,044,064,679,887,385,961,981 up to which we know that our SPRP tests give an exact answer using the first 13 prime bases.
var maxExactValueForPrime13 *big.Int

/////////////////////////////////////////////////////////////////////////
// SPRP functions
/////////////////////////////////////////////////////////////////////////

// isSPRP returns true if n is an a-SPRP and false otherwise. Here a should be non-zero, and n should be at least 2.
func isSPRP(n int64, a int64) bool {
	// Find d and s such that 2^s*d = n-1 and d is odd
	d := n - 1
	var s int64
	for d%2 == 0 {
		d = d / 2
		s++
	}
	// If  a^d = 1 (mod n) or (a^d)^(2^i) = -1 (mod n) for some non-negative i less than s then return true. Otherwise return false.
	aPower := mathutil.ExpModInt64(a, d, n)
	if aPower == 1 || aPower == n-1 {
		return true
	}
	for ; s > 1; s-- {
		aPower = mathutil.MulModInt64(aPower, aPower, n)
		if aPower == n-1 {
			return true
		}
	}
	return false
}

// isSPRPWithCancel returns true if n is an a-SPRP and false otherwise. Here a should be non-zero, and n should be at least 2. The computation can be aborted at any time by closing the "cancel" channel.
func isSPRPWithCancel(n int64, a int64, cancel <-chan struct{}) bool {
	// Find d and s such that 2^s*d = n-1 and d is odd
	d := n - 1
	var s int64
	for d%2 == 0 {
		d = d / 2
		s++
	}
	// If  a^d = 1 (mod n) or (a^d)^(2^i) = -1 (mod n) for some non-negative i less than s then return true. Otherwise return false.
	aPower := mathutil.ExpModInt64(a, d, n)
	if aPower == 1 || aPower == n-1 {
		return true
	}
	for ; s > 1; s-- {
		select {
		case _ = <-cancel:
			// We've been asked to cancel
			return false
		default:
			// Perform the test
			aPower = mathutil.MulModInt64(aPower, aPower, n)
			if aPower == n-1 {
				return true
			}
		}
	}
	return false
}

// isSPRPForSlice returns true iff n is an a-SPRP for each a in A.
func isSPRPForSlice(n int64, A []int64) bool {
	// Create the communication channels
	c := make(chan bool)
	cancel := make(chan struct{})
	// Start the SPRP tests running in separate go-routines
	for _, a := range A {
		go func(a int64) {
			c <- isSPRPWithCancel(n, a, cancel)
		}(a)
	}
	// Wait for the results -- we return as soon as we get a failure
	for i := 0; i < len(A); i++ {
		if !<-c {
			// An SPRP test failed
			go func(i int) {
				// Ask the other tests to cancel
				close(cancel)
				// Wait for them to exit
				for ; i < len(A); i++ {
					_ = <-c
				}
				// Tidy up
				close(c)
			}(i + 1)
			// Return failure
			return false
		}
	}
	// If we're here then all the SPRP tests passed
	close(cancel)
	close(c)
	return true
}

// isSPRPBigIntWithCancel returns true if n is an a-SPRP and false otherwise. Here a should be non-zero, and n should be at least 2. The computation can be aborted at any time by closing the "cancel" channel.
func isSPRPBigIntWithCancel(n *big.Int, a int64, cancel <-chan struct{}) (result bool) {
	// Find d and s such that 2^s*d = n-1 and d is odd
	nSub1 := bigintfactory.New().Sub(n, oneAsBigInt)
	d := bigintfactory.New().Set(nSub1)
	s := bigintfactory.New().SetInt64(0)
	for d.Bit(0) == 0 {
		d.Rsh(d, 1)
		s.Add(s, oneAsBigInt)
	}
	// If  a^d = 1 (mod n) or (a^d)^(2^i) = -1 (mod n) for some non-negative i less than s then return true. Otherwise return false.
	aPower := bigintfactory.New().SetInt64(a)
	aPower.Exp(aPower, d, n)
	if aPower.Cmp(oneAsBigInt) == 0 || aPower.Cmp(nSub1) == 0 {
		result = true
		goto finished
	}
	d.SetInt64(2)
	for ; s.Cmp(oneAsBigInt) > 0; s.Sub(s, oneAsBigInt) {
		select {
		case _ = <-cancel:
			// We've been asked to cancel
			goto finished
		default:
			// Perform the test
			if aPower.Exp(aPower, d, n).Cmp(nSub1) == 0 {
				result = true
				goto finished
			}
		}
	}
finished:
	bigintfactory.Reuse(nSub1)
	bigintfactory.Reuse(d)
	bigintfactory.Reuse(s)
	bigintfactory.Reuse(aPower)
	return
}

// isSPRPForSliceBigInt returns true iff n is an a-SPRP for each a in A.
func isSPRPForSliceBigInt(n *big.Int, A []int64) bool {
	// Create the communication channels
	c := make(chan bool)
	cancel := make(chan struct{})
	// Start the SPRP tests running in separate go-routines
	nn := bigintfactory.New().Set(n)
	for _, a := range A {
		go func(a int64) {
			c <- isSPRPBigIntWithCancel(nn, a, cancel)
		}(a)
	}
	// Wait for the results -- we return as soon as we get a failure
	for i := 0; i < len(A); i++ {
		if !<-c {
			// An SPRP test failed
			go func(i int) {
				// Ask the other tests to cancel
				close(cancel)
				// Wait for them to exit
				for ; i < len(A); i++ {
					_ = <-c
				}
				// Tidy up
				close(c)
				bigintfactory.Reuse(nn)
			}(i + 1)
			// Return failure
			return false
		}
	}
	// If we're here then all the SPRP tests passed
	close(cancel)
	close(c)
	bigintfactory.Reuse(nn)
	return true
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// initMaxExactValueForPrime is called by this packages init function, and sets the constant values maxExactValueForPrime12 and maxExactValueForPrime13.
func initMaxExactValueForPrime() {
	maxExactValueForPrime12 = new(big.Int)
	_, err := fmt.Sscan("318665857834031151167461", maxExactValueForPrime12)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	maxExactValueForPrime13 = new(big.Int)
	_, err = fmt.Sscan("3317044064679887385961981", maxExactValueForPrime13)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
}

// isPrimeBigInt runs primality tests on the absolute value |n| of n, returning true if they pass and false if they fail. A return value of false guarantees that n is composite. A return value of true guarantees that |n| is prime if |n| < 3,317,044,064,679,887,385,961,981. For larger |n| a return value of true indicates that |n| has a high probability of being p
func isPrimeBigInt(n *big.Int) (result bool) {
	// Calculate the absolute value of n
	m := bigintfactory.New().Abs(n)
	// If |n| < 2^63 - 1 we can give an exact answer via an int64 computation
	if m.IsInt64() {
		result = IsPrimeInt64(m.Int64())
		goto finished
	}
	// If |n| is a Mersenne number, we know the answer
	if ok, l := isMersenneNumberBigInt(m); ok {
		result = isMersennePrime(l)
		goto finished
	}
	// If |n| < maxExactValueForPrime12 then we can give a precise answer using
	// the first 12 prime bases
	if m.Cmp(maxExactValueForPrime12) < 0 {
		result = isSPRPForSliceBigInt(m, []int64{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37})
		goto finished
	}
	// If |n| < maxExactValueForPrime13 then we can give a precise answer using
	// the first 13 prime bases
	if m.Cmp(maxExactValueForPrime13) < 0 {
		result = isSPRPForSliceBigInt(m, []int64{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41})
		goto finished
	}
	// Otherwise we return a probabilistic result
	result = m.ProbablyPrime(DefaultNumberOfMillerRabinTests)
finished:
	bigintfactory.Reuse(m)
	return
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsPrimeInt64 returns true iff the absolute value |n| is a prime.
func IsPrimeInt64(n int64) bool {
	// Move to the absolute value and deal with the easy cases
	if n < 0 {
		if n == math.MinInt64 {
			return false
		}
		n = -n
	}
	if n == 0 || n == 1 {
		return false
	} else if n%2 == 0 {
		return n == 2
	}
	// If |n| is small enough, look it up in the small primes cache
	if n <= maxSmallCachedPrime {
		return isInSmallPrimesTable(n)
	}
	// If |n| is a Mersenne number, we know the answer
	if ok, l := isMersenneNumberInt64(n); ok {
		return isMersennePrime(l)
	}
	// |n| should be 2-SPRP and 3-SPRP
	if !isSPRP(n, 2) || !isSPRP(n, 3) {
		return false
	}
	// If |n| < 1,373,653 this suffices
	if n < 1373653 {
		return true
	}
	// |n| should be a 5-SPRP as well
	if !isSPRP(n, 5) {
		return false
	}
	// If |n| < 25,326,001 this suffices
	if n < 25326001 {
		return true
	}
	// 3,215,031,751 is composite
	if n == 3215031751 {
		return false
	}
	// |n| should be a 7-SPRP as well
	if !isSPRP(n, 7) {
		return false
	}
	// If |n| < 118,670,087,467 this suffices
	if n < 118670087467 {
		return true
	}
	// |n| should be an 11-SPRP as well
	if !isSPRP(n, 11) {
		return false
	}
	// If |n| < 2,152,302,898,747 this suffices
	if n < 2152302898747 {
		return true
	}
	// |n| should be a 13-SPRP as well
	if !isSPRP(n, 13) {
		return false
	}
	// If |n| < 3,474,749,660,383 this suffices
	if n < 3474749660383 {
		return true
	}
	// |n| should be a 17-SPRP as well
	if !isSPRP(n, 17) {
		return false
	}
	// If |n| < 341,550,071,728,321 this suffices
	if n < 341550071728321 {
		return true
	}
	// |n| is prime iff it is an a-SPRP for all primes a <= 37
	return isSPRPForSlice(n, []int64{19, 23, 29, 31, 37})
}

// IsPrime runs primality tests on the absolute value |n| of n, returning true if they pass and false if they fail. A return value of false guarantees that n is composite. A return value of true guarantees that |n| is prime if |n| < 3,317,044,064,679,887,385,961,981. For larger |n| a return value of true indicates that |n| has a high probability of being prime.
func IsPrime(n *Element) bool {
	if n.IsInt64() {
		return IsPrimeInt64(n.int64())
	}
	return isPrimeBigInt(n.bigInt())
}

// IsProbablyPrime returns false if n is definitely composite and returns true if the absolute value |n| of n has a high probability of being prime. It uses the ProbablyPrime() function from the Go big library, so per the documentation performs N Miller--Rabin tests.
func IsProbablyPrime(n *Element, N int) bool {
	return n.bigInt().ProbablyPrime(N)
}

// IsPrime runs primality tests on the absolute value |n| of n, returning true if they pass and false if they fail. A return value of false guarantees that n is composite. A return value of true guarantees that |n| is prime if |n| < 3,317,044,064,679,887,385,961,981. For larger |n| a return value of true indicates that |n| has a high probability of being prime.
func (n *Element) IsPrime() bool {
	return IsPrime(n)
}
