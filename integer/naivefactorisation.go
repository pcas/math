// Naive factorisation algorithm using successive division by primes.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcastools/fatal"
	"math"
)

// floorSqrts is a look-up table such that the i-th entry is the floor of the square-root of 2^i - 1.
var floorSqrts = [...]int64{0, 1, 1, 2, 3, 5, 7, 11, 15, 22, 31, 45, 63, 90,
	127, 181, 255, 362, 511, 724, 1023, 1448, 2047, 2896, 4095, 5792, 8191,
	11585, 16383, 23170, 32767, 46340, 65535, 92681, 131071, 185363, 262143,
	370727, 524287, 741455, 1048575, 1482910, 2097151, 2965820, 4194303,
	5931641, 8388607, 11863283, 16777215, 23726566, 33554431, 47453132,
	67108863, 94906265, 134217727, 189812531, 268435455, 379625062, 536870911,
	759250124, 1073741823, 1518500249, 2147483647, 3037000499}

////////////////////////////////////////////////////////////////////////////////
// Local functions
////////////////////////////////////////////////////////////////////////////////

// indexForFloorSqrtsTable returns the smallest n such that 2^n - 1 >= k. Here k is assumed to be a non-negative integer.
func indexForFloorSqrtsTable(k int64) int {
	// Get the edge case out of the way
	if k == math.MaxInt64 {
		return 63
	}
	// Add one to k and start bit shifting
	n := uint(1)
	l := (k + 1) >> 1
	for l != 0 {
		n++
		l = l >> 1
	}
	if (1<<n)-1 < k {
		n++
	}
	return int(n)
}

// floorSqrt returns the largest integer k such that k * k <= n. Here n is assumed to be a non-negative integer.
func floorSqrt(k int64) int64 {
	// Get the easy cases out of the way
	if k == 0 {
		return 0
	} else if k < 4 {
		return 1
	}
	// We do a binary search -- set the initial range
	idx := indexForFloorSqrtsTable(k)
	var bottom int64
	if idx > 0 {
		bottom = floorSqrts[idx-1]
	}
	top := floorSqrts[idx]
	// Perform a binary search to find our square-root
	var middle, middle2 int64
	for top > bottom {
		middle = top + bottom
		if middle%2 == 0 {
			middle /= 2
		} else {
			middle = middle/2 + 1
		}
		middle2 = middle * middle
		switch {
		case middle2 < k:
			bottom = middle
		case middle2 == k:
			return middle
		case middle2 > k:
			top = middle - 1
		}
	}
	// Return the square-root
	if k < middle2 {
		return middle - 1
	}
	return middle
}

// naivePrimeFactorsInt64StartingFrom returns prime factors of n as a sorted slice, starting from p (inclusive). Here n is assumed to be a positive integer, p is assumed to be a (positive) prime, and n is assumed to have no prime factors < p.
func naivePrimeFactorsInt64StartingFrom(n int64, p int64) []int64 {
	// Is n prime?
	if IsPrimeInt64(n) {
		return []int64{n}
	}
	// Result will hold the prime factors (with repetition)
	result := make([]int64, 0, 5)
	// Calculate the floor of the square root of n
	sqrt := floorSqrt(n)
	// Loop until done
	for p <= sqrt {
		// Remove factors of p
		modified := false
		for n%p == 0 {
			n = n / p
			result = append(result, p)
			modified = true
		}
		// If we removed factors, we need to do some work
		if modified {
			// Re-calculate the floor of the square root of n
			sqrt = floorSqrt(n)
			// Perhaps n is now prime (or a unit)?
			if p > sqrt || IsPrimeInt64(n) {
				if n != 1 {
					result = append(result, n)
				}
				return result
			}
		}
		// Move on to the next prime
		var err error
		if p, err = NextPrimeInt64(p); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	}
	// If we're here then n must be prime (or a unit)
	if n != 1 {
		result = append(result, n)
	}
	return result
}
