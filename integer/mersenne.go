// Mersenne contains functions for factorising numbers of the form 2^k-1.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package integer

import (
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/fatal"
	"math"
	"math/big"
	"sort"
)

// mersennePrimePowers is a table of powers n for which 2^n - 1 is a known Mersenne prime. This table was last updated on 11 March 2016, and contains 49 powers.
var mersennePrimePowers = [...]int64{
	2, 3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607, 1279, 2203, 2281,
	3217, 4253, 4423, 9689, 9941, 11213, 19937, 21701, 23209, 44497, 86243,
	110503, 132049, 216091, 756839, 859433, 1257787, 1398269, 2976221, 3021377,
	6972593, 13466917, 20996011, 24036583, 25964951, 30402457, 32582657,
	37156667, 42643801, 43112609, 57885161, 74207281,
}

// mersenneFactorTable is a table of prime factors of 2^n - 1, for 2 <= n <= 63, where the i-th entry corresponds to the factorisation for the (n+2)-th exponent.
var mersenneFactorTable = [...][]int64{
	{3},
	{7},
	{3, 5},
	{31},
	{3, 3, 7},
	{127},
	{3, 5, 17},
	{7, 73},
	{3, 11, 31},
	{23, 89},
	{3, 3, 5, 7, 13},
	{8191},
	{3, 43, 127},
	{7, 31, 151},
	{3, 5, 17, 257},
	{131071},
	{3, 3, 3, 7, 19, 73},
	{524287},
	{3, 5, 5, 11, 31, 41},
	{7, 7, 127, 337},
	{3, 23, 89, 683},
	{47, 178481},
	{3, 3, 5, 7, 13, 17, 241},
	{31, 601, 1801},
	{3, 2731, 8191},
	{7, 73, 262657},
	{3, 5, 29, 43, 113, 127},
	{233, 1103, 2089},
	{3, 3, 7, 11, 31, 151, 331},
	{2147483647},
	{3, 5, 17, 257, 65537},
	{7, 23, 89, 599479},
	{3, 43691, 131071},
	{31, 71, 127, 122921},
	{3, 3, 3, 5, 7, 13, 19, 37, 73, 109},
	{223, 616318177},
	{3, 174763, 524287},
	{7, 79, 8191, 121369},
	{3, 5, 5, 11, 17, 31, 41, 61681},
	{13367, 164511353},
	{3, 3, 7, 7, 43, 127, 337, 5419},
	{431, 9719, 2099863},
	{3, 5, 23, 89, 397, 683, 2113},
	{7, 31, 73, 151, 631, 23311},
	{3, 47, 178481, 2796203},
	{2351, 4513, 13264529},
	{3, 3, 5, 7, 13, 17, 97, 241, 257, 673},
	{127, 4432676798593},
	{3, 11, 31, 251, 601, 1801, 4051},
	{7, 103, 2143, 11119, 131071},
	{3, 5, 53, 157, 1613, 2731, 8191},
	{6361, 69431, 20394401},
	{3, 3, 3, 3, 7, 19, 73, 87211, 262657},
	{23, 31, 89, 881, 3191, 201961},
	{3, 5, 17, 29, 43, 113, 127, 15790321},
	{7, 32377, 524287, 1212847},
	{3, 59, 233, 1103, 2089, 3033169},
	{179951, 3203431780337},
	{3, 3, 5, 5, 7, 11, 13, 31, 41, 61, 151, 331, 1321},
	{2305843009213693951},
	{3, 715827883, 2147483647},
	{7, 7, 73, 127, 337, 92737, 649657},
}

// maxTestedMersennePower is such that for any n < maxTestedMersennePower, with n not in mersennePrimePowers, it is known that 2^n - 1 is not prime. Note that this value is often smaller than the largest known Mersenne prime power -- there may well be gaps in the mersennePrimePowers table.
const maxTestedMersennePower = 32582657

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isLucasLehmer performs the Lucas–-Lehmer primality test on the prime exponent p, returning true iff 2^p - 1 is prime. Assumes that p is an odd prime.
func isLucasLehmer(p int64) bool {
	s := bigintfactory.New().SetInt64(4)
	M := toMersenneNumberBigInt(p)
	two := bigintfactory.New().SetInt64(2)
	for i := int64(0); i < p-2; i++ {
		s.Exp(s, two, M)
		if s.Cmp(two) >= 0 {
			s.Sub(s, two)
		} else if s.Cmp(oneAsBigInt) == 0 {
			s.Sub(M, oneAsBigInt)
		} else {
			s.Sub(M, two)
		}
	}
	bigintfactory.Reuse(M)
	bigintfactory.Reuse(two)
	result := s.BitLen() == 0
	bigintfactory.Reuse(s)
	return result
}

// sumOfPowersOfTwoBigInt returns the sum 1 + 2^a + 2^{2a} + ... + 2^{ka}. Here a is assumed to be a positive integer, and k a non-negative integer.
func sumOfPowersOfTwoBigInt(a int64, k int64) *big.Int {
	if a <= 0 {
		panic("Argument 1 should be a positive integer")
	} else if k < 0 {
		panic("Argument 2 should be a non-negative integer")
	}
	sum := bigintfactory.New().SetInt64(1)
	powtwo := bigintfactory.New().SetInt64(1)
	for i := int64(0); i < k; i++ {
		powtwo.Lsh(powtwo, uint(a))
		sum.Add(sum, powtwo)
	}
	bigintfactory.Reuse(powtwo)
	return sum
}

// isMersenneNumberInt64 returns true iff k = 2^n - 1. If true, also returns the exponent n.
func isMersenneNumberInt64(k int64) (bool, int64) {
	// Check that k is non-negative
	if k < 0 {
		return false, 0
	}
	// Perform the check
	n := int64(0)
	for k != 0 {
		if k&1 != 1 {
			return false, 0
		}
		k >>= 1
		n++
	}
	return true, n
}

// isMersenneNumberBigInt returns true iff k = 2^n - 1. If true, also returns the exponent n.
func isMersenneNumberBigInt(k *big.Int) (bool, int64) {
	// Check that k is non-negative
	if k.Sign() < 0 {
		return false, 0
	}
	// If we can do this in terms of int64s, do that
	if k.IsInt64() {
		return isMersenneNumberInt64(k.Int64())
	}
	// Perform the check
	n := k.BitLen()
	for i := 0; i < n; i++ {
		if k.Bit(i) != 1 {
			return false, 0
		}
	}
	return true, int64(n)
}

// isMersenneNumber returns true iff k = 2^n - 1. If true, also returns the exponent n.
func isMersenneNumber(k *Element) (bool, int64) {
	if k.IsInt64() {
		return isMersenneNumberInt64(k.int64())
	}
	return isMersenneNumberBigInt(k.bigInt())
}

// isInMersennePrimePowers returns true iff n is contained in the table mersennePrimePowers of known Mersenne prime exponents.
func isInMersennePrimePowers(n int64) bool {
	if n < 2 || n > mersennePrimePowers[len(mersennePrimePowers)-1] {
		return false
	}
	// Perform a binary search on the table for the index of n
	idx := sort.Search(len(mersennePrimePowers), func(i int) bool { return mersennePrimePowers[i] >= n })
	return mersennePrimePowers[idx] == n
}

// isMersennePrime returns true iff k = 2^n - 1 is prime. n is assumed to be a non-negative integer.
func isMersennePrime(n int64) bool {
	// Is n one of the known exponents for a Mersenne prime?
	if isInMersennePrimePowers(n) {
		return true
	}
	// If n is small enough, it might be known that k cannot be prime. If n is
	// composite, k cannot be prime.
	if n < maxTestedMersennePower || !IsPrimeInt64(n) {
		return false
	}
	// Perform the Lucas–-Lehmer primality test on the exponent n
	return isLucasLehmer(n)
}

// toMersenneNumberBigInt returns the Mersenne number k = 2^n - 1, where n is assumed to be a non-negative integer.
func toMersenneNumberBigInt(n int64) *big.Int {
	if n < 0 {
		panic("Argument should be a non-negative integer")
	} else if n == 0 {
		return bigintfactory.New().SetInt64(0)
	} else if n == 1 {
		return bigintfactory.New().SetInt64(1)
	}
	k := bigintfactory.New().Lsh(oneAsBigInt, uint(n))
	return k.Sub(k, oneAsBigInt)
}

// toMersenneNumber returns the Mersenne number k = 2^n - 1, where n is assumed to be a non-negative integer.
func toMersenneNumber(n int64) *Element {
	if n < 0 {
		panic("Argument should be a non-negative integer")
	} else if n == 0 {
		return Zero()
	} else if n == 1 {
		return One()
	}
	// Can we do this as an int64 computation?
	if n < 63 {
		return FromInt64((1 << uint(n)) - 1)
	} else if n == 63 {
		return FromInt64(math.MaxInt64)
	}
	// No luck -- do this as a *big.Int computation
	k := bigintfactory.New().Lsh(oneAsBigInt, uint(n))
	k.Sub(k, oneAsBigInt)
	return fromBigIntAndReuse(k)
}

// mersennePrimeFactors returns (true,F) where F is a slice of prime factors (in increasing order) for k = 2^n - 1, if this is easy to calculate. Otherwise returns (false,nil). Here n is assumed to be a positive integer.
func mersennePrimeFactors(n int64) (bool, []*Element) {
	// Sanity check
	if n <= 0 {
		panic("Argument should be a positive integer")
	}
	// Get the trivial case out of the way
	if n == 1 {
		return true, []*Element{}
	}
	// Is the factorisation recorded in the mersenneFactorTable?
	if n-2 < int64(len(mersenneFactorTable)) {
		I := mersenneFactorTable[int(n-2)]
		Fs := make([]*Element, 0, len(I))
		for _, p := range I {
			Fs = append(Fs, FromInt64(p))
		}
		return true, Fs
	}
	// Is n one of the known exponents for a Mersenne prime?
	if isInMersennePrimePowers(n) {
		return true, []*Element{toMersenneNumber(n)}
	}
	// Compute the factorisation of n
	Fs, err := PrimeFactorsInt64(n)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// If n is prime then we're stuck...
	if len(Fs) <= 1 {
		return false, nil
	}
	// ...otherwise recursively compute the factorisation
	return true, mersennePrimeFactorsInternal(n, Fs)
}

// mersenneRemainderFactorsToChannelInternal is used by mersennePrimeFactorsInternal to recursively factor the remainder of a Mersenne number. See mersennePrimeFactorsInternal for details. The resulting factors are fed down the given channel, which will be closed on completion.
func mersenneRemainderFactorsToChannelInternal(a int64, k int64, res chan<- *Element) {
	// Calculate the factors of c = 1 + 2^a + 2^{2a} + ... + 2^{ka}
	c := sumOfPowersOfTwoBigInt(a, k)
	factors, err := PrimeFactors(fromBigIntAndReuse(c))
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	// Feed the factors down the communication channel
	for _, d := range factors {
		res <- d
	}
	// Close the channel
	close(res)
}

// mersennePrimeFactorsInternal uses the fact that
//    m | n implies that 2^m - 1 | 2^n - 1
// to recursively compute a factorisation of 2^n - 1. Here Fs is a slice of prime factors of n, assumed to be in increasing order.
func mersennePrimeFactorsInternal(n int64, Fs []int64) []*Element {
	// If n is prime we do this via PrimeFactors
	if len(Fs) == 1 {
		nFs, err := PrimeFactors(toMersenneNumber(Fs[0]))
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return nFs
	}
	// Fix our notation: We factor 2^n - 1 = b * c, where
	//	 m = p2...pk,
	//   b = 2^m - 1, and
	//   c = 1 + 2^m + 2^{2m} + ... + 2^{(p1 - 1)m}.
	// Start a go routine computing the factors of c.
	m := n / Fs[0]
	cChan := make(chan *Element)
	go mersenneRemainderFactorsToChannelInternal(m, Fs[0]-1, cChan)
	// Calculate the factors of b
	var bFs []*Element
	if len(Fs) == 2 {
		var err error
		if bFs, err = PrimeFactors(toMersenneNumber(m)); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	} else {
		bFs = mersennePrimeFactorsInternal(m, Fs[1:])
	}
	// Merge the factors
	factors := make([]*Element, 0, len(bFs))
	okb := true
	nextb := bFs[0]
	idx := 1
	nextc, okc := <-cChan
	for okb && okc {
		if nextb.IsLessThan(nextc) {
			factors = append(factors, nextb)
			if idx < len(bFs) {
				nextb = bFs[idx]
				idx++
			} else {
				okb = false
			}
		} else {
			factors = append(factors, nextc)
			nextc, okc = <-cChan
		}
	}
	// Append any remaining factors
	if okb {
		factors = append(factors, nextb)
		factors = append(factors, bFs[idx:]...)
	}
	if okc {
		factors = append(factors, nextc)
		for d := range cChan {
			factors = append(factors, d)
		}
	}
	// Return the factors
	return factors
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// IsMersenneNumber returns true iff k = 2^n - 1 for some non-negative integer n. If true, also returns n.
func IsMersenneNumber(k *Element) (bool, *Element) {
	if ok, n := isMersenneNumber(k); ok {
		return true, FromInt64(n)
	}
	return false, nil
}
