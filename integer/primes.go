// Primes contains functions for primality testing and for generating primes.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

/*
FIX ME TODO: what about negative inputs?  adjust documentation accordingly
remember that prime-factorizing 1 should return an empty slice
remember that taking the divisors of n should include 1, so 1 gives [1] and 2 gives [1,2]
what happens if I prime factor zero?
*/
/////////////////////////////////////////////////////////////////////////
// primes.go
/////////////////////////////////////////////////////////////////////////

package integer

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer/internal/bigintfactory"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/mathutil"
	"math"
	"math/big"
	"runtime"
)

// maxLenGCD is the maximum size of a slice before we do a recursive split when computing the GCD.
const maxLenGCD = 500

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setBigIntToAbs sets n to the absolute value of m, and returns n.
func setBigIntToAbs(n *big.Int, m *Element) *big.Int {
	if m.IsInt64() {
		return n.Abs(n.SetInt64(m.int64()))
	}
	return n.Abs(m.bigInt())
}

/////////////////////////////////////////////////////////////////////////
// GCD functions
/////////////////////////////////////////////////////////////////////////

// gcdForSliceAsBigIntInternal calculates the gcd for the given slice S and passes the result down the given channel.
func gcdForSliceAsBigIntInternal(c chan<- *big.Int, S []*Element) {
	c <- gcdForSliceAsBigInt(S)
}

// gcdForSliceAsBigInt calculates the gcd for the given slice S.
func gcdForSliceAsBigInt(S []*Element) *big.Int {
	// If the slice is small enough, we do this without recursion
	if len(S) <= maxLenGCD {
		gcd := bigintfactory.New()
		if len(S) == 0 {
			return gcd.SetInt64(0)
		} else if len(S) == 1 {
			return setBigIntToAbs(gcd, S[0])
		}
		x := bigintfactory.New()
		setBigIntToAbs(gcd, S[0])
		for _, a := range S[1:] {
			if gcd.Cmp(oneAsBigInt) == 0 {
				return gcd
			} else if !a.IsZero() {
				if gcd.BitLen() == 0 {
					setBigIntToAbs(gcd, a)
				} else {
					gcd.GCD(nil, nil, gcd, setBigIntToAbs(x, a))
				}
			}
		}
		bigintfactory.Reuse(x)
		return gcd
	}
	// Split the slice and calculate the gcds recursively
	n := runtime.GOMAXPROCS(0)
	if n == 1 {
		n = 2
	}
	length := len(S) / n
	c := make(chan *big.Int)
	for i := 0; i < n; i++ {
		go gcdForSliceAsBigIntInternal(c, S[i*length:(i+1)*length])
	}
	if n*length != len(S) {
		go gcdForSliceAsBigIntInternal(c, S[n*length:])
		n++
	}
	// Calculate the gcd of the returned gcds
	isOne := false
	gcd := <-c
	for i := 1; i < n; i++ {
		x := <-c
		if !isOne {
			if gcd.Cmp(oneAsBigInt) == 0 {
				isOne = true
			} else if x.BitLen() != 0 {
				if gcd.BitLen() == 0 {
					gcd.Set(x)
				} else {
					gcd.GCD(nil, nil, gcd, x)
				}
			}
		}
	}
	return gcd
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// UniquePrimeFactors returns the prime factors of a non-zero integer n as a sorted slice of *Elements, without repeats, or an error if n is zero.
func UniquePrimeFactors(n *Element) ([]*Element, error) {
	// Sanity check
	if n.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	}
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// PrimeFactorsInt64 returns the prime factors of |n| as a sorted slice. It returns an error if and only if n is zero.
func PrimeFactorsInt64(n int64) ([]int64, error) {
	// Sanity check
	if n == 0 {
		return nil, errors.IllegalZeroArg.New()
	}
	// Get the negative special case out of the way, and then pass to the
	// absolute value
	if n == math.MinInt64 {
		result := make([]int64, 0, 63)
		for i := 0; i < 63; i++ {
			result = append(result, 2)
		}
		return result, nil
	} else if n < 0 {
		n = -n
	}
	// Deal with 1
	if n == 1 {
		return []int64{}, nil
	}
	// If |n| is a Mersenne number, we know the answer
	if ok, k := isMersenneNumberInt64(n); ok {
		return copySliceInt64(mersenneFactorTable[int(k-2)]), nil
	}
	// Run the factorisation algorithm
	return naivePrimeFactorsInt64StartingFrom(n, 2), nil
}

// PrimeFactors returns the prime factors of |n| as a sorted slice. It returns an error if and only if n is zero. Primality testing is done by the IsPrime function, which in particular is probabilistic for large arguments.
func PrimeFactors(n *Element) ([]*Element, error) {
	// Get the zero and int64 cases out of the way
	if n.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	} else if n.IsInt64() {
		results, err := PrimeFactorsInt64(n.int64())
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		return SliceFromInt64Slice(results), nil
	}
	// Pass to the absolute value
	n = n.Abs()
	// Is n prime?
	if IsPrime(n) {
		return []*Element{n}, nil
	}
	// Is n a Mersenne number?
	if ok, k := IsMersenneNumber(n); ok && k.IsInt64() {
		if ok, results := mersennePrimeFactors(k.int64()); ok {
			return results, nil
		}
	}
	// Results will hold the prime factors, with repetitions.
	results := make([]*Element, 0, 5)
	// Out strategy is naive: work up through the primes in order, dividing as
	// we go.
	useNthPrime := true
	N := int64(1)
	var p *Element
	for {
		// We use NthPrime for as long as we can to recover the next prime
		if useNthPrime {
			if nextp, err := NthPrime(N); err != nil {
				useNthPrime = false
				p = NextPrime(p)
			} else {
				p = nextp
				N++
			}
		} else {
			p = NextPrime(p)
		}
		// At this point we have out candidate prime factor p. If p is too big
		// then n must be prime.
		pSquared := p.ScalarMultiplyByInteger(p)
		if pSquared.IsGreaterThan(n) {
			if !n.IsOne() {
				results = append(results, n)
			}
			return results, nil
		}
		// Remove as many factors of p as we can
		modified := false
		isDivisible, err := n.IsDivisibleBy(p)
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		for isDivisible {
			if n, err = Quotient(n, p); err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
			results = append(results, p)
			modified = true
			if isDivisible, err = n.IsDivisibleBy(p); err != nil {
				panic(fatal.ImpossibleError(err)) // This should never happen
			}
		}
		// If we removed a factor of p from n, we can check whether n is now
		// small enough to simplify this calculation
		if modified {
			// If p is too big, then n must be prime
			if pSquared.IsGreaterThan(n) {
				if !n.IsOne() {
					results = append(results, n)
				}
				return results, nil
			}
			// If n is now prime, return
			if IsPrime(n) {
				results = append(results, n)
				return results, nil
			}
			// If n is now small enough, drop into the PrimeFactorsInt64 code
			if n.IsInt64() {
				return append(results, SliceFromInt64Slice(naivePrimeFactorsInt64StartingFrom(n.int64(), NextPrime(p).int64()))...), nil
			}
		}
	}
}

// Divisors returns the divisors of a non-zero integer n as a sorted slice of *Elements.
func Divisors(n *Element) ([]*Element, error) {
	// The argument must be non-zero
	if n.IsZero() {
		return nil, errors.IllegalZeroArg.New()
	}
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// AreCoprime returns true iff a and b are coprime.
func AreCoprime(a *Element, b *Element) bool {
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// IsSliceCoprime returns true iff the elements of the slice are coprime.
func IsSliceCoprime(as []*Element) (bool, error) {
	// The slice can't be empty
	if len(as) == 0 {
		return false, errors.EmptySlice.New()
	}
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// IsSlicePairwiseCoprime returns true iff the elements of the slice are pairwise coprime.
func IsSlicePairwiseCoprime(as []*Element) (bool, error) {
	// The slice can't be empty
	if len(as) == 0 {
		return false, errors.EmptySlice.New()
	}
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// UniqueCommonPrimeFactors returns the common prime factors of a and b as a sorted slice of *Elements, without repeats.
func UniqueCommonPrimeFactors(a *Element, b *Element) []*Element {
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// CommonPrimeFactors returns the common prime factors of a and b as a sorted slice of *Elements.
func CommonPrimeFactors(a *Element, b *Element) []*Element {
	// FIX ME write this
	panic(fatal.NotImplemented())
}

// GCD returns the greatest common divisor of a1, a2, ..., ak. The gcd is always non-negative. The gcd of 0 and n is defined to be |n| (in particular, the gcd of 0 is 0, and the gcd of the empty sequence is 0).
func GCD(ai ...*Element) *Element {
	// Get the easy cases out of the way
	if len(ai) == 0 {
		return Zero()
	} else if len(ai) == 1 {
		return ai[0].Abs()
	} else if len(ai) == 2 {
		return GCDOfPair(ai[0], ai[1])
	}
	// Calculate the gcd
	return fromBigIntAndReuse(gcdForSliceAsBigInt(ai))
}

// GCDOfPair returns the greatest common divisor of a and b. The gcd is always non-negative. The gcd of 0 and n is defined to be |n| (in particular, the gcd of 0 is 0). If you also require integers u and v such that u a + v y = gcd, then use XGCDOfPair.
func GCDOfPair(a *Element, b *Element) *Element {
	// Get the easy cases out of the way
	if a.IsZero() {
		return b.Abs()
	} else if b.IsZero() {
		return a.Abs()
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() {
		if aa, bb := a.int64(), b.int64(); aa != math.MinInt64 || bb != math.MinInt64 {
			gcd, _, _ := mathutil.XGCDOfPairInt64(aa, bb)
			return FromInt64(gcd)
		}
	}
	// No luck -- do this as a *big.Int computation.
	aa := setBigIntToAbs(bigintfactory.New(), a)
	bb := setBigIntToAbs(bigintfactory.New(), b)
	z := fromBigIntAndReuse(bigintfactory.New().GCD(nil, nil, aa, bb))
	bigintfactory.Reuse(aa)
	bigintfactory.Reuse(bb)
	return z
}

// XGCDOfPair returns the greatest common divisor of a and b, along with integers u and v such that u a + v b = gcd. The gcd is always non-negative. The gcd of 0 and n is defined to be |n| (in particular, the gcd of 0 and 0 is 0).
func XGCDOfPair(a *Element, b *Element) (gcd *Element, u *Element, v *Element) {
	// Get the easy cases out of the way
	if a.IsZero() {
		if b.IsZero() {
			return Zero(), Zero(), Zero()
		} else if b.IsNegative() {
			return b.Abs(), Zero(), FromInt64(-1)
		}
		return b, Zero(), One()
	} else if b.IsZero() {
		if a.IsNegative() {
			return a.Abs(), FromInt64(-1), Zero()
		}
		return a, One(), Zero()
	}
	// Can we do this as an int64 computation?
	if a.IsInt64() && b.IsInt64() {
		if aa, bb := a.int64(), b.int64(); aa != math.MinInt64 || bb != math.MinInt64 {
			gcd, u, v := mathutil.XGCDOfPairInt64(aa, bb)
			return FromInt64(gcd), FromInt64(u), FromInt64(v)
		}
	}
	// No luck -- do this as a *big.Int computation.
	uu := bigintfactory.New()
	vv := bigintfactory.New()
	aa := setBigIntToAbs(bigintfactory.New(), a)
	bb := setBigIntToAbs(bigintfactory.New(), b)
	ggcd := bigintfactory.New().GCD(uu, vv, aa, bb)
	bigintfactory.Reuse(aa)
	bigintfactory.Reuse(bb)
	// Fix the signs
	if a.IsNegative() {
		uu.Neg(uu)
	}
	if b.IsNegative() {
		vv.Neg(vv)
	}
	return fromBigIntAndReuse(ggcd), fromBigIntAndReuse(uu), fromBigIntAndReuse(vv)
}

// LCM returns the least common multiple of a and b.
func LCM(a *Element, b *Element) *Element {
	// FIX ME write this
	panic(fatal.NotImplemented())
}

/////////////////////////////////////////////////////////////////////////
// Element methods
/////////////////////////////////////////////////////////////////////////

// UniquePrimeFactors returns the prime factors of n as a sorted slice of *Elements, without repeats.
func (n *Element) UniquePrimeFactors() ([]*Element, error) {
	return UniquePrimeFactors(n)
}

// PrimeFactors returns the prime factors of n as a sorted slice of *Elements.
func (n *Element) PrimeFactors() ([]*Element, error) {
	return PrimeFactors(n)
}

// Divisors returns the divisors of n as a sorted slice of *Elements.
func (n *Element) Divisors() ([]*Element, error) {
	return Divisors(n)
}
