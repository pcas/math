// Tests for chineseremainder.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/integer"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

type testCRT struct {
	residues []int64
	moduli   []int64
	result   string
}

// TestChineseRemainder tests ChineseRemainder
func TestChineseRemainder(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// Some straightforward tests
	tests := []testCRT{
		{
			residues: []int64{2, 4, 6},
			moduli:   []int64{3, 5, 7},
			result:   "104",
		},
		{
			residues: []int64{114703925764141624, 3297487825940013106},
			moduli:   []int64{242323627012434073, 8881582859973419789},
			result:   "524084001448375425938536378217686942",
		},
		{
			residues: []int64{2425593234612630944, 4698972572119269613,
				3224370405824169309},
			moduli: []int64{8907085068174860783, 7718217414236791009,
				4164101401596744779},
			result: "99615563082047332669922718309223784498622617715238018224",
		},
		{
			residues: []int64{1152463212492613880, 4035231066366928665,
				485878538199030777, 1702212592458237262},
			moduli: []int64{8074975814572811357, 7303995980210163781,
				8870161078069213621, 2885344808758613081},
			result: "467224795935403185528613510463567888167860435254668754701534816141425325550",
		},
		{
			residues: []int64{287829467192593487, 4448126858511371964,
				4436651545031870978, 1776315456656030432, 101714410059981618},
			moduli: []int64{7010240934610414807, 9032296804724960831,
				5485345734295856263, 2156171813122848217, 907402527583107727},
			result: "317688533755936847267031165968902278587150832186758183021150918454077937057212226619938458066",
		},
	}
	for _, test := range tests {
		residues := make([]*Element, 0, len(test.residues))
		for i := 0; i < len(test.residues); i++ {
			k, err := GF(test.moduli[i])
			require.NoError(err)
			residues = append(residues, FromInt64(k, test.residues[i]))
		}
		if test.result == "error" {
			_, err := ChineseRemainderTheorem(residues)
			assert.Error(err)
		} else {
			expected, err := integer.FromString(test.result)
			require.NoError(err)
			result, err := ChineseRemainderTheorem(residues)
			assert.NoError(err)
			assert.True(expected.IsEqualTo(result))
		}
	}
}

// TestPrepareChineseRemainderTheorem tests PrepareChineseRemainderTheorem
func TestPrepareChineseRemainderTheorem(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// we test reconstructing 0..41 from their residues mod 2, 3, and 7
	//////////////////////////////////////////////////////////////////////
	gf2, err := GF(2)
	assert.NoError(err)
	gf3, err := GF(3)
	assert.NoError(err)
	gf7, err := GF(7)
	assert.NoError(err)
	residues2 := make([]*Element, 0, 42)
	residues3 := make([]*Element, 0, 42)
	residues7 := make([]*Element, 0, 42)
	for i := int64(0); i < 42; i++ {
		residues2 = append(residues2, FromInt64(gf2, i%2))
		residues3 = append(residues3, FromInt64(gf3, i%3))
		residues7 = append(residues7, FromInt64(gf7, i%7))
	}
	crtFunc, err := PrepareChineseRemainderTheorem(2, 3, 7)
	assert.NoError(err)
	for i := 0; i < 42; i++ {
		if result, err := crtFunc(residues2[i], residues3[i], residues7[i]); assert.NoError(err) {
			assert.True(result.IsEqualTo(integer.FromInt(i)))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test some failing cases
	//////////////////////////////////////////////////////////////////////
	// the parents should match
	_, err = crtFunc(residues2[0], residues7[0], residues7[0])
	assert.Error(err)
	// the number of residues should match the number of moduli
	_, err = crtFunc(residues2[0], residues3[0])
	assert.Error(err)
	// the moduli should be prime
	_, err = PrepareChineseRemainderTheorem(2, 6, 7)
	assert.Error(err)
	// the moduli should be distinct
	_, err = PrepareChineseRemainderTheorem(2, 3, 5, 7, 11, 5)
	assert.Error(err)
}
