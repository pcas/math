// Chineseremainder contains functions that implement the Chinese Remainder Theorem.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
)

// CRTFromFiniteFieldElementsFunc applies the Chinese Remainder Theorem to the given finite field elements. If the number of residues does not match the number of field elements expected, or if the characteristics of the fields do not match those expected, then an error is returned.
type CRTFromFiniteFieldElementsFunc func(residues ...*Element) (*integer.Element, error)

// ChineseRemainderTheorem returns the unique non-negative integer a such that 0 <= a < N and that a reduces to reductions. Here reductions  is a slice [a_1,...,a_n] of finite field elements where a_i is an element of GF(p_i) with p_i prime, and the p_i are distinct; the value N above is the product of the p_i. The return value a is congruent to a_i mod p_i, for each i.
func ChineseRemainderTheorem(reductions []*Element) (*integer.Element, error) {
	// sanity check
	for _, x := range reductions {
		// we need x to be an element of GF(p)
		k := x.Parent().(*Parent)
		if k.order != k.characteristic {
			return nil, errors.ArgNotASliceOfPrimeFieldElements.New()
		}
	}
	// package up the residues and moduli as integers
	residues := make([]*integer.Element, len(reductions))
	moduli := make([]*integer.Element, len(reductions))
	for i, x := range reductions {
		residues[i] = integer.FromInt64(x.n)
		moduli[i] = integer.FromInt64(x.p)
	}
	// hand off to the CRT code in the integer package
	return integer.ChineseRemainderTheorem(residues, moduli)
}

// PrepareChineseRemainderTheorem returns a function f that applies the Chinese Remainder Theorem to finite field elements a_1,...,a_k. More precisely, given elements a_1,...,a_k of GF(p_1),...,GF(p_k) where moduli=[p_1,...,p_k] and the primes p_1,...,p_k are distinct, f(a_1,...,a_k) returns the unique non-negative integer a such that 0 <= a < p_1*...*p_k and, for each i, n is congruent to a_i mod p_i.
func PrepareChineseRemainderTheorem(moduli ...int64) (CRTFromFiniteFieldElementsFunc, error) {
	// sanity checks
	for i, p := range moduli {
		// the moduli must be prime
		if !integer.IsPrimeInt64(p) {
			return nil, errors.ArgNotASliceOfPrimes.New()
		}
		// the moduli must be distinct
		for j := 0; j < i; j++ {
			if moduli[j] == p {
				return nil, errors.ArgsNotCoprime.New()
			}
		}
	}
	// convert the moduli to integers
	integerModuli := make([]*integer.Element, 0, len(moduli))
	for _, p := range moduli {
		integerModuli = append(integerModuli, integer.FromInt64(p))
	}
	// grab the integer CRT function
	integerCRTFunc, err := integer.PrepareChineseRemainderTheorem(integerModuli...)
	if err != nil {
		return nil, err
	}
	// wrap the integer CRT function and return
	return func(residues ...*Element) (*integer.Element, error) {
		// sanity checks
		if len(residues) != len(moduli) {
			return nil, errors.SliceLengthNotEqual.New()
		}
		for i, x := range residues {
			if x.p != moduli[i] {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
		// convert the residues to integers
		integerResidues := make([]*integer.Element, 0, len(residues))
		for _, res := range residues {
			integerResidues = append(integerResidues, integer.FromInt64(res.n))
		}
		// return the result
		return integerCRTFunc(integerResidues...)
	}, nil
}
