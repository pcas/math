// Tests for most functions in parent.go    Tests for Sum and Product are in sumproduct_test.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcas/math/testing/assert"
	"fmt"
	"math"
	"testing"
)

// Some test primes
const (
	bigprime    = math.MaxInt64 - 24  // bigprime is the largest prime that fits in an int64
	mediumprime = 1122331314832901153 // mediumprime is a prime of size roughly 2^62
	smallprime  = 24979               // smallprime is a prime of size roughly 2^16
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGF tests the creation function GF(p), Order, and Characteristic
func TestGF(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test
	ps := []int64{smallprime, mediumprime, bigprime}
	// do this twice to test the caching
	for i := 0; i < 2; i++ {
		for _, p := range ps {
			k, err := GF(p)
			assert.NoError(err)
			k.String()
		}
	}
	var k *Parent
	var err error
	// GF(n) should fail if n is negative
	k, err = GF(-2)
	if !assert.Error(err) {
		k.String()
	}
	// same again
	k, err = GF(math.MinInt64)
	assert.Error(err)
	// GF(n) should fail if n is not a prime power
	k, err = GF(6)
	assert.Error(err)
	// GF(p^k) should fail if k is not 1, because we haven't implemented the prime-power case yet
	k, err = GF(8)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for Order and Characteristic
	//////////////////////////////////////////////////////////////////////
	for _, p := range ps {
		k, err = GF(p)
		assert.NoError(err)
		if char, err := k.Characteristic().Int64(); assert.NoError(err) {
			assert.True(char == p)
		}
		assert.True(k.CharacteristicInt64() == p)
		if order, err := k.Order().Int64(); assert.NoError(err) {
			assert.True(order == p)
		}
		assert.True(k.OrderInt64() == p)
	}
	// test the nil cases
	var nilParent *Parent
	assert.True(nilParent.Characteristic() == integer.One())
	assert.True(nilParent.Order() == integer.One())
	assert.True(nilParent.CharacteristicInt64() == 1)
	assert.True(nilParent.OrderInt64() == 1)
	// test GF(1)
	k, err = GF(1)
	assert.NoError(err)
	assert.True(k.Characteristic() == integer.One())
	assert.True(k.Order() == integer.One())
}

// TestString tests the string representation of finite fields
func TestString(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test
	ps := []int64{smallprime, mediumprime, bigprime}
	for _, p := range ps {
		if k, err := GF(p); assert.NoError(err) {
			assert.Equal(k.String(), fmt.Sprintf("Finite field of order %v.", p))
		}
	}
	// test the nil case
	var nilParent *Parent
	assert.Equal(nilParent.String(), "Finite field of order 1.")
}

// TestZero tests basic things about zero
func TestZero(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test
	gfs := make([]*Parent, 3)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	// zero should print as 0, and be zero, and be contained within its parent field
	var zero *Element
	for _, k := range gfs {
		zero = k.zero
		assert.Equal("0", zero.String())
		assert.Equal(zero, k.Zero())
		assert.True(zero.IsZero())
		assert.True(k.IsZero(zero))
	}
	// zero of one finite field should only be contained in that finite field and the field with one element
	zero1 := gfs[1].Zero()
	zero2 := gfs[2].Zero()
	assert.True(gfs[1].Contains(zero1))
	assert.False(gfs[2].Contains(zero1))
	assert.False(gfs[1].Contains(zero2))
	assert.True(gfs[2].Contains(zero2))
	ok, err := gfs[1].IsZero(zero2)
	assert.False(ok)
	assert.Error(err)
}

// TestIsOne tests IsOne
func TestIsOne(t *testing.T) {
	assert := assert.New(t)
	var result bool
	var err error
	// set up the fields
	k1, err := GF(smallprime)
	assert.NoError(err)
	k2, err := GF(bigprime)
	assert.NoError(err)
	// run the tests
	result, err = k1.IsOne(k1.One())
	assert.NoError(err)
	assert.True(result)
	result, err = k1.IsOne(k1.Zero())
	assert.NoError(err)
	assert.False(result)
	_, err = k1.IsOne(k2.One())
	assert.Error(err)
}

// TestToElement tests ToElement
func TestToElement(t *testing.T) {
	assert := assert.New(t)
	// set up the fields we use
	gf2, err := GF(2)
	assert.NoError(err)
	gf3, err := GF(3)
	assert.NoError(err)
	// 15/2 is zero in GF(3) and ill-defined in GF(2)
	rat, err := rational.FromString("15/2")
	assert.NoError(err)
	_, err = ToElement(gf2, rat)
	assert.Error(err)
	if result, err := gf3.ToElement(rat); assert.NoError(err) {
		assert.True(gf3.IsZero(result))
	}
	// -81 is zero in GF(3) and one in GF(2)
	minusEightyOne := integer.FromInt64(-81)
	if result, err := gf3.ToElement(minusEightyOne); assert.NoError(err) {
		assert.True(gf3.IsZero(result))
	}
	if result, err := gf2.ToElement(minusEightyOne); assert.NoError(err) {
		assert.True(gf2.IsOne(result))
	}
}

// TestFromParent tests FromRational and FromInt in parent.go
func TestFromParent(t *testing.T) {
	assert := assert.New(t)
	// set up the fields we use
	gf2, err := GF(2)
	assert.NoError(err)
	gf3, err := GF(3)
	assert.NoError(err)
	// test FromInt
	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			assert.True(FromInt(gf2, i).IsZero())
		} else {
			assert.True(FromInt(gf2, i).IsOne())
		}
	}
	// 73/4 is ill-defined in GF(2) and 1 in GF(3)
	rat, err := rational.FromString("73/4")
	assert.NoError(err)
	_, err = gf2.FromRational(rat)
	assert.Error(err)
	if result, err := gf3.FromRational(rat); assert.NoError(err) {
		if ok, err := gf3.IsOne(result); assert.NoError(err) {
			assert.True(ok)
		}
	}
}

// TestAreEqual tests an error case in AreEqual in parent.go
func TestAreEqual(t *testing.T) {
	assert := assert.New(t)
	// set up the fields
	k, err := GF(mediumprime)
	assert.NoError(err)
	var nilParent *Parent
	// do the test
	_, err = k.AreEqual(One(k), Zero(nilParent))
	assert.Error(err)
}

// TestScalarMultiplyByInteger tests ScalarMultiplyByInteger
func TestScalarMultiplyByInteger(t *testing.T) {
	assert := assert.New(t)
	k, err := GF(5)
	assert.NoError(err)
	// We run over a range
	for i := 0; i < 5; i++ {
		x := k.FromInteger(integer.FromInt(i))
		for j := -11; j <= 11; j++ {
			if y, err := k.ScalarMultiplyByInteger(integer.FromInt(j), x); assert.NoError(err) {
				if ok, err := k.AreEqual(y, integer.FromInt((i*j)%5)); assert.NoError(err) {
					assert.True(ok)
				}
			}
		}
	}
	// Something not in the field should error
	if k2, err := GF(3); assert.NoError(err) {
		_, err = k.ScalarMultiplyByInteger(integer.One(), k2.One())
		assert.Error(err)
	}
}

// TestIsUnit tests IsUnit
func TestIsUnit(t *testing.T) {
	assert := assert.New(t)
	k, err := GF(7)
	assert.NoError(err)
	//////////////////////////////////////////////////////////////////////
	// a slice containing the elements of k
	//////////////////////////////////////////////////////////////////////
	elements := make([]*Element, 0, 7)
	for i := int64(0); i < 7; i++ {
		elements = append(elements, FromInt64(k, i))
	}
	//////////////////////////////////////////////////////////////////////
	// the tests
	//////////////////////////////////////////////////////////////////////
	if bool, _, err := k.IsUnit(elements[0]); assert.NoError(err) {
		assert.False(bool)
	}
	if bool, inv, err := k.IsUnit(elements[1]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[1])
	}
	if bool, inv, err := k.IsUnit(elements[2]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[4])
	}
	if bool, inv, err := k.IsUnit(elements[3]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[5])
	}
	if bool, inv, err := k.IsUnit(elements[4]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[2])
	}
	if bool, inv, err := k.IsUnit(elements[5]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[3])
	}
	if bool, inv, err := k.IsUnit(elements[6]); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(k, inv, elements[6])
	}
	wrongK, err := GF(3)
	assert.NoError(err)
	_, _, err = k.IsUnit(FromInt64(wrongK, 2))
	assert.Error(err)
	_, _, err = k.IsUnit(FromInt64(wrongK, 1))
	assert.Error(err)
	var nilElement *Element
	_, _, err = k.IsUnit(nilElement)
	assert.Error(err)
	//////////////////////////////////////////////////////////////////////
	// tests for GF(1)
	//////////////////////////////////////////////////////////////////////
	var nilParent *Parent
	gf1, err := GF(1)
	assert.NoError(err)
	if bool, inv, err := gf1.IsUnit(gf1.Zero()); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(gf1, inv, gf1.Zero())
	}
	if bool, inv, err := nilParent.IsUnit(nilParent.Zero()); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(nilParent, inv, nilParent.Zero())
	}
	if bool, inv, err := gf1.IsUnit(nilElement); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(gf1, inv, gf1.Zero())
	}
	if bool, inv, err := nilParent.IsUnit(nilElement); assert.NoError(err) {
		assert.True(bool)
		assert.AreEqual(nilParent, inv, nilParent.Zero())
	}
}
