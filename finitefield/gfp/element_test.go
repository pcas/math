// Tests for most functions in element.go     Tests for Sum and Product are in sumproduct_test.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/rational"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestFrom tests the conversions from *integer.Elements, int64s, and rationals
func TestFrom(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make the GF(p)'s we test
	//////////////////////////////////////////////////////////////////////
	gfs := make([]*Parent, 3)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	//////////////////////////////////////////////////////////////////////
	// the nil *Parent, representing the field with one element
	//////////////////////////////////////////////////////////////////////
	var nilParent *Parent
	//////////////////////////////////////////////////////////////////////
	// tests of FromInteger and FromInt64
	//////////////////////////////////////////////////////////////////////
	// -3 as a *integer.Element
	minusThree := integer.FromInt64(-3)
	// we test powers of -3, including some that would overflow an int64
	var bigint *integer.Element
	var modp *Element
	for i := 0; i <= 50; i++ {
		bigint, err = minusThree.PowerInt64(int64(i))
		assert.NoError(err)
		for _, k := range gfs {
			modp, err = PowerInt64(FromInt64(k, -3), int64(i))
			assert.NoError(err)
			assert.True(k.AreEqual(FromInteger(k, bigint), modp))
			// now the same again, but with FromInt not FromInteger
			modp, err = PowerInt64(FromInt(k, -3), int64(i))
			assert.NoError(err)
			assert.True(k.AreEqual(FromInteger(k, bigint), modp))
		}
	}
	// check the zero case
	for _, k := range gfs {
		assert.True(k.AreEqual(FromInteger(k, integer.Zero()), k.Zero()))
	}
	// check the nil case
	assert.Equal(Zero(nilParent), nilParent.FromInteger(integer.FromInt64(3)))
	assert.Equal(Zero(nilParent), FromInt64(nilParent, -21))
	//////////////////////////////////////////////////////////////////////
	// tests of FromInt64
	//////////////////////////////////////////////////////////////////////
	for _, k := range gfs {
		for i := int64(0); i < 100; i++ {
			assert.Equal(FromInt64(k, i).String(), strconv.Itoa(int(i)))
		}
	}
	for _, k := range gfs {
		for i := int64(-100); i < 0; i++ {
			assert.Equal(FromInt64(k, i).String(), strconv.Itoa(int(k.CharacteristicInt64())+int(i)))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests of FromUint64
	//////////////////////////////////////////////////////////////////////
	for _, k := range gfs {
		for i := uint64(0); i < 100; i++ {
			assert.Equal(FromUint64(k, i).String(), strconv.Itoa(int(i)))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// tests of FromInt32
	//////////////////////////////////////////////////////////////////////
	for _, k := range gfs {
		for i := int32(0); i < 100; i++ {
			assert.Equal(FromInt32(k, i).String(), strconv.Itoa(int(i)))
		}
	}
	for _, k := range gfs {
		for i := int32(-100); i < 0; i++ {
			assert.Equal(FromInt32(k, i).String(), strconv.Itoa(int(k.CharacteristicInt64())+int(i)))
		}
	}
	//////////////////////////////////////////////////////////////////////
	// test of FromRational
	//////////////////////////////////////////////////////////////////////
	// we test powers of -6/35
	rat, err := rational.FromString("-6/35")
	assert.NoError(err)
	for _, k := range gfs {
		modp, err = FromRational(k, rat)
		assert.NoError(err)
		for i := -50; i < 50; i++ {
			ratpower, err := rat.PowerInt64(int64(i))
			assert.NoError(err)
			modppower, err := modp.Power(integer.FromInt(i))
			assert.NoError(err)
			if val, err := FromRational(k, ratpower); assert.NoError(err) {
				if val2, err2 := k.AreEqual(val, modppower); assert.NoError(err2) {
					assert.True(val2)
				}
			}
		}
	}
	// check the zero case
	for _, k := range gfs {
		if z, err := FromRational(k, rational.Zero()); assert.NoError(err) {
			if val, err2 := k.AreEqual(z, k.Zero()); assert.NoError(err2) {
				assert.True(val)
			}
		}
	}
	// check the nil case
	if result, err := FromRational(nilParent, rational.One()); assert.NoError(err) {
		assert.Equal(result, Zero(nilParent))
	}
}

// TestZeroOne tests Zero and One
func TestZeroOne(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// make the GF(p)'s we test
	//////////////////////////////////////////////////////////////////////
	gfs := make([]*Parent, 3)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	//////////////////////////////////////////////////////////////////////
	// the nil *Parent and *Element
	//////////////////////////////////////////////////////////////////////
	var nilParent *Parent   // this represents the field with one element
	var nilElement *Element // this represents the unique element thereof
	//////////////////////////////////////////////////////////////////////
	// zero and one should not be equal, except in the field with one
	// element
	//////////////////////////////////////////////////////////////////////
	for _, k := range gfs {
		assert.False(Zero(k).IsEqualTo(One(k)))
	}
	assert.True(Zero(nilParent).IsEqualTo(One(nilParent)))
	//////////////////////////////////////////////////////////////////////
	// the nil *Element is only equal to zero and one in the field with
	// one element
	//////////////////////////////////////////////////////////////////////
	for _, k := range gfs {
		assert.False(Zero(k).IsEqualTo(nilElement))
		assert.False(One(k).IsEqualTo(nilElement))
	}
	assert.True(Zero(nilParent).IsEqualTo(nilElement))
	assert.True(One(nilParent).IsEqualTo(nilElement))

}

// TestMisc tests String, Hash, Parent, IsOne, and IsEqualTo
func TestMisc(t *testing.T) {
	assert := assert.New(t)
	//////////////////////////////////////////////////////////////////////
	// tests of String
	//////////////////////////////////////////////////////////////////////
	k, err := GF(mediumprime)
	assert.NoError(err)
	total := k.zero
	for i := 0; i < 10; i++ {
		assert.Equal(strconv.Itoa(i), total.String())
		total, err = Add(total, k.one)
		assert.NoError(err)
	}
	// test the nil case
	var nilElement *Element // this is nil
	assert.Equal("0", nilElement.String())
	//////////////////////////////////////////////////////////////////////
	// tests of Parent
	//////////////////////////////////////////////////////////////////////
	assert.Equal(k, k.one.Parent())
	assert.Equal(k, k.zero.Parent())
	assert.Equal(k, total.Parent())
	// test the nil case
	var nilParent *Parent
	assert.Equal(One(nilParent).Parent(), nilElement.Parent())
	//////////////////////////////////////////////////////////////////////
	// tests of IsOne
	//////////////////////////////////////////////////////////////////////
	assert.True(k.one.IsOne())
	assert.True(k.One().(*Element).IsOne())
	assert.False(k.zero.IsOne())
	assert.False(total.IsOne())
	//////////////////////////////////////////////////////////////////////
	// tests of IsEqualTo
	//////////////////////////////////////////////////////////////////////
	var xx *Element // this is nil
	assert.False(k.zero.IsEqualTo(xx))
	assert.False(xx.IsEqualTo(k.one))
	// elements of different GF(p)'s are automatically unequal
	gf2, err := GF(2)
	assert.NoError(err)
	assert.False(k.one.IsEqualTo(gf2.one))
	//////////////////////////////////////////////////////////////////////
	// tests of Hash
	//////////////////////////////////////////////////////////////////////
	// nil and zero should hash to the same thing
	var xxx *Element // this is nil
	assert.Equal(xxx.Hash(), k.zero.Hash())
}

// TestExpModInt64IntegerInt64 tests some special cases in expModInt64IntegerInt64
func TestExpModInt64IntegerInt64(t *testing.T) {
	assert := assert.New(t)
	// anything mod 1 is zero
	assert.True(expModInt64IntegerInt64(-237654, integer.FromInt64(81672345), 1) == 0)
	// (-4)^2 = 2 mod 7
	assert.True(expModInt64IntegerInt64(-4, integer.FromInt64(2), 7) == 2)
	// anything to the power 1 is itself
	assert.True(expModInt64IntegerInt64(923856, integer.One(), bigprime) == 923856)
}
