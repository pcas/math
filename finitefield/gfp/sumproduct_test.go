// Tests for the Sum and Product functions in parent.go and element.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/testing/assert"
	"testing"
)

// TestSum tests Sum. We sum [1..N] for various N
func TestSum(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test. We include the field with one element as the last entry in the slice
	gfs := make([]*Parent, 4)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	// we run tests for each field
	for _, k := range gfs {
		//////////////////////////////////////////////////////////////////////
		// test long slices
		//////////////////////////////////////////////////////////////////////
		for n := 1; n <= 5; n++ {
			// Create a long slice to sum
			S := make([]*Element, 0, 5000*n)
			for i := int64(1); i <= int64(cap(S)); i++ {
				S = append(S, FromInt64(k, i))
			}
			// Calculate the sum using Sum(k, S...) from element.go
			N := int64(len(S))
			if sum, err := Sum(k, S...); assert.NoError(err) {
				assert.True(sum.IsEqualTo(FromInt64(k, (N*(N+1))/2)))
			}
			// recreate the long slice as a slice of object.Elements
			SS, err := object.SliceToElementSlice(S)
			assert.NoError(err)
			// Calculate the sum using k.Sum(SS...) from parent.go
			if sum, err := k.Sum(SS...); assert.NoError(err) {
				assert.True(sum.(*Element).IsEqualTo(FromInt64(k, (N*(N+1))/2)))
			}
		}
		//////////////////////////////////////////////////////////////////////
		// Test the edge cases for small n
		//////////////////////////////////////////////////////////////////////
		var err error
		var sum *Element
		var sum2 object.Element
		// length-zero case (element.go)
		sum, err = Sum(k)
		assert.NoError(err)
		assert.True(sum.IsZero())
		// length-zero case (parent.go)
		sum2, err = k.Sum()
		assert.NoError(err)
		assert.True(sum2.(*Element).IsZero())
		// length-one case (element.go)
		sum, err = Sum(k, One(k))
		assert.NoError(err)
		assert.True(sum.IsOne())
		// length-one case (parent.go)
		sum2, err = k.Sum(One(k))
		assert.NoError(err)
		assert.True(sum2.(*Element).IsOne())
		// length-two case (element.go)
		sum, err = Sum(k, One(k), Zero(k))
		assert.NoError(err)
		assert.True(sum.IsOne())
		// length-two case (parent.go)
		sum2, err = k.Sum(One(k), Zero(k))
		assert.NoError(err)
		assert.True(sum2.(*Element).IsOne())
	}
	//////////////////////////////////////////////////////////////////////
	// test cases where the parents are mismatched
	//////////////////////////////////////////////////////////////////////
	k1, err := GF(smallprime)
	assert.NoError(err)
	k2, err := GF(bigprime)
	assert.NoError(err)
	var nilParent *Parent   // the field with one element
	var nilElement *Element // the element of the field with one element
	var junk object.Element
	// length-one cases
	_, err = Sum(k1, Zero(k2))
	assert.Error(err)
	_, err = Sum(k1, nilElement)
	assert.Error(err)
	_, err = k1.Sum(Zero(k2))
	assert.Error(err)
	_, err = k1.Sum(nilElement)
	assert.Error(err)
	_, err = nilParent.Sum(junk)
	assert.Error(err)
	// length-two cases
	_, err = Sum(k1, One(k1), Zero(k2))
	assert.Error(err)
	_, err = Sum(k1, One(k1), nilElement)
	assert.Error(err)
	_, err = k1.Sum(One(k1), Zero(k2))
	assert.Error(err)
	_, err = k1.Sum(nilElement, One(k1))
	assert.Error(err)
	_, err = nilParent.Sum(junk, One(nilParent))
	assert.Error(err)
	// length-three cases
	_, err = Sum(k1, One(k1), Zero(k2), Zero(k1))
	assert.Error(err)
	_, err = Sum(k1, nilElement, One(k1), Zero(k1))
	assert.Error(err)
	_, err = k1.Sum(One(k1), Zero(k2), Zero(k1))
	assert.Error(err)
	_, err = k1.Sum(One(k1), Zero(k1), nilElement)
	assert.Error(err)
	_, err = nilParent.Sum(One(nilParent), junk, Zero(nilParent))
	assert.Error(err)
}

// TestProduct tests Product. We take the product over [1..N] for various N
func TestProduct(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test. We include the field with one element as the last entry in the slice
	gfs := make([]*Parent, 4)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	// we run tests for each field
	for _, k := range gfs {
		//////////////////////////////////////////////////////////////////////
		// test long slices
		//////////////////////////////////////////////////////////////////////
		for n := 1; n <= 5; n++ {
			// Create a long slice to sum
			S := make([]*Element, 0, 5000*n)
			for i := int64(1); i <= int64(cap(S)); i++ {
				S = append(S, FromInt64(k, i))
			}
			// Calculate the product (using Product(k,S...) from element.go)
			N := int64(len(S))
			expected, err := integer.FactorialInt64(N)
			assert.NoError(err)
			if product, err := Product(k, S...); assert.NoError(err) {
				assert.True(product.IsEqualTo(FromInteger(k, expected)))
			}
			// recreate the slice as a slice of object.Elements
			SS, err := object.SliceToElementSlice(S)
			// recalculate the product (using k.Product(S...) from parent.go)
			assert.NoError(err)
			if product, err := k.Product(SS...); assert.NoError(err) {
				assert.True(product.(*Element).IsEqualTo(FromInteger(k, expected)))
			}
		}
		//////////////////////////////////////////////////////////////////////
		// Test the edge cases for small n
		//////////////////////////////////////////////////////////////////////
		var err error
		var product *Element
		var product2 object.Element
		// length-zero case (element.go)
		product, err = Product(k)
		assert.NoError(err)
		assert.True(product.IsOne())
		// length-zero case (parent.go)
		product2, err = k.Product()
		assert.NoError(err)
		assert.True(product2.(*Element).IsOne())
		// length-one case (element.go)
		product, err = Product(k, Zero(k))
		assert.NoError(err)
		assert.True(product.IsZero())
		// length-one case (parent.go)
		product2, err = k.Product(Zero(k))
		assert.NoError(err)
		assert.True(product2.(*Element).IsZero())
		// length-one case again (element.go)
		product, err = Product(k, One(k))
		assert.NoError(err)
		assert.True(product.IsOne())
		// length-one case again (parent.go)
		product2, err = k.Product(One(k))
		assert.NoError(err)
		assert.True(product2.(*Element).IsOne())
		// length-two case (element.go)
		product, err = Product(k, One(k), Zero(k))
		assert.NoError(err)
		assert.True(product.IsZero())
		// length-two case (parent.go)
		product2, err = k.Product(One(k), Zero(k))
		assert.NoError(err)
		assert.True(product2.(*Element).IsZero())
		// length-two case again (element.go)
		product, err = Product(k, One(k), One(k))
		assert.NoError(err)
		assert.True(product.IsOne())
		// length-two case again (parent.go)
		product2, err = k.Product(One(k), One(k))
		assert.NoError(err)
		assert.True(product2.(*Element).IsOne())
	}
	//////////////////////////////////////////////////////////////////////
	// test cases where the parents are mismatched
	//////////////////////////////////////////////////////////////////////
	k1, err := GF(smallprime)
	assert.NoError(err)
	k2, err := GF(bigprime)
	assert.NoError(err)
	var nilParent *Parent   // the field with one element
	var nilElement *Element // the element of the field with one element
	var junk object.Element
	// length-one cases
	_, err = Product(k1, Zero(k2))
	assert.Error(err)
	_, err = Product(k1, nilElement)
	assert.Error(err)
	_, err = k1.Product(Zero(k2))
	assert.Error(err)
	_, err = k1.Product(nilElement)
	assert.Error(err)
	_, err = nilParent.Product(junk)
	assert.Error(err)
	// length-two cases
	_, err = Product(k1, One(k1), Zero(k2))
	assert.Error(err)
	_, err = Product(k1, One(k1), nilElement)
	assert.Error(err)
	_, err = k1.Product(One(k1), Zero(k2))
	assert.Error(err)
	_, err = k1.Product(One(k2), Zero(k1))
	assert.Error(err)
	_, err = k1.Product(One(k1), nilElement)
	assert.Error(err)
	_, err = k1.Product(nilElement, One(k1))
	assert.Error(err)
	_, err = nilParent.Product(One(nilParent), junk)
	assert.Error(err)
	_, err = nilParent.Product(junk, Zero(nilParent))
	assert.Error(err)
	// length-three cases
	_, err = Product(k1, One(k1), Zero(k2), Zero(k1))
	assert.Error(err)
	_, err = Product(k1, nilElement, One(k1), Zero(k1))
	assert.Error(err)
	_, err = k1.Product(One(k1), Zero(k2), Zero(k1))
	assert.Error(err)
	_, err = k1.Product(One(k1), Zero(k1), nilElement)
	assert.Error(err)
	_, err = nilParent.Product(One(nilParent), Zero(nilParent), junk)
	assert.Error(err)
}

// TestDotProduct tests DotProduct. We take the dot product of two copies of [1..N] for various N
func TestDotProduct(t *testing.T) {
	assert := assert.New(t)
	// the GF(p)'s we test. We include the field with one element as the last entry in the slice
	gfs := make([]*Parent, 4)
	var err error
	for i, p := range []int64{smallprime, mediumprime, bigprime} {
		gfs[i], err = GF(p)
		assert.NoError(err)
	}
	// we run tests for each field
	for _, k := range gfs {
		//////////////////////////////////////////////////////////////////////
		// test long slices
		//////////////////////////////////////////////////////////////////////
		for n := 1; n <= 5; n++ {
			// Create a long slice to sum
			S := make([]*Element, 0, 5000*n)
			for i := int64(1); i <= int64(cap(S)); i++ {
				S = append(S, FromInt64(k, i))
			}
			// recreate S as a slice of object.Elements
			SS := make([]object.Element, 0, len(S))
			for _, x := range S {
				SS = append(SS, x)
			}
			// Calculate the dot product
			N := int64(len(S))
			// as elements of k
			if result, err := DotProduct(k, S, S); assert.NoError(err) {
				assert.True(result.IsEqualTo(FromInt64(k, N*(N+1)*(2*N+1)/6)))
			}
			// as object.Elements
			if result, err := k.DotProduct(SS, SS); assert.NoError(err) {
				assert.AreEqual(k, FromInt64(k, N*(N+1)*(2*N+1)/6), result)
			}
			// Test slices of different lengths
			if result, err := DotProduct(k, S, append(S, S...)); assert.NoError(err) {
				assert.True(result.IsEqualTo(FromInt64(k, N*(N+1)*(2*N+1)/6)))
			}
			if result, err := DotProduct(k, append(S, S...), S); assert.NoError(err) {
				assert.True(result.IsEqualTo(FromInt64(k, N*(N+1)*(2*N+1)/6)))
			}
		}
		//////////////////////////////////////////////////////////////////////
		// test short slices
		//////////////////////////////////////////////////////////////////////
		S1 := make([]*Element, 0, 2)
		S2 := make([]*Element, 0, 2)
		S1 = append(S1, FromInt64(k, 2))
		S1 = append(S1, FromInt64(k, 3))
		S2 = append(S2, FromInt64(k, 4))
		S2 = append(S2, FromInt64(k, 5))
		// length zero
		if result, err := DotProduct(k, S1[:0], S2[:0]); assert.NoError(err) {
			assert.True(result.IsZero())
		}
		// length one
		if result, err := DotProduct(k, S1[:1], S2[:1]); assert.NoError(err) {
			assert.True(result.IsEqualTo(FromInt64(k, 8)))
		}
		if result, err := DotProduct(k, S1, S2[:1]); assert.NoError(err) {
			assert.True(result.IsEqualTo(FromInt64(k, 8)))
		}
		if result, err := DotProduct(k, S1[:1], S2); assert.NoError(err) {
			assert.True(result.IsEqualTo(FromInt64(k, 8)))
		}
		// length two
		if result, err := DotProduct(k, S1, S2); assert.NoError(err) {
			assert.True(result.IsEqualTo(FromInt64(k, 23)))
		}
		//////////////////////////////////////////////////////////////////////
		// test cases where some of the slice elements are not in the correct
		// field
		//////////////////////////////////////////////////////////////////////
		S1 = make([]*Element, 0, 100)
		S2 = make([]*Element, 0, 100)
		for i := int64(1); i <= int64(cap(S1)); i++ {
			S1 = append(S1, FromInt64(k, i))
		}
		copy(S2, S1)
		wrongK, err := GF(7)
		assert.NoError(err)
		S1[len(S1)/2] = FromInt64(wrongK, 3)
		_, err = DotProduct(k, S1, S2)
		assert.Error(err)
		_, err = DotProduct(k, S2, S1)
		assert.Error(err)
		// recreate S1 and S2 as slices of object.Elements
		SS1 := make([]object.Element, len(S1))
		SS2 := make([]object.Element, len(S2))
		for _, x := range S1 {
			SS1 = append(SS1, x)
		}
		for _, y := range S2 {
			SS2 = append(SS2, y)
		}
		_, err = k.DotProduct(SS1, SS2)
		assert.Error(err)
		_, err = k.DotProduct(SS2, SS1)
		assert.Error(err)
	}
}
