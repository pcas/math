// Tests of finite field element gob encoding/decoding.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/testing/assert"
	"bytes"
	"encoding/gob"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestGobEncoding tests gob encoding/decoding.
func TestGobEncoding(t *testing.T) {
	assert := assert.New(t)
	// The slices of values to test, in the form {k,n} where n is an element
	// in GF(k)
	tests := [][]int64{
		{3, 0},
		{3, 1},
		{3, 2},
		{101, 0},
		{101, 1},
		{101, 73},
	}
	// Work through the tests
	for _, test := range tests {
		// Create the finite field and element
		k, err := GF(test[0])
		require.NoError(t, err)
		n := FromInt64(k, test[1])
		// Create the encoder and decoder
		var b bytes.Buffer
		enc := gob.NewEncoder(&b)
		dec := gob.NewDecoder(&b)
		// Encode the value
		require.NoError(t, enc.Encode(n))
		// Decode the value
		if m, err := GobDecode(dec); assert.NoError(err) {
			assert.AreEqual(k, n, m)
		}
	}
}

// TestGobEncodeNil tests gob encoding a nil object.
func TestGobEncodeNil(t *testing.T) {
	assert := assert.New(t)
	// Encode a nil object
	var n *Element
	if b, err := n.GobEncode(); assert.NoError(err) {
		// Decoding should give zero in GF(1)
		m := &Element{}
		if assert.NoError(m.GobDecode(b)) {
			if assert.IsZero(m) {
				assert.True(m.Parent() == gF1)
			}
		}
	}
}

// TestGobDecodeEmpty tests gob decoding an empty slice of bytes
func TestGobDecodeEmpty(t *testing.T) {
	assert := assert.New(t)
	// This should give zero in GF(1)
	m := &Element{}
	if assert.NoError(m.GobDecode(nil)) {
		if assert.IsZero(m) {
			assert.True(m.Parent() == gF1)
		}
	}
}

// TestGobDecodeNil tests gob decoding into a nil object.
func TestGobDecodeNil(t *testing.T) {
	require := require.New(t)
	// Create a finite field
	k, err := GF(101)
	require.NoError(err)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode a value
	require.NoError(enc.Encode(k.One()))
	// Try to decode directly into a nil object -- this should error
	var m *Element
	require.Error(m.GobDecode(b.Bytes()))
}

// TestGobDecodeJunk tests gob decoding of junk.
func TestGobDecodeJunk(t *testing.T) {
	require := require.New(t)
	// Create a random byte slice
	b := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	dec := gob.NewDecoder(bytes.NewReader(b))
	// Try to decode as a *Element -- this should error
	_, err := GobDecode(dec)
	require.Error(err)
	m := &Element{}
	require.Error(m.GobDecode(b))
}

// TestGobDecodeVersion tests gob decoding of an incorrect version.
func TestGobDecodeVersion(t *testing.T) {
	require := require.New(t)
	// Encode a version number from the future
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	require.NoError(enc.Encode(byte(encodingVersion + 1)))
	// Try decoding -- this should error
	m := &Element{}
	require.Error(m.GobDecode(b.Bytes()))
}

// TestGobDecodeExisting tests gob decoding into an existing value.
func TestGobDecodeExisting(t *testing.T) {
	require := require.New(t)
	// Create a finite field
	k, err := GF(101)
	require.NoError(err)
	// Create the encoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// Encode a value
	require.NoError(enc.Encode(k.One()))
	// Try to decode directly into an existing value -- this should error
	require.Error(FromInt64(k, 7).GobDecode(b.Bytes()))
}

// TestGobSliceEncoding tests gob encoding/decoding of slices.
func TestGobSliceEncoding(t *testing.T) {
	assert := assert.New(t)
	// Create a finite field
	k, err := GF(101)
	require.NoError(t, err)
	// The slices of values to test
	test := []int64{0, 1, 17, 22, 34, 86}
	S := make([]*Element, 0, len(test))
	for _, n := range test {
		S = append(S, FromInt64(k, n))
	}
	// Create the encoder and decoder
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)
	// Encode the slice of values
	if assert.NoError(enc.Encode(S)) {
		// Decode the slice of values
		if SS, err := GobDecodeSlice(dec); assert.NoError(err) && assert.Len(SS, len(S)) {
			// Check for equality
			for i, n := range S {
				assert.AreEqual(k, n, SS[i])
			}
		}
	}
}
