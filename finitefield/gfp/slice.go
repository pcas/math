// Slice contains utility functions for working with slices of finite field elements.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/slice"
)

// Slice wraps an []*Element implementing slice.Interface.
type Slice []*Element

/////////////////////////////////////////////////////////////////////////
// Slice functions
/////////////////////////////////////////////////////////////////////////

// Len returns the length of the slice.
func (s Slice) Len() int {
	return len(s)
}

// Entry returns the i-th element in the slice. This will panic if i is out of range.
func (s Slice) Entry(i int) object.Element {
	return s[i]
}

// Slice returns a subslice starting at index k and of length m - k. The returned subslice will be of the same underlying type. This will panic if the arguments are out of range.
func (s Slice) Slice(k int, m int) slice.Interface {
	return s[k:m]
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// CopySlice returns a copy of the slice S. The capacity will be preserved.
func CopySlice(S []*Element) []*Element {
	T := make([]*Element, len(S), cap(S))
	copy(T, S)
	return T
}

// CopySliceOfSlices returns a copy of the slice S. The capacity will be preserved.
func CopySliceOfSlices(S [][]*Element) [][]*Element {
	T := make([][]*Element, len(S), cap(S))
	for i, s := range S {
		T[i] = CopySlice(s)
	}
	return T
}
