// Tests for slice.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/slice"
	"github.com/stretchr/testify/assert"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// testSubslice tests subslices of the slice S. Requires that S is of length at least three.
func testSubslice(t *testing.T, k *Parent, S slice.Interface) {
	assert := assert.New(t)
	// Sanity check
	n := S.Len()
	if n < 3 {
		panic("Slice must be of length at least three.")
	}
	// Create a subslice
	T := slice.Slice(S, 1, n-1)
	assert.Equal(T.Len(), n-2)
	for i := 0; i < n-2; i++ {
		assert.True(k.AreEqual(S.Entry(i+1), T.Entry(i)))
	}
	// Asking for illegal subslices should panic
	assert.Panics(func() { slice.Slice(S, -1, 0) })
	assert.Panics(func() { slice.Slice(S, 0, -1) })
	assert.Panics(func() { slice.Slice(S, 1, 0) })
	assert.Panics(func() { slice.Slice(S, 0, n+1) })
	assert.Panics(func() { slice.Slice(S, n, n+1) })
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestCopySlice tests CopySlice
func TestCopySlice(t *testing.T) {
	assert := assert.New(t)
	// make a finite field
	k, err := GF(31)
	assert.NoError(err)
	// Create a slice of finite field elements
	S := make([]*Element, 0, 5)
	for _, i := range []int{5, -5, 12, -12, 6} {
		S = append(S, FromInt(k, i))
	}
	// Make a copy
	T := CopySlice(S)
	// Assert equality
	assert.Len(T, len(S))
	for i, t := range T {
		assert.True(t.IsEqualTo(S[i]))
	}
}

// TestCopySliceOfSlices tests CopySliceOfSlices
func TestCopySliceOfSlices(t *testing.T) {
	assert := assert.New(t)
	// make a finite field
	k, err := GF(31)
	assert.NoError(err)
	// Create a slice of slices of finite field elements
	S := make([][]*Element, 5)
	for i := 0; i < 5; i++ {
		S[i] = make([]*Element, 0, 5*i+1)
		for j := 1; j < 5*i+1; j++ {
			S[i] = append(S[i], FromInt(k, i*j))
		}
	}
	// Make a copy
	T := CopySliceOfSlices(S)
	// Assert equality
	assert.Len(T, len(S))
	for i, t := range T {
		assert.Len(t, len(S[i]))
		for j, v := range t {
			assert.True(v.IsEqualTo(S[i][j]))
		}
	}
	// Check that nil slices are handled correctly
	U := [][]*Element{
		nil,
		[]*Element{FromInt(k, 3), FromInt(k, -3)},
	}
	V := CopySliceOfSlices(U)
	assert.Len(V, len(U))
	assert.False(V[0] == nil)
	assert.Len(V[0], 0)
	assert.True(cap(V[0]) == 0)
	for i, v := range V[1] {
		assert.True(v.IsEqualTo(U[1][i]))
	}
}

// TestSlice tests Slice.
func TestSlice(t *testing.T) {
	assert := assert.New(t)
	// Make a finite field
	k, err := GF(7)
	assert.NoError(err)
	// Create the test slice
	S := make(Slice, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, FromInt(k, i))
	}
	// Check that it reports the correct length and the entries agree
	if assert.Equal(S.Len(), len(S)) {
		for i, s := range S {
			if ok, err := k.AreEqual(S.Entry(i), s); assert.NoError(err) {
				assert.True(ok)
			}
		}
	}
	// Asking for an out-of-range entry should panic
	assert.Panics(func() { S.Entry(-1) })
	assert.Panics(func() { S.Entry(len(S)) })
	// Test subslices
	testSubslice(t, k, S)
}
