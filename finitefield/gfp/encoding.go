// Encoding contains functions for encoding/decoding a finite field element.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/gobutil"
	"encoding/gob"
)

// encodingVersion is the internal version number. This will permit backwards-compatible changes to the encoding.
const encodingVersion byte = 1

/////////////////////////////////////////////////////////////////////////
// Gob encoding/decoding
/////////////////////////////////////////////////////////////////////////

// GobEncode implements the gob.GobEncoder interface.
func (a *Element) GobEncode() ([]byte, error) {
	// Deal with the nil case
	if a == nil {
		return Zero(nil).GobEncode()
	}
	// Create an encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Add the version number
	if err := enc.Encode(encodingVersion); err != nil {
		return nil, err
	}
	// Add the parent's modulus
	if err := enc.Encode(a.p); err != nil {
		return nil, err
	}
	// Add the value
	if err := enc.Encode(a.n); err != nil {
		return nil, err
	}
	// Return the buffer
	return enc.Bytes(), nil
}

// GobDecode implements the gob.GobDecoder interface. Important: Take great care that you are decoding into a new *Element; the safe way to do this is to use the GobDecode(dec *gob.Decode) function.
func (a *Element) GobDecode(buf []byte) error {
	// Sanity check
	if a == nil {
		return gobutil.DecodingIntoNilObject.New()
	} else if a.p != 0 {
		return gobutil.DecodingIntoExistingObject.New()
	} else if len(buf) == 0 {
		a.p = 1
		a.n = 0
		return nil
	}
	// Wrap the bytes up in a decoder
	dec := gobutil.NewDecoder(buf)
	// Read the version number
	var v byte
	if err := dec.Decode(&v); err != nil {
		return err
	} else if v != encodingVersion {
		return gobutil.EncodingVersion.New(v)
	}
	// Read in the parent's modulus
	var p int64
	if err := dec.Decode(&p); err != nil {
		return err
	}
	// make sure the parent is cached, and that p is prime
	if _, err := GF(p); err != nil {
		return err
	}
	// Read in the value
	var n int64
	if err := dec.Decode(&n); err != nil {
		return err
	}
	// Set the modulus and value
	a.p = p
	a.n = n
	return nil
}

// GobDecode reads the next value from the given gob.Decoder and decodes it as a finite field elements.
func GobDecode(dec *gob.Decoder) (*Element, error) {
	// Decode into a new element
	a := &Element{}
	if err := dec.Decode(a); err != nil {
		return nil, err
	}
	// Handle the zero and one cases
	gFp, err := GF(a.p)
	if err != nil {
		panic(fatal.ImpossibleErrorf(err, "The characteristic should be prime.")) // This should never happen
	}
	if a.IsZero() {
		return Zero(gFp), nil
	} else if a.IsOne() {
		return One(gFp), nil
	}
	// Return the value
	return a, nil
}

// GobDecodeSlice reads the next value from the given gob.Decoder and decodes it as a slice of finite field elements.
func GobDecodeSlice(dec *gob.Decoder) ([]*Element, error) {
	// Decode into a slice
	var S []*Element
	if err := dec.Decode(&S); err != nil {
		return nil, err
	}
	// Replace any instances of zero or one
	for i, a := range S {
		gFp, err := GF(a.p)
		if err != nil {
			panic(fatal.ImpossibleErrorf(err, "The characteristic should be prime.")) // This should never happen
		}
		if a.IsZero() {
			S[i] = Zero(gFp)
		} else if a.IsOne() {
			S[i] = One(gFp)
		}
	}
	// Return the slice
	return S, nil
}
