// Element represents an element of a finite field (ZZ/pZZ) where p is prime

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/mathutil"
	"strconv"
)

// Element is an element of a field ZZ/pZZ where p is prime
type Element struct {
	p int64 // the value of the modulus
	n int64 // the value of the element. Here 0 <= n < p.
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// parentsAgree returns true if and only if the parents of a and b agree, where if either a or b is nil it is interpreted as an element of the field with one element
func parentsAgree(a *Element, b *Element) bool {
	if a == nil {
		return b == nil || b.p == 1
	} else if b == nil {
		return a.p == 1
	}
	return a.p == b.p
}

// isInGF1 returns true if and only if a is either nil or has parent equal to gF1, that is, if and only if a represents an element of the field with one element
func isInGF1(a *Element) bool {
	return a == nil || a.p == 1
}

// isInParent returns true if and only if either a has parent k or a is nil and parent is gF1, that is, if and only if a represents an element of k
func isInParent(k *Parent, a *Element) bool {
	if a == nil {
		return k == nil || k == gF1
	}
	return a.p == k.characteristic
}

// expModInt64IntegerInt64 returns a^k mod m. m is required to be positive; this will panic otherwise. k is required to be non-negative; this will panic otherwise. The return value is always in the range [0,m-1].
func expModInt64IntegerInt64(a int64, k *integer.Element, m int64) int64 {
	// sanity checks
	if k.IsNegative() {
		panic("Illegal negative exponent.")
	} else if m <= 0 {
		panic("Illegal non-positive modulus.")
	} else if m == 1 {
		// do the trivial case
		return 0
	}
	// reduce a mod m, getting the sign right
	if a = a % m; a < 0 {
		a += m
	}
	// do the easy cases
	if a == 1 || k.IsZero() {
		return 1
	} else if a == 0 {
		return 0
	} else if k.IsOne() {
		return a
	} else if a == m-1 {
		if k.IsEven() {
			return 1
		}
		return m - 1
	}
	// Do the exponentiation
	res := int64(1)
	pow := a
	kk := k.BigInt()
	for i := 0; i <= kk.BitLen(); i++ {
		if kk.Bit(i) == 1 {
			res = mathutil.MulModInt64(res, pow, m)
		}
		pow = mathutil.MulModInt64(pow, pow, m)
	}
	return res
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// FromInteger returns n as an element of k.
func FromInteger(k *Parent, n *integer.Element) *Element {
	// deal with the nil parent case
	if k == nil || k == gF1 {
		return Zero(gF1)
	}
	// special case zero and one
	if n.IsZero() {
		return Zero(k)
	} else if n.IsOne() {
		return One(k)
	}
	// general case
	residueAsInteger, err := n.Mod(integer.FromInt64(k.characteristic))
	if err != nil {
		panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is non-zero.")) // This should never happen
	}
	residueAsInt64, err := residueAsInteger.Int64()
	if err != nil {
		panic(fatal.ImpossibleErrorf(err, "We know a priori that the residue should fit in an int64.")) // This should never happen
	}
	result := &Element{
		n: residueAsInt64,
		p: k.characteristic,
	}
	return result
}

// FromInt64 returns n as an element of k.
func FromInt64(k *Parent, n int64) *Element {
	// deal with the nil parent case
	if k == nil || k == gF1 {
		return Zero(gF1)
	}
	// special case zero and one
	if n == 0 {
		return Zero(k)
	} else if n == 1 {
		return One(k)
	}
	// general case
	residue := n % k.characteristic
	if residue < 0 {
		residue += k.characteristic
	}
	result := &Element{
		n: residue,
		p: k.characteristic,
	}
	return result
}

// FromInt32 returns n as an element of k.
func FromInt32(k *Parent, n int32) *Element {
	return FromInt64(k, int64(n))
}

// FromInt returns n as an element of k.
func FromInt(k *Parent, n int) *Element {
	return FromInt64(k, int64(n))
}

// FromUint64 returns n as an element of k.
func FromUint64(k *Parent, n uint64) *Element {
	return FromInteger(k, integer.FromUint64(n))
}

// FromRational returns q as an element of k. It returns an error if the denominator of q is not invertible in k.
func FromRational(k *Parent, q *rational.Element) (*Element, error) {
	// deal with the nil parent case
	if k == nil || k == gF1 {
		return Zero(gF1), nil
	}
	// special case zero and one
	if q.IsZero() {
		return Zero(k), nil
	} else if q.IsOne() {
		return One(k), nil
	}
	// general case
	return Divide(FromInteger(k, q.Numerator()), FromInteger(k, q.Denominator()))
}

// Zero returns the zero of k
func Zero(k *Parent) *Element {
	if k == nil {
		return gF1.zero
	}
	return k.zero
}

// One returns the unit element of k
func One(k *Parent) *Element {
	if k == nil {
		return gF1.one
	}
	return k.one
}

// Add returns the sum of a and b. It returns an error if a and b do not have the same parent.
func Add(a *Element, b *Element) (*Element, error) {
	// make sure the parents agree
	if !parentsAgree(a, b) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// deal with the case where a and b are in the field with one element
		return Zero(gF1), nil
	}
	if a.IsZero() {
		return b, nil
	} else if b.IsZero() {
		return a, nil
	}
	result := &Element{
		n: mathutil.AddModInt64(a.n, b.n, a.p),
		p: a.p,
	}
	return result, nil
}

// Subtract returns a-b. It returns an error if a and b do not have the same parent.
func Subtract(a *Element, b *Element) (*Element, error) {
	// make sure the parents agree
	if !parentsAgree(a, b) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// deal with the case where a and b are in the field with one element
		return Zero(gF1), nil
	}
	if b.IsZero() {
		return a, nil
	}
	result := &Element{
		n: mathutil.AddModInt64(a.n, b.p-b.n, a.p),
		p: a.p,
	}
	return result, nil
}

// Multiply returns the product of a and b. It returns an error if a and b do not have the same parent.
func Multiply(a *Element, b *Element) (*Element, error) {
	// make sure the parents agree
	if !parentsAgree(a, b) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// deal with the case where a and b are in the field with one element
		return Zero(gF1), nil
	}
	if a.IsZero() {
		return a, nil
	} else if a.IsOne() {
		return b, nil
	} else if b.IsZero() {
		return b, nil
	} else if b.IsOne() {
		return a, nil
	}
	result := &Element{
		n: mathutil.MulModInt64(a.n, b.n, a.p),
		p: a.p,
	}
	return result, nil
}

// MultiplyThenAdd returns the value a * b + c.  It returns an error unless a, b, and c have the same parent.
func MultiplyThenAdd(a *Element, b *Element, c *Element) (*Element, error) {
	// make sure the parents agree, then get the easy cases out of the way
	if !parentsAgree(a, b) || !parentsAgree(b, c) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// in this case a, b, and c are in the field with one element
		return Zero(gF1), nil
	} else if a.n == 0 || b.n == 0 {
		return c, nil
	} else if c.n == 0 {
		return Multiply(a, b)
	} else if a.n == 1 {
		return Add(b, c)
	} else if b.n == 1 {
		return Add(a, c)
	}
	// Perform the calculation
	product := mathutil.MulModInt64(a.n, b.n, a.p)
	result := &Element{
		n: mathutil.AddModInt64(product, c.n, a.p),
		p: a.p,
	}
	return result, nil
}

// MultiplyThenSubtract returns the value a * b - c.
func MultiplyThenSubtract(a *Element, b *Element, c *Element) (*Element, error) {
	// make sure the parents agree, then get the easy cases out of the way
	if !parentsAgree(a, b) || !parentsAgree(b, c) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// in this case a, b, and c are in the field with one element
		return Zero(gF1), nil
	} else if a.n == 0 || b.n == 0 {
		return c.Negate(), nil
	} else if c.n == 0 {
		return Multiply(a, b)
	} else if a.n == 1 {
		return Subtract(b, c)
	} else if b.n == 1 {
		return Subtract(a, c)
	}
	// Perform the calculation
	product := mathutil.MulModInt64(a.n, b.n, a.p)
	result := &Element{
		n: mathutil.AddModInt64(product, -c.n, a.p),
		p: a.p,
	}
	return result, nil
}

// MultiplyMultiplyThenAdd returns the value a * b + c * d.  It returns an error unless a, b, c, and d have the same parent
func MultiplyMultiplyThenAdd(a *Element, b *Element, c *Element, d *Element) (*Element, error) {
	// make sure the parents agree, then get the easy cases out of the way
	if !parentsAgree(a, b) || !parentsAgree(b, c) || !parentsAgree(c, d) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// in this case a, b, c, and d are in the field with one element
		return Zero(gF1), nil
	} else if a.n == 0 || b.n == 0 {
		if c.n == 0 || d.n == 0 {
			gFp, err := GF(a.p)
			if err != nil {
				panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
			}
			return gFp.zero, nil
		}
		result := &Element{
			n: mathutil.MulModInt64(c.n, d.n, a.p),
			p: a.p,
		}
		return result, nil
	} else if c.n == 0 || d.n == 0 {
		return Multiply(a, b)
	}
	// Perform the calculation
	product1 := mathutil.MulModInt64(a.n, b.n, a.p)
	product2 := mathutil.MulModInt64(c.n, d.n, a.p)
	result := &Element{
		n: mathutil.AddModInt64(product1, product2, a.p),
		p: a.p,
	}
	return result, nil
}

// MultiplyMultiplyThenSubtract returns the value a * b - c * d.
func MultiplyMultiplyThenSubtract(a *Element, b *Element, c *Element, d *Element) (*Element, error) {
	// make sure the parents agree, then get the easy cases out of the way
	if !parentsAgree(a, b) || !parentsAgree(b, c) || !parentsAgree(c, d) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// in this case a, b, c, and d are in the field with one element
		return Zero(gF1), nil
	} else if a.n == 0 || b.n == 0 {
		if c.n == 0 || d.n == 0 {
			gFp, err := GF(a.p)
			if err != nil {
				panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
			}
			return gFp.zero, nil
		}
		result := &Element{
			n: mathutil.MulModInt64(-c.n, d.n, a.p),
			p: a.p,
		}
		return result, nil
	} else if c.n == 0 || d.n == 0 {
		return Multiply(a, b)
	}
	// Perform the calculation
	product1 := mathutil.MulModInt64(a.n, b.n, a.p)
	product2 := mathutil.MulModInt64(-c.n, d.n, a.p)
	result := &Element{
		n: mathutil.AddModInt64(product1, product2, a.p),
		p: a.p,
	}
	return result, nil
}

// Divide returns a/b. It returns an error if a and b do not have the same parent.
func Divide(a *Element, b *Element) (*Element, error) {
	// make sure the parents agree
	if !parentsAgree(a, b) {
		return nil, errors.ParentsDoNotAgree.New()
	} else if isInGF1(a) {
		// deal with the case where a and b are in the field with one element
		return Zero(gF1), nil
	} else if b.IsOne() {
		return a, nil
	}
	bInverse, err := b.Inverse()
	if err != nil {
		return nil, err
	}
	return Multiply(a, bInverse)
}

// PowerInt64 returns a^n. It returns an error if n is negative and a is not invertible.
func PowerInt64(a *Element, n int64) (*Element, error) {
	if isInGF1(a) {
		// if a is in the field with one element then we know what the answer is
		return Zero(gF1), nil
	}
	// general case
	var err error
	if n == 0 {
		// if n==0 then we return 1
		gFp, err := GF(a.p)
		if err != nil {
			panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
		}
		return gFp.one, nil
	} else if n < 0 {
		// replace a by a^{-1} and n by -n, unless a is zero in which case we return an error
		a, err = a.Inverse()
		if err != nil {
			// division by zero error
			return nil, err
		}
		n = -n
	}
	if n == 1 {
		return a, nil
	}
	return &Element{
		n: mathutil.ExpModInt64(a.n, n, a.p),
		p: a.p,
	}, nil
}

// Power returns a^n. It returns an error if n is negative and a is not invertible.
func Power(a *Element, n *integer.Element) (*Element, error) {
	if isInGF1(a) {
		// if a is in the field with one element then we know what the answer is
		return Zero(gF1), nil
	}
	// general case
	if n.IsZero() {
		// if n==0 then we return 1
		gFp, err := GF(a.p)
		if err != nil {
			panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
		}
		return gFp.one, nil
	} else if n.IsNegative() {
		// replace a by a^{-1} and n by -n, unless a is zero in which case we return an error
		aInverse, err := a.Inverse()
		if err != nil {
			// division by zero error
			return nil, err
		}
		a = aInverse
		n = n.Negate()
	}
	if n.IsOne() {
		return a, nil
	}
	result := &Element{
		n: expModInt64IntegerInt64(a.n, n, a.p),
		p: a.p,
	}
	return result, nil
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is zero.
func Sum(k *Parent, S ...*Element) (*Element, error) {
	// deal with the case where k is the field with one element
	if k == nil || k == gF1 {
		return Zero(gF1), nil
	}
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return Zero(k), nil
	}
	// Check that the elements of S all lie in k
	for _, s := range S {
		if !isInParent(k, s) {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	// Fast-track the length 1 and length-2 cases
	if len(S) == 1 {
		return S[0], nil
	} else if len(S) == 2 {
		return Add(S[0], S[1])
	}
	// Hand off to the int64 Sum code in mathutil/modular.go
	sS := make([]int64, 0, len(S))
	for _, entry := range S {
		sS = append(sS, entry.n)
	}
	return &Element{
		n: mathutil.SumInt64ModInt64(sS, k.characteristic),
		p: k.characteristic,
	}, nil

}

// Product returns the product of the elements in the slice S. The product of the empty slice is 1.
func Product(k *Parent, S ...*Element) (*Element, error) {
	// deal with the case where k is the field with one element
	if k == nil || k == gF1 {
		return One(gF1), nil
	}
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return One(k), nil
	}
	// Check that the elements of S all lie in k
	for _, s := range S {
		if !isInParent(k, s) {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	// Fast-track the length 1 and length-2 cases
	if len(S) == 1 {
		return S[0], nil
	} else if len(S) == 2 {
		return Multiply(S[0], S[1])
	}
	// Hand off to the int64 Sum code in mathutil/modular.go
	sS := make([]int64, 0, len(S))
	for _, entry := range S {
		sS = append(sS, entry.n)
	}
	return &Element{
		n: mathutil.ProductInt64ModInt64(sS, k.characteristic),
		p: k.characteristic,
	}, nil

}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[n] * S2[n], where n+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element.  Unless all elements of S1 and S2 lie in k, an error is returned.
func DotProduct(k *Parent, S1 []*Element, S2 []*Element) (*Element, error) {
	// Handle the case where k is the field with one element
	if k == nil || k == gF1 {
		for _, x := range S1 {
			if !isInGF1(x) {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
		for _, y := range S2 {
			if !isInGF1(y) {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
		return Zero(k), nil
	}
	// check that all elements of the slices lie in k
	for _, x := range S1 {
		if !isInParent(k, x) {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	for _, y := range S2 {
		if !isInParent(k, y) {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	// Resize the slices to that they're both of the same length
	n := len(S1)
	if n2 := len(S2); n != n2 {
		if n < n2 {
			S2 = S2[:n]
		} else {
			n = n2
			S1 = S1[:n]
		}
	}
	// Fast-track the easy cases
	if n <= 2 {
		if n == 0 {
			return k.zero, nil
		} else if n == 1 {
			return Multiply(S1[0], S2[0])
		}
		return MultiplyMultiplyThenAdd(S1[0], S2[0], S1[1], S2[1])
	}
	// hand off to the int64 dot product code in mathutil/modular.go
	sS1 := make([]int64, 0, n)
	for _, entry := range S1 {
		sS1 = append(sS1, entry.n)
	}
	sS2 := make([]int64, 0, n)
	for _, entry := range S2 {
		sS2 = append(sS2, entry.n)
	}
	result := &Element{
		n: mathutil.DotProductInt64ModInt64(sS1, sS2, k.characteristic),
		p: k.characteristic,
	}
	return result, nil
}

/////////////////////////////////////////////////////////////////////////
// Element functions
/////////////////////////////////////////////////////////////////////////

// IsZero returns true iff a is zero.
func (a *Element) IsZero() bool {
	return a == nil || a.n == 0
}

// IsOne returns true iff a is one.
func (a *Element) IsOne() bool {
	return isInGF1(a) || a.n == 1
}

// IsEqualTo returns true iff a = b.
func (a *Element) IsEqualTo(b *Element) bool {
	if a == b {
		return true
	} else if isInGF1(a) {
		return isInGF1(b)
	} else if isInGF1(b) {
		return false
	} else if a.p != b.p {
		return false
	}
	return a.n == b.n
}

// Negate return -a.
func (a *Element) Negate() *Element {
	if a.IsZero() {
		return a
	}
	return &Element{
		n: a.p - a.n,
		p: a.p}
}

// PowerInt64 returns a^n. It returns an error if n is negative and a is zero.
func (a *Element) PowerInt64(n int64) (*Element, error) {
	return PowerInt64(a, n)
}

// Power returns a^n.  It returns an error if n is negative and a is zero.
func (a *Element) Power(n *integer.Element) (*Element, error) {
	return Power(a, n)
}

// ScalarMultiplyByInteger returns na, where this is defined to be a + ... + a (n times) if n is positive, -a - ... - a (|n| times) if n is negative, and the zero element if n is zero.
func (a *Element) ScalarMultiplyByInteger(n *integer.Element) *Element {
	if a.IsZero() {
		return a
	}
	gFp, err := GF(a.p)
	if err != nil {
		panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
	}
	if n.IsZero() {
		return gFp.zero
	}
	y, err := Multiply(FromInteger(gFp, n), a)
	if err != nil {
		panic(fatal.ImpossibleError(err)) // This should never happen
	}
	return y
}

// Inverse returns 1/a. It will return an error if a is zero.
func (a *Element) Inverse() (*Element, error) {
	if isInGF1(a) {
		return One(gF1), nil
	} else if a.n == 1 {
		return a, nil
	} else if a.IsZero() {
		return nil, errors.DivisionByZero.New()
	}
	gcd, u, _ := mathutil.XGCDOfPairInt64(a.n, a.p)
	if gcd != 1 {
		panic(fatal.ImpossibleErrorf(nil, "The modulus and residue should be coprime.")) // This should never happen
	}
	if u < 0 {
		u += a.p
	}
	result := &Element{
		n: u,
		p: a.p,
	}
	return result, nil
}

// ToInt64 returns the int64 b that is equal to a mod p and satisfies 0 <= b < p, where a lies in ZZ/pZZ
func ToInt64(a *Element) int64 {
	return a.n
}

// Parent returns the finite field containing a as an object.Parent. This can safely be coerced to type *Parent.
func (a *Element) Parent() object.Parent {
	if a == nil {
		return gF1
	}
	gFp, err := GF(a.p)
	if err != nil {
		panic(fatal.ImpossibleErrorf(err, "We know a priori that the characteristic is prime.")) // This should never happen
	}
	return gFp
}

// Hash returns a hash value for a
func (a *Element) Hash() hash.Value {
	if a == nil {
		return 0
	}
	return hash.Value(a.n)
}

// String returns a string representation of the finite field element a
func (a *Element) String() string {
	if a == nil {
		return "0"
	}
	return strconv.FormatInt(a.n, 10)
}
