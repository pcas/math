// Parent represents a finite field GF(p) where p is prime.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package gfp

import (
	"bitbucket.org/pcas/math/errors"
	"bitbucket.org/pcas/math/integer"
	"bitbucket.org/pcas/math/object"
	"bitbucket.org/pcas/math/rational"
	"bitbucket.org/pcastools/mathutil"
	"fmt"
	"sync"
)

// Parent represents a finite field GF(p) for some prime p.
type Parent struct {
	characteristic int64    // the characteristic of the field
	order          int64    // the order of the field
	zero           *Element // the zero element of the field
	one            *Element // the unit element of the field
}

// The GF(p)'s that we know about, and a mutex to control access
var (
	gFps      map[int64]*Parent
	gFpsMutex *sync.RWMutex
)

// The field with one element
var gF1 *Parent

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initializes the memory we need.
func init() {
	// Create the map of fields and controlling mutex
	gFpsMutex = new(sync.RWMutex)
	gFps = make(map[int64]*Parent)
	// Create the field with one element
	gF1 = &Parent{
		characteristic: 1,
		order:          1,
	}
	gF1Element := &Element{
		p: 1,
		n: 0,
	}
	gF1.zero = gF1Element
	gF1.one = gF1Element
}

// objectsToElements returns the arguments as elements of k, if possible
func objectsToElements(k *Parent, x1 object.Element, x2 object.Element) (*Element, *Element, error) {
	y1, err1 := ToElement(k, x1)
	if err1 != nil {
		return nil, nil, errors.Arg1NotContainedInParent.New()
	}
	y2, err2 := ToElement(k, x2)
	if err2 != nil {
		return nil, nil, errors.Arg2NotContainedInParent.New()
	}
	return y1, y2, nil
}

/////////////////////////////////////////////////////////////////////////
// Parent functions
/////////////////////////////////////////////////////////////////////////

// Contains returns true iff x is an element of the field, or can naturally be regarded as an element of the field
func (k *Parent) Contains(x object.Element) bool {
	_, err := ToElement(k, x)
	return err == nil
}

// String returns a string representation of the field
func (k *Parent) String() string {
	if k == nil || k == gF1 {
		return "Finite field of order 1."
	}
	return fmt.Sprintf("Finite field of order %v.", k.order)
}

// Zero returns 0.
func (k *Parent) Zero() object.Element {
	return Zero(k)
}

// IsZero returns true iff x is the zero element in this field
func (k *Parent) IsZero(x object.Element) (bool, error) {
	xx, err := ToElement(k, x)
	if err != nil {
		return false, err
	}
	return xx.IsZero(), nil
}

// One returns 1.
func (k *Parent) One() object.Element {
	return One(k)
}

// IsOne returns true iff x is the unit element in this field.
func (k *Parent) IsOne(x object.Element) (bool, error) {
	xx, err := ToElement(k, x)
	if err != nil {
		return false, err
	}
	return xx.IsOne(), nil
}

// Add returns x + y.
func (k *Parent) Add(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(k, x, y)
	if err != nil {
		return nil, err
	}
	return Add(xx, yy)
}

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is zero.
func (k *Parent) Sum(S ...object.Element) (object.Element, error) {
	// deal with the case where k is the field with one element
	if k == nil || k == gF1 {
		// we return zero, provided that everything in S actually lies in gF1
		for _, x := range S {
			if !gF1.Contains(x) {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
		return Zero(gF1), nil
	}
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return Zero(k), nil
	}
	// Fast-track the length 1 and length-2 cases
	if len(S) == 1 {
		return ToElement(k, S[0])
	} else if len(S) == 2 {
		return k.Add(S[0], S[1])
	}
	// Convert everything in the slice to elements of k, and then
	// hand off to the int64 Sum code in mathutil/modular.go
	sS := make([]int64, 0, len(S))
	for _, x := range S {
		if xx, err := ToElement(k, x); err == nil {
			sS = append(sS, xx.n)
		} else {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	return &Element{
		p: k.characteristic,
		n: mathutil.SumInt64ModInt64(sS, k.characteristic),
	}, nil
}

// Subtract returns x - y.
func (k *Parent) Subtract(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(k, x, y)
	if err != nil {
		return nil, err
	}
	return Subtract(xx, yy)
}

// Multiply returns the product x * y.
func (k *Parent) Multiply(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(k, x, y)
	if err != nil {
		return nil, err
	}
	return Multiply(xx, yy)
}

// Product returns the product of the elements in the slice S. The product of the empty slice is 1.
func (k *Parent) Product(S ...object.Element) (object.Element, error) {
	// deal with the case where k is the field with one element
	if k == nil || k == gF1 {
		// we return zero, provided that everything in S actually lies in gF1
		for _, x := range S {
			if !gF1.Contains(x) {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
		return Zero(gF1), nil
	}
	// Get the zero length sequence out of the way
	if len(S) == 0 {
		return One(k), nil
	}
	// Fast-track the length 1 and length-2 cases
	if len(S) == 1 {
		return ToElement(k, S[0])
	} else if len(S) == 2 {
		return k.Multiply(S[0], S[1])
	}
	// Convert everything in the slice to elements of k, and then
	// hand off to the int64 Sum code in mathutil/modular.go
	sS := make([]int64, 0, len(S))
	for _, x := range S {
		if xx, err := ToElement(k, x); err == nil {
			sS = append(sS, xx.n)
		} else {
			return nil, errors.ArgNotContainedInParent.New()
		}
	}
	return &Element{
		n: mathutil.ProductInt64ModInt64(sS, k.characteristic),
		p: k.characteristic,
	}, nil
}

// DotProduct returns the dot-product of the slices S1 and S2. That is, it returns S1[0] * S2[0] + ... + S1[k] * S2[k], where k+1 is the minimum of len(S1) and len(S2). The dot-product of two empty slices is the zero element of k.
func (k *Parent) DotProduct(S1 []object.Element, S2 []object.Element) (object.Element, error) {
	// Convert S1 and S2 to slices of elements in k
	T1 := make([]*Element, 0, len(S1))
	for _, x := range S1 {
		xx, err := ToElement(k, x)
		if err != nil {
			return nil, err
		}
		T1 = append(T1, xx)
	}
	T2 := make([]*Element, 0, len(S2))
	for _, x := range S2 {
		xx, err := ToElement(k, x)
		if err != nil {
			return nil, err
		}
		T2 = append(T2, xx)
	}
	// Return the dot-product
	return DotProduct(k, T1, T2)
}

// Negate returns -n.
func (k *Parent) Negate(n object.Element) (object.Element, error) {
	nn, err := ToElement(k, n)
	if err != nil {
		return nil, err
	}
	return nn.Negate(), nil
}

// Power returns a^n. It returns an error if n is negative and a is not invertible.
func (k *Parent) Power(a object.Element, n *integer.Element) (object.Element, error) {
	aa, err := ToElement(k, a)
	if err != nil {
		return nil, err
	}
	return Power(aa, n)
}

// PowerInt64 returns a^n. It returns an error if n is negative and a is not invertible.
func (k *Parent) PowerInt64(a object.Element, n int64) (object.Element, error) {
	aa, err := ToElement(k, a)
	if err != nil {
		return nil, err
	}
	return PowerInt64(aa, n)
}

// Inverse returns 1/n. Returns an errors.DivisionByZero error if n is zero.
func (k *Parent) Inverse(n object.Element) (object.Element, error) {
	nn, err := ToElement(k, n)
	if err != nil {
		return nil, err
	}
	return nn.Inverse()
}

// IsUnit returns true iff n is a multiplicative unit (i.e. n is non-zero). If true, also returns the inverse element 1/n.
func (k *Parent) IsUnit(n object.Element) (bool, object.Element, error) {
	nn, err := ToElement(k, n)
	if err != nil {
		return false, nil, err
	} else if k == nil || k == gF1 {
		return true, gF1.Zero(), nil
	} else if nn.IsZero() {
		return false, nil, nil
	}
	nn, err = nn.Inverse()
	if err != nil {
		return false, nil, err
	}
	return true, nn, nil
}

// Divide returns the quotient x / y. Returns a errors.DivisionByZero error if y is zero.
func (k *Parent) Divide(x object.Element, y object.Element) (object.Element, error) {
	xx, yy, err := objectsToElements(k, x, y)
	if err != nil {
		return nil, err
	}
	return Divide(xx, yy)
}

// ScalarMultiplyByInteger returns nx, where this is defined to be x + ... + x (n times) if n is positive, -x - ... - x (|n| times) if n is negative, and the zero element if n is zero.
func (k *Parent) ScalarMultiplyByInteger(n *integer.Element, x object.Element) (object.Element, error) {
	xx, err := ToElement(k, x)
	if err != nil {
		return nil, err
	}
	return xx.ScalarMultiplyByInteger(n), nil
}

// AreEqual returns true iff x equals y.
func (k *Parent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, yy, err := objectsToElements(k, x, y)
	if err != nil {
		return false, err
	}
	return xx.IsEqualTo(yy), nil
}

// ToElement attempts to convert the given object.Element to an element of k. If necessary this will be done by first attempting to convert x to an integer or, if that fails, to a rational number.
func (k *Parent) ToElement(x object.Element) (object.Element, error) {
	return ToElement(k, x)
}

// FromInteger returns n as an element of k
func (k *Parent) FromInteger(n *integer.Element) object.Element {
	return FromInteger(k, n)
}

// FromRational returns q as an element of k. It returns an error if the denominator of q is not invertible in k.
func (k *Parent) FromRational(q *rational.Element) (object.Element, error) {
	return FromRational(k, q)
}

// Characteristic returns the characteristic of k.
func (k *Parent) Characteristic() *integer.Element {
	if k == nil {
		return integer.One()
	}
	return integer.FromInt64(k.characteristic)
}

// CharacteristicInt64 returns the characteristic of k.
func (k *Parent) CharacteristicInt64() int64 {
	if k == nil {
		return 1
	}
	return k.characteristic
}

// Order returns the order of k.
func (k *Parent) Order() *integer.Element {
	if k == nil {
		return integer.One()
	}
	return integer.FromInt64(k.order)
}

// OrderInt64 returns the order of k.
func (k *Parent) OrderInt64() int64 {
	if k == nil {
		return 1
	}
	return k.order
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// GF returns the finite field with p elements. p must be prime or 1.
func GF(p int64) (*Parent, error) {
	// Sanity checks
	if p < 1 {
		return nil, errors.ArgNotPrime.New()
	} else if p == 1 {
		return gF1, nil
	}
	// Acquire a read lock on the cache gFps and check the cache
	gFpsMutex.RLock()
	K, ok := gFps[p]
	gFpsMutex.RUnlock()
	if ok {
		return K, nil
	}
	// No luck -- check that n is a prime power
	if !integer.IsPrimeInt64(p) {
		return nil, errors.ArgNotPrime.New()
	}
	// Build a new instance of GF(p)
	newK := &Parent{
		order:          p,
		characteristic: p,
	}
	newK.zero = &Element{
		n: 0,
		p: p}
	newK.one = &Element{
		n: 1,
		p: p}
	// Acquire a write lock and check the cache once again before continuing
	gFpsMutex.Lock()
	if K, ok = gFps[p]; !ok {
		gFps[p] = newK
		K = newK
	}
	gFpsMutex.Unlock()
	return K, nil
}

// ToElement attempts to convert the given object.Element to an element of k. If necessary this will be done by first attempting to convert x to an integer or, if that fails, to a rational number, and then to a element of k.
func ToElement(k *Parent, x object.Element) (*Element, error) {
	// special case: finite field elements get mapped to the unique element of gF1
	if k == nil || k == gF1 {
		if y, ok := x.(*Element); ok {
			if y == nil || y.p == 1 {
				return Zero(gF1), nil
			} else {
				return nil, errors.ArgNotContainedInParent.New()
			}
		}
	}
	// otherwise try to convert the element
	if y, ok := x.(*Element); ok {
		if y == nil {
			// the nil *Element is only contained in gF1, and at this point we know that k is not nil or gF1
			return nil, errors.ArgNotContainedInParent.New()
		} else if isInParent(k, y) {
			return y, nil
		}
	} else if y, err := integer.ToElement(x); err == nil {
		return FromInteger(k, y), nil
	} else if y, err := rational.ToElement(x); err == nil {
		if result, err2 := FromRational(k, y); err2 == nil {
			return result, nil
		}
	}
	return nil, errors.ArgNotContainedInParent.New()
}
