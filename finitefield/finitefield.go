// Finitefield defines the interface satisfied by a finite field

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package finitefield

import (
	"bitbucket.org/pcas/math/field"
	"bitbucket.org/pcas/math/integer"
)

// Interface defines the interface that all finite fields must satisfy.
type Interface interface {
	field.Interface
	Order() *integer.Element          // the order of the field
	Characteristic() *integer.Element // the characteristic of the field
}
